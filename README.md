# MBASE to Laravel

Laravel package to be used by the https://github.com/iljubin/mbase2l project.

## Getting started

### Add repository to main project's composer.json file

```
composer config repositories.mbase2dtl vcs git@gitlab.com:mbase2/source-code.git
```

or manually:

```
"repositories": {
    "mbase2dtl": {
        "type": "vcs",
        "url": "git@gitlab.com:mbase2/source-code.git"
    }
}
```

See https://getcomposer.org/doc/05-repositories.md#loading-a-package-from-a-vcs-repository if you run into troubles.

### Require the package

```
composer require mbase2/dtl
```

### Refresh Laravel

```
sail artisan cache:clear
sail artisan route:cache
```

### Run the migrations
```
sail artisan migrate
```
## Dev environment

When .env APP_ENV is set to `local` the paths to libraries resolve to `http://localhost:8081` (cf. src/Controllers/Mbase2Controller.php), so, you are to run webpack dev server from the src/frontend host's (NOT from the docker container) folder of the package by running

```
npm install
npm start
```

If everything went well you should see MBASE modules on the same links as in the https://portal.mbase.org, e.g.:
- data (map) query: http://localhost/mbase2/map/
- genetic sample enter form: http://localhost/mbase2/modules/genetics/samples/sample
- genetic samples index: http://localhost/mbase2/modules/genetics/samples

## Production

There is publish directive tagged `laravel-assets` in the [Mbase2ServiceProvider.php](https://gitlab.com/mbase2/source-code/-/blob/master/src/Providers/Mbase2ServiceProvider.php) so all you have to do is to run (from the main application folder):

```
composer update mbase2/dtl
```

and run any pending migrations:

```
sail artisan migrate
```
