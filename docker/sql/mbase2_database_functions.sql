/**
To be imported before importing the mbase2 schema.
*/

CREATE OR REPLACE FUNCTION sync_date_modified() RETURNS trigger AS $$
BEGIN
	NEW.date_record_modified := NOW()::timestamp;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sync_date_created() RETURNS trigger AS $$
BEGIN
	NEW.date_record_created := NOW()::timestamp;
	NEW.date_record_modified := NEW.date_record_created;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_code_list() RETURNS trigger AS $$
declare
_list_key varchar;
_list_key_id integer;
BEGIN
select label_key into _list_key from mbase2.code_lists where id=new.list_id;
select id into _list_key_id from mbase2.code_list_options 
		where list_key='code_lists' and
		key = _list_key;
new.list_key := _list_key;
new.list_key_id := _list_key_id;
RETURN NEW;
END;
$$ LANGUAGE plpgsql;