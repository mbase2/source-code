psql -h localhost -U mbase_org -d postgres -c "drop database portal_mbase_org"
psql -h localhost -U mbase_org -d postgres -c "create database portal_mbase_org owner mbase_org"
psql -h localhost -U mbase_org -d portal.mbase.org -c "create extension postgis"
psql -h localhost -U mbase_org -d portal_mbase_org < $1
psql -h localhost -U mbase_org -d portal_mbase_org < /home/utils/recreate_code_list_options_fkeys.sql