# Organisations

- Each organisation can have one or more users.
- A user can be appointed as an organisation admin – one or more admins – admin can add new users to the organisation.

---

## Key Entities

### 1. **Organisation**:
   - An organisation represents a company, group, or entity.
   - It can have multiple users associated with it.
   - Each organisation can have one or more users who are designated as admins.

### 2. **User**:
   - A user can be a member of one or more organisations.
   - Users can be either normal users or admins within the organisation.
   - Admin users have additional privileges such as adding new users to the organisation.

---

## Relationships

- **Organisation to Users**: One-to-many relationship (An organisation can have multiple users).
- **User to Organisations**: Many-to-many relationship (A user can belong to multiple organisations).
- **Admin Role**: A user in an organisation can be an admin. There can be one or more admins in a single organisation, and admins have special permissions.
