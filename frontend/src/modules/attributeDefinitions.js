/**
 * Returns static definitions of variables (cf. mbase2.module_variables) for pages which are not counted as mbase2 modules
 * 
 * @param {*} exports 
 * @param {*} globals 
 * @returns {Object}
 */
export default function (exports, globals) {
    
    return {
        module_variables: (op) => [
            {
                key_name_id: 'module_id',
                _component: {
                    import: () => exports.Storage()
                }
            },
            {
                key_data_type_id: 'text',
                key_name_id: 'key_name_id',
                t_name_id: 'Variable slug',
                visible_in_table: true,
                read_only: true
            },
            {
                key_data_type_id: 'json',
                visible_in_table: true,
                key_name_id: 'translations',
                _op:{
                    attributes: globals.languages.map(key=>({label: key, key: key})),
                    accordion: 'Translations'
                }
            },
            {
                key_data_type_id: 'json',
                key_name_id: 'help',
                _op:{
                    attributes: globals.languages.map(key=>({label: key, key: key})),
                    accordion: 'Help'
                }
            },
            {
                key_data_type_id: 'code_list_reference',
                visible_in_table: true,
                key_name_id: 'data_type_id',
                ref: 'data_types',
                _component: {
                    default: true,
                    additionalComponentOptions: {
                      read_only: true  
                    }
                }
            },
            {
                key_data_type_id: 'reference',
                visible_in_table: true,
                key_name_id: 'ref',
                _component: {
                    import: () => exports.Storage(),
                    additionalComponentOptions: {
                        returnNullWhenEmpty: true  
                    }
                }
            },
            {
                key_data_type_id: 'boolean',
                key_name_id: 'visible_in_table'
            },
            {
                key_data_type_id: 'real',
                visible_in_table: true,
                key_name_id: 'weight'
            },
            {
                key_data_type_id: 'real',
                visible_in_table: true,
                key_name_id: 'weight_in_table'
            },
            {
                key_data_type_id: 'boolean',
                key_name_id: 'visible_in_popup'
            },
            {
                key_data_type_id: 'real',
                visible_in_table: true,
                key_name_id: 'weight_in_popup'
            },
            {
                key_data_type_id: 'boolean',
                visible_in_table: true,
                key_name_id: 'importable'
            },
            {
                key_data_type_id: 'boolean',
                visible_in_table: true,
                key_name_id: 'exportable'
            },
            {
                key_data_type_id: 'real',
                visible_in_table: true,
                key_name_id: 'weight_in_popup_cv'
            },
            {
                key_data_type_id: 'real',
                visible_in_table: true,
                key_name_id: 'weight_in_import'
            },
            {
                key_data_type_id: 'real',
                visible_in_table: true,
                key_name_id: 'weight_in_export'
            },
            {
                key_data_type_id: 'boolean',
                visible_in_table: true,
                key_name_id: 'filterable'
            }
        ],
        code_list_options: async () => {
            const utils = await exports.utils();
            const languages = await utils.request(globals.apiRoot + '/code_list_options?:list_key=languages');
            const t = utils.t;
            const languageKeys = languages.map(lang => lang.key);
            const languageKeysAttributes = languageKeys.map(key => ({label: key, key: key}));

            return [
                {
                    key_name_id: 'list_id',
                    _component: {
                        import: () => exports.Storage()
                    }
                },
                {
                    key_data_type_id: 'text',
                    key_name_id: 'key',
                    _component: {
                        default: true,
                        additionalComponentOptions: {
                          required: true  
                        }
                    }
                },
                {
                    key_data_type_id: 'json',
                    key_name_id: 'translations',
                    _op:{
                        attributes: languageKeysAttributes
                    }
                },
                {
                    key_data_type_id: 'boolean',
                    key_name_id: 'enabled'
                }
            ];
        } 
    }
}