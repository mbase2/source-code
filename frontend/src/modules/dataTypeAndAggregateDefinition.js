import * as exports from '../libs/exports';
import {t, patterns} from '../libs/utils';
import globals, {cache} from '../../app-globals';

/**
 * Translation form
 * 
 * @param {object} $parent a jQuery $container for the form
 * @param {object} cm ComponentManager object instance
 * @param {object} data 
 * @param {object} data.languages code list values of the available languages
 * @param {array<object>} data.aggregateDefs aggregate_defs table values
 * @param {object} [data.id] ID of the variable record for update
 */

export default async ($parent, cm, data = {}) => {
    const [
        Select2,
        Button,
        mutils,
        utils,
        ComponentManager
    ] = await Promise.all([
        exports.Select2(),
        exports.Button(),
        exports.mutils(),
        exports.utils(),
        exports.ComponentManager()
    ]);

    const dataTypes = data.dataTypes.map(a => a.key);
    const codeLists = data.codeLists.map(cl => [cl.id, cl.label_key]);
    const modules = data.modules.map(v => [v.id, v.key]);

    const {model} = data;

    const components = {
        data_type_key: {
            component: Select2.default,
            options: {
                data: dataTypes,
                badge: t`Data type`,
                required: true,
                onSelect: onDataTypeSelect
            },
            $parent: $parent
        },
        'code_list_reference': {
            component: Select2.default,
            options: {
                badge: t`Code lists`,
                data: codeLists
            },
            $parent: $parent,
            onComponentAppended: component => component.$el().hide()
        },
        'module_reference': {
            component: Select2.default,
            options: {
                badge: t`Module tables`,
                data: modules
            },
            $parent: $parent,
            onComponentAppended: component => component.$el().hide()
        }
    }

    utils.addKeysToComponents(components);
    
    cm = cm || new ComponentManager.default();
    
    await cm.add(components.data_type_key);
    await cm.add(components.code_list_reference);
    await cm.add(components.module_reference);

    //initial values
    cm.get('data_type_key').val('integer');

    //if model set values from the model
    if (model) {
        cm.model(model);
    }

    function onDataTypeSelect(e) {
        let dataType = utils.select2value(e);
        if (!dataType) return;
        const references = ['code_list_reference', 'module_reference'];

        references.map(r => cm.get(r).$el().hide());

        if (references.includes(dataType)) {
            cm.get(dataType).$el().show();
            dataType = 'integer'; //reference has integer dataType
        }
    }

}