import globals from '../app-globals'
import {onMarkerClicked} from '../helpers/onMarkerClicked'

let exports, utils, mutils, ComponentManager, Button, Map, DropDown, ModalDialog, DataFilter, batchesModule;
/**
 * 
 * @param {object} op 
 * @param {string} op.moduleKey
 * @param {boolean} op.defaultRecordEditor use default modal dialog for record editor if true, else use external editor (redirect to edit page)
 * @param {array} op.batchOptionsModel a function that should return array of attributes for import
 */
function Mbase2ModuleIndex(op ={}) {
    this.op=op;
    this.op.viewName = op.viewName || op.dataTable; //show data from dataTable if viewName is not defined
    this.$parent = op.$parent;
    this.op.schema = op.schema || 'mb2data';
}

let compare = null;

Mbase2ModuleIndex.prototype.init = async function() {
    exports = await import('../libs/exports');
    [   utils,
        mutils,
        ComponentManager,
        Button,
        Map,
        DropDown,
        ModalDialog,
        DataFilter,
        batchesModule
    ] = this.imports = await Promise.all([
        exports.utils(),
        exports.mutils(),
        exports.ComponentManager(),
        exports.Button(),
        exports.Map(),
        exports.DropDown(),
        exports.ModalDialog(),
        exports.DataFilter(),
        import('./batches')
    ]);

    compare = utils.compare;

    let filterValuesString = utils.getQueryParameter('f') || '';
    let spatialFilterValuesString = utils.getQueryParameter('sf') || '';
    let predefinedFilterValuesString = utils.getQueryParameter('pf') || '';

    let dataFilterCmp = null;

    let moduleVariables = null;

    let moduleVariablesForImport = null;

    await DataFilter.loadExports(exports);

    const {$content, $header} = this.op;

    const t = utils.t;

    const unameField = this.op.unameField || '_uname';

    const moduleKey = this.op.moduleKey;
    const mainModuleKey = this.op.mainModuleKey;

    this.isAdmin = mutils.permissions(['admin'], mainModuleKey || moduleKey) ? true : false;
    this.isEditor = mutils.permissions(['editor'], mainModuleKey || moduleKey) ? true : false;

    const cm = new ComponentManager.default();

    const $headerButtonGroup = $('<div/>',{style: 'display: flex'});

    this.op.buttonLabels = this.op.buttonLabels || {};

    const filterParametersString = mutils.buildFilterParametersString(filterValuesString, spatialFilterValuesString, predefinedFilterValuesString);

    this.filterParametersString = filterParametersString;
    
    const listOfImports = new ModalDialog.default(
        {
            size: 'modal-xl',
            onClose: () => {
             
            },
            onShown: onListOfImportsShown
    });

    function onListOfImportsShown() {
        listOfImports.$body.empty();
        batchesModule.default({
            $parent: listOfImports.$body,
            scrollY: '65vh',
            moduleKey: moduleKey,
            showDownloads: true
        });
    }

    const batchImportItems = [];
    const defaultBatchImportItems = [
        {
            key: 'import',
            label: t`Import from XLSX`
        },
        {
            key: 'list',
            label: t`List of imports`
        }
    ]; 

    if (this.op.additionalBatchImportOptions) {
        const additionalBatchImportItemsByKey = {};

        this.op.additionalBatchImportOptions.map(item => {
            additionalBatchImportItemsByKey[item.key] = item;
        });

        //override default item if default key exists in additional items
        defaultBatchImportItems.map(item => {
            if (additionalBatchImportItemsByKey[item.key]) {
                batchImportItems.push(additionalBatchImportItemsByKey[item.key]);
            }
            else {
                batchImportItems.push(item);
            }
        });

        //add non existing
        const itemsByKey = {};
        batchImportItems.map(item => {
            itemsByKey[item.key] = item;
        });

        this.op.additionalBatchImportOptions.map(item => {
            if (!itemsByKey[item.key]) {
                batchImportItems.push(item);
            }
        });
    }


    this.buttons = [...[
        mutils.permissions(['admin'], mainModuleKey || moduleKey) && this.op.adminButton, 
        mutils.permissions(['admin','curator'], mainModuleKey || moduleKey) && {

            key: 'batch_import',
            classes: 'btn-lg',
            component: DropDown,
            type: 'btn-primary',
            iconClass: 'file',
            items: batchImportItems.length > 0 ? batchImportItems : defaultBatchImportItems,
            label: globals.t`Batch imports`,
            onClick: async ({key}) => {
                if (key==='import') {
                    mutils.batchImport({
                        batch: await import('./batchImport'),
                        ModalDialog: ModalDialog
                    }, moduleKey, moduleVariablesForImport, null, t`Data import`, 'xlsx', this.op.batchOptionsFkeys);
                }
                else if (key==='list') {
                    listOfImports.show();
                }
                else if (this.op.additionalBatchImportOptions) {
                    const o = this.op.additionalBatchImportOptions.find(o=>o.key === key);
                    if (o) {
                        o.callback && o.callback(this);
                    }
                }
                    
                return false;
            }   
        },
        !(this.op.skipExportButton) && mutils.permissions(['admin','editor','consumer'], mainModuleKey || moduleKey) && {
            key: 'export',
            label: t`Export`,
            iconClass:'download',
            type: 'btn-primary',
            classes: 'btn-lg',
            onClick: () => {
              utils.hrefClick(globals.apiRoot+'/export/'+moduleKey+filterParametersString, null);
            }
        },
        !(this.op.skipNewButton) && mutils.permissions(['admin','editor'], mainModuleKey || moduleKey) && {
            key: 'new',
            label: this.op.buttonLabels.new || t`New`,
            iconClass: 'plus',
            path: this.op.singleRecordPath
        },
        !(this.op.skipFilterButton) && mutils.permissions(['admin','editor','consumer','reader'], mainModuleKey || moduleKey) && {
            key: 'filter',
            label: this.op.buttonLabels.filter || t`Filter`,
            iconClass:'filter',
            type:'btn-primary',
            classes:'btn-lg'
        }
    ], ...(this.op.additionalButtons || [])].filter(btn => btn);

    if (this.op.overrideToolbarButtons) {
        this.op.overrideToolbarButtons.map(ovr => {
            const inx = this.buttons.findIndex(btn => btn.key == ovr.key);
            if (inx !== -1) {
                this.buttons[inx] = ovr;
            }
        });
    }

    if (this.op.beforeButtonsAdded) {
        this.buttons = this.op.beforeButtonsAdded(this.buttons);
    }

    this.buttons && this.buttons.map(btn => {
        const btnCmp = btn.component ? new btn.component.default(btn) : new Button.default({
            label: btn.label,
            iconClass: btn.iconClass,
            type: btn.type || 'btn-primary',
            classes: btn.classes !== undefined ? btn.classes : 'btn-lg',
            onClick: btn.key==='new' ? () => {
                const pathname = utils.rtrimSlash(window.location.pathname);
                utils.hrefClick(pathname+btn.path);
            } : btn.onClick
        }); 
        btn.cmp = btnCmp;
        btnCmp.$el().css('margin-left', '10px');
        $headerButtonGroup.append(btnCmp.$el());
    })
    
    $header.html(mutils.moduleHeader(this.op.title,null,$headerButtonGroup));

    const mapHeight = this.op.mapHeight || 50;

    const $map = $('<div/>', {style:`height:calc(${mapHeight}vh - 142px); margin-bottom:22px`});
    !this.op.withoutMap && $content.append($map);

    let dataTable = this.table = null;

    //TODO: pass attributes
    const mapComponent = this.mapComponent = !this.op.withoutMap && new Map.default({
        $container: $map,
        onMarkerClicked: (marker, data, moduleName, actionCallback) => {
            onMarkerClicked(marker, data, {
            dataTable,
            moduleKey,
            moduleAttributes: moduleVariables
        }, actionCallback)}
    });

    moduleVariables = this.op.variables || await mutils.getModuleVariables(this.op.moduleKey, true, false);

    this.moduleVariables = [...moduleVariables];

    if (this.op.additionalVariables) {
        moduleVariables = await this.op.additionalVariables(moduleVariables);
    }

    if (this.op.additionalComponentOptions) {
        mutils.assignAdditionalComponentOptions(moduleVariables, this.op.additionalComponentOptions);
    }

    const allRefValues = this.allRefValues = await mutils.getRefCodeListValues(moduleVariables);

    const $tableDiv = this.$tableDiv = $('<div/>');

    $content.append($tableDiv);
    
    const tableRecordsOptions = await mutils.generalTableRecordsOptions($tableDiv, this.op.moduleKey, moduleVariables, allRefValues); //mutils.generalTableRecordsOptions also returns reference values. If you filter variables before this function you loose the referenced values for e.g. batch import form

    const skipVariables = this.op.skipVariables || [];

    const variables = tableRecordsOptions.variablesAttributes = moduleVariables.sort((a,b) => compare(a,b,'weight_in_table')).filter(v => skipVariables.indexOf(v.key_name_id)===-1 && v.visible_in_table === true);

    const columns = variables.map(v => v.key_name_id);

    if (this.op.batchOptionsModel) {
        moduleVariablesForImport = await this.op.batchOptionsModel(moduleVariables);
    }
    else {
        moduleVariablesForImport = [...moduleVariables];
    }

    tableRecordsOptions.batchOptions = {
        model: Array.isArray(moduleVariablesForImport) ? {[this.op.moduleKey]: moduleVariablesForImport} : moduleVariablesForImport,
        fkeys: this.op.batchOptionsFkeys
    }
    
    tableRecordsOptions.cm = cm;
    tableRecordsOptions.saveOptions = this.op.saveOptions || {};
    tableRecordsOptions.sortVariables = this.op.sortVariables!==undefined ? this.op.sortVariables : false;
    tableRecordsOptions.splitRequiredAndOptionalAttributesWhenSorting = this.op.splitRequiredAndOptionalAttributesWhenSorting;
    tableRecordsOptions.url = globals.apiRoot + `/${this.op.schema}/${this.op.viewName}` + filterParametersString; 
    tableRecordsOptions.urlForDelete = this.op.urlForDelete;
    tableRecordsOptions.skipId = true;
    tableRecordsOptions.deletable = this.op.deletable !== undefined ? this.op.deletable : ((this.isAdmin || this.isEditor) ? true : false);
    tableRecordsOptions.disableEdit = this.op.disableEdit !== undefined ? this.op.disableEdit : false;
    tableRecordsOptions.editRecordModel = this.op.editRecordModel;
    tableRecordsOptions.select = this.op.select;
    tableRecordsOptions.tableName = this.op.tableName || this.op.moduleKey;

    tableRecordsOptions.onRowDeleted = this.op.onRowDeleted;
    tableRecordsOptions.onSuccessfullySaved = this.op.onSuccessfullySaved;

    tableRecordsOptions.onTableDataProcessed = this.op.onTableDataProcessed;

    tableRecordsOptions.rowEditOptions = row => [true, tableRecordsOptions.deletable && row.__deletable]
    
    const pathname = utils.rtrimSlash(window.location.pathname);
    
    tableRecordsOptions.externalEditRecord = this.op.defaultRecordEditor ? false : (row) => {
        utils.hrefClick(pathname+`${this.op.singleRecordPath}?fid=`+row.id);
    }

    const tableHeightAdjustment = this.op.tableHeightAdjustment || '-142px';

    tableRecordsOptions.tableOptions = {
        onRowSelected: this.op.onRowSelected,
        scrollY: this.op.withoutMap ? 'calc(85vh - 142px)': `calc(${tableHeightAdjustment} + ${100-mapHeight}vh)`,
        exportButtons: false,
        scrollCollapse: false,
        __icons: this.op.withoutMap ? [] : [
            {
                action: 'zoom-to-marker',
                class: 'map',
                style: 'margin-left:5px',
                onClick: (data) => {
                    const marker = data.__marker;
                    marker && mapComponent.zoomToMarker(marker, data, moduleKey);
                }
            }
        ]
    }

    if (this.op.additionalTableIcons) {
        tableRecordsOptions.tableOptions.__icons = [...tableRecordsOptions.tableOptions.__icons, ...this.op.additionalTableIcons];
    }

    if (this.op.hideColumnsFromTable) {
        tableRecordsOptions.tableOptions.hideColumns = [...this.op.hideColumnsFromTable];
    }

    tableRecordsOptions.btn_add_record = false;
    tableRecordsOptions.btn_batch_import = false;

    if (this.op.onInitRecord) {
        tableRecordsOptions.onInitRecord = this.op.onInitRecord;
    }

    /**
     * Formats the date in record time stamps
     * Adds __deletable property - __deletable is used to show trash can icon
     * 
     * @param {array} data raw data
     * @returns {array} preprocessed data
     */

    tableRecordsOptions.preprocessTableData = (data) => {

        mutils.convertJsonToObjects(data, variables);

        data.map((r,inx) => {
            const d = r;

            r.__deletable = this.isAdmin || globals.user.uid == r[unameField];
            
            columns.map(c => r[c] = d[c] === undefined ? null : d[c]);

            d.date_record_created = d.date_record_created && moment.utc(new Date(d.date_record_created+'Z')).format('YYYY-MM-DD HH:mm:ss');
            d.date_record_modified = d.date_record_modified && moment.utc(new Date(d.date_record_modified+'Z')).format('YYYY-MM-DD HH:mm:ss');
        });

        if (this.op.preprocessTableData) {
            data = this.op.preprocessTableData(data) || data;
        }

        return data;
    }

    tableRecordsOptions.onTableCreated = (table, data) => {
        if (mapComponent) {
            dataTable = this.table = table;
            mapComponent.addLayer(data, moduleKey, false, this.op.moduleProps && JSON.parse(this.op.moduleProps.properties) || {});
        }

        this.op.onTableCreated && this.op.onTableCreated(table, data);
    }

    tableRecordsOptions.tableOptions = Object.assign(tableRecordsOptions.tableOptions, this.op.tableOptions || {});

    const filterVariables = moduleVariables.filter(a => a.filterable === true)
    .sort((a,b) => compare(a,b,'weight_in_filter'));

    const btnFilter = this.buttons.find(b=>b.key === 'filter');

    const $dataFilter = btnFilter && btnFilter.cmp.$el();

    const executeQuery = this.executeQuery = (_predefinedFilterValuesString) => {
        predefinedFilterValuesString = _predefinedFilterValuesString || predefinedFilterValuesString;

        window.location = window.location.origin + window.location.pathname + mutils.getFilterParametersString(dataFilterCmp, false, predefinedFilterValuesString);
    }

    function updateFilterIcon(fvals) {
        const $icon = $dataFilter.find('.fa.fa-filter');
        
        if (fvals && fvals.length > 0) {
            $icon.css('color', 'red');
        }
        else {
            $icon.css('color', null);
        }
    }

    dataFilterCmp = this.dataFilterCmp = new DataFilter.default({
        attributesDefinition: filterVariables,
        refValues: allRefValues,
        $filter: $dataFilter,
        apply: {
            button:{label: t`Apply`},
            callback: () => executeQuery()
        },
        onValueSet: updateFilterIcon
    });

    dataFilterCmp.setValueFromString(filterValuesString, spatialFilterValuesString);

    this.predefinedFilterValuesString = predefinedFilterValuesString;

    const recordsTable = await import('./recordsTable');
    const res = await recordsTable.default(tableRecordsOptions);
    return res;
}

export default Mbase2ModuleIndex;
