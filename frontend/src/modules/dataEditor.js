/**
 * mbase2 data editor
 */

import * as exports from '../libs/exports';
import globals from '../app-globals';

/**
 * @param {object} op options
 * @param {object} op.$parent
 * @param {string} [op.key_id] key (name) of default table to open (this is tableName - name of the table without the table schema, it is called key_id for compatibility reasons)
 * @param {string} [op.title] title for display (if omitted key_id is used)
 */
export default async op => {

    const {$parent, key_id, title} = op;

    const [
        ComponentManager,
        utils,
        mutils
    ] = await Promise.all([
        exports.ComponentManager(),
        exports.utils(),
        exports.mutils()
    ]);

    const t = utils.t;

    let dataEditor = {  //this is for the export
        tableRecordsInstance: null
    }

    if (key_id) {
        op.skipTitle!==true && $parent.prepend(`<h2>${title || key_id}</h2><hr>`);

        const tableName = key_id;

        const variables = (op.variables || await mutils.getModuleVariables(tableName, false, false, true)).sort((a,b)=>utils.compare(a,b,'weight_in_table'));

        const refValues = await mutils.getRefCodeListValues(variables);

        const tableEditor = await import('./tableEditor');

        dataEditor.tableRecordsInstance = await tableEditor.default(Object.assign(op, {
            refValues: refValues,
            variables: variables,
            sortVariables: false,
            $parent: $parent,
            tableName: tableName,
            schema: op.schema,
            tableRecordsOptions: op.tableRecordsOptions
        }));

        return dataEditor;
    }

    let tables = [];

    if (op.tables) {
        tables = op.tables;
    }
    else {

        tables = await batchRequestHelper([
            `modules/language/${globals.language}`, 
            `referenced_tables/language/${globals.language}`
        ]);
    
        tables[1].map(item => {

            if (item.t_id) {
                item.t_id = item.t_id + '*';
            }
            else {
                item.t_id = item.key_id + '*';
            }

        });

        tables = [...tables[0], ...tables[1]];
    }

    const components = {
        modules: exports.select({
            label: t`Modules`,
            onSelect: onSelectModuleName,
            data: {
                values: tables
            },
            process: item => ({value: item.id, text: (item.t_id || item.key_id)}),
            $parent: $parent
        })
    };

    const cm = new ComponentManager.default();

    await utils.loadComponents(components);
    await cm.add(components.modules);

    async function onSelectModuleName(e) {
        const moduleId = utils.select2value(e);
        if (!moduleId) return;

        const tableName = cm.getData('modules').filter(m => m.id == moduleId)[0].key_id;

        const references = await mutils.requestHelper(globals.apiRoot + `/module_references/language/${globals.language}?:module_id=${moduleId}`);

        const allVariablesArray = [];
        Object.values(references).map(arr => allVariablesArray.push(...arr));
        
        const refValues = await mutils.getRefCodeListValues(allVariablesArray);

        const tableEditor = await import('./tableEditor');
        dataEditor.tableRecordsInstance = await tableEditor.default(Object.assign(op, {
            refValues: refValues,
            variables: references[tableName],
            $parent: $parent,
            tableName: tableName
        }));
    }

    return dataEditor;
} 