import * as exports from '../libs/exports';
import globals from '../app-globals';

/**
 * Translation form
 * 
 * @param {object} op
 * @param {object} op.$parent a jQuery $container for the form
 * @param {object} op.cm ComponentManager object instance
 * @param {object} op.refValues
 * @param {object} op.allModelAttributes allModelAttributes as defined in op.model parameter
 * @param {array|object} op.model values referenced by the attributes that are shown in select element; if array is passed as argument it gets converted to associative object of arrays based on 'id' property
 * @param {string} op.tableName name of the table to which the data is imported
 */

export default async op => {    

    const [
        utils, 
        mutils,
        ComponentManager,
        Button,
        Accordion,
        TTomSelect,
        ButtonGroup,
        ModalDialog,
        batchesModule,
        TranslationHandler,
        DropDown,
        Inputs
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        exports.ComponentManager(),
        exports.Button(),
        exports.Accordion(),
        exports.TTomSelect(),
        exports.ButtonGroup(),
        exports.ModalDialog(),
        import('./batches'),
        exports.TranslationHandler(),
        exports.DropDown(),
        exports.Inputs()
    ]);

    const th = new TranslationHandler.default();

    th.addCodeList('mbase2_ui');
    th.addCodeList('modules');

    const {$parent, model, tableName} = op;

    if (tableName === 'gensam') {
        th.addCodeList('gensam_ui');
    }

    const ColumnMapComponent = function(op = {}) {
        const $el = $('<div/>');
        const $badge = $(`<span class="badge">${op.label}</span>`);
        if (op.required === true) {
            $badge.addClass('required');
        }
        const btnGroup = new ButtonGroup.default(op.btnGroup.dataEntryOptions, {cm: op.cm, key: op.key, table: op.table, cmParent: op.cmParent, dataType: op.dataType});
        const $placeholder = this.$placeholder = $('<div/>');
        $el.append($badge);
        $el.append(btnGroup.$el());
        $el.append($placeholder);

        this.$el = () => $el;

        this.val = function (value) {
            return btnGroup.val(value);
        }
    }

    const CoordinateMapComponent = function (data) {
        const $el = $('<div/>');
        $el.css('display','flex');
        $el.css('flex-flow', 'row wrap');
        $el.css('border','solid 1px #9c9c9c');
        $el.css('width','100%');
        $el.css('padding','5px');
        
        const lat = this.lat = new TTomSelect.default({
            label: th.t`latitude`,
            data: data,
            configurationOverrides: {
                plugins: ['dropdown_input', 'remove_button']
            }
        });

        const lon = this.lon = new TTomSelect.default({
            label: th.t`longitude`,
            data: data,
            configurationOverrides: {
                plugins: ['dropdown_input', 'remove_button']
            }
        });

        const crs = this.crs = new TTomSelect.default({
            label: th.t`CRS`,
            onSelect: () => {
                if (this.crs) {
                    if (!this.crs.val()) {
                        this.crs.val(4326);
                    }
                }
            },
            data: [
                [3912, 'EPSG:3912 D48/GK'],
                [4326, 'EPSG:4326 WGS84/φλ – GPS'], 
                [3794, 'EPSG 3794 - D96/TM'],
                [3765, 'EPSG 3765 - HTRS96/TM'],
                [8677, 'EPSG 8677 - MGI 1901 / Balkans zone 5'],
                [32632,'EPSG:32632 - WGS 84 / UTM zone 32N']
            ]});

        crs.val(4326);

        const $lat = lat.$el();
        const $lon = lon.$el();
        const $crs = crs.$el();

        $lat.css('width', '50%');
        $lon.css('width', '50%');
        $crs.css('width', '100%');

        $el.append(lat.$el());
        $el.append(lon.$el());
        $el.append(crs.$el());

        this.$el = () => $el;
    };

    CoordinateMapComponent.prototype.val = function(value) {
        if (value !== undefined) {
            this.lat.val(value.lat);
            this.lon.val(value.lon);
            this.crs.val(value.crs);
        }
        return {
            lat: this.lat.val(),
            lon: this.lon.val(),
            crs: this.crs.val()
        }
    }

    CoordinateMapComponent.prototype.text = function(value) {
        if (value !== undefined) {
            if (value) {
                this.lat.text(value.lat);
                this.lon.text(value.lon);
                this.crs.val(value.crs);
            }
            else {
                this.lat.val(null);
                this.lon.val(null);
            }
        }

        return {
            lat: this.lat.text(),
            lon: this.lon.text(),
            crs: this.crs.val()
        }
    }

    const {componentDefinitions, definitionAliases} = (await import('../libs/components/componentDefinitions'));
    const importDefinitions = componentDefinitions(exports);
    const aliases = definitionAliases(utils.patterns);

    /** if container is modal window it can have $body, $title and $footer parts */
    const $body = $parent.$body || $parent;
    const $title = $parent.$title;
    const $footer = $parent.$footer;

    const propertiesKeys = {};

    $title.html($title.html() + '&nbsp;-&nbsp;' + tableName);

    /*accordion*/
    const acc = new Accordion.default({$parent: $body, closeOthers: false});
    const dsPanel = acc.addPanel(th.t`Data source properties`);
    const cmPanel = acc.addPanel(th.t`Variable mappings`);
    
    const fup = await import('../helpers/fileUpload');
    let fileUploadCm;

    let subfolder = tableName || 'mbase2';
    subfolder=subfolder.split('_');
    if (subfolder.length>1) {
        subfolder = subfolder[0] + '_%25';
    }
    else {
        subfolder = subfolder[0];
    }

    fileUploadCm = await fup.default({onSelect: onDataFileSelect, $parent: dsPanel.$body, subfolder: subfolder, ext: op.ext});

    let pageSelector;

    pageSelector = new TTomSelect.default({
        label: th.t`Select a page in the uploaded xlsx file`,
        onSelect: onSelectPage
    });
    dsPanel.$body.append(pageSelector.$el());

    const cms = {};

    let updateKeyComponent = null;

    const importType = new TTomSelect.default({
        data: [
                ['insert',th.t`INSERT - append all the rows from the selected page to the database if possible`],
                ['update',th.t`UPDATE - ignore the rows with unmatched update ID and update only the defined columns`]
            ],
        label: th.t`How would you like to import the data?`,
        onSelect: (value) => {
            
            if (!updateKeyComponent) return;

            const $el = updateKeyComponent.$el();

            if (value==='insert') {
                $el.hide();
            }   
            else {
                $el.show();
            }
        }
    });

    tableName === 'gensam' && dsPanel.$body.append(importType.$el());

    updateKeyComponent = new TTomSelect.default({
        label: th.t`Select a variable holding an ID for update`,
        data: (op.allModelAttributes || model[tableName]).map(v=>{
            let label = v.t_key_name_id || v.key_name_id;
            if (label !== v.key_name_id) {
                label = label + ` (${v.key_name_id})`;
            }

            return [v.key_name_id, label];
        })
    });

    dsPanel.$body.append(updateKeyComponent.$el());

    const dateFormat = new TTomSelect.default({
        data: [
                ['Y-m-d','YYYY-MM-DD'],
                ['d.m.Y','DD. MM. YYYY']
            ],
        label: th.t`What format are the dates in?`
    });

    dsPanel.$body.append(dateFormat.$el());
    dateFormat.val('Y-m-d');

    updateKeyComponent.$el().hide();

    const delimiter = new Inputs.Input({
        type: 'textbox',
        label: `Delimiter for multivalued fields`
    });

    delimiter.val(',');

    dsPanel.$body.append(delimiter.$el());

    importType.val('insert');

    let moduleKey = null;
    const applyExistingModal = new ModalDialog.default(
        {
            onClose: () => {            
             
            },
            onShown: onApplyImportDefinitionsModalShown
    });

    const applySavedModal = new ModalDialog.default(
        {
            onClose: () => {            
             
            },
            onShown: onApplySavedImportDefinitionsModalShown
    });

    const applyTemplateDropDown = new DropDown.default({
        items: [
            {
                key:'previous_import',
                label: th.t`Apply import definition from previous import`
            },
            {
                key:'saved',
                label: th.t`Apply saved import definition`
            }
        ],
        label: th.t`Saved import definitions`,
        onClick: ({key}) => {
                key === 'saved' ? applySavedModal.show() : applyExistingModal.show();
            }     
    });

    const $panelHeader = cmPanel.$panel.find('.panel-heading');
    $panelHeader.append(applyTemplateDropDown.$el());
    $panelHeader.css('display','flex').css('justify-content','space-between');
    applyTemplateDropDown.$el().find('.dropdown-menu').addClass('dropdown-menu-right');
    applyTemplateDropDown.$el().hide();

    const $variableDefinitionsParent = $('<div/>');
    cmPanel.$body.append($variableDefinitionsParent);
    
    /*import button*/
    const importBtn = new Button.default({
        label:th.t`Import`,
        //iconClass: 'upload',
        type: 'btn-primary',
        onClick: onImport
    });

    const saveDefinitionBtn = new Button.default({
        label:th.t`Save import definition`,
        onClick: onSaveColumnMap
    });

    $footer.append(saveDefinitionBtn.$el());
    $footer.append(importBtn.$el());

    $footer.css('display','flex').css('justify-content','flex-end');

    importBtn.$el().hide(); //shown in mappings function
    saveDefinitionBtn.$el().hide();
    
    /*DropDown*/
    /*const dropdown = new DropDown.default({
        label: th.t`Actions`,
        items: [
            {key: 'apply_template', label: th.t`Apply form template ...` },
            {key: 'save_template', label: th.t`Save form template ...` }
        ],
        onClick: onAction
    });

    $title.after(dropdown.$el());
    */

    const dataMapComponentDefinitions = {};

    function onAction() {
        
    }

    function onApplySavedImportDefinitionsModalShown() {
        const url = globals.apiRoot + `/mbase2/import_templates_vw?:__filter=[["key_module_id","like","%${op.tableName}%"]]`;
        const fields = [
            {
                key_data_type_id: 'text',
                key_name_id: '__username',
                t_name_id: 'Uporabnik',
                visible_in_table: true
            },
            {
                key_data_type_id: 'date',
                key_name_id: '__date',
                t_name_id: 'Datum',
                visible_in_table: true
            },
            {
                key_data_type_id: 'text',
                key_name_id: 'note',
                t_name_id: 'Opomba',
                visible_in_table: true
            }
        ];
        onApplyImportDefinitionsModalShown(applySavedModal, url, fields);
        
    }

    function onApplyImportDefinitionsModalShown(modalDialog = applyExistingModal, url = null, fields=null) {
        modalDialog.$body.empty();
        batchesModule.default({
            $parent: modalDialog.$body,
            fields: fields,
            url: url,
            skipId:false,
            moduleKey:op.tableName,
            scrollY: '65vh',
            applyExisting: {
                module: op.tableName,
                onRowSelected: (dataObject) => {
                    const data = JSON.parse(dataObject.data);
                    const dataDefinitionsAll = data.dataDefinitions || data;
                    Object.keys(dataDefinitionsAll).map(moduleKey => {
                        const dataDefinitions = dataDefinitionsAll[moduleKey];

                        Object.keys(dataDefinitions.types).map(key=> cms[moduleKey] && cms[moduleKey].cmParent &&  cms[moduleKey].cmParent.get(key) && cms[moduleKey].cmParent.get(key).val(dataDefinitions.types[key]));

                        if (dataDefinitions.headers) {
                            setTimeout(() =>
                                Object.keys(dataDefinitions.headers).map(key=> cms[moduleKey] && cms[moduleKey].cm && cms[moduleKey].cm.get(key) && cms[moduleKey].cm.get(key).text(dataDefinitions.headers[key])),
                            100);
                        }
                        else {
                            setTimeout(() =>
                                Object.keys(dataDefinitions.values).map(key=> cms[moduleKey] && cms[moduleKey].cm && cms[moduleKey].cm.get(key) && cms[moduleKey].cm.get(key).val(dataDefinitions.values[key])),
                            100);
                        }
                    });

                    modalDialog.hide();
                    
                }
            }
        });
    }

    async function onDataFileSelect(e) {
        if (!fileUploadCm) return;
        const pages = await mutils.requestHelper(globals.apiRoot + `/get_data_source_pages/${fileUploadCm.get('select').val()}`); 
        pageSelector.reinit(pages.map((p, inx) => [inx, p] ));
        pageSelector.val(0, false);
    }

    async function onSelectPage(e) {
        if (!pageSelector) return;
        const columns = await mutils.requestHelper(globals.apiRoot + `/get_data_source_page_columns/${fileUploadCm.get('select').val()}/${pageSelector.val()}`);
        mappings(columns.map((c,inx) => [inx, c]));
    }

    function mappings(cnames) {
        if (!cnames || cnames.length === 0) return;
        applyTemplateDropDown.$el().show();
        const defaultDataEntryOptions = {
            buttons: [
                {
                    key: 'column', 
                    label: th.t`column`
                },
                {
                    key: 'fixed', 
                    label: th.t`fixed`
                }
            ],
            onClick: onDataEntryOptionClicked
        };

        const tableNames = Object.keys(model);

        let multipleTableDefinitionAccordion = null;

        if (tableNames.length > 1) {
            $variableDefinitionsParent.empty();
            multipleTableDefinitionAccordion = new Accordion.default({$parent: $variableDefinitionsParent, closeOthers: false});
        }

        for (let i = 0; i < tableNames.length-1; i++) {
            let tableAttributeName = tableNames[i];
            const schemaPointIndex = tableAttributeName.indexOf('.');
            if (schemaPointIndex !== -1) {
                tableAttributeName = tableAttributeName.substring(schemaPointIndex+1,schemaPointIndex.length);
            }
            
            tableAttributeName = tableAttributeName + '_id';

            const nextTableName = tableNames[i+1];
            const attributeToHide = model[nextTableName].find(a => a.key_name_id === tableAttributeName);

            if (attributeToHide) {
                attributeToHide.__hide = true;                
            }
        }

        tableNames.map(async table => {
            moduleKey = table;
            
            let modelVariables = model[table];
            if (!modelVariables) return;
            if (modelVariables.length === 0) return;

            cms[table] = {
                cmParent: new ComponentManager.default(),
                cm: new ComponentManager.default()
            };
            dataMapComponentDefinitions[table] = {};
            
            const cm = cms[table].cm;
            const cmParent = cms[table].cmParent;

            let $parent = null;
            
            if (multipleTableDefinitionAccordion) {
                const panel = multipleTableDefinitionAccordion.addPanel(table);
                $parent = panel.$body;
            }
            else {
                $parent = $variableDefinitionsParent;
                $variableDefinitionsParent.empty();
            }

            let propsPanel = null;
            let labPanel = null;

            const panels = {};

            if (table === 'gensam') {
                const $div = $('<div/>');
                $parent.append($div);
                const $props = $('<div/>');
                $parent.append($props);

                $parent = $div;
                
                const acc = new Accordion.default({$parent: $props, closeOthers: false});
                propsPanel = acc.addPanel(th.t`Sample properties`);
                labPanel = acc.addPanel(th.t`Lab data attributes`);
                
                const lang = globals.language;

                for (const r of modelVariables) {
                    if (!r.key_name_id.includes('sample_properties.')) continue;
                    r.key_data_type_id = r.data_type;
                    r.key_name_id = r.variable_name;
                    propertiesKeys[r.key_name_id] = true;
                    r.t_name_id = (r.translations && utils.jsonParse(r.translations)[lang]) || r.key_name_id;
                    
                    if (r.t_name_id !== r.key_name_id) {
                        r.t_name_id = r.t_name_id + ` (${r.key_name_id})`;
                    }
                };
            }
            else if (table === 'mortbiom') {
                const acc = new Accordion.default({$parent: $parent, closeOthers: false});
                ['animal', 'biometry_animal_handling','biometry_data'].map(key=>{
                    panels[key] = acc.addPanel(key);
                });

                modelVariables = [...modelVariables.filter(v=>v.required),...modelVariables.filter(v=>!v.required)];
            }

            for (let ainx = 0; ainx < modelVariables.length; ainx++) {
                const v = modelVariables[ainx];
                if (v.read_only === true) continue;

                //if (v.key_data_type_id === 'table_reference') continue;

                const key = v.key_name_id;
                const dataType = v.key_data_type_id;
                
                let buttons = ['location_reference', 'location_data_json', 'location_geometry'].indexOf(dataType) !== -1 ? 
                    defaultDataEntryOptions.buttons.filter(btn=>btn.key==='column') : [...defaultDataEntryOptions.buttons];

                dataMapComponentDefinitions[table][key] = {};

                const dataEntryOptions = Object.assign({}, defaultDataEntryOptions);

                if (table === 'ct' && key === 'trap_station_name') {
                    buttons.push({
                        key: 'from_location',
                        label: th.t`from location`
                    });
                }
                else if (table === 'gensam' && key === 'sample_code') {
                    buttons.push({
                        key: 'auto_generate',
                        label: th.t`Auto generate`
                    });
                }

                let $parentContainer = null;

                if (table === 'gensam' && v.section_in_form==='lab') {
                    $parentContainer = labPanel.$body;
                }
                else if (table === 'mortbiom') {
                    $parentContainer = panels[v.section_in_form].$body;
                }
                else if (propertiesKeys[key]) {
                    $parentContainer = propsPanel.$body;
                }
                else {
                    $parentContainer = $parent;
                }

                dataEntryOptions.buttons = buttons;
    
                const cmc = await cmParent.add({
                    key: key,
                    component: ColumnMapComponent,
                    options: {
                        btnGroup: {
                            dataEntryOptions: dataEntryOptions
                        },
                        label: (v.t_name_id || mutils.parseTranslation(v)) + ` (${v.key_data_type_id})`, 
                        cmParent: cmParent,
                        cm: cm,
                        key: key, 
                        table: table,
                        dataType: dataType,
                        required: v.required
                    },
                    $parent: $parentContainer
                });

                if (v.__hide) {
                    cmc.$el().hide();
                }

                if (table === 'ct' && key === 'trap_station_name') {

                    dataMapComponentDefinitions[table][key].from_location = {
                        key: key,
                        component: CoordinateMapComponent,
                        options: cnames,
                        $parent: cmc.$placeholder
                    }
                }

                dataMapComponentDefinitions[table][key].column = ['location_reference', 'location_data_json', 'location_geometry'].indexOf(dataType) !== -1 ? 
                {
                    key: key,
                    component: CoordinateMapComponent,
                    options: cnames,
                    $parent: cmc.$placeholder
                }
                :
                {
                    key: key,
                    component: TTomSelect.default,
                    options: {
                        configurationOverrides: {
                            plugins: ['dropdown_input', 'remove_button']
                        }
                    },
                    data: {
                        values: cnames
                    },
                    $parent: cmc.$placeholder
                };

                cmc.$placeholder.css('margin-bottom','8px');

                cmc.val('column');

                await addComponentDefinition(v, 'fixed'); //without await it works "strangely" - i.e. asynchronously
                async function addComponentDefinition(variableAttributes, type, _cdef = null) {
                    const cdef = dataMapComponentDefinitions[table][key][type] = await mutils.getComponentDefinition(variableAttributes, _cdef || importDefinitions, aliases, op.refValues);
                    
                    if (cdef && cdef.component && cdef.component.name === 'TCheckBox') {
                        cdef.options = Object.assign(cdef.options || {},{wrapperClass: 'cb-import'});
                    }

                    if (cdef.options && cdef.options.label) {
                        delete cdef.options.label;
                    }
                    cdef.$parent = cmc.$placeholder;
                    cdef.key = key;
                }

                /**
                 * Default values
                 */

                if (op.tableName==='mortbiom' && ['animal_status','animal_status_on_handling'].indexOf(key)!==-1) {
                    cms[table].cmParent.components[key].val("fixed");
                    cms[table].cm.components[key].val("dead");
                }
                
            }; //models loop
            
        });

        importBtn.$el().show();
        saveDefinitionBtn.$el().show();

        importType.val(importType.val(),false);

    } //function mappings

    async function onSaveColumnMap() {
        const data = getImportData();
        const callbacks = await mutils.assignRequestCallbackToasters({});
        const note = prompt("Here you can (optionally) enter a note - it will appear in the table of saved templates beside this entry");
        const res = await mutils.requestHelper(globals.apiRoot + `/mbase2/import_templates`, 'POST', {
            _uid: 1,
            data: data,
            note: note,
            key_module_id: op.tableName
        },callbacks);
    }

    function getImportData() {
        const data = {};
        Object.keys(cms).map(table => {
            const types = cms[table].cmParent.val();
            const values = cms[table].cm.val();

            const importValues = {};
            const importTypes = {};
            const importHeaders = {};

            Object.keys(values).map(key => {
                const value = values[key];
                if (value || value === 0) {
                    importValues[key] = value;
                    importTypes[key] = types[key];
                    const cmp = cms[table].cm.get(key);
                    if (cmp.text) {
                        importHeaders[key] = cmp.text();    
                    }
                }
                else if (model[table]) {
                    const a = model[table].find(a=>a.key_name_id === key);
                    if (a && a.__hide) {
                        importValues[key] = 0;
                        importTypes[key] = 'fixed';
                    }
                }
            });

            data[table] = {
                types: importTypes,
                headers: importHeaders,
                values: importValues,
                import_type: importType.val(),
                update_key: updateKeyComponent.val(),
                dateFormat: dateFormat.val(),
                multivalue_delimiter: delimiter.val()
            }

            if (op.fkeys && op.fkeys[table]) {
                data[table].fkeys = op.fkeys[table];
            }

        });

        return data;
    }

    async function onImport() {
        const data = getImportData();
        
        const dsid = fileUploadCm.get('select').val();
        const page = pageSelector.val();
        const callbacks = await mutils.assignRequestCallbackToasters({});
        const res = await mutils.requestHelper(globals.apiRoot + `/import/${dsid}/${page}/${tableName}/language/${globals.language}`, 'POST', data, callbacks);

        if (res !== false) {
           showImportStatus(res);
        }
    }

    function getCountMsg(res) {
        let countMsg = '';
                        
        if (res.importType === 'insert') {
            countMsg = th.t`Number of inserted rows:` + ' ' + `${res.numberOfInsertedRows}`
        }
        else if (res.importType === 'update') {
            countMsg = th.t`Number of updated rows:` + ' ' + `${res.numberOfUpdatedRows}`;
        }

        countMsg+= '<br>' + th.t`Number of all rows in the data input:` + ' ' + `${res.numberOfInputRows}`;

        return countMsg;
    }

    function showImportStatus(res) {
        const modal = new ModalDialog.default(
            {
                onClose: () => {
                    //if (res.length === 0) {
                        window.location.reload();
                    //}
                },
                onShown: () => {
                    
                    if (res.errors.length === 0) {
                        const countMsg = getCountMsg(res);

                        modal.$body.html(mutils.alertCode(th.t`The import has finished without errors.` + '<br>' + countMsg,'success'));
                    }
                    else {

                        let criticalErrors = 0;

                        const missingRequiredAreCritical = res.importType === 'insert' ? true : false;
                        
                        const values = [];
                        res.errors.map(rowData => {
                            
                            let rowDataObject = {};

                            if (Array.isArray(rowData)) {
                                rowDataObject = utils.jsonParse(rowData[0]);
                            }
                            else {
                                rowDataObject = utils.jsonParse(rowData);
                            }

                            if (rowDataObject.critical || (rowDataObject.required && missingRequiredAreCritical)) criticalErrors++;
                            
                            values.push({
                                data: rowData
                            });
                        });

                        const countMsg = getCountMsg(res);

                        let msg = '';
                        if (criticalErrors === 0) {
                            msg = mutils.alertCode(th.t`The import has finished with some non critical errors.` + '<br>' + countMsg, th.t`All the rows were imported.`, 'warning');
                        }
                        else {
                            msg = mutils.alertCode(th.t`The import has finished with some critical errors.` + '<br>' + countMsg, th.t`Some or all rows were NOT imported or updated.`, 'danger');
                        }
                        
                        modal.$body.html(msg);
                        
                        import('./importErrors').then(async importErrors => {
                            await importErrors.default({$parent: modal.$body, batchId: null, data: {
                                values: values
                            }});
                        })
                    }
                }
            }
        );

        modal.show();
    }

    function onDataEntryOptionClicked(data, btnKey) {
        data.cmParent.get(data.key).$placeholder.empty();
        data.cm.add(dataMapComponentDefinitions[data.table][data.key][btnKey], true);
    }
    
    
}