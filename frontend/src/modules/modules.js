/**
 * mbase2 settings: modules
 */
import globals from '../app-globals';
import * as exports from '../libs/exports';

export default async op => {

    const {$parent, module} = op;

    const [
        ComponentManager,
        mutils,
        utils
    ] = await Promise.all([
        exports.ComponentManager(),
        exports.mutils(),
        exports.utils()
    ]);

    const t = utils.t;

    const components = {
        modules: exports.select({
            label: t`Modules`,
            onSelect: onSelectModuleName,
            tname: 'modules',
            $parent: $parent
        }),
        acc: exports.accordion({
            panels: [
                t`Module properties`,
                t`Variables`
            ],
            $parent: $parent
        })
    };

    const cm = new ComponentManager.default();

    await utils.loadComponents(components);
    const modules = await cm.add(components.modules);

    cm.get('modules').$el().hide();

    const references = {
        table_reference:[],
        code_list_reference:[]
    }
    ///////////////////////////////////////////////////////////////////////////////////////////
    /** module variables */
    const definitions = (await import('./attributeDefinitions.js')).default(exports, globals);
    const moduleVariablesAttributes = await definitions.module_variables({});
    
    const codes = await mutils.getRefCodeListValues(moduleVariablesAttributes,['module_variables']);

    //add translation - only needed beacause these variable properties are not fetched from table
    mutils.translateKey('key_name_id', 't_name_id', moduleVariablesAttributes, 
        codes.codeLists.find(cl => cl.key === 'module_variables').id, codes.codeListValues);

    const codeListValues = utils.arrayToObject(codes.codeListValues, 'id');
    const tableReferences = utils.arrayToObject(codes.tableReferences, 'id');

    codes.codeListValues.map(clv => {
        if (clv.list_key === 'referenced_tables') {
            references.table_reference.push([clv.id, clv.key]);
        }
        else if (clv.list_key === 'code_lists') {
            references.code_list_reference.push([clv.id, clv.key]);
        }
    })

    /////////////////////////////////////////////////////////////

    const moduleVariablesCm = new ComponentManager.default();

    if (module) {
        const data = cm.getData('modules');
        const m = data.find(d => d.key_id == module);
        if (m) {
            modules.val(m.id);
        }
    }

    async function onSelectModuleName(e) {
        const moduleId = utils.select2value(e)
        if (!moduleId) return;
        
        await cm.add(components.acc);

        const selectedModuleData = cm.getData('modules').find(d => d.id == moduleId);

        ///////////////////////////////////////////////////////////////////////
         /**selected module properties */
        
        cm.model.attributes = [
            {
                key_data_type_id: 'json',
                key_name_id: 'properties',
                _op: {
                    attributes: [
                        {
                            label: t`Grid size`, 
                            key: 'grid_size', 
                            required: true, 
                            pattern: utils.patterns.non_zero_integer,
                            help: t`Grid size in meters`
                        },
                        {
                            label: t`Color`, 
                            key: 'color', 
                            required: true, 
                            type: 'color',
                            help: t`Select a color of the grid cell`
                        }
                    ]
                }
            }
        ];
        cm.model.values = {
            id: modules.val(),
            properties: utils.jsonParse(selectedModuleData.properties)
        }
        cm.model.tableName = cm.model.dataComponentKey = 'modules';

        const record = await exports.record();
        record.default({
            $parent: cm.get('acc').panels[0].$body,
            cm: cm,
            saveOptions: {
                upsertDataFunction: null
            },
            replaceComponents: false
        });

        const recordsTable = await import('./recordsTable');
        
        const additionalFilter = '';

        const batchImportModuleName = mutils.batchImportModuleName(module);

        console.log('module', module)

        const tableRecordsOptions = await mutils.moduleVariablesTableRecordsOptions(cm.get('acc').panels[1].$body, batchImportModuleName, moduleVariablesAttributes, codeListValues, tableReferences, additionalFilter);

        tableRecordsOptions.url = globals.apiRoot + `/module_variables_vw/language/${globals.language}?:module_name=${batchImportModuleName}`;
        
        tableRecordsOptions.saveOptions = {
            beforeRequest: (req, saveOptions) => {
                const attributes = req.attributes;
                ['key_name_id','t_name_id'].map(key=>{
                    if (attributes[':'+key]) {
                        delete attributes[':'+key];
                    }
                });
            }
        }

        tableRecordsOptions.tableOptions = {
            scrollY: 'calc(100vh - 500px)'
        };

        tableRecordsOptions.cm = moduleVariablesCm;
        tableRecordsOptions.btn_batch_import = false;
        tableRecordsOptions.btn_add_record = false;
        tableRecordsOptions.preprocessTableData = (data, model) => {
            const filteredData = [];
            data.map (v=>{
                if (['location_geometry','location_data_json'].indexOf(v.key_data_type_id) ===-1) {
                    filteredData.push(v);
                }
            });
            
            return filteredData;
        }
        recordsTable.default(tableRecordsOptions);
    }
}