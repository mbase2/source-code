import * as exports from '../../libs/exports';
import globals from '../../app-globals';
import toggleTrackDetail from '../../helpers/toggleTrackDetail';

export default async op => {
    const [
        utils,
        mutils,
        Mbase2ModuleIndex,
        ImageGallery
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        import('../Mbase2ModuleIndex'),
        exports.ImageGallery()
    ]);

    const t = utils.t;

    let moduleIndex;

    const moduleIndexOptions = Object.assign({
        moduleProps: (await mutils.requestHelper(globals.apiRoot + `/modules/language/${globals.language}`)).find(m=>m.key_id==='tlm'),
        mapHeight: '63',
        skipNewButton: true,
        batchOptionsModel: async (attributes) => ({
            tlm_tracks: attributes,
            tlm: await mutils.getModuleVariables('tlm', false)
        }),
        batchOptionsFkeys: {
            'tlm_tracks':['slug','animal_id']
        },
        additionalBatchImportOptions: [
            {
                key: 'tlm_animals',
                label: t`tlm_animals`,
                callback: () => {
                    utils.hrefClick('tlm_animals');
                }
            },
            {
                key: 'api',
                label: t`Import from API`,
                callback: () => {
                    utils.hrefClick('tlm-api');
                }
            },
        ],
        additionalTableIcons: [{
            action: 'detail-points',
            class: 'map-marker',
            style: 'margin-left:5px',
            onClick: async (data,id, $a) => {
                await toggleTrackDetail(data, moduleIndex.mapComponent.map, $a.find('span'));
            }
        }],
        recordTimeStamps: false,
        moduleKey:'tlm_tracks',
        mainModuleKey: 'tlm',
        title: t`Telemetry`,
        viewName:'tlm_tracks_vw',
        defaultRecordEditor:true,
        urlForDelete: globals.apiRoot + `/mb2data/tlm_tracks`,
        onRowDeleted: data => {
            
            data.__pnts && moduleIndex.mapComponent.map.removeLayer(data.__pnts);
            data.__marker && data.__marker.remove();
        },
        onSuccessfullySaved: (cm, result, model)=> {
            window.location.reload();
        },
        saveOptions: {
            rootUrl: globals.apiRoot + '/mb2data'
        }
    }, op);

    moduleIndex = new Mbase2ModuleIndex.default(moduleIndexOptions);
    await moduleIndex.init();
}