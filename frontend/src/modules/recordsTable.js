/**
 * Helper function for DataTable component
 */

 import * as exports from '../libs/exports';
 import globals from '../app-globals';
 import {t} from '../libs/utils';
 import './styles.css';
 
 /**
  * @param {object} op options
  * @param {object} op.$parent
  * @param {array} op.skipAttributesFromTable
  * @param {boolean} op.sortVariables
  * @param {object} op.tableOptions //table component options
  * @param {array} op.variablesAttributes
  * @param {array} op.refValues
  * @param {boolean} op.disableEdit //default false
  * @param {function} op.onEditRecord   //opens ModalDialog with row data for editing
  * @param {function} op.externalEditRecord //only pass row data to externalEditRecord(row, rowId, rowIndex) function
  * @param {boolean} op.skipId //skips id column from table, default false
  * @param {function} op.preprocessTableData
  * @param {function} op.onTableCreated //triggered when table if created, passes table component reference as parameter
  * @param {boolean} op.selectable //rows can be selected
  * @param {function} op.onRowSelected onRowSelected(data) where data is table row data
  * 
  * op.variables
  */
 export default async op => {
     const [
         mutils,
         utils,
         Button,
         DataTable,
         ModalDialog,
         ButtonGroup,
         ComponentManager
     ] = await Promise.all([
         exports.mutils(),
         exports.utils(),
         exports.Button(),
         exports.DataTable(),
         exports.ModalDialog(),
         exports.ButtonGroup(),
         exports.ComponentManager()
     ]);
 
     const {$parent} = op;
     const tableCm = op.cm || new ComponentManager.default();
 
     const {refValues} = op;

     op.tableOptions = op.tableOptions || {};
 
     const refValuesKeyed = await mutils.convertReferencesToAssocArray(refValues);
 
     if (op.select && op.select.length > 0) {
         op.url = op.url + (op.url.includes('?') ? '&' : '?') + op.select.map(s => `:${s.key}=${s.value}`).join('&');
         tableCm.model.select = op.select;
     }
 
     tableCm.model.attributes = Object.assign([], op.variablesAttributes.filter(v=>v.visible_in_table));
 
     //sort and filter out attributes that are not bound to database table
     const recordSetAttributes = op.variablesAttributes.filter(a => a.dbcolumn === true || a.dbcolumn === undefined);
 
     if (op.tableName && op.batchOptions && op.batchOptions.model && !op.batchOptions.model[op.tableName]) { //if batchOptions.model for the table is not defined it gets the value of recordsSetAttributes
         op.batchOptions.model[op.tableName] = [...recordSetAttributes];
     }
 
     const sortVariables = op.sortVariables === undefined ? true : op.sortVariables;
 
     if (sortVariables) {
         tableCm.model.attributes = mutils.sortVariables(tableCm.model.attributes, op.splitRequiredAndOptionalAttributesWhenSorting);
     }
     
     tableCm.model.tableName = op.tableName;
     
     const attributesObj = utils.arrayToObject(tableCm.model.attributes, 'key_name_id');
 
     const $buttonsContainer = $('<div class="flex-container"></div>');
 
     const components = {
         table: (op = {header:[], model:[]}) => ({
             component: DataTable.default,
             options: {
                 exportButtons: false,
                 scroller: true,
                 scrollX: true,
                 deferRender:true,
                 scrollY: op.scrollY || '75vh',
                 header: op.header,
                 onEdit: (rowData, rowId, rowIndex) => {
                     editRecord(rowData, rowId, rowIndex);
                 },
                 onDelete: (rowData, rowId) => {
                     deleteRow(rowData, rowId);
                 }
             },
             data: {
                 model: op.model,
                 process: processTableData,
                 refresh: (component, data) => {
                     component.setData(data);
                 }
             },
             beforeComponentCreate: options => {
                 const $div = $('<div/>');
                 $parent.append($div);
                 options.$container = $div;
             }
         }),
         btn_add_record:{
             component: Button.default,
             options: {
                 label: op.t && op.t.btn_add_record || t`Add a record`,
                 style: op.btn_add_record && op.btn_add_record.style || 'width:100%',
                 type: op.btn_add_record && op.btn_add_record.type || 'btn-primary',
                 classes: op.btn_add_record && op.btn_add_record.classes,
                 onClick: () => editRecord()
             },
             $parent: op.btn_add_record || $buttonsContainer
         },
         btn_batch_import:{
             component: Button.default,
             options: {
                 label: t`Batch import`,
                 style: 'width:100%',
                 type: 'btn-primary',
                 onClick: () => editRecords()
             },
             $parent: $buttonsContainer
         },
 
     };
 
     $parent.append($buttonsContainer);
 
     utils.addKeysToComponents(components);
 
     op.btn_add_record !== false && await tableCm.add(components.btn_add_record);
     op.btn_batch_import !== false && await tableCm.add(components.btn_batch_import);
 
     const btnAdd = tableCm.get('btn_add_record');
     const btnBatch = tableCm.get('btn_batch_import');
 
     const $btnAdd = btnAdd && btnAdd.$el();
     const $btnBatch = btnBatch && btnBatch.$el();
 
     op.btn_add_record === undefined && $btnAdd && $btnAdd.addClass('flex-child width15');
     $btnBatch && $btnBatch.addClass('flex-child width15');
     
     if (op.changeRecordSet) {
         op.changeRecordSet(onRecordSetChanged);
     }
     else {
         onRecordSetChanged();
     }
 
     //$btnAdd.find('button').click()
     //$btnBatch.find('button').click();
 
     function _op() {return op};   //to ensure we access most recent value for example in the callbacks that are attached in the initialisation
 
     async function deleteRow(data, row) {
         if (confirm(t`Do you really want to delete selected row?`)) {
             const callbacks = await mutils.assignRequestCallbackToasters({});
             const res = await mutils.requestHelper((op.urlForDelete || op.url) + '/' + data.id, 'DELETE', null, callbacks);
             if (res!==false) {
                 row
                 .remove()
                 .draw();
                 op.onRowDeleted && op.onRowDeleted(data);
             }
         }
     }
 
     async function editRecord(row, rowId, rowIndex) {
         const op = _op();
         if (op.externalEditRecord) {
             op.externalEditRecord(row, rowId, rowIndex);
             return;
         }
         const cm = new ComponentManager.default();
         cm.model = Object.assign({}, op.editRecordModel || tableCm.model);
         cm.model.attributes = (op.editRecordModel && op.editRecordModel.attributes) || recordSetAttributes;
         
         if (row!==undefined) {
             const data = tableCm.getData('table').find(r => r.id == row.id);
             if (!data) return;
             
             const attributes = utils.convertToAssocArray(recordSetAttributes, 'key_name_id');
 
             const values = {};
 
             Object.keys(data).map(key => {
                 const a = attributes[key];
                 let value = data[key];
                 if (a && value && (['json','jsonb','table_reference_array'].indexOf(a.key_data_type_id) !== -1)) {
                     value = utils.jsonParse(value);
                 }
                 values[key] = value;
             });
             cm.model.values = values;
         }
 
         let _continue = true;
         if (op.onEditRecord) {
             _continue = op.onEditRecord(cm, row); //pass records components to parent
         }
 
         if (_continue === false) return false;
 
         const record = await exports.record();
         const modal = new ModalDialog.default({
             onShown: () => {
 
                 const saveOptions = op.saveOptions || {};
 
                 //add callback to other possibly defined callbacks
                 if (!saveOptions.onSuccessCallbacks) {
                     saveOptions.onSuccessCallbacks = [];
                 }
                 saveOptions.onSuccessCallbacks.push(onRecordUpserted);  
 
                 record.default({
                     $parent: modal,
                     cm: cm,
                     saveOptions: saveOptions,
                     refValues: op.refValues,
                     onInit: function (cm) {  //here you get initialized component manager
                         const args = Array.prototype.slice.call(arguments);
                         args.push(modal);
                         op.onInitRecord && op.onInitRecord.apply(null, args);
                     },
                     onSuccessfullySaved: (cm, result, model) => {
                         op.onSuccessfullySaved && op.onSuccessfullySaved(cm, result, model);
                         modal.hide();
                     }
                 });
             }
         });
         modal.show();
     }
 
     async function editRecords() {
        const op = _op();

        const batchModel = op.batchOptions && op.batchOptions.model;

        if (!batchModel) return;

        mutils.batchImport({
        batch: await import('./batchImport'),
        ModalDialog: ModalDialog
        }, op.tableName, batchModel, op.refValues, t`Data import`, 'xlsx'); 
     }
 
     async function onRecordSetChanged(dataValues = null) {
         const op = _op();
         tableCm.destroy('table');
 
         const skipAttributesFromTable = op.skipAttributesFromTable || [];
 
         const idcol = op.skipId ? [] : [{key: 'id', label: 'id'}];
 
         const header = [...idcol, ...(op.tableAttributesOverride || tableCm.model.attributes).filter(a => skipAttributesFromTable.findIndex(key => key === a.key_name_id)===-1).map(d => {
 
             if (op.tableAttributesOverride && !d.t_name_id) {
                 const attributeDefinition = attributesObj[d.key_name_id];
                 d.t_name_id = attributeDefinition && attributeDefinition.t_name_id;
             }
 
             //translate the folowing data types (DataTables component fills rows according to the header key value):
             const key = ['boolean', 'code_list_reference','table_reference','reference','location_reference','table_reference_array', 'code_list_reference_array'].indexOf(d.key_data_type_id) === -1 ? d.key_name_id : 't_' + d.key_name_id;

             if (op.tableOptions && op.tableOptions.titleAttr) {
                op.tableOptions.titleAttr[d.key_name_id] =  op.tableOptions.titleAttr[d.key_name_id] && mutils.parseHelpIconText(d, globals.language);
             }
             
             return {
                 key: key,
                 label: d.t_name_id || mutils.parseTranslation(d) || d.key_name_id
             }}
         )];
 
         const tableDataModel = {
             keys: header.map(h => h.key),
             url: op.url
         }
 
         const tableDefinition = components.table({header, model:tableDataModel});   //defaultOptions
 
         if (op.data && op.data.values) {
             tableDefinition.data.values = op.data.values;
         }
         else if (dataValues) {
             tableDefinition.data.values = dataValues;
         }
         else {
             tableDefinition.data.request = model => mutils.requestHelper(model.url);
         }
         
         if (op.disableEdit) {
             delete tableDefinition.options['onEdit'];
         }
 
         if (op.preprocessTableData) {
             tableDefinition.data.preprocess = op.preprocessTableData;
         }
 
         tableDefinition.options = Object.assign(tableDefinition.options, op.tableOptions || {});
 
         if (globals.language === 'sl') {
             tableDefinition.options.language = await import('../libs/components/DataTable/i18n/sl.json');
         }
 
         tableDefinition.options.deletable = op.deletable;
         tableDefinition.options.rowEditOptions = op.rowEditOptions;
 
         if (op.selectable || op.onRowSelected) {
             tableDefinition.options.select = true;
             if (op.onRowSelected) {
                 tableDefinition.options.onRowSelected = op.onRowSelected;
             }
         }
 
 
         /*
         const SearchInput = await import('../../components/SearchInput');
         tableDefinition.options.customSearchInput = {
             Constructor: SearchInput.default,
             options: Object.assign({
                 placeholder: t`Search`
             }, op.searchInputOptions || {})
         };
         */
 
         if (op.cellEdit) {
             tableDefinition.options.keys=op.cellEdit.keys || true;
         }
 
         const table = await tableCm.add(tableDefinition);
 
         /**
          * this was a "hack" and remains here for backward compatibility - the way to get access to these functions is by using returned object from this helper
          */
         table.__editRecord = editRecord;    
         table.__editRecords = editRecords;
 
         if (tableDefinition.options.keys) {
             const tableKeys = await import('./tableKeys');
             tableKeys.default(table, table.getHeaderKeys(), op.cellEdit);
         }
 
         if (op.tableOptions && op.tableOptions.select) {
 
             const actionsBtn = new ButtonGroup.default({
                 buttons: [
                     {
                         key: 'select_all',
                         label: t`Select all`
                     },
                     {
                         key: 'select_none',
                         label: t`Select none`
                     }/*,
                     {
                         key: 'edit',
                         label: '<a href="#" class="table-action"><span class="fa fa-edit"></span></a>'
                     }*/
                 ],
                 classes: 'btn btn-default',
                 onClick: (data, key) => {
                     if (key==='select_all') {
                         table.table.rows({ search: 'applied' }).select();
                     }
                     else if (key==='select_none') {
                         table.table.rows().deselect();
                     }
                     else if (key==='edit') {
 
                     }
                 },
                 highlight: false
             });
 
             
             const $wrapper = table.$el().parents('.dataTables_wrapper:first');
             const $div = $('<div/>',{style: "display: inline-flex"});
             $div.append(actionsBtn.$el());
             $wrapper.prepend($div);
         }

         if (op.tableOptions && op.tableOptions.$toolbarElement) {
            const $wrapper = table.$el().parents('.dataTables_wrapper:first');
            const $div = $('<div/>',{style: "display: inline-flex"});
            $div.append(op.tableOptions.$toolbarElement);
            $wrapper.prepend($div);
         }
 
         
         if (op.onTableCreated) {
             op.onTableCreated(tableCm.get('table'), tableCm.getData('table'));
         }
 
         if (op.onRecordSetChanged) {
             op.onRecordSetChanged(tableCm.getData('table'));
         }        
     }
 
     function processTableData(data, model) {
        const op = _op();
        const processedData = data.map((row, inx) => {
            mutils.processAttributeValues(row, attributesObj, refValuesKeyed);

            //add keys if they are not present in the structure
            model.keys.map(key => {
                if (row[key] === undefined) row[key] = null;
            })
            
            if (op.processTableData) {
                row = op.processTableData(row, inx);
            }

            row.__rowInx = inx;
            return row;
        });

        op.onTableDataProcessed && op.onTableDataProcessed(processedData);
        
        return processedData;
     }
 
     function onRecordUpserted(_cm, model) {
         const modelArray = Array.isArray(model) ? model : [model];
         modelArray.map(m => utils.upsertArray(tableCm.getData('table'), m));
         tableCm.refresh('table');
         op.onRecordSetUpserted && op.onRecordSetUpserted(model, tableCm.get('table'));
     }
 
     return Object.freeze({
         editRecord: editRecord,
         editRecords: editRecords
     });
 } 
 