/**
 * mbase2 settings: modules
 * op.applyExisting: loads reduced table with only specific module uploads
 */

import * as exports from '../libs/exports';
import globals from '../app-globals';
import { hrefClick, jsonParse } from '../libs/utils';

export default async op => {

    const {$parent, $header, moduleKey} = op;

    const [
        mutils,
        ModalDialog,
        Button,
        DropDown
    ] = await Promise.all([
        exports.mutils(),
        exports.ModalDialog(),
        exports.Button(),
        exports.DropDown()
    ]);

    const t = globals.t;

    const moduleNameTranslations = await mutils.getTranslationsForList('modules');

    if ($header) {

        const $headerButtonGroup = $('<div/>',{style: 'display: flex'});
        
        $header.html(mutils.moduleHeader(globals.t`Batch imports`,null,$headerButtonGroup));

        const importBtn = new Button.default({
            label:t`Batch import`,
            iconClass: 'upload',
            type: 'btn-primary',
            classes: 'btn-lg',
            onClick: async () => {
                let model = null;

                const batchImportModuleName = mutils.batchImportModuleName(moduleKey);
                
                const res = await mutils.requestHelper(globals.apiRoot + `/mbase2/module_variables_vw?:__filter=[["module_name","=","${batchImportModuleName}"]]`); 
                model = {};
                res.map(row => {
                    if (!model[row.module_name]) {
                        model[row.module_name] = [];
                    }
                    model[row.module_name].push(row);
                });
                
                mutils.batchImport({
                    batch: await import('./batchImport'),
                    ModalDialog: ModalDialog
                }, moduleKey, model, null, moduleNameTranslations[moduleKey]);
            }
        });

        $headerButtonGroup.append(importBtn.$el());

        const allModulesKeys = Object.keys(moduleNameTranslations).filter(key => ['__default_module', 'mbase2'].indexOf(key)===-1);

        const modulesWithEditorOrAdminRole = [];
        globals.user.roles.map(role => {
            allModulesKeys.map(key => {
                if ([`mbase2-${key}-admins`,`mbase2-${key}-editors`, `mbase2-${key}-curators`].indexOf(role)!==-1) {
                    modulesWithEditorOrAdminRole.push(key);
                }
            });
        });

        if (['howling', 'mortbiom'].indexOf(moduleKey) !== -1) {
            const items = [
                {
                    key:'mbase2/admin/modules/' + moduleKey,
                    label: t`Module settings`
                }
            ];

            if (moduleKey === 'howling') {
                items.push({
                    key: 'mbase2/admin/code-lists?m=' + moduleKey,
                    label: t`Code lists`
                });
            }

            const adminButtonDropDown = new DropDown.default({
                classes: 'btn-lg',
                iconClass: 'cog',
                items: items,
                label: t`Admin settings`,
                onClick: ({key}) => {
                        window.open(window.location.origin + '/' + key, '_blank');
                        return false;
                    }     
            });

            $headerButtonGroup.prepend(adminButtonDropDown.$el());
        }
    }
        
///////////////////////////////////////////////////////////////////////////////////////////
        /**
         * double underscore before the key_name_id to prevent mixing with column names obtained from table (if variableAttribute key_name_id is not present in table it gets added to data array)
         */
    
    let variablesAttributes = op.fields || [
        {
            key_data_type_id: 'text',
            key_name_id: '__username',
            t_name_id: t`username`,
            visible_in_table: true
        },
        {
            key_data_type_id: 'date',
            key_name_id: '__date',
            t_name_id: t`date`,
            visible_in_table: true
        },
        {
            key_data_type_id: 'text',
            key_name_id: '__source',
            t_name_id: t`source`,
            visible_in_table: true
        },
        {
            key_data_type_id: 'text',
            key_name_id: 'note',
            t_name_id: t`note`,
            visible_in_table: true
        },
        {
            key_data_type_id: 'text',
            key_name_id: '__status',
            t_name_id: `# imported/ #updated / # all rows`,
            visible_in_table: true
        },
        
    ];

    if (op.applyExisting) {
        variablesAttributes = variablesAttributes.filter(a => ['__status'].indexOf(a.key_name_id)===-1);
    }
    
    const tableRecordsOptions = {
        $parent: $parent,
        tableName: 'import_batches_vw',
        url: op.url || globals.apiRoot + `/import_batches_vw?:__filter=[["key_module_id","like","%${op.moduleKey}%"]]`,
        urlForDelete: globals.apiRoot + '/drop-batch-data',
        select: [],
        skipId: op.skipId,
        skipAttributesFromTable: ['data'],
        variablesAttributes: variablesAttributes,
        disableEdit: true,
        btn_add_record: false,
        btn_batch_import: false,
        deletable: op.applyExisting ? false : true,
        selectable: op.applyExisting ? true : false,
        onRowSelected: op.applyExisting ? op.applyExisting.onRowSelected : () => {},
        preprocessTableData: (data, model) => {

            const filteredData = [];    //only used when applyExisting is set

            for (const d of data) {
                const idata = JSON.parse(d.data) || {};

                const dataDefinitions = idata.dataDefinitions;
                if (dataDefinitions) {
                    const module = d.key_module_id;
                    if (module) {

                        if (op.applyExisting && op.applyExisting.module!==module) continue;

                        d.__module = moduleNameTranslations[module] || module;
                    }
                }

                if (idata.camelotId) {
                    d.__source = 'camelot: '+idata.camelotId;
                }
                else {
                    d.__source = idata.fileName + ', ' + idata.sheetInx;
                }

                const updatedRows = idata.updatedRows || 0;

                d.__status = idata.insertedRows + "/" + updatedRows + "/" + idata.crows;

                let style = '';

                if ((idata.insertedRows + updatedRows) !== idata.crows) {
                    style="color:red";
                }

                d.__status = `<a href="#" id="${d.id}" class="import-error" style="${style}">${d.__status}</a>`;

                d.__date = d.date_record_created;
                d.__username = d.user_name;

                filteredData.push(d);
            }
            
            return op.applyExisting ? filteredData : data;
        },
        onTableCreated: table=>{
            table.$el().on('click','.import-error', async function() {
                const batchId = $(this).attr('id');
                const importErrors = await import('./importErrors');
                const dlg = new ModalDialog.default({
                    onShown: () => {
                        importErrors.default({$parent: dlg.$body, batchId: batchId});
                    }
                });
                
                dlg.show();
            })
        }

    }

    if (op.scrollY) {
        tableRecordsOptions.tableOptions = {
            scrollY: op.scrollY,
            defaultClass: "display cell-border selectable-rows"
        }
    }

    const recordsTable = await import('./recordsTable');

    if (op.showDownloads) {
        tableRecordsOptions.tableOptions = Object.assign(tableRecordsOptions.tableOptions || {}, 
        {
            __icons: [{
                action: 'download',
                class: 'download',
                onClick: row => {
                    if (!row.data) return;
                    const data = jsonParse(row.data);
                    const dsid = data.dsid;
                    if (!dsid) return;
                    hrefClick(globals.apiRoot + '/uploaded-file/private/' + dsid);
                }
            }]
        });
    }

    if (op.additionalIcons) {
        const icons = tableRecordsOptions.tableOptions.__icons || [];
        icons.push(...op.additionalIcons);
    }

    recordsTable.default(tableRecordsOptions);
}
