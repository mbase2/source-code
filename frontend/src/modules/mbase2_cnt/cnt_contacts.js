import globals from '../../app-globals';

const t = globals.t;

export default async ($content) => {
    const module = await import('../dataEditor');
    /*const mutils = await exports.mutils();
    const utils = await exports.utils();*/

    let recordCm = null;
    
    await module.default({
        $parent: $content,
        title: t`Kontakti za obveščanje`,
        key_id: 'cnt_contacts',
        componentInitialization: {
            '_hunting_ground': cmp => cmp.$el().hide(),
            '_luo': cmp => cmp.$el().hide()
        },
        tableRecordsOptions: {
            onInitRecord: (cm) => {
                recordCm = cm;
                onContactTypeSelected();
            }
        },
        additionaComponentOptions: {
            '_cnt_vrsta_kontakta': {
                onSelect: onContactTypeSelected
            }
        }
    });

    function onContactTypeSelected() {
        const cm = recordCm;
        if (!cm) return;
        const lovCmp = cm.get('_hunting_ground');
        const luoCmp = cm.get('_luo');
        if (!lovCmp || !luoCmp) return;

        const key = cm.get('_cnt_vrsta_kontakta').getAdditionalDataForSelectedOption();
        
        if (key) {
            lovCmp.$el().hide();
            luoCmp.$el().hide();

            if (key === 'LUO (OZUL)') {
                luoCmp.$el().show();
            }
            else if (key === 'Lovišče (LD)') {
                lovCmp.$el().show();
            }
        }
    }
}