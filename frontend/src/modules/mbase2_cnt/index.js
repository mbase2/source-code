import * as exports from '../../libs/exports';
import globals from '../../app-globals';

export default async op => {
    const [
        utils,
        mutils,
        Mbase2ModuleIndex,
        DropDown
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        import('../Mbase2ModuleIndex'),
        exports.DropDown()
    ]);

    const t = utils.t;

    let moduleIndex;

    let recordCm = null;

    let tableRecordsInstance = null;

    const moduleIndexOptions = Object.assign({
        moduleProps: (await mutils.requestHelper(globals.apiRoot + `/modules/language/${globals.language}`)).find(m=>m.key_id==='tlm'),
        withoutMap: true,
        deletable: false,
        saveOptions: {
            rootUrl: globals.apiRoot + '/mb2data'
        },
        batchOptionsModel: async (attributes) => {
            console.log('attributes', attributes)
            return {
                cnt_monitoring: attributes,
                cnt_observation_reports: await mutils.getModuleVariables('cnt_observation_reports', false)
        }},
        batchOptionsFkeys: {
            'cnt_monitoring':['_start_date']
        },
        skipExportButton: true,
        skipNewButton: true,
        skipFilterButton: true,
        adminButton: {
            key: 'admin',
            classes: 'btn-lg',
            component: DropDown,
            iconClass: 'cog',
            items: [
                {
                    key: 'mbase2/admin/modules/cnt',
                    label: t`Module settings`
                },
                {
                    key: 'mbase2/admin/code-lists?m=cnt',
                    label: t`Code lists`
                },
                {
                    key: 'mbase2/modules/cnt/admin/cnt_permanent_spots',
                    label: t`Stalna števna mesta`
                }
            ],
            label: globals.t`Admin settings`,
            onClick: ({key}) => {
                    //window.location=window.location.origin + '/' + key;
                    window.open(window.location.origin + '/' + key, '_blank');
                    return false;
                }     
        },
        additionalButtons:[
            {
                label: t`Dodaj novo serijo opazovanj`,
                iconClass: 'plus',
                type: 'btn-primary',
                classes: 'btn-lg',
                onClick: () => {
                    tableRecordsInstance.editRecord();
                }
            }  
        ],
        additionalTableIcons: [{
            action: 'list',
            class: 'list',
            style: 'margin-left:5px',
            onClick: (data, id, $a) => {
                const pathname = utils.rtrimSlash(window.location.pathname);
                utils.hrefClick(pathname+'?cnt_monitoring_id='+data.id);
            }
            },
            {
                action: 'download',
                style: 'margin-left:5px',
                class: 'download',
                onClick: (data, id, $a) => {
                    
                }
            }
        ],
        recordTimeStamps: false,
        moduleKey:'cnt_monitoring',
        mainModuleKey: 'cnt',
        title: t`Štetja`,
        viewName: 'cnt_monitoring',
        defaultRecordEditor:true,
        onInitRecord: (cm) => {
            recordCm = cm;
            onVrstaOpazovanjaSelected();
        },
        additionalComponentOptions: {
            _vrsta_opazovanja: {
                onSelect: () => {
                    onVrstaOpazovanjaSelected();
                }
            },
            _start_date: {
                onChange: () => {
                    if (!recordCm) return;

                    const endDateCmp = recordCm.get('_end_date');
                    const selectedDate = recordCm.get('_start_date').val();

                    if (moment(selectedDate).isAfter(endDateCmp.val())) {
                        endDateCmp.val(selectedDate);
                    }

                    endDateCmp.dp.setStartDate(selectedDate);

                    isPermanentCountSpot() && endDateCmp.val(selectedDate);
                    
                }
            }
        }
    }, op);

    moduleIndex = new Mbase2ModuleIndex.default(moduleIndexOptions);
    tableRecordsInstance = await moduleIndex.init();

    function onVrstaOpazovanjaSelected() {
        if (!recordCm) return;
        const key = recordCm.get('_vrsta_opazovanja').getAdditionalDataForSelectedOption();
        if (!key) return;

        const $endDate = recordCm.get('_end_date').$el();
        const hideEndDate = isPermanentCountSpot() ? $endDate.hide() : $endDate.show();
    }

    function isPermanentCountSpot() {
        const key = recordCm.get('_vrsta_opazovanja').getAdditionalDataForSelectedOption();
        if (!key) return;

        return key === 'stalna števna mesta';
    }
}