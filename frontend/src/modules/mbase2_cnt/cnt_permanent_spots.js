import globals from '../../app-globals';
import * as exports from '../../libs/exports';

const t = globals.t;

export default async ($content) => {
    const module = await import('../dataEditor');
    const mutils = await exports.mutils();
    const utils = await exports.utils();

    const tableAttributesOverride = [
        mutils.createVariableDefinition('lov_ime', null, 'text', null),
        mutils.createVariableDefinition('luo_ime', null, 'text', null),
        mutils.createVariableDefinition('_local_name', null, 'text', null),
        mutils.createVariableDefinition('_cnt_permanent_spot_code', null, 'text', null),
        mutils.createVariableDefinition('_coordinates_x_y', null, 'text', null)
    ];


    module.default({
        $parent: $content,
        key_id: 'cnt_permanent_spots',
        view: 'cnt_permanent_spots_vw',
        title: t`Stalna števna mesta`,
        skipVariables: ['_location'],
        tableRecordsOptions: {
            skipId: true,
            tableAttributesOverride,
            processTableData: (row, inx) => {

                const data = utils.jsonParse(row._location_data);
                row._location_data = data;
                if (data) {
                    const _location = data;

                    if (_location.lat && _location.lon) {
                        row._location = {
                            lat: _location.lat,
                            lon: _location.lon
                        }

                        let res = [];

                        if (_location.crs && parseInt(_location.crs)==3912) {
                            res[0] = _location.lon;
                            res[1] = _location.lat;
                        }
                        else {
                            res = proj4(globals.proj4defs['3912'], [_location.lon, _location.lat].map(c => parseFloat(c)));
                        }

                        if (res && Array.isArray(res)) {
                            res = res.map(r => Math.round(r));
                            row['_coordinates_x_y'] = `(${res[0]}, ${res[1]})`
                        }
                    }

                    if (_location.spatial_request_result) {
                        const srr = _location.spatial_request_result;
                        row.luo_ime = srr.luo_ime;
                        row.lov_ime = srr.lov_ime;
                    }

                    if (_location.additional && _location.additional.lname) {
                        row['_local_name'] = _location.additional.lname;
                    }
                }

                return row;
            }
        },
        additionaComponentOptions: {    //tell the location component to fetch spatial unit data
            _location_data: {
                defaultSpatialRequest: {
                    apiRoot: globals.apiRoot
                },
                localName: true
            }
        }
    });

}