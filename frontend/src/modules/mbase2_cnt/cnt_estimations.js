import globals from '../../app-globals';
import * as exports from '../../libs/exports';

export default async op => {
    const [
        mutils,
        utils
    ] = await Promise.all([
        exports.mutils(),
        exports.utils()
    ]);

    const {$header, $content, monitoringData} = op;

    if (!monitoringData) return;

    const t = globals.t;

    const cnt_monitoring_id = monitoringData.id;

    if (!cnt_monitoring_id) return;

    const tableName = 'cnt_estimations';

    const tableEditor = await import('../tableEditor');

    let dataTable = null;

    const variables = await mutils.getModuleVariables(tableName, false, false, true);

    const monitoringAttributeDefinition = variables.find(v => v.key_name_id === 'cnt_monitoring_id');
    monitoringAttributeDefinition._component = {
        import: () => exports.Storage(),
        additionalComponentOptions: {
            data: cnt_monitoring_id
        }
    }

    monitoringAttributeDefinition.key_data_type_id = 'integer';

    //this is done to skip auto fetching permanent_spot data
    const cspotAttributeDefintion = variables.find(v => v.key_name_id === '_cnt_lov_koda');
    cspotAttributeDefintion.key_data_type_id = 'integer';

    const refValues = await mutils.getRefCodeListValues(variables);
    
    const luolov = await mutils.requestHelper(globals.apiRoot + `/mbase2/luo_lov_vw`);

    const lovAssocByKoda = {};
    luolov.map(ll => {
        lovAssocByKoda[ll.lov_koda] = {luo_koda: ll.luo_koda, luo_ime: ll.luo_ime, lov_ime: ll.lov_ime}
    });
    ////////////
    const cameraTrapAttributeDefintion = variables.find(v => v.key_name_id === '_cnt_camera_trap');

    const observations = await mutils.requestHelper(globals.apiRoot + `/mb2data/cnt_observation_reports${mutils.prepareFilter([["cnt_monitoring_id","=",cnt_monitoring_id]], false)}`)

    const observationsBySpot = {};
    observations.map(observation => {
        if (!observationsBySpot[observation._cnt_location_id]) observationsBySpot[observation._cnt_location_id] = [];
        observationsBySpot[observation._cnt_location_id].push(observation);
    });

    const datum = '<br>'+monitoringData._start_date + ' - ' + monitoringData._end_date;

    variables.splice(2,0,Object.assign(mutils.createVariableDefinition('lov_ime', t`Lovišče`, 'text', null), {visible_in_table: true}));
    variables.splice(2,0,Object.assign(mutils.createVariableDefinition('luo_ime', t`LUO`, 'text', null), {visible_in_table: true}));

    const $parentEl = await CountingSpotsFilter();

    $header.html(mutils.moduleHeader(monitoringData.t__vrsta_opazovanja || monitoringData.key__vrsta_opazovanja, datum, $parentEl, null, null, null, 'flex-start'));

    const $statusShranjevanja = $('<div style="color: red; display:none; margin-top:0.5em; margin-bottom:-2em">'+globals.t`Shranjujem podatke v bazo ...`+'</div>');

    $header.find('.mbase-title:first').parent().css('width','100%').append($statusShranjevanja);

    async function CountingSpotsFilter() {
        const [
            Select2
        ] = await Promise.all([
            exports.Select2()
        ]);

        const selects = {};

        const luo = {};
        const lov = {};

        luolov.map(ll => {
            luo[ll.luo_koda] = ll.luo_ime;
            lov[ll.lov_koda] = ll.lov_ime;
        });

        selects.luo = new Select2.default({
            data: Object.keys(luo).map(koda => luo[koda]),
            label: t`LUO`,
            allowClear: true,
            onSelect: e => {
                if (!e) return;
                if (!e.params) return;
                const id = e.params.data.id;
                if (!id) return;
                
                updateFilter(luolov.filter(ll => ll.luo_ime == id),['lov']);
            },
            onUnselect: () => {
                updateFilter(luolov,['lov']);
            }
        });

        selects.lov = new Select2.default({
            data: Object.keys(lov).map(koda => lov[koda]),
            label: t`Lovišče`,
            allowClear: true,
            onSelect: e => {
                if (!e) return;
                if (!e.params) return;
                const id = e.params.data.id;
                if (!id) return;
                updateFilter();
            },
            onUnselect: () => {
                updateFilter(null,[]);
            }
        });

        const $parentEl = $('<div/>',{style: 'display: flex; justify-content:center;width:83%;align-items: flex-end;'});

        const $el = $('<div/>',{style: 'display: flex; align-items: flex-start; margin-left:10px'});


        Object.keys(selects).map(
            key => {
                const select = selects[key];
                const $select = select.$el();
                $select.css('width','25vw');
                $select.css('margin-left', '10px')
                $el.append($select);
            }
        );

        $parentEl.append($el);

        return $parentEl;

        function updateFilter(luolov, updateSelects = []) {

            if (updateSelects.length>0) {
                const lov = {};
                luolov.map(ll => {
                    lov[ll.lov_ime] = true;
                });

                updateSelects.map(key => {
                    if (key==='lov') {
                        selects.lov.reinit(Object.keys(lov));
                    }
                });
            }

            //dataTable.table.columns().search( '' ).draw();
            
            const headerKeys = dataTable.getHeaderKeys();

            [{key:'luo', headerKey:'luo_ime'}, {key:'lov', headerKey:'lov_ime'}].map(c => {
                let value = selects[c.key].val();
                
                value = value || '';
                dataTable.table.column(headerKeys.indexOf(c.headerKey)).search(value);
                
            });

            dataTable.table.draw();
        }
    
    }

    const editables = {};

    variables.map(v => {
        
        if (['cnt_monitoring_id','_cnt_lov_koda', 'date_record_created', 'date_record_modified', 'luo_ime', 'lov_ime'].indexOf(v.key_name_id) === -1) {
            let type = 'text';
            if (v.key_data_type_id === 'integer') {
                type = 'number';
            }
            else if (v.key_data_type_id === 'boolean') {
                type = 'checkbox';
            }

            editables[v.key_name_id] = {
                type: type
            };
        }
    });

    await tableEditor.default({

        refValues: refValues,
        variables: variables,
        $parent: $content,
        tableName: tableName,
        skipVariables: ['cnt_monitoring_id','_cnt_lov_koda', 'date_record_created', 'date_record_modified'],

        tableRecordsOptions: {
            skipId: true,
            btn_add_record: false,
            btn_batch_import: false,
            sortVariables: false,
            processTableData: (row) => {
                const spot = lovAssocByKoda[row._cnt_lov_koda];
                row.luo_ime = spot.luo_ime;
                row.lov_ime = spot.lov_ime;
                return row;
            },

            onTableCreated: (dt) => {
                dataTable = dt;
            },
                
            externalEditRecord: (rowData, rowId, rowIndex) => {
                EstimationsInputForm(rowData, rowIndex);
            },
            cellEdit: {
                keys: {
                    columns: ':not(:nth(0), :nth(1), :nth(2))'
                },
                editables: editables,
                onChange: (editable, value, cell, datatable) => {

                    const key = editable.key;
                    
                    const row = datatable.row(cell.index().row);
                    const rowData = row.data();
            
                    rowData[key] = value || parseInt(value || 0);
            
                    $statusShranjevanja.show();
                    mutils.requestHelper(globals.apiRoot + `/mb2data/${tableName}/${rowData['id']}`, 'PUT', {
                        ':cnt_monitoring_id': cnt_monitoring_id,
                        [':'+key]: rowData[key]
                    },{},null).then(res => {
                        $statusShranjevanja.hide();
                        /*
                            https://datatables.net/reference/api/cell().data()
                            Moreover, although the internal cache is not updated until the next draw, the change to the cell's content is visible immediately upon calling this method as a setter, as it writes to the cell's content using innerHTML.
                        */
                    });
                }
            },
            tableOptions:{
            //searching: false,
            fixedColumns: {
                leftColumns: 3
            },
            dom: 'lrtip',
            /*__icons: [
                {
                    action: 'download',
                    class: 'download',
                    onClick: data => {
                        
                    }
                }
            ]*/
        }
        }
    });

    const estimationsInputFormVariables = [
        mutils.createVariableDefinition('cnt_est_bear',t`Koliko (različnih) osebkov rjavega medveda ste opažali v vašem lovišču v zadnjih 3 mesecih?`, 'integer', null),
        mutils.createVariableDefinition('cnt_est_bear_leading_female_cubs0',t`Koliko od teh je vodečih medvedk z letošnjimi mladiči (starosti 0+)?`, 'integer', null),
        mutils.createVariableDefinition('cnt_est_bear_leading_female_cubs1',t`Koliko od teh je vodečih medvedk z lanskimi mladiči (starosti 1+)?`, 'integer', null),
        mutils.createVariableDefinition('cnt_est_MEDV_OPOMBE',t`Opombe`, 'text', null),

        mutils.createVariableDefinition('cnt_est_VOLK_prisotnost',t`Ali ste v zadnjih 3 mesecih zaznali prisotnost volka v vašem lovišču?`, 'code_list_reference', 'yes_no'),
        mutils.createVariableDefinition('cnt_wolf_nacini_opazanja',t`Način opažanja (izbereš lahko več načinov opažanja)`, 'code_list_reference_array', '_cl_cnt_nacini_opazanja'),
        mutils.createVariableDefinition('cnt_est_VOLK_velikost_tropa',t`Koliko živali je bilo v preteklih 3 mesecih največ naenkrat (trop) opaženih, sledenih ali fotografiranih?`, 'integer', null),
        mutils.createVariableDefinition('cnt_est_VOLK_OPOMBE',t`Opombe`, 'text', null),

        mutils.createVariableDefinition('cnt_est_RIS_prisotnost',t`Ali ste v zadnjih 3 mesecih v vašem lovišču zaznali prisotnost risa?`, 'code_list_reference', 'yes_no'),
        mutils.createVariableDefinition('cnt_est_RIS_rednost',t`Je prisotnost redna ali občasna?`, 'code_list_reference', '_cl_redna_obcasna_prisotnost'),
        mutils.createVariableDefinition('cnt_lynx_nacini_opazanja',t`Način opažanja (izbereš lahko več načinov opažanja)`, 'code_list_reference_array', '_cl_cnt_nacini_opazanja'),  
        mutils.createVariableDefinition('cnt_est_RIS_OPOMBE',t`Opombe`, 'text', null),
    ];

    estimationsInputFormVariables.map(v => {
        if (v.ref === '_cl_cnt_nacini_opazanja') {
            v._component = {
                import: () => exports.SelectWithSpecify(),
                defaultOptions: true
            }
        }
    })

    const estimationsInputFormVariablesKeyed = await utils.convertToAssocArray(estimationsInputFormVariables, 'key_name_id');
    const estimationsInputFormRefValues = await mutils.getRefCodeListValues(estimationsInputFormVariables);
    const estimationsInputFormRefValuesKeyed = await mutils.convertReferencesToAssocArray(estimationsInputFormRefValues);

     async function EstimationsInputForm(rowData, rowIndex) {
        
        const estimationReportId = rowData.id;
        const lovId = rowData._cnt_lov_koda;

        const [
            ModalDialog,
            ComponentManager
        ] = await Promise.all([
            exports.ModalDialog(),
            exports.ComponentManager()
        ]);

        const cmr = new ComponentManager.default();
        const record = await exports.record();

        cmr.model.attributes = estimationsInputFormVariables;
        
        const modal = new ModalDialog.default({
            onShown:async ()=>{
                record.default({
                    $parent: modal,
                    cm: cmr,
                    /*saveCallbacks: [onRecordUpserted],
                    saveOptions: op.saveOptions,*/
                    refValues: estimationsInputFormRefValues,
                    onInit: function (cm) {
                        cm.get('cnt_est_bear').$el().before('<h3>'+t`Rjavi medved`+'</h3>');
                        cm.get('cnt_est_MEDV_OPOMBE').$el().after('<h3>'+t`Volk`+'</h3>');
                        cm.get('cnt_est_VOLK_OPOMBE').$el().after('<h3>'+t`Ris`+'</h3>');
                        
                    },
                    title: (monitoringData.t__vrsta_opazovanja || monitoringData.key__vrsta_opazovanja) + ': ' + datum+'<br>'+lovAssocByKoda[lovId].lov_ime + ` (LUO: ${lovAssocByKoda[lovId].luo_ime})`,
                    onSave: (cm) => {
                        console.log(cm)
                        modal.hide();
                    }
                });
            },
            onClose: () => {
                const data = {':cnt_monitoring_id': cnt_monitoring_id};

                variables.map(v=>{
                    const key = v.key_name_id;
                    const cmp = cmr.get(key);
                    if (cmp) {
                        let value = cmp.val();
                        
                        if (value) {
                            if (['cnt_est_VOLK_prisotnost', 'cnt_est_RIS_prisotnost'].indexOf(key)!==-1) {
                                value = estimationsInputFormRefValuesKeyed.codeListValues[value].key === 'yes' ? 1 : 0;
                            }
                            else if (key === 'cnt_est_RIS_rednost') {
                                value = estimationsInputFormRefValuesKeyed.codeListValues[value].key === 'redna' ? 1 : 0;
                            }

                            data[':'+key] = value;
                        }
                    }
                });

                ['cnt_wolf_nacini_opazanja', 'cnt_lynx_nacini_opazanja'].map(key => {
                    let value = cmr.get(key).val();
                    if (value) {
                        const animal = key === 'cnt_wolf_nacini_opazanja' ? 'VOLK' : 'RIS';
                        let specify = null;

                        if (!Array.isArray(value) && value.select) {
                            specify = value.specify;
                            value = value.select;
                        }
                        
                        for (let v of value) {
                            v = estimationsInputFormRefValuesKeyed.codeListValues[v].key;
                            if (v!=='_specify_other_cl_cnt_nacini_opazanja') {
                                if (v==='fotografija - posnetek') v = 'foto';
                                const k = `cnt_est_${animal}_${v}`;
                                data[':'+k] = 1;
                            }
                            else if (specify) {
                                data[`:${animal}_drugo`] = specify;
                            }
                        }
                    }
                });

                $statusShranjevanja.show();
                mutils.requestHelper(globals.apiRoot + `/mb2data/${tableName}/${estimationReportId}`, 'PUT', data ,{},null).then(res => {
                    $statusShranjevanja.hide();
                    Object.keys(data).map(dkey => {
                        const key = dkey.substring(1);
                        const cell = dataTable.getCell(key, rowIndex);
                        cell && cell.data(data[dkey]);
                    });
                });
                
            }
        });

        modal.$body.css('max-height','75vh');
        modal.show();
    }
}

