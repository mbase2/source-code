import globals from '../../app-globals';
import * as exports from '../../libs/exports';

export default async op => {
    const [
        mutils,
        utils
    ] = await Promise.all([
        exports.mutils(),
        exports.utils()
    ]);

    const {$header, $content, monitoringData} = op;

    if (!monitoringData) return;

    const t = globals.t;

    const cnt_monitoring_id = monitoringData.id;

    if (!cnt_monitoring_id) return;

    const tableName = 'cnt_observation_reports';

    const tableEditor = await import('../tableEditor');

    const selectData = {
        'lov':{},
        'luo':{},
        'spot':{}
    }

    let dataTable = null;

    const variables = await mutils.getModuleVariables(tableName, false, false, true);

    const monitoringAttributeDefinition = variables.find(v => v.key_name_id === 'cnt_monitoring_id');
    monitoringAttributeDefinition._component = {
        import: () => exports.Storage(),
        additionalComponentOptions: {
            data: cnt_monitoring_id
        }
    }

    monitoringAttributeDefinition.key_data_type_id = 'integer';

    //this is done to skip auto fetching permanent_spot data
    const cspotAttributeDefintion = variables.find(v => v.key_name_id === '_cnt_location_id');
    cspotAttributeDefintion.key_data_type_id = 'integer';

    const refValues = await mutils.getRefCodeListValues(variables);
    
    //spots = await mutils.requestHelper(globals.apiRoot + `/mb2data/cnt_permanent_spots_vw${mutils.prepareFilter([["_cnt_active","=",true]], false)}`); //example of using a filter
    const spots = await mutils.requestHelper(globals.apiRoot + `/mb2data/cnt_permanent_spots_vw`);
    const spotsAssocById = {};
    spots.map(s => {
        const ldata = utils.jsonParse(s._location_data);
        let parentUnits = '';
        let spatialRequestResult = {};
        if (ldata && ldata.spatial_request_result) {
            spatialRequestResult = ldata.spatial_request_result;
            parentUnits = ` (${spatialRequestResult.luo_ime}, ${spatialRequestResult.lov_ime})`;
            
            selectData.lov[spatialRequestResult.lov_ime] = true;
            selectData.luo[spatialRequestResult.luo_ime] = true;
        }

        const label = s._cnt_permanent_spot_code + ' ' + s._local_name + parentUnits;

        spotsAssocById[s.id] = {label: label, data: s, luo_ime: spatialRequestResult.luo_ime, lov_ime: spatialRequestResult.lov_ime};
        return [s.id, label];
    });

    ////////////
    const cameraTrapAttributeDefintion = variables.find(v => v.key_name_id === '_cnt_camera_trap');

    const observations = await mutils.requestHelper(globals.apiRoot + `/mb2data/cnt_observation_reports${mutils.prepareFilter([["cnt_monitoring_id","=",cnt_monitoring_id]], false)}`)

    const observationsBySpot = {};
    observations.map(observation => {
        if (!observationsBySpot[observation._cnt_location_id]) observationsBySpot[observation._cnt_location_id] = [];
        observationsBySpot[observation._cnt_location_id].push(observation);
    });

    const datum = monitoringData._start_date;

    //add luo and hunting ground
    variables.splice(2,0,Object.assign(mutils.createVariableDefinition('cnt_spot_label', t`Števno mesto`, 'text', null), {visible_in_table: true}));
    variables.splice(2,0,Object.assign(mutils.createVariableDefinition('lov_ime', t`Lovišče`, 'text', null), {visible_in_table: true}));
    variables.splice(2,0,Object.assign(mutils.createVariableDefinition('luo_ime', t`LUO`, 'text', null), {visible_in_table: true}));

    const $parentEl = await CountingSpotsFilter();

    $header.html(mutils.moduleHeader(monitoringData.t__vrsta_opazovanja || monitoringData.key__vrsta_opazovanja, datum, $parentEl, null, null, null, 'flex-start'));

    const $statusShranjevanja = $('<span style="color: red; display:none; padding-top:5px">'+globals.t`Shranjujem podatke v bazo ...`+'</span>');

    $header.find('.mbase-title:first').parent().append($statusShranjevanja);

    const observationVariables = await mutils.getModuleVariables('cnt_observation_reports', false, false, true);
    const observationRefValues = await mutils.getRefCodeListValues(observationVariables);

    async function CountingSpotsFilter() {
        const [
            Select2
        ] = await Promise.all([
            exports.Select2()
        ]);

        const selects = {};

        const spots = Object.keys(spotsAssocById).map(id => spotsAssocById[id]);

        selects.luo = new Select2.default({
            data: Object.keys(selectData.luo),
            label: t`LUO`,
            allowClear: true,
            onSelect: e => {
                if (!e) return;
                if (!e.params) return;
                const id = e.params.data.id;
                if (!id) return;
                
                updateFilter(spots.filter(spot => spot.luo_ime == id),['lov', 'spot']);
            },
            onUnselect: () => {
                updateFilter(spots,['lov', 'spot']);
            }
        });

        selects.lov = new Select2.default({
            data: Object.keys(selectData.lov),
            label: t`Lovišče`,
            allowClear: true,
            onSelect: e => {
                if (!e) return;
                if (!e.params) return;
                const id = e.params.data.id;
                if (!id) return;
                updateFilter(spots.filter(spot => spot.lov_ime == id),['spot']);
            },
            onUnselect: () => {
                updateFilter(spots,['spot']);
            }
        });

        selects.spot = new Select2.default({
            data: spots.map(spot =>  spot.label),
            label: t`Števno mesto`,
            allowClear: true,
            onSelect: e => {
                if (!e) return;
                if (!e.params) return;
                const id = e.params.data.id;
                if (!id) return;
                updateFilter();
            },
            onUnselect: () => {
                updateFilter(spots,[]);
            }
        });

        const $parentEl = $('<div/>',{style: 'display: flex; justify-content:center;width:83%;align-items: flex-end;'});

        const $el = $('<div/>',{style: 'display: flex; align-items: flex-start; margin-left:10px'});


        Object.keys(selects).map(
            key => {
                const select = selects[key];
                const $select = select.$el();
                $select.css('width','25vw');
                $select.css('margin-left', '10px')
                $el.append($select);
            }
        );

        $parentEl.append($el);

        return $parentEl;

        function updateFilter(filteredSpots, updateSelects = []) {

            if (updateSelects.length>0) {
                const lov = {};
                const spots = filteredSpots.map(spot => {
                    lov[spot.lov_ime] = true;
                    return spot.label;
                });

                updateSelects.map(key => {
                    if (key==='lov') {
                        selects.lov.reinit(Object.keys(lov));
                    }
                    else if (key==='spot') {
                        selects.spot.reinit(spots);
                    }
                });
            }

            //dataTable.table.columns().search( '' ).draw();
            
            const headerKeys = dataTable.getHeaderKeys();

            [{key:'luo', headerKey:'luo_ime'}, {key:'lov', headerKey:'lov_ime'}, {key: 'spot', headerKey:'cnt_spot_label'}].map(c => {
                let value = selects[c.key].val();
                
                value = value || '';
                dataTable.table.column(headerKeys.indexOf(c.headerKey)).search(value);
                
            });

            dataTable.table.draw();
        }
    
    }

    await tableEditor.default({

        refValues: refValues,
        variables: variables,
        $parent: $content,
        tableName: tableName,
        skipVariables: ['cnt_monitoring_id','_cnt_location_id', 'date_record_created', 'date_record_modified'],

        tableRecordsOptions: {
            skipId: true,
            btn_add_record: false,
            btn_batch_import: false,
            sortVariables: false,
            processTableData: (row) => {
                const spot = spotsAssocById[row._cnt_location_id];
                row.luo_ime = spot.luo_ime;
                row.lov_ime = spot.lov_ime;
                row.cnt_spot_label = spot.label;
                return row;
            },

            onTableCreated: (dt) => {
                dataTable = dt;
                /*$('table.DTFC_Cloned thead:nth(1)').hide();
                dt.table.on( 'order.dt',  function () {
                    //let order = table.order();
                    //console.log("Ordered column " + order[0][0] + ", in direction " + order[0][1]);
                    setTimeout(()=>$('table.DTFC_Cloned thead:nth(1)').hide(),20);
                } );
                */
            },
                
            externalEditRecord: (rowData, rowId, rowIndex) => {
                ObservationsInputForm(rowData, rowIndex);
            },
            cellEdit: {
                editables: {
                    _cnt_camera_trap: {
                        type: 'checkbox'
                    },
                    _cnt_all: {
                        type: 'number'
                    },
                    _cnt_leading_female_cubs0: {
                        type: 'number'
                    },
                    _cnt_leading_female_cubs1: {
                        type: 'number'
                    },
                    _cnt_leading_female: {
                        type: 'number'
                    },
                    _cnt_cubs0_with_leading_female: {
                        type: 'number'
                    },
                    _cnt_cubs1_with_leading_female: {
                        type: 'number'
                    },
                    _cnt_cubs0: {
                        type: 'number'
                    },
                    _cnt_cubs1: {
                        type: 'number'
                    },
                    _cnt_cubs: {
                        type: 'number'
                    },
                    _cnt_remaining: {
                        type: 'number'
                    }
                },
                onChange: (editable, value, cell, datatable) => {

                    const key = editable.key;
                    
                    const rowIndex = cell.index().row;
                    const row = datatable.row(rowIndex);
                    const rowData = row.data();
            
                    rowData[key] = value || parseInt(value || 0);

                    const keysForUpdate = [];
            
                    if (key != '_cnt_leading_female' && ['_cnt_leading_female_cubs0','_cnt_leading_female_cubs1'].indexOf(key)!==-1) {
                        rowData._cnt_leading_female = parseInt(rowData._cnt_leading_female_cubs0 || 0) + parseInt(rowData._cnt_leading_female_cubs1 || 0);
                        keysForUpdate.push('_cnt_leading_female');
                    }
                    else if (key != '_cnt_cubs0' && key == '_cnt_cubs0_with_leading_female') {
                        rowData._cnt_cubs0 = parseInt(rowData._cnt_cubs0_with_leading_female || 0);
                        keysForUpdate.push('_cnt_cubs0');
                    }
                    else if (key != '_cnt_cubs1' && key == '_cnt_cubs1_with_leading_female') {
                        rowData._cnt_cubs1 = parseInt(rowData._cnt_cubs1_with_leading_female || 0);
                        keysForUpdate.push('_cnt_cubs1');
                    }
                    else if (key != '_cnt_cubs' && ['_cnt_cubs0', '_cnt_cubs1'].indexOf(key)!==-1) {
                        rowData._cnt_cubs = parseInt(rowData._cnt_cubs0 || 0) + parseInt(rowData._cnt_cubs1 || 0);
                        keysForUpdate.push('_cnt_cubs');
                    }
                    else if (key != '_cnt_all' && ['_cnt_leading_female','_cnt_cubs','_cnt_remaining'].indexOf(key)!==-1) {
                        rowData._cnt_all = parseInt(rowData._cnt_leading_female || 0) + parseInt(rowData._cnt_cubs || 0) + parseInt(rowData._cnt_remaining || 0);
                        keysForUpdate.push('_cnt_all');
                    }
            
                    $statusShranjevanja.show();
                    mutils.requestHelper(globals.apiRoot + `/mb2data/${tableName}/${rowData['id']}`, 'PUT', {
                        ':cnt_monitoring_id': cnt_monitoring_id,
                        [':'+key]: rowData[key]
                    },{},null).then(res => {
                        $statusShranjevanja.hide();
                        //row.data(rowData);
                        for (const key of keysForUpdate) {
                            const cell = dataTable.getCell(key, rowIndex);
                            cell && cell.data(rowData[key]);
                        }
                        /*
                            https://datatables.net/reference/api/cell().data()
                            Moreover, although the internal cache is not updated until the next draw, the change to the cell's content is visible immediately upon calling this method as a setter, as it writes to the cell's content using innerHTML.
                        */
                    });
                }
            },
            tableOptions:{
            fixedColumns: {
                leftColumns: 4
            },
            //searching: false,
            dom: 'lrtip',
            /*__icons: [
                {
                    action: 'download',
                    class: 'download',
                    onClick: data => {
                        
                    }
                }
            ]*/
        }
        }
    });

    async function ObservationsInputForm(rowData, rowIndex) {
        
        const observationReportId = rowData.id;
        const locationId = rowData._cnt_location_id;

        const [
            ModalDialog
        ] = await Promise.all([
            exports.ModalDialog()
        ]);

        const $cameraTrap = $('<input style="margin-right:5px; padding-top:2px" type="checkbox" id="camera-trap" name="camera-trap">');

        $cameraTrap.prop('checked', rowData._cnt_camera_trap === globals.t`DA`);

        let observationsTable = null;
        
        const modal = new ModalDialog.default({
            size: 'modal-xxl',
            keyboard: false,
            onShown:async ()=>{
                
                await tableEditor.default({
                    refValues: observationRefValues,
                    variables: observationVariables,
                    $parent: modal.$body,
                    tableName: 'cnt_observation_reports',
                    skipVariables: ['_data', '_batch_id', '_uname','date_record_created', 'date_record_modified'],
                    tableRecordsOptions:{
                        onTableCreated: (table) => {
                            observationsTable = table;
                        },
                        deletable: true,
                        skipAttributesFromTable: ['cnt_monitoring_id', '_cnt_location_id'],
                        select: [
                            {key: 'id', value: observationReportId}
                        ],
                        skipId: true,
                        t: {
                            btn_add_record: t`Dodaj opažanje`
                        },
                        btn_batch_import: false
                    }
                });
            },
            onClose: () => {
                
                const observationsSum = {
                    _cnt_all: 0,
                    _cnt_leading_female: 0,
                    _cnt_cubs0: 0,
                    _cnt_cubs1: 0
                }

                observationsTable.getData().map(row => {
                    Object.keys(observationsSum).map(key => observationsSum[key]+=row[key]);
                });

                observationsSum._cnt_cubs = observationsSum._cnt_cubs0 + observationsSum._cnt_cubs1;
                observationsSum._cnt_cubs0_with_leading_female = observationsSum._cnt_cubs0;
                observationsSum._cnt_cubs1_with_leading_female = observationsSum._cnt_cubs1;
                observationsSum._cnt_remaining = observationsSum._cnt_all - (observationsSum._cnt_cubs + observationsSum._cnt_leading_female);

                const data = {':cnt_monitoring_id': cnt_monitoring_id};

                Object.keys(observationsSum).map(key => {
                    const value = observationsSum[key];
                    data[':'+key] = value;
                });

                data[':_cnt_camera_trap'] = $cameraTrap.prop('checked') ? 1 : 0;

                $statusShranjevanja.show();
                mutils.requestHelper(globals.apiRoot + `/mb2data/${tableName}/${observationReportId}`, 'PUT', data ,{},null).then(res => {
                    $statusShranjevanja.hide();
                    Object.keys(observationsSum).map(key => {
                        const cell = dataTable.getCell(key, rowIndex);
                        cell.data(observationsSum[key]);
                    });
                    dataTable.getCell('_cnt_camera_trap', rowIndex).data($cameraTrap.prop('checked') ? globals.t`DA` : globals.t`NE`);
                });
                
            }
        });

        modal.$title.html((monitoringData.t__vrsta_opazovanja || monitoringData.key__vrsta_opazovanja) + ': ' + datum+'<br>'+spotsAssocById[locationId].label);

        const $el = $('<div/>');
        $el.append($cameraTrap);
        $el.append(`<label for="camera-trap">${cameraTrapAttributeDefintion.t_name_id || cameraTrapAttributeDefintion.key_name_id}</label>`);

        modal.$body.append($el);//Čas začetka posameznega opazovanja cameraTrapAttributeDefintion

        modal.show();
    }
}

