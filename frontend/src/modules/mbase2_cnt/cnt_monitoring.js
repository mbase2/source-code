import globals from '../../app-globals';
import * as exports from '../../libs/exports';

const t = globals.t;

export default async (op) => {
    const {$header, $content} = op;

    const [
        mutils,
        utils,
        Button,
        module
    ] = await Promise.all([
        exports.mutils(),
        exports.utils(),
        exports.Button(),
        import('../dataEditor')
    ]);

    let dataEditor = null;
    let recordCm = null;

    /*const mutils = await exports.mutils();
    const utils = await exports.utils();*/
    const button = new Button.default({
        label: t`Dodaj novo serijo opazovanj`,
        iconClass: 'plus',
        type: 'btn-primary',
        classes: 'btn-lg',
        onClick: () => {
            dataEditor.tableRecordsInstance.editRecord();
        }
    });
    
    $header.html(mutils.moduleHeader(t`Štetja`,null,button.$el()));
    
    dataEditor = await module.default({
        $parent: $content,
        key_id: 'cnt_monitoring',
        skipVariables: [],
        tableRecordsOptions: {
            onInitRecord: (cm) => {
                recordCm = cm;
                onVrstaOpazovanjaSelected();
            },
            skipId: true,
            btn_add_record:false,
            btn_batch_import: false,
            tableOptions: {
                __icons: [
                    {
                        action: 'list',
                        class: 'list',
                        onClick: data => {
                            const pathname = utils.rtrimSlash(window.location.pathname);
                            utils.hrefClick(pathname+'?cnt_monitoring_id='+data.id);
                        }
                    },
                    {
                        action: 'download',
                        class: 'download',
                        onClick: data => {
                            
                        }
                    }
                ]
            }
        },
        skipTitle: true,
        additionaComponentOptions: {    //tell the location component to fetch spatial unit data
            _vrsta_opazovanja: {
                onSelect: () => {
                    onVrstaOpazovanjaSelected();
                }
            },
            _start_date: {
                onChange: () => {
                    if (!recordCm) return;

                    const endDateCmp = recordCm.get('_end_date');
                    const selectedDate = recordCm.get('_start_date').val();

                    if (moment(selectedDate).isAfter(endDateCmp.val())) {
                        endDateCmp.val(selectedDate);
                    }

                    endDateCmp.dp.setStartDate(selectedDate);

                    isPermanentCountSpot() && endDateCmp.val(selectedDate);
                    
                }
            }
        }
    });

    function isPermanentCountSpot() {
        const key = recordCm.get('_vrsta_opazovanja').getAdditionalDataForSelectedOption();
        if (!key) return;

        return key === 'stalna števna mesta';
    }

    function onVrstaOpazovanjaSelected() {
        if (!recordCm) return;
        const key = recordCm.get('_vrsta_opazovanja').getAdditionalDataForSelectedOption();
        if (!key) return;

        const $endDate = recordCm.get('_end_date').$el();
        const hideEndDate = isPermanentCountSpot() ? $endDate.hide() : $endDate.show();
    }
}