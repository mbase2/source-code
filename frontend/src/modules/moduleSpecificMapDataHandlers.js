import { translateAttributeValue } from '../libs/mbase2_utils';
import { jsonParse } from '../libs/utils';

import globals from '../app-globals';
/**
 * Functions that deal with data presentation for specific modules - don't add additional object properties here, use existing variables keys
 */
export default (moduleKey, dataRows, isGrid, moduleAttributes) => {

    function preprocessOutline(out) {

        if (!isGrid && moduleKey === 'dmg') {
            out['_dmg_object_list_ids'] = out['_dmg_object_list_ids'].map((o,inx) => {
                let quantity = '';
                if (out['kol_rnd'][inx] && out['kol_rnd'][inx].trim()) {
                    quantity = ` (${out['kol_rnd'][inx].trim()}`;
                    if (out['enota_gs'][inx] && out['enota_gs'][inx].trim()) {
                        quantity = quantity + ' ' + out['enota_gs'][inx].trim();
                    }
                    quantity = quantity + ')';
                }
    
                return o + quantity;
            });
            
            delete out['kol_rnd'];
            delete out['enota_gs'];
    
            if (out['_dmg_object_list_ids'].length > 1) {
                out['_dmg_object_list_ids'] = ['<ul style="margin-bottom:-1em"><li>&nbsp;&nbsp;-&nbsp;' + out['_dmg_object_list_ids'].join('</li><li>&nbsp;&nbsp;-&nbsp;') + '</li></ul>'];
            }
        }
    }

    if (!dataRows) return dataRows;

    const handlers = {
        dmg:{
            _dmg_object_list_ids: (value) => {
                value=JSON.parse(value);
                return isGrid ? [...new Set(value)] : translateAttributeValue(value, 
                    moduleAttributes.attributesKeyed['_dmg_object_list_ids'],
                    moduleAttributes.refValues, true, false);
            },
            _compensation_sum: (value) => {
                return value ? parseFloat(value).toFixed(2) : '';
            },
            kol_rnd: (value) => {
                if (!value) return [""];
                return JSON.parse(value);
                
            },
            enota_gs: (value) => {
                if (!value) return [""];
                return JSON.parse(value);
            },
            protection_measure: (value) => {
                if (!value) return "";
                value = value + "";
                return value.split('|');
            }
        }
    }

    const handler = handlers[moduleKey];

    if (!handler) return [...dataRows];

    const dataOut = [];

    for (const data of dataRows) {
        const out = {};

        Object.keys(data).map(key => {
            out[key] = handler[key] ? handler[key](data[key]) : data[key];
        });

        preprocessOutline(out);

        dataOut.push(out);
    }

    return dataOut;
}

function formatGroupDate(date) {
    const mdate = moment(date);
    const month = mdate.format('MMMM');
    const year = mdate.format('YYYY');

    return globals.t`${month}` + ' ' + year;
}

export const groupData = (moduleKey, dataRows, moduleAttributes) => {

    const vgroups ={};
    ['event_date', 'species_list_id','_licence_name','licence_list_id'].map(key=>{
        vgroups[key] = dataRows.map(row => row[key]);
    });

    const outRow = {};
    outRow._event_count = dataRows.length;
    vgroups.event_date.sort();
    
    const minDate = vgroups.event_date[0];
    const maxDate = vgroups.event_date[vgroups.event_date.length-1];

    outRow.event_date = minDate === maxDate ? formatGroupDate(minDate) : `${formatGroupDate(minDate)} - ${formatGroupDate(maxDate)}`;
    /////////////////////
    const uniqueSpecies = [... new Set(vgroups.species_list_id)];

    outRow.t_species_list_id = uniqueSpecies.map(sid => 
    translateAttributeValue(sid, 
        moduleAttributes.attributesKeyed.species_list_id,
        moduleAttributes.refValues)
    ).filter(x=>x).join(', ');

    const licenceAttributeName = moduleAttributes.attributesKeyed.licence_list_id ? 'licence_list_id':'_licence_name';

    if (moduleAttributes.attributesKeyed[licenceAttributeName]) {
        const uniqueLicences = [... new Set(vgroups[licenceAttributeName])];
        outRow['__licence_list'] = []; 
        uniqueLicences.map(sid => { 
            outRow['__licence_list'].push({
                id: sid,
                value: translateAttributeValue(sid, 
                moduleAttributes.attributesKeyed[licenceAttributeName],
                moduleAttributes.refValues)
            });
        });
    }

    if (moduleKey === 'dmg') {
        
        vgroups[ '_dmg_object_list_ids'] = dataRows.map(row => row['_dmg_object_list_ids']);
        /////////////////

        const groupObjectsTranslated = translateAttributeValue(vgroups._dmg_object_list_ids.flat(1), 
            moduleAttributes.attributesKeyed._dmg_object_list_ids,
            moduleAttributes.refValues, true, false);
        
        const groupCount = groupCountArrayElements(groupObjectsTranslated);

        outRow.t__dmg_object_list_ids = Object.keys(groupCount).map(key => `${key}:  ${groupCount[key]}x`).join(', '); //if key is preceeded with t_ it will be skipped from the translation later (when rendering HTML)
    }
    else if (moduleKey==='mortbiom') {
        ['way_of_withdrawal_list_id', 'biometry_loss_reason_list_id'].map(vkey=>{
            const groupObjectsTranslated = dataRows.map(row => row[vkey]).map(id=>
                translateAttributeValue(id, 
                    moduleAttributes.attributesKeyed[vkey], 
                    moduleAttributes.refValues)
            ).filter(x=>x);

            const groupCount = groupCountArrayElements(groupObjectsTranslated);

            outRow['t_' + vkey] = Object.keys(groupCount).map(key => `${key}:  ${groupCount[key]}x`).join(', ');
        });
    }
    else if (moduleKey==='interventions') {
        ['intervention_reason'].map(vkey=>{
            const groupObjectsTranslated = dataRows.map(row => row[vkey]).map(id =>
                translateAttributeValue(id, 
                    moduleAttributes.attributesKeyed[vkey], 
                    moduleAttributes.refValues)
            ).filter(x=>x);

            const groupCount = groupCountArrayElements(groupObjectsTranslated);

            outRow['t_' + vkey] = Object.keys(groupCount).map(key => `${key}:  ${groupCount[key]}x`).join(', ');
        });
    }
    else if (moduleKey==='cnt') {
        ['_cnt_all','_cnt_leading_female_cubs'].map(vkey=>{
            outRow[vkey] = dataRows.map(row => row[vkey]).reduce((partialSum, a) => partialSum + a, 0);
        });
    }

    return [outRow];
}

function groupCountArrayElements(a) {
    const out={};
    a.map(v => out[v] = (out[v] || 0) + 1);
    return out;
}