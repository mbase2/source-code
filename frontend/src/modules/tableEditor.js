import globals from '../app-globals';
import { assignAdditionalComponentOptions } from '../libs/mbase2_utils';

export default async op => {

    const {refValues, $parent, tableName, key_id} = op;
    let {variables} = op;

    if (!variables) return;

    if (op.skipVariables) {
        variables = variables.filter(v => op.skipVariables.indexOf(v.key_name_id)===-1);
    }

    if (op.additionaComponentOptions) {
        assignAdditionalComponentOptions(variables, op.additionaComponentOptions);
    }

    if (op.componentInitialization) {
        Object.keys(op.componentInitialization).map(key => {
            const attribute = variables.find(v => v.key_name_id === key);
            if (attribute) {
                attribute._component = Object.assign(attribute._component || {default: true}, {onComponentAdded: op.componentInitialization[key]});
            }
        });
    }

    const schema = op.schema || 'mb2data';
    
    const tableRecordsOptions = Object.assign({
        $parent: $parent,
        tableName: tableName,
        refValues: refValues,
        sortVariables: op.sortVariables,
        variablesAttributes: variables, 
        saveOptions: {
            rootUrl: globals.apiRoot + '/' + schema
        },
        url: globals.apiRoot + `/${schema}/${op.view || tableName}/language/${globals.language}`,
        batchOptions: {
            model: {[tableName]: variables}
        }
    }, op.tableRecordsOptions || {});

    tableRecordsOptions.tableOptions = Object.assign({
        scrollY: 'calc(100vh - 300px)'
    }, (op.tableRecordsOptions || {}).tableOptions || {});

    if (key_id === 'ct_camelot_sources') {
        tableRecordsOptions.skipId = true;
        tableRecordsOptions.btn_batch_import = false;
    }

    const recordsTable = await import('./recordsTable');
    return await recordsTable.default(tableRecordsOptions);    
}