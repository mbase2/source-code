import * as exports from '../libs/exports';
import {t, patterns} from '../libs/utils';
import globals from '../../app-globals';

/**
 * Translation form
 * 
 * @param {object} $parent a jQuery $container for the form
 * @param {object} cm ComponentManager object instance
 * @param {object} data 
 * @param {object} data.languages code list values of the available languages
 * @param {object} data.model code list ID for storing the translation
 * @param {object} data.list_id
 */

export default async ($parent, cm, data = {}) => {
    const [
        Inputs,
        ComponentManager,
        Button,
        mutils,
        utils,
        Storage
    ] = await Promise.all([
        exports.Inputs(),
        exports.ComponentManager(),
        exports.Button(),
        exports.mutils(),
        exports.utils(),
        exports.Storage()
    ]);

    const clv = data.languages;

    const {model} = data;

    const components = {
        list_id: {
            component: Storage.default,
            options: {
                data: data.list_id
            }
        },
        key:{
            component: Inputs.Input,
            options: 
            {
                label: 'label_key',
                required: true, 
                pattern: patterns.machine_key,
                help: t`Allowed characters a-z,_,0-9. Has to start with a letter!`
            },
            $parent: $parent
        },
        values:{
            component: Inputs.default,
            options: {
                inputs:clv.map(codeListItem => ({label: codeListItem.key, key: codeListItem.key}))
            },
            $parent: $parent
        },

        btn_save_translation:{
            component: Button.default,
            options: {
                label: t`Save`, 
                style:'float:right',
                onClick: onSave
            },
            $parent: $parent
        }
    }

    utils.addKeysToComponents(components);
    cm = cm || new ComponentManager.default();
    
    await cm.add(components.list_id);
    await cm.add(components.key);
    await cm.add(components.values);
    await cm.add(components.btn_save_translation);

    if (model) {
        model.values = JSON.parse(model.values);
        cm.model(model);
    }

    const saveOptions = await mutils.saveButtonHelper({
        attributes: {
            ':list_id': 'list_id',
            ':key': 'key',
            ':values': 'values'
        },
        validate: true, 
        url: globals.apiRoot + '/code_list_options'
    });

    function onSave() {
        cm.save(saveOptions);
    }
}