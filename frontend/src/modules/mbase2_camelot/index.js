import globals from '../../app-globals';
import * as exports from '../../libs/exports';

const fastParse = nw.require('@fast-csv/parse');
let selectedSource = null;
let serverImages = {};
let speciesField;

//https://stackoverflow.com/questions/8493195/how-can-i-parse-a-csv-string-with-javascript-which-contains-comma-in-data
function csvToArray(text) {
    let p = '', row = [''], ret = [row], i = 0, r = 0, s = !0, l;
    for (l of text) {
        if ('"' === l) {
            if (s && l === p) row[i] += l;
            s = !s;
        } else if (',' === l && s) l = row[++i] = '';
        else if ('\n' === l && s) {
            if ('\r' === p) row[i] = row[i].slice(0, -1);
            row = ret[++r] = [l = '']; i = 0;
        } else row[i] += l;
        p = l;
    }
    return ret;
};

function restartApp() {
    alert('The application was updated and is going to be restarted.');
    const { exec } = nw.require('child_process');
    exec(`import_from_camelot-win-x64.exe --allow-running-insecure-content .`, (error, stdout, stderr) => {
        if (error) {
            console.error(`Error during restart: ${error.message}`);
            return;
        }
        process.exit(); // Close the current instance after the new one starts
    });
}

function checkIfInsecureContentIsEnabled() {
    const fs = nw.require('fs');

    const pkg = JSON.parse(fs.readFileSync('./package.json'));

    if (!pkg) return true;
    
    if (!pkg['chromium-args'] || pkg['chromium-args'] != "--allow-running-insecure-content --disable-web-security") {
        pkg["chromium-args"] = "--allow-running-insecure-content --disable-web-security";
        fs.writeFileSync('./package.json', JSON.stringify(pkg, null, "\t"));
        return false;
    }

    return true;
}

export default async (op) => {
    const [
        utils, 
        mutils,
        ComponentManager,
        Button,
        Select2
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        exports.ComponentManager(),
        exports.Button(),
        exports.Select2()
    ]);

    if (!checkIfInsecureContentIsEnabled()) {
        $('body').html('<div style="padding:20px;font-size:1.5em">The application was updated and needs to be manually restarted.</div>');
        return;
    }

    nw.Window.get().showDevTools();

    const {$parent} = op;

    let surveysCmp = null;

    const $surveys = $('<div/>');

    const $controls = $('<div/>');

    $parent.css('padding','10px').css('padding-top','80px');

    const $main = $('<div style="width:100%; margin-top:20px; max-height: 75vh; overflow-y:auto"></div>');

    let $output = $('<div/>');

    $("nav div.hidden:first").hide();

    const cm = new ComponentManager.default();
    const cmp = {
        source: exports.select({
            label: 'Camelot source',
            onSelect: onCamelotSelected,
            url: globals.apiRoot + '/mb2data/ct_camelot_sources',
            process: d => [d.id, d.camelot_source_name],
            $parent: $controls
        })
    };

    await utils.loadComponents(cmp);

    await cm.add(cmp.source);

    $controls.append($surveys);

    await cm.add({
        key: 'button',
        component: Button.default,
        options: {
            label: 'Import data to mbase2',
            classes: 'btn-lg btn-primary',
            style: 'margin-top:10px',
            onClick: onImport
        },
        $parent: $controls
    });

    $parent.append($controls);
    $parent.append($main);

    //onImport();

    function resolveReferences(surveyArray, context) {
        const survey = {};
        for (let i = 1; i < surveyArray.length; i += 2) {
            const key = surveyArray[i].replace(/^~:/, ''); // Remove the "~:" prefix from keys
            let value = surveyArray[i + 1];

            // Resolve references if the value is a reference
            if (typeof value === 'string' && value.startsWith('^')) {
                const refIndex = parseInt(value.substring(1), 10);
                value = context[refIndex];
            }

            // Add to survey object
            survey[key] = value;
        }
        return survey;
    }

    // Function to parse the surveys data
    function parseSurveys(data) {
        const context = {};

        // First pass: build context with initial values
        data.forEach((surveyArray, index) => {
            context[index] = surveyArray;
        });

        // Second pass: resolve references and build the final surveys
        return data.map(surveyArray => resolveReferences(surveyArray, context));
    }

    async function onCamelotSelected(e) {
        $surveys.empty();
        const id = utils.select2value(e);
        if (!id) return;
        selectedSource = cm.getData('source').filter(d => id == d.id);
        if (selectedSource) {
            selectedSource = selectedSource[0];

            selectedSource.url = utils.trimSlash(selectedSource.url);  //remove leading and trailing /
            
            const surveysRes = await mutils.requestHelper(selectedSource.url + '/surveys');
            const surveys = [];
            
            if (surveysRes) {
                const sdata = JSON.parse(surveysRes);
                if (sdata && sdata.length > 0) {
                    const s = parseSurveys(sdata);
                    const keys = Object.keys(s[0]);
                    const inxId = keys.indexOf('survey-id');
                    const inxName = keys.indexOf('survey-name');
                    
                    if (inxId > -1 && inxName > -1) {
                        
                        surveys.push([s[0]['survey-id'], s[0]['survey-name']]);

                        for (let i = 1; i < s.length; i++) {
                            const surveyData = s[i];
                            const orderedSurveyData = Object.keys(surveyData).sort().reduce(
                                (obj, key) => { 
                                  obj[key] = surveyData[key]; 
                                  return obj;
                                }, 
                                {}
                            );
                            surveys.push([Object.values(orderedSurveyData)[inxId], Object.values(orderedSurveyData)[inxName]])
                        }

                        surveys.map(survey => survey[1] = `${survey[1]} (ID: ${survey[0]})`);
                    }
                }
            }
            
            surveysCmp = new Select2.default({
                label: 'Surveys',
                data: surveys
            });
    
            $surveys.html(surveysCmp.$el());
        }
    }

    async function uploadImagesToMbase(data, onFinish) {

        const header = data[0];

        //data=[data[0], data[1]];
        
        const mediaNameInx = header.indexOf('Media Filename');

        $controls.LoadingOverlay("show");
        const $count = $('<span>0</span>');
        $output.append('Copying media data: ');
        $output.append($count);

        let mediaFileNames = {};
        for (let inx = 1; inx < data.length; inx++) {
            const fname = data[inx][mediaNameInx];
            mediaFileNames[fname] = true;
        }

        mediaFileNames = Object.keys(mediaFileNames);

        $output.append('/' + mediaFileNames.length);

        function updateCounter() {
            let cnt = parseInt($count.text());
            cnt++;
            $count.text(cnt);
            
            if (cnt === mediaFileNames.length) {
                $controls.LoadingOverlay("hide");
                $output.before("<div style='color:#006400'><br><b>Media data have been successfully copied to the Mbase server.</b></div>");
                $output.before("<div><br><b>Importing the data to the database...</b></div>");
                onFinish();
            }
        }
        
        for (let inx = 0; inx < mediaFileNames.length; inx++) {
       
           //https://developer.mozilla.org/en-US/docs/Web/API/Streams_API

           const fname = mediaFileNames[inx];

           if (serverImages[fname.toLowerCase()]) {
            updateCounter();
            const $o = $(`<div>Image ${fname} is already on the server.</div><hr>`); 
            $output.append($o);  
            continue;
           }
           
           const fullPath = selectedSource.url + '/media/photo/'+fname;
           const $o = $(`<div>Fetching ${fullPath} from Camelot ...</div>`); 
           $output.append($o);  

            // Fetch the original image
            fetch(fullPath)
            // Retrieve its body as ReadableStream
            .then(response => response.body)
            // Create a gray-scaled PNG stream out of the original
            //.then(rs => rs.pipeThrough(new TransformStream(new GrayscalePNGTransformer())))
            // Create a new response out of the stream
            .then(rs => new Response(rs))
            // Create an object URL for the response
            .then(response => response.blob())
            .then(blob => {
                //https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Sending_and_Receiving_Binary_Data
                var oReq = new XMLHttpRequest();
                $o.append('<div>OK. Pushing image to mbase2 server ...</div>');
                oReq.open("POST", globals.apiRoot + '/camelot-image-upload/private/' + fname, true);
                oReq.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                oReq.onload = function (oEvent) {
                    //console.log('uploaded', oEvent)
                    updateCounter();
                    $o.append(`<div>Image ${fname} successfully uploaded.</div><hr>`);
                };

                oReq.send(blob);
            })

            //.then(blob => URL.createObjectURL(blob))
            // Update image
            //.then(url => image.src = url)
            .catch(error=> {
                console.error(error);
                $o.append(`<div style="color: red"><b>Error while uploading image</b> ${fname}: ${error}</div><hr>`);
                if (cnt === data.length) $controls.LoadingOverlay("hide");
            });
        }
    }

    async function onImport() {

        $main.empty();
        $output = $('<div/>');
        $main.html($output);

        if (!selectedSource) {
            alert('You have to select a Camelot data source');
            return;
        }

        const surveyId = surveysCmp.val()

        if (!surveyId) {
            alert('You have to select a survey for import');
            return;
        }

        speciesField = selectedSource.camelot_species_field || 'Species Common Name';

        const serverImagesRes = await mutils.requestHelper(globals.apiRoot+'/mbase2call/get_camelot_image_names/'+selectedSource.id);

        serverImages = {};

        serverImagesRes.map(r => serverImages[r.toLowerCase()]=true);

        /*
        const fs = nw.require('fs');

        const dataOut = JSON.parse(fs.readFileSync('./test.txt'));

        console.log(dataOut)

        uploadImagesToMbase(dataOut, () => onImagesUploaded(dataOut));
        return;
        */

        $controls.LoadingOverlay("show");
        $output.append(`Fetching a full-export report from: ${selectedSource.url + '/report/survey-export/download?survey-id=' + surveyId}<br>`);
        
        const res = await utils.request(selectedSource.url + '/report/survey-export/download?survey-id=' + surveyId, undefined, null, {onError: (r, e) => {
            $output.append('<div style="color: red"><b>Error:</b> '+e+'</div>');
        }}, undefined, 'text');

        if (res===false) {
            $controls.LoadingOverlay("hide");
            return;
        }

        setTimeout(()=>{
            $output.append(`Report file successfully fetched. Parsing the data ... this can take 20 seconds or more.<br>`);
            parseCsvData(res);
        }, 500);     
    }

    function parseCsvData(res) {
        const data = [];
        
        fastParse.parseString(res, { ignoreEmpty: true })
        .on('error', error => console.error(error))
        .on('data', row => data.push(row))
        .on('end', (rowCount) => {
            onDataParsed(data);
        });
    }

    function onDataParsed(lines) {
        const header = lines[0];
        const speciesFieldInx = header.indexOf(speciesField);
        const mediaFileNameInx = header.indexOf('Media Filename');
        const ncols = header.length;
        const dataOut = [header];

        for (let i=1; i < lines.length; i++) {
            let row = lines[i];
            if (row.length !== ncols) continue;
            if (row[speciesFieldInx].toLowerCase().includes('lynx')) {
                if (!serverImages[row[mediaFileNameInx].toLowerCase()]) {
                    dataOut.push(row);
                }
            }
        }

        setTimeout(()=>{
            $output.append(`Data successfully parsed.<br>`);
            if (dataOut.length === 1) {
                $controls.LoadingOverlay("hide");
                $output.before(`<div style='color:#006400'><br><b>There are no new rows for import.</b></div>`);
            }
            else{
                uploadImagesToMbase(dataOut, () => onImagesUploaded(dataOut));
            }
        }, 500);
    }

    function onImagesUploaded(dataOut) {
        mutils.requestHelper(globals.apiRoot+'/camelot/'+selectedSource.id, 'POST', dataOut,
        {
            onError: (r, e) => $output.before('<div style="color: red"><b>Error:</b> '+ (e.err || e) +'</div>')
        }, $controls).then(res => {
            $controls.LoadingOverlay("hide");
            if (res === false) return;
            
            if (res.length === 0 || (res.errors && res.errors.length === 0)) {
                $output.before(`<div style='color:#006400'><br><b>Data upload finished without reported errors.</b></div>`);
            }
            else {
                const $container = $('<div/>',{style:'margin-top:10px;margin-bottom:10px;border-bottom:solid 1px'});
                $output.before($container);
                mutils.importErrorsHandler(res, $container);    
            }
        });
    }

    function testData() {
        return [
            [
                "Absolute Path",
                "Camera ID",
                "Make",
                "Model",
                "Camera Name",
                "Attention Needed Flag",
                "Camera Check Flag",
                "Date/Time",
                "Media created",
                "Media Filename",
                "Media Format",
                "Media ID",
                "Media Processed Flag",
                "[Translation missing]",
                "Media updated",
                "Media URI",
                "Exposure Bias Value",
                "Flash",
                "Aperture Value",
                "Focal Length",
                "ISO Speed Ratings",
                "Orientation",
                "Image Height",
                "Image Width",
                "Sighting Created",
                "Sighting ID",
                "Sighting Quantity",
                "Sighting Updated",
                "Site Area (km2)",
                "City",
                "Country/Primary Location Name",
                "Site ID",
                "Site Name",
                "Province/State",
                "Sub-location",
                "Species Mass End",
                "Species Mass ID",
                "Species Mass Start",
                "[Translation missing]",
                "[Translation missing]",
                "[Translation missing]",
                "Survey ID",
                "Survey Name",
                "Survey Site ID",
                "[Translation missing]",
                "Class",
                "Species Common Name",
                "Taxonomy created",
                "Family",
                "Genus",
                "Species ID",
                "Species",
                "Order",
                "Species",
                "Taxonomy updated",
                "GPS Altitude",
                "Trap Station ID",
                "Camelot GPS Latitude",
                "Camelot GPS Longitude",
                "Trap Station Name",
                "Trap Station Session Camera ID",
                "Session End Date",
                "Trap Station Session ID",
                "Session Start Date",
                "Sex",
                "Life stage",
                "Individual name",
                "Remarks",
                "Position",
                "Hair trap",
                "Anomaly",
                "Beauty shot",
                "Marking site",
                "Zoochory"
            ],
            [
                ".local/share/camelot/Media/45/test1.jpg",
                "1514",
                "",
                "",
                "M025",
                "false",
                "false",
                "2020-02-16 03:27:53",
                "2020-05-22 09:21:08",
                "test1.jpg",
                "jpg",
                "60202",
                "true",
                "true",
                "2020-05-22 09:21:08",
                "",
                "2162709/32768000 sec",
                "Flash fired",
                "f/2.8",
                "0.1 mm",
                "700",
                "Top, left side (Horizontal / normal)",
                "3840",
                "2160",
                "2020-06-01 11:25:28",
                "44291",
                "1",
                "2020-07-27 09:56:23",
                "",
                "",
                "",
                "142",
                "LD_Col",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "2019-10-08T09:58:57.286Z",
                "103",
                "LYNX2019_20",
                "242",
                "2019-10-08T09:58:57.286Z",
                "",
                "Lynx lynx",
                "2019-06-14 13:05:56",
                "",
                "Lynx",
                "10",
                "Lynx lynx",
                "",
                "lynx",
                "2019-06-14 13:05:56",
                "",
                "371",
                "45.874500",
                "14.104000",
                "station_012",
                "3381",
                "2020-04-05 00:00:00",
                "2315",
                "2020-01-19 00:00:00",
                "",
                "Adult",
                "Hrusica2",
                "",
                "left",
                "not present",
                "",
                "",
                "no",
                ""
            ]
        ];
    }
}