/**
 * mbase2 settings: modules
 */

import * as exports from '../libs/exports';
import globals from '../app-globals';

export default async op => {

    const {$parent} = op;

    const [
        ComponentManager,
        utils
    ] = await Promise.all([
        exports.ComponentManager(),
        exports.utils()
    ]);
        
///////////////////////////////////////////////////////////////////////////////////////////
        /**
         * double underscore before the key_name_id to prevent mixing with column names obtained from table (if variableAttribute key_name_id is not present in table it gets added to data array)
         */
    
    const variablesAttributes = [
        {
            key_data_type_id: 'text',
            key_name_id: '__row',
            t_name_id: 'row',
        },
        {
            key_data_type_id: 'text',
            key_name_id: '__column',
            t_name_id: 'column',
        },
        {
            key_data_type_id: 'text',
            key_name_id: '__value',
            t_name_id: 'value',
        },
        {
            key_data_type_id: 'text',
            key_name_id: '__variable',
            t_name_id: 'variable'
        },
        {
            key_data_type_id: 'text',
            key_name_id: '__error',
            t_name_id: 'error'
        }
        
    ];

    variablesAttributes.map(a => a.visible_in_table=true);
    
    const tableRecordsOptions = {
        $parent: $parent,
        tableName: 'import_errors',
        url: globals.apiRoot + '/import_errors',
        select: [{key: 'batch_id', value: op.batchId}],
        skipId: true,
        variablesAttributes: variablesAttributes,
        btn_add_record: false,
        btn_batch_import: false,
        disableEdit: true,
        data: op.data,
        preprocessTableData: (data, model) => {
            const ndata = [];
            data.map(d=>{
                const idata = Array.isArray(d.data) ? d.data : (JSON.parse(d.data || null) || []);
                Array.isArray(idata) && idata.map(d=>{
                    d = utils.jsonParse(d);
                    const nd = {};
                    nd.__error = d.error;
                    nd.__variable = d.variable;
                    nd.__value = d.value;
                    nd.__column = d.column;
                    nd.__row = d.row;
                    ndata.push(nd);
                });
                
            });
            
            return ndata;
        }
    }

    tableRecordsOptions.tableOptions = {
        scrollY: '65vh'
    }

    const recordsTable = await import('./recordsTable');
    recordsTable.default(tableRecordsOptions) 
}