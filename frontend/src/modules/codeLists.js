/**
 * mbase2 settings: codeLists
 */

import * as exports from '../libs/exports';
import {t} from '../libs/utils';
import globals from '../app-globals';

export default async op => {

    const {$parent} = op;

    const [
        ComponentManager,
        utils,
        mutils
    ] = await Promise.all([
        exports.ComponentManager(),
        exports.utils(),
        exports.mutils()
    ]);

    const translations = await mutils.getCodeList('code_lists');
    const translationsByKey = utils.arrayToObject(translations, 'key');
    
    const moduleKey = utils.getQueryParameter('m');

    const components = {
        codeLists: exports.select({
            label: t`Code lists`,
            onSelect: onSelectCodeList,
            tname: 'code_lists',
            queryParameters: moduleKey ? [`m=${moduleKey}`] : null,
            process: item => {
                const translation = mutils.getCodeListValue(item.label_key, translationsByKey);
                return {
                    value: item.id, 
                    text: translation === item.label_key ? translation : `${translation} (${item.label_key})`
                }
            },
            $parent: $parent
        })
    };

    const isAdmin = globals.user.roles.find(role => role === 'administrator' || role.includes('-admins')) ? true : false;

    const cm = new ComponentManager.default();
    await utils.loadComponents(components);
    const codeListsSelector = await cm.add(components.codeLists);

    codeListsSelector.$el().css('margin-bottom','10px')

    if (op.codeListKey) {
        const data = cm.getData('codeLists');
        const m = data.find(d => d.label_key == op.codeListKey);
        if (m) {
            codeListsSelector.val(m.id);
        }
    }
    
    const tableCm = new ComponentManager.default();

    async function onSelectCodeList(e) {
        const listId = utils.select2value(e)
        if (!listId) return;

        if (!op.codeListKey) {
            const data = cm.getData('codeLists');
            const listKey = data.find(d => d.id == listId);
            const loc = window.location;      
            window.location = loc.origin + loc.pathname + '/' + listKey + loc.search;
            return;
        }
        
///////////////////////////////////////////////////////////////////////////////////////////
        /** module variables */
        const definitions = (await import('./attributeDefinitions.js')).default(exports, globals);
        const variablesAttributes = await definitions.code_list_options();
        variablesAttributes.map(a => a.visible_in_table =true);
        
        const tableRecordsOptions = {
            $parent: $parent,
            tableName: 'code_list_options',
            url: globals.apiRoot + '/code_list_options',
            select: [{
                key: 'list_id',
                value: listId
            }],
            skipAttributesFromTable: ['list_id'],
            variablesAttributes: variablesAttributes
        }

        tableRecordsOptions.cm = tableCm;

        tableRecordsOptions.onInitRecord = cm => {
            if (cm.model.values) { //edit record
                cm.get('key').$input.prop('disabled', true);
            }   
        }

        tableRecordsOptions.btn_batch_import = false;

        if (!isAdmin) {
            tableRecordsOptions.btn_add_record = false;
        }

        tableRecordsOptions.tableOptions = {
            scrollY: 'calc(100vh - 270px)'
        };

        const recordsTable = await import('./recordsTable');
        recordsTable.default(tableRecordsOptions)

        //const moduleVariables = await cmVariables.add(components.module_variables(selectedModule.id, cm));
        
    } 
}