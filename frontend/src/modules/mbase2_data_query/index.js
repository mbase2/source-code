/**
 * @param {object} op options
 * @param {object} op.$parent
 * @param {string} op.tname
 */

import globals from '../../app-globals';
import * as exports from '../../libs/exports';
import {onMarkerClicked} from '../../helpers/onMarkerClicked';
import * as ctMapDataProcessor from '../mbase2_ct/mapDataProcessor';

import * as moduleSpecificMapDataHandlers from '../../modules/moduleSpecificMapDataHandlers'

export default async op => {
    const {$parent} = op;

    let _query = {};

    const [
        Map,
        Sidebar,
        mutils,
        utils,
        ComponentManager,
        Button,
        Alert,
        DataFilter,
        DateInputs,
        ModalDialog,
        Accordion,
        Select2
    ] = await Promise.all([
        exports.Map(),
        exports.Sidebar(),
        exports.mutils(),
        exports.utils(),
        exports.ComponentManager(),
        exports.Button(),
        exports.Alert(),
        exports.DataFilter(),
        exports.DateInputs(),
        exports.ModalDialog(),
        exports.Accordion(),
        exports.Select2()
    ]);

    await DataFilter.loadExports(exports);

    const t = utils.t;

    //Module variables

    const cachedMapLayers = {};
    const cachedAttributes = {};
    const cachedFilters = {};
    const cachedStats = {};
    let prefilter = {};

    let _urlQuery = {}; //query parameters parsed

    let legendCmp = null;

    ///////Leaflet Map

    const _onMarkerClicked = (marker, rows, moduleKey, callback) => {  
        const moduleAttributes = getCachedAttributes(moduleKey);

        const isGrid = legendCmp.isAccuracySetToGrid(moduleKey);
        
        if (moduleKey === 'ct') {

            if (legendCmp.isAccuracySetToGrid(moduleKey)) {
                rows=[rows[0].data];
            }

            ctMapDataProcessor.onMarkerClicked(marker, rows, {moduleKey, moduleAttributes});
        }
        else if (moduleKey === 'tlm') {
            rows.map(row => row.id = row.tlm_tracks_id);
            onMarkerClicked(marker, rows, {moduleKey, moduleAttributes}, callback);
        }
        else {
            let dataRows = moduleSpecificMapDataHandlers.default(moduleKey, rows, isGrid, moduleAttributes);
            
            if (legendCmp.isAccuracySetToGrid(moduleKey)) {
                dataRows = moduleSpecificMapDataHandlers.groupData(moduleKey, dataRows, moduleAttributes);
            }

            onMarkerClicked(marker, dataRows, {moduleKey, moduleAttributes}, callback);
        }
    }

    const _map = new Map.default({
        $container: $parent,
        onMarkerClicked: _onMarkerClicked,
        centerPopupOnOpen: true,
        scaleControl: true
    });

    const sidebarContainer = new Sidebar.default({id:'leaflet-sidebar',title: t`Data overview`,$container: $parent});
    const sidebar = _map.addSidebar('leaflet-sidebar');

    let modules = await mutils.requestHelper(globals.apiRoot + `/modules/language/${globals.language}`);

    modules = modules.filter(m => m.key_id !== 'mbase2');//.filter(m=>['interventions', 'cnt'].indexOf(m.key_id)===-1);

    modules.map(m => {
        m.color = JSON.parse(m.properties).color;
        m._id = m.id;
        m.id = m.key_id;
    });

    const $sidebarContainer = sidebarContainer.$el().find('#home .container-fluid:first');
    const components = {
        modules: {
            module: exports.EventsLegend,
            options: {
                rows: modules,
                user: globals.user,
                onDataFilter: (moduleName) => {
                    console.log('onDataFilter', moduleName);
                    const accuracy = legendCmp.isAccuracySetToGrid(moduleName) ? 'grid' : 'detail';
                    const filter = cachedFilters[moduleName][accuracy];
                    if (!filter) return;
                    filter.showFilterModal();
                },
                onDataDownload: (moduleName) => {
                    utils.hrefClick(globals.apiRoot+'/general-export/'+moduleName + getAllFiltersString(moduleName, 'detail'), null);
                },
                onDataAccuracy: (moduleName) => {
                    onLayerVisibilityChange(moduleName, true);
                },
                onChange: onLayerVisibilityChange
            },
            $parent: $sidebarContainer
        }
    }

    const cm = new ComponentManager.default({
        onComponentChange: (key, value, cm) => {
            updateQuery();
        }
    });
    await utils.loadComponents(components);
    legendCmp = await cm.add(components.modules);
    $sidebarContainer.append('<br>');

    const $filterDiv = $('<div/>');

    $sidebarContainer.append($filterDiv);

    const acc = new Accordion.default({$parent: $filterDiv, closeOthers: true});    

    const filterPanel = acc.addPanel('<i class="fa fa-filter" aria-hidden="true"></i>&nbsp;&nbsp;' + t`Data overview filter`);

    const individualTracking = acc.addPanel('<i class="fa pficon-trend-up" aria-hidden="true"></i>&nbsp;&nbsp;' + t`Track individual animals`)

    individualTracking.$panel.hide();

    const statsPanel = acc.addPanel('<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;' + t`Selected layers data`);
    
    modules.map(m => {

        const $el = $(mutils.alertCode('', m.t_id));
        const $msg = $el.find('.alert-msg:first');
        cachedStats[m.id] = {
            $el: $el,
            $msg: $msg,
            stats: {}
        }

        $el.hide();
        
        statsPanel.$body.append($el);
    });

    const shareDataPanel = acc.addPanel('<i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp;&nbsp;' + t`Share this data query`)

    // date selector
    await cm.add({
        key: 'event_date',
        component: DateInputs.DateSpan,
        $parent: filterPanel.$body,
        options:{
            labelStartDate:t`Start date`,
            labelEndDate:t`End date`,
            format:'YYYY-MM-DD',
            date1:moment().year(moment().year() - 1).startOf('year'),
            date2: moment().add(1,'d'),
            //label: attributes[key].t_name_id || key,
            required: true,
            //disabled: disabled,
            onChange: (component) => {
                const cmp = cm.get('event_date');
                cmp && cmp.$el().trigger('change')
            }
        }
    });

    const $speciesFilterDiv = $('<div/>',{style: "margin-top:10px"});

    const [speciesList, nutsList] = await mutils.batchRequestHelper([
        'table_reference_values/language/null?:id:code_list_options/key=species_list', 
        'table_reference_values/language/null?:id:code_list_options/key=nuts', 
    ]);

    speciesList.map(item => {
        if (item.translations) {
            item.translations = utils.jsonParse(item.translations);
            item.translations = item.translations[globals.language] || item.key;
        }
    });

    await cm.add({
        key: 'species_list_id',
        component: Select2.default,
        options: {
            data: speciesList.map(item => [item.id, item.translations]),
            badge: t`Species`,
            allowClear: true
        },
        $parent: $speciesFilterDiv});

    filterPanel.$body.append($speciesFilterDiv);
    

    const $spatialUnitsFilterDiv = $('<div/>',{style: "margin-top:10px"});

    const spatialUnitsFilter = await cm.add({
        key: 'spatial_units',
        component: Select2.default,
        options: {
            data: nutsList.map(item => [item.id, item.key]),
            badge: t`Spatial units` + ' (NUTS)',
            allowClear: true
        },
        $parent: $spatialUnitsFilterDiv});

    filterPanel.$body.append($spatialUnitsFilterDiv);

    sidebar.open('home');
    
    const executeQueryBtn = new Button.default({
        label: t`Apply`,
        //classes: 'btn-lg',
        iconClass: 'bolt',
        onClick: () => executeQuery()
    });

    executeQueryBtn.$el().find('button').prop('disabled', true);
    
    const $btnDiv = utils.createRightAlignedButton(executeQueryBtn.$el());
    filterPanel.$body.append($btnDiv);

    _query = cm.val();   //initial value

    let qe = utils.getQueryParameter('qe');

    _urlQuery = qe = qe && JSON.parse(utils.base64ToString(qe));

    if (_urlQuery && _urlQuery.m) {
        _map.map.setView(_urlQuery.m.c, _urlQuery.m.z);
    }

    prefilter = (qe && qe.q) || JSON.parse(utils.getQueryParameter('q')) || {};

    const globalPrefilter = (qe && qe.g) || JSON.parse(utils.getQueryParameter('g')) || {};

    const filteredModules = [];
    Object.keys(prefilter).map(key => {
        filteredModules.push(key);
    });

    globalPrefilter.d1 && cm.get('event_date').date1.val(globalPrefilter.d1);
    globalPrefilter.d2 && cm.get('event_date').date2.val(globalPrefilter.d2);
    globalPrefilter.spatial_units && spatialUnitsFilter.val(globalPrefilter.spatial_units);
    globalPrefilter.species_list_id && cm.get('species_list_id').val(globalPrefilter.species_list_id);

    if (filteredModules.length>0) {
        const legend = cm.get('modules');
        legend.val(filteredModules);
        Object.keys(prefilter).map(key => {
            legend.setDetailAccuracy(key, prefilter[key].g ? false : true);
        });
        
        cm.get('modules').$el().find('input.layer-visibility').trigger('change');
    }

    //
    //cm.get('modules').val(['ct']);
    //cm.get('modules').$el().find('input').trigger('change');
    //

    /**callbacks */
    function onCachedAttributesLoaded(moduleName) {
        executeQueryBtn.$el().find('button').prop('disabled', false);
        executeQuery(moduleName);
    }

    function getCachedAttributes(moduleKey) {
        const accuracy = legendCmp.isAccuracySetToGrid(moduleKey) ? 'grid' : 'detail';

        if (cachedAttributes[moduleKey] && cachedAttributes[moduleKey][accuracy]) {
            return cachedAttributes[moduleKey][accuracy];
        }
        
        return null;
    }

    function getAllFiltersString(m, accuracy = 'detail') {

        const nquery = cm.val();

        const filter = [...(nquery.species_list_id ? [["AND", "species_list_id","=", nquery.species_list_id]]: []), ...nquery.event_date, ...(cachedFilters[m] && cachedFilters[m][accuracy] ? cachedFilters[m][accuracy].val() : [])];
        const __filter = mutils.prepareFilter(filter, false);
        const __sfilter = nquery.spatial_units ? '&:__sfilter=' + JSON.stringify([["", "nuts","=", nquery.spatial_units]]) : '';

        const gparam = `&:__grid=${accuracy == 'detail' ? 0 : 1}`;

        return __filter + __sfilter + gparam;
    }

    async function executeQuery(moduleName = null) {

        const promisses = {};

        const visibleModules = cm.get('modules').val();

        for (const m of visibleModules) {
            
            if (moduleName && moduleName !== m) continue;

            let grid = legendCmp.isAccuracySetToGrid(m);
            if (!mutils.permissions(['admin','editor','reader','consumer'], m) && grid === false) grid = true;

            const accuracy = grid ? 'grid' : 'detail';

            const viewName = m+'_vw';

            let endPoint = `/mb2data/${viewName}/language/${globals.language}`;
            const request = `${endPoint}${getAllFiltersString(m, accuracy)}`;

            if (!cachedStats[m].stats.total) {
                
                if (m==='tlm') {
                    endPoint = `/mb2data/tlm_tracks/language/${globals.language}`;
                }

                mutils.requestHelper(globals.apiRoot + `${endPoint}?:__count=1`).then(res=>{
                    cachedStats[m].stats.total = res[0].count;
                    updateStats(m);
                });
            }

            if (cachedMapLayers[m] && cachedMapLayers[m].request === request) {
                promisses[m] = null;
            }
            else {
                
                if (cachedMapLayers[m] && cachedMapLayers[m].layer) {
                    _map.map.removeLayer(cachedMapLayers[m].layer.group);
                }

                cachedMapLayers[m] = {
                    request
                }

                promisses[m] = mutils.requestHelper(globals.apiRoot + request);
            }
        }

        for(const [moduleName, promise] of Object.entries(promisses)) {
            
            if (!promise) {
                const layer = cachedMapLayers[moduleName].layer;
                _map.map.addLayer(layer.group);
                cachedStats[moduleName].stats.filtered = layer.rawLength;
                onModuleDataShown(moduleName);

            }
            else {
                promise.then(res => {
                    let mapData = res;
                    
                    const moduleAttributes = getCachedAttributes(moduleName);
    
                    if (moduleName === 'ct') {
                        console.log(ctMapDataProcessor)
                        mapData = ctMapDataProcessor.default({
                            result: res,
                            grid: legendCmp.isAccuracySetToGrid('ct') ? 1 : 0,
                            attributes: moduleAttributes.attributes,
                            refValues: moduleAttributes.refValues,
                            fileProperties: moduleAttributes.additional.fileProperties,
                            attributesKeyed: moduleAttributes.attributesKeyed,
                            refValuesKeyed: moduleAttributes.refValuesKeyed
                        });
                    }
    
                    const props = JSON.parse(modules.find(m => m.id === moduleName).properties);
                    
                    const layer = cachedMapLayers[moduleName].layer = moduleName==='tlm' && legendCmp.isAccuracySetToGrid(moduleName) ? _map.addTrackLayer(mapData, moduleName, legendCmp.isAccuracySetToGrid(moduleName), props) : _map.addLayer(mapData, moduleName, legendCmp.isAccuracySetToGrid(moduleName), props);
                    layer.rawLength = res.length;
                    cachedStats[moduleName].stats.filtered = layer.rawLength;
                    onModuleDataShown(moduleName);
                });
            }
        }
    }

    function onModuleDataShown(moduleName) {
        updateDeepLink();
        updateStats(moduleName);

        if (moduleName === 'gensam') {
            if (!legendCmp.isAccuracySetToGrid(moduleName)) {
                const cmp = cm.get('ref_animal_id');
                if (cmp) {
                    cmp.$select.prop('disabled', false);

                    if (_urlQuery && _urlQuery.c && _urlQuery.c.ref_animal_id) {
                        cmp.val(_urlQuery.c.ref_animal_id);
                        delete _urlQuery.c.ref_animal_id;
                    }

                    onIndividualTrackingApplied();
                }
            }
        }
    }
    
    /**utils */

    function updateStats(moduleName) {
        const $el = cachedStats[moduleName].$el;
        const $msg = cachedStats[moduleName].$msg;
        $msg.html(`${utils.t`Features Total`}: ${cachedStats[moduleName].stats.total}<br>
            ${utils.t`Filtered`}: ${cachedStats[moduleName].stats.filtered}`);

        $el.show();
        //const $el = cachedStats[moduleName].cmp;
    }

    function updateQuery() {
        const nquery = cm.val();

        if (_.isEqual(_query, nquery)) {
            
        }
    }
    /*
    const variables = mutils.getModuleVariables(moduleId);
    const tableRecordsOptions = await mutils.generalTableRecordsOptions($parent, tableName, variables);
    tableRecordsOptions.url = globals.apiRoot + `/mb2data/${tableName}/language/${globals.language}`;  
    const recordsTable = await import('./recordsTable');
    await recordsTable.default(tableRecordsOptions);
    */

    //shareData();
    
    function updateDeepLink() {
        const filters = {};
        const globalFilters={};
        const visibleModules = cm.get('modules').val();

        const nquery = cm.val();

        const eventDateCmp = cm.get('event_date');

        globalFilters.d1 = eventDateCmp.date1.val();
        globalFilters.d2 = eventDateCmp.date2.val();

        if (nquery.species_list_id) {
            globalFilters.species_list_id = nquery.species_list_id;
        }

        if (nquery.spatial_units) {
            globalFilters.spatial_units = nquery.spatial_units;
        }

        for (const moduleKey of visibleModules) {
            const isGrid = legendCmp.isAccuracySetToGrid(moduleKey);
            const accuracy = isGrid ? 'grid' : 'detail';

            filters[moduleKey] = {}

            const f = cachedFilters[moduleKey][accuracy].val();

            if (f.length > 0) {
                filters[moduleKey].f = f;
            }

            if (isGrid) {
                filters[moduleKey].g = 1;
            }
        }

        const clientSideActions = {};

        if (visibleModules.indexOf('gensam') !== -1) {
            if (!legendCmp.isAccuracySetToGrid('gensam')) {
                const cmp = cm.get('ref_animal_id');
                if (cmp) {
                    const value = cmp.val();
                    if (value) {
                        clientSideActions['ref_animal_id'] = value;
                    }
                }
            }
        }

        const qe= utils.stringToBase64(
            JSON.stringify(
                {
                    g: globalFilters,
                    q: filters,
                    c: clientSideActions,
                    m: {
                        z: _map.map.getZoom(),
                        c: _map.map.getCenter()
                    }
                })
        );
        
        const link = `${window.location.origin}${window.location.pathname}?qe=${qe}`;

        shareDataPanel.$body.html(`<a href="${link}" target="_blank">${t`Right click here and copy link to the clipboard or left click here and the link will open in new tab.`}</a>`);

        /*
        const button = new Button.default({
            label: t`Copy link`,
            classes: 'btn-lg',
            iconClass: 'copy',
        });

        const $btnDiv = $('<div/>', {style: 'display: flex; float: right; margin-top:20px'});

        shareDataPanel.$body.append($btnDiv);

        $btnDiv.html(button.$el());
        */
    }

    function statsVisibility(moduleName, isVisible) {
        const stats = cachedStats[moduleName];
        if (stats && stats.$el) {
            isVisible ? stats.$el.show() : stats.$el.hide();
        }
    }

    function onIndividualTrackingApplied() {
        const layers = cachedMapLayers['gensam'];
        const cmp = cm.get('ref_animal_id');
        if (!layers || !cmp) return;

        const value = cmp.val();
        const layer = layers.layer;

        cmp._$msg.empty();
        cmp._$msg.hide();

        if (value) {
            const markers = layer.markers.filter(m => m.__data[0].ref_animal_id == value);
            if (markers.length == 0) {
                cmp._$msg.show();
                cmp._$msg.html(mutils.alertCode(t`The samples could have been collected outside the selected date span. Try changing data overview filter parameters.`,t`No samples are found for the selected reference animal ID.`,'warning'))
                cmp._$msg[0].scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'nearest' });
                return;
            }

            markers.sort((a,b) => utils.compare(a.__data[0], b.__data[0], 'event_date'));

            layer.group.remove();

            if (cmp._group) {
                cmp._group.remove();
                delete cmp._group; 
            }

            const group = L.featureGroup();
            const pointList = [];

            markers.map(m => {
                group.addLayer(m);
                m.on('click',() => _onMarkerClicked(m, m.__data, 'gensam'));
                pointList.push(m.getLatLng());
            });

            if (markers.length > 1) {

                const svgCircle = (color='#9ACD32') => `<svg viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="15" cy="15" r="15" fill="${color}"/>
                </svg>`;

                [[0, '#9ACD32'], [markers.length - 1, '#FF4500']].map(([inx,color]) => {
                    const m = markers[inx];
                    const svgIcon = L.divIcon({
                        html: svgCircle(color),
                        className: "",
                        iconSize: [15, 15],
                        iconAnchor: [7, 34],
                    });
        
                    const proxyMarker = L.marker(m.getLatLng(),{
                        icon:svgIcon
                    });
                    
                    group.addLayer(proxyMarker);

                    proxyMarker.on('click',() => m.fire('click'));
                });

                //markers[0].setIcon(svgIcon);

                group.addLayer(new L.Polyline(pointList, {
                    color: 'green',
                    weight: 3,
                    opacity: 1.0,
                    smoothFactor: 1
                }));
            }

            group.addTo(_map.map)
            
            cmp._group = group;

            _map.map.fitBounds(group.getBounds());
        }
        else {
            
            if (cmp._group) {
                cmp._group.remove();
                delete cmp._group; 
            }

            layer.group.addTo(_map.map);
        }

        updateDeepLink();
    }

    async function handleIndividualTrackingVisibility(selectedModuleName, accuracy, show) {
        
        if (selectedModuleName == 'gensam') {
            if (show) {
                if (accuracy === 'detail') {
                    let cmp = cm.get('ref_animal_id');

                    if (!cmp) {
                        const a = getCachedAttributes('gensam');
                        const label = a.attributesKeyed.ref_animal_id.t_name_id;
                        const refAnimalList = a.refValues.tableReferences.filter(item=>item.list_key=='gensam_ref_animals');
                        cmp = await cm.add({
                            key: 'ref_animal_id',
                            component: Select2.default,
                            options: {
                                label: label,
                                data: refAnimalList.map(item => [item.id, item.key]),
                                allowClear: true
                            },
                            $parent: individualTracking.$body});
                        
                        const btn = new Button.default({
                            label: t`Apply`,
                            //classes: 'btn-lg',
                            iconClass: 'bolt',
                            onClick: () => onIndividualTrackingApplied()
                        });

                        const $btnDiv = utils.createRightAlignedButton(btn.$el());
                        filterPanel.$body.append($btnDiv);
                        
                        individualTracking.$body.append($btnDiv);
                    }

                    cmp.$select.prop('disabled', true);

                    const $div = $('<div/>',{style:'padding:5px'});

                    $div.hide();

                    cmp._$msg = $div;

                    individualTracking.$panel.append($div);

                    individualTracking.$panel.show();
                }
                else {
                    individualTracking.$panel.hide();
                }
            }
            else {
                individualTracking.$panel.hide();
            }
        }
    }

    async function onLayerVisibilityChange(moduleName, show) {
        
        statsVisibility(moduleName, show);
        const accuracy = legendCmp.isAccuracySetToGrid(moduleName) ? 'grid' : 'detail';

        if (show === true) {

            if (!getCachedAttributes(moduleName)) {
                executeQueryBtn.$el().find('button').prop('disabled', true);

                let additional = {};

                const visibleKey = 'visible_in_cv_' + accuracy;

                const batchImportModuleName = mutils.batchImportModuleName(moduleName);

                const allModuleVariables = await mutils.getModuleVariables(batchImportModuleName, true, false, true);

                const attributes = allModuleVariables.filter(a => a[visibleKey]);

                const additionalFilters = allModuleVariables.filter(a => accuracy === 'detail' && !a[visibleKey] && a.filterable );

                if (accuracy === 'grid') {
                    const eventCountAttributeDefinition = mutils.createVariableDefinition('_event_count',t`Events count`,'integer');
                    eventCountAttributeDefinition.weight_in_popup_cv = -1;
                    eventCountAttributeDefinition.visible_in_cv_grid = true;
                    attributes.push(eventCountAttributeDefinition);
                }

                if (moduleName === 'ct') {
                    additional.fileProperties = await mutils.requestHelper(globals.apiRoot + `/mb2data/ct_view_file_properties`);
                }

                const attributesWithAdditionalFilters = [...attributes, ...additionalFilters];
                
                const refValues = await mutils.getRefCodeListValues(attributesWithAdditionalFilters);
                const attributesKeyed = utils.arrayToObject(attributesWithAdditionalFilters, 'key_name_id');
                const refValuesKeyed = await mutils.convertReferencesToAssocArray(refValues);

                if (!cachedAttributes[moduleName]) cachedAttributes[moduleName] = {};

                cachedAttributes[moduleName][accuracy] = {attributes, refValues, attributesKeyed, refValuesKeyed, additional};

                if (!cachedFilters[moduleName]) cachedFilters[moduleName] = {};

                cachedFilters[moduleName][accuracy] = new DataFilter.default({
                        attributesDefinition: attributesWithAdditionalFilters.filter(a=>a.filterable === true),
                        refValues: refValues,
                        apply: {
                            button:{label: t`Apply`},
                            callback: () => executeQuery(moduleName)
                        }
                });

                if (prefilter[moduleName]) {
                    const pfa = prefilter[moduleName].g ? 'grid' : 'detail';
                    if (pfa === accuracy && prefilter[moduleName].f) {
                        cachedFilters[moduleName][accuracy].val(prefilter[moduleName].f);
                    }
                }
            }

            onCachedAttributesLoaded(moduleName);
        }
        else {
            cachedMapLayers[moduleName] && _map.map.removeLayer(cachedMapLayers[moduleName].layer.group);
        }

        handleIndividualTrackingVisibility(moduleName, accuracy, show)
    }

}
