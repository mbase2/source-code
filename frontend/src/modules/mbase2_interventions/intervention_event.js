import * as exports from '../../libs/exports';
import globals from '../../app-globals';

/**
 * @param {object} op options
 * @param {object} op.$parent
 * @param {string} op.tname
 */
export default async op => {
    const [
        utils,
        mutils,
        Mbase2Module,
        Button,
        SimpleTable,
        ComponentManager,
        ModalDialog,
        SelectWithSpecify,
        Storage,
        InterventionMeasures
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        exports.Mbase2Module(),
        exports.Button(),
        exports.SimpleTable(),
        exports.ComponentManager(),
        exports.ModalDialog(),
        exports.SelectWithSpecify(),
        exports.Storage(),
        import('./InterventionMeasures')
    ]);

    const t = globals.t;

    let table = null;

    let moduleVariablesKeyed=null, refValuesKeyed=null;

    const m = new Mbase2Module.default({
        addHeader: false,
        ignoreQueryFid: true,
        moduleKey: 'interventions',
        tableName: 'interventions_events',
        viewName: 'interventions_events',
        title: t`Intervencije`,
        subtitle: {
            edit: t`urejanje intervencije`, 
            new: t`nova intervencija`
        },
        $parent: op.$parent,
        skipVariables: ['_licence_name', '_batch_id','_species_name', '_uname', 'event_date'],
        onBeforeRequest: (key, request, cm) => {
            if (key !== 'save') return;
            request.attributes[':_uid'] = true;
            mutils.handleSelectWithSpecifyComponentData(request, cm);
        },
        sections: [
            {
                container: null,
                variables: ['interventions_id','_data'],
            },
            {
                title: t`Klic`,
                variables: ['intervention_caller', 'intervention_call_timestamp']
            },
            {
                title: t`Vzrok intervencije`,
                variables: ['intervention_reason', 'situation_notes']
            },
            {
                title: t`Intervencijski ukrepi`,
                variables: ['intervention_measures']
            },
            {
                title: t`Čas začetka in konca intervencije`,
                variables: ['intervention_start_timestamp', 'intervention_end_timestamp']
            },
            {
                title: t`Izid intervencije`,
                variables: ['intervention_outcome']
            },
            {
                title: t`Intervencijska skupina`,
                variables: ['chief_interventor', 'interventors'],
                overrides: {
                    _interventors: {
                        onSelect: e => {
                            if (!table || !e) return;
                        }
                    }
                }
            },
            {
                title: t`Podrobnosti intervencijskega dogodka`,
                variables: ['intervention_details']
            },
            {
                title: t`Notes`,
                variables: ['notes']
            },

        ],
        buttons: [
            {
                key: 'save',
                label: t`Shrani`
            },
            {
                key: 'add',
                label: t`Nov dogodek`
            }
        ],
        componentDefinitionOverrides: {
            intervention_caller: {
                component: SelectWithSpecify,
                defaultOptions: true
            },
            intervention_reason: {
                component: SelectWithSpecify,
                defaultOptions: true
            },
            intervention_measures: {
                component: InterventionMeasures,
                defaultOptions: true,
                additionalComponentOptions:{
                    onSelect: value => onInterventionMeasuresTypeSelect(value)
                }
            },
            intervention_outcome: {
                component: SelectWithSpecify,
                defaultOptions: true
            },
            interventions_id: {
                _component: {
                    import: () => Storage
                }
            },
            _data: {
                _component: {
                    import: () => Storage
                }
            },
            intervention_call_timestamp: {
                additionalComponentOptions:{
                    format:'dd.mm.yyyy',
                    onChange: (value, valueDate, valueTime) => {
                        onTimeStampChanged(value, valueDate, valueTime, 'intervention_start_timestamp','intervention_call_timestamp');
                        const cm = m.cm;
                        const cmp = cm.get('intervention_measures');
                        if (!cmp) return;
                        onInterventionMeasuresTypeSelect(cmp.select.val());
                    }
                }
            },
            intervention_end_timestamp: {
                additionalComponentOptions:{
                    format:'dd.mm.yyyy'
                }
            },
            intervention_start_timestamp: {
                additionalComponentOptions:{
                    format:'dd.mm.yyyy',
                    onChange: (value, valueDate, valueTime) => {
                        onTimeStampChanged(value, valueDate, valueTime, 'intervention_end_timestamp','intervention_start_timestamp');
                    }
                }
            }
        },
        componentOverrides: {
            intervention_details: intervention_details
        },
        initialData: op.initialData,
        moduleVariables: op.moduleVariables,
        refValues: op.refValues,
        onSaveSuccessCallback: (res) => {
            op.$bm.html(processInterventionEventResult(res));
            op.onSaveSuccessCallback && op.onSaveSuccessCallback(res);
        },
        onSectionsInitialized: () => {
            
            validateSectionStatus();
            op.onSectionsInitialized && op.onSectionsInitialized();
        },
        onComponentChange: (key, value, cm) => {
            
            validateSectionStatus();
            op.onComponentChange && op.onComponentChange(key, value, cm);
        }
    });

    function validateSectionStatus() {
        if (m.sectionsValidated()) {
            op.$bm.addClass('bookmark-completed');
        }
        else {
            op.$bm.removeClass('bookmark-completed');
        }
    }

    await m.init();

    const moduleVariables = m.variables;
    const refValues = m.refValues;

    refValuesKeyed = await mutils.convertReferencesToAssocArray(refValues);
    moduleVariablesKeyed = utils.convertToAssocArray(moduleVariables, 'key_name_id');

    let state = m.cm.val();

    function onTimeStampChanged(value, valueDate, valueTime, nextTimeStampComponentKey, thisTimeStampComponentKey) {
        const cm = m.cm;
        const nextTimeStampComponent = cm.get(nextTimeStampComponentKey);

        if (!nextTimeStampComponent) return;

        const startValue = nextTimeStampComponent.val()
        if (startValue && startValue < value) {
            nextTimeStampComponent.val(value);
            const anext = cm.model.attributes[nextTimeStampComponentKey];
            const athis = cm.model.attributes[thisTimeStampComponentKey];
            $.toaster({ message : `Vrednost polja "${anext.t_name_id || anext.key_name_id}" je bila avtomatsko nastavljena na vrednost polja "${athis.t_name_id || athis.key_name_id}".` , priority: 'warning' });
        }

        nextTimeStampComponent.dp.dp.setStartDate(valueDate);
    }

    function onInterventionMeasuresTypeSelect(value) {   
        if (!value) return;

        const cm = m.cm;
        const interventionStartCmp = cm.get('intervention_start_timestamp');
        const interventionEndCmp = cm.get('intervention_end_timestamp');

        if (!interventionStartCmp) return;
        if (!interventionEndCmp) return;

        let disabled = false;

        if (value==-1 || (value.params && value.params.data)) { //če je izbran "Ukrepi na terenu" ali če je izbral enega od ukrepov na terenu
            disabled = false;
        }
        else {
            disabled = true;
            const interventionCallCmp = cm.get('intervention_call_timestamp');
            interventionStartCmp.val(interventionCallCmp.val());
            interventionEndCmp.val(interventionCallCmp.val());
        }

        interventionStartCmp.dp.$input.prop('disabled', disabled);
        interventionStartCmp.tp.$input.prop('disabled', disabled);
        interventionEndCmp.dp.$input.prop('disabled', disabled);
        interventionEndCmp.tp.$input.prop('disabled', disabled);
    }

    async function intervention_details($container, cm, key) {

        const attributes = [
            mutils.createVariableDefinition('_interventor',t`Član interv. skupine`,'table_reference',m.refValues.trefs.find(tref => tref.key==='interventions_view_interventors').id),
            mutils.createVariableDefinition('_measure',t`Intervencijski ukrep`,'code_list_reference',m.refValues.codeLists.find(cl => cl.key==='intervention_measure_options').id),
            mutils.createVariableDefinition('_intervention_action_date',t`Datum`,'date'),
            mutils.createVariableDefinition('_elapsed_time',t`Porabljen čas (h)`,'integer'),
            mutils.createVariableDefinition('_km_job',t`Kilometrina službeno vozilo (km)`,'integer'),
            mutils.createVariableDefinition('_km_private',t`Kilometrina privatno vozilo (km)`,'integer')
        ];

        const button = new Button.default({
            label: t`Dodaj`,
            onClick: async () => {
              const cmr = new ComponentManager.default();
              const record = await exports.record();
              cmr.model.attributes = attributes;
  
              const modal = new ModalDialog.default();
              record.default({
                  refValues: m.refValues,
                  $parent: modal,
                  cm: cmr,
                  saveOptions: {
                      beforeRequest: requestParameters => {
                          
                          if(!table) return;
                          
                          const attributes = requestParameters.attributes;
                          const row = {};
                          Object.keys(attributes).map(key => {
                              row[key.substring(1)] = attributes[key];
                          });
  
                          table.add(row);
                          modal.hide();
                          return false;
                      }
                  }
              });
              modal.show();
            }
          });
  
          $container.append(button.$el());
          $container.append('<br>');

        table = await cm.add({
            key: 'intervention_event_details',
            component: SimpleTable.default,
            options: {
                attributes: attributes,
                language: globals.language,
                tableReferences: utils.convertToAssocArray(m.refValues.tableReferences, '_id_list_id'),
                codeListValues: utils.convertToAssocArray(m.refValues.codeListValues, 'id')
            },
            $parent: $container
        });

    }

    function processInterventionEventResult(res, skipEmpty = true, delimiter='<br>', keys=[
        'id', 
        '_uid', 
        'intervention_caller',
        'intervention_call_timestamp',
        'intervention_start_timestamp',
        'intervention_end_timestamp',
        'intervention_reason',
        'intervention_outcome',
        'chief_interventor',
        'notes'
    ]) {
        const clonedRes = Object.assign({}, res);
        
        if (skipEmpty === true) {
            Object.keys(clonedRes).map(key => {
                const value = clonedRes[key];
                if (!value) delete clonedRes[key];
            });
        }

        mutils.processAttributeValues(clonedRes, moduleVariablesKeyed, refValuesKeyed);
        return mutils.attributeValuesToHtml(clonedRes, moduleVariablesKeyed, delimiter, keys);
    }
    

    function save(interventionId) {
        m.cm.get('interventions_id').val(interventionId);
        const nstate = m.cm.val();
        
        if (!_.isEqual(state, nstate)) {
            state = nstate;
            return m.actionHandler('save');
        }
    }

    return Object.freeze({
        save,
        moduleVariables,
        refValues,
        processInterventionEventResult,
        cm: m.cm,
        moduleVariablesKeyed,
        refValuesKeyed,
        m
    });
}