import { Select2, SelectWithSpecify } from "../../libs/exports";

import globals from '../../app-globals'

const InterventionMeasures = function(op={}) {
    const $el = $('<div/>');
    this.$el = () => $el;

    this.op = op;
    
    const $selectDiv = $('<div/>', {style: 'width: 100%'});
    const $selectWithSpecifyDiv = $('<div/>', {style: 'width: 100%'});

    $el.append($selectDiv);
    $el.append($selectWithSpecifyDiv);

    const selectOptions = Object.assign({}, op);

    this.fixedOptions = ["No intervention needed", "Phone-call conversation"];

    selectOptions.multiple = false;
    selectOptions.data = selectOptions.data.filter(d => this.fixedOptions.indexOf(d[2])!==-1);
    this.fixedCodes = selectOptions.data.map(d => d[0]);
    selectOptions.data.push([-1, globals.t`Ukrepi na terenu`, "Ukrepi na terenu"]);

    selectOptions.onSelect = () => {
        if (!this.select) return;

        const value = this.select.val();

        if (!value) return;

        if (this.fixedCodes.findIndex(c => c==value) === -1) {
            this.selectWithSpecify.$el().show();
        }
        else {
            this.selectWithSpecify.$el().hide();
        }

        op.onSelect && op.onSelect(value);
    }

    this.select = new op.imports.Select2.default(selectOptions);

    const selectWithSpecifyOptions = Object.assign({}, op);
    
    selectWithSpecifyOptions.imports = op.imports.SelectWithSpecify.imports;

    selectWithSpecifyOptions.label = globals.t`Ukrepi na terenu`;

    selectWithSpecifyOptions.data = selectWithSpecifyOptions.data.filter(d => this.fixedOptions.indexOf(d[2])===-1);
    
    this.selectWithSpecify = new op.imports.SelectWithSpecify.default(selectWithSpecifyOptions);

    this.selectWithSpecify.$el().hide();

    $selectDiv.html(this.select.$el());
    $selectWithSpecifyDiv.html(this.selectWithSpecify.$el());
}

InterventionMeasures.prototype.val = function (value) {
    if (value!==undefined) {
        if (Array.isArray(value) && value.length===1) {
            if (this.fixedCodes.findIndex(c => c == value[0])!==-1) {
                this.select.val(value[0]);
            }
            else {
                this.select.val(-1);
            }
        }
        else {
            this.select.val(-1);
        }
        
        this.selectWithSpecify.val(value);
        return;
    }

    const selectedFixedValue = this.select.val();

    if (selectedFixedValue == -1) {
        return this.selectWithSpecify.val();
    }
    else {
        return [selectedFixedValue];
    }
}

InterventionMeasures.imports = async function () {
    const [
        _Select2,
        _SelectWithSpecify
    ] = await Promise.all([
        Select2(),
        SelectWithSpecify()
    ]);


    _SelectWithSpecify.imports = await _SelectWithSpecify.default.imports();

    return {
        Select2: _Select2, 
        SelectWithSpecify: _SelectWithSpecify
    };
}

export default InterventionMeasures;