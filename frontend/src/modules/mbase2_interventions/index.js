import * as exports from '../../libs/exports';
import globals from '../../app-globals';

export default async op => {
    const [
        utils,
        mutils,
        Mbase2ModuleIndex,
        DropDown
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        import('../Mbase2ModuleIndex'),
        exports.DropDown()
    ]);

    const t = utils.t;

    let moduleIndex;

    const isAdmin = globals.user.roles.find(role => role === 'administrator' || role === 'mbase2-interventions-admins') ? true : false;

    const moduleIndexOptions = Object.assign({
        adminButton: {
            key: 'admin',
            classes: 'btn-lg',
            component: DropDown,
            iconClass: 'cog',
            items: [
                {
                    key: 'mbase2/admin/modules/interventions',
                    label: t`Module settings`
                },
                {
                    key: 'mbase2/admin/code-lists?m=interventions',
                    label: t`Code lists`
                }
            ],
            label: globals.t`Admin settings`,
            onClick: ({key}) => {
                    //window.location=window.location.origin + '/' + key;
                    window.open(window.location.origin + '/' + key, '_blank');
                    return false;
                }     
        },
        buttons: [
            {
                key: 'export',
                label: t`Izvoz podatkov`,
                onClick: () => {
                    utils.hrefClick(globals.apiRoot+'/export/interventions', null);
                }
            },
            {
                key: 'new',
                label: t`Nova intervencija`,
                iconClass: 'plus',
                path:'/intervention'
            }
        ],
        deletable: true,    //table will have trash can icon if row.__deletable===true
        recordTimeStamps: true,
        moduleKey:'interventions',
        urlForDelete: globals.apiRoot + '/mb2data/interventions',
        title: t`Pregled intervencij`,
        viewName:'interventions_vw',
        skipVariables: ['_data','_licence_name','event_date','_location','_batch_id', '_location_data'],
        hideColumnsFromTable: [],
        singleRecordPath: '/intervention',
        sortVariables: true,
        splitRequiredAndOptionalAttributesWhenSorting: false,
        batchOptionsModel: async (attributes) => ({
            interventions: [],
            interventions_batch_imports: await mutils.getModuleVariables('interventions_batch_imports', false)
        }),
        preprocessTableData: data => {
            data.map(row => {
                const completedLabel = row._completed ? t`ZAKLJUČENA` : t`V TEKU`;
                row._completed = `<span class="label label-${row._completed?'success':'danger'}">${completedLabel}</span>`;

                row.__deletable = isAdmin || globals.user.uid == row._uname;

                const interventionMeasures = [];
                row.intervention_measures && row.intervention_measures.map(m => {
                    m && m.map(m=>interventionMeasures.push(m));
                });

                if (interventionMeasures.length > 0) {
                    row.intervention_measures = [...new Set(interventionMeasures)];
                }

                row._location_data = utils.jsonParse(row._location_data);
                
                if (row._location_data && row._location_data._location && row._location_data._location.spatial_request_result) {
                    const spatialRequestResult = row._location_data._location.spatial_request_result;
                    ['oe_ime','lov_ime','luo_ime'].map(key => {
                        row[key] = spatialRequestResult[key];
                    });
                }
                
                
            });
        },
        additionalVariables: async (moduleVariables) => {
            
            let variables = await mutils.getModuleVariables('interventions_view', false);

            variables = [...moduleVariables, ...variables.filter(v => v.key_module_id !== "__default_module")];

            variables.map((v,inx) => {
                if (v.key_name_id === '_uname') {
                    v.weight = 10000;
                }
                else if (v.key_name_id === 'date_record_created') {
                    v.weight = 10001;
                }
                else if (v.key_name_id === 'date_record_modified') {
                    v.weight = 10002;
                }
                else if (v.key_name_id === 'notes') {
                    v.weight = 10003;
                }
                else if (v.key_name_id === '_completed') {
                    v.weight = -1;
                }
                else {
                    v.weight = inx;
                }
            });

            variables.push(Object.assign(mutils.createVariableDefinition('oe_ime',t`Območna enota`,'text'), {weight:1.1}));
            variables.push(Object.assign(mutils.createVariableDefinition('lov_ime',t`Lovišče`,'text'), {weight:1.2}));
            variables.push(Object.assign(mutils.createVariableDefinition('luo_ime',t`LUO`,'text'), {weight:1.3}));

            return variables;
        }
    }, op);

    moduleIndex = new Mbase2ModuleIndex.default(moduleIndexOptions);
    await moduleIndex.init();
}