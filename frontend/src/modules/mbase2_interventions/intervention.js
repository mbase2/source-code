import * as exports from '../../libs/exports';
import globals from '../../app-globals';

import interventionEvent from './intervention_event';

window.addEventListener('popstate', (event) => {
    //event.state && event.state.id && parseInt(event.state.id) > 0 && window.location.reload();
    window.location.reload();
});

/**
 * @param {object} op options
 * @param {object} op.$parent
 * @param {string} op.tname
 */
export default async op => {
    const [
        utils,
        mutils,
        Mbase2Module,
        Button,
        SimpleTable,
        ComponentManager,
        ModalDialog
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        exports.Mbase2Module(),
        exports.Button(),
        exports.SimpleTable(),
        exports.ComponentManager(),
        exports.ModalDialog()
    ]);

    const t = utils.t;

    let table = null;

    let interventionEventModuleVariables = null;
    let interventionEventRefValues = null;

    const interventionEvents = [];

    const m = new Mbase2Module.default({
        moduleKey: 'interventions',
        tableName: 'interventions',
        viewName: 'interventions',
        title: t`Intervencije`,
        subtitle: {
            edit: t`urejanje intervencije`, 
            new: t`nova intervencija`
        },
        $parent: op.$parent,
        skipVariables: ['_location','_data', '_licence_name', '_batch_id', '_uname', 'event_date','date_record_created', 'date_record_modified', '_completed'],
        onBeforeRequest: (key, request, cm) => {
            if (key !== 'save') return;
            
            request.attributes[':_uname'] = true;

            mutils.handleSelectWithSpecifyComponentData(request, cm);

            request.attributes[':_completed'] = isInterventionCompleted();
        },
        //sections: [],
        buttons: [
            {
                key: 'add',
                iconClass:'plus',
                onClick: () => {
                    addInterventionEvent();
                },
                label: t`Nov intervencijski dogodek`
            },
            {
                key: 'save',
                label: t`Shrani`,
            }
        ],
        onSaveSuccessCallback: async (res) => {
            for (const ev of interventionEvents) {
                const result = await ev.dataObject.save(res.id);
                ev.initialData = result;
            }
        },
        componentOverrides: {
            
        },
        componentDefinitionOverrides: {
            _location_data: {
                additionalComponentOptions: {
                    defaultSpatialRequest: {
                        apiRoot: globals.apiRoot
                    },
                    distanceFromSettlement: true,
                    localName: true,
                    _locations_table: "interventions"
                }
            }
        }
    });

    await m.init();

    m._initialData && m._initialData.id && fetchInterventionEvents(m._initialData.id);

    const interventionEventsSection = await m.addSectionContainer(t`Intervencijski dogodki`, '_intervention_events', null, {
        title: t`Intervencijski dogodki`,
        required: true
    });

    async function fetchInterventionEvents(interventionId) {
        if (!interventionId) return;
        const res = await mutils.requestHelper(globals.apiRoot + `/mb2data/interventions_events?:interventions_id=${interventionId}`);
        for (const r of res) {
            await addInterventionEvent(r);
        }
        updateInterventionData();
    }

    function isInterventionCompleted() {
        let completed = true;

        for (const ev of interventionEvents) {
            
            if (!ev.dataObject.m.sectionsValidated()) {
                completed = false;
                break;
            }
        }

        const interventionEventsKeys = Object.keys(m.bookmarks).filter(key => key.startsWith('_intervention_event'));

        return m.sectionsValidated(interventionEventsKeys) && completed ? true : 0;
    }

    async function addInterventionEvent(initialData = null) {

        const inx = interventionEvents.length+1;

        const interventionEventSection = await m.addSectionContainer(initialData ? '' : `<b>${t`Začasni ID`}:</b> ${1000+inx}`, '_intervention_event_'+inx, interventionEventsSection.$container, {
            content: !initialData && `<b>${t`Začasni ID`}:</b> ${1000+inx}<br><b>${t`Uporabnik`}:</b> ${globals.user.name}`,
            required: true,
            $parent: interventionEventsSection.$bm,
            delayScroll: true,
            $scrollContainer: m.$right
        }, {
            onPanelRemove: async ($pdiv, $el) => {
                if (!confirm(t`Ali res želiš izbrisati izbrani dogodek?`)) return;

                const id = (initialData && initialData.id) || (interventionEventSection && interventionEventSection.initialData && interventionEventSection.initialData.id);
                
                if (id) {
                    const callbacks = await mutils.assignRequestCallbackToasters({});
                    const res = await mutils.requestHelper(globals.apiRoot + '/mb2data/interventions_events/' + id, 'DELETE', null, callbacks);
                    if (res===false) return;
                }

                interventionEventSection.$bm.remove();
                $el.remove();
                
                const spliceInx = interventionEvents.findIndex(ie => ie.inx === interventionEventSection.inx);
                
                interventionEvents.splice(spliceInx, 1);
                onInterventionEventChange();
                updateInterventionData();

                const interventionCompleted = isInterventionCompleted();

                if (m._initialData._completed != interventionCompleted) {
                    await mutils.requestHelper(globals.apiRoot + '/mb2data/interventions/' + m._initialData.id, 'PUT', {':_completed':interventionCompleted});
                }
            }
        });

        interventionEventSection.inx = interventionEvents.length;

        interventionEventSection.initialData = initialData;

        const $sectionHeaderLink = interventionEventSection.$container.parent().parent().find('.panel-title:first a');

        interventionEvents.push(interventionEventSection);
    
        interventionEventSection.dataObject = await interventionEvent({
            $parent: interventionEventSection.$container,
            initialData: initialData,
            moduleVariables: interventionEventModuleVariables,
            refValues: interventionEventRefValues,
            $bm: interventionEventSection.$bm,
            onSaveSuccessCallback: res => {
                updateSectionHeader(res);
                updateInterventionData();
            },
            onComponentChange: () => {
                onInterventionEventChange();
            },
            onSectionsInitialized: () => {
                onInterventionEventChange();
            }
        });

        function onInterventionEventChange() {
            if (interventionEventsSection.$bm.find('.bookmark-completed').length === interventionEvents.length) {
                interventionEventsSection.$bm.addClass('bookmark-completed');
            }
            else {
                interventionEventsSection.$bm.removeClass('bookmark-completed');
            }
        }

        interventionEventModuleVariables = interventionEventModuleVariables || interventionEventSection.dataObject.moduleVariables;
        interventionEventRefValues = interventionEventRefValues || interventionEventSection.dataObject.refValues;

        initialData && interventionEventSection.$bm.html(interventionEventSection.dataObject.processInterventionEventResult(initialData));

        initialData && updateSectionHeader(initialData);
        
        function updateSectionHeader(res) {
            $sectionHeaderLink.html(
                interventionEventSection.dataObject.processInterventionEventResult(res, true, '<br>', ['id', 'intervention_reason', 'intervention_call_timestamp','intervention_start_timestamp','intervention_end_timestamp'])
            );
        }
    }

    function updateInterventionData() {
        if (interventionEvents.length === 0) return;

        const keys = ['intervention_caller', 'intervention_call_timestamp', 'intervention_start_timestamp', 'intervention_end_timestamp', 'intervention_reason', 'intervention_measures', 'intervention_measures', 'chief_interventor','interventors']
        
        const evals = {};
        for (const key of keys) {
            evals[key] = [];
        }

        let numberOfUnfinishedInterventions = 0;
        
        interventionEvents.map(ev => {
            const values = ev.dataObject.cm.val()
            for (const key of keys) {
                let value = values[key];
                if (!value) {
                    if (key === 'intervention_end_timestamp') {
                        numberOfUnfinishedInterventions++;
                    }
                    continue;
                }

                if (value.select) { //select with specify
                    value = value.select;
                }

                evals[key].push(value);
            }
        });

        const {moduleVariablesKeyed, refValuesKeyed} = interventionEvents[0].dataObject;

        const summary = {};
        for (const key of keys) {
            summary[key] = [];
        }

        Object.keys(evals).map(key=>{

            if (key === 'intervention_measures') {
                const measures = [];
                evals[key] = evals[key].map(_measures => measures.push(..._measures));
                evals[key] = measures;
            }

            evals[key] = [...new Set(evals[key])];  //unique values
            
            for (const value of evals[key]) {
                const o = {[key]: value};
                mutils.processAttributeValues(o, moduleVariablesKeyed, refValuesKeyed);
                summary[key].push(o['t_'+key] || o[key] );
            }
        });
        
        summary.intervention_call_timestamp.sort();

        m.$status.html(`<b>${t`Čas prvega klica`}:</b> ${summary.intervention_call_timestamp[0]}, <b>${t`Čas zadnjega klica`}:</b> ${utils.lastItem(summary.intervention_call_timestamp)}`);
        m.$status.append('<br>');
        m.$status.append(`<b>${t`Čas prvega odziva`}:</b> ${summary.intervention_start_timestamp[0]}`);
        m.$status.append('<br>');
        m.$status.append(`<b>${t`Število odzivov brez določenega časa konca odziva`}:</b> ${numberOfUnfinishedInterventions}`);
    }
}