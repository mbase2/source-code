const promises = {
    cm: import('../../ComponentManager'),
    inputs: import('../../components/Inputs'),
    utils: import('../utils'),
    acc: import('../../components/Accordion'),
    select: import('../../components/Tselect2'),
    mutils: import('../../libs/mbase2_utils')
};

import {t} from '../libs/utils';

export default async (data, $container, update = true) => {

    const acc = (await promises.acc).default(
        {
            id: 'acc1',
            closeOthers: false,
            panels: [
                t`Translation`
            ]
        }
    );

    $container.append(acc.$panelGroup);
    
    const utils = (await promises.utils);
    const cm = new (await promises.cm).default();
    const batch = [
        'code_list_options?:list_key=languages'
    ];

    const batchData = await utils.batchRequestHelper(batch);

    const codeList = batchData[0];

    cm.add('translation', (await promises.inputs).default, 
        {
            inputs: () => {
                
                return codeList.map(codeListItem => ({label: codeListItem.key, id: codeListItem.key}));
            }
        },
        acc.panels[0].$body
    );

    cm.get('translation').val(JSON.parse(data.values));

    return cm;

}
