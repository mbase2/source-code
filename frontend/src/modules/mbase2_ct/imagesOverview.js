import {MediaScroller} from '../../libs/exports';
import globals from '../../app-globals';
import {t} from '../../libs/utils'
import * as mutils from '../../libs/mbase2_utils';
import * as Map from '../../libs/components/Map';
import * as ModalDialog from '../../libs/components/ModalDialog';

export default async (op) => {

        const $container = $('<div/>');

        let imageMap = null;

        const {attributesKeyed, refValuesKeyed} = op;
        
        const $left =  $('<div style="float: left; width:450px"/>');
        const $right = $('<div style="float: right; height:100%; width:calc(100% - 450px); max-height: calc(100vh - 187px); border-left: solid black 1px; padding:10px"/>');
        
        const $stationMap = $('<div/>',{'style':'width:100%; height:300px'});
        const $map = $('<div style="height:80vh"/>');
        
        const $mediaScroller =  $('<div style="height:100%; max-height: calc(80vh - 300px); overflow-y: auto"/>');

        $left.append($stationMap);
        $left.append($mediaScroller);
        
        $right.append($map);
        
        $container.append($left);
        $container.append($right);

        let selectedImage = null;
    
        const mediaScroller = new (await MediaScroller()).default({
            mediaRoot: globals.mediaRoot + op.mediaRootFolder,
            fetch: async (offset, scope = '/') => {
                const data = op.data;
                return [data, op.fileProperties];
            },
            containerWidth: '420px',
            processAttributeValues: (row) => {
                const clonedRow = Object.assign({}, row);
                mutils.processAttributeValues(clonedRow, attributesKeyed, refValuesKeyed);
                const output = [];
                Object.keys(clonedRow).map(key => {
                    const a = attributesKeyed[key];
                    if (a) {
                        const label =  a.t_name_id || a.key_name_id;
                        output.push(`<b>${label}:</b> ${clonedRow['t_'+key] || clonedRow[key]}`);
                    }
                });
                
                return output.join('<br>');    
            },
            onMediaSelected: mediaData => {
                imageMap = Map.leafletPreview(imageMap, $map, globals.mediaRoot + op.mediaRootFolder + mutils.filePath(mediaData.currentPhotoHash || mediaData.file_hash), mediaData.properties);
            },
            onDataFetched: (res, added) => {
                selectedImage = added.find(im => im.file_hash === op.selectedMedia.file_hash);
            }
        });

        $mediaScroller.append(mediaScroller.$el());

        const modal = new ModalDialog.default({
            size: 'modal-xxl',
            onShown:()=>{
                const map = new Map.default({
                    $container: $stationMap
                });

                const {markers} = map.addLayer([op.geom],'ct', op.grid);

                const marker = markers[0];
                marker.getPopup().setContent('<h5>'+op.stationName+'</h5><br>'+`<b>${t`Number of photos`}:</b> ${op.numberOfPhotos}`);
                marker.openPopup();

                if (selectedImage) {
                    selectedImage.$div.trigger('click');
                    selectedImage.$div[0].scrollIntoView();
                }
                 
                selectedImage = null;
            }
        });
        //modal.$body.css('height', '90vh');
        modal.$body.html($container);
        modal.show();

}