import * as exports from '../../libs/exports';
import globals from '../../app-globals';
import dztemplate from '../../libs/components/DropzoneTemplates/blueimp'

export default async op => {

    const {$parent} = op;

    const [
        utils,
        mutils,
        DataFilter,
        Button,
        RadioButtonSelector,
        MediaScroller,
        ModalDialog,
        DropzoneLoader,
        ComponentManager,
        Inputs,
        DataOrder,
        Alert,
        Map,
        DropDown,
        batchesModule
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        exports.DataFilter(),
        exports.Button(),
        exports.RadioButtonSelector(),
        exports.MediaScroller(),
        exports.ModalDialog(),
        exports.DropzoneLoader(),
        exports.ComponentManager(),
        exports.Inputs(),
        exports.DataOrder(),
        exports.Alert(),
        exports.Map(),
        exports.DropDown(),
        import('../batches')
    ]);

    const t = utils.t;

    let map = null;
    let dataTable = null;
    let fileTable = null;

    let groupCmp;
    let selectedMedia=[];
    let uploadBtn;
    let mediaScroller, info;
    let changeTableRecordSetFunction = null;
    let changeFileTableRecordSetFunction = null;

    let processedImages = {}; //the images processed in the current unpreocessed images window - this is needed to show image attributes when clicked in the unprocessed window

    const mediaRootFolder = '/private/ct';

    const naslov = t`Camera Trapping`;
    const podnaslov = t`data overview`;

    const $btnGroup = $('<div/>',{style:"display: flex"});

    const _attributes = await mutils.getModuleVariables('ct', false);

    const attributes = _attributes;

    const refValues = await mutils.getRefCodeListValues(attributes);

    const $header = mutils.moduleHeader(naslov, podnaslov, $btnGroup, null, null, null);
    $parent.append($header);

    $btnGroup.css('margin-top',0);
    $btnGroup.css('align-items', 'center');

    const $left =  $('<div style="float: left;width:640px"/>');
    const $mediaScroller =  $('<div style="height:100%; max-height: calc(100vh - 280px); overflow-y: auto"/>');
    const $right = $('<div style="float: right; height:100%; width:calc(100vw - 680px); max-height: calc(100vh - 187px); border-left: solid black 1px; padding:10px"/>');
    $parent.append($left);
    $parent.append($right);

    const $leftToolbar = $('<div/>');

    const $imageTypeSelectorDiv = $('<div/>', {style:'display:flex;align-items:baseline'});

    $leftToolbar.append($imageTypeSelectorDiv);

    $left.append($leftToolbar);
    $left.append($mediaScroller);

    const imageTypeSelector = new RadioButtonSelector.default({
        classes: 'btn-lg',
        items: [{key: 0,label: t`Processed images`}, {key: 1, label: t`Uprocessed images`}],
        onChange: (item) => {
            const currentValue = item.key;
            onImageListAction({key: !currentValue ? 'load_processed' : 'load_unprocessed'});
            !currentValue ? $fileTable.hide() && $table.show() : $table.hide() && $fileTable.show();
        }
    });

    info = new Alert.default({type: 'info', sections: ['processed','selected']});
    $imageTypeSelectorDiv.append(info.$el());
    info.$el().css('width','100%');

    const $map = $('<div style="height:50vh"/>');
    const $table = $('<div style="height:calc(50vh - 190px)"/>')
    const $fileTable = $('<div style="height:calc(50vh - 190px)"/>')
    $fileTable.hide();
    $right.append($map);
    $right.append($table);
    $right.append($fileTable);

    await DataFilter.loadExports(exports);
    const dataFilter = new DataFilter.default({
        attributesDefinition: attributes.filter(a => a.filterable === true),
        refValues: refValues,
        additionalReferenceTableValues: true,
        apply: {
            button: true,
            callback: () => {
                
                if (dataFilter.val().length>0) {
                    dataFilter.$el().find('button > i').css('color','red');
                }
                else {
                    dataFilter.$el().find('button > i').css('color','inherit');
                }

                onImageListAction({key: !imageTypeSelector.val() ? 'load_processed' : 'load_unprocessed'});
            }
        }
    });

    console.log('dataFilter', dataFilter)

    uploadBtn = new Button.default({
        label:t`Upload images`,
        iconClass: 'upload',
        classes: 'btn-lg',
        onClick: () => {
            if (imageTypeSelector.val() == 0) {
                imageTypeSelector.val(1, false);
            }

            const loader = new DropzoneLoader.default({
                url: globals.apiRoot + '/file-upload' + mediaRootFolder,
                onComplete: onFileUploaded,
                autoProcessQueue: false,
                template: dztemplate
            });

            const dlg = new ModalDialog.default({
                onClose: () => loader.dropzone.destroy()
            });

            dlg.$body.append(loader.$el());

            const uploadBtn = new Button.default({
                label:t`Upload`,
                onClick: () => {
                    loader.startUpload();
                    //loader.dropzone.processQueue();
                }
            });

            dlg.$footer.append(uploadBtn.$el());
            dlg.show();
        }
    });

    const selectNoneBtn = new Button.default({
        label:t`Select none`,
        iconClass: 'ban',
        classes: 'btn-lg',
        onClick: () => {
            mediaScroller.action('deselect');
        }
    });

    $imageTypeSelectorDiv.append(selectNoneBtn.$el());
    selectNoneBtn.$el().css('padding-left','15px');

    $btnGroup.append(imageTypeSelector.$el().css('padding-right', '15px').css('margin-top','-15px'));
    $btnGroup.append(dataFilter.$el().css('padding-right', '15px'));
    $btnGroup.append(uploadBtn.$el().css('padding-right', '15px'));

    const dropDown = new DropDown.default({
        classes: 'btn-lg',
        items: [
            {
                key:'mbase2/admin/modules/ct',
                label: t`Module settings`
            },
            {
                key: 'mbase2/admin/code-lists?m=ct',
                label: t`Code lists`
            },
            {
                key: 'mbase2/modules/ct/ct_camelot_sources',
                label: t`Camelot sources`
            },
            {
                key: 'mbase2/modules/ct/ct_stations',
                label: t`Camera trap stations`
            },
            {
                key: 'mbase2/modules/ct/ct_surveys',
                label: t`Surveys`
            },
            {
                key: 'mbase2/admin/referenced_tables/individuals',
                label: t`Individuals`
            }
        ],
        label: globals.t`Admin settings`,
        onClick: ({key}) => {
            window.open(window.location.origin + '/' + key, '_blank');
            return false;
        }     
    });

    dropDown.$el().css('padding-right','20px');
    dropDown.$el().css('margin-right','20px');
    dropDown.$el().css('border-right','solid 1px');

    $btnGroup.prepend(dropDown.$el());

    const addRecordBtn = new Button.default({
        label:t`Add a record`,
        iconClass: 'plus',
        classes: 'btn-lg',
        onClick: () => {
            dataTable && dataTable.__editRecord && dataTable.__editRecord();
        }
    });

    const listOfImports = new ModalDialog.default(
        {
            size: 'modal-xl',
            onClose: () => {
             
            },
            onShown: onListOfImportsShown
    });

    function onListOfImportsShown() {
        listOfImports.$body.empty();
        batchesModule.default({
            $parent: listOfImports.$body,
            scrollY: '65vh',
            moduleKey: 'ct'
        });
    }

    const batchImportdBtn = new DropDown.default({
        classes: 'btn-lg',
        iconClass: 'file',
        items: [
            {
                key: 'import',
                label: t`Import from XLSX`
            },
            {
                key: 'list',
                label: t`List of imports`
            },
            {
                key: 'camelot',
                label:t`Download desktop app for import from Camelot`
            }
        ],
        label: globals.t`Batch imports`,
        onClick: async ({key}) => {
            if (key==='import') {
                mutils.batchImport({
                    batch: await import('../batchImport'),
                    ModalDialog: ModalDialog
                }, 'ct', attributes, null, t`Data import`, 'xlsx');
            }
            else if (key==='list') {
                listOfImports.show();
            }
            else if (key === 'camelot') {
                utils.hrefClick(globals.apiRoot+'/storage/import_from_camelot-win-x64.zip', null);
            }
                
            return false;
        }
    });

    const exportBtn = new Button.default({
        label:t`Export`,
        iconClass: 'download',
        classes: 'btn-lg',
        onClick: () => {
            utils.hrefClick(globals.apiRoot+'/export/ct' + mutils.getFilterParametersString(dataFilter, true), null);
        }
    });

    $btnGroup.append(batchImportdBtn.$el().css('padding-right', '15px'));
    $btnGroup.append(addRecordBtn.$el().css('padding-right', '15px'));
    $btnGroup.append(exportBtn.$el().css('padding-right', '15px'));

    /////////////////////////////
    const tableName = 'ct';

    //add onSelect to trap_station_name field
    const trapStationName = attributes.find(v => v.key_name_id === 'trap_station_name');
    trapStationName._component = {
        default: true,
        additionalComponentOptions: {
            onSelect: () => {}
        }
    };

    //hide columns
    ['_batch_id', 'camelot_sighting_id'].map(key => {
        const a = attributes.find(v => v.key_name_id === key);
        if (a) {
            a._component = {
                default: true,
                onComponentAdded: (cmp) => {
                    cmp.$el().hide();
                }
            }
        }
    });

    //change component root
    attributes.find(v => v.key_name_id === 'photos')._component = {
        default: true,
        additionalComponentOptions: {
            mediaRoot: globals.mediaRoot + mediaRootFolder
        }
    }

    const uname = attributes.find(v => v.key_name_id === '_uname');
    uname._component = {
        import: () => exports.Storage()
    }

    const licenceNameIndex = attributes.findIndex(v => v.key_name_id === '_licence_name');
    attributes.push(attributes.splice(licenceNameIndex, 1)[0]); //move to the end of array

    /**
     * Processed images table
     */
    const dataTableRecordsOptions = await mutils.generalTableRecordsOptions($table, tableName, mutils.sortVariables([...attributes.filter(a=>a.visible_in_table)], false, 'weight_in_table'), refValues);
    dataTableRecordsOptions.sortVariables = false;

    dataTableRecordsOptions.changeRecordSet = (_changeRecordSet) => {   //change recordset will be called manually
        changeTableRecordSetFunction = _changeRecordSet;
    }

    dataTableRecordsOptions.onInitRecord = onInitRecord;
    dataTableRecordsOptions.onEditRecord = onEditRecord;
    dataTableRecordsOptions.skipAttributesFromTable = ['camelot_sighting_id'];
    dataTableRecordsOptions.batchOptions = {
        model: {'ct':  attributes.filter(a => a.dbcolumn === true || a.dbcolumn === undefined).filter(a => ['_batch_id', '_uname', 'sighting_quantity'].indexOf(a.key_name_id) ===-1)}
    }

    dataTableRecordsOptions.tableOptions = {
        hideColumns: ['photos'],
        scrollY: 'calc(50vh - 300px)'
    }

    dataTableRecordsOptions.btn_batch_import = false;
    dataTableRecordsOptions.btn_add_record = false;

    dataTableRecordsOptions.skipId = true;

    dataTableRecordsOptions.processTableData = (row, inx) => {

        row.photos = utils.jsonParse(row.photos);
        row.photos.map(photo => {
            row[photo] = true;
        });
        return row;
    };

    dataTableRecordsOptions.onTableCreated = (_table) => dataTable = _table;

    dataTableRecordsOptions.cm = new ComponentManager.default();

    dataTableRecordsOptions.saveOptions = {
        rootUrl: globals.apiRoot + '/mb2data',
        beforeRequest: (req, saveOptions) => {
            const photos = req.attributes[':photos'];
            if (photos.length > 1 && groupCmp.val() === false) {
                saveOptions.parseResult = res => res ? res.map(r => r[0]) : null;
                saveOptions.updateModelAfterSave = false;
                const attributes = [];
                photos.map(photo => {
                    const a = Object.assign({}, req.attributes);
                    a[':photos'] = [photo];
                    attributes.push(a);
                });
                req.method = 'POST';
                req.url = globals.apiRoot + '/mb2data/ct';
                req.attributes = attributes;
            }
            else {
                saveOptions.parseResult = (res) => res ? res[0] : null;
                saveOptions.updateModelAfterSave = true;
            }
        }
    }

    const model = dataTableRecordsOptions.cm.model;
    const initModelValues = () => ({
        '_licence_name':10,
        '_uname': true
    });
    
    model.values = initModelValues();

    /**
     * 
     * @param {ComponentManager} cm 
     * @param {object|array<object>} res result returned from the insert/update 
     */

    dataTableRecordsOptions.onSuccessfullySaved = async (cm, res, model) => {
        if (imageTypeSelector.val()) {  //unprocessed images
            let photos = res.photos.map ? res.photos : JSON.parse(res.photos);
            photos.map(photo => {
                processedImages[photo] = true;
            })
        }

        if (Array.isArray(res) && res.length > 1 && groupCmp.val() === false && model.values.id) {
            const res = await mutils.requestHelper(globals.apiRoot + '/mb2data/ct/'+model.values.id, 'DELETE');
            window.location.reload();
        }
        dataTableRecordsOptions.cm.model.values = initModelValues();
    };
  
    const recordsTable = await import('../recordsTable');
    await recordsTable.default(dataTableRecordsOptions);

    /**
     * Unprocessed images table
     */
    const fileAttributes = [];
    fileAttributes.push(mutils.createVariableDefinition('file_name', t`File name`, 'text'));
    fileAttributes.push(mutils.createVariableDefinition('uid', t`Uploaded by`, 'table_reference','_users'));
    fileAttributes.push(mutils.createVariableDefinition('record_created', t`Uploaded on`, 'timestamp'));
    fileAttributes.push(mutils.createVariableDefinition('photos', 'file_hash', 'jsonb'));

    const fileTableRecordsOptions = await mutils.generalTableRecordsOptions($fileTable, '__unprocessed', fileAttributes); //there is no add or edit record posibility so we choose a dummy name for this table because it won't be binded directly to a database table

    fileTableRecordsOptions.tableOptions = {
        hideColumns: ['photos'],
        scrollY: 'calc(50vh - 300px)'
    }

    fileTableRecordsOptions.btn_batch_import = false;
    fileTableRecordsOptions.btn_add_record = false;
    fileTableRecordsOptions.skipId = true;

    fileTableRecordsOptions.processTableData = (row, inx) => {

        row.photos = [row.file_hash];
        row.photos.map(photo => {
            row[photo] = true;
        });
        return row;
    };

    fileTableRecordsOptions.onTableCreated = (_table) => fileTable = _table;

    fileTableRecordsOptions.changeRecordSet = (_changeRecordSet) => {
        changeFileTableRecordSetFunction = _changeRecordSet;
        changeFileTableRecordSetFunction([]);
    }

    fileTableRecordsOptions.cm = new ComponentManager.default();

    fileTableRecordsOptions.disableEdit = true;

    fileTableRecordsOptions.externalEditRecord = row => {
        dataTable && dataTable.__editRecord && dataTable.__editRecord();
    }
  
    await recordsTable.default(fileTableRecordsOptions);

    /**
     * Media scroller
     */

    mediaScroller = new MediaScroller.default({
        fetch: (offset, scope = '/') => {
            const __filter = mutils.getFilterParametersString(dataFilter, true);
            return scope === '/processed' ? mutils.batchRequestHelper(['mb2data/ct_vw' + __filter,'mb2data/ct_view_file_properties']) : mutils.requestHelper(globals.apiRoot + '/uploaded-files' + mediaRootFolder +'/' + (offset || 0) + scope + __filter);
        },
        mediaRoot: globals.mediaRoot + mediaRootFolder,
        onMediaSelected: onMediaSelected,
        onMediaSelectionChanged: onMediaSelectionChanged,
        onDataFetched: (data, added) => {
            if (!imageTypeSelector.val()) {
                info.val({'processed': `<b>${t`Number of images`}:</b> ${added.length}`});
                changeTableRecordSetFunction && changeTableRecordSetFunction(data[0]);
            }
            else {
                info.val({'processed': `<b>${t`Number of images`}:</b> ${data.length}`});
                changeFileTableRecordSetFunction && changeFileTableRecordSetFunction(data);
            }
        }
    });

    $mediaScroller.append(mediaScroller.$el());

    imageTypeSelector.val(0);

    //////////////////////
 
    function onMediaSelected(mediaData) {
        map = Map.leafletPreview(map, $map, globals.mediaRoot + mediaRootFolder + mutils.filePath(mediaData.currentPhotoHash || mediaData.file_hash), mediaData.properties);
    }

    function onMediaSelectionChanged() {
        let currentTableCmp = !imageTypeSelector.val() ? dataTable : fileTable;
        !imageTypeSelector.val() ? $fileTable.hide() && $table.show() : $table.hide() && $fileTable.show();
        selectedMedia = mediaScroller.val();
        console.log('selectedMedia', selectedMedia)
        if (imageTypeSelector.val()) {  //unprocessed images
            for (const hash of selectedMedia){
               if (processedImages[hash]) {
                    currentTableCmp = dataTable;
                    $fileTable.hide();
                    $table.show();
                    break;
               }
            };
        }

        //if (selectedMedia.length === 0) return;
        
        const photosColumnIndex = currentTableCmp.getColumnsMap()['photos'];
        //search the selected images in the table
        currentTableCmp.table
        .columns( photosColumnIndex )
        .search( selectedMedia.join('|'), true )
        .draw();

        info.val({'selected': `<b>${t`Number of selected images`}:</b> ${selectedMedia.length}`});
    }

    function onFileUploaded(fileData, $el) {
        $el.find('.btn.btn-danger.delete').hide();
        mediaScroller.appendImages([fileData], false);
        const cm = fileTableRecordsOptions.cm;
        const data = cm.getData('table');
        data.push(fileData);
        cm.refresh('table');
        info.val({'processed': `<b>${t`Number of unprocessed images`}:</b> ${Object.keys(mediaScroller.images).length}`});
    }

    function onInitRecord(cm, modal) {
        const photos = cm.get('photos');
        photos.$el().find('button.btn-default').remove();   //remove add image btn
        const selectedPhotos = cm.model.values && cm.model.values.id ? cm.model.values.photos : mediaScroller.val();
        photos.val(selectedPhotos);

        groupCmp = new Inputs.Input({
            type:'checkbox',
            label: t`Group images`
        });

        groupCmp.val(true);

        groupCmp.$el().find('.checkbox:first').css('margin-right', '10px').css('display','inline');

        selectedPhotos.length < 2 && groupCmp.$el().hide();

        modal.$footer.append(groupCmp.$el());
    }

    function onEditRecord(cm, row) {
        if (row === undefined) {
            if (mediaScroller.val().length === 0) {
                alert(t`No photo(s) selected. You have to select at least one photo to add a record to.`);
                return false;
            }
        }
        return true;
    }

    function onImageListAction(item) {
        if (['load_processed', 'clear', 'load_unprocessed', 'deselect'].indexOf(item.key) !== -1) {
            
            mediaScroller.action(item.key);
        }
    }
}