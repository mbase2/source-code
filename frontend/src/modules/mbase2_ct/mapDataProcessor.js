import {MediaScroller} from '../../libs/exports';
import globals from '../../app-globals';
import imagesOverview from './imagesOverview';
import * as mutils from '../../libs/mbase2_utils';
import * as utils from '../../libs/utils';

/**
 * ct map data processor
 */

export default op => {

    const stationData = {};

    const t = utils.t;

    for (const sighting of op.result) {
        if (!stationData[sighting.trap_station_name]) stationData[sighting.trap_station_name] = [];
        stationData[sighting.trap_station_name].push(sighting);
    }

    const data = [];

    const adef = op.attributes.find(a => a.key_name_id == 'trap_station_name');

    Object.keys(stationData).map(key => {
        const sightings = stationData[key];

        const stationName = op.refValues.tableReferences.find(r => r._id_list_id === key+'_'+adef.ref).key;
        
        data.push({
            geom: sightings[0].geom,
            data: {
                sightings,
                stationName,
                rows: op.result,
                grid: op.grid,
                fileProperties: op.fileProperties,
                stationKey: key
            }
        });

    });

    return data;
}

export const onMarkerClicked = async (marker, dataRows, op) => {

    const row = dataRows[0];
    
    const $container = $('<div/>');
    $container.css('width', '450px');

    const popup = marker.getPopup();

    const t = utils.t;

    popup.setContent($container[0]);
    
    const $div = $(popup.getElement()).find('.leaflet-popup-content:first');
    $div.css('min-width', '450px');

    const {sightings, stationName} = row;
    
    const photos = {};
    sightings.map(record => {
        record.photos = utils.jsonParse(record.photos);
        record.photos.map(photo => photos[photo] = true);
    });
    
    const numberOfPhotos = Object.keys(photos).length;

    $container.html('<h3>'+stationName+'</h3>');
    $container.append(`<b>${t`Number of records`}:</b> ${sightings.length}`);
    $container.append('<br>');
    $container.append(`<b>${t`Number of photos`}:</b> ${numberOfPhotos}`);
    $container.append('<hr>');

    const $mediaContainer = $('<div/>');

    $mediaContainer.css('width', '100%');
    $mediaContainer.css('max-height', '50vh');
    $mediaContainer.css('overflow-y', 'auto');

    $container.append($mediaContainer);

    const stationKey = row.stationKey;

    const data = row.rows.filter(r=>r.trap_station_name == stationKey);

    const mediaRootFolder = '/private/ct';

    const {attributesKeyed, refValuesKeyed} = op.moduleAttributes;
                    
    const mediaScroller = new (await MediaScroller()).default({
        mediaRoot: globals.mediaRoot + mediaRootFolder,
        fetch: async (offset, scope = '/') => {
            return [data, row.fileProperties];
        },
        containerWidth: '420px',
        processAttributeValues: (row) => {
            const clonedRow = Object.assign({}, row);
            mutils.processAttributeValues(clonedRow, attributesKeyed, refValuesKeyed);
            const output = [];
            Object.keys(clonedRow).map(key => {
                const a = attributesKeyed[key];
                if (a && a.key_name_id !== 'photos') {
                    const value = clonedRow['t_'+key] || clonedRow[key];
                    if (value) {
                        const label =  a.t_name_id || a.key_name_id;
                        output.push(`<b>${label}:</b> ${value}`);
                    }
                }
            });
            
            return output.join('<br>');    
        },
        onMediaSelected: mediaData => {
            imagesOverview({
                selectedMedia: mediaData,
                result: row.rows,
                grid: row.grid,
                fileProperties: row.fileProperties,
                mediaRootFolder: mediaRootFolder,
                data: data,
                attributesKeyed: attributesKeyed,
                refValuesKeyed: refValuesKeyed,
                geom: {geom: sightings[0].geom},
                stationName: stationName,
                numberOfPhotos: numberOfPhotos
            });                        
        }
    });

    $mediaContainer.append(mediaScroller.$el());
    popup.update();
}