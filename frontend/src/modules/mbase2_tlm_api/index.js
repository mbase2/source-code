import globals from '../../app-globals';
import * as exports from '../../libs/exports';
import modalDialogTableEditor from '../../helpers/modalDialogTableEditor';

export default async op => {
    const {$parent} = op;

    const [
        Map,
        Sidebar,
        Button
    ] = await Promise.all([
        exports.Map(),
        exports.Sidebar(),
        exports.Button()
    ]);

    const t=globals.t;

    ///////Leaflet Map

    const map = new Map.default({
        $container: $parent,
        scaleControl: true
    });

    const sidebarContainer = new Sidebar.default({id:'leaflet-sidebar',title: `Data overview`,$container: $parent});
    const $sidebarContainer = sidebarContainer.$el().find('#home .container-fluid:first');
    const sidebar = map.addSidebar('leaflet-sidebar');

    const $div=$('<div/>',{style:'display:flex;justify-content:right'});

    const showDeploymentsBtn = new Button.default({
        label: t`Deployments`,
        onClick: () => {
            modalDialogTableEditor({
                tableName:'tlm_deployments'
            });
        }
    });

    const showApiKeysBtn = new Button.default({
        label: t`API keys`,
        onClick: () => {
            modalDialogTableEditor({
                tableName:'tlm_keys'
            });
        }
    });

    $div.append(showDeploymentsBtn.$el());
    $div.append(showApiKeysBtn.$el());

    $sidebarContainer.append($div);
    
    
    sidebar.open('home');
}
