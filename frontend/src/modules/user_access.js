import * as exports from '../libs/exports';
import globals from '../app-globals';

/**
 * @param {object} op options
 * @param {object} op.$parent
 * @param {string} op.tname
 */
export default async op => {
    const {$parent} = op;

    const [
        ComponentManager,
        utils,
        mutils,
        Button,
        ModalDialog

    ] = await Promise.all([
        exports.ComponentManager(),
        exports.utils(),
        exports.mutils(),
        exports.Button(),
        exports.ModalDialog()
    ]);

    const t = utils.t;

    const sourceTable = op.sourceTable || 'mbase2_users';   //primary table or view from which you can select users to be added to the secondary talbe
    const sourceTableSchema = op.sourceTableSchema ? '/' + op.sourceTableSchema : '';

    const btnAdd = new Button.default({
        label: (op.labels && op.labels['Add users']) || t`Add users`,
        onClick: onAddUsers,
        classes: 'btn-primary btn-lg'
    });

    const $header = mutils.moduleHeader(op.title, op.subtitle, btnAdd.$el());
    $parent.append($header);

    const $moduleUsersTableDiv = $('<div/>');
    $parent.append($moduleUsersTableDiv);

    const tname = op.targetTable;

    const filter = op.filter ? '?:__filter='+op.filter : '';

    const moduleUserVariables = await mutils.requestHelper(globals.apiRoot + `/module_variables_vw/language/${globals.language}?:module_name=${tname}`);

    const tableRecordsOptions = await mutils.generalTableRecordsOptions($moduleUsersTableDiv, tname, moduleUserVariables);
    
    tableRecordsOptions.url = globals.apiRoot + '/mb2data/'+tname;
    tableRecordsOptions.disableEdit = false;
    tableRecordsOptions.skipId = true;
    tableRecordsOptions.deletable = true;
    tableRecordsOptions.btn_add_record = false;
    tableRecordsOptions.btn_batch_import = false;
    tableRecordsOptions.saveOptions = {
        rootUrl: globals.apiRoot + '/mb2data'
    }
    tableRecordsOptions.onEditRecord = (cm, row) => {
        const unameProps = cm.model.attributes.find(v => v.key_name_id === '_uname');
        unameProps._skipUpsert = true;
        cm.model.values._uname = cm.model.values.t__uname;
        unameProps.read_only = true;
        unameProps.key_data_type_id = 'text';
    }

    let accessTableComponent = null;
    tableRecordsOptions.onTableCreated = (table) => {   //get reference to the table when table is created
        accessTableComponent = table;
    }
 
    tableRecordsOptions.cm = new ComponentManager.default();

    tableRecordsOptions.tableOptions = {
        scrollY:'63vh',
        select: false,
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-left'
            },
            { width: 50, targets: 0 }
        ]
    };

    const recordsTable = await import('./recordsTable');
    recordsTable.default(tableRecordsOptions);

    let userVariables = null;
    let usersTableComponent = null;
 
    async function onAddUsers() {
        const modal = new ModalDialog.default();
        modal.show();

        userVariables = userVariables || await mutils.requestHelper(globals.apiRoot + `/module_variables_vw/language/${globals.language}?:module_name=${sourceTable}`);

        const tableRecordsOptions = {

            $parent: modal.$body,
            tableName: sourceTable,
            url: globals.apiRoot + sourceTableSchema + '/' + sourceTable + filter,
            variablesAttributes: userVariables,
            btn_add_record: false,
            btn_batch_import: false,
            disableEdit: true,
            skipId: true,
            tableOptions: {
                /*columnDefs: [ {
                    orderable: false,
                    className: 'select-checkbox',
                    targets:   0
                } ],*/
                select: true,
                scrollY:'63vh'
            },
            onTableCreated: (table) => {
                usersTableComponent = table;
            }
        }
    
        const recordsTable = await import('./recordsTable');
        recordsTable.default(tableRecordsOptions);

        const btnDodaj = new Button.default({
            label: t`Add selected items`,
            onClick: addSelectedItems,
            classes: 'btn-primary'
        });

        modal.$footer.append(btnDodaj.$el());
    }

    async function addSelectedItems() {
        const data = usersTableComponent.table.rows( { selected: true } ).data();
        const existingUids = {};

        accessTableComponent.table.rows().data().map(row => {
            existingUids[row._uname] = true;
        });

        const toAdd = [];

        data.map(row => {
            if (!existingUids[row._uid]) {
                toAdd.push(row._uid);
            }
        });

        for (const uid of toAdd) {
            await mutils.requestHelper(globals.apiRoot + '/mb2data/dmg_deputies','POST',{
                _uname: uid
            });
        }
        
        recordsTable.default(tableRecordsOptions);
    }
}
