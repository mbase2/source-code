import * as exports from '../../libs/exports';
import globals from '../../app-globals';

window.addEventListener('popstate', (event) => {
    //event.state && event.state.id && parseInt(event.state.id) > 0 && window.location.reload();
    window.location.reload();
});

/**
 * @param {object} op options
 * @param {object} op.$parent
 * @param {string} op.tname
 */
export default async op => {
    const {$parent} = op;

    const [
        ComponentManager,
        utils,
        mutils,
        Select2,
        Inputs,
        ModalDialog,
        InputMask,
        Button,
        LocationSelector,
        TreeView,
        ButtonGroup,
        DropzoneLoader,
        dmgu,
        DateInputs,
        SimpleTable,
        TTomSelect

    ] = await Promise.all([
        exports.ComponentManager(),
        exports.utils(),
        exports.mutils(),
        exports.Select2(),
        exports.Inputs(),
        exports.ModalDialog(),
        exports.InputMask(),
        exports.Button(),
        exports.LocationSelector(),
        exports.TreeView(),
        exports.ButtonGroup(),
        exports.DropzoneLoader(),
        import('./dmg_utils'),
        exports.DateInputs(),
        exports.SimpleTable(),
        exports.TTomSelect()
    ]);

    dmgu.default(exports);


    const t = utils.t;

    let _skodniObjekti = {
        data: null
    };

    const bookmarks = {};

    let naciniVarovanjaClipboard = [];

    const $nezakljucen = $(`<span class="label label-default">${t`NEZAKLJUČEN ZAHTEVEK`}</span>`);
    const $neshranjen = $(`<span class="label label-warning">${t`NESHRANJEN ZAHTEVEK`}</span>`);
    let $header = null;

    const $status = $('<div/>');

    const $breadcrumbs = $('<div/>');

    setInitialStatus();

    const $buttonGroupContainer = $('<div/>');

    let group = null;

    let $saveFormButton = null;
    let $completeClaimButton = null;

    let initialData = {};

    const cm = new ComponentManager.default({
        onComponentChange: (key, value, cm) => {
            //console.log('onComponentChange', key, value, cm)
            const bm = bookmarks[key];

            if (!bm) return;

            if (bm.parent) {
                bm.$bm = bookmarks[bm.parent].$bm;
            }

            const validated = bm.validated = validateComponentValue(key, value, bm.required, bm.parent);

            $saveFormButton && $saveFormButton.prop('disabled', false);
            
            if (bm.$bm) {
                if (bm.required === true && validated) {
                    bm.$bm.addClass('bookmark-completed');
                }
                else {
                    bm.$bm.removeClass('bookmark-completed');
                }
            }

            if (key==='_dmg_examination_date') {
                _skodniObjekti.data !== null && skodniObjekti({
                    key: "_dmg_objects",
                    title: "Škodni objekti"
                });
            }

            validateConditionsForCompleteClaimButton();
        },
        onComponentAppended: (key, component, cm) => {
            
            let value = initialData[key];

            if (value && component.val && !_.isEqual(value, component.val())) {
                component.val(value);
                cm.op.onComponentChange(key, value, cm);
            }
        }
    });

    const batch = [
        `module_variables_vw/language/${globals.language}?:module_name=dmg`,
        `mb2data/dmg?:__select=_claim_id`
    ];
    
    let fid = utils.getQueryParameter('fid');

    if (fid) {
        batch.push(`mb2data/dmg_agreements?:_claim_id=${fid}`);
        batch.push(`mb2data/dmg/${fid}`);
    }

    const result = await mutils.batchRequestHelper(batch);

    const dmgVariables = result[0];

    const dmgClaims = utils.arrayToObject(result[1].filter(r=>r._claim_id), '_claim_id');

    cm.model.attributes = utils.arrayToObject(dmgVariables,'key_name_id');

    let agreementsReached = false;
    
    if (fid) {
        const res = result[3];
        if (res && res[0]) {

            mutils.convertJsonToObjects(res, dmgVariables);
            initialData = res[0];

            if (initialData._claim_status==1) {
                const agreementsStatus = mutils.getAgreementsStatus(result[2]);
                console.log('aggrementStatus', agreementsStatus)
                if (agreementsStatus[fid] === initialData._affectees.length) {
                    agreementsReached = true;
                    $status.html($(`<span class="label label-info">${t`ZAKLJUČEN ZAHTEVEK`}</span>`));
                }
                else {
                    $status.html($(`<span class="label label-danger">${t`ZAKLJUČEN ZAHTEVEK - brez zaključenih obrazcev o sporazumih`}</span>`));
                }
            }

            if (posredniZahtevek()) {
                $status.append($(`<span class="label label-warning">${t`NAKNADNA ŠKODA št.`} ${initialData.__indirect_counter}</span>`));
            }
            
            cm.model.values = {id: fid};
        }
        else {
            alert(t`Zahtevek ne obstaja.`);
            window.location = window.location.origin + window.location.pathname.replace('/zahtevek','');
        }
    }

    const naslov = t`Škode`;
    const podnaslov = fid ? t`urejanje zahtevka` : t`nov zahtevek`;

    group = createButtonGroup();

    const breadcrumbs = initialBreadcrumbs();

    if (initialData._claim_status == 1) {
        breadcrumbs.push({
            href: window.location.pathname+'/sporazumi?fid=' + fid,
            text: t`urejanje sporazumov`
        });
    }

    $breadcrumbs.html(utils.createBreadcrumbs(breadcrumbs));

    $buttonGroupContainer.html(group.$el());

    $header = mutils.moduleHeader(naslov, podnaslov, $buttonGroupContainer, $status, null, $breadcrumbs);
    $parent.append($header);

    const $left =  $('<div style="float: left; height:100%;  width:20%; max-height: calc(100vh - 187px); overflow: auto"/>');
    const $right = $('<div style="float: right; height:100%; width:80%; max-height: calc(100vh - 187px); overflow: auto; border-left: solid black 1px; padding:10px"/>');
    $parent.append($left);
    $parent.append($right);
    $('body').css('overflow','hidden');

    const bookmark = (op) => {
        return $(`<div class="bookmark bookmark-${op.required ? 'required' : 'optional' }">
            <h5>${op.title}${op.required === false ? '' : '*'}</h5>
        </div>`);
    }

    const sections = [
        {
            key:'_deputy',
            title: t`Pooblaščenec ZGS za zahtevek`,
            required: true,
            fun: pooblascenec
        },
        {
            key:'_claim_id',
            title: t`Številka škodnega zahtevka`,
            required: true,
            fun: stevilkaSkodnegaPrimera
        },
        {
            key: '_affectees',
            title: t`Oškodovanci`,
            required: true,
            fun: oskodovanci
        },
        {
            key: '_dmg_dates',
            title: t`Datumi`,
            required: true,
            fun: dmgDates
        },
        {
            key: '_location_data',
            title: t`Lokacija škodnega dogodka`,
            required: true,
            fun: lokacija
        },
        {
            key: '_culprit',
            title: t`Povzročitelj in njegovi znaki`,
            required: true,
            fun: povzrocitelj
        },
        {
            key: '_culprit_signs',
            parent: '_culprit',
            required: true
        },
        {
            key: '_expected_event',
            title: t`Pričakovanost dogodka`,
            required: true,
            fun: expectedEvent
        },
        {
            key: '_dmg_objects',
            title: t`Škodni objekti`,
            required: false,
            fun: skodniObjekti
        },
        {
            key: '_others',
            title: t`Ostali udeleženci ogleda`,
            fun: others,
            required: false
        },
        {
            key: '_genetic_samples',
            title: t`Genetika`,
            fun: genetika,
            required: false
        },
        {
            key: '_attachments',
            title: t`Priloge`,
            required: false,
            fun: attachments
        },
        {
            key: '_notes',
            title: t`Opombe`,
            required:false,
            fun: notes
        }   
    ];

    ['_dmg_start_date',
    '_dmg_end_date',
    '_dmg_noticed_date',
    '_dmg_date_type',
    '_dmg_examination_date'].map(key => {
        sections.push({
            key: key,
            parent: '_dmg_dates',
            required: true
        });
    });

    for (let section of sections) {
        const required = section.required;

        if (section.parent) {
            bookmarks[section.key] = {
                parent: section.parent,
                required: required
            };
            continue;
        }

        const title = section.title;
        
        const $bm = bookmark({
            title: title, 
            required: required
        }); 

        $bm.on('click', () => {
            const panel = cm.get('acc_'+section.key);
            if (!panel) return;
            
            cm.get('_location_data').$el().hide(); /*without hidding fixed height element
            the top margin of container div was ignored after scrolling (any) element into the view
            */
            cm.get('_others').$el().hide(); //this was also causing the error when scrolling
            cm.get('_expected_event').$el().hide(); //this was also causing the error when scrolling
            panel.$el()[0].scrollIntoView();
            cm.get('_expected_event').$el().show(); //this was also causing the error when scrolling
            cm.get('_others').$el().show();
            cm.get('_location_data').$el().show();
        })
        
        bookmarks[section.key] = {
            $bm: $bm,
            required: required
        };

        $left.append($bm);
    }

    //load sections
    for (let section of sections) {
        if (!section.fun) continue;
        await section.fun(section);
    }

    refreshButtonsState();

    setFormControllsAccordingToStatus();

    if (agreementsReached) {
        disable2();
    }

    if (initialData && initialData.__access==404) { //restricted access
        $status.append($(`<span class="label label-warning">${t`Zahtevek druge OE - ureja lahko samo admin`}</span>`));
        disable2(true, false);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function refreshButtonsState() {
        buttonControlsState();
        validateConditionsForCompleteClaimButton();
    }

    function setFormControllsAccordingToStatus() {
        const isPosredni = posredniZahtevek();
        if (initialData._claim_status > 0 || isPosredni) {
            disable1(true);
        }
    }

    async function notes(titleKey) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);

        const attributes = utils.convertToAssocArray(dmgVariables, 'key_name_id');
        
        await cm.add({
            key: '_deputy_notes',
            component: Inputs.Input,
            options: {
                label: attributes['_deputy_notes'].t_name_id || '_deputy_notes'
            },
            $parent: $container
        });

        await cm.add({
            key: '_affectee_notes',
            component: Inputs.Input,
            options: {
                label: attributes['_affectee_notes'].t_name_id || '_affectee_notes'
            },
            $parent: $container
        });

        await cm.add({
            key: '_affectee_claim_notes',
            component: Inputs.Input,
            options: {
                label: attributes['_affectee_claim_notes'].t_name_id || '_affectee_claim_notes'
            },
            $parent: $container
        });
    }

    function buttonControlsState() {
        const saveFormButton = group.buttons.find(b => b.key === 'save');
        $saveFormButton = saveFormButton && saveFormButton.$btn;

        if ($completeClaimButton && initialData._claim_status == 1) {
            $completeClaimButton.remove();
            $completeClaimButton = null;
        }

        const completeClaimButton = group.buttons.find(b => b.key === 'complete');
        $completeClaimButton = completeClaimButton && completeClaimButton.$btn;

        const copyButton = group.buttons.find(b => b.key === 'copy');
        fid ? copyButton && copyButton.$btn.prop('disabled', false) : copyButton.$btn.prop('disabled', true);
        
        group.buttons.map(btn => {
            btn.$btn.show();
        });

        fid ? $neshranjen.hide() : $neshranjen.show();

        $completeClaimButton && $completeClaimButton.prop('disabled', true);

    }

    async function attachments(titleKey) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);
        
        await cm.add({
            key: titleKey.key,
            $parent: $container,
            component: DropzoneLoader.default,
            options: {
                url: globals.apiRoot + '/file-upload/private/dmg',
                acceptedFiles: '.*',
                onComplete: (data, $el) => {
                    $el.trigger('change');
                },
                labels: {
                    add_files: t`Dodaj datoteke`
                },
                dictDefaultMessage: t`Sem lahko povlečeš in spustiš datoteke.`,
                acceptAllFiles: true
            }
        });
    }

    //cm.get('pooblascenec').val('2');

    function selectedDeputyData() {
        const kkodaCmp = cm.get('_deputy');
        if (!kkodaCmp || !kkodaCmp.val()) return null;
        const selectedDeputyUID = kkodaCmp.val();
        
        return cm.getData('_deputy').find(d => d._uid == selectedDeputyUID);
    }
    function kadrovskaKoda () {
        const data = selectedDeputyData();
        if (initialData._claim_id && initialData._deputy == data._uid) {//ne spreminjaj kadrovske kode, če je številka zahtevka že vpisana in če gre za istega pooblaščenca
            return initialData._claim_id.split('/')[1];
        }
        if (!data) return '--';
        const kkoda = data._kadrovska_koda;
        return kkoda || '--';
    }

    function oe () {
        const data = selectedDeputyData();
        if (initialData._claim_id && initialData._deputy == data._uid) {//ne spreminjaj kadrovske kode, če je številka zahtevka že vpisana in če gre za istega pooblaščenca
            return initialData._claim_id.split('/')[0];
        }
        if (!data) return '--';
        let oeid = data._oe_id;
        if (oeid===0) oeid = '0';
        if (!oeid) return '--';
        const padded = utils.strLeftPad(oeid, 2);
        return `{\\${padded[0]}\\${padded[1]}}`;
    }

    async function expectedEvent(titleKey) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);
        await cm.add({
            key: titleKey.key,
            component: Select2.default,
            options:{
                data: [[1, t`Dogodek JE bil objektivno pričakovan`], [2, t`Dogodek NI bil objektivno pričakovan`]]
            },
            $parent: $container
        });
    }

    async function stevilkaSkodnegaPrimera(titleKey, replace = false) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);
        
        const claimIdCmp = await cm.add({
            key: titleKey.key,
            component: Inputs.Input,
            options: {
                mask: {
                    component: InputMask.default,
                    options: {
                        onAccept: (mask,$el) => $el.trigger('change'),
                        onComplete: (mask,$el) => $el.trigger('change'),
                        maskOptions: {
                            mask: `${oe()}/${kadrovskaKoda()}/\`NUM[000]/LETO`,
                            lazy: false,  // make placeholder always visible
                        
                            blocks: {
                                LETO: {
                                    mask: IMask.MaskedRange,
                                    from: 2000,
                                    to: new Date().getFullYear()
                                },
                                NUM: {
                                    mask: IMask.MaskedRange,
                                    from:1,
                                    to: 9
                                }
                            }
                        }
                    }
                },
                help: t`Številka škodnega zahtevka v obliki: "šifra OE/kadrovska koda pooblaščenca/zaporedna številka zapisnika/leto". OE in kadrovska koda sta določeni z izbranim pooblaščencem.`
            },
            $parent: $container
        }, replace);

        const origValFun = claimIdCmp.val;
        claimIdCmp.val = function (value) {
            if (value!==undefined) {
                origValFun.call(claimIdCmp, value);
                return;
            }

            return posredniZahtevek() ? initialData._claim_id : origValFun.call(claimIdCmp);

        };
    }
    
    async function pooblascenec(titleKey) {
        await cm.add(Object.assign(exports.select({
            url: globals.apiRoot + '/mb2data/dmg_deputies_vw' + (initialData && initialData.__access==404 ? '?:__select=*' : ''), //override output filter for deputies if restricted access
            onSelect: () => {
                //change claim_id on select
                const claimCmp = cm.get('_claim_id');
                if (!claimCmp) return;
                if (!claimCmp.imask) return;
                claimCmp.imask.mask.destroy();
                stevilkaSkodnegaPrimera(sections.find(tk => tk.key === '_claim_id'), true);
            },
            $parent: await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right),
            filter: deputyFilter,
            process: processDeputyData
        }, false), {key:titleKey.key, component: Select2.default}));
    
    }

    function deputyFilter(d) {
        if ((initialData && initialData._claim_status !== undefined && initialData._claim_status < 1) || !fid) {
            return d._active;
        }
        return true;
    }

    function processDeputyData(d) {
        return [d._uid, d._uname + ` (${d._oe_ime})`]
    }

    let deputiesData = null;

    async function reinitDeputiesSelector() {
        const data = deputiesData || await mutils.requestHelper(globals.apiRoot + '/mb2data/dmg_deputies_vw' + (initialData && initialData.__access==404 ? '?:__select=*' : '')); //override output filter for deputies if restricted access
        deputiesData = data;
        cm.get('_deputy').reinit(data.filter(deputyFilter).map(processDeputyData));
    }
    
    async function skodniObjekti(titleKey) {

        const containerObj = cm.get('acc_' + titleKey.key);
        const $container = (containerObj && containerObj.panels[0].$body) || await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);
        
        const data = _skodniObjekti.data || await mutils.batchRequestHelper([
            'mb2data/dmg_objects_price_lists',
            'code_list_options?:list_key=dmg_nacini_varovanja_options',
            'mb2data/dmg_nacini_varovanja_vw'
        ]);

        _skodniObjekti.data = data;

        const examinationDate = cm.get('_dmg_examination_date').val();
        
        if (!examinationDate) {
            $container.html(mutils.alertCode(globals.t`Pred prikazom seznama škodnih objektov je treba izbrati datum ogleda škode`, '', 'danger'));
            _skodniObjekti.priceListInx = null;
            return false;
        }

        const priceLists = data[0];

        const priceListInx = mutils.dmgGetPriceListIndex(examinationDate, priceLists);

        if (priceListInx === -1) {
            $container.html(mutils.alertCode(globals.t`Napaka pri nalaganju seznama škodnih objektov`, '', 'danger'));
            return false;
        }

        if (priceListInx === _skodniObjekti.priceListInx) return;

        _skodniObjekti.priceListInx = priceListInx;

        $container.empty();

        const $treeViewContainer = $('<div/>',{style:"width:50%"});
        const $formContainer = $('<div/>',{style:"border:solid 1px #f1f1f1; width:50%; max-width: 50%"});
        $container.css('display', 'flex');
        $container.append($treeViewContainer);
        $container.append($formContainer);
        const SearchInput = await import('../../libs/components/SearchInput');

        const valid_from = priceLists[priceListInx].valid_from;
        const valid_till = priceLists[priceListInx].valid_till;
        
        let msg = '';
        if (!valid_from) {
            msg = globals.t`Prikazan je seznam škodnih objektov po ceniku veljavnem <b>do` + ' ' + moment(valid_till).format('DD. MM. YYYY') + '</b>.';
        }
        else if (!valid_till) {
            msg = globals.t`Prikazan je seznam škodnih objektov po ceniku veljavnem <b>od` + ' ' + moment(valid_from).format('DD. MM. YYYY') + '</b>.';
        }
        else {
            msg = globals.t`Prikazan je seznam škodnih objektov po ceniku veljavnem <b>od` + ' ' + moment(valid_from).format('DD. MM. YYYY') + ' do ' + moment(valid_till).format('DD. MM. YYYY') + '</b>.';
        }

        $treeViewContainer.append(mutils.alertCode(msg, '', 'info'));

        const treeData = utils.jsonParse(priceLists[priceListInx].data);   //dmg_objects_price_lists

        const naciniVarovanja = data[1];
        const naciniVarovanjaCulprit = data[2];

        //https://stackoverflow.com/questions/34273782/keep-clicked-node-as-selected

        const objekti = await cm.add({
            key: 'objekti',
            component: TreeView.default,
            options: {
                //levels:2, //remove in production
                $parent: $treeViewContainer,
                imports: {
                    SearchInput: SearchInput,
                    t: t
                },
                data: treeData,
                triggerChange: false,
                onNodeSelected: async function(event, data, $tree) {

                    console.log('onNodeSelected - škodni objekti', data)

                    const affectees = cm.get('_affectees').val();
                    if (!affectees || affectees.length === 0) {
                        $tree.treeview('unselectNode', data);
                        alert('Pred dodajanjem škodnih objektov je treba izbrati vsaj enega oškodovanca.');
                        return false;
                    }

                    let parentId = data.parentId;
                    if (!parentId) return;
                    const path = (parentId+"").split('.');
                    parentId = path[0]+'.'+path[1];
                    const objectGroupId = parentId + '.' + path[2];
                    const nodes = $tree.treeview('getNodes');
                    const parentNode = nodes.find(n => n.nodeId == parentId);

                    const objectGroupNode = nodes.find(n => n.nodeId == objectGroupId);
                    
                    const parentGroupKey = parentNode.text;
                    const objectGroupKey = (objectGroupNode && objectGroupNode.text) || '';
                    $tree.treeview('unselectNode', data);
                    editDamageObject(data, parentGroupKey, objectGroupKey);
                }
            }
        }, true);

        async function editDamageObject(selectedNode, parentGroupKey = null, objectGroupKey = null, add = true) {
            const objektiForme = await import('./damage_objects_props');
        
            if (!parentGroupKey) {
                parentGroupKey = selectedNode.parentGroupKey;
            }

            if (!parentGroupKey) {
                alert(`Ne najdem skupine škodnega objekta.`);
                return;
            }

            if (!objectGroupKey) {
                objectGroupKey = selectedNode.objectGroupKey;
            }

            const affecteesCmp = cm.get('_affectees');
            const affectees = _.zip(affecteesCmp.val(), affecteesCmp.text());

            if (!add && !selectedNode.cenik) {
                const objectKey = selectedNode.objectKey;

                let items = treeData;
                for (const key of [parentGroupKey, objectGroupKey]) {
                    if (!key) continue;
                    const item = items.find(d => d.text === key);

                    if (item && item.nodes) {
                        items = item.nodes;
                    }
                    else {
                        break;
                    }
                }

                if (items) {
                    const item = items.find(item => item.text ===objectKey);
                    if (item) {
                        selectedNode.cenik = item.cenik;
                    }
                }
            }

            const modal = new ModalDialog.default();
            objektiForme.default({
                add:add,
                naciniVarovanjaClipboard: naciniVarovanjaClipboard,
                affectees: affectees,
                selectedNode: selectedNode,
                naciniVarovanja: naciniVarovanja,
                naciniVarovanjaCulprit: naciniVarovanjaCulprit,
                selectedCulprit: cm.get('_culprit').val(),
                zgsKey: selectedNode.zgsKey,  
                dmg_object_id: selectedNode.dmg_object_id, 
                parentGroupKey: parentGroupKey,
                objectGroupKey: objectGroupKey,
                imports: {t, utils, exports, mutils},
                $parent: modal.$body,
                $footer: modal.$footer,
                refreshIzbraniObjekti: refreshIzbraniObjekti,
                tree: cm.get(titleKey.key),
                onOk: () => {
                    modal.hide();
                    modal.destroy();
                }
            });
            modal.$title.text([parentGroupKey, objectGroupKey, add ? selectedNode.text : selectedNode.objectKey].filter(key => key).join(' > '));
            modal.show();
        }

        await cm.add({
            key: 'btn_group',
            component: ButtonGroup.default,
            options: {
                highlight: false,
                onClick: (data, key, self) => {
                    const cmp = cm.get(titleKey.key);
                    const selectedNode = cmp.getSelectedNode();
                    
                    if (!selectedNode || selectedNode.length === 0) {
                        alert(`Potrebno je izbrati škodni objekt za urejanje.`);
                        return;
                    }
                    if (key === 'edit') {
                        editDamageObject(selectedNode[0], null, null, false);
                    }
                    else if (key === 'remove') {
                        if (confirm(t`Ali res želiš odstraniti izbrani objekt?`)) {
                            const data = cmp.val();
                            data.splice(selectedNode[0].index, 1);
                            refreshIzbraniObjekti(data);
                        }
                    }
                },
                buttons:[
                    {
                        key: 'edit',
                        label: t`Uredi`
                    },
                    {
                        key: 'remove',
                        label: t`Odstrani`
                    }
                ]
            },
            $parent: $formContainer
        }, true);

        await cm.add({
            key: titleKey.key,
            component: TreeView.default,
            options: {
                reinitTreeWithDataValues: true,
                $parent: $formContainer,
                data: [],
                imports: {
                    t: t
                }
            }
        }, true);

        function refreshIzbraniObjekti(data=[]) {
            const cmp = cm.get(titleKey.key);
            cmp.val(data);
            //cmp && cmp.$el() && cmp.$el().trigger('change');
        }
    }

    async function povzrocitelj(titleKey) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);

        const $treeViewContainer = $('<div/>',{style:"width:50%"});
        const $formContainer = $('<div/>',{style:"border:solid 1px #f1f1f1; width:50%; max-width: 50%;padding-left:10px"});
        $container.css('display', 'flex');
        $container.append($treeViewContainer);
        $container.append($formContainer);

        const SearchInput = await import('../../libs/components/SearchInput');
        const data = await mutils.batchRequestHelper([
            'mbase2call/get_tree/mb2data.dmg_culprits_vw',
            'code_list_options?:list_key=culprit_options'
        ]);

        const treeData = data[0];

        const culpritsCodeList = utils.arrayToObject(data[1],'key');

        convertTreeData(treeData, culpritsCodeList);

        await cm.add({
            key: '_selected_culprit',
            component: Inputs.Input,
            options: {
                label: t`Izbrani povzročitelj`,
                required: true
            },
            $parent: $formContainer
        });

        const selectedCulpritCmp = await cm.add({
            key: titleKey.key,
            component: TreeView.default,
            options: {
                imports: {
                    SearchInput: SearchInput,
                    t: t
                },
                data: treeData,
                preventUnselect: true,
                onNodeSelected: (event,data,$tree) => {
                    const cmp = cm.get('_selected_culprit');
                    if (!cmp) return;

                    if (["Other species of a songbird",
                    "Other species of a bird of prey or an owl",
                    "Other",
                    "Unprotected species (also a dog)"].indexOf(data.key) !==-1) {
                        cmp.val('');
                        cmp.$el().find('.label:first').text(data.text);
                        cmp.$el().find('input:first').val(initialData._selected_culprit).prop('disabled', false).focus();
                    }
                    else {
                        cmp.$el().find('.label:first').text(t`Izbrani povzročitelj`);
                        cmp.val(data.text);
                        cmp.$el().find('input:first').prop('disabled', true);
                    }

                    const culpritSignsCmp = cm.get('_culprit_signs');
                    if (!culpritSignsCmp) return;
                    culpritSignsCmp.dropdown(data.key);
                    culpritSignsCmp.val([]);
                }
            },
            $parent: $treeViewContainer
        });

        const selectedCulpritNode = selectedCulpritCmp.getSelectedNode();

        const CulpritSigns = await import('./CulpritSigns');

        await cm.add({
            key: '_culprit_signs',
            component: CulpritSigns.default,
            options: {
                label: t`Znaki povzročitelja`,
                required: true,
                imports:{
                    TTomSelect: TTomSelect,
                    Inputs: Inputs
                },
                selectedCulprit: selectedCulpritNode && selectedCulpritNode[0] && selectedCulpritNode[0].key
                //signs: znaki.map(znak => znak._dmg_znak_opis)
            },
            $parent: $formContainer
        });
    }

    async function lokacija(titleKey) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);

        await cm.add({
            key: titleKey.key,
            component: LocationSelector.default,
            options: {
                defaultSpatialRequest: {
                    apiRoot: globals.apiRoot
                },
                spatialAttributes: ['ob_uime', 'oe_ime', 'luo_ime', 'lov_ime'],
                showAllCrsTransformationsInPopup:true,
                defaultCRS: 3912,
                imports: {
                    Select2: Select2,
                    t:t,
                    Button: Button,
                    mutils: mutils
                },
                $parent: $container,
                distanceFromSettlement: true,
                localName: true,
                locationTypeData: false,
                revert: true,
                confirmChange: true
            }
        });
    }

    async function others(titleKey) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);

        const attributes = [
            Object.assign(mutils.createVariableDefinition('_name',t`Ime in priimek / naziv`,'text'), {required: true})
        ];

        let table = null;

        const button = new Button.default({
          label: t`Dodaj udeleženca`,
          onClick: async () => {
            const cmr = new ComponentManager.default();
            const record = await exports.record();
            cmr.model.attributes = attributes;

            const modal = new ModalDialog.default();
            record.default({
                $parent: modal,
                cm: cmr,
                saveOptions: {
                    beforeRequest: requestParameters => {
                        
                        if(!table) return;
                        
                        const attributes = requestParameters.attributes;
                        const row = {};
                        Object.keys(attributes).map(key => {
                            row[key.substring(1)] = attributes[key];
                        });

                        table.add(row);
                        modal.hide();
                        return false;
                    }
                }
            });
            modal.show();
          }
        });

        $container.append(button.$el());
        $container.append('<br>');

        table = await cm.add({
                key: titleKey.key,
                component: SimpleTable.default,
                options: {
                    attributes: attributes,
                    language: globals.language
                },
                $parent: $container
        });
    }

    async function oskodovanci(titleKey) {
            
        const $oskodovanciContainer = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);

        dmgu.personsComponents(cm, titleKey, {
            $container: $oskodovanciContainer,
            moduleTableName: 'dmg_affectees',
            selectDataTable: 'dmg_affectees_vw',
            affectees_data: initialData && initialData.affectees_data && JSON.parse(initialData.affectees_data),
            _affectees: initialData && initialData._affectees
        });
    }

    async function dmgDates(titleKey) {

        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);

        const $firstRow = $('<div/>');
        const $secondRow = $('<div/>');

        $container.append($firstRow);
        $container.append($secondRow);

        $firstRow.addClass('mbase2-inline-controls');
        $secondRow.addClass('mbase2-inline-controls');
        
        const attributes = utils.convertToAssocArray(dmgVariables, 'key_name_id');

        await addDateControl('_dmg_start_date', $firstRow, false, (date, format) => {
            
            if (!date) return;

            const mdate = moment(date);

            if (!mdate.isValid()) return;

            if (mdate.isAfter(new Date())) {
                alert('Izbran je datum v prihodnosti.');
            }
            
            ['_dmg_end_date',
            '_dmg_noticed_date'].map(key=>{
                const cmp = cm.get(key);
                if (cmp) {
                    !cmp.val() && cmp.dp.setDate(date);
                    cmp.$input.prop('disabled', false);
                    
                    const currentCmpDate = cmp.val();
                    if (currentCmpDate && moment(date).isAfter(currentCmpDate)) {
                        cmp.val(null);
                    }
                    
                    cmp.dp.setStartDate(date);
                    
                }
            })

        });

        await cm.add({
            key: '_dmg_date_type',
            component: Select2.default,
            $parent: $firstRow,
            options:{
                data: [{value: 1, text: 'Datum škode je OCENJEN.'}, {value: 2, text: 'Datum škode je UGOTOVLJEN.'}],
                label: attributes['_dmg_date_type'].t_name_id || '_dmg_date_type',
                required: true
            }
        });

        await addDateControl('_dmg_noticed_date',$secondRow, true, (date, format)=>{
            
            if (!date || !moment(date).isValid()) return;

            const cmp = cm.get('_dmg_end_date');
            if (cmp) {
                cmp.dp.setStartDate(date);
            }
        });
        
        await addDateControl('_dmg_end_date', $secondRow, true, (date, format) => {
            
            if (!date || !moment(date).isValid()) {
                const cmp = cm.get('_dmg_examination_date');
                cmp && cmp.val(null);
                return;
            }
            
            ['_dmg_examination_date'].map(key=>{
                const cmp = cm.get(key);
                if (cmp) {
                    !cmp.val() && cmp.dp.setDate(date);
                    cmp.$input.prop('disabled', false);

                    const currentCmpDate = cmp.val();
                    if (currentCmpDate && moment(date).isAfter(currentCmpDate)) {
                        cmp.val(null);
                    }

                    cmp.dp.setStartDate(date);
                }
            });

            const cmp = cm.get('_dmg_noticed_date');
            if (cmp) {
                cmp.dp.setEndDate(date);
            }

        });

        $firstRow.css('margin-bottom','10px');

        await addDateControl('_dmg_examination_date', $secondRow, true);

        ['_dmg_end_date',
        '_dmg_noticed_date',
        '_dmg_examination_date'].map(key => {
            if (initialData[key]) {
                const cmp = cm.get(key);
                cmp && cmp.$input.prop('disabled', false);
            }
        });

        ['_dmg_start_date', '_dmg_end_date'].map(key => {
            if (initialData[key]) {
                const cmp = cm.get(key);
                cmp && cmp.$input.trigger('change');
            }
        });

        const today = new Date();

        ['_dmg_start_date',
        '_dmg_end_date',
        '_dmg_noticed_date',
        '_dmg_examination_date'].map(key => {
            const cmp = cm.get(key);
            cmp && cmp.dp.setEndDate(today);
        });

        async function addDateControl(key, $container, disabled, onChange) {
            await cm.add({
                key: key,
                component: DateInputs.DateInput,
                $parent: $container,
                options:{
                    format:'dd.mm.yyyy',
                    startDate: '01.01.2020',
                    label: attributes[key].t_name_id || key,
                    required: true,
                    disabled: disabled,
                    onChange: onChange
                }
            });
        }
    }

    async function genetika(titleKey) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);

        const attributes = [
            Object.assign(mutils.createVariableDefinition('_sample_id',t`Koda`,'text'), {required: true}),
            Object.assign(mutils.createVariableDefinition('_sample_type',t`Tip vzorca`,'code_list_reference', 'sample_type_options'), {required: true }),
            mutils.createVariableDefinition('_description',t`Opis`,'text')
        ];

        const refValues = await mutils.getRefCodeListValues(attributes);

        let table = null;

        const button = new Button.default({
          label: t`Dodaj vzorec`,
          onClick: async () => {
            const cmr = new ComponentManager.default();
            const record = await exports.record();
            cmr.model.attributes = attributes;

            const modal = new ModalDialog.default();
            record.default({
                refValues: refValues,
                $parent: modal,
                cm: cmr,
                saveOptions: {
                    beforeRequest: requestParameters => {
                        
                        if(!table) return;
                        
                        const attributes = requestParameters.attributes;
                        const row = {};
                        Object.keys(attributes).map(key => {
                            row[key.substring(1)] = attributes[key];
                        });

                        table.add(row);
                        modal.hide();
                        return false;
                    }
                }
            });
            modal.show();
          }
        });

        $container.append(button.$el());
        $container.append('<br>');

        table = await cm.add({
                key: titleKey.key,
                component: SimpleTable.default,
                options: {
                    attributes: attributes,
                    language: globals.language,
                    codeListValues: utils.convertToAssocArray(refValues.codeListValues, 'id')
                },
                $parent: $container
        });

    }

    function posredniZahtevek() {
        if (!initialData._claim_id) return false;
        const parts =initialData._claim_id.split('/');
        if (parts.length < 5) return false;
        if (initialData.__indirect_counter != parts[4]) return false;
        return true;
    }

    function validateConditionsForCompleteClaimButton() {
        mutils.validateConditionsForCompleteButton($completeClaimButton, bookmarks);
    }

    function validate_claim_id(value) {
        const key = '_claim_id'

        cm.get(key).$error.empty();

        const data = selectedDeputyData();

        if (initialData._claim_id && initialData._deputy == data._uid) {//ne spreminjaj kadrovske kode, če je številka zahtevka že vpisana in če gre za istega pooblaščenca
            return true;
        }
        
        if (!data) {
            return false;
        }

        const splitted = value.split('/');

        if (posredniZahtevek()) {
            splitted.pop();
        }

        const oe = splitted[0];
        if (utils.strLeftPad(data._oe_id, 2) != oe) {
            return false;
        }

        const koda = splitted[1];
        if (koda != data._kadrovska_koda) {
            return false;
        }

        const stevilka = parseInt(splitted[2]);
        if (!stevilka) {
            return false;
        }

        const leto = splitted[3];
        if (parseInt(leto) != leto) {
            return false;
        }

        if (dmgClaims[value] && value != initialData._claim_id) {
            
            cm.get(key).$error.text(`Ta številka škodnega zahtevka že obstaja.`);
            return false;
        }

        return true;
    }

    function validateComponentValue(key, value, required, parentKey) {
        if (required === true) {
            if (utils.empty(value)) return false;
        }

        if (key === '_claim_id') {
            if (validate_claim_id(value) === false) {
                return false;
            }
        }
        else if (key === '_culprit') {
            const cmp = cm.get('_culprit_signs');
            if (!cmp) return false;
            if (utils.empty(cmp.val())) return false;
        }
        else if (parentKey === '_dmg_dates') {
            for (key of ['_dmg_start_date',
            '_dmg_end_date',
            '_dmg_noticed_date',
            '_dmg_date_type',
            '_dmg_examination_date']){
                const cmp = cm.get(key);
                if (!cmp) return false;
                if (!cmp.val()) return false;
            }

            const bm = bookmarks[parentKey];
            if (!bm) return false;
            bm.validated = true;

            return true;
        }
        else if (key==='_location_data') {
            if (!value) return false;
            if (!value.lat) return false;
            if (!value.lon) return false;
        }

        if (parentKey) {
            const bm = bookmarks[parentKey];
            if (!bm) return false;
            const cmp = cm.get(parentKey);
            if (!cmp) return false;

            bm.validated = validateComponentValue(parentKey, cmp.val(), bm.required, null);

            return bm.validated;
        }

        return true;
    }

    function createButtonGroup() {

        let buttons = [];
    
        if (!initialData._claim_status) {
            buttons = [
                {
                    key: 'save',
                    label: t`Shrani`,
                    onAppend: $btn => $btn.hide()
        
                },
                {
                    key: 'copy',
                    label: t`Kopiraj`,
                    title: t`Prenesi podatke v nov zahtevek`,
                    onAppend: $btn => {
                        $btn.hide();
                        $btn.prop('disabled', true);
                    }
                },
                {
                    key: 'complete',
                    label: t`Zaključi`,
                    title: t`Shrani in zaključi zahtevek`,
                    onAppend: $btn => $btn.hide()
                }
            ];
        }
        else if (initialData._claim_status === 1) {
            
            if (agreementsReached) {
                    buttons = [
                        {
                            key: 'posredni',
                            label: t`Naknadna škoda`,
                            onAppend: $btn => $btn.hide()
                        }
                    ];
            }
            else {
                buttons = [
                        {
                            key: 'agreement',
                            label: t`Uredi sporazum(e)`,
                            onAppend: $btn => $btn.hide()
                        },
                        {
                            key: 'copy',
                            label: t`Kopiraj`,
                            title: t`Prenesi podatke v nov zahtevek`,
                            onAppend: $btn => {
                                $btn.hide();
                                $btn.prop('disabled', true);
                            }
                        },
                        {
                            key: 'save',
                            label: t`Shrani`,
                            onAppend: $btn => $btn.hide()
                        }
                ];
            }
        }

        group = new ButtonGroup.default({
            highlight: false,
            classes: 'btn-primary btn-lg',
            onClick: async (data, key, self) => {

                if (key === 'copy') {
                    cm.get('_claim_id').val('');
                    cm.model.values.id = fid = null;
                    initialData = {};
                    setInitialStatus();
                    disable1(false);
                    disable2(false);
                    $breadcrumbs.html(utils.createBreadcrumbs(initialBreadcrumbs()));
                    group = createButtonGroup();
                    $buttonGroupContainer.empty().html(group.$el());
                    window.history.pushState({'id':null},"", window.location.pathname);
                    refreshButtonsState();
                    $header.find('span.extended-title').text(t`nov zahtevek`);
                    $saveFormButton && $saveFormButton.prop('disabled', false);
                    await reinitDeputiesSelector();
                    return;
                }

                if (key==='agreement') {
                    utils.hrefClick(window.location.pathname+'/sporazumi?fid=' + fid);
                    return;
                }

                const saveOptions = Object.assign(await mutils.saveButtonHelper(op.saveOptions), op.saveOptions || {});
                saveOptions.rootUrl = globals.apiRoot + '/mb2data';

                if (!initialData._claim_status && key === 'save') {
                    cm.model.saveOptions.validate = false;
                }
                else {
                    cm.model.saveOptions.validate = true;
                }
                
                cm.saveOptions.onSuccessCallbacks = [(cm, res) => {
                    self.prop('disabled', true);
                    if (key === 'complete') {
                        window.location.assign(window.location.pathname + '?fid=' + res.id);
                    }
                    else if (key === 'posredni') {
                        window.location.assign(window.location.pathname + '?fid=' + res.id);
                        return;
                    }
                    else if (key === 'save') {
                        cm.model.values.id = fid = res.id;
                        window.history.pushState({'id':res.id},"", window.location.pathname + '?fid=' + res.id);
                        refreshButtonsState();
                    }
                }];

                saveOptions.onValidationStart = component => {
                    console.log('component', component)
                }

                saveOptions.beforeRequest = async request => {
                        
                    if (key === 'complete') {
                        request.attributes[':_claim_status'] = 1;
                    }
                    else if (key === 'posredni' && cm.model.values && cm.model.values.id!==undefined) {
                        
                        request.url = globals.apiRoot + '/dmg-indirect-claim/' + cm.model.values.id;
                        request.method = 'POST';
                    }
                    else if (key === 'save') {
                        if (!request.attributes[':_claim_status']) {
                            if (posredniZahtevek()) {
                                delete request.attributes[':_claim_id'];
                            }
                            else if (!validate_claim_id(cm.get('_claim_id').val())) {
                                request.attributes[':_claim_id'] = null;
                            }
                        }
                    }
                    
                    request.attributes[':_uid'] = true;

                    if (request.attributes[':_dmg_objects']) {
                        const dmgObjectIds = [];
                        for(const obj of request.attributes[':_dmg_objects']) {
                            dmgObjectIds.push(obj.dmg_object_id);
                        }
                        request.attributes[':_dmg_object_list_ids'] = dmgObjectIds;
                    }
                    
                }

                cm.model.tableName = 'dmg';
                cm.save(saveOptions);
            },
            buttons: buttons
        });

        return group;
    }

    function setInitialStatus() {
        $status.empty();
        $status.append($nezakljucen);
        $status.append($neshranjen);
    }

    /**
     * disable controls when claim is completed zakljucen1
     */
    function disable1(disabled=true) {
        cm.get('_claim_id').$el().find('input:first').prop('disabled', disabled);
        cm.get('_deputy').disabled(disabled);
    }

    /**
     * disable controls when claim is completed zakljucen2
     */
    function disable2(disabled = true, showPosredniButton = true) {
        if (disabled === true) {
            $right.find('.panel-group .panel').addClass('disable-mouse-events');
            $header.find('.breadcrumb li:eq(1)').html('<strong>pregled zahtevka</strong>');
            $header.find('.breadcrumb li:last a').text('pregled sporazumov');
            
            group.buttons.map(btn => btn.$btn.hide());

            const $el = cm.get('acc__dmg_objects').$el();
            $el.find('.panel').removeClass('disable-mouse-events');
            $el.find('button').addClass('disable-mouse-events').hide();
            $el.find('.panel-body div:first').addClass('disable-mouse-events');
        }
        else {
            $right.find('.panel-group .panel').removeClass('disable-mouse-events');
        }

        if (showPosredniButton) {
            const posredniBtnCmp = group.buttons.find(btn => btn.key === 'posredni');
            !posredniZahtevek() && posredniBtnCmp && posredniBtnCmp.$btn.show();
        }
    }

    function initialBreadcrumbs() {
        return [
            {
                text: t`Pregled zahtevkov`,
                href: window.location.origin + '/mbase2/modules/dmg'
            },
            {
                text: podnaslov,
                class: 'active',
                href: window.location.href
            }
        ];
    }

    /**
     * Adds key and text properties to individual tree data items
     * @param {array} treeData 
     * @param {object} culpritsCodeList
     */

    function convertTreeData(treeData, culpritsCodeList) {
        //convert treeData
        treeData.map(td => {
            const codeListItem = culpritsCodeList[td.text];
            
            if (codeListItem) {
                td.key = codeListItem.key;
                if (codeListItem.translations) {
                    const values = utils.jsonParse(codeListItem.translations);
                    if (values[globals.language]) {
                        td.text = values[globals.language];
                    }
                }
            }

            if (td.nodes) {
                convertTreeData(td.nodes, culpritsCodeList);
            }
        });
    }
}
