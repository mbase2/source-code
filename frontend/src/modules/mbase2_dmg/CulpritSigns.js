function CulpritSigns(op={}) {
    const $el = $('<div/>');
    this.$el = () => $el;
    this.op = op;

    this.signs = {
        "Značilne sveže sledi (stopinje, dlaka, perje, iztrebki itd.) na škodnem objektu ali v neposredni bližini": [],
        "Sledi močnih udarcev (močne podplutbe, zmečkanine, kri v ustni votlini ipd.)": [
            "Ursus arctos"
        ],
        "Značilen ugriz v vrat": [
            "Mustela putorius",
            "Felis silvestris",
            "Lynx lynx",
            "Ursus arctos",
            "Canis lupus",
            "Unprotected species (also a dog)",
            "Other"
        ],
        "Obsežne poškodbe z velikimi ranami v predelu grla ali vratu": [
            "Canis lupus",
            "Unprotected species (also a dog)"
        ],
        "Več ugrizov na trebuhu, nogah, gobcu": [
            "Canis lupus",
            "Unprotected species (also a dog)",
            "Ursus arctos"
        ],
        "Več ugriznih ran po celem telesu": [
            "Mustela putorius",
            "Felis silvestris",
            "Mustela nivalis",
            "Mustela erminea",
            "Lynx lynx",
            "Ursus arctos",
            "Canis lupus",
            "Unprotected species (also a dog)",
            "Other"
        ],
        "Požrto vime": [
            "Canis lupus",
            "Unprotected species (also a dog)",
            "Ursus arctos"
        ],
        "Požrti notranji organi": [
            "Canis lupus",
            "Unprotected species (also a dog)",
            "Ursus arctos",
            "Corvus corax"
        ],
        "Požrte mišice, notranji organi nedotaknjeni": [
            "Lynx lynx"
        ],
        "Koža slečena s kadavra": [
            "Lynx lynx"
        ],
        "Počena lobanja": [
            "Canis lupus",
            "Ursus arctos"
        ],
        "Zlomljena hrbtenica": [
            "Canis lupus",
            "Ursus arctos"
        ],
        "Značilen razmak med podočniki (60 mm – 90 mm)": [
            "Ursus arctos"
        ],
        "Značilen razmak med podočniki (35 mm – 40 mm)": [
            "Canis lupus"
        ],
        "Značilen razmak med podočniki (30 mm – 35 mm)": [
            "Lynx lynx"
        ],
        "Pokončane živali niso požrte": [
            "Canis lupus",
            "Mustela putorius"
        ],
        "Površinske praske na telesu": [
            "Canis lupus",
            "Unprotected species (also a dog)"
        ],
        "Oglodani uhlji": [
            "Unprotected species (also a dog)"
        ],
        "Plen je premeščen/odvlečen z mesta uboja": [
            "Canis lupus",
            "Ursus arctos"
        ],
        "Plen je zakopan": [
            "Ursus arctos"
        ],
        "Populjena dlaka ali perje ob kadavru": [
            "Buteo buteo",
            "Accipiter gentilis",
            "Aquila chrysaetos",
            "Other species of a bird of prey or an owl"
        ],
        "Način poškodovanja škodnega objekta": [],
        "Način poškodovanja varovalnega objekta (ograje, ograde ipd.)": [
            "Ursus arctos",
            "Canis lupus"
        ],
        "Predhodno pojavljanje istovrstnih škod v okolici z zanesljivo prepoznanim povzročiteljem": [],
        "Povzročitelj opažen ali posnet v okolici nastanka škode pred/po nastanku škode": [],
        "Pooblaščencu je bil prikazan slikovni material": [],
        "Poročilo ali zapisnik pooblaščene osebe (veterinar, policist ipd.)": [],
        "Drugo": []
    };

    op.signs = Object.keys(this.signs);

    this.$selectDiv = $('<div/>');

    this.inputs = {};

    $el.append(this.$selectDiv);

    this.dropdown(op.selectedCulprit);
}

CulpritSigns.prototype.dropdown = function(culprit) {
    const op = this.op;
    const $el = this.$el();

    let signs = [];

    if (culprit) {
        Object.keys(this.signs).map(key => {
            const a = this.signs[key];
            if (a.indexOf(culprit) !== -1) {
                signs.push(key);
            }
            else if (a.length === 0) {
                signs.push(key);
            }
        })
    }

    if (signs.length === 0) {
        signs = Object.keys(this.signs);
    }

    this.select = new op.imports.TTomSelect.default({
        multiple: true,
        data: signs,
        label: op.label,
        required: op.required,
        additionalPlugins: ['remove_button'],
        onUnselect: e => {
            if (!e) return;
            const key = signs[e];
            const input = this.inputs[key];
            if (!input) return;
            input.$el().remove();
            delete this.inputs[key];
            $el.trigger('change');
        },
        onSelect: e => {

            if (!e) return;
            if (e.length === 0) return;
            
            const value = e[e.length-1];
            const key = signs[value];
            const keys = Object.keys(this.inputs);
            
            if (keys.indexOf(key)===-1) {
                this._createInput(key);
            }

            $el.trigger('change');
        }
    });

    this.$selectDiv.html(this.select.$el());
}

CulpritSigns.prototype._createInput = function(key) {
    const input = new this.op.imports.Inputs.Input({
        label: key,
        type: 'text'
    });

    this.$el().append(input.$el());

    this.inputs[key] = input;
}

CulpritSigns.prototype.val = function(value) {
    if (value!==undefined){
        Object.keys(this.inputs).map(key => {
            this.inputs[key].$el().remove();
            delete this.inputs[key];
        });

        this.inputs = {};

        const keys = [];
        value.map(v => {
            const key = v.key;
            keys.push(key);
            this._createInput(key);
            this.inputs[key].val(v.value);
        });

        this.select && this.select.val(keys);
    }

    return Object.keys(this.inputs).map(key => ({
        key: key,
        value: this.inputs[key].val()
    }));
}

export default CulpritSigns;