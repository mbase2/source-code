import * as exports from '../../libs/exports';
import globals from '../../app-globals';

export default async op => {
    const [
        Inputs,
        Button,
        mutils
    ] = await Promise.all([
        exports.Inputs(),
        exports.Button(),
        exports.mutils()
    ]);

    op.$parent.append('<h2>DMG DB editor</h2>');

    op.$parent.append('<h3>Preimenovanje škodnega primera</h3>');

    const claimIdCmp1 = new Inputs.Input({
        label: 'Številka obstoječega škodnega primera'
    });

    const claimIdCmp2 = new Inputs.Input({
        label: 'Številka novega škodnega primera'
    });

    const $renameStatus = $('<div/>');
    const $deleteStatus = $('<div/>');

    const renameBtnCmp = new Button.default({
        label: 'Preimenuj',
        onClick: async () => {
            $renameStatus.empty();
            const claimIdOld = claimIdCmp1.val().trim();
            const claimIdNew = claimIdCmp2.val().trim();

            if (!claimIdOld || !claimIdNew) {
                alert('Številka škodnega primera ne sme biti prazna.');
                return;
            }

            if (confirm(`Ali res želiš preimenovati ${claimIdOld} v ${claimIdNew}?`)) {
            
                const res = await mutils.requestHelper(globals.apiRoot + `/dmg-admin-editor/rename`, 'PUT', {
                    claimIdOld, claimIdNew 
                }, {
                    onError: (a,b) => {
                        $renameStatus.append(mutils.alertCode(b.err, '', 'danger'));
                    }
                }, op.$parent);

                $renameStatus.append(mutils.alertCode("Število posodobljenih zahtevkov: " + (res || 0) , '','info'));
            }
        }
    });

    let $div = $('<div/>',{style:'display:flex; gap:10px'});
    $div.append(claimIdCmp1.$el());
    $div.append(claimIdCmp2.$el());
    $div.append(renameBtnCmp.$el());
    $div.append($renameStatus);

    op.$parent.append($div);

    op.$parent.append('<hr>');

    op.$parent.append('<h3>Brisanje škodnega primera</h3>');

    const claimIdCmp = new Inputs.Input({
        label: 'Številka škodnega primera za brisanje'
    });

    const deleteBtnCmp = new Button.default({
        label: 'Briši',
        onClick: async () => {
            $deleteStatus.empty();
            const claimId = claimIdCmp.val().trim();

            if (!claimId) {
                alert('Številka škodnega primera ne sme biti prazna.');
                return;
            }

            const confirmClaimId = prompt(`Še enkrat vpiši številko škodnega primera, ki ga želiš zbrisati!`);

            if (claimId !== confirmClaimId) {
                alert('Številka škodnega primera za brisanje ni bila potrjena z vnosom iste številke.');
                return;
            }

            if (confirm(`Ali res želiš brisati primer št. ${claimId}?`)) {
                const res = await mutils.requestHelper(globals.apiRoot + `/dmg-admin-editor/delete`, 'DELETE', {
                    claimId 
                }, {
                    onError: (a, b) => {
                        $deleteStatus.append(mutils.alertCode(b.err, '', 'danger'));
                    }
                }, op.$parent);

                $deleteStatus.append(mutils.alertCode("Število izbrisanih zahtevkov: " + (res || 0)));
            }
        }
    });

    $div = $('<div/>',{style:'display:flex; gap:10px'});
    $div.append(claimIdCmp.$el());
    $div.append(deleteBtnCmp.$el());
    $div.append($deleteStatus);

    op.$parent.append($div);
}