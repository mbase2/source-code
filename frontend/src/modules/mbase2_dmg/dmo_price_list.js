import * as exports from '../../libs/exports';
import globals from '../../app-globals';

import * as fup from '../../helpers/fileUpload';

import * as mutils from '../../libs/mbase2_utils';

import * as utils from '../../libs/utils';

export default async op => {
    const {$parent} = op;

    let fileUploadCm = null;

    fileUploadCm = await fup.default({onSelect: onDataFileSelect, $parent: $parent, subfolder: 'dmg'});

    async function onDataFileSelect(e) {

        if (!fileUploadCm) return;

        const pages = await mutils.requestHelper(globals.apiRoot + `/dmg-parse-price-list/${fileUploadCm.get('select').val()}`, 'POST', {
            valid_till: utils.getQueryParameter('valid_till')
        }); 

        const Button =  await exports.Button();

        const uploadBtn = new Button.default({
            label:`Apply`,
            type: 'btn-primary',
            classes: 'btn-lg',
            onClick: () => {
            },
            $parent: $parent
        });

    }
}
