const DmgTable = function (op = {}) {

    const {cols, initialData} = op;

    this.op = op;

    this.cols = cols;

    const fixedPriceValues = this.op.fixedPriceValues === undefined ? false : this.op.fixedPriceValues;

    this.op.fixedPriceValues = fixedPriceValues;

    this.attributes = createAttributes(cols, fixedPriceValues);

    if (!fixedPriceValues) {
        this.attributes.push({
            key_name_id: op.imports.utils.t`Razlog za spremembo`,
            key_data_type_id: 'text',
            required: true,
            
            _component: {
                default: true,
                additionalComponentOptions: {
                    validate: false
                }
            }
        });
    }

    const $table = $(`<table class="table table-striped table-bordered"></table>`);
    const $thead = $('<thead/>');
    $thead.html(`<tr>${cols.map(c => `<th>${c.key}</th>`).join('')}</tr>`)
    $table.append($thead);

    this.$el = () => $table;

    this.setData(initialData);
}

function createAttributes(cols, fixedPriceValues = true) {
    
    const attributes = [];
    cols.map(c => {
        const prop = {
            key_name_id: c.key,
            key_data_type_id: 'text'
        };

        let additionalComponentOptions = null;
        
        if (c.read_only===true) {
            additionalComponentOptions = {
                read_only: true
            }
        }
        
        if (c.required===true) {
            additionalComponentOptions = additionalComponentOptions || {};
            additionalComponentOptions.required = true;
        }
        
        if (c.onChange) {
            additionalComponentOptions = additionalComponentOptions || {};
            additionalComponentOptions.onChange = c.onChange;
        }

        if (!fixedPriceValues && c.key !== 'Priznana vrednost') {
            additionalComponentOptions = additionalComponentOptions || {};
            additionalComponentOptions.read_only = true;
        }

        if (c.key !== 'Opis' && c.key !== 'Enota' && c.key !== 'Šk. objekt') {
            additionalComponentOptions = additionalComponentOptions || {};
            additionalComponentOptions.type = 'number';
            additionalComponentOptions.step = 0.01;
        }

        if (c.read_only !== undefined) {
            additionalComponentOptions.read_only = c.read_only;
        }

        if (additionalComponentOptions) {
            prop._component = {
                default: true,
                additionalComponentOptions: additionalComponentOptions
            };
        }

        attributes.push(prop);
    });

    return attributes;
}

DmgTable.prototype.setData = function (data) {
    if (!data) return;

    const cols = this.op.cols;
    if (this.op.fixedPriceValues) {

        this.fixedRowTypes = data.map(d => d.custom);
        this.data = data.map(d => Object.assign([], d.row));
    }
    else {
        this.data = data.map(d => Object.assign([], d));
    }
    
    this.data.map((dataRow, inx) => {

        const $a = $(`<a href="#" class="table-action"><span class="fa fa-edit"></span></a>`);

        if (this.op.fixedPriceValues) {
            $a.on('click', async () => {
                this.fixedPriceValuesModal(inx,cols, this.fixedRowTypes[inx]);
            });   
        }
        else {
            const $priznano = $('<div/>');
            const $razlog = $('<div/>',{style:'font-size:0.85em'});

            const cKolicina = this.cols.find(c => c.key === 'Količina') || {};

            $a.on('click', async () => {
                const cmr = new this.op.imports.ComponentManager.default({
                    onComponentChange: (key, component, cm) => {
                        this.op.onComponentChange && this.op.onComponentChange(key, component, cm);
                    }
                });
                const record = await this.op.imports.record();
                cmr.model.attributes = [...this.attributes];

                let drugo = false;

                if (dataRow[0].key === 'Drugo' && cols[0].key === 'Opis') {
                    drugo =  true;
                    cmr.model.attributes[0]._component.additionalComponentOptions.read_only = false;
                    cmr.model.attributes[1]._component.additionalComponentOptions.read_only = true;
                    cmr.model.attributes[6]._component.additionalComponentOptions.required = false;
                }
                
                const modelValues = {};
                cols.map((c, inx) => {
                    modelValues[c.key] = inx === 0 ? dataRow[inx].key : dataRow[inx];
                });
                
                cmr.model.values = modelValues;

                const $obj = cmr.model.values['Priznana vrednost'];
                cmr.model.values['Priznana vrednost'] = $obj.find('div:first').text();
                cmr.model.values['Razlog za spremembo'] = $obj.find('div:last').text();

                if (drugo) {
                    cmr.model.values['Opis'] = dataRow[0].value;
                }

                cmr.model.values.id = -1;
                
                const modal = new this.op.imports.ModalDialog.default();
                record.default({
                    $parent: modal,
                    cm: cmr,
                    onInit: () => {
                        if (drugo) {
                            cmr.get('Razlog za spremembo').$el().hide();
                        }
                    },
                    beforeSave: () => {
                        if (cols[0].key !== 'Opis') return;

                        if (Math.abs(parseFloat(cmr.get('Cena').val())-parseFloat(cmr.get('Priznana vrednost').val()))<1e-6) {
                            cmr.get('Razlog za spremembo').$el().find('input').removeAttr('required');
                        }
                        else {
                            cmr.get('Razlog za spremembo').$el().attr('required','');
                        }
                    },
                    saveOptions: {
                        beforeRequest: requestParameters => {
                            const attributes = requestParameters.attributes;
                            const priznano = attributes[':Priznana vrednost'];
                            $priznano.text(priznano);
                            const razlog = attributes[':Razlog za spremembo'];
                            $razlog.text(razlog);
                            
                            if (cKolicina.read_only === false && !drugo) {
                                dataRow[1] = attributes[':Količina'];
                                dataRow[4] = attributes[':Cena'];
                                const $tr = this.findRow(inx);
                                $tr.find('td:nth(1)').text(attributes[':Količina']);
                                $tr.find('td:nth(4)').text(attributes[':Cena']);
                            }
                            else if (drugo) {
                                dataRow[0].value = attributes[':Opis'];
                                const $tr = this.findRow(inx);
                                $tr.find('td:nth(0)').text(attributes[':Opis']);
                            }

                            this.$el().trigger('change');
                            modal.hide();
                            return false;
                        }
                    }
                });
                modal.show();
            });   

            const priznanaVrednostInx = this.op.cols.findIndex(c => c.key === 'Priznana vrednost');
            let priznano = dataRow[priznanaVrednostInx];

            let valPriznano;

            if (Array.isArray(priznano)) {
                valPriznano = priznano[0];
            }
            else {
                valPriznano = priznano;
            }

            const fval = parseFloat(valPriznano);
            
            if (!isNaN(fval)) {
                valPriznano = fval.toFixed(2);

                if (Array.isArray(priznano)) {
                    priznano[0] = valPriznano;
                }
                else {
                    priznano = valPriznano;
                }
            }

            const $div = $('<div/>');
            dataRow[priznanaVrednostInx] = $div;
            
            $div.append($priznano);
            $div.append($razlog);

            Array.isArray(priznano) ? $priznano.text(priznano[0]) : $priznano.text(priznano);

            Array.isArray(priznano) && $razlog.text(priznano[1]);
        }

        const $divControls = $('<div/>');
        $divControls.append($a);

        if (this.op.iconRemove) {
            $divControls.append('&nbsp;&nbsp;&nbsp;');
            const $remove = $(`<a href="#" class="table-action"><span class="pficon pficon-delete"></span></a>`);
            $remove.on('click',() => {
                const data = this.val();
                data.splice(inx, 1);
                this.val(data);
            });
            $divControls.append($remove);
        }

        dataRow.push($divControls);
    });

    const $tbody = $('<tbody/>');

    this.$el().find('tbody').remove();

    const itemNameKey = this.op.fixedPriceValues ? (this.op.itemNameKey || 'Opis') : (this.op.itemNameKey || 'Šk. objekt');

    const imeObjektaInx = this.op.cols.findIndex(c => c.key === itemNameKey);
   
    this.data.map((dataRow, inx) => {
        const $tr = $('<tr/>');
        $tr.data('inx', inx);
        console.log('dataRow', dataRow)
        dataRow.map((cellValue, inx) => {

            if (this.op.fixedPriceValues) {
                ;//todo izračun v celico
            }

            if (inx === imeObjektaInx) {
                cellValue = this.op.fixedPriceValues ? cellValue : (cellValue.value || cellValue.key);
            }
            else {
                const fval = parseFloat(cellValue);
                
                if (!isNaN(fval)) {
                    cellValue = fval.toFixed(2);
                }

            }
            
            const $td = $('<td/>');
            $td.html(cellValue);
            $tr.append($td);
        });
        $tbody.append($tr);
    });

    this.$el().append($tbody);

    if (data.length === 0) {
        this.$el().hide();
    }
    else {
        this.$el().show();
    }
    
    this.$el().trigger('change');
}

DmgTable.prototype.findRow = function(inx) {
    const trows = this.$el().find('tr');
    for (let i=0; i<trows.length; i++) {
        const $tr=$(trows[i]);
        if ($tr.data('inx') == inx) return $tr;
    }
}

DmgTable.prototype.fixedFieldsProperties = function(custom) {
    const ncols = Object.assign([],this.cols);

    if (custom) {
        [1, 2, 3].map(i => ncols[i].read_only = true);
        ncols[0].read_only = false;
        ncols[4].read_only = false;
        ncols[0].required = true;
        ncols[4].required = true;
    }
    else {
        ncols[1].required = true;
        ncols[1].read_only = false;
        
        ncols[1].onChange = (_inputs) => {
            ;
        }

        [0, 2, 3, 4].map(i => ncols[i].read_only = true);
    }

    return ncols;
}

DmgTable.prototype.fixedPriceValuesModal = async function(inx, cols = null, custom = null) {

    let attributes;
    if (!cols) {
        cols = this.cols;
        attributes = this.attributes;
    }
    else {
        attributes = createAttributes(this.fixedFieldsProperties(custom), true);  //fixedprice values
    }

    const cmr = new this.op.imports.ComponentManager.default();
    const record = await this.op.imports.record();
    cmr.model.attributes = attributes;

    const d = Array.isArray(inx) ? inx : this.data[inx];
    
    const modelValues = {};
    this.cols.map((c, inx) => {
        modelValues[c.key] = d[inx];
    });
    
    cmr.model.values = modelValues;

    cmr.model.values.id = -1;
    const modal = new this.op.imports.ModalDialog.default();
    record.default({
        $parent: modal,
        cm: cmr,
        saveOptions: {
            beforeRequest: requestParameters => {
                const data = this.val();

                if (custom === false) {
                    requestParameters.attributes[':Cena'] = (parseFloat(requestParameters.attributes[':Cena/enoto']) * parseFloat(requestParameters.attributes[':Količina'])).toFixed(2);
                }

                if (Array.isArray(inx)) {
                    const row = [];

                    attributes.map((a,i) => {
                        row.push(requestParameters.attributes[':'+a.key_name_id]);
                    });

                    data.push({
                        custom: custom,
                        row: row
                    });
                }
                else {
                    const row = data[inx].row;

                    attributes.map((a,i) => {
                        row[i] = requestParameters.attributes[':'+a.key_name_id];
                    });
                }

                modal.hide();
                
                this.val(data);
                return false;
            }
        }
    });
    modal.show();
}

DmgTable.prototype.znesek = function() {
    let sum = 0.0;
    const data = this.data;

    if (!data) return sum;
    
    if (this.op.fixedPriceValues) {
        data.map(row => {
            sum = sum + parseFloat(row[4]);
        });
    }
    else {
        data.map(row => {
            sum = sum + parseFloat(row[5].find('div:first').text() || 0);
        });
    }

    return sum;
}

DmgTable.prototype.val = function (values) {

    if (values !== undefined) {
        this.setData(values);
        return values;
    }

    const rval = [];
    const priznanaVrednostInx = this.op.fixedPriceValues ? null : this.op.cols.findIndex(c => c.key === 'Priznana vrednost');
    this.data && this.data.map((_row, inx) => {
        let row = Object.assign([],_row);
        if (priznanaVrednostInx !== null) {
            const value = row[priznanaVrednostInx];
            row[priznanaVrednostInx] = [value.find('div:first').text(), value.find('div:last').text()];
        }
        row.pop();
        
        if (this.op.fixedPriceValues) {
            row = {
                custom: this.fixedRowTypes[inx],
                row: row
            }
        }

        rval.push(row);
    });
    
    return rval;
}

export default DmgTable;