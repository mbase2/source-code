import * as exports from '../../libs/exports';
import globals from '../../app-globals';

let ComponentManager, ModalDialog, mutils, utils, Inputs, Button;

export default async (exports) => {
    [
        ComponentManager, 
        ModalDialog,
        mutils,
        utils,
        Inputs,
        Button
    ] = await Promise.all([
        exports.ComponentManager(),
        exports.ModalDialog(),
        exports.mutils(),
        exports.utils(),
        exports.Inputs(),
        exports.Button()
    ]);
};

export const personText = d => `${d._full_name}, ${d._street} ${d._house_number}, ${parseInt(d._post) === -1 ? d._foreign_post : d._post}`;

export const $containers = $parent => {
    const $left =  $('<div style="float: left; height:100%;  width:20%; max-height: calc(100vh - 187px); overflow: auto"/>');
    const $right = $('<div style="float: right; height:100%; width:80%; max-height: calc(100vh - 187px); overflow: auto; border-left: solid black 1px; padding:10px"/>');
    $parent.append($left);
    $parent.append($right);

    return {
        $left: $left,
        $right: $right
    }
}

export const initSections = (sections, bookmarks, cm, $left) => {
    for (let section of sections) {
        const required = section.required;

        if (section.parent) {
            bookmarks[section.key] = {
                parent: section.parent,
                required: required
            };
            continue;
        }

        const title = section.title;
        
        const $bm = bookmark({
            title: title, 
            required: required
        }); 

        $bm.on('click', () => {
            const panel = cm.get('acc_'+section.key);
            if (!panel) return;
            
            const lref = cm.get('_location_reference');
            lref && lref.$el().hide(); /*without hidding fixed height element
            the top margin of container div was ignored after scrolling (any) element into the view
            */
            panel.$el()[0].scrollIntoView();
            lref && lref.$el().show();
        })
        
        bookmarks[section.key] = {
            $bm: $bm,
            required: required
        };

        $left.append($bm);
    }
}

export const bookmark = (op) => {
    return $(`<div class="bookmark bookmark-${op.required ? 'required' : 'optional' }">
        <h5>${op.title}${op.required === false ? '' : '*'}</h5>
    </div>`);
}

export const personVariablesDefinition = async (moduleTableName) => {
    const personVariables = mutils.sortVariables(await mutils.requestHelper(globals.apiRoot + `/module_variables_vw/language/${globals.language}?:module_name=${moduleTableName}`));
    const refValues = await mutils.getRefCodeListValues(personVariables);

    return {
        personVariables: personVariables,
        refValues: refValues
    }
}

/**
 * 
 * @param {jquery DOM} op.$container
 * @param {object} op.t translations
 * @param {string} op.selectDataTable 
 * @param {string} op.moduleTableName
 */

export async function personsComponents(cm, titleKey, _op={}) {

    const op = Object.assign({
        t: {}
    },_op);

    const utils = await exports.utils();

    const t = utils.t;

    const {personVariables, refValues} = await personVariablesDefinition(op.moduleTableName);
    
    const cdef = exports.select({
        url: globals.apiRoot + '/mb2data/'+op.selectDataTable+'?json_flags=0',
        $parent: op.$container,
        preprocess: data => {
            const affecteesById = {};
            data.map(d => {
                if (!affecteesById[d.affectee_id]) {
                    affecteesById[d.affectee_id] = d;
                }
                else {
                    if (d.id > affecteesById[d.affectee_id].id) {
                        affecteesById[d.affectee_id] = d;
                    }
                }
            });

            //add previous versions of affectee data

            _op._affectees && _op._affectees.map((aid,inx) => {
                if (affecteesById[aid] && affecteesById[aid].id != aid) {
                    const adata = data.find(d => d.id == aid);
                    if (adata) {
                        affecteesById[aid + '-' + inx] = adata;
                    }
                } 
            });

            return Object.values(affecteesById);
        },
        process: d => [d.id, personText(d)]
    }, false);

    const recordEditorOptions = personsRecordEditorOptions(titleKey, personVariables);

    const TTomSelect = await exports.TTomSelect();

    cdef.key = titleKey.key;
    cdef.component = TTomSelect.default;
    cdef.options.multiple = true;
    cdef.options.additionalPlugins = ['remove_button'];

    cdef.options.placeholder = op.t['Dodaj oškodovance iz seznama'] || t`Dodaj oškodovance iz seznama`;
    
    cdef.options.tags = true;

    cdef.options.configurationOverrides= {
        render: {
            option_create: function(data, escape) {
                return '<div class="create">'+ t`Nov vnos: naziv oškodovanca` +' <strong>' + escape(data.input) + '</strong>&hellip;</div>';
            },
            item: function(data, escape) {
                var $a = $('<a href="#"></a>');
                const $div = $('<div/>');
                const storedAffecteeData = _op.affectees_data && _op.affectees_data.find(a => a.id == data.value);
                const currentAffecteeData = cdef.data.model.originalData.find(d => d.id == data.value);

                const equal = mutils.compareStoredAndCurrentAffecteeData(storedAffecteeData, currentAffecteeData);

                if (storedAffecteeData && !storedAffecteeData._post) {
                    storedAffecteeData._post = mutils.getPostName(storedAffecteeData._post_number, personVariables, refValues);
                }
                
                $a.text(equal ? data.text : personText(storedAffecteeData));
                $a.on('click', () => {
                    personUpsert(op.moduleTableName, true, data.value, recordEditorOptions);
                    return false;
                })
                $div.append($a);
                return $div[0];
            },
        },
        create: true,
        createFilter: function (value) {
            for (const key of Object.keys(this.options)) {
                if (this.options[key].text == value) return false;
            }
            return true;
        }
    };

    cdef.options.onSelect = (e, added) => {
        if (!added) return;
        personUpsert(op.moduleTableName, false, null, Object.assign(recordEditorOptions, {
            text: added.data.text, newOption: true
        }));
    };

    await cm.add(cdef);

    function personsRecordEditorOptions(titleKey, personVariables) {
        return {

            onShown: (cmr, modal, data) => {
                cmr.get('_full_name').$el().find('input:first').focus().select();
                const affecteeId = cmr.model.values.id;
                if (affecteeId && !(data && data._existingAffecteeId)) {
                    
                    const btn = new Button.default({
                        label: t`Add new data for this affectee`,
                        onClick: async () => {
                            modal.destroy();
                            const title = t`New record for existing affectee` + ': ' + cmr.model.values._full_name + ` (ID: ${affecteeId})`;
                            await personUpsert(op.moduleTableName, false, affecteeId, 
                                Object.assign({}, recordEditorOptions, {title}));
                        }
                    });

                    btn.$el().css('display','flex');

                    modal.$footer.append(btn.$el());
                }
            },

            onClose: (update, data, cm) => {
                const cmp = cm.get(titleKey.key);
                cmp.tselect.removeOption(data.text)
            },

            refValues: refValues,
            cm: cm,
            titleKey: titleKey,
            personVariables: personVariables,

            onSuccessfullySaved: (_cm, values, personVariables, refValues, update, _data, cm) => {
                const listId = mutils.getReferenceTableListId('poste', personVariables);
                if (listId) {
                    const option = refValues.tableReferences.find(item => item.list_id == listId && item.id == values._post_number)
                    if (option) {
                        values._post = option.key;
                        const cmp = cm.get(titleKey.key);
                        const data = cm.getData(titleKey.key);
                        
                        if (update || _data._existingAffecteeId) {
                            const itemInx = data.findIndex(d => d.id == values.id);
                            if (itemInx !== -1) {
                                data[itemInx] = values;
                                cmp.tselect.updateOption(_data._existingAffecteeId || values.id, {value: values.id, text: personText(values)});
                            }
                        }
                        else {
                            data.push(values);
                            cmp.tselect.addOption({value: values.id, text: personText(values)});
                        }
                        
                        cmp.tselect.addItem(values.id);
                    }
                }
            }
        }
    }
}

export async function personUpsert(moduleTableName, update = false, oid = null, data = {}) {
    const cm = data.cm;
    const titleKey = data.titleKey;
    const cmr = new ComponentManager.default();

    const utils = await exports.utils();

    const t = utils.t;

    data._existingAffecteeId = 0;

    if (update === true) {
        if (!oid) return;
        cmr.model.values = cm.getData(titleKey.key).find(d => d.id == oid);
    }
    else if (oid) { //new data for existing affectee
        cmr.model.values = cm.getData(titleKey.key).find(d => d.id == oid);
        data._existingAffecteeId = cmr.model.values.id;
    }
    else {
        if (data.newOption && data.text) {
            cmr.model.values = {
                _full_name: data.text
            }
        }
    }

    const record = await exports.record();

    cmr.model.attributes = Object.assign([], data.personVariables);
    cmr.model.attributes.find(a => a.key_name_id === '_foreign_post').required = true;
    cmr.model.tableName = moduleTableName;
    const modal = new ModalDialog.default({
        onShown: (modal) => data.onShown && data.onShown(cmr,modal, data),
        onClose: () => data.onClose && data.onClose(update, data, cm)
    });
    record.default({
        $parent: modal,
        cm: cmr,
        title: data.title,
        saveOptions: {
            rootUrl: globals.apiRoot + '/mb2data',
            urlParameters: ['json_flags=0'],
            beforeRequest: request => {
                const attributes = request.attributes;

                const affecteeId = cmr.model.values.affectee_id;
                
                if (affecteeId) {
                    attributes[':affectee_id'] = affecteeId;
                }

                if (data._existingAffecteeId) {
                    request.method='POST';
                }

                return true;
            }
        },
        onInit: () => {
            const add = cmr.model.values && cmr.model.values.id ? false : true;
            const slovenijaId = (cmr.getData('_country').find(c => c.key === 'Slovenia') || {}).id;

            const postCmp = cmr.get('_post_number');
            const countryCmp = cmr.get('_country');
            const citizenShipCmp = cmr.get('_citizenship');
            const foreignPostCmp = cmr.get('_foreign_post');
            const phoneCmp = cmr.get('_phone');

            phoneCmp && phoneCmp.$help && phoneCmp.$help.text(t`telefonska številka brez ločil, lahko vsebuje presledke, če je tuja telefonska številka se mora začeti z izhodno številko Slovenije 00`);

            foreignPostCmp.$el().hide();
            foreignPostCmp.skipValidation = true;

            if (add) {
                countryCmp.val(slovenijaId);
                citizenShipCmp.val(slovenijaId);
            }
            else {
                if (cmr.model.values['_post_number']==-1) {
                    postCmp.$el().hide();
                    foreignPostCmp.$el().show();
                    foreignPostCmp.skipValidation = false;
                }
            }

            countryCmp.onSelectHook = () => {
                if (parseInt(countryCmp.val()) === parseInt(slovenijaId)) {
                    if (parseInt(postCmp.val()) === -1) {
                        postCmp.val(null);
                    }
                    postCmp.$el().show();
                    
                    foreignPostCmp.$el().hide();
                    foreignPostCmp.skipValidation = true;
                }
                else {
                    foreignPostCmp.$el().show();
                    foreignPostCmp.skipValidation = false;
                    postCmp.$el().hide();
                    postCmp.val(-1);
                }
            }

        },
        refValues: data.refValues,
        
        onSuccessfullySaved: (_cm, values) => {
            data.onSuccessfullySaved && data.onSuccessfullySaved(_cm, values, data.personVariables, data.refValues, update, data, cm);
            modal.hide();
        }
    });
    modal.show();

    return modal;
}