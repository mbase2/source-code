/**
 * @param {object} op options
 * @param {object} op.$parent
 * @param {string} op.tname
 */

//https://stackoverflow.com/questions/36517173/how-to-store-a-javascript-function-in-json

export default async op => {

    const {t, utils, exports, mutils} = op.imports;
    
    const $parent = op.$parent;

    const naciniVarovanjaByKey = utils.arrayToObject(op.naciniVarovanja,'key');

    const ComponentManager = await import ('../../libs/ComponentManager')    
    const cm = new ComponentManager.default();

    const dodatneLastnostiKeys = new Set();
    const varovanjaKeys = new Set();

    const {parentGroupKey, objectGroupKey} = op;

    const objectKey = op.add ? op.selectedNode.text : op.selectedNode.objectKey;

    const cenikKey = [parentGroupKey, objectGroupKey, objectKey].filter(key => key).join(' > ');

    op.enote = {
        [op.selectedNode.text]: utils.jsonParse(op.selectedNode.cenik).map(item => item.e)
    }

    let initialData = {};
    let editIndex = -1;

    if (!op.add) {
        editIndex = op.selectedNode.index;
        initialData = utils.jsonParse(op.selectedNode.data);
    }

    if (op.selectedNode.navedi || initialData.navedba) {
        const Inputs = await exports.Inputs();
        const navedba = await cm.add({
            key: 'navedba',
            component: Inputs.Input,
            options:{
                required: true,
                label: t`Navedba`
            },
            $parent: await mutils.addSingleAccordionPanel(cm, exports, t`Škodni objekt`, 'navedba', $parent)
        });

        initialData.navedba && navedba.val(initialData.navedba);
    }

    const Select2 = await exports.Select2();

    await cm.add({
        key: 'affectee',
        component: Select2.default,
        options:{
            required: true,
            label: t`Oškodovanec`,
            data: op.affectees || []
        },
        $parent: await mutils.addSingleAccordionPanel(cm, exports, t`Oškodovanec`, 'affectee', $parent)
    });

    initSingleOptionsSelect('affectee',op.affectees);
    function initSingleOptionsSelect(key, values) {
        
        if (initialData[key]) {
            cm.get(key).val(initialData[key]);
        }
        else if (values.length === 1) {
            const value = Array.isArray(values[0]) ? values[0][0] : values[0];
            cm.get(key).val(value);
        }
    }

    await dodatneLastnosti(parentGroupKey, $parent, op);

    await obsegSkode(parentGroupKey, $parent, op);

    await varovanje(parentGroupKey, $parent, op);

    const Button = await exports.Button();

    cm.add({
        key: 'dodaj',
        component: Button.default,
        options: {
            label: t`Potrdi`,
            classes:'btn-primary btn-lg',
            onClick: () => {

                if (!kontrole()) return;

                const nObject = {
                    text: cenikKey,
                    zgsKey: op.zgsKey,
                    dmg_object_id: op.dmg_object_id,
                    parentGroupKey: parentGroupKey,
                    objectGroupKey: objectGroupKey,
                    objectKey: objectKey,
                    cenik: op.selectedNode.cenik,
                    oid: Date.now(),
                    nodes: [
                        
                    ],
                    data: {}
                };

                const navedba = cm.get('navedba');

                if (navedba) {
                    addTreeNodes(nObject, [
                        {
                            text: `<b>${t`Navedba`}</b>&nbsp;${navedba.val()}`,
                            key: 'navedba'
                        }    
                    ]);
                }

                const oskodovanecCmp = cm.get('affectee');

                addTreeNodes(nObject, [
                    {
                        text: `<b>${t`Oškodovanec`}</b>&nbsp;${oskodovanecCmp.text()} (ID: ${oskodovanecCmp.val()})`,
                        key: 'affectee'
                    }    
                    ]);
                addTreeNodeGroup(nObject, t`Dodatne lastnosti`, dodatneLastnostiKeys);
                addTreeNodeGroup(nObject, t`Obseg škode`, new Set(['enota', 'kolicina']));

                const varovanjePrisotno = nObject.data.varovanjePrisotno = cm.get('varovanjePrisotno').val();
                if (varovanjePrisotno) {
                    
                    if (!validateVarovanja(varovanjaKeys)) {
                        return;
                    }

                    addTreeNodeGroup(nObject,t`Načini varovanja`, varovanjaKeys, '_nacini_varovanja');
                }

                //nObject.data = JSON.stringify(nObject.data);

                
                if (!op.add && editIndex !== -1) {
                    op.tree.data[editIndex] = nObject;
                }
                else {
                    op.tree.data.push(nObject);
                }
                
                op.refreshIzbraniObjekti(op.tree.data);

                op.onOk && op.onOk();

            }
        },
        $parent: op.$footer    
    });

    async function varovanje(parentGroupKey, $parent, op) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, t`Varovanje`, 'varovanje', $parent);
        const $varovanje = $('<div/>');

        const ButtonGroup = await exports.ButtonGroup();

        const btnGroup = new ButtonGroup.default({
            highlight: false,
            onClick: (data, key, self) => {
                if (key==='copy') {
                    op.naciniVarovanjaClipboard.length = 0;
                    for (let key of varovanjaKeys) {
                        if (cm.get(key)) {
                            let value = cm.get(key).val();
                            op.naciniVarovanjaClipboard.push({[key]: value});
                        }
                    }

                    $.toaster({ message : t`Načini varovanja so bili uspešno prenešeni na odložišče. Pri dodajanju novega objekta jih lahko prilepite.`, timeout:5000 });
                }
                else if (key === 'paste') {
                    op.naciniVarovanjaClipboard.map(nacinVarovanja => {
                        const key = Object.keys(nacinVarovanja)[0];
                        addNacinVarovanja(naciniVarovanjaByKey[key], $varovanje, nacinVarovanja);
                    });
                }
            },
            buttons:[
                {
                    key: 'copy',
                    label: t`Kopiraj`,
                    title: t`Kopiraj izbrane načine varovanja na odložišče`
                },
                {
                    key: 'paste',
                    label: t`Prilepi`,
                    title: t`Prilepi kopirane načine varovanja iz odložišča`
                }
            ]
        });

        $varovanje.append(btnGroup.$el());

        const comp = {};

        comp.varovanjePrisotno = {
            module: exports.Inputs,
            import: 'Input',
            options: {
                label: t`Varovanje škodnega objekta`,
                type: 'checkbox',
                onChange: () => {
                    if (cm.get('varovanjePrisotno').val()) {
                        $varovanje.show();
                    }
                    else {
                        $varovanje.hide();
                    }
                }
            },
            $parent: $container
        };

        comp.varovanje = {
            module: exports.Select2,
            options: {
                label: t`Način varovanja`,
                data: op.naciniVarovanja.filter(nv => 
                    op.naciniVarovanjaCulprit.filter(nvc=>nvc._nacin_varovanja === nv.key).findIndex(nvc => op.selectedCulprit==nvc._culprit_key_id) !== -1
                ).map(nv => nv.key),
                onSelect: async (e) => {
                    if (!e) return;
                    const value = cm.get('varovanje').val();
                    addNacinVarovanja(naciniVarovanjaByKey[value], $varovanje);
                }
            },
            $parent: $varovanje
        };

        await utils.loadComponents(comp);

        const varovanjePrisotno = await cm.add(comp.varovanjePrisotno);
        if (!op.selectedCulprit) {
            varovanjePrisotno.setEnabled(false);
            $container.append(mutils.alertCode(t`Načine varovanja lahko izberete šele potem, ko ste izbrali povzročitelja škode.`, 'Povzročitelj ni izbran.', 'warning'));
        }

        $container.append($varovanje);
        const selectVarovanje = await cm.add(comp.varovanje);

        selectVarovanje.$el().css('padding-bottom','10px');

        $varovanje.hide();

        if (!op.add) {
            if (initialData.varovanjePrisotno) {
                cm.get('varovanjePrisotno').val(true);
                cm.get('varovanjePrisotno').$el().find('input:first').trigger('change');

                initialData._nacini_varovanja && initialData._nacini_varovanja.map(nacinVarovanja => {
                    const key = Object.keys(nacinVarovanja)[0];
                    addNacinVarovanja(naciniVarovanjaByKey[key], $varovanje, nacinVarovanja);
                });
            }
        }
    }

    function getOptionsVisineOgraje(key, inputKey, naciniVarovanja) {
        
        const rval = {
            inputKey,
            options: [
                '< 115 cm', 
                '120 cm', 
                '140 - 145 cm',
                '160 - 170 cm'
            ]
        };

        if (naciniVarovanja && naciniVarovanja[key] && naciniVarovanja[key][inputKey]) {
           const visina = naciniVarovanja[key][inputKey];

           if (rval.options.indexOf(visina) === -1) {
                rval.oldOption = visina;
                rval.options.push(visina);
           }
        }

        return rval;
    }

    /**
     * 
     * @param {object} nv definicija načina varovanja
     * @param {string} nv.key
     * @param {string} nv._attributes
     * @param {*} $varovanje 
     * @param {*} naciniVarovanja 
     */

    async function addNacinVarovanja(nv, $varovanje, naciniVarovanja = null) {

        const value = nv.key;

        varovanjaKeys.add(value);
        
        const $panel = await mutils.addSingleAccordionPanel(cm, exports, value, value, $varovanje, 'acc_', false, {
            onPanelRemove: ($pdiv, $el) => {
                $el.empty();
                $el.remove();
                cm.remove('acc_'+value);
                varovanjaKeys.delete(value);
                cm.get('varovanje').val(null);
            }
        });

        const varovanjeInputs = [];
        
        let optionsVisineOgraje = null;

        if (value === 'elektromreža') {
            
            optionsVisineOgraje = getOptionsVisineOgraje(value, 'Najnižja višina ograje', naciniVarovanja);
            
            varovanjeInputs.push({
                key: 'Najnižja višina ograje',
                label: t`Najnižja višina ograje`,
                type: 'select',
                options: optionsVisineOgraje.options
            });

            varovanjeInputs.push({
                    key: 'Povprečna napetost toka [kV] (zg. meja 20 kV)',
                    label: t`Povprečna napetost toka [kV] (zg. meja 20 kV)`,
                    type: 'number',
                    min: 0,
                    step: 0.1,
                    max: 20
            });

            varovanjeInputs.push({
                key: 'Ustrezna vzdrževanost',
                label: t`Ustrezna vzdrževanost`,
                type: 'select',
                options: [
                    'da','ne'
                ]
            });

            varovanjeInputs.push({
                key: 'Obseg (obodna dolžina) elektromreže [m]',
                label: t`Obseg (obodna dolžina) elektromreže [m]`,
                type: 'number',
                min: 0
            });
        }
        else if (value === 'žična elektroograja') {
            varovanjeInputs.push({
                key: 'Število žic',
                label: t`Število žic`,
                type: 'select',
                options: [
                    1,2,3,4,5,6,7,8,9
                ]
            });

            optionsVisineOgraje = getOptionsVisineOgraje(value, 'Najnižja višina zgornje žice', naciniVarovanja)

            varovanjeInputs.push({
                key: 'Najnižja višina zgornje žice',
                label: t`Najnižja višina zgornje žice`,
                type: 'select',
                options: optionsVisineOgraje.options
            });


            varovanjeInputs.push({
                    key: 'Povprečna napetost toka [kV] (zg. meja 20 kV)',
                    label: t`Povprečna napetost toka [kV] (zg. meja 20 kV)`,
                    type: 'number',
                    min: 0,
                    step: 0.1,
                    max: 20
            });

            varovanjeInputs.push({
                key: 'Ustrezna vzdrževanost',
                label: t`Ustrezna vzdrževanost`,
                type: 'select',
                options: [
                    'da','ne'
                ]
            });

            /*varovanjeInputs.push({
                key: 'Obseg (obodna dolžina) ograje [m]',
                label: t`Obseg (obodna dolžina) ograje [m]`,
                type: 'number',
                min: 0
            });*/
        }
        else if (value === 'farmer mreža nadgrajena z električnimi žicami') {
            varovanjeInputs.push({
                key: 'Število žic',
                label: t`Število žic`,
                type: 'select',
                options: [
                    1,2,3,4,5,6,7,8,9
                ]
            });

            varovanjeInputs.push({
                    key: 'Povprečna napetost toka [kV] (zg. meja 20 kV)',
                    label: t`Povprečna napetost toka [kV] (zg. meja 20 kV)`,
                    type: 'number',
                    min: 0,
                    step: 0.1,
                    max: 20
            });
        }
        else if (value === 'nivo dvignjen od tal (čebelnjak, krmilnik ipd.)') {
            varovanjeInputs.push({
                key: 'Višina dvignjenega nivoja',
                label: t`Višina dvignjenega nivoja`,
                type: 'select',
                options: [
                    'manj kot 250 cm',
                    '250 cm ali več'
                ]
            });
        }
        else if (value === 'pastirski pes ob čredi') {
            varovanjeInputs.push({
                key: 'Število psov',
                label: t`Število psov`,
                type: 'select',
                options: [
                    1,2,3,4,5,6,7,8,9
                ]
            });
        }

        const attributes = [...varovanjeInputs, ...(nv._attributes || [
            {
                key: 'navedba',
                label: t`Navedi`, 
                type: 'text'
            }
        ])];

        const Inputs = await exports.Inputs();
        const inputs = await cm.add({
            key: value,
            component: Inputs.default,
            options: {
                inputs:attributes,
            },
            $parent: $panel
        });

        if (naciniVarovanja && naciniVarovanja[value]) {
            inputs.val(naciniVarovanja[value]);

            if (optionsVisineOgraje.oldOption) {
                const $select = inputs.inputs[optionsVisineOgraje.inputKey].$el().find('select');
                const $oldOption =  $select.find(`option[value="${optionsVisineOgraje.oldOption}"]`);
                $select.data('forced-value', optionsVisineOgraje.oldOption);
                $oldOption.prop('disabled', true);
            }
        }
    }

    async function obsegSkode(parentGroupKey, $parent, op) {
        const enote = op.enote[op.selectedNode.text];
        const $container = await mutils.addSingleAccordionPanel(cm, exports, t`Obseg škode`, 'obseg_skode', $parent);
        const comp = {};
        comp.enota = {    
            module: exports.Select2,
            options: {
                required: true,
                label: t`Enota`,
                data: enote
            },
            $parent: $container
        };
        comp.kolicina = {
            module: exports.Inputs,
            import: 'Input',
            options: {
                label: t`Količina`,
                type: 'number',
                step: '0.000001',
                required: true
            },
            $parent: $container
        }

        await utils.loadComponents(comp);

        await cm.add(comp.enota);
        const kolicina = await cm.add(comp.kolicina);

        initSingleOptionsSelect('enota',op.enote[op.selectedNode.text]);
        if (!op.add) kolicina.val(initialData.kolicina);

    }

    async function dodatneLastnosti(parentGroupKey, $parent, op) {
        const comp = {};
        if (parentGroupKey === "Rastlina na kmetijski površini / Rastlinski pridelek") {
            const $container = await mutils.addSingleAccordionPanel(cm, exports, t`Dodatne lastnosti izbranega šk. objekta`, parentGroupKey, $parent);
            comp.tip = {    
                module: exports.Select2,
                options: {
                    required: true,
                    label: t`Rastlina na kmetijski površini / Rastlinski pridelek (izberi)`,
                    data: [
                        ['Rastlina na kmetijski površini', 'Rastlina na kmetijski površini'],
                        ['Rastlinski pridelek', 'Rastlinski pridelek']
                    ],
                    onSelect: async (e) => {
                        if (!e) return;
                        const izbira = cm.get('tip').val();
                        if (izbira === 'Rastlinski pridelek') {
                            cm.get('kmetijska_povrsina') && cm.remove('kmetijska_povrsina');
                            const pridelek = await cm.add(comp.pridelek);
                            if (!op.add) pridelek.val(initialData.pridelek);
                            dodatneLastnostiKeys.delete('kmetijska_povrsina');
                            dodatneLastnostiKeys.add('pridelek');
                        }
                        else {
                            cm.get('pridelek') && cm.remove('pridelek');
                            const kmetijska_povrsina = await cm.add(comp.kmetijska_povrsina);
                            if (!op.add) kmetijska_povrsina.val(initialData.kmetijska_povrsina);
                            dodatneLastnostiKeys.delete('pridelek');
                            dodatneLastnostiKeys.add('kmetijska_povrsina');
                        }
                    }
                },
                $parent: $container
            };

            comp.kmetijska_povrsina = {
                    module: exports.Inputs,
                    options: {
                            inputs: [{
                                label: t`Vrsta dejanske rabe kmetijskih površin`,
                                key: "dejanska raba",
                                help: 'npr. njiva/vrt, travnik, vinograd, hmeljišče, intenzivni sadovnjak, ekstenzivni sadovnjak, oljčnik, plantaža gozdnega drvja, drugo'
                            },
                            {
                                label: `poškodovana kultura je bila nadomeščena z nadomestno`,
                                type: 'checkbox',
                                key: "nadomestna kultura"
                            }
                        ]
                    },
                    $parent: $container
            };
            
            comp.pridelek = {
                    module: exports.Inputs,
                    import: 'Input',
                    options: {
                        label: "Lokacija skladiščenja kmetijskega pridelka",
                    },
                    $parent: $container
            };
        
            await utils.loadComponents(comp);

            const tip = await cm.add(comp.tip);
            if (!op.add) tip.val(initialData.tip);
            dodatneLastnostiKeys.add('tip');
        }
    }

    function addTreeNodes(nObject, nodes) {
        const data = nObject.data;

        for (let node of nodes) {
                data[node.key] = cm.get(node.key).val();
                nObject.nodes.push({
                    text: node.text,
                    icon: 'fa fa-file-o',
                    selectable: false
                });
        }
    }

    /**
     * 
     * @param {*} nObject 
     * @param {*} title 
     * @param {*} keys 
     * @param {string} dataKey key to array in nObject.data structure to write the data to
     */

    function addTreeNodeGroup(nObject, title, keys, dataKey = null) {
        if (keys.size === 0) return;

        const data = nObject.data;

        if (dataKey) {
            if (!data[dataKey]) data[dataKey] = [];
        }
        
        const node = {
            text: title,
            nodes: [
                
            ],
            selectable: false
        };

        nObject.nodes.push(node);

        for (let key of keys) {
            if (cm.get(key)) {
                let value = cm.get(key).val();
                
                node.nodes.push({
                    text: `<b>${key}</b>&nbsp;${typeof value === 'object' ? JSON.stringify(value) : value}`,
                    icon: 'fa fa-file-o',
                    selectable: false
                });

                if (dataKey) {
                    data[dataKey].push({[key]:value});
                }
                else {
                    data[key] = value;
                }
            }
        }
    }

    function validateVarovanja(varovanjaKeys) {
        for (const key of varovanjaKeys) {
            const cmp = cm.get(key);

            if (cmp) {
                const inputs = cmp.inputs;
                
                if (!inputs) {
                    return true;
                }

                for (const key of Object.keys(inputs)) {
                    if (!inputs[key].validate(false)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    function kontrole() {
        try {

            const navedba = cm.get('navedba');
            if (navedba && !navedba.validate()) {
                throw ({
                    msg: t`Potrebno je navesti opis škode`
                });
            }

            let c = cm.get('affectee');
            if (!c.validate()) {
                throw ({
                    msg: t`Potrebno je izbrati oškodovanca za škodni objekt.`
                });
            }

            if (parentGroupKey === "Rastlina na kmetijski površini / Rastlinski pridelek") {
                const c = cm.get('tip');

                const err = {
                    msg: t`Treba je izbrati med 'Rastlina na kmetijski površini' in 'Rastlinski pridelek'`,
                }

                if (!c.validate()) {
                    throw(err);
                }
            }

            let msg = [];
            ['enota', 'kolicina'].map(key=> {
                if (!cm.get(key).validate()) {
                    msg.push(t`Manjkajoči podatki za obseg škode` + ` (${key}).`);
                }
            });
            
            if (msg.length>0) {
                throw({
                    msg: msg.join("\n")
                })
            }

        }//try
        catch(err) {
            alert(err.msg);
            
            console.log(err)
          return false;
        }
        return true;
    }

}