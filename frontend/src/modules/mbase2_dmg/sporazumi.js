import * as exports from '../../libs/exports';
import globals from '../../app-globals';
import * as DmgTable from './DmgTable';

/**
 * @param {object} op options
 * @param {object} op.$parent
 * @param {string} op.tname
 */
export default async op => {
    const {$parent} = op;

    const [
        ComponentManager,
        utils,
        mutils,
        Select2,
        ModalDialog,
        Button,
        ButtonGroup,
        DropzoneLoader,
        dmgu

    ] = await Promise.all([
        exports.ComponentManager(),
        exports.utils(),
        exports.mutils(),
        exports.Select2(),
        exports.ModalDialog(),
        exports.Button(),
        exports.ButtonGroup(),
        exports.DropzoneLoader(),
        import('./dmg_utils')
    ]);

    dmgu.default(exports);

    $('body').css('overflow','hidden');

    const t = utils.t;

    const bookmarks = {};
    const $znesek = $('<h5/>');

    const isAdmin = globals.user.roles.find(role => role === 'administrator' || role === 'mbase2-dmg-admins') ? true : false;

    let $saveFormButton = null;
    let $completeAgreementButton = null;
    let $reopenAgreementButton = null;
    let initializedComponents = {};

    let initialClaimData = {};
    const affecteesFormData = {};

    let $affecteeStatus = null;

    const fid = utils.getQueryParameter('fid');

    let znesek = {_odskodnina:0.0, _odskodnina_additional:0.0};

    if (!fid) return;

    const cm = new ComponentManager.default({
        onComponentChange: (key, value, cm) => {
            //console.log('onComponentChange', key, value, cm)
            const bm = bookmarks[key];

            if (!bm) return;

            if (key === '_odskodnina' || key === '_odskodnina_additional') {
                znesek[key] = cm.get(key).znesek();
                console.log('znesek', znesek);
                $znesek.text(((znesek._odskodnina || 0.0) + (znesek._odskodnina_additional || 0.0)).toFixed(2) + ' EUR');
            }

            //console.log('onComponentChange', key, value, cm)

            if (bm.parent) {
                bm.$bm = bookmarks[bm.parent].$bm;
            }

            initializedComponents[key] = true;

            const validated = bm.validated = validateComponentValue(key, value, bm.required, bm.parent);

            $saveFormButton && isDataChanged('onComponentChange_'+key+'_'+value+'_'+cm.get('_affectee').val()) && $saveFormButton.prop('disabled', false);
            
            if (bm.$bm) {
                if (validated) {
                    bm.$bm.addClass('bookmark-completed');
                }
                else {
                    bm.$bm.removeClass('bookmark-completed');
                }
            }

            mutils.validateConditionsForCompleteButton($completeAgreementButton, bookmarks, cm);
        },
        onComponentAppended: (key, component, cm) => {
            
            const value = initialClaimData[key];

            //console.log('onConponetn appended', key, value, component.val && component.val())

            if (value && component.val && !_.isEqual(value, component.val())) {
                component.val(value);
                cm.op.onComponentChange(key, value, cm);
            }
        }
    });

    const result = await mutils.batchRequestHelper(
        [
            `mb2data/dmg?:id=${fid}`,
            'mb2data/dmg_objects_price_lists',
            'mb2data/dmg_urne_postavke',
            `mb2data/dmg_agreements?:_claim_id=${fid}`,
            'module_variables_vw/language/${globals.language}?:module_name=dmg'
        ]);

    mutils.convertJsonToObjects(result[0], result[4]);

    initialClaimData = result[0][0];

    if (!initialClaimData._dmg_objects) return;

    if (initialClaimData._attachments) delete initialClaimData._attachments;    //claim attachments should not be visible in agreement - onComponentAppended adds initialClaimData to controls

    const examinationDate = initialClaimData._dmg_examination_date;
    const priceLists = result[1];
    const priceListInx = mutils.dmgGetPriceListIndex(examinationDate, priceLists);

    if (priceListInx === -1) {
        alert(globals.t`Napaka pri nalaganju cenika`);
        return;
    }
    
    const cenikObjektov = utils.jsonParse(priceLists[priceListInx].data);

    const urnePostavke = mutils.dmgGetWorkHourPrices(initialClaimData._dmg_examination_date, result[2]); //_dmg_examination_date

    const skodniObjektiZahtevka = {};
    initialClaimData._dmg_objects.map(skObjekt => skodniObjektiZahtevka[skObjekt.oid] = skObjekt);

    const skodniObjektiZahtevkaPoOskodovancih = await extractInitialDamageObjects(initialClaimData);

    let spremembaSkodnihObjektov = false;   //zgodila se je sprememba skodnih objektov v zaključenem zahtevku
    
    result[3].map(a => {
        const affecteeId = a._affectee;
        const data = affecteesFormData[affecteeId] = JSON.parse(a._data);
        affecteesFormData[a._affectee].id = a.id;
        affecteesFormData[a._affectee]._completed = a._completed;
        
        const skodniObjektiSporazumaOskodovanca = {};
        //delete nonexisting objects
        data._odskodnina.map((skObjekt, inx) => {
            const oid = skObjekt[0].oid;

            if (!skodniObjektiZahtevka[oid]) {
                delete data._odskodnina[inx];
                spremembaSkodnihObjektov = true;
            }
            else {
                skodniObjektiSporazumaOskodovanca[oid] = skObjekt;
            }
        });

        //add new objects
        skodniObjektiZahtevkaPoOskodovancih[affecteeId] && skodniObjektiZahtevkaPoOskodovancih[affecteeId].map(skObjekt => {
            if (!skodniObjektiSporazumaOskodovanca[skObjekt[0].oid]) {
                spremembaSkodnihObjektov = true;
                affecteesFormData[affecteeId]._odskodnina.push(skObjekt);
            }
        });
    });

    //initialize if agreements are not already saved in the database
    initialClaimData._affectees.map(aid => {
        if (!affecteesFormData[aid]) {
            affecteesFormData[aid] = {
                _odskodnina: skodniObjektiZahtevkaPoOskodovancih[aid],
                _additional_damage_claims: null,
                _odskodnina_additional: [],
                _status: null,
                _affectee: aid
            };
        }
    });
    
    const naslov = t`Škode`;
    const podnaslov = fid ? t`urejanje sporazumov` : t`nov sporazum`;

    let buttons = [];
    
    buttons = [
        {
            key: 'complete',
            label: t`Zaključi sporazum`,
            onAppend: $btn => $btn.hide()

        },
        {
            key: 'save',
            label: t`Shrani`,
            onAppend: $btn => $btn.hide()
        },
        {
            key: 'reopen',
            label: t`Ponovno odpri`,
            onAppend: $btn => $btn.hide()
        }
    ];
    
    const group = new ButtonGroup.default({
        highlight: false,
        classes: 'btn-primary btn-lg',
        onClick: async (data, key, self) => {
            if (key === 'complete') {
                saveAgreement(null, key);
            }
            else if (key === 'save') {
                saveAgreement(null, key);
            }
            else if (isAdmin && key === 'reopen') {
                saveAgreement(null, key);
            }
        },
        buttons: buttons
    });

    const $info = $('<div/>');
    const $status = $('<div/>');
    $info.append($status);
    const $completed = $(`<span class="label label-info">${t`Sporazum je ZAKLJUČEN.`}</span>`);
    $completed.hide();
    $info.append($completed);

    const breadcrumbs = [
        {
            text: t`Pregled zahtevkov`,
            href: window.location.origin + '/mbase2/modules/dmg'
        },
        {
            text: t`zahtevek`,
            href: window.location.origin + '/mbase2/modules/dmg/zahtevek?fid=' + fid
        },
        {
            text: podnaslov,
            class: 'active'
        }
    ];

    const $header = mutils.moduleHeader(naslov, podnaslov, group.$el(), $info, t`Številka šk. zahtevka` + ': '+ initialClaimData._claim_id, utils.createBreadcrumbs(breadcrumbs));
    $parent.append($header);

    const {$left, $right} = dmgu.$containers($parent);

    const floats = utils.createFloatContainersSideBySide($right);

    const sections = [
        {
            key: '_affectee',
            title: t`Oškodovanec`,
            required: true,
            fun: oskodovanci
        },
        {
            key: '_deputy',
            title: t`Pooblaščenec`,
            required: true,
            fun: pooblascenec
        },
        {
            key: '_odskodnina',
            title: t`Odškodnina`,
            required: true
            //fun: odskodnina
        },
        {
            key: '_odskodnina_additional',
            parent: '_odskodnina',
            required: false
        },
        {
            key: '_status',
            title: t`Status sporazuma`,
            required: true,
            fun: statusSporazuma
        },
        {
            key: '_attachments',
            title: t`Priloge`,
            required: false,
            fun: attachments
        }
    ];

    dmgu.initSections(sections, bookmarks, cm, $left);
    
    for (let section of sections) {
        if (!section.fun) continue;
        await section.fun(section);
    }

    $completeAgreementButton = group.buttons.find(b => b.key === 'complete').$btn;
    $reopenAgreementButton = group.buttons.find(b => b.key === 'reopen').$btn;
    $saveFormButton = group.buttons.find(b => b.key === 'save').$btn;
    
    group.buttons.map(btn => btn.$btn.show());

    $saveFormButton && $saveFormButton.prop('disabled', !spremembaSkodnihObjektov);

    mutils.validateConditionsForCompleteButton($completeAgreementButton, bookmarks, cm);

    setCompletedState();

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getCurrentlySelectedAffectee() {
        const cmp = cm.get('_affectee');
        return cmp ? cmp.val() : null;
    }
    function setCompletedState() {
        const currentlySelectedAffectee = getCurrentlySelectedAffectee();

        if (!currentlySelectedAffectee) return;
        const affecteeFormData = affecteesFormData[currentlySelectedAffectee];

        $reopenAgreementButton && $reopenAgreementButton.hide();
                
        if (affecteeFormData._completed === true) {
            $right.find('.panel-group .panel').addClass('disable-mouse-events');
            const affecteeCmp = cm.get('_affectee');
            affecteeCmp && affecteeCmp.$el().css('pointer-events', 'auto');
            $completeAgreementButton && $completeAgreementButton.hide();
            $saveFormButton && $saveFormButton.hide();
            $completed.show();
            isAdmin && $reopenAgreementButton && $reopenAgreementButton.show();
        }
        else {
            $right.find('.panel-group .panel').removeClass('disable-mouse-events');
            $completeAgreementButton && $completeAgreementButton.show();
            $saveFormButton && $saveFormButton.show();
            $completed.hide();
        }
    }

    function validateComponentValue(key, value, required, parentKey) {

        if (key==='_affectee') {
            const data = cm.getData('_affectee');
            $affecteeStatus && $affecteeStatus.hide() && $affecteeStatus.html('<span class="pficon pficon-error-circle-o"></span>');
            if (data && data.length > 0) {
                const aid = getCurrentlySelectedAffectee();
                const affecteeIndex = data.findIndex(d => d.id == aid);
                if (affecteeIndex > -1) {
                    let rval = true;

                    rval && $affecteeStatus.hide();
                    
                    return rval;
                }
            }
            else {
                return false;
            }
        }
        
        if (required === true) {
            if (utils.empty(value)) return false;
        }

        return true;
    }

    async function odskodnina(titleKey) {
        
        const panel = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right, 'acc_', true);
        const $container = panel.$body;
        panel.$panel.find('.panel-title:first').append($znesek);
        const affectee = cm.get('_affectee').val();
        
        let items = affecteesFormData[affectee]._odskodnina;

        $container.empty();

        if (!items || items.length === 0) {
            const $msgbox = $(`<div class="alert alert-danger">
                <span class="pficon pficon-error-circle-o"></span>
            </div>`);

            $msgbox.append(t`Za izbranega oškodovanca ni vpisanih škodnih objektov.`);
            $container.html($msgbox);
            items = [];
        }

        let cols = [
            {key: t`Šk. objekt`}, {key: t`Količina`}, {key: t`Enota`},{key:  t`Cena/enoto`}, {key: t`Cena`}, 
            {
                key: t`Priznana vrednost`,
                read_only: false
            }
        ];

        const table1 = await cm.add({
            key: '_odskodnina',
            component: DmgTable.default,
            options: {
                cols: cols, 
                initialData: $.extend(true, [], items), //[["Pšenica","4","t","122.35",489.4,["489.4","razlog 123"]]],//values,
                imports: {
                    utils: utils,
                    ComponentManager: ComponentManager,
                    ModalDialog: ModalDialog,
                    record: exports.record
                }
            },
            $parent: $container
        }, true);

        table1.$el().trigger('change');

        let table2 = null;

        $container.append(`<h4>${t`Dodatno`}</h4>`)

        const cdef = exports.select({
            data: {
                values: urnePostavke
            },
            $parent: $container,
            process: d => d.label,
            onSelect: e => {
                if (!e) return;
                if (!table2) return;

                const cmp = cm.get('_additional_damage_claims');            
                const value = cmp.val();

                let row = [{key: value},''];

                let rowAddon = [];

                if (value !== 'Drugo') {
                    const up = urnePostavke.find(up => up.label === value);
                    rowAddon = [up.enota, up.cena];
                }
                else {
                    rowAddon = ['', ''];
                }

                row = [...row, ...rowAddon, '', ['','']];

                const tableValue = table2.val() || [];
                tableValue.push(row);

                table2.val(tableValue);

                cmp.val(null);

            }
        }, false);

        cdef.key = '_additional_damage_claims';
        cdef.component = Select2.default;
        await cm.add(cdef, true);

        cols = [
            {key: t`Opis`, read_only: true}, {key: t`Količina`, read_only: false}, {key: t`Enota`},{key:  t`Cena/enoto`}, {key: t`Cena`},
            {
                key: t`Priznana vrednost`,
                read_only: false
            }
        ];

        /**
         * For compatibility reasons, because before this component was fixedPriceValues
         */
        let odskodninaAdditionalInitialValues = [];

        if (affecteesFormData[affectee]._odskodnina_additional && 
            affecteesFormData[affectee]._odskodnina_additional.length>0 &&
            affecteesFormData[affectee]._odskodnina_additional[0].row){
                affecteesFormData[affectee]._odskodnina_additional.map(row => {
                    const dataRow = row.row;
                    dataRow[0] = {key:dataRow[0]};
                    dataRow.push([dataRow[dataRow.length-1],'']);
                    odskodninaAdditionalInitialValues.push(dataRow);
                })
        }
        else {
            odskodninaAdditionalInitialValues = $.extend(true, [], affecteesFormData[affectee]._odskodnina_additional);
        }

        console.log(odskodninaAdditionalInitialValues)

        table2 = await cm.add({
            key: '_odskodnina_additional',
            component: DmgTable.default,
            options: {
                //fixedPriceValues: true,
                itemNameKey: 'Opis',  //for compatibility
                iconRemove: true,
                cols: cols, 
                initialData: odskodninaAdditionalInitialValues, //$.extend(true, [], [["Delovna ura - ročna - enostavna dela","2","h","5",10],["Strošek traktorja 60 KW – štirikolesni pogon","1","h","18.90",18.90]]), //[["Pšenica","4","t","122.35",489.4,["489.4","razlog 123"]]],//values,
                imports: {
                    utils: utils,
                    ComponentManager: ComponentManager,
                    ModalDialog: ModalDialog,
                    record: exports.record
                },
                onComponentChange: (key, value, cm) => {
                    if (key==='Količina') {
                        const _ce = cm.get('Cena/enoto');
                        const _c = cm.get('Cena');
                        if (_ce && _c) {
                            const cena = parseFloat(value) * parseFloat(_ce.val());
                            _c.val(cena && cena.toFixed(2));
                            const _pv = cm.get('Priznana vrednost');
                            if (_pv && !_pv.val()) {
                                _pv.val(cena && cena.toFixed(2));
                            }
                        }
                    }
                }
            },
            $parent: $container
        }, true);

        table2.$el().trigger('change');
    }

    async function statusSporazuma(titleKey) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);

        const cdef = exports.select({
            data: {
                values: [[1, t`Sporazum JE dosežen.`], [2, t`Sporazum NI dosežen.`], [3, t`Obrazec 2.3 - vloga stranke`]],
            },
            $parent: $container,
            process: d => ({value: d[0], text: d[1]}),
            onSelect: e => {
                $status.empty();
                if (!e) return;
                const cmp = cm.get(titleKey.key);
                const status = cmp.val();
                const text = cmp.text();

                $status.html(`<span class="label label-${status == 1 ? 'success' : 'danger'}">${text}</span>`);

            }
        }, false);

        cdef.key = titleKey.key;
        cdef.component = Select2.default;

        const cmp = await cm.add(cdef, true);

        const selectedAffectee = cm.get('_affectee').val();
        let status = affecteesFormData[selectedAffectee][titleKey.key];

        if (status === undefined) status = null;
        
        cmp.val(status);
        
    }

    async function attachments(titleKey) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, $right);
        
        const cmp = await cm.add({
            key: titleKey.key,
            $parent: $container,
            component: DropzoneLoader.default,
            options: {
                url: globals.apiRoot + '/file-upload/private/dmg',
                acceptedFiles: '.*',
                onComplete: (data, $el) => {
                    $el.trigger('change');
                },
                labels: {
                    add_files: t`Dodaj datoteke`
                },
                dictDefaultMessage: t`Sem lahko povlečeš in spustiš datoteke.`,
                acceptAllFiles: true
            }
        });

        const currentlySelectedAffectee = getCurrentlySelectedAffectee();
        if (!currentlySelectedAffectee) return;
        const affecteeFormData = affecteesFormData[currentlySelectedAffectee];
        affecteeFormData && affecteeFormData._attachments && cmp.val(affecteeFormData._attachments);

    }

    async function pooblascenec(titleKey) {
        const $container = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, floats.$right);
        const data = await mutils.requestHelper(globals.apiRoot + '/mb2data/dmg_deputies_vw?:_uid='+initialClaimData._deputy);
        const attributes = ['_ime', '_priimek', '_oe_ime ', '_phone',  '_email'];
        const codeList = await mutils.getCodeList('variables', globals.apiRoot, attributes);
        const {translated, keyMap} = mutils.translateVariableKeys(data[0], utils.convertToAssocArray(codeList, 'key'), globals.language);

        utils.keyValueOutput(translated, $container, {keys: attributes, keyMap: keyMap});

        bookmarks._deputy.$bm.addClass('bookmark-completed');
        bookmarks._deputy.validated = true;
    }
    
    async function oskodovanci(titleKey) {
            
        const {personVariables, refValues} = await dmgu.personVariablesDefinition('dmg_affectees');

        const oskodovanciVariables = personVariables;

        oskodovanciVariables.map(v => {
            if (v.key_name_id === '_citizenship') {
                v.required = false;
            }
        });

        const $oskodovanciContainer = await mutils.addSingleAccordionPanel(cm, exports, titleKey.title, titleKey.key, floats.$left);
        
        const $div = $('<div/>');
        const $affectee = $('<div/>', {style:"padding-top:1em;padding-bottom:0.5em"});
        $div.append($affectee);

        $affecteeStatus = $(`<div class="alert alert-danger">
            </div>`);
        $affecteeStatus.hide();
        $div.append($affecteeStatus);

        const posteRefId = oskodovanciVariables.find(a => a.reference === 'poste').ref;
        
        const btn = new Button.default({
            label: t`Uredi podatke oškodovanca`,
            onClick: () => {
                dmgu.personUpsert('dmg_affectees', true, cm.get(titleKey.key).val(), {

                    onShown: (cmr) => {
                        cmr.get('_full_name').$el().find('input:first').focus().select();
                    },
    
                    onClose: (update, data, cm) => {
                        if (!update && data.newOption) {
                            const cmp = cm.get(titleKey.key);
                            const values = cmp.val();
                            cmp.val(values.filter(v => v != data.text));
                        }
                    },
    
                    refValues: refValues,
                    cm: cm,
                    titleKey: titleKey,
                    personVariables: oskodovanciVariables,

                    onSuccessfullySaved: (_cm, values, oskodovanciVariables, refValues, update, data, cm) => {
                        values._post = mutils.getReferenceTableKeyLabel(values._post_number, posteRefId, refValues);
                        
                        const oskodovanciComponent = cm.get(titleKey.key);
                        
                        const _data = cm.getData(titleKey.key);

                        const inx = _data.findIndex(d => d.id == values.id);
                        if (inx === -1) return;
                        _data[inx] = values;
                        
                        cm.refresh(titleKey.key);
                        
                        oskodovanciComponent.val(values.id);
                    }
                });
            }
        });

        $div.append(btn.$el());

        const storedAffecteesData = (initialClaimData.affectees_data && JSON.parse(initialClaimData.affectees_data)) || [];

        const oskodovanciVariablesAssoc = utils.convertToAssocArray(oskodovanciVariables, 'key_name_id');

        const cdef = exports.select({
            url: globals.apiRoot + `/mb2data/dmg_affectees_vw?:id=(${initialClaimData._affectees.join(',')})&json_flags=0`,
            $parent: $oskodovanciContainer,
            process: d => {
                const storedAffecteeData = storedAffecteesData.find(row => row.id == d.id);
                const equal = mutils.compareStoredAndCurrentAffecteeData(
                    storedAffecteeData, 
                    cdef.data.model.originalData.find(row =>row.id == d.id));

                if (!equal && !storedAffecteeData._post) {
                    const ref_list_id = oskodovanciVariablesAssoc._post_number.ref;
                    storedAffecteeData._post = refValues.tableReferences.find(tr => tr.list_id == ref_list_id && tr.id == storedAffecteeData._post_number).key;
                }

                return [d.id, equal ? dmgu.personText(d) : dmgu.personText(storedAffecteeData)]
            },
            onSelecting: e => {
                if (e && e.params && e.params.args && e.params.args.data) {
                    const id = e.params.args.data.id;
                        if (id && isDataChanged('selecting')) {
                            if (confirm(t`Podatki za oškodovanca so bili spremenjeni. Ali jih želite shraniti?`)) {
                                saveAgreement(cm.val(), 'save', [()=>cm.get('_affectee').val(id)]);
                            }
                        }
                }

                initializedComponents = {};
                
                return true;
            },
            onSelect: e => {
                if (!e) return;
                console.log('cdef', cdef)
                //affectee data output:
                const data = cm.getData(titleKey.key);
                const selectedAffectee = getCurrentlySelectedAffectee();
                znesek = {_odskodnina:0.0, _odskodnina_additional:0.0};
                
                let selectedData = data.find(d => d.id == selectedAffectee);

                delete selectedData._post;

                const storedAffecteeData = storedAffecteesData.find(row => row.id == selectedAffectee);
                const equal = mutils.compareStoredAndCurrentAffecteeData(
                    storedAffecteeData, 
                    cdef.data.model.originalData.find(row =>row.id == selectedAffectee));

                if (!equal) {
                    selectedData = storedAffecteeData;
                }

                const ref = oskodovanciVariablesAssoc._country.ref;

                const {translated, keyMap} = mutils.translateVariableKeys(selectedData, oskodovanciVariablesAssoc);

                translated[keyMap._country] = refValues.tableReferences.find(tr => tr.list_id == ref && tr.id == selectedData._country).key;

                try {
                    translated[keyMap._citizenship] = refValues.tableReferences.find(tr => tr.list_id == ref && tr.id == selectedData._citizenship).key;
                }
                catch (err) {
                    ;
                }
                
                if (selectedData._post_number == -1) {
                    delete translated[keyMap._post_number];
                }
                else {
                    delete translated[keyMap._foreign_post];
                }

                if (selectedData._tax_id !== undefined) {
                    delete translated[keyMap._tax_id];
                }

                if (selectedData._iban !== undefined) {
                    delete translated[keyMap._iban];
                }

                utils.keyValueOutput(translated, $affectee);
                
                odskodnina(sections.find(s => s.key === '_odskodnina'));

                statusSporazuma(sections.find(s => s.key === '_status'));

                setCompletedState();
            }
        }, false);

        cdef.key = titleKey.key;
        cdef.component = Select2.default;

        const cmp = await cm.add(cdef);
        $oskodovanciContainer.append($div);
        cmp.val(cm.getData(titleKey.key)[0].id);
    }

    async function extractInitialDamageObjects(_data) {
        const skodniObjekti = {};
        console.log('_data', _data)
        for (let obj of _data._dmg_objects) {
            const data = utils.jsonParse(obj.data);

            const cenikKeys = [obj.parentGroupKey, obj.objectGroupKey, obj.objectKey].filter(key => key);

            const priceListForObject = mutils.findObjectPriceList(cenikKeys, cenikObjektov);

            const cenik = (priceListForObject && utils.jsonParse(priceListForObject));

            const affecteeObjects = skodniObjekti[data.affectee] || [];

            const enota = data.enota;
            const cena = (cenik && parseFloat(cenik.find(cn => cn.e === enota).c)) || '';
            const kolicina = parseFloat(data.kolicina);
            const vrednost = cena * kolicina;

            affecteeObjects.push([
                {key: obj.text, oid: obj.oid},
                kolicina.toFixed(2),
                enota,
                cena && cena.toFixed(2),
                vrednost.toFixed(2),
                [vrednost.toFixed(2),'']
            ]);

            skodniObjekti[data.affectee] = affecteeObjects;
        }

        return skodniObjekti;
    }

    async function saveAgreement(values, key = 'save', onSuccessCallbacks = []) {
        const saveOptions = await mutils.saveButtonHelper();
        saveOptions.rootUrl = globals.apiRoot + '/mb2data';

        const selectedAffectee = (values && values._affectee) || getCurrentlySelectedAffectee();
        cm.model.values = {
            id: affecteesFormData[selectedAffectee].id
        };
        
        cm.saveOptions.onSuccessCallbacks = [...[(cm, res) => {
            const data = JSON.parse(res._data);
            data.id = res.id;
            affecteesFormData[data._affectee] = data;
            $saveFormButton && $saveFormButton.prop('disabled', true);

            if (key === 'complete' || key === 'reopen') {
                window.location.reload();
            }
        }], ...onSuccessCallbacks];

        saveOptions.beforeRequest = request => {
               
            if (key === 'save' || key === 'complete') {
                const data = values || cm.val();

                request.attributes = {
                    ':_data': data,
                    ':_claim_id': fid,
                    ':_affectee': selectedAffectee
                };

                ['_odskodnina', '_odskodnina_additional'].map(key => {
                    data[key].map(o => o[5][0] = parseFloat(o[5][0]) || null);
                });

                if (key === 'complete') {
                    if (!request.attributes[':_data']._status) {    //this should never happen
                        alert(t`Sporazum lahko zaključiš le, če je določen njegov STATUS.`);
                        return false;
                    }
                    request.attributes[':_completed'] = true;
                }
            }
            else if (isAdmin && key === 'reopen') {
                request.attributes = {
                    ':_completed': false
                }
            }
        }

        cm.model.tableName = 'dmg_agreements';
        return cm.save(saveOptions);
    }

    function isDataChanged(caller) {

        for (const key of ['_affectee', '_odskodnina', '_status', '_odskodnina_additional']) {
            if (!initializedComponents[key]) return false;
        }

        const currentlySelectedAffectee = getCurrentlySelectedAffectee();
        
        if (!currentlySelectedAffectee) return true;
        const currentData = cm.val();
        const initialData = {};
        const affecteeFormData = affecteesFormData[currentlySelectedAffectee];

        Object.keys(currentData).map(key => {
            initialData[key] = affecteeFormData[key];
        });

        return !_.isEqual(currentData, initialData);
    }
}
