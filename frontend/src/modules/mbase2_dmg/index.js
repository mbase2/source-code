import * as exports from '../../libs/exports';
import globals from '../../app-globals';

export default async op => {
    const [
        utils,
        mutils,
        Mbase2ModuleIndex,
        DropDown
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        import('../Mbase2ModuleIndex'),
        exports.DropDown()
    ]);

    const t = utils.t;

    let moduleIndex;

    const singleRecordPath = '/zahtevek';

    const moduleIndexOptions = Object.assign({
        moduleKey:'dmg',
        buttonLabels: {
            new: t`Nov zahtevek`
        },
        title: t`Pregled škodnih zahtevkov`,
        viewName:'dmg_app_vw',
        urlForDelete: globals.apiRoot + `/mb2data/dmg`,
        unameField: '_uid',
        //hideColumnsFromTable: ['_photos'],
        singleRecordPath: singleRecordPath,
        tableOptions: {
            orderBy:['date_record_modified','desc']
        },
        batchOptionsModel: async (attributes) => ({
            dmg: [],
            dmg_batch_imports: await mutils.getModuleVariables('dmg_batch_imports', false)
        }),
        preprocessTableData: preprocessTableData,
        additionalVariables: (variables) => {
            return variables;
        }
    }, op);

    moduleIndexOptions.adminButton = {
        key: 'admin',
        classes: 'btn-lg',
        component: DropDown,
        iconClass: 'cog',
        items: [
            {
                key: 'mbase2/modules/dmg/deputies',
                label: t`Deputies`
            },
            {
                key: 'mbase2/modules/dmg/affectees',
                label: t`Affectees`
            },
            {
                key: 'mbase2/admin/modules/dmg',
                label: t`Module settings`
            },
            {
                key: 'mbase2/admin/editor/dmg',
                label: t`DB editor`
            },
            {
                key: 'mbase2/admin/code-lists?m=dmg',
                label: t`Code lists`
            }
        ],
        label: globals.t`Admin settings`,
        onClick: ({key}) => {
                //window.location=window.location.origin + '/' + key;
                window.open(window.location.origin + '/' + key, '_blank');
                return false;
            }     
    };

    moduleIndexOptions.overrideToolbarButtons = [
        {
            key: 'export',
            label: t`Export`,
            component: DropDown,
            iconClass:'download',
            type: 'btn-primary',
            classes: 'btn-lg',
            items: [
                {
                    key: 'full_export',
                    label: t`Full export`
                },
                {
                    key: 'dmg_objects',
                    label: t`Damage objects`
                },
                {
                    key: 'monthly_reports',
                    label: t`Export for monthly report`
                },
                {
                    key: 'dmg_objects_by_oe',
                    label: t`Damage objects by RU`
                }
            ],
            onClick: (item) => {
                const filterParametersString = moduleIndex.filterParametersString;

                if (item.key === 'full_export') {
                    utils.hrefClick(globals.apiRoot+'/export/dmg'+filterParametersString, null);
                }
                else if (item.key === 'dmg_objects') {
                    utils.hrefClick(globals.apiRoot+'/export/dmg/objects'+filterParametersString, null);
                }
                else if (item.key === 'monthly_reports') {
                    utils.hrefClick(globals.apiRoot+'/export/dmg/monthly-reports'+filterParametersString, null);
                }
                else if (item.key === 'dmg_objects_by_oe') {
                    utils.hrefClick(globals.apiRoot+'/export/dmg/dmg_objects_by_oe'+filterParametersString, null);
                }
            }
        }
    ];

    moduleIndex = new Mbase2ModuleIndex.default(moduleIndexOptions);
    await moduleIndex.init();

    async function preprocessTableData(data) {

        const isAdmin = moduleIndex.isAdmin;

        const outData = [];

        data.map(d=>{
            if (!d._batch_id) {
                outData.push(d);
            }

            d.__deletable = false;

            if (d._claim_agreement_status == 2) {
                d._claim_agreement_status = `<span class="label label-info">ZAKLJUČEN 2</span>`;
            }
            else if (d._claim_agreement_status == 1) {
                d._claim_agreement_status = `<span class="label label-danger">ZAKLJUČEN 1</span>`;
            }
            else if (d._claim_agreement_status == 0) {
                d.__deletable = isAdmin || globals.user.uid == d._uid;
                d._claim_agreement_status = `<span class="label label-default">NEZAKLJUČEN</span>`;
            }

            if (d._expected_event==1) {
                d._expected_event = t`DA`;
            }
            else if (d._expected_event==2) {
                d._expected_event = t`NE`;
            }
        });

        return outData;
    }
}
