/**
 * mbase2 settings
 * 
 * Modules
 * Variable definition
 * Code lists
 */

import './styles.css';
import {utils as _utils, mutils as _mutils} from '../libs/exports';
import globals from '../app-globals';

export default async ($parent, module, language, userData) => {
    const [
        utils,
        mutils
    ] = await Promise.all([
        _utils(),
        _mutils()
    ]);

    const t = utils.t;

    globals.language = language;
    globals.user = JSON.parse(userData);

    const $container = $('<div class="flex-box"></div>');
    const $header = $('<div class="flex-box-row header"></div>');
    const $content = $('<div class="flex-box-row content"></div>');
    const $footer = $('<div class="flex-box-row footer"></div>');
    
    $container.append($header);
    $container.append($content);
    $container.append($footer);

    async function loadModule(module) {
        $content.empty();

        if (module.startsWith('mbase2/admin/modules')) {
            const modules = await import('./modules');
            modules.default({
                $parent: $content,
                module: module.replace('mbase2/admin/modules/','')
            });
        }
        else if (module.startsWith('mbase2/admin/code-lists')) {
            const variables = await import('./codeLists');
            variables.default({
                $parent: $content,
                codeListKey: module.replace('mbase2/admin/code-lists/','')
            });
        }
        else if (module === 'mbase2/dataEditor') {
            const dataEditor = await import('./dataEditor');
            dataEditor.default({
                $parent: $content,
                label: t`Data records tables`
            });
        }
        else if (module === 'mbase2/modules/ct') {
            /*const module = await import('./mbase2_ct');
            module.default({
                $parent: $content
            });
            */
           const module = await import('./mbase2_ct');
            module.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/ct/ct_stations') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'ct_trap_stations',
                view: 'ct_view_trap_stations',
                additionaComponentOptions: {
                    '_location_reference': {    //additionaComponentOptions key_name_id
                        '_locations_table':'ct_locations'
                    }
                }
            });
        }
        else if (module === 'mbase2/modules/cnt/admin/cnt_permanent_spots') {
            const module = await import('./mbase2_cnt/cnt_permanent_spots');
            await module.default($content);
        }
        else if (module === 'mbase2/modules/cnt/admin/cnt_contacts') {
            const module = await import('./mbase2_cnt/cnt_contacts');
            await module.default($content);
        }
        else if (module === 'mbase2/modules/cnt') {
            const cnt_monitoring_id = utils.getQueryParameter('cnt_monitoring_id');
            if (cnt_monitoring_id) {
                const data = await mutils.requestHelper(globals.apiRoot + `/mb2data/cnt_monitoring/language/${globals.language}/${cnt_monitoring_id}`);
                
                if (!data) return;
                if (!data[0]) return;

                const monitoringData = data[0];
                
                if (monitoringData.key__vrsta_opazovanja==="stalna števna mesta") {
                    module = await import('./mbase2_cnt/cnt_observation_reports');
                }
                else {
                    module = await import('./mbase2_cnt/cnt_estimations');
                }
                
                await module.default({$header, $content, monitoringData});
            }
            else {
                const module = await import('./mbase2_cnt');
                module.default({
                    $header: $header,
                    $content: $content
                });
            }
        }
        else if (module === 'mbase2/modules/cnt/old') {
            const module = await import('./mbase2_cnt/cnt_monitoring');
            await module.default({$header,$content});
        }
        else if (module === 'mbase2/modules/ct/ct_sites') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'ct_sites'
            });
        }
        else if (module === 'mbase2/modules/ct/ct_cameras') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'ct_cameras'
            });
        }
        else if (module === 'mbase2/modules/ct/ct_surveys') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'ct_surveys'
            });
        }
        else if (module === 'mbase2/admin/referenced_tables/individuals') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'individuals',
                schema: 'mb2data'
            });
        }
        else if (module === 'mbase2/admin/referenced_tables/organisations') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'organisations',
                schema: 'mbase2'
            });
        }
        else if (module === 'mbase2/modules/genetics/administration/referenced_tables/samplers') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'gensam_samplers',
                schema: 'mb2data',
                tableRecordsOptions: {
                    deletable:true
                }
            });
        }
        else if (module === 'mbase2/modules/genetics/administration/referenced_tables/gensam_studies') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'gensam_studies',
                schema: 'mb2data',
                tableRecordsOptions: {
                    deletable:true
                }
            });
        }
        else if (module === 'mbase2/modules/genetics/administration/referenced_tables/gensam_populations') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'gensam_populations',
                schema: 'mb2data',
                tableRecordsOptions: {
                    deletable:true
                }
            });
        }
        else if (module === 'mbase2/modules/genetics/administration/referenced_tables/gensam_organisations') {
            const module = await import('../helpers/gensam_organisations');
            module.default({
                $parent:$content
            });
        }
        else if (module === 'mbase2/modules/genetics/administration/referenced_tables/gensam_ref_animals') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'gensam_ref_animals',
                schema: 'mb2data'
            });
        }
        else if (module === 'mbase2/modules/sop') {
            const module = await import('./mbase2_sop');
            module.default({
                $header: $header,
                $content: $content
            });
        }
        else if (module === 'mbase2/modules/tlm') {
            const module = await import('./mbase2_tlm');
            module.default({
                $header: $header,
                $content: $content
            });
        }
        else if (module === 'mbase2/modules/sop/sign') {
            const module = await import('./mbase2_sop/sign');
            module.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/genetics/samples') {
            const module = await import('./mbase2_gensam');
            module.default({
                $header: $header,
                $content: $content
            });
        }
        else if (module === 'mbase2/modules/genetics/samples/sample') {
            const module = await import('./mbase2_gensam/sample');
            module.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/gensam') {
            const module = await import('./mbase2_gensam');
            module.default({
                $header: $header,
                $content: $content
            });
        }
        else if (module === 'mbase2/modules/gensam/sample') {
            const module = await import('./mbase2_gensam/sample');
            module.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/map') {
            const module = await import('./mbase2_data_query');
            module.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/ct/ct_camelot_sources') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'ct_camelot_sources'
            });
        }
        else if (module.startsWith('mbase2/batches')) {
            const batchModule = await import('./batches');
            batchModule.default({
                showDownloads: true,
                $header,
                $parent: $content,
                moduleKey: module.replace('mbase2/batches/','')
            });
        }
        else if (module === 'mbase2/modules/dmg/zahtevek') {
            const module = await import('./mbase2_dmg/zahtevek');
            module.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/dmg/ceniki') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'dmg_objects_price_lists'
            });
        }
        else if (module === 'mbase2/modules/dmg/zahtevek/sporazumi') {
            const module = await import('./mbase2_dmg/sporazumi');
            module.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/dmg') {
            const module = await import('./mbase2_dmg');
            module.default({
                $header: $header,
                $content: $content
            });
        }
        else if (module === 'mbase2/modules/ct/camelot') {
            const module = await import('./mbase2_camelot');
            module.default({
                $header: $header,
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/interventions') {
            const module = await import('./mbase2_interventions');
            module.default({
                $header: $header,
                $content: $content
            });
        }
        else if (module === 'mbase2/modules/interventions/intervention') {
            const module = await import('./mbase2_interventions/intervention');
            module.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/interventions/interventors') {
            const uac = await import('./user_access');
            uac.default({
                $parent: $content,
                targetTable: 'interventions_interventors',
                labels: {
                    'Add users': t`Dodaj uporabnike`
                },
                title: t`Intervencijska skupina`
            });
        }
        else if (module === 'mbase2/modules/dmg/deputies') {
            const uac = await import('./user_access');
            uac.default({
                $parent: $content,
                targetTable: 'dmg_deputies',
                filter: '[ ["_roles","@>","[\\"mbase2-dmg-editors\\"]"], ["or","_roles","@>","[\\"mbase2-dmg-admins\\"]"] ]',
                labels: {
                    'Add users': t`Dodaj pooblaščence`
                },
                title: t`Pooblaščenci`
            });
        }
        else if (module === 'mbase2/modules/dmg/affectees') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'dmg_affectees'
            });
        }
        else if (module === 'mbase2/modules/dmg/dmo_price_list') {
            const priceList = await import('./mbase2_dmg/dmo_price_list');
            priceList.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/test') {
            const test = await import('./test');
            test.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/tlm-api') {
            const module = await import('./mbase2_tlm_api');
            module.default({
                $parent: $content
            });
        }
        else if (module === 'mbase2/modules/tlm_animals') {
            const module = await import('./dataEditor');
            module.default({
                $parent: $content,
                key_id: 'tlm_animals',
                schema: 'mb2data'
            });
        }
        else if (module === 'mbase2/admin/editor/dmg') {
            const module = await import('./mbase2_dmg/adminEditor');
            module.default({
                $parent: $content
            });
        }
    }

    loadModule(module);

    $parent.append($container);
}
