import * as exports from '../../libs/exports';
import globals from '../../app-globals';
import './import_genotypes.css';
/**
 */
import { jsonParse } from '../../libs/utils';
import { hrefClick } from '../../libs/utils';


export default async op => {
    const [
        mutils,
        TranslationHandler,
        Button,
        Accordion,
        SimpleTable,
        NucleotideViewer,
        Tabs
    ] = await Promise.all([
        exports.mutils(),
        exports.TranslationHandler(),
        exports.Button(),
        exports.Accordion(),
        exports.SimpleTable(),
        import('./nucleotide_viewer'),
        exports.Tabs()
    ]);

    const th = new TranslationHandler.default();

    showResults(op.dialog, op.result);

    function createContent(key, data) {

        let $html = $('<div/>');

        if (key === 'missing_value') {
            const attributes = ['row', 'name', 'marker', 'sequence'].map(key =>
                mutils.createVariableDefinition(key, th.t`${key}`, 'text')
            );
            
            const table = new SimpleTable.default({
                attributes,
                removableRows: false
            });

            data.map(row => {
                table.add(row);
            });

            $html = table.$el();
        }
        else if (key === 'same_seq_diff_name_same_marker') {
            const attributes = ['row', 'name'].map(key => 
                mutils.createVariableDefinition(key, th.t`${key}`, 'text')
            );

            $html = $('<div/>');

            for (const drow of data) {
                let $div = $('<div/>',{class:'flex-label-text'});
                $div.append(`<div class="genotype-error-label label label-primary">${th.t`Marker`}</div>`);
                $div.append(`<div class="genotype-error-value label label-default">${drow.marker}</div>`);
                
                const $seq = $(`<a href="#" class="label label-default" style="align-content: center;">
                    <span>${drow.seq}</span>
                </a>`)
                $div.append(`<div class="genotype-error-label label label-primary">${th.t`Sequence`}</div>`);
                $div.append($seq);
                $html.append($div);

                $seq.on('click',()=>{
                    NucleotideViewer.default({
                        sequence: drow.seq
                    });
                });

                const table = new SimpleTable.default({
                    attributes,
                    removableRows: false
                });

                jsonParse(drow.found_rows).map(row => {
                    table.add(row);
                });

                $html.append(table.$el());
            }
        }
        else if (key === 'diff_seq_same_name_same_marker') {
            const attributes = ['row', 'sequence'].map(key => 
                mutils.createVariableDefinition(key, th.t`${key}`, 'text')
            );

            $html = $('<div/>');

            for (const drow of data) {
                let $div = $('<div/>',{class:'flex-label-text'});
                $div.append(`<div class="genotype-error-label label label-primary">${th.t`Marker`}</div>`);
                $div.append(`<div class="genotype-error-value label label-default">${drow.marker}</div>`);
                
                $div.append(`<div class="genotype-error-label label label-primary">${th.t`Allele`}</div>`);
                $div.append(`<div class="genotype-error-value label label-default">${drow.name}</div>`);
                $html.append($div);

                const table = new SimpleTable.default({
                    attributes,
                    removableRows: false
                });

                jsonParse(drow.found_rows).map(row => {
                    row[1] = `<a href="#" class="sequence-display-text">
                        <span>${row[1]}</span>
                    </a>`;
                    table.add(row);
                });

                $html.append(table.$el());
            }
        }
        else if (key === 'empty_sample_code') {
            createReportTable($html, ['row'], data);
        }
        else if (key === 'wrong_sample_code') {
            createReportTable($html, ['row', 'sample code'], data);
        }
        else if (key === 'missing_allele_pair') {
            createReportTable($html, ['row', 'allele', 'marker'], data);
        }
        else if (['genotype_exists', 'access_denied_samples'].indexOf(key) !== -1) {
            const tdata = [];
            Object.keys(data).map(row => {
                tdata.push([row, data[row]]);
            });
            createReportTable($html, ['row', 'sample code'], tdata);
        }
        else if (['multiplicated_sample_row_same_genotype', 'multiplicated_sample_row'].indexOf(key) !== -1) {
            const tdata = [];
            Object.keys(data).map(sampleCode => {
                tdata.push([sampleCode, JSON.stringify(data[sampleCode])]);
            });
            createReportTable($html, ['sample code', 'lines where repeated'], tdata);
        }
        else if (key === 'unreferenced_allele') {
            const tdata = [];
            Object.keys(data).map(dkey => {
                const parts = dkey.split('|');
                if (parts.length === 3) {
                    tdata.push(parts);
                }
            });
            createReportTable($html, ['row', 'allele', 'marker'], tdata);
        }
        else if (key === 'multiplicated_sample_row') {
            const tdata = [];
            Object.keys(data).map(sample_code => {
                tdata.push([sample_code, JSON.stringify(data[sample_code])]);
            })
            createReportTable($html, ['sample code', 'rows'], tdata);
        }
        else if (key === 'empty_markers') {
            const tdata = [];
            Object.keys(data).map(rowInx => {
                tdata.push([rowInx, data[rowInx]]);
            });

            createReportTable($html, ['row', 'sample_code'], tdata);
        }

        $html.find('table').css('table-layout','fixed');

        return $html;
    }

    function createReportTable($html, columnKeys, data, op = {}) {
        const attributes = columnKeys.map(key => 
            mutils.createVariableDefinition(key, th.t`${key}`, 'text')
        );

        const table = new SimpleTable.default(Object.assign({
            attributes,
            removableRows: false
        }, op));

        data.map(row => {
            table.add(row);
        });

        $html.append(table.$el());
    }
    
    function onDownloadImportedGenotypes(batchId) {
        hrefClick(globals.apiRoot+'/download-genotypes/'+batchId, null);
    }

    function onDownloadAlleleMapping(batchId) {
        hrefClick(globals.apiRoot+'/download-allele-map/'+batchId, null);
    }

    function showResults(dlg, res) {
        const importReportData = {
            genotype: {
                $div: $('<div/>'),
                label: th.t`Genotypes validation`,
                keyLabels: {
                    'empty_sample_code': th.t`Empty sample code`,
                    'wrong_sample_code': th.t`Sample code in wrong format`,
                    'access_denied_samples': th.t`Sample codes with denied access`,
                    'genotype_exists': th.t`Sample codes with existing genotype`,
                    'missing_allele_pair': th.t`Missing allele pair`,
                    'empty_markers': th.t`Samples without genotype`,
                    'unreferenced_allele': th.t`Unreferenced allele name`,
                    'multiplicated_sample_row': th.t`Same sample code - different genotype`,
                    'multiplicated_sample_row_same_genotype':th.t`Same sample code - same genotype`
                },
                panels:[],
                errors: res[1]
            },
            alleleRef: {
                $div: $('<div/>'),
                label: th.t`Referenced allele sequence validation`,
                keyLabels: {
                    'missing_value': th.t`Missing value`,
                    'same_seq_diff_name_same_marker': th.t`Repeated sequence with different allele name`,
                    'diff_seq_same_name_same_marker': th.t`Repeated allele name with different sequence`
                },
                panels:[],
                errors: res[0]
            },
            report: {
                data: res[2],
                keyLabels: {
                    'reference_allele_count': th.t`Number of rows in allele seq. page`,
                    'all_referenced_reference_allele_count': th.t`Number of ALL valid and referenced rows in allele seq. page`,
                    'unique_referenced_reference_allele_count': th.t`Number of UNIQUE valid and referenced rows in allele seq. page`,
                    'existing_alleles_count': th.t`Number of allele seq. page alleles in the database before the import`,
                    'insertedAlleleSequencesCount': th.t`Number of imported referenced allele seq.`,
                    'all_sample_genotypes_count': th.t`Number of rows in genotypes page`,
                    'invalid_sample_code_count': th.t`Number of genotypes with empty or invalid sample code`,
                    'same_sample_code_different_genotype_count': th.t`Number of genotypes with different genotype data for the same sample code`,
                    'multiplicated_genotype_row_count': th.t`Number of multiplicated rows (including sample code) in the genotypes page`,
                    'gtypes_with_unreferenced_allele_count': th.t`Number of genotypes with at least one unreferenced allele`,
                    'gtypes_with_one_allele_pair_count': th.t`Number of genotypes with one or more single alleles missing`,
                    'valid_sample_genotypes_count': th.t`Number of genotypes for import`,
                    'existing_samples_with_genotypes_count': th.t`Number of samples in the database with genotype data already present`,
                    'access_denied_samples_count': th.t`Number of samples with denied access`,
                    'insertedGenotypesCount': th.t`Number of imported genotypes`,
                    'found_samples_count': th.t`Number of samples in the database where the genotype data was added`,
                    'numberOfSamplesCreatedCount': th.t`Number of inserted samples`,
                }
            }
        };
        
        for (const pageKey of Object.keys(importReportData)) {
            if (pageKey === 'report') continue;
            const keyLabels = importReportData[pageKey].keyLabels;
            const errors = importReportData[pageKey].errors;
            const panels = importReportData[pageKey].panels;

            for (const key of Object.keys(keyLabels)) {
                if (keyLabels[key] && Object.keys(errors[key]).length > 0) {
                    panels.push({
                        key, label: keyLabels[key],
                        content: createContent(key, errors[key])
                    });
                }
            }
        }

        const batchId = res[4];

        const buttons = [
            new Button.default({
                label: th.t`Download the allele mapping table`,
                type: 'btn-primary',
                onClick: () => onDownloadAlleleMapping(batchId)
            }),
            new Button.default({
                label: th.t`Download the imported genotypes`,
                type: 'btn-primary',
                onClick: () => onDownloadImportedGenotypes(batchId)
            })
        ];

        const $buttonsDiv = $('<div/>',{
            style:'display:flex; justify-content:center'
        });

        buttons.map(btn=>$buttonsDiv.append(btn.$el()));

        dlg.$footer.append($buttonsDiv);

        let alertColor = '#e9f4e9';
        let alertBorderColor = '#3f9c35';
        let alertIcon = '<span class="pficon pficon-ok genotype-report-dialog-icon"></span>';
        let alertMsg = th.t`Successfully imported.`;

        const errorException = res[3];

        const report = res[2];

        let styleReportData = () => {};

        const samplesPresentOrAccessDeniedMsg = th.t`This could be because genotypes for some samples are either already present or you don't have the permissions to add genotype data to the respective samples.`;

        if (errorException || report.all_sample_genotypes_count == 0 ||
            report.valid_sample_genotypes_count == 0 || report.insertedGenotypesCount == 0) {
            alertColor = '#ffe6e6';
            alertBorderColor = '#c00';
            alertIcon = '<span class="pficon pficon-error-circle-o genotype-report-dialog-icon"></span>';
            alertMsg = th.t`Database error` + ': '+ errorException;

            if (report.all_sample_genotypes_count == 0) {
                alertMsg = th.t`No genotypes found in the uploaded file.`
                styleReportData = ($html) => {
                    $html.find('.all_sample_genotypes_count').closest('tr').css('background-color', alertColor).css('font-weight','bold');
                }
            }
            else if (report.valid_sample_genotypes_count == 0) {
                alertMsg = th.t`No valid genotype data for the import found.`;
                styleReportData = ($html) => {
                    ['invalid_sample_code_count',
                    'same_sample_code_different_genotype_count',
                    'multiplicated_genotype_row_count',
                    'gtypes_with_unreferenced_allele_count',
                    'gtypes_with_one_allele_pair_count'].map(key => {
                        if (report[key] !==0) {
                            $html.find('.' + key).closest('tr').css('background-color', alertColor);
                        }
                    });
                    $html.find('.valid_sample_genotypes_count').closest('tr').css('background-color', alertColor).css('font-weight','bold');
                }
            }
            else if (report.insertedGenotypesCount == 0) {
                alertMsg = th.t`There were some valid genotype data found - but none of them was imported.`;
                if (report.existing_samples_with_genotypes_count > 0 || report.access_denied_samples_count > 0) {
                    alertMsg = alertMsg + ' ' + samplesPresentOrAccessDeniedMsg;
                }
                
                styleReportData = ($html) => {
                    ['existing_samples_with_genotypes_count',
                    'access_denied_samples_count',
                    'insertedGenotypesCount'].map(key => {
                        if (report[key] !==0) {
                            $html.find('.' + key).closest('tr').css('background-color', alertColor);
                        }
                    });
                    
                    $html.find('.insertedGenotypesCount').closest('tr').css('background-color', alertColor).css('font-weight','bold');
                }
            }
        }
        else if (report.all_sample_genotypes_count == report.valid_sample_genotypes_count && 
            report.valid_sample_genotypes_count == report.insertedGenotypesCount) {
                alertColor = '#e9f4e9';
                alertBorderColor = '#3f9c35';
                alertIcon = '<span class="pficon pficon-ok genotype-report-dialog-icon"></span>';
                alertMsg = th.t`All the data was successfully imported.`;
        }
        else {
            alertColor = '#fdf2e5';
            alertBorderColor = '#ec7a08';
            alertIcon = '<span class="pficon pficon-warning-triangle-o genotype-report-dialog-icon"></span>';
            alertMsg = th.t`Some data might not be imported.`;

            if (report.existing_samples_with_genotypes_count > 0 || report.access_denied_samples_count > 0) {
                alertMsg = alertMsg + ' ' + samplesPresentOrAccessDeniedMsg;
            }

            styleReportData = ($html) => {
                ['invalid_sample_code_count',
                    'same_sample_code_different_genotype_count',
                    'multiplicated_genotype_row_count',
                    'gtypes_with_unreferenced_allele_count',
                    'gtypes_with_one_allele_pair_count',
                    'existing_samples_with_genotypes_count',
                    'access_denied_samples_count',
                    'insertedGenotypesCount'
                ].map(key => {
                        if (report[key] !==0) {
                            $html.find('.' + key).closest('tr').css('background-color', alertColor);
                        }
                });
                $html.find('.insertedGenotypesCount').closest('tr').css('background-color', alertColor).css('font-weight','bold');
            }
        }

        if (report.insertedGenotypesCount == 0) {
            buttons.map(btn=>btn.setDisabled(true));
        }

        const tabItems = [];
        const tabDivs = [];

        const $importReportDiv = $('<div/>');
        tabItems.push(th.t`Import report`);
        createImportReport($importReportDiv, importReportData);
        tabDivs.push($importReportDiv);

        styleReportData($importReportDiv);

        dlg.$title.parent().css('background-color', alertColor).css('border-top', 'solid ' + alertBorderColor);

        const $div=$('<div/>', {class:'genotype-report-dialog-title'});
        $div.append(alertIcon);
        $div.append(alertMsg);

        dlg.$title.replaceWith($div);

        for (const pageKey of Object.keys(importReportData)) {
            if (pageKey === 'report') continue;
            if (importReportData[pageKey].panels.length > 0) {
                tabItems.push(importReportData[pageKey].label);
                const $div = importReportData[pageKey].$div;
                tabDivs.push($div);
                const acc = new Accordion.default({
                    $parent: $div,
                    panels: importReportData[pageKey].panels
                });
            }
        }

        tabDivs.map($div => $div.hide());

        const tabs = new Tabs.default({
            items: tabItems,
            onSelect: (inx) => {
                tabDivs.map($div => $div.hide());
                tabDivs[inx].show();
            }
        });

        tabs.val(0);

        dlg.$body.append(tabs.$el());
        for (const pageKey of Object.keys(importReportData)) {
            dlg.$body.append(importReportData[pageKey].$div);
        }

        $importReportDiv && dlg.$body.append($importReportDiv);
    }

    function createImportReport($div, importReportData) {
        const data = importReportData.report.data;
        const keyLabels = importReportData.report.keyLabels;
        const tdata = [];
        Object.keys(data).map(key => {
            tdata.push([`<span class="${key}">${keyLabels[key]}</span>`, data[key]]);
        });
        createReportTable($div, ['key', 'value'], tdata, {hideHeader: true});
    }
}