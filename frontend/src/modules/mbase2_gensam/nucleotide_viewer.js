import './nucleotide_viewer.css';
import * as exports from '../../libs/exports';
import globals from '../../app-globals';
/**
 */

export default async op => {
    const [
        ModalDialog
    ] = await Promise.all([
        exports.ModalDialog()
    ]);

    const {sequence} = op;

    const dlg = new ModalDialog.default({
        onShown: dlg => {
            const nucleotidesPerPage = 16;
            createNucleotideViewer(dlg.$body, sequence, nucleotidesPerPage);  
        }
    });

    dlg.show();

    function createNucleotideViewer($parentElement, sequence, nucleotidesPerPage = 16) {
    // Create the HTML structure dynamically
    const componentHTML = `
      <div class="nucleotide-component">
        <div class="nucleotide-carousel">
          <button class="control-btn" id="prev-16">Up 16</button>
          <button class="control-btn" id="prev-1">Up 1</button>
  
          <div class="nucleotide-display" id="nucleotideDisplay">
            <!-- Nucleotides with colored backgrounds will be displayed here -->
          </div>
  
          <button class="control-btn" id="next-1">Down 1</button>
          <button class="control-btn" id="next-16">Down 16</button>
        </div>
  
        <div class="sequence-text">
          Full Sequence: <span id="fullSequence"></span>
        </div>
      </div>
    `;
  
    // Append the component to the given parent element
    $parentElement.append(componentHTML);
  
    // Define colors for nucleotides
    const nucleotideColors = {
      A: '#4caf50',  // Green
      T: '#f44336',  // Red
      G: '#ffeb3b',  // Yellow
      C: '#2196f3'   // Blue
    };
  
    let startIndex = 0;  // Starting index for the visible nucleotides
  
    // Function to render the visible nucleotides in the "carousel"
    function renderNucleotides() {
      const $display = $('#nucleotideDisplay');
      $display.empty();  // Clear the display area
  
      // Get the current chunk of nucleotides
      const currentNucleotides = sequence.slice(startIndex, startIndex + nucleotidesPerPage);
  
      // Create boxes for each nucleotide
      for (let i = 0; i < currentNucleotides.length; i++) {
        const nucleotide = currentNucleotides[i];
        const nucleotideBox = $('<div class="nucleotide-box"></div>')
          .text(nucleotide)
          .css('background-color', nucleotideColors[nucleotide] || '#cccccc'); // Default to gray if undefined
  
        $display.append(nucleotideBox);
      }
  
      // Update the full sequence with highlighted section
      renderFullSequence();
    }
  
    // Function to render the full sequence with highlighted current section
    function renderFullSequence() {
      const $fullSeqElem = $('#fullSequence');
      $fullSeqElem.empty();  // Clear the existing text
  
      // Highlight the current visible nucleotides
      const before = sequence.slice(0, startIndex);
      const current = sequence.slice(startIndex, startIndex + nucleotidesPerPage);
      const after = sequence.slice(startIndex + nucleotidesPerPage);
  
      // Add the full sequence with highlighted visible section
      $fullSeqElem.html(`${before}<span class="highlight">${current}</span>${after}`);
    }
  
    // Move the sequence up or down based on button click
    function moveSequence(steps) {
      const maxIndex = sequence.length - nucleotidesPerPage;
      startIndex += steps;
  
      // Make sure the index is within bounds
      if (startIndex < 0) startIndex = 0;
      if (startIndex > maxIndex) startIndex = maxIndex;
  
      // Re-render the sequence
      renderNucleotides();
    }
  
    // Set up button controls
    $('#next-1').click(function() {
      moveSequence(1);  // Move down 1 nucleotide
    });
  
    $('#next-16').click(function() {
      moveSequence(nucleotidesPerPage);  // Move down 16 nucleotides
    });
  
    $('#prev-1').click(function() {
      moveSequence(-1);  // Move up 1 nucleotide
    });
  
    $('#prev-16').click(function() {
      moveSequence(-nucleotidesPerPage);  // Move up 16 nucleotides
    });
  
    // Initial render
    renderNucleotides();
  }
}