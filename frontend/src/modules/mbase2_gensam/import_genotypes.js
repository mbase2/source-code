import * as exports from '../../libs/exports';
import globals from '../../app-globals';
import './import_genotypes.css';
/**
 */

export default async op => {
    const [
        ModalDialog,
        FileUpload,
        mutils,
        TTomSelect,
        TranslationHandler,
        Button,
        Accordion
    ] = await Promise.all([
        exports.ModalDialog(),
        import('../../helpers/fileUpload'),
        exports.mutils(),
        exports.TTomSelect(),
        exports.TranslationHandler(),
        exports.Button(),
        exports.Accordion()
    ]);

    const th = new TranslationHandler.default();
    th.addCodeList('gensam_ui');

    const _labs = await mutils.requestHelper(globals.apiRoot + `/mb2data/gensam_labs_vw`);

    const _dlg = new ModalDialog.default({
        onShown: init
    });

    _dlg.show();

    let _fileUploadCmp = null;
    let _importBtn = null;
    let _uploadedCodes = null;

    const _cmp = {
        allelePageSelectorCmp: null,
        genotypePageSelectorCmp: null,
        gensamOrganisationCmp: null,
        analysisLabCmp: null
    };
    
    async function init() {
        _fileUploadCmp  = await FileUpload.default({onSelect: onDataFileSelect, $parent: _dlg.$body, subfolder: 'gensam', ext: 'xlsx'});
        _importBtn = new Button.default({
            label: th.t`Import`,
            type: 'btn-primary',
            onClick: onImportClicked
        });

        _dlg.$body.addClass('genotype-import');

        //_fileUploadCmp.components.select.val(17454, false)
        //_fileUploadCmp.components.select.val(17457, false)

        _dlg.$footer.append(_importBtn.$el());
        console.log(_importBtn)
        _importBtn.setDisabled(true);
    }

    async function onDataFileSelect(id, added) {
        const pages = await mutils.requestHelper(globals.apiRoot + `/get_data_source_pages/${id}`); 

        [{
            cmp: 'allelePageSelectorCmp',
            label: th.t`Select a page which holds the data for alleles`,
            onSelect: onPageSelected
        },
        {
            cmp: 'genotypePageSelectorCmp',
            label: th.t`Select a page which holds the data for genotypes`,
            onSelect: onPageSelected
        },
        {
            cmp: 'analysisLabCmp',
            label: th.t`Select analysis lab`,
            onSelect: onPageSelected,
            data: _labs.map(item => [item.id, item._full_name])
        }/*,
        {
            cmp: 'gensamOrganisationCmp',
            label: th.t`Select the organization on whose behalf you are uploading the data`,
            onSelect: onPageSelected,
        },*/
    
        ].map(item => {
            if (!_cmp[item.cmp]) {
                console.log('item.data', item.data)
                _cmp[item.cmp] = new TTomSelect.default({
                    label: item.label,
                    onSelect: item.onSelect,
                    data: item.data || [],
                    required: true
                });
                _dlg.$body.append(_cmp[item.cmp].$el());
            }
        });

        _cmp.analysisLabCmp.val(3, false); //test

        _cmp.allelePageSelectorCmp.reinit(pages.map((p, inx) => [inx, p] ));
        _cmp.genotypePageSelectorCmp.reinit(pages.map((p, inx) => [inx, p] ));
        _cmp.allelePageSelectorCmp.val(0, false);
        _cmp.genotypePageSelectorCmp.val(1, false);

        if (!_uploadedCodes) {
            _uploadedCodes = new Accordion.default({
                $parent: _dlg.$body,
                closeOthers: false,
                headingPointer: false,
                onPanelCreated: (key, label, $panel) => {        
                    const $heading = $panel.find('div.panel-heading');
                    const $input = $(`<input type="radio" name="codes" id="${key}">`);
                    $input.css('font-size','1.5em');
                    $input.on('click', () => {
                        const $a = $heading.find('a');
                        $a.trigger('click');
                    });
                    $heading.append($input);
                    $heading.css('display', 'flex');
                    $heading.css('align-items', 'center');
                    $heading.css('justify-content', 'space-between');
                    return $panel;
                },
                panels: [
                    {
                        key: 'cs-mbase2',
                        label: th.t`Sample codes follow the coding system in the database.`,
                        content: `
                        <div>
                        <ol class="custom-list">
                            <li class="custom-list-item">
                                When you upload a sample:
                                <ul class="custom-sublist">
                                    <li class="custom-sublist-item">
                                        If a sample with the same code exists in the database but lacks genotype information, the genotype from the uploaded sample will be added to it.
                                    </li>
                                    <li class="custom-sublist-item">
                                        If a sample with the same code exists in the database and already has genotype information, the genotype from the uploaded sample will not be imported.
                                    </li>
                                </ul>
                            </li>
                            <li class="custom-list-item">
                                If a sample with the specific code does not exist in the database, it will be added along with the genotype information from the uploaded sample.
                            </li>
                        </ol>
                        <hr>
                        <p class="custom-header">The coding system for the database requires:</p>
                        <ul class="custom-sublist">
                            <li class="custom-sublist-item">Exactly 6 characters.</li>
                            <li class="custom-sublist-item">
                                Characters must be selected from: <pre>012345678ACEFHJKLMPTUX</pre>
                            </li>
                            <li class="custom-sublist-item">The first character cannot be a digit.</li>
                        </ul>
                    </div>
                        `
                    },
                    {
                        key: 'cs-custom',
                        label: th.t`Sample codes use a custom coding system.`,
                        content: th.t`All the samples with a valid genotypes are going to be imported. Random database sample code will be generated and your sample code will be stored in the 2nd lab code field.`
                    }
                ]
            });

            //_uploadedCodes.$el().find('*').removeAttr('data-toggle').removeAttr('data-parent').removeAttr('data-target');
            _uploadedCodes.$el().find('a').trigger('click');
            _uploadedCodes.$el().find('#cs-mbase2').prop('checked', true);


        
        }
    }

    function onPageSelected() {
        const val1 = _cmp.allelePageSelectorCmp.val();
        const val2 = _cmp.genotypePageSelectorCmp.val();
        const val3 = _cmp.analysisLabCmp.val();

        if (val1 && val2 && val3 && val1 != val2) {
            _importBtn.setDisabled(false);
        }
        else {
            _importBtn.setDisabled(true);
        }
    }

    async function onImportClicked() {
        const upid = _fileUploadCmp.get('select').val();
        const page1 = _cmp.allelePageSelectorCmp.val();
        const page2 = _cmp.genotypePageSelectorCmp.val();
        const lab = _cmp.analysisLabCmp.val();
        const cs = _uploadedCodes.$el().find('#cs-custom').prop('checked') ? 1 : 0;

        const res = await mutils.requestHelper(globals.apiRoot + `/import-genotypes?upid=${upid}&page1=${page1}&page2=${page2}&lab=${lab}&cs=${cs}`, 'POST');

        const resultsDialog = new ModalDialog.default({
            onShown: async dlg => {
                const showResults = await import('./import_report');
                showResults.default({dialog: dlg, result: res});
            }
        })

        resultsDialog.show();
    }
}
