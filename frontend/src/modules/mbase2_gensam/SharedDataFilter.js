import * as exports from '../../libs/exports';
import globals from '../../app-globals';
/**
 */

export default async op => {
    const [
        mutils,
        ModalDialog,
        Accordion,
        TranslationHandler
    ] = await Promise.all([
        exports.mutils(),
        exports.ModalDialog(),
        exports.Accordion(),
        exports.TranslationHandler()
    ]);

    let _previousValue = [];
    
    const _togglers = {};
    let _sharedDataOptionsModal = null;

    const th = new TranslationHandler.default();
    th.addCodeList('gensam_ui');

    const t = th.t.bind(th);

    _sharedDataOptionsModal = new ModalDialog.default({
        size: 'modal-xs',
        titleText: t`Shared data filter`,
        onShown:() => {
        
        },
        onClose:() => {
            val(_previousValue);
        }
    });

    const panels = [
        {
            key: '1',
            label: t`Shared with me`,
            content: t`This option shows the data of other users shared with me or with organisations I am a member of.` 
        },
        {
            key: '2',
            label: t`Shared by me`,
            content: t`This option shows the data uploaded by me and shared with other organisations or/and users.`
        },
        {
            key: '4',
            label: t`Unshared data`,
            content: t`This option shows data uploaded by me and not shared with other organisations or/and users.`
        }
    ];

    if (mutils.permissions(['admin'], 'gensam')) {
        panels.push({
            key: '3',
            label: t`Uploaded by others`,
            content: t`This option shows the data uploaded by others and not shared with you - you can manage this data because you are a module administrator.`
        });
    }

    new Accordion.default({
        $parent: _sharedDataOptionsModal.$body,
        closeOthers: false,
        onPanelCreated: createHtmlOption,
        panels: panels
    });

    _sharedDataOptionsModal.$footer.append(op.sharedDataButtons.applyButton.$el());

    val(op.inversePredefinedFilter || []);
    updateSharedButtonState();
    /**
     * 
     * Functions
     */

    function switchOff($i) {
        $i.removeClass('fa-toggle-on').addClass('fa-toggle-off').removeClass('toggle-on-color').removeClass('is-on');
    }

    function switchOn($i) {
        $i.removeClass('fa-toggle-off').addClass('fa-toggle-on').addClass('toggle-on-color').addClass('is-on');
    }

    function toggleSharedOptionsButton($i) {
        if ($i.hasClass('is-on')) {
            switchOff($i);
        }
        else {
            switchOn($i);
        }
    }

    function onToggleButtonClick(key, $i) {
        toggleSharedOptionsButton($i);
    }

    function updateSharedButtonState() {
        let switchedOn = 0;
        Object.keys(_togglers).map(key => {
            if (_togglers[key].hasClass('is-on')) {
                switchedOn++;
            }
        });

        if (switchedOn === Object.keys(_togglers).length) {
            op.sharedButton.cmp.$el().find('i:first').removeClass('red-color');
        }
        else {
            op.sharedButton.cmp.$el().find('i:first').addClass('red-color');
        }  
    }

    function val(value) {
        if (value) {
            _previousValue = [...value];

            Object.keys(_togglers).map(key => {
                switchOn(_togglers[key]);
            });

            value.map(key => {
                switchOff(_togglers[key]);
            });

            return;
        }

        return Object.keys(_togglers).filter(key => !_togglers[key].hasClass('is-on'));
    }

    function createHtmlOption(key, label, $panel) {        
        const $heading = $panel.find('div.panel-heading');
        const $i = $('<i class="fa fa-toggle-off"></i>');
        _togglers[key] = $i;
        $i.css('font-size','1.5em');
        $heading.append($i);
        $heading.css('display', 'flex');
        $heading.css('align-items', 'center');
        $heading.css('justify-content', 'space-between');
        $i.on('click',function(event) {
            event.preventDefault();
            event.stopPropagation();
            onToggleButtonClick(key, $i);
            return false;
        });
        return $panel;
    }

    return Object.freeze({
        val,
        show: () => _sharedDataOptionsModal.show()
    });
}