import * as exports from '../../libs/exports';
import globals from '../../app-globals';

export default async op => {
    const [
        utils,
        mutils,
        Mbase2Module,
        TSelect2,
        DropDown,
        ComponentManager,
        Accordion,
        Storage,
        TTomSelect
        
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        import('../Mbase2Module'),
        exports.Select2(),
        exports.DropDown(),
        exports.ComponentManager(),
        exports.Accordion(),
        exports.Storage(),
        exports.TTomSelect()
    ]);

    const t = utils.t;

    let m = null;

    let $sampleTypePropertiesElement = null;

    let $completeBtn = null;

    const mediaRootFolder = '/private/gensam';

    const locationTypeData = await mutils.getCodeList('location_types');

    const uiTranslations = await mutils.getCodeList('gensam_ui');

    const uiTranslationsByKey = {};

    uiTranslations.map(item => {
        uiTranslationsByKey[item.key] = item;
    });

    m = new Mbase2Module.default({
        onRefreshStatus: onRefreshStatus,
        onSectionsInitialized: () => onSampleTypeSelected(),
        localStorageTemplateKey: "gensam-sample-template",
        tableName: 'gensam',
        moduleKey: 'gensam',
        viewName: 'gensam_vw',
        rootRelativePath:'genetics/samples',
        title: mutils.keyToTranslation(`Genetic samples`, uiTranslationsByKey),
        $parent: op.$parent,
        subtitle: {
            new: mutils.keyToTranslation(`new`, uiTranslationsByKey),
            edit: mutils.keyToTranslation(`edit`, uiTranslationsByKey)
        },
        sectionContainer: 'div',
        sectionContainerOverrides: {
            _location_data: 'acc',
            _photos: 'acc'
        },
        buttons: [   
            {
                key: 'new',
                label: mutils.keyToTranslation(`New`, uiTranslationsByKey)
            },  
            {
                key: 'edit',
                label: mutils.keyToTranslation(`Edit`, uiTranslationsByKey),
                visible: false,
                onClick: (that) => {
                    that.save('save', 'uncomplete');
                }
            },  
            {
                key: 'save-and-new',
                label:mutils.keyToTranslation(`Complete & Save & New`, uiTranslationsByKey),
                title: mutils.keyToTranslation(`Complete and save this entry and start a new one`, uiTranslationsByKey),
                onClick: (that) => {
                    
                    if (that.sectionsValidated()) {
                        if (that._initialData.completed === true) {
                            alert(mutils.keyToTranslation(`Error: the record is already completed.`, uiTranslationsByKey));
                            return;
                        }

                        that.save('save', 'complete');
                    }
                }
                
            },
            {
                key: 'save',
                label: mutils.keyToTranslation(`Save`, uiTranslationsByKey)
            }
        ],
        onBeforeRequest: (key, request, cm, additionalAction=null) => {
            if (key!=='save') return;
            
            if (additionalAction === 'complete') {
                request.attributes[':completed'] = true;
            }
            else if (additionalAction === 'uncomplete') {
                request.attributes = {
                    ':completed':false
                };
            }

            request.attributes[':_uid'] = 1;
        },
        skipVariables: ['_batch_id', '_uid','_uname','_location','date_record_created', 'date_record_modified'],
        skipDefaultVariables: true,
        componentOrder: [
            
        ],
        componentOverrides: {
            sample_code:sample_code,
            samplers:samplers,
            module_link: module_link
        },
        hideComponents: ['sample_properties','hybridization_level','hybridization_method_detection'],  //this prevents bookmark creation
        componentDefinitionOverrides: {
            sample_properties: {
                component: Storage,
                additionalComponentOptions: {
                    returnNullWhenEmpty:true
                }
            },
            _location_data: {
                additionalComponentOptions: {
                    defaultSpatialRequest: {
                        apiRoot: globals.apiRoot
                    },
                    moduleVariables: () => m.variables,
                    revert: true,
                    additionalCRS: [[3765, 'EPSG 3765 - HTRS96/TM'], [8677, 'EPSG 8677 - MGI 1901 / Balkans zone 5']],
                    distanceFromSettlement: false,
                    localName: true,
                    locationTypeData: locationTypeData,
                    confirmChange: true,
                    onSpatialRequestFinished: res=>{
                        m.refreshState();
                    }
                }
            },
            _sample_type: {
                additionalComponentOptions: {
                    onSelect: onSampleTypeSelected
                }
            },
            genetic_species: {
                additionalComponentOptions: {
                    onSelect: onGeneticSpeciesSelected
                }
            },
            quality_index: {
                additionalComponentOptions: {
                    max: 1,
                    min: 0
                }
            },
            user_access_ids: {
                additionalComponentOptions: {
                    additionalPlugins: ['remove_button'],
                }
            },
            organisation_access_ids: {
                additionalComponentOptions: {
                    additionalPlugins: ['remove_button'],
                }
            }
        },

        beforeVariableAddedToSection: v => {
            if (v.key_name_id.startsWith('sample_properties.')) {
                return false;
            }
            return true;
        },

        beforeInitSections: async (self) => {
            
            const $generalData = await  mutils.addSingleAccordionPanel(self.cm, exports, mutils.keyToTranslation(`General data`, uiTranslationsByKey), '_general_data_section_container', self.$right);

            const $fieldData = await  mutils.addSingleAccordionPanel(self.cm, exports, mutils.keyToTranslation(`Field data`, uiTranslationsByKey), '_field_data_section_container', self.$right);

            const $labdData = await  mutils.addSingleAccordionPanel(self.cm, exports, mutils.keyToTranslation(`Laboratory data`, uiTranslationsByKey), '_lab_data_section_container', self.$right);

            self.variableParents = {};

            const variableParents = {
                'sample_code': $generalData,
                'second_lab_code': $generalData,
                '_location_data': $fieldData,
                'study': $generalData,
                'data_quality': $generalData,
                'organisation': $generalData,
                'analysing_lab': $generalData,

                '_sample_collection_method':$fieldData,
                '_sample_type':$fieldData,
                '_notes':$fieldData,
                '_notes_to_lab':$generalData,
                '_species_name':$fieldData,
                '_licence_name':$generalData,
                'event_date': $fieldData,
                'samplers': $fieldData,
                'module_link': $fieldData,
                '_data_quality': $fieldData,
                'gensam_populations_id': $labdData,

                'lab_notes': $labdData,
                'extraction_date': $labdData,
                analysis_status: $labdData,
                analysis_result: $labdData,
                ref_animal_id: $labdData,
                results_date: $labdData,
                quality_index: $labdData,
                hybridization_level: $labdData,
                genetic_species: $labdData,
                genetic_sex: $labdData,
                hybridization_method_detection: $labdData
            };

            Object.keys(variableParents).map(key => {
                    self.variableParents[key] = variableParents[key];
            });
            
            const $generalDataAttributes = await mutils.addSingleAccordionPanel(self.cm, exports, mutils.keyToTranslation('General data attributes', uiTranslationsByKey), 'General data attributes', self.$left);
            const $fieldDataAttributes = await mutils.addSingleAccordionPanel(self.cm, exports, mutils.keyToTranslation('Field data attributes', uiTranslationsByKey), 'Field data attributes', self.$left);
            const $labDataAttributes = await mutils.addSingleAccordionPanel(self.cm, exports, mutils.keyToTranslation('Lab data attributes', uiTranslationsByKey), 'Lab data attributes', self.$left);

            $generalData.$bookmarkContainer = $generalDataAttributes;
            $fieldData.$bookmarkContainer = $fieldDataAttributes;
            $labdData.$bookmarkContainer = $labDataAttributes;

            self.$left.css('padding-top','10px').css('padding-right','10px');
        },
        onSaveSuccessCallback: onSaveSuccessCallback,
        beforeHeaderIsCreated: beforeHeaderIsCreated,
        onComponentAppended:onComponentAppended
    });

    await m.init();

    /**
     * initial user
     */
    const uid = m._initialData._uid;
    const userData = m.refValues.tableReferences.find(r => r.list_key == 'mbase2_users' && r.id==uid);

    if (userData && userData.key) {
        const uname = userData.key;
        const $moduleHeaderBottom = m.$header.find('#module-header-bottom');
        $moduleHeaderBottom.css('margin-bottom', '-15px');
        $moduleHeaderBottom.html(t`Record created by` + ': ' + uname);
    }

    const cmp = m.cm.get('acc__location_data');
    cmp.$el().css('margin-top','10px');

    $('.extended-title').css({'color':'#0088ce','font-weight':'bold'});

    const dropDown = new DropDown.default({
        classes: 'btn-lg',
        items: [
            {
                key: 'save-template',
                label: mutils.keyToTranslation(`Save template`, uiTranslationsByKey)
            },
            {
                key: 'reset-template',
                label: mutils.keyToTranslation(`Reset template`, uiTranslationsByKey)
            }
        ],
        label: mutils.keyToTranslation(`Template`, uiTranslationsByKey),
        onClick: ({key}) => {
                if (key === 'save-template') {
                    const values = JSON.parse(JSON.stringify(m.cm.val()));
                    
                    if (values.sample_code !== undefined) {
                        delete values.sample_code;
                    }
                    
                    localStorage.setItem("gensam-sample-template", JSON.stringify(values));
                    $.toaster({ message : mutils.keyToTranslation(`Template data successfully saved.`, uiTranslationsByKey) });
                }
                else if (key === 'reset-template') {
                    localStorage.removeItem('gensam-sample-template')
                    $.toaster({ message : mutils.keyToTranslation(`Template data successfully reset.`, uiTranslationsByKey) });
                }
                return false;
            }     
    });

    m.$buttonGroupContainer.css('display', 'flex');
    m.$buttonGroupContainer.prepend(dropDown.$el());

    m.group.buttons.map(b => b.$btn.css('margin-left', '10px'));

    $completeBtn = m.group.buttons.find(b => b.key === 'save-and-new').$btn;

    m.refreshState();

    function $createSampleTypePropertiesElement($el) {

        const acc = new Accordion.default({$parent: $el, closeOthers: true});
        const key = mutils.keyToTranslation(`Sample properties`, uiTranslationsByKey);
        acc.addPanel(key);
        return acc.getPanel(key).$body;
    }

    async function onGeneticSpeciesSelected() {
        const cmp = m.cm.get('genetic_species');
        if (!cmp) return;

        ['hybridization_level','hybridization_method_detection'].map(key => {
            const c = m.cm.get(key);
            c && c.$el().hide();
        });

        const selectedValue = cmp.val();

        const selectedValueData = m.refValues.codeListValues.find(clo => clo.id == selectedValue);

        if (selectedValueData && selectedValueData.key === 'hybrid') {
            ['hybridization_level','hybridization_method_detection'].map(key => {
                const c = m.cm.get(key);
                c && c.$el().show();
            });
        }
    }

    async function onSampleTypeSelected() {
        const cmp = m.cm.get('_sample_type');
        if (!cmp) return;
        //get key from id
        const id = cmp.val();
        if (!id) return;
        const sampleType = cmp.getAdditionalData(id);

        const attributes = m.variables.filter(v => {
            if (v._is_sample_property || v.key_name_id.startsWith('sample_properties.')) {
                v._data = utils.jsonParse(v._data);
                if (v._data && Array.isArray(v._data.sample_type)) {
                    return v._data.sample_type.indexOf(sampleType) !== -1 ? true : false;
                }
            }
            return false;
        }).map(a => {
            a.key_name_id = a.key_name_id.replace('sample_properties.','');
            a._is_sample_property = true;
            return a;
        });
        
        const refValues = m.refValues;

        $sampleTypePropertiesElement = $sampleTypePropertiesElement || $createSampleTypePropertiesElement(cmp.$el().parent());
        m.cm.get('sample_properties').val({});
        const samplePropertiesCm = new ComponentManager.default({
            onComponentChange: (key, _value, _cm) => {  //called on single item change
                const cmp = m.cm.get('sample_properties');
                const value = cmp.val() || {};
                value[key] = _value;
                cmp.val(value); //store samplePropertiesCm values to the overriden sample_properties component
                m.cm.op.onComponentChange(key, value, m.cm);
            }
        });

        $sampleTypePropertiesElement.empty();
        await mutils.createFormElements(samplePropertiesCm, $sampleTypePropertiesElement, attributes, refValues);

        if (m._initialData.sample_properties) {
            samplePropertiesCm.val(utils.jsonParse(m._initialData.sample_properties), false);
        }
    }

    function onRefreshStatus($status, cm) {
        const keys = Object.keys(cm.model.attributes);

        let requiredAttributesCompleted = true;

        for (const key of keys) {
            if (!cm.model.attributes[key].required) continue;
            const value = cm.get(key).val();

            if (value === undefined || value === null || (value.trim && value.trim()===''))  {
                requiredAttributesCompleted = false;
                break;
            }
            else if (cm.model.attributes[key].key_data_type_id === 'location_data_json') {
                if (!value.lat || !value.lon) {
                    requiredAttributesCompleted = false;
                    break;
                }
            }
        }

        if (requiredAttributesCompleted) {
            $status.html($(`<span class="label label-success">${mutils.keyToTranslation(`All required attributes are entered.`, uiTranslationsByKey)}</span>`));
            $completeBtn && $completeBtn.prop('disabled', false);
        }
        else {
            $status.html($(`<span class="label label-warning">${mutils.keyToTranslation(`Some required attributes are not entered.`, uiTranslationsByKey)}</span>`));
            $completeBtn && $completeBtn.prop('disabled', true);
        }

        const edit = m.group.buttons.find(btn => btn.key === 'edit');
        const save = m.group.buttons.find(btn => btn.key === 'save');
        const saveAndNew = m.group.buttons.find(btn => btn.key === 'save-and-new');

        if (m._initialData['completed'] === true) {
            $status.append($(`<span class="label label-info">${mutils.keyToTranslation(`This record is COMPLETED. For editing you have to unlock it.`, uiTranslationsByKey)}</span>`));
            $('.panel-group').addClass('disable-mouse-events');
            edit.$btn.show();
            save.$btn.prop('disabled', true);
            saveAndNew.$btn.hide();
        }
        else {
            edit.$btn.hide();
            saveAndNew.$btn.show();
            //save.$btn.prop('disabled', false)
            $('.panel-group').removeClass('disable-mouse-events');
            $status.append($(`<span class="label label-danger">${mutils.keyToTranslation(`This record is UNCOMPLETED.`, uiTranslationsByKey)}</span>`));
        }
    }

    /**
     * Variable component override
     * 
     * @param {object} $container jQuery object for the component's container
     * @param {object} cm ComponentManager of the form
     * @param {string} key variable key in the form
     */

    async function samplers($container, cm, key) {
        
        const variableDefinition = m.variables.find(v => v.key_name_id === key);

        const samplersOptions = m.refValues.tableReferences.filter(r=>r.list_key==='gensam_samplers').map(r => [r.id, r.key]);

        let cmp = null;

        cmp = await cm.add({
            key: key,
            component: TTomSelect.default,
            options: {
                tags: true,
                multiple: true,
                additionalPlugins: ['remove_button'],
                configurationOverrides: {
                    render: {
                        option_create: function(data, escape) {
                            return '<div class="create">'+ mutils.keyToTranslation(`New entry (sampler's name)`, uiTranslationsByKey) +' <strong>' + escape(data.input) + '</strong>&hellip;</div>';
                        },
                        item: function(data, escape) {
                            var $a = $('<a href="#"></a>');
                            const $div = $('<div/>');
                            $a.text(data.text);
                            data.$a = $a;
                            $a.on('click', () => {
                                personUpsert({
                                    text: data.text,
                                    id: data.value
                                }, cmp, samplersOptions);
                                return false;
                            })
                            $div.append($a);
                            return $div[0];
                        },
                    },
                    create: true,
                    createFilter: function (value) {
                        for (const key of Object.keys(this.options)) {
                            if (this.options[key].text == value) return false;
                        }
                        return true;
                    }
                },
                data: samplersOptions,
                helpIconText: mutils.parseHelpIconText(variableDefinition, globals.language),
                label: variableDefinition.t_name_id || variableDefinition.key_name_id,
                onSelect: (e, added) => {
                    if (!added) return;
                    personUpsert({
                        text: added.data.text,
                        newOption: true
                    }, cmp, samplersOptions);
                }
            },
            $parent: $container
        }, true);
    }

    let _gensamSamplersAttributes = null;
    let _gensamSamplersRefValues = null;

    async function personUpsert(data, cmp, selectOptions) {
        const cmr = new ComponentManager.default();

        if (data.newOption && data.text) {
            cmr.model.values = {
                slug: data.text
            }
        }
        else {
            const _samplers = await mutils.requestHelper(globals.apiRoot + `/mb2data/gensam_samplers/${data.id}`);
            cmr.model.values = _samplers[0];
        }

        _gensamSamplersAttributes = cmr.model.attributes = _gensamSamplersAttributes || await mutils.requestHelper(globals.apiRoot + `/module_variables_vw/language/${globals.language}?:module_name=gensam_samplers`);
        _gensamSamplersRefValues = _gensamSamplersRefValues || await mutils.getRefCodeListValues(_gensamSamplersAttributes);
        cmr.model.tableName = 'gensam_samplers';

        const dialog = await import('../../helpers/referencedTableDialog.js');
        dialog.default(cmr,{
            refValues: _gensamSamplersRefValues,
            onSuccessfullySaved: (_cm, values, add) => {
                if (add) {
                    cmp.tselect.addOption({value: values.id, text: values.slug});
                }
                else {
                    cmp.tselect.updateOption(values.id, {value: values.id, text: values.slug});
                }
                
                cmp.tselect.addItem(values.id);
            },
            onClose: () => {
                cmp.tselect.removeOption(data.text)
            }
        });
    }

    async function module_link($container, cm, key) {

        cm.add({
            key: 'module_link',
            component: Storage.default,
            options: {
                returnNullWhenEmpty: true
            }
        });

        const $div = $('<div/>', {style:"display: flex; justify-content: space-between"});
        const $msgLinkDiv = $('<div/>');
        const $btn = $('<button type="button" class="btn btn-default btn-xs" style="height:100%">'+mutils.keyToTranslation(`Link to other MBASE module`, uiTranslationsByKey)+'</button>');
        $div.append($msgLinkDiv);
        $div.append($btn);
        $container.html($div);

        if (m._initialData && m._initialData.module_link && m._initialData.module_link.id) {
            refreshModuleLinkData(m._initialData.module_link);
        }

        const moduleLinkSelector = await import('./moduleLinkSelector');

        $btn.on('click', async function () {
            const ModalDialog = await exports.ModalDialog();
            const modal = new ModalDialog.default({
                onShown: () => {
                    moduleLinkSelector.default({
                        $parent: modal.$body,
                        onLinkSelected: (data) => {
                            modal.hide();

                            cm.get('_location_data').val(utils.jsonParse(data._location_data));

                            cm.get('event_date').val(data.event_date);

                            delete data._location_data;
                            delete data.event_date;

                            cm.get('module_link').val(data);

                            cm.get('_location_data').$el().addClass('disable-mouse-events');
                            cm.get('event_date').$el().addClass('disable-mouse-events');

                            refreshModuleLinkData(data);
                        }
                    })
                }
            });
    
            modal.show();
        });

        function refreshModuleLinkData(data) {

            let additional = [];

            for (const [key, value] of Object.entries(data)) {
                if (['tname', 'id'].indexOf(key) === -1) {
                    additional.push(`${key}: ${value}`);
                }
            }

            additional = additional.join(',');
            additional = additional ? ', ' + additional : '';

            const msg = `${mutils.keyToTranslation(`This record is linked to the following other module record`, uiTranslationsByKey)}: <a id="module-link" href="#"><b>${data.tname}</b> (ID: ${data.id}${additional})</a>.<br>
            The date and coordinates of the sample are thus taken from the linked module record and cannot be edited via this form.`;

            $msgLinkDiv.html(mutils.alertCode(msg,'','info'));

            $msgLinkDiv.find('#module-link').on('click', () => {
                window.open(moduleLinkSelector.moduleRecordLink(data.tname, data.id), '_blank');
                return false;
            });
        }
    }

    async function sample_code($container, cm, key) {
        //get translated label for the component
        const variableDefinition = m.variables.find(v => v.key_name_id === key);

        const id_sample_code = await mutils.requestHelper(globals.apiRoot + `/mb2data/${m.op.viewName}?:__select=id,sample_code`);

        const idBySampleCode = {};

        id_sample_code.map(row => {
            const scode = row.sample_code && (row.sample_code+'').trim();
            if (scode) {
                idBySampleCode[scode] = row.id;
            }
        });

        await cm.add({
            key: key,
            component: TTomSelect.default,
            options: {
                configurationOverrides: {
                    create: true
                },
                required: true,
                helpIconText: mutils.parseHelpIconText(variableDefinition, globals.language),
                data: Object.keys(idBySampleCode).map(sampleCode => [sampleCode, sampleCode]),
                label: variableDefinition.t_name_id || variableDefinition.key_name_id,
                onSelect: (value, addedOption) => {

                    if (!addedOption) { //load existing
                        const fid = idBySampleCode[value];
                        if (fid) {
                            window.location = window.location.origin + window.location.pathname+'?fid='+fid
                        }
                    }
                    else {
                        m.fid && m._initialData && m._initialData.sample_code && m.copy(null, false); //if new sample was entered in existing sample data form then copy form data
                    }
                }
            },
            $parent: $container
        }, true);

        const cmp = cm.get(key);
        cmp.$el().attr('id','sample-code-selector-placeholder');
        cmp.$el().find('.ts-dropdown input.dropdown-input').css('text-transform','uppercase');
        //!cmp.val() && cmp.tselect.open();
    }

    function onSaveSuccessCallback(res, key, additionalAction) {
        if (additionalAction === 'uncomplete') {
            window.location.reload();
            return;
        }
        else if (additionalAction === 'complete') {
            window.location = window.location.origin + window.location.pathname;
            return;
        }

        m.updateSubtitle(m.op.subtitle.edit + ' ' + (res.sample_code || ''));
    }

    function beforeHeaderIsCreated() {
        const r = {};
        
        if (m.fid) {
            const c = m._initialData && m._initialData.sample_code;
            if (c) {
                r.modifiedSubtitle = (m._initialData.completed === true ? '' : m.op.subtitle.edit) + ' ' + c;
            }
        }

        return r;
    }

    function onComponentAppended(key, component, cm) {
        /**
         * disable date and location components if module_link is set
         */
        if (['event_date', '_location_data'].indexOf(key) !==-1) {
            if (m._initialData && m._initialData.module_link) {
                component.$el().addClass('disable-mouse-events');
            }
        }
        else if (key === 'analysis_status') {
            //default value gensam_analysis_status, entered
            if (!(m._initialData && m._initialData.id)) {
                const item = m.refValues.codeListValues.find(clo => clo.list_key == 'gensam_analysis_status' && clo.key == 'entered');
                component.val(item.id);
                setTimeout(()=>component.$el().trigger('change'), 100);
            }
        } 
    }
}