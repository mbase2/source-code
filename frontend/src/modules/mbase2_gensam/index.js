import * as exports from '../../libs/exports';
import globals from '../../app-globals';
import './gensam.css';

export default async op => {
    const [
        utils,
        mutils,
        Mbase2ModuleIndex,
        DropDown,
        ModalDialog,
        Button,
        importBatches,
        TranslationHandler,
        SharedDataFilter,
        ModalDialogMultipleSelector
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        import('../Mbase2ModuleIndex'),
        exports.DropDown(),
        exports.ModalDialog(),
        exports.Button(),
        import('../batches'),
        exports.TranslationHandler(),
        import('./SharedDataFilter'),
        exports.ModalDialogMultipleSelector()
    ]);

    const th = new TranslationHandler.default();
    th.addCodeList('gensam_ui');

    const t = th.t.bind(th);

    let moduleIndex;

    let _sharedDataFilter = null;
    let _individualsFilter = null;
    let _individualsFilterButton = null;
    let _$selectAll = null;

    const _individuals = {}; //here I store individuals once the table is initialised

    const singleRecordPath = '/sample';

    const uiTranslations = await mutils.getCodeList('gensam_ui');

    const uiTranslationsByKey = {};

    const _selectedRows = {};

    uiTranslations.map(item => {
        uiTranslationsByKey[item.key] = item;
    });

    const moduleIndexOptions = Object.assign({
        buttonLabels: {
            new: mutils.keyToTranslation(`New sample`, uiTranslationsByKey),
            filter: mutils.keyToTranslation(`All filters`, uiTranslationsByKey)
        },
        recordTimeStamps: true,
        moduleKey:'gensam',
        title: mutils.keyToTranslation(`Genetic samples`, uiTranslationsByKey),
        viewName:'gensam_vw',
        urlForDelete: globals.apiRoot + `/mb2data/gensam`,
        unameField: '_uid',
        skipDefaultVariables: false,
        skipVariables: ['_sighting_time', '_uname', '_animals_number', '_juvenile_number', '_batch_id', '_gps_colar_id','_prey_species','_marking_object', '_genetics', '_location_reference','_location', '_location_data'],
        //hideColumnsFromTable: ['_photos'],
        singleRecordPath: singleRecordPath,
        additionalTableIcons: [
            {
                prepend: true,
                action: 'check',
                class: 'square-o',
                onClick: (data, rowId, $a) => {
                    const $span = $a.find('span');
                    const isSelected = toggleSelectorSpan($span);
                    _selectedRows[data.id] = isSelected;
                    uncheckRowSelectBox(_$selectAll.find('span'));
                },
                style: 'margin-right: 5px',
                title: t`Select this row`,
                additionalClasses: 'outer-frame row-checkbox'
            }
        ],
        tableOptions: {
            orderBy:['date_record_modified','desc'],
            columnDefsByColumnName: {
                completed: {
                    width:'50px'
                }
            },
            doNotAddNoWrapClass: true,
            titleAttr: {
                completed: true
            },
            editColumnHeader: {
                title: `<a href="#" title="${t`Select all`}" class="outer-frame select-all"><span class="fa fa-square-o"></span></a>`
            }
        },
        preprocessTableData: preprocessTableData
    }, op);

    moduleIndexOptions.adminButton = {
        key: 'admin',
        classes: 'btn-lg',
        component: DropDown,
        iconClass: 'cog',
        items: [
            {
                key:'mbase2/admin/modules/gensam',
                label: mutils.keyToTranslation(`Module settings`, uiTranslationsByKey)
            },
            {
                key: 'mbase2/modules/genetics/administration/referenced_tables/gensam_ref_animals',
                label: mutils.keyToTranslation(`Reference animals`, uiTranslationsByKey)
            },
            {
                key: 'mbase2/modules/genetics/administration/referenced_tables/samplers',
                label: mutils.keyToTranslation(`Samplers`, uiTranslationsByKey)
            },
            {
                key: 'mbase2/modules/genetics/administration/referenced_tables/gensam_studies',
                label: mutils.keyToTranslation(`Studies`, uiTranslationsByKey)
            },
            {
                key: 'mbase2/modules/genetics/administration/referenced_tables/gensam_populations',
                label: mutils.keyToTranslation(`Populations`, uiTranslationsByKey)
            },
            {
                key: 'mbase2/modules/genetics/administration/referenced_tables/gensam_organisations',
                label: mutils.keyToTranslation(`Labs and other organisations`, uiTranslationsByKey)
            },
            {
                key: 'mbase2/admin/code-lists?m=gensam',
                label: mutils.keyToTranslation(`Code lists`, uiTranslationsByKey)
            }
        ],
        label: mutils.keyToTranslation(`Admin settings`, uiTranslationsByKey),
        onClick: ({key}) => {
                //window.location=window.location.origin + '/' + key;
                window.open(window.location.origin + '/' + key, '_blank');
                return false;
            }     
    };

    function uncheckRowSelectBox($span) {
        $span.removeClass('fa-check-square');
        $span.addClass('fa-square-o');
    }

    function checkRowSelectBox($span) {
        $span.removeClass('fa-square-o');
        $span.addClass('fa-check-square');
    }

    function toggleSelectorSpan($span, _isSelected) {

        if (_isSelected !== undefined) {
            if (_isSelected) {
                checkRowSelectBox($span);
            }
            else {
                uncheckRowSelectBox($span);
            }
            return _isSelected;
        }

        let isSelected;

        if ($span.hasClass('fa-check-square')) {
            uncheckRowSelectBox($span);
            isSelected = false;
        }
        else {
            checkRowSelectBox($span);
            isSelected = true;
        }

        return isSelected;
    }

    function applyAndResetButton(callbacks) {
        const applyButton = new Button.default({
            label: t`Apply`,
            style: 'float:right',
            onClick: () => callbacks.onApply && callbacks.onApply()
        });
    
        const resetButton = new Button.default({
            label: t`Reset`,
            style: 'float:right',
            onClick: () => callbacks.onReset && callbacks.onReset()
        });

        return {applyButton, resetButton};
    }

    moduleIndexOptions.additionalButtons = [
        {
            key: 'shared',
            type: 'btn-default',
            label: t`Shared`,
            iconClass: 'pficon pficon-users',
            
            onClick: () => {
                _sharedDataFilter.show();
            }
        }       
    ];

    const listOfGenotypeImports = new ModalDialog.default(
        {
            size: 'modal-xl',
            onClose: () => {
             
            },
            onShown: onListOfGenotypeImportsShown
    });

    function onListOfGenotypeImportsShown() {
        listOfGenotypeImports.$body.empty();
        importBatches.default({
            $parent: listOfGenotypeImports.$body,
            scrollY: '65vh',
            moduleKey: 'genotypes',
            showDownloads: true,
            fields: [{
                key_data_type_id: 'text',
                key_name_id: '__username',
                t_name_id: t`username`,
                visible_in_table: true
            },
            {
                key_data_type_id: 'date',
                key_name_id: '__date',
                t_name_id: t`date`,
                visible_in_table: true
            },
            {
                key_data_type_id: 'text',
                key_name_id: '__source',
                t_name_id: t`source`,
                visible_in_table: true
            },
            {
                key_data_type_id: 'text',
                key_name_id: 'note',
                t_name_id: t`note`,
                visible_in_table: true
            }],
            additionalIcons: [{
                action: 'report',
                class: 'info-circle',
                onClick: row => {
                    if (!row.data) return;
                    const data = utils.jsonParse(row.data);
                    const res = data.import_status;
                    if (res && res.length && res.length > 0) {
                        const resultsDialog = new ModalDialog.default({
                            onShown: async dlg => {
                                const showResults = await import('./import_report');
                                res[4] = row.id;
                                showResults.default({dialog: dlg, result: res});
                            }
                        })
                
                        resultsDialog.show();
                    }
                }
            }]
        });
    }

    moduleIndexOptions.tableHeightAdjustment = '-175px';

    moduleIndexOptions.overrideToolbarButtons = [
        {
            key: 'export',
            label: t`Export`,
            component: DropDown,
            iconClass:'download',
            type: 'btn-primary',
            classes: 'btn-lg',
            items: [
                {
                    key: 'download_samples',
                    label: t`Samples`
                },
                {
                    key: 'download_genotypes',
                    label: t`Genotypes`
                },
                {
                    key: 'download_allele_name_map',
                    label: t`Allele name map`
                }
            ],
            onClick: async (item) => {
                const filterParametersString = moduleIndex.filterParametersString;

                if (item.key === 'download_samples') {
                    utils.hrefClick(globals.apiRoot+'/export/gensam'+filterParametersString, null);
                }
                else if (item.key === 'download_genotypes') {
                    utils.hrefClick(globals.apiRoot+'/export-genotypes/'+filterParametersString, null);
                }
                else if (item.key === 'download_allele_name_map') {

                    let modalDialogSelector = null;

                    const exportBtn = new Button.default({
                        label: t`Export`,
                        style: 'float:right',
                        onClick: () => {
                            const alab_id = modalDialogSelector.val();
                            if (!alab_id) {
                                alert(t`The analysing lab is not selected.`);
                                return;
                            }
                            utils.hrefClick(globals.apiRoot+'/export-allele-name-map/'+alab_id, null);
                            modalDialogSelector.hide();
                        }
                    });

                    exportBtn.setDisabled(true);

                    modalDialogSelector = await ModalDialogMultipleSelector.default({
                        modalDialogOptions: {
                            size: 'modal-dialog',
                            titleText: t`Export allele name map`,
                            onShown: async () => {
                                const res = await mutils.requestHelper (globals.apiRoot+'/mb2data/gensam_allele_labs_vw', 'GET', null, {}, modalDialogSelector.$body);
                                modalDialogSelector.initModalDialogSelect(res.map(r=>[r.id, r._full_name]));
                            }
                        },
                        modalDialogSelectOptions: {
                            label: t`Select analysis lab`,
                            onSelect: (value) => {
                                exportBtn.setDisabled(false);
                            }
                        },
                        footerButtons: {
                            export: exportBtn
                        }
                    });

                    modalDialogSelector.show();
                
                    
                }
              
            }
        }
    ];

    moduleIndexOptions.onTableCreated = function(tableCmp, data) {

        initInidividualsFilter(tableCmp);

        const individualsColumnIndex = tableCmp.getColumnsMap().t_ref_animal_id;

        const $div = moduleIndex.$tableDiv.find('.dataTables_filter:first');
        
        const btn = _individualsFilterButton = new Button.default({
            type: 'btn-default',
            classes: 'btn-lg',
            label: t`Individuals`,
            iconClass:'fa-solid fa-paw',
            onClick: () => {
                _individualsFilter.show();
            }
        });
        
        btn.$el().css('display','inline-flex');
        btn.$el().css('margin-left','1em');
        $div.append(btn.$el());

        const dropDownActions = new DropDown.default({
            classes: 'btn-lg',
            items: [
                {
                    key: 'delete-genotypes',
                    label: t`Delete genotypes`
                },
                {
                    key: 'delete-gensam',
                    label: t`Delete samples`
                },
                {
                    key: 'access-add',
                    label: t`Add access permissions`
                },
                {
                    key: 'access-remove',
                    label: t`Remove all access permissions`
                },
                {
                    key: 'reference',
                    label: t`Mark as reference`
                }
            ],
            label: t`Actions on selected samples`,
            onClick: async ({key}) => {
                const ids = [];
                Object.keys(_selectedRows).map(id => _selectedRows[id] && ids.push(id));
                if (ids.length === 0) return;

                if (key === 'delete-genotypes') {    
                    await mutils.requestHelper(globals.apiRoot + '/delete-genotypes', 'DELETE', {':ids': ids});
                    location.reload();
                }
                else if (key === 'delete-gensam') {
                    await mutils.requestHelper(globals.apiRoot + '/delete-gensam', 'DELETE', {':ids': ids});
                    location.reload();
                }
                else if (key === 'access-add') {
                    let uacSelector = null;

                    const applyButton = applyAndResetButton({
                        onApply: async () => {
                            
                            const value = uacSelector.val();
                            if (!value.organisations && !value.users) {
                                uacSelector.hide();
                                return;
                            }
                            
                            const res = await mutils.requestHelper(globals.apiRoot + '/gensam-batch-update-uac', 'POST', {
                                ':ids': ids,
                                ':organisations': value.organisations,
                                ':users': value.users
                            });

                            alert(t`Updated records` + ' ' + `(${res.updated}/${ids.length})` + "\n" + t`The page will be reloaded.`);

                            if (parseInt(res.updated) > 0) {
                                location.reload();
                            }
                            else {
                                uacSelector.hide();
                            }
                        }
                    }).applyButton;
                    
                    uacSelector = await ModalDialogMultipleSelector.default({
                        modalDialogOptions: {
                            size: 'modal-xs',
                            titleText: t`Add access permissions to the selected records`,
                            onShown: async () => {
                                
                            }
                        },
                        modalDialogSelectOptions: {
                            'organisations': {
                                label: t`Organisations`,
                                multiple: true,
                                data: moduleIndex.allRefValues.tableReferences.filter(o => o.list_key == 'gensam_organisations').map(item => [item.id, item.key])
                            },
                            'users': {
                                label: t`Individual users`,
                                multiple: true,
                                data: moduleIndex.allRefValues.tableReferences.filter(o => o.list_key == 'mbase2_users').map(item => [item.id, item.key])
                            }
                        },
                        keys: [
                            'organisations', 'users'
                        ],
                        footerButtons: {
                            applyButton: applyButton
                        }
                    });

                    uacSelector.initModalDialogSelect();
            
                    uacSelector.show();
                }
                else if (key === 'access-remove') {
                    if (confirm(t`Do you really want to remove all access permissions for the selected records?`)) {
                        const res = await mutils.requestHelper(globals.apiRoot + '/gensam-batch-update-uac', 'POST', {
                            ':ids': ids,
                            ':remove': 1
                        });

                        alert(t`Updated records` + ' ' + `(${res.updated}/${ids.length})` + "\n" + t`The page will be reloaded.`);

                        if (parseInt(res.updated) > 0) {
                            location.reload();
                        }
                        else {
                            uacSelector.hide();
                        }
                    }
                }
            }
        });

        const $dropDownElement = dropDownActions.$el();

        $dropDownElement.css('position', 'absolute').css('left', 0);

        $div.prepend($dropDownElement);

        $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
            // Specify the column index to check (e.g., "Office" column, index 2)
            var columnValue = data[individualsColumnIndex]; // Get the data from the third column
            const filterRefSampleIdValues = _individualsFilter.val() || [];

            if (filterRefSampleIdValues.length === 0) {
                return true; // If no filter values, show all rows
            }

            const filterRefSampleCodeValues = filterRefSampleIdValues.map(id => _individuals[id][0].t_ref_animal_id);

            return filterRefSampleCodeValues.includes(columnValue);
        });

        const ids = data.map(row => row.id);

        _$selectAll = $('a.select-all');
        _$selectAll.on('click', () => {
            const isSelected = toggleSelectorSpan(_$selectAll.find('span'));
            const $span = tableCmp.$el().find('a.row-checkbox span');
            ids.map(id => {
                _selectedRows[id] = isSelected;
            });
            toggleSelectorSpan($span, isSelected);
        });    
    }


    moduleIndexOptions.additionalBatchImportOptions = [
        {
            key: 'import',
            label: t`Samples from XLSX`
        },
        {
            key: 'list',
            label: t`List of sample imports`
        },
        {
            key: 'genotype',
            label: t`Genotypes from XLSX`,
            callback: async () => {
                const genotypeImportModule = await import('./import_genotypes');
                genotypeImportModule.default();
            }
        },
        {
            key: 'genotypes_list',
            label: t`List of genotype imports`,
            callback: () => {
                listOfGenotypeImports.show();
            }
        }
    ];

    moduleIndex = new Mbase2ModuleIndex.default(moduleIndexOptions);
    await moduleIndex.init();

    _sharedDataFilter = await SharedDataFilter.default({
        inversePredefinedFilter: (moduleIndex.predefinedFilterValuesString && JSON.parse(moduleIndex.predefinedFilterValuesString)) || [],
        sharedButton: moduleIndex.buttons.find(btn => btn.key === 'shared'),
        sharedDataButtons:applyAndResetButton({
            onApply: () => {
                moduleIndex.executeQuery(JSON.stringify(_sharedDataFilter.val()));
            }
        })
    });

    const filterBtn = moduleIndex.buttons.find(btn => btn.key === 'filter');

    filterBtn.cmp.$el()
        .css('border-left', '1px solid rgb(187, 187, 187)')
        .css('padding-left', '10px');

    const moduleVariablesKeyed = {};
    
    moduleIndex.moduleVariables.map(a => moduleVariablesKeyed[a.key_name_id] = a);

    //https://gitlab.com/mbase2/source-code/-/wikis/Genetic-samples-index-page---sample-properties---frontend/src/modules/mbase2_gensam/index.js

    async function initInidividualsFilter(tableCmp) {
        
        const data = Object.keys(_individuals).map(id => [id, _individuals[id][0].t_ref_animal_id]);

        _individualsFilter = await ModalDialogMultipleSelector.default({
            modalDialogOptions: {
                size: 'modal-xs',
                titleText: t`Individuals filter`
            },
            modalDialogSelectOptions: {
                data: data,
                label: t`Select individuals you would like to filter out of the shown dataset`,
                multiple: true,
            },
            footerButtons: applyAndResetButton({
                onApply: () => {
                    _individualsFilterButton.$el().find('i').addClass('red-color');
                    tableCmp.table.draw();
                },
                onReset: () => {
                    _individualsFilter.val([]);
                    tableCmp.table.draw();
                    _individualsFilterButton.$el().find('i').removeClass('red-color');
                }
            })
        });

        _individualsFilter.initModalDialogSelect();
    }

    function preprocessTableData(data) {

        for (const row of data) {
            row.t_completed = row.completed = row.completed ? `<span class="label label-success">${(mutils.keyToTranslation(`COM`, uiTranslationsByKey))}</span>` : `<span class="label label-danger">${(mutils.keyToTranslation(`UNC`, uiTranslationsByKey))}</span>`;
            
            if (row.ref_animal_id) {
                if (!_individuals[row.ref_animal_id]) {
                    _individuals[row.ref_animal_id] = [];
                }

                _individuals[row.ref_animal_id].push(row);
            }

            if (row.sample_properties) {

                const sampleProperties = utils.jsonParse(row.sample_properties);
                const samplePropertiesKeys = Object.keys(sampleProperties);

                if (Array.isArray(sampleProperties)) {
                    row.sample_properties = '';
                    continue;
                }

                const props = [];
                samplePropertiesKeys.map(key=>{
                    let value = sampleProperties[key];
                    const attr = moduleVariablesKeyed['sample_properties.' + key];
                    
                    if (attr.data_type.includes('reference')) {
                        value = mutils.translateAttributeValue(value, attr, moduleIndex.allRefValues );   
                    }

                    if (attr.translations) {
                        attr.translations = utils.jsonParse(attr.translations);
                        key = attr.translations[globals.language] || key;
                    }
                    
                    props.push(`<b>${key}:</b> ${value}`);
                });

                row.sample_properties = props.join('<br>');
            }
        };
    }
}