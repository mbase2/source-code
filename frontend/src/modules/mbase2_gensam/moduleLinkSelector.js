import * as exports from '../../libs/exports';
import globals from '../../app-globals';
import record from '../../libs/record';
/**
 * @param {object} op options
 * @param {function} op.onLinkSelected callback function
 * @param {jQuery DOM object} op.$parent component container
 */
export default async op => {
    const [
        Select2,
        recordsTable,
        mutils,
        utils,
        ComponentManager
    ] = await Promise.all([
        exports.Select2(),
        import('../../modules/recordsTable'),
        exports.mutils(),
        exports.utils(),
        exports.ComponentManager()
    ]);

    const t = globals.t;

    const {$parent} = op;

    //select

    let selectCmp = null;

    const tableCm = new ComponentManager.default();

    selectCmp = new Select2.default({
            data: [['dmg',t`Damages on human property`],['sop', t`Signs of presence`]],
            label: t`Select module to link with`,
            onSelect: async () => {
                if (!selectCmp) return;
                const tname = selectCmp.val();
                if (!tname) return;
                await initTable(tname);
            }
        });

    $parent.html(selectCmp.$el());

    ///table
    const $tableDiv = $('<div/>');

    $parent.append($tableDiv);

    //await initTable('dmg');

    async function initTable(tname) {

        let attributes = [mutils.createVariableDefinition('__linked', t`Linked sample IDs`,'jsonb')];
        
        if (tname === 'dmg') {
            attributes = [...attributes,
                mutils.createVariableDefinition('_claim_id', t`Claim ID`, 'text'),
                mutils.createVariableDefinition('_dmg_start_date', t`Date`, 'date'),
                mutils.createVariableDefinition('_genetic_samples', '_genetic_samples', 'jsonb')
            ];
        }
        else if (tname === 'sop') {
            attributes = [...attributes,
                mutils.createVariableDefinition('event_date', t`Date`, 'date'),
                mutils.createVariableDefinition('_genetics', '_genetics', 'jsonb')
            ];
        }
            
        const recordsTableOptions = await mutils.generalTableRecordsOptions($tableDiv, tname, attributes);
        recordsTableOptions.btn_add_record = false;
        recordsTableOptions.btn_batch_import = false;

        recordsTableOptions.tableOptions = {
            scrollY: 'calc(75vh - 100px)'
        }

        recordsTableOptions.cm = tableCm;

        const queryParams = getQueryParams(tname);

        recordsTableOptions.url = globals.apiRoot + `/mb2data/${tname}?`+queryParams;
        
        const sampleTypeOptions = tname === 'dmg' ? await mutils.requestHelper(globals.apiRoot + `/code_list_options?:list_key=sample_type_options`) : null;

        const linked = await mutils.requestHelper(globals.apiRoot + `/mb2data/gensam?:__select=id,module_link->>'id' as module_link_id,sample_code&:__filter=[["module_link->>'tname'","=","${tname}"]]`);
        const linksByModuleRecordId = {};
        linked.map(linkedSample => {
            if (!linksByModuleRecordId[linkedSample.module_link_id]) linksByModuleRecordId[linkedSample.module_link_id] = [];
            linksByModuleRecordId[linkedSample.module_link_id].push(`${linkedSample.sample_code?linkedSample.sample_code+' ' : ''}(ID: ${linkedSample.id})`);
        })

        recordsTableOptions.preprocessTableData = (data, model) => {
            
            data.map(row => {
                if (linksByModuleRecordId[row.id]) {
                    row.__linked = linksByModuleRecordId[row.id].join('<br>');
                }
            });

            if (tname==='dmg') {
                preprocess_dmg_table_data(data);
            }
            else if (tname==='sop') {
                preprocess_sop_table_data(data);
            }

            return data;
        }

        recordsTableOptions.onRowSelected = (data) => {
            const out = {};
            out.tname = tname;
            out.id = data.id;
            
            if (tname==='dmg') {
                out.event_date = data._dmg_start_date;
                out._claim_id = data._claim_id;
            }
            else if (tname==='sop') {
                out.event_date = data.event_date;
            }

            out._location_data = data._location_data;

            op.onLinkSelected && op.onLinkSelected(out);
        }

        recordsTableOptions.externalEditRecord = (row, rowId, rowIndex) => {
            window.open(moduleRecordLink(tname, row.id), '_blank');
        }

        await recordsTable.default(recordsTableOptions);

        function getQueryParams(tname) {
            if (tname==='dmg') {
                return ':__select=id,_claim_id,_genetic_samples,_dmg_start_date,_location_data&:__filter=[["_genetic_samples","<>","[]"],["_genetic_samples","is not","null"]]'
            }
            else if (tname==='sop') {
                return ':__select=id,_genetics,event_date,_location_data&:__filter=[["_genetics","<>","[]"],["_genetics","is not","null"]]' 
            }     
            
        }

        function preprocess_dmg_table_data(data) {
            const lang = globals.language;
            for (const op of sampleTypeOptions) {
                op.translations = JSON.parse(op.translations);
            }
            const sampleTypeOptionsById = utils.convertToAssocArray(sampleTypeOptions);

            for(const row of data) {
                const sdata = JSON.parse(row._genetic_samples);
                const out = [];

                for (const s of sdata) {
                    let type = '';
                    if (s[1]) {
                        type = sampleTypeOptionsById[s[1]];
                        type = type.translations[lang] || type.key;
                    }

                    out.push(`<b>ID</b>: ${s[0]}, <b>${t`type`}</b>: ${type}, <b>${t`note`}</b>: ${s[2]}`);
                }
                row._genetic_samples = out.join('<br>');
            }
        }

        function preprocess_sop_table_data(data) {

            for(const row of data) {
                const sdata = JSON.parse(row._genetics);
                const out = [];

                for (const s of sdata) {
                    out.push(`<b>ID</b>: ${s[0]}, <b>${t`note`}</b>: ${s[2]}`);
                }
                row._genetics = out.join('<br>');
            }
        }
    }
}

export function moduleRecordLink(tname, id) {
    let urlName = null;

    if (tname === 'dmg') {
        urlName = 'zahtevek';
    }
    else if (tname ==='sop') {
        urlName = 'sign';
    }
    return window.location.origin + `/mbase2/modules/${tname}/`+urlName+'?fid='+id;
}