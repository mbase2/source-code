import * as exports from '../libs/exports';
import globals from '../app-globals';

import * as TSelect2 from '../libs/components/Tselect2';
import * as TTomSelect from '../libs/components/TTomSelect';

export default async op => {
    const {$parent} = op;

    let cmp;

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    const data = [
        'tine', 'tone', 'jure'
    ];

    cmp = new TTomSelect.default({
        data:data
        //allowEmptyOption:true
    });
    $parent.append(cmp.$el());

window.cmp=cmp;
    
}
