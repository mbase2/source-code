import globals from '../app-globals'

window.addEventListener('popstate', (event) => {
    //event.state && event.state.id && parseInt(event.state.id) > 0 && window.location.reload();
    window.location.reload();
});

let exports, utils, mutils, ComponentManager, ButtonGroup;
/**
 * 
 * @param {object} op 
 * @param {string} op.moduleKey
 */
function Mbase2Module(op ={}) {
    this.op=op;
    this.$parent = op.$parent;
}

Mbase2Module.prototype.init = async function() {
    exports = await import('../libs/exports');

    [
        utils,
        mutils,
        ComponentManager,
        ButtonGroup
    ] = this.imports = await Promise.all([
        exports.utils(),
        exports.mutils(),
        exports.ComponentManager(),
        exports.ButtonGroup()
    ]);

    const t = utils.t;

    const op = this.op;

    this.dataAfterSuccessfullSave = {};
    this.initialized = false;

    this.op.beforeVariableAddedToSection = this.op.beforeVariableAddedToSection || function(v){return true};

    this.op.addHeader = this.op.addHeader === undefined ? true : this.op.addHeader;

    op.sectionContainerOverrides = op.sectionContainerOverrides || {};
    op.skipVariables = ['_location','_batch_id', '_uname', ...(op.skipVariables || [])];
    op.buttons = op.buttons || [
        {
            key: 'save',
            label: utils.t`Save`
        }
    ];
        
    op.subtitle = op.subtitle && {
        edit: op.subtitle.edit || '',
        new: op.subtitle.new || ''
    };

    op.componentOverrides = op.componentOverrides || {};

    op.hideComponents = op.hideComponents || [];

    const bookmarks = this.bookmarks = {};
    const $status = this.$status = $('<div/>');
    const $buttonGroupContainer = this.$buttonGroupContainer = $('<div/>');

    const $parent = this.$parent;

    let $header = this.$header = null;
    
    this._initialData = {};
    this.sectionsByKey = {};

    const $breadcrumbs = this.$breadcrumbs = $('<div/>');

    const fid = this.fid = (this.op.initialData && this.op.initialData.id) || 
        (!this.op.ignoreQueryFid && utils.getQueryParameter('fid'));

    this.moduleComponentManager();

    let variables = this.variables = op.moduleVariables || await mutils.getModuleVariables(this.op.tableName || this.op.moduleKey, false, true, this.op.skipDefaultVariables === undefined ? true : this.op.skipDefaultVariables);
    
    variables = variables.filter(v => op.skipVariables.indexOf(v.key_name_id)===-1).filter(v=>v.visible===true)

    variables.sort((a, b) => (a.weight > b.weight) ? 1 : -1)

    this.buttons = {};
    this.op.buttons.map(btn => this.buttons[btn.key] = btn);

    if (fid) {
        const result = this.op.initialData ? [this.op.initialData] : await mutils.requestHelper(globals.apiRoot + `/mb2data/${this.op.viewName}/${fid}`);
        const res = result;
        if (res && res[0]) {
            mutils.convertJsonToObjects(res, variables);
            this._initialData = res[0];
            mutils.parseGeomField(this._initialData);
            this.cm.model.values = {id: fid};
        }
        else {
            alert(t`The requested record does not exist.`);
            if (!this.op.ignoreQueryFid) {
                let pname = window.location.pathname.split('/');
                pname.pop();
                pname = pname.join('/');
                window.location = window.location.origin + pname;
            }
        }
    }
    else {
        if (this.op.localStorageTemplateKey) {
            const templateData = localStorage.getItem(this.op.localStorageTemplateKey);
            
            if (templateData) {
                try {
                    this.copy(JSON.parse(templateData));
                }
                catch(error) {
                    console.log('error', error)
                }
                
            }
        }
    }

    if (this.op.addHeader) {
        let title = op.title;
        op.subtitle = op.subtitle || {};
        let subtitle = this.subtitle = this.fid ? op.subtitle.edit : op.subtitle.new;

        const breadcrumbs = this._initialBreadcrumbs();

        $breadcrumbs.html(utils.createBreadcrumbs(breadcrumbs));

        if (this.op.beforeHeaderIsCreated) {
            const {modifiedTitle, modifiedSubtitle} = this.op.beforeHeaderIsCreated();

            if (modifiedSubtitle)  {
                subtitle = this.subtitle = modifiedSubtitle;
            }

            if (modifiedTitle) {
                title = modifiedTitle;
            }
        }

        $header = this.$header = mutils.moduleHeader(title, subtitle, $buttonGroupContainer, $status, null, $breadcrumbs);
        $parent.append($header);
    }

    this._initButtonGroup();

    if (!op.sections) {
        this.sections = [];
        variables.map((v, inx) => {
            if (this.op.skipVariables.indexOf(v.key_name_id)===-1 && this.op.beforeVariableAddedToSection(v)) {
                
                let orderInx = this.op.componentOrder ? this.op.componentOrder.indexOf(v.key_name_id) : inx;
                if (orderInx === -1) orderInx = inx;    //if not found

                const translations = (v.translations && utils.jsonParse(v.translations)) || {};
                
                this.sections.push({
                    container:op.sectionContainerOverrides[v.key_name_id] || op.sectionContainer, 
                    hide: this.op.hideComponents.indexOf(v.key_name_id)!==-1, 
                    orderInx:orderInx, 
                    title: v.t_name_id || translations['en'] || v.key_name_id, 
                    variables: [v.key_name_id]
                });
            }
        });

        this.op.componentOrder && this.sections.sort((a,b) => (a.orderInx > b.orderInx) ? 1 : ((b.orderInx > a.orderInx) ? -1 : 0));
    }
    else {
        this.sections = [...op.sections];
    }

    this.cm.model.attributes = utils.convertToAssocArray(variables,'key_name_id');
    this.refValues = op.refValues || await mutils.getRefCodeListValues(this.variables);

    const $left = this.$left = $('<div style="float: left; height:100%;  width:20%; max-height: calc(100vh - 187px); overflow: auto"/>');
    const $right = this.$right = $('<div style="float: right; height:100%; width:80%; max-height: calc(100vh - 187px); overflow: auto; border-left: solid black 1px; padding:10px"/>');
    $parent.append($left);
    $parent.append($right);
    $('body').css('overflow','hidden');

    this.op.beforeInitSections && await this.op.beforeInitSections(this);
    await this._initSections();
    this.op.onSectionsInitialized && this.op.onSectionsInitialized();

    this.dataAfterSuccessfullSave = JSON.parse(JSON.stringify(this.cm.val()));
    this.initialized = true;

    this.refreshState();
}

Mbase2Module.prototype._initialBreadcrumbs = function () {
    
    return [
        {
            text: this.op.title,
            href: window.location.origin + `/mbase2/modules/` + (this.op.rootRelativePath || this.op.moduleKey)
        },
        {
            text: this.subtitle,
            class: 'active',
            href: window.location.href
        }
    ];
}

Mbase2Module.prototype.validateComponentSection = function (key, value, cm) {
    
    function validateComponentValue(key, value, bookmarkRequired, variableRequired) {
        if (!bookmarkRequired) return true;

        if (variableRequired === true) {
            if (utils.empty(value)) return false;
        }

        return true;
    }

    const cmp = cm.get(key);
    if (!cmp) return;
    const section = this.sectionsByKey[key];
    if (!section) return;
    
    const bm = this.bookmarks[section.title];

    if (!bm) return;

    let validated = validateComponentValue(key, value, bm.required, this.cm.model.attributes[key].required);

    if (validated) {
        for (const variableKey of section.variables) {
            if (variableKey === key) continue;
            const cmp = cm.get(variableKey);
            if (!cmp) continue;
            validated = validateComponentValue(variableKey, cmp.val(), bm.required, this.cm.model.attributes[variableKey].required);
            if (!validated) break;
        }
    }

    bm.validated = validated;
    
    if (bm.$bm) {
        if (validated) {
            if (bm.required === true) {
                bm.$bm.addClass('bookmark-completed');
            }
            else if (value) {
                bm.$bm.addClass('optional-bookmark-completed');
            }
        }
        else {
            const $el = bm.$bm;
            $el.removeClass('bookmark-completed');
            $el.removeClass('optional-bookmark-completed');
        }
    }
}

Mbase2Module.prototype.moduleComponentManager = function() {

    this.cm = new ComponentManager.default({
        onComponentChange: (key, value, cm) => {
            //console.log('onComponentChange', key, value, cm, this.initialized)
            this.validateComponentSection(key, value, cm);

            this.op.onComponentChange && this.op.onComponentChange(key, value, cm);

            this.initialized && this.refreshState();
        },
        onComponentAppended: (key, component, cm) => {

            const attr = this.cm.model.attributes[key];
            
            let value = this._initialData[key];

            //reference values can have additional data stored in the _data field
            if (attr) {
                if (attr.key_data_type_id==='code_list_reference' || attr.key_data_type_id==='code_list_reference_array') {
                    if (this._initialData._data && this._initialData._data[key]) {
                        value = {
                            select: value,
                            specify: this._initialData._data[key]
                        }
                    }
                }
            }

            this.op.onComponentAppended && this.op.onComponentAppended(key, component, cm);

            this.validateComponentSection(key, value, cm);

            if (value && component.val && !_.isEqual(value, component.val())) {
                component.val(value);
                cm.op.onComponentChange(key, value, cm);
            }
        }
    });
}

Mbase2Module.prototype.sectionsValidated = function (skipSections = []) {
    let sectionsValidated = true;
    const bookmarks = this.bookmarks;
    const keys = Object.keys(bookmarks);
    
    for (let key of keys) {

        if (skipSections.indexOf(key)!==-1) continue;
        
        const bm = bookmarks[key].parent ? bookmarks[bookmarks[key].parent] : bookmarks[key];
        
        if (bm.required && !bm.validated) {
            sectionsValidated = false;
            break;
        }
    }
    return sectionsValidated;
}

Mbase2Module.prototype.getComponentDefinition = async function($container, variable, removeLabel = true) {
    const cdef = await mutils.getComponentDefinition(
        this.cm.model.attributes[variable.key_name_id], 
        this.importDefinitions, 
        this.aliases,
        this.refValues, 
        true, 
        $container
    );

    if (removeLabel && cdef && cdef.options && cdef.options.label) {
        delete cdef.options.label;
    }

    cdef.key = variable.key_name_id;
    cdef.$parent = $container;
    return cdef;
}

Mbase2Module.prototype.createComponent = async function($container, variable, removeLabel = true) {
    const cdef = await this.getComponentDefinition($container, variable, removeLabel);
    const cmp = await this.cm.add(cdef);
    return cmp;
}

function onComponentFocused($bm) {
    if (!$bm) return;
    $('.bookmark-focus').removeClass('bookmark-focus');
    setTimeout(()=>$bm.addClass('bookmark-focus'),200);
}

Mbase2Module.prototype._initSections = async function() {

    const {componentDefinitions, definitionAliases} = (await import('../libs/components/componentDefinitions'));
    this.importDefinitions = componentDefinitions(exports);
    this.aliases = definitionAliases(utils.patterns);

    const cm = this.cm;

    for (const section of this.sections) {
        let $container = null;
        /////
        let $parent = this.$right;
        //check if section has $parent different than this.$right
        for (const key of section.variables) {
            if (this.variableParents && this.variableParents[key]) {
                $parent = this.variableParents[key];
                break;
            }
        }
        ////
        if (section.container === 'div' ) {
            $container = $('<div/>',{id: 'div_'+section.title});
            $parent.append($container);
        }
        else if (section.container === null) {

        }
        else {
            $container = await mutils.addSingleAccordionPanel(this.cm, exports, section.title, section.title, $parent);
        }

        const title = section.title;
        
        const $bm = $container ? mutils.bookmark({
            title: title, 
            required: false
        }) : null; 

        if ($bm) {
            $container.on('focus', 'input', () => {
                onComponentFocused($bm);
            });
            $container.on('focus', '.select2-selection.select2-selection--single', () => {
                onComponentFocused($bm);
            });

            $container.on('blur', 'input', () => {
                $bm.removeClass('bookmark-focus');
            });
           
            /*
            $('select.select2').on('select2:closing', function (e) {
                $(e.target).data("select2").$selection.one('focus focusin', function (e) {
                  e.stopPropagation();
                });
            });
            */

            $bm.on('click', () => {
                mutils.scrollToElement($container, this.$right);
            });
            
            this.bookmarks[section.title] = {
                $bm: $bm,
                required: false
            };

            section.hide && $bm.hide();

            if ($parent.$bookmarkContainer) {
                $parent.$bookmarkContainer.append($bm);
            }
            else {
                this.$left.append($bm);
            }
        }
        
        for (const key of section.variables) {
            
            this.sectionsByKey[key] = section;
            const variable = this.cm.model.attributes[key];

            if (variable && variable.required === true && this.bookmarks[section.title] && this.bookmarks[section.title].required === false) {   //if any of the section variables is required than change the section and bookmark to required
                $bm && $bm.removeClass('bookmark-optional');
                $bm && $bm.addClass('bookmark-required');
                $bm && $bm.children().text($bm.children().text()+'*');
                this.bookmarks[section.title].required = true;
            }

            if (this.op.componentOverrides[key]) {
                await this.op.componentOverrides[key]($container, cm, key, this.bookmarks[section.title]);
            }
            else {
                    
                if (this.op.componentDefinitionOverrides && this.op.componentDefinitionOverrides[key]) {

                    const o = this.op.componentDefinitionOverrides[key];
                    variable._component = {};

                    if (o._op) {
                        variable._op = Object.assign(variable._op || {}, o._op);    //_op are additional options that are passed as parameter into component definition found in componentDefinitions.js file
                    }

                    if (o.component) {
                        variable._component = Object.assign({}, {
                            import: () => o.component
                        }, o);
                    }
                    else if (o._component) {
                        variable._component = Object.assign({}, o._component);
                    }
                    else {
                        variable._component = Object.assign({},o, {default: true});
                    }
                }

                const cmp = await this.createComponent($container, variable, false);
                
                if (section.hide) {
                    cmp.$el && cmp.$el().hide();
                }
            }
        }
    }
}

Mbase2Module.prototype.refreshState = function() {
    if (!this.initialized) return;
    this.refreshButtonsState();
    this.op.onRefreshStatus && this.op.onRefreshStatus(this.$status, this.cm);
}

Mbase2Module.prototype.confirmFormLeave = function() {
    if (_.isEqual(this.dataAfterSuccessfullSave, this.cm.val())) return true;

    return confirm(utils.t`There are unsaved changes in your form - do you really want to leave? The unsaved changes will be lost.`);
}

Mbase2Module.prototype.refreshButtonsState = function() {
    this.group && this.group.buttons.map(btn => {
        const $btn = btn.$btn;
        if (btn.key === 'save') {
            if (_.isEqual(this.dataAfterSuccessfullSave, this.cm.val())) {
                $btn.addClass('btn-primary');
                $btn.prop('disabled', true);
                $btn.removeClass('btn-danger');
            }
            else {
                $btn.prop('disabled', false);
                $btn.removeClass('btn-primary');
                $btn.addClass('btn-danger');  
            }
        }
    });
}

Mbase2Module.prototype.addSectionContainer = async function (title, key, $parent=null, bm=false, panelOptions={}) {
    
    if (!$parent) $parent = this.$right;

    const $container = await mutils.addSingleAccordionPanel(this.cm, exports, title, key, $parent, 'acc_', false, panelOptions);

    if (!bm) return $container;

    const $bm = mutils.bookmark(bm);

    const onClick = () => {
        const panel = this.cm.get('acc_'+key);
        //console.log('$bm.onClick', key, panel, bm.$scrollContainer)
        mutils.scrollToElement(panel.$el(), bm.$scrollContainer);
    };

    $bm.on('click', () => bm.delayScroll ? setTimeout(onClick, 100) : onClick());

    this.bookmarks[key] = {
        $bm: $bm,
        required: bm.required
    };

    (bm.$parent || this.$left).append($bm);

    return {$container, $bm};
}

Mbase2Module.prototype._initButtonGroup = function() {
    const group = this.group = new ButtonGroup.default({
        highlight: false,
        classes: 'btn-primary btn-lg',
        onClick: async (data, key, self) => {
            this.actionHandler(key);
        },
        buttons: this.op.buttons,
        onLabelInit: key => { //proxy to pass this from component to children
            const btn = this.op.buttons.find(btn =>  btn.key === key);
            return btn.label(this)
        }
    });

    this.$buttonGroupContainer.html(group.$el());
}

Mbase2Module.prototype.updateSubtitle = function(subtitle) {
    this.$header && this.$header.find('span.extended-title').text(subtitle);
}

Mbase2Module.prototype.copy = function(data=null, callCallback = true) {
    if (!this.cm.model.values) this.cm.model.values = {};
    this.cm.model.values.id = this.fid = null;
    this._initialData = data || {};
    !this.op.ignoreQueryFid && window.history.pushState({'id':null},"", window.location.pathname);
    this.updateSubtitle(this.op.subtitle.new);
    this.subtitle = this.op.subtitle.new;
    this.$breadcrumbs.html(utils.createBreadcrumbs(this._initialBreadcrumbs()));
    const key = 'copy';
    callCallback && this.buttons[key] && this.buttons[key].callback && this.buttons[key].callback(this);
}

Mbase2Module.prototype.actionHandler = async function (key) {
               
    if (this.buttons[key] && this.buttons[key].onClick) {
        this.buttons[key].onClick(this);
        return;
    }

    if (key === 'copy') {
        this.copy();
        return;
    }
    else if (key === 'save') {
        this.save(key);
    }
    else if (key === 'new') {
        if (this.confirmFormLeave()) {
            window.location = window.location.origin + window.location.pathname;
        }
    }
}

/**
 * 
 * @param {string} key action name
 * @param {string} additionalAction this was introduced for save && complete functionality in gensam
 */

Mbase2Module.prototype.save = async function(key='save', additionalAction=null) {
    const saveOptions = await mutils.saveButtonHelper();
    saveOptions.rootUrl = globals.apiRoot + '/mb2data';
    
    this.cm.model.saveOptions.validate = false;
    
    this.cm.saveOptions.onSuccessCallbacks = [(cm, res) => {
        if (key === 'complete') {
            !this.op.ignoreQueryFid && window.location.assign(window.location.pathname + '?fid=' + res.id);
        }
        else if (key === 'save') {
            cm.model.values.id = this.fid = res.id;
            !this.op.ignoreQueryFid && window.history.pushState({'id':res.id},"", window.location.pathname + '?fid=' + res.id);
            this.dataAfterSuccessfullSave = JSON.parse(JSON.stringify(this.cm.val()));
            this.refreshButtonsState();
            this.op.onSaveSuccessCallback && this.op.onSaveSuccessCallback(res, key, additionalAction);
        }
    }];

    saveOptions.beforeRequest = request => {
        
        this.op.onBeforeRequest && this.op.onBeforeRequest(key, request, this.cm, additionalAction);

        if (key === 'complete') {
            //request.attributes[':_claim_status'] = 1;
        }
        else if (key === 'save') {
            
        }
        
        //request.attributes[':_uname'] = true;
        
    }

    this.cm.model.tableName = this.op.tableName;

    return this.cm.save(saveOptions);
}

export default Mbase2Module;