import globals from "../app-globals";

/**
 * editables.type
 */
export default (table, keys, op) => {

    const {editables} = op;
    
    let $input = null;
    
    table.table
    .on( 'key', function ( e, datatable, key, cell, originalEvent ) {
        //console.log( '<div>Key press: '+key+' for cell <i>'+cell.data()+'</i></div>' );
    } )
    .on( 'key-focus', function ( e, datatable, cell ) {

        const key = getCellHeaderKey(cell, keys);
        const editable = editables[key];
        if (!editable) return;
        editable.key = key;

        const $td = $(cell.node());
        const value = $td.text();
        $input = $(`<input style="${editable.type==='checkbox' ? '' : 'width:100%'}" type="${editable.type || 'text'}">`);
        
        if (editable.type === 'checkbox') {
            if (value === globals.t`DA`) {
                $input.prop('checked', true);
            }
        }

        $input.data('old-value', value);
        op.onChange && $input.on('change', () => {
            const oldValue = $input.data('old-value');
            const newValue = getValue(editable, $input);
            if (oldValue !== newValue) {
                op.onChange(
                    editable, 
                    newValue, 
                    cell, datatable);
                $input.data('old-value', newValue);
            }
        });
        $input.val(value);
        $td.html($input);
        $input.trigger('focus');
    } )
    .on( 'key-blur', function ( e, datatable, cell ) {
        
        if ($input) {

            const key = getCellHeaderKey(cell, keys);
            const editable = editables[key];
            if (!editable) return;
            editable.key = key;

            const $td = $(cell.node());
            
            const oldValue = $input.data('old-value');
            $input.remove();
            const newValue = getValue(editable, $input);
            $input && $td.text(newValue);
            
            if (oldValue != newValue && op.onChange) {
                op.onChange(editable, newValue, cell, datatable);
            }
            $input = null;
        }
        
        //console.log( '<div>Cell blur: <i>'+cell.data()+'</i></div>' );
    } );

    function getValue(editable, $input) {
        return editable.type === 'checkbox' ? ($input.prop('checked') ? globals.t`DA` : globals.t`NE`) : $input.val()
    }
    function getCellHeaderKey(cell, keys) {
        const position = cell.index();
        const column = position.column;

        return (keys[column].startsWith('t_') && keys[column].replace('t_','')) || keys[column];  //if column is code list, then key has 't_' prepended
    }
}