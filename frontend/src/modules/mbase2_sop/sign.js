import * as exports from '../../libs/exports';
import globals from '../../app-globals';

export default async op => {
    const [
        utils,
        mutils,
        Mbase2Module,
        ComponentManager,
        Button,
        SimpleTable,
        ModalDialog
        
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        import('../Mbase2Module'),
        exports.ComponentManager(),
        exports.Button(),
        exports.SimpleTable(),
        exports.ModalDialog()
    ]);

    const t = utils.t;

    const mediaRootFolder = '/private/sop';

    const hideComponents = ['_gps_colar_id',
                            '_prey_species',
                            '_marking_object'
    ];

    const moduleOptions = {
        tableName: 'sop',
        moduleKey: 'sop',
        viewName: 'sop_vw',
        title: t`Signs of presence`,
        $parent: op.$parent,
        subtitle: {
            new: t`new sign of presence`,
            edit: t`Edit sign of presence`
        },
        sectionContainer: 'div',
        sectionContainerOverrides: {
            _genetics: 'acc',
            _location_data: 'acc',
            _photos: 'acc'
        },
        onBeforeRequest: (key, request, cm) => {
            if (key !== 'save') return;
            request.attributes[':_uname'] = true;   //this is necessary for the backend to add current user name to the database
        },
        componentOrder: [
            '_licence_name',
            '_sign_of_presence',
            '_gps_colar_id',
            '_prey_species',
            '_marking_object',
            '_species_name',
            'event_date',
            'event_time',
            '_data_quality',
            '_animals_number',
            '_juvenile_number',
            'photos',
            '_genetics'
        ],
        componentOverrides: {
            _genetics: async function genetika($container, cm, key) {
                      
                const attributes = [
                    Object.assign(mutils.createVariableDefinition('_sample_id',t`Sample code`,'text'), {required: true}),
                    mutils.createVariableDefinition('_result',t`Analysis result`,'text'),
                    mutils.createVariableDefinition('_notes',t`Notes`,'text')
                ];
        
                let table = null;
        
                const button = new Button.default({
                  label: t`Add genetic sample data`,
                  onClick: async () => {
                    const cmr = new ComponentManager.default();
                    const record = await exports.record();
                    cmr.model.attributes = attributes;
        
                    const modal = new ModalDialog.default();
                    record.default({
                        $parent: modal,
                        cm: cmr,
                        saveOptions: {
                            beforeRequest: requestParameters => {
                                
                                if(!table) return;
                                
                                const attributes = requestParameters.attributes;
                                const row = {};
                                Object.keys(attributes).map(key => {
                                    row[key.substring(1)] = attributes[key];
                                });
        
                                table.add(row);
                                modal.hide();
                                return false;
                            }
                        }
                    });
                    modal.show();
                  }
                });
        
                $container.append(button.$el());
                $container.append('<br>');
        
                table = await cm.add({
                        key: key,
                        component: SimpleTable.default,
                        options: {
                            attributes: attributes,
                            language: globals.language
                        },
                        $parent: $container
                });
        
            }
        },
        componentDefinitionOverrides: {
            _photos: {
                additionalComponentOptions:{
                    mediaRoot: globals.mediaRoot + mediaRootFolder
                },
                //_op are additional options that are passed as parameter into component definition found in componentDefinitions.js file
                _op: {
                    fileUploadPath: '/file-upload/private/sop'
                }
            },
            _sign_of_presence: {
                additionalComponentOptions:{
                    onSelect: onSignOfPresenceSelected
                }
            },
            _location_data: {
                additionalComponentOptions: {
                    defaultSpatialRequest: {
                        apiRoot: globals.apiRoot
                    },
                    _locations_table: "sop",
                    distanceFromSettlement: true,
                    localName: true
                }
            }
        },
        hideComponents: hideComponents
    }

    const module = new Mbase2Module.default(moduleOptions);

    await module.init();

    componentsVisibility();
    //callbacks

    function onSignOfPresenceSelected() {
        componentsVisibility();
    }

    function componentsVisibility() {
        
        const cmp = module.cm.get('_sign_of_presence');
        if (!cmp) return;

        const signId = cmp.val();

        if (!signId) return;

        const selectedItem = module.cm.getData('_sign_of_presence').find(d => d.id == signId);

        hideComponents.map(key => {
            const cmp = module.cm.get(key);
            cmp && cmp.$el().hide();
            const bm = module.bookmarks[key];
            bm && bm.$bm.hide();
        });

        if (['sop.hair', 'sop.urine'].indexOf(selectedItem.key) !==-1) {
            const cmp = module.cm.get('_marking_object');
            cmp && cmp.$el().show();
            const bm = module.bookmarks['_marking_object'];
            bm && bm.$bm.show();
        }
        else if (selectedItem.key === 'sop.prey') {
            const cmp = module.cm.get('_prey_species');
            cmp && cmp.$el().show();
            const bm = module.bookmarks['_prey_species'];
            bm && bm.$bm.show();
        }
        else if (selectedItem.key === 'sop.capture') {
            const cmp = module.cm.get('_gps_colar_id');
            cmp && cmp.$el().show();
            const bm = module.bookmarks['_gps_colar_id'];
            bm && bm.$bm.show();        
        }
    }
}