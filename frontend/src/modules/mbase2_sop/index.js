import * as exports from '../../libs/exports';
import globals from '../../app-globals';

export default async op => {
    const [
        utils,
        mutils,
        Mbase2ModuleIndex,
        ImageGallery,
        DropDown
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        import('../Mbase2ModuleIndex'),
        exports.ImageGallery(),
        exports.DropDown()
    ]);

    const t = utils.t;

    let moduleIndex;

    const moduleIndexOptions = Object.assign({
        buttonLabels: {
            new: t`New sign of presence`
        },
        additionalButtons:[
            {
                key: 'image_gallery',
                label: t`Image gallery`,
                iconClass:'image',
                onClick: () => {
                    ImageGallery.default({
                        mediaRootFolder: '/private/sop',
                        globals: globals
                    });
                }
            }        
        ],
        batchOptionsModel: (attributes) => attributes.filter(a => a.dbcolumn === true || a.dbcolumn === undefined)
                            .filter(a => ['_batch_id', '_uname', '_genetics', '_location'].indexOf(a.key_name_id) ===-1)
                            .sort((a,b) => a.required ? -1 : 1),
        recordTimeStamps: false,
        moduleKey:'sop',
        title: t`Signs of presence`,
        viewName:'sop_vw',
        urlForDelete: globals.apiRoot + `/mb2data/sop`,
        skipVariables: ['_sighting_time', '_animals_number', '_juvenile_number', '_batch_id', '_gps_colar_id','_prey_species','_marking_object', '_genetics', '_location_reference','_location','_location_data'],
        hideColumnsFromTable: ['_photos'],
        singleRecordPath: '/sign'
    }, op);

    moduleIndexOptions.adminButton = {
        key: 'admin',
        classes: 'btn-lg',
        component: DropDown,
        iconClass: 'cog',
        items: [
            {
                key:'mbase2/admin/modules/sop',
                label: t`Module settings`
            },
            {
                key: 'mbase2/admin/code-lists?m=sop',
                label: t`Code lists`
            },
            {
                key: 'mbase2/admin/referenced_tables/individuals',
                label: t`Individuals`
            }
        ],
        label: globals.t`Admin settings`,
        onClick: ({key}) => {
                //window.location=window.location.origin + '/' + key;
                window.open(window.location.origin + '/' + key, '_blank');
                return false;
            }     
    };

    moduleIndex = new Mbase2ModuleIndex.default(moduleIndexOptions);
    await moduleIndex.init();
}