import globals from './app-globals';

import 'gasparesganga-jquery-loading-overlay';
import 'jquery.toaster';

$.toaster({ settings : {donotdismiss:['danger', 'warning'], toaster: {class:'toaster mbase2-pf'}} });

globals.language = 'sl';

$('.language-link').one('click',function() {
    const language = $(this).text().toLowerCase();
    const pathname = window.location.pathname;
    const pos = pathname.search('mbase2');
    if (pos!==-1) {
        window.location = window.location.origin + (language === 'en' ? '' : '/'+language) + '/' + pathname.substring(pos) + window.location.search;
    }
    return false;
});

export function mbase2_modules () {
    return import('./modules');
}

export function mbase2_camelot() {
    return import('./modules/mbase2_camelot');
}
  
  
