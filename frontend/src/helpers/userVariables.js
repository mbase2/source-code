import {requestHelper} from '../libs/mbase2_utils';
import globals from '../app-globals';

export default async (moduleKey) => {
    const res = await requestHelper(globals.apiRoot + `/mbase2/referenced_tables_vw?:key=user_groups_vw`);    
    return [
    {
        'key_name_id': 'name',
        'key_data_type_id': 'text',
        'required': true,
        'visible': true,
        'visible_in_table': true
    },
    {
        'key_name_id': 'username',
        'key_data_type_id': 'text',
        'visible': true,
        'required': true,
        'visible_in_table': true
    },
    {
        'key_name_id': 'email',
        'key_data_type_id': 'email',
        'visible': true,
        'required': true,
        'visible_in_table': true
    },
    {
        'key_name_id': 'password',
        'key_data_type_id': 'password',
        'visible': true,
        'required': true,
        'visible_in_table': false
    },
    {
        'key_name_id': 'groups',
        'key_data_type_id': 'table_reference_array',
        'ref': res[0] && res[0].id,
        'visible': true,
        'required': true,
        'visible_in_table': true,
        _component: {
            default: true,
            additionalComponentOptions: {
                additionalPlugins: ['dropdown_input', 'remove_button'] 
            },
            additionalPreprocessDataFilter: (row) => {
                return row.key.includes(moduleKey);
            }
        }
    }
];}