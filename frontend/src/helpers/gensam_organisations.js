export default async op => {

    function onOrganisationUsersChanged(cm) {
        if (!cm) return;

        const data = cm.components.gensam_organisations_users.getSelectedItemsData();
        const admins = cm.model.values && cm.model.values.gensam_organisations_admins;
        const cmp = cm.components.gensam_organisations_admins;
        cmp.reinit(data);
        cmp.val(admins);
    }
    
    const module = await import('../modules/dataEditor');
    let _cm = null;

    module.default({
        $parent: op.$parent,
        key_id: 'gensam_organisations',
        schema: 'mb2data',
        tableRecordsOptions: {
            deletable: true,
            onInitRecord: (cm, modal) => {
                _cm = cm;
                onOrganisationUsersChanged(cm);
            }
        },
        additionaComponentOptions: {
            'gensam_organisations_users': {
                onSelect: values => {
                    onOrganisationUsersChanged(_cm);
                },
                additionalPlugins: ['remove_button'],
                //dropdownParent: op.$parent[0],
                data : []
            },
            'gensam_organisations_admins': {
                additionalPlugins: ['remove_button'],
                data : []
            }
        }
    });


}
