import * as exports from '../libs/exports';
import globals from '../app-globals';

export default async (cm, op={}) => {
    const record = await exports.record();
    const ModalDialog = await exports.ModalDialog();

    const modal = new ModalDialog.default({
        onShown: () => op.onShown && op.onShown(),
        onClose: () => op.onClose && op.onClose()
    });
    
    record.default({
        $parent: modal,
        cm: cm,
        saveOptions: {
            rootUrl: globals.apiRoot + '/mb2data',
            urlParameters: ['json_flags=0'],
            beforeRequest: request => {

                if (op.beforeRequest) {
                    return op.beforeRequest(request);
                }

                return true;
            }
        },
        onInit: () => { 
            op.onInit && op.onInit();
        },
        refValues: op.refValues,
        
        onSuccessfullySaved: (cm, values, model, add) => {
            op.onSuccessfullySaved && op.onSuccessfullySaved(cm, values, add);
            modal.hide();
        }
    });
    modal.show();
}