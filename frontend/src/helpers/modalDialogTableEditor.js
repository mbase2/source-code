import globals from '../app-globals';
import * as exports from '../libs/exports';

/**
 * Modal dialog table editor
 * 
 * @params {Object} op
 * @params {string} op.tableName module_name from mbase2.module_variables_vw view
 * 
 */

export default async (op={}) => {
    const [
        mutils,
        recordsTable,
        ModalDialog
    ] = await Promise.all([
        exports.mutils(),
        exports.recordsTable(),
        exports.ModalDialog()
    ]);

    const {tableName} = op;
    const variables = await mutils.requestHelper(globals.apiRoot + `/mbase2/module_variables_vw/language/${globals.language}?:module_name=${tableName}`);

    const modal = new ModalDialog.default({
        onShown: async () => {
            const tableSchema = op.tableSchema || 'mb2data';

            const tableRecordsOptions = await mutils.generalTableRecordsOptions(modal.$body, tableName, variables);
            tableRecordsOptions.url = globals.apiRoot + '/' + tableSchema + '/' + tableName;
            tableRecordsOptions.saveOptions={
                    rootUrl: globals.apiRoot + '/' + tableSchema
            };
              
            tableRecordsOptions.disableEdit = true;
            tableRecordsOptions.skipId = true;
            tableRecordsOptions.tableOptions = {
                select: false,
                scrollY:'63vh'
            };
            tableRecordsOptions.onTableCreated = (table) => {
                    
            }
            recordsTable.default(tableRecordsOptions);
        }
    });
    modal.show();
}