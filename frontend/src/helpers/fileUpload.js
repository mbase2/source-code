/**
 * handles excel file upload
 */

import * as exports from '../libs/exports';
import globals from '../app-globals';

export default async (op = {}) => {
const [
    ComponentManager,
    utils,
    mutils
] = await Promise.all([
    exports.ComponentManager(),
    exports.utils(),
    exports.mutils()
]);

const components = {};
const ext = op.ext ? `?_ext=${op.ext}` : '';
components.select = exports.select({
    label: `Data source file`, 
    onSelect: op.onSelect, 
    configurationOverrides: {
        //openOnFocus:false
    },
    url: globals.apiRoot + '/uploaded-files/private' + (op.subfolder !== undefined ? '/' + op.subfolder : '') + ext,
    process: d => [d.id, d.file_name + (d.file_number === 0 ? '' : ` (${d.file_number})` )]
}, true, exports.TTomSelect);

components.button = {
    module: exports.FileUploadButton,
    options: {
        onChange: onFileSelected
    }
}

await utils.loadComponents(components);

const cm = new ComponentManager.default();
await cm.add(components.select, false, op.$parent);
await cm.add(components.button, false, op.$parent);

async function onFileSelected(file) {
    const res = await utils.request(globals.apiRoot + '/file-upload/private' + (op.subfolder !== undefined ? '/' + op.subfolder : ''), 'POST', file, mutils.requestCallbacksHelper());
    if (res === false) return;
    
    const data = cm.getData('select');
    data.push(res);
    const select = cm.refresh('select');
    select.val(res.id, false);
}
    return cm;
}