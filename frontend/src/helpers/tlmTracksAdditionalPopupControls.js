import {permissions} from '../libs/mbase2_utils';

/**
 * @param {Object[]} dataRow - dataRow representing single map item data
 */

export default (dataRow) => {

    const $highlight = $(`<span data-id="${dataRow.id}" data-action="show" class="tlm-track-action fa fa-lightbulb-o"></span>`);

    let highlightDataAction = 'show';

    if (dataRow && dataRow.__marker && dataRow.__marker.__highlighted) {
        $highlight.css('background-color','yellow'); 
        highlightDataAction = 'hide';
    }

    $highlight[0].dataset.action = highlightDataAction;

    let iconClass = 'fa-map-marker';

    if (dataRow && dataRow.__pnts) {
        iconClass = 'pficon-zone';
    }

    let detailPointsIcon = '';

    if (permissions(['*'], 'tlm')) {
        detailPointsIcon = `<span data-id="${dataRow.id}" data-action="detail" class="tlm-track-action fa ${iconClass}"></span>`;
    }

    return '<br>'+
    '<div class="tlm-track-action-controls">'+
    $highlight[0].outerHTML +
    detailPointsIcon +
    '</div>';
}