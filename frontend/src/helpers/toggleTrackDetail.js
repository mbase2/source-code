import {requestHelper} from '../libs/mbase2_utils';
import globals from '../app-globals';

/**
 * @param {Object[]} dataRow - dataRow representing single map item data
 * @param {Object} map -leaflet map object
 * @param {jQuery} $span - jQuery span DOM element holding fa icon
 */

export default async (dataRow, map, $span) => {
    if (dataRow.__pnts) {
        map.removeLayer(dataRow.__pnts);
        dataRow.__pnts = false;
        $span.removeClass('pficon-zone').addClass('fa-map-marker');
    }
    else {
        console.log(requestHelper);
        const res = await requestHelper(globals.apiRoot + `/mb2data/tlm_vw?:__filter=${JSON.stringify([["tlm_tracks_id","=",dataRow.id]])}`); 
        //https://gis.stackexchange.com/questions/360293/add-something-into-l-circlemarker

        $span.removeClass('fa-map-marker').addClass('pficon-zone');

        var geojsonMarkerOptions = {
            radius: 3,
            fillColor: "#ff7800",
            color: "#000",
            weight: 1,
            opacity: 1,
            fillOpacity: 0.8
        };

        const group = new L.FeatureGroup();

        res.map(row => {     
            group.addLayer(L.geoJSON(JSON.parse(row.geom)  , {
                pointToLayer: function (feature, latlng) {
                    return L.circleMarker(latlng, geojsonMarkerOptions);
                }
            }));
        });

        /*
        group.setStyle(
            function(feature){
                return {color: 'red'};
            }
        );
        */

        group.addTo(map);

        dataRow.__pnts = group;
    }
}