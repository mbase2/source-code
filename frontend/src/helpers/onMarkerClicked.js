import * as exports from '../libs/exports';
import globals from '../app-globals';
import { convertToAssocArray, compare } from '../libs/utils';

import tlmTracksAdditionalPopupControls from './tlmTracksAdditionalPopupControls';

/**
 * 
 * @param {object} marker 
 * @param {object} row 
 * @param {object} op
 * @param {object} [op.dataTable] DataTable component (used for collecting formated and translated data)
 * @param {object} op.moduleKey
 * @param {object} [op.moduleAttributes] module attributes definitions (not needed if op.dataTable is present)
 * @param {object} op.moduleAttributes.attributes
 * @param {object} op.moduleAttributes.attributesKeyed
 * @param {object} [op.moduleAttributes.refValues]
 * 
 */

export const onMarkerClicked = async (marker, dataRows, op, actionCallback) => {

    const [
        ImageArray,
        mutils,
        ImageViewer,
        ModalDialog
    ] = await Promise.all([
        exports.ImageArray(),
        exports.mutils(),
        exports.ImageViewer(),
        exports.ModalDialog()
    ]);

    const {dataTable, moduleKey, moduleAttributes} = op;

    const popupContent = [];

    let photos = [];

    const keyNames = dataTable ? [] : moduleAttributes.attributes.filter(a=>a.visible_in_popup || a.visible_in_cv_detail || a.visible_in_cv_grid).sort((a,b) => compare(a,b,'weight_in_popup_cv')).map(a=>a.key_name_id);

    for (const row of dataRows) {
        
        const rowInx = row && row.__rowInx !== undefined ? row.__rowInx : row;

        if (rowInx !== undefined && rowInx !== null) {

            let additionalControls = '';
            if (moduleKey === 'tlm_tracks' || moduleKey === 'tlm') {
                additionalControls = tlmTracksAdditionalPopupControls(row);
                if (!row['event_date'] && (row['ed1'] && row['ed2'])) {
                    row['event_date'] = row['ed1'] + ' - ' + row['ed2'];
                    console.log(moduleAttributes)
                }
            }
            
            if (dataTable) { //use dataTable formatted and translated data if dataTable is defined
                const attributesKeyed = convertToAssocArray(moduleAttributes, 'key_name_id');
                const lines = [];

                const rowData = dataTable.getRowCellsFormattedData(rowInx);
                rowData.map(o => {
                    const key = o.key = o.data.startsWith('t_') ? o.data.replace('t_','') : o.data;
                    o.weight_in_popup_cv = attributesKeyed[key].weight_in_popup_cv;
                });

                rowData.sort((a,b) => compare(a,b,'weight_in_popup_cv')).map(o => {
                    if (attributesKeyed[o.key].visible_in_popup && o.data !== '_photos' && o.value) {
                        lines.push(`<b>${o.title}:</b> ${o.value}`);
                    }
                });

                popupContent.push(lines.join('<br>') + additionalControls);
            }
            else {
                let content = mutils.attributeValuesToHtml(row, moduleAttributes.attributesKeyed,'<br>',keyNames, true, moduleAttributes.refValues, ['_photos']);

                popupContent.push(content + additionalControls);
            }

            if (row._photos) {
                photos = [...photos, ...JSON.parse(row._photos)];
            }
        }
    }

    const popup = marker.getPopup();
    popup.setContent(popupContent.join('<hr>'));
    $('.licence-link').on('click', async function () {
        const $this = $(this);
        const id = $this.data('id');
        const res = await mutils.requestHelper(globals.apiRoot + `/mbase2/licence_list_vw/${id}`);

        const title = JSON.parse(res[0].name);
        const description = JSON.parse(res[0].description);

        const modalDialog = new ModalDialog.default(
            {
                onClose: () => {
                 
                },
                onShown: onModalShown
        });
    
        function onModalShown() {
            modalDialog.$title.text(title[globals.language] || title['en'] || res[0].title);
            modalDialog.$body.text(description[globals.language] || description['en']);
        }

        modalDialog.show();
    
    })

    if (moduleKey === 'tlm_tracks' || moduleKey === 'tlm') {
        const $actionElements = $(popup.getElement()).find('.tlm-track-action');

        $actionElements.on('click', function(){
            const $this = $(this);
            const id = $this.data('id');
            const action = $this.data('action');
            actionCallback && actionCallback(id, action, $this);
        })
    }

    if (photos.length > 0) {
        const $div = $(popup.getElement()).find('.leaflet-popup-content:first');
        const cmp = new ImageArray.default({
            mediaRoot: globals.mediaRoot+'/private/'+moduleKey, 
            skipAddButton: true,
            onClick: async () => {
                await ImageViewer.default({
                    data: photos.map(p => ({
                        file_hash:p,
                        properties: '{}'
                    })),
                    mediaRootFolder: globals.mediaRoot+'/private/'+moduleKey
                });
            }
        });
        $div.append(cmp.$el());
        cmp.val(photos);
    }

    return popup;
}