import globals from '../app-globals'

export const ComponentManager = () => import(/* webpackChunkName: "component-manager" */ './ComponentManager');
export const record = () => import ('./record');
export const Select2 = () => import(/* webpackChunkName: "select2" */'./components/Tselect2');
export const TTomSelect = () => import('./components/TTomSelect');
export const Select = () => import(/* webpackChunkName: "select" */'./components/Tselect');
export const mutils = () => import('./mbase2_utils');
export const TranslationHandler = () => import('./TranslationHandler');
export const utils = () => import('./utils');
export const ModalDialogMultipleSelector = () => import('./components/ModalDialogMultipleSelector');
export const IconButton = () => import('./components/IconButton');
export const Accordion = () => import(/* webpackChunkName: "acc" */'./components/Accordion');
export const Inputs = () => import(/* webpackChunkName: "inputs" */'./components/Inputs');
export const Button = () => import(/* webpackChunkName: "btn" */'./components/Button');
export const DataTable = () => import('./components/DataTable');
export const ModalDialog = () => import('./components/ModalDialog');
export const Storage = () => import('./components/Storage');
export const CheckBox = () => import('./components/TCheckBox');
export const Tabs = () => import('./components/Tabs');
export const ImageArray = () => import('./components/ImageArray');
export const DropzoneLoader = () => import('./components/DropzoneLoader');
export const FileUploadButton = () => import('./components/FileUploadButton');
export const Media = () => import('./components/Media');
export const Switch = () => import('./components/Switch');
export const ButtonGroup = () => import('./components/ButtonGroup');
export const DropDown = () => import('./components/DropDown');
export const LocationSelector = () => import('./components/LocationSelector');
export const MediaScroller = () => import('./components/MediaScroller');
export const Map = () => import('./components/Map');
export const Sidebar = () => import('./components/Sidebar');
export const EventsLegend = () => import('./components/EventsLegend');
export const DateRangeInput = () => import('./components/DateRangeInput');
export const Alert = () => import('./components/Alert');
export const InputMask = () => import('./components/InputMask');
export const TreeView = () => import('./components/TreeView');
export const DateInputs = () => import('./components/DateInputs');
export const SimpleTable = () => import('./components/SimpleTable');
export const DataFilter = () => import('./components/DataFilter');
export const DataOrder = () => import('./components/DataOrder');
export const BootstrapToggle = () => import('./components/BootstrapToggle');
export const RadioButtonSelector = () => import('./components/RadioButtonSelector');
export const ImageGallery = () => import('./components/ImageGallery');
export const ImageViewer = () => import('./components/ImageViewer');
export const TimePicker = () => import('./components/TimePicker');
export const TimeStampInput = () => import('./components/TimeStampInput');

export const SelectWithSpecify = () => import('./components/SelectWithSpecify');

export const Mbase2Module = () => import('../modules/Mbase2Module');

export const recordsTable = () => import('../modules/recordsTable');

///////////////////////////////////////Helper functions
/**
* Helper function for Select2 component - Heads up: call utils.loadComponents before use
* 
* @param {object} op
* @param {string} op.label
* @param {function} op.onSelect
* @param {string} [op.url] url of the select data to fetch
* @param {string} [op.tname] used if url is not defined - the tname constructed url is in this case: globals.apiRoot + `/${op.tname}/language/` + globals.language;
* @param {array} [op.queryParameters] array of optional query parameters to be added to tname query ['p1=v1','p2=v2',...]
* @param {function} op.filter filter data fetched from the op.url before processing
* @param {DOM object} op.$parent
*/
export const select = (op, deferredImport = true, component=Select2) => {
    const cdef = {
        options: {
            badge: op.label,
            onSelect: op.onSelect,
            onSelecting: op.onSelecting,
            configurationOverrides: op.configurationOverrides
        },
        data: {
            model: {
                key: 'id'
            },
            preprocess: (data, model) => {
                model.originalData = [...data];
                
                if (op.filter) {
                    data = data.filter(op.filter);
                }
                else if (op.preprocess) {
                    data = op.preprocess(data);
                }

                return data;
            },
            process: (data, model) => {
                
                const fun = op.process || (item => [item[model.key], item.t_id || item.key_id]);
                
                return data.map(fun);
            },
            refresh: (component, data) => {
                component.reinit(data);
            }
        },
        $parent: op.$parent
    };

    if (op.url || op.tname) {
        cdef.data.model.url = op.url || globals.apiRoot + `/${op.tname}/language/` + globals.language + (op.queryParameters ? '?' + op.queryParameters.join('&') : '');
        cdef.data.request = async model => (await utils()).request(model.url, 'GET');
    }
    else if (op.data && op.data.values) {
        cdef.data.values = op.data.values;
    }

    if (deferredImport===true) {
        cdef.module = component;
    }

    return cdef;
}

export const table = (op = {header:[], model:[]}) => {
    
    const cdef = {
        module: DataTable,
        options: {
            scroller: true,
            scrollX: true,
            deferRender:true,
            scrollY:'75vh',
            header: op.header
        },
        data: {
            model: {},
            process: op.processTableData || null,
            refresh: (component, data) => {
                component && component.setData(data);
            }
        },
        beforeComponentCreate: options => {
            const $div = $('<div/>');
            if (op.wrapperClass) $div.addClass(op.wrapperClass);
            op.$parent.append($div);
            options.$container = $div;
        }
    };

    if (op.onEdit) cdef.onEdit = (row, id) => {
        op.onEdit(row, id);
    }

    if (op.url) {
        cdef.data.model.url = op.url;
        cdef.request = async model => (await mutils()).requestHelper(model.url)
    }
    else if (op.data && op.data.values) {
        cdef.data.values = op.data.values;
    }

    return cdef;
};

export const accordion =  op => {
    return {
        module: Accordion,
        options: componentsOptions.accordion(op.panels),
        $parent: op.$parent
    }
};

export const componentsOptions = ({
    accordion: panels => ({
        closeOthers: false,
        panels: panels || []
    })
});

export const button = op => {
    return {
        module: Button,
        options: {
            label: `Save`, 
            style:'float:right'
        }
    }
}