import globals from '../app-globals';

export const getComponent = (name, components) => {
    return components.filter(component => component[name])[0][name];
}

export const deepClone = (obj) => {
    if (!obj) return obj;
    return JSON.parse(JSON.stringify(obj));
}

/**
 * User in array sorting
 * @param {*} a 
 * @param {*} b 
 * @param {string} p property name by which to compare
 * @returns 
 */

export const compare = function ( a, b, p ) {

    const firstItem = a[p] || Number.MAX_SAFE_INTEGER;
    const secondItem = b[p] || Number.MAX_SAFE_INTEGER;

    if ( firstItem < secondItem ){
      return -1;
    }

    if ( firstItem > secondItem ){
      return 1;
    }

    return 0;
}

/**
 * https://developer.mozilla.org/en-US/docs/Glossary/Base64#the_unicode_problem
 * @param {*} base64 
 * @returns 
 */
export const base64ToString = function (base64) {
    if (!base64) return base64;
    const binString = atob(base64);
    return new TextDecoder().decode(Uint8Array.from(binString, (m) => m.codePointAt(0)));
}

/**
 * https://developer.mozilla.org/en-US/docs/Glossary/Base64#the_unicode_problem
 * @param {*} bytes 
 * @returns 
 */
export const stringToBase64 = function (str) {
    if (!str) return str;
    const bytes = new TextEncoder().encode(str);
    const binString = Array.from(bytes, (x) => String.fromCodePoint(x)).join("");
    return btoa(binString);
}

/**
 * Resolves promisses
 * @param {Object} promises Object with key - value pairs, where value is a Promise
 * @returns {Object} a promise of all input promises which - when resolved - returns Object with key - value pairs, where value is resolved input promise
 */
export const resolvePromises = (promises) => {
    return Promise.all( Object.values(promises) ).then( resolved => 
        Object.keys(promises).reduce((obj, key, index) => ({ ...obj, [key]: resolved[index] }), {}) //combine input object keys with resolved results
    );
}

/**
 * Extracts selected item from Select2 callback parameter
 * @param {object} e select2 callback argument
 */
export const select2value = e => {
    if (!e) return e;
    return (e.params && e.params.data) ? e.params.data.id : e;
}

export const select2text = e => {
    if (!e) return e;
    return (e.params && e.params.data) ? e.params.data.text : e;
}

export const patterns = {
    non_zero_integer: '^[1-9][0-9]+',
    any_letter_two_times: '\\p{L}{2}',
    _kadrovska_koda_pattern: '\\p{Lu}{2}[\\d\\p{Lu}]?',
    machine_key: '^[A-Z_][A-Z0-9_]+',
    real_number: '^\\d*\\.?\\d+$',
    email: "[^@]+@[^@]+\\.[a-zA-Z]{2,6}",
    phone: '^[0][0-9]\\s*\\d(\\s*\\d){6,11}'
}

export const hiddenInputs = keys => {
    return keys.map(key => 
        ({
            key: key, 
            required: true,
            validate: false
        })
    );
}

export async function processData(data) {
    
    var values = data;

    if (data.fetch) {
        values = await request(data.fetch);
    }

    if (data.parse) {
        values = await data.parse(values);
    }

    return values;
}

export const upsertArray = (arr, item, key = 'id') => {
    const variableIndex = arr.findIndex(v => v[key] == item[key]);

    if (variableIndex !== -1) {
        arr[variableIndex] = item;
    }
    else {
        arr.push(item);
    }
}

/**
 * https://stackoverflow.com/questions/2090551/parse-query-string-in-javascript
 * 
 * @param {string} variable variable to parse from query string
 */

export const getQueryParameter = (variable) => {

    const params = new URLSearchParams(document.location.search);
    return params.get(variable);
}

export async function getEavData(model, callback, apiRoot='') {

    const values = await request(apiRoot + `/virtual-table-values/${model.table}/language/${model.language}`);

    if (callback) {
        return await callback(values, model);
    }

    return values;
}

export async function getData(model, apiRoot='') {

    let req;

    if (model.language) {
        req = `${model.table}/language/${model.language}`;
    }
    else {
        req = model.table;
    }

    const values = await request(apiRoot + '/' + req);

    return values;
}

export function hrefClick(path, target = '_blank') {
    path = path.replace(/"/g, '&quot;');
    $(`<a href="${path}" ${target ? 'target='+target : ''}></a>`)[0].click();
}

/**
 * Pads input number with 
 * @param {integer | string} num 
 * @param {integer} size size of padded string
 * @param {string} [char = '0']  character to paddwith
 */

export function strLeftPad(num, size, char = '0') {
    num = num.toString();
    if (!size) size = num.length;
    if (!size) return num;
    while (num.length < size) num = '0' + num;
    return num;
}

/**
 * https://www.sitepoint.com/testing-for-empty-values/
 * @param {*} data 
 * @returns true when data is:
 * - undefined or null
 * - a zero-length string
 * - an array with no members
 * - an object with no enumerable properties
 */

export function empty(data)
{
  if(typeof(data) == 'number' || typeof(data) == 'boolean')
  { 
    return false; 
  }
  if(typeof(data) == 'undefined' || data === null)
  {
    return true; 
  }
  if(typeof(data.length) != 'undefined')
  {
    return data.length == 0;
  }
  var count = 0;
  for(var i in data)
  {
    if(data.hasOwnProperty(i))
    {
      count ++;
    }
  }
  return count == 0;
}

/**
 * Adds components object keys to object values
 * if object value is function it gets executed with a key as argument
 * 
 * @param {object} components 
 * @param {string} preposition
 */

export const addKeysToComponents = (components, preposition = '') => {
    preposition = preposition && (preposition + '_'); //add an underscore if preposition is not empty
     
    Object.keys(components).map(key => {

        if (typeof components[key] === "function") {

            var proxied = components[key];
            components[key] = function() {
                const obj = proxied.apply( null, arguments );
                obj.key = preposition + key;
                return obj;
            };

        }
        else {
            components[key].key = preposition + key;
        }

    });  //add keys to components
}

/**
 * Adds a "key" property to the components defined in the first input argument and loads the components
 * 
 * @param {object} components 
 * @param {string} preposition preposition to be appended to a component key
 */

export const loadComponents = (components, preposition = '') => {
    addKeysToComponents(components, preposition);
    return Promise.all(Object.keys(components).map(async key => {
        const component = components[key];
        if (component.module && typeof component.module === "function"){
            const impKey = component.import || 'default';
            component.component = (await component.module())[impKey];
        }
    }));
}

export const loadComponent = async (key, component) => {
    if (component.module && typeof component.module === "function"){
        const impKey = component.import || 'default';
        component.component = (await component.module())[impKey];
    }
    component.key = key;
    return component;
}


/**
 * Get the number of days in any particular month
 * @link https://stackoverflow.com/a/1433119/1293256
 * @param  {integer} m The month (valid: 0-11)
 * @param  {integer} y The year
 * @return {integer}   The number of days in the month
 */
export const daysInMonth = function (m, y) {
    switch (m) {
        case 1 :
            return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
        case 8 : case 3 : case 5 : case 10 :
            return 30;
        default :
            return 31
    }
};

/**
 * Check if a date is valid
 * @link https://stackoverflow.com/a/1433119/1293256
 * @param  {[type]}  d The day
 * @param  {[type]}  m The month
 * @param  {[type]}  y The year
 * @return {Boolean}   Returns true if valid
 */
export const isValidDate = function (d, m, y) {
    m = parseInt(m, 10) - 1;
    return m >= 0 && m < 12 && d > 0 && d <= daysInMonth(m, y);
};

/**
 * Helper function: if array is passed it gets converted to object
 * @param {array|object} a 
 * @param {string} key 
 */
export const convertToAssocArray = (a, key='id') => {
    if (a && Array.isArray(a)) {
        return arrayToObject(a, key);
    }
    return a;
}

export const createBreadcrumbs = function(breadcrumbs) {
    const $ol = $('<ol class="breadcrumb" style="padding-bottom:0px"></ol>');
    for (let i=0; i<breadcrumbs.length; i++) {
        const bc = breadcrumbs[i];
        if (bc.class === 'active') {
            $ol.append(`<li class = "active"><strong>${bc.text}</strong></li>`);
        }
        else {
            $ol.append(`<li><a href="${bc.href}">${bc.text}</a></li>`);
        }
    }
    return $ol;
}

/**
 * Returns object form JSON string or object if argument is already an objevt
 * 
 * @param {string|object} json 
 */
export const jsonParse = json => {
    if (typeof json !== 'object') {
        return JSON.parse(json);
    }

    return json;
}

export const trimSlash = url => url.replace(/^\/+|\/+$/g, '');

export const rtrimSlash = url => url.replace(/\/+$/g, '');

/**
 * A helper function to append key, value pairs to $el
 * @param {object} obj object of key values to putpur
 * @param {jquery DOM object} $el DOM element for the output
 * @param {object} keys iterate over keys provided in this parameter instead of all keys of the obj parameter
 * @param {array} keys.keys keys for the output
 * @param {object} keys.keyMap associative array obj.key 
 */
export const keyValueOutput = (obj, $el, keys = null) => {
    const output = [];

    if (keys) {
        keys.keys.map(key => {
            const label = keys.keyMap[key];
            obj[label] && output.push(`<b>${label}:</b> ${obj[label]}`);    
        })
    }
    else {
        Object.keys(obj).map(label => {
            output.push(`<b>${label}:</b> ${obj[label]}`);
        });
    }

    $el.html(output.join('<br>'));
}

/**
 * Appends float left and float right container to $parent
 * @param {jQuery DOM object} $parent 
 */
export const createFloatContainersSideBySide = $parent => {
    const $left =  $('<div style="float:  left;  width:49%;  overflow: auto"/>');
    const $right = $('<div style="float: right;  width:49%;  overflow: auto;"/>');
    $parent.append($left);
    $parent.append($right);
    $parent.append('<br style="clear:both">');

    return {
        $left: $left,
        $right: $right
    }
}


const fetchOptions = (op = {}) => Object.assign({
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        headers: { 'Content-Type': 'application/json', 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        //credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer' // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    },op);

/**
 * Fetch API helper function with loading overlay option
 * 
 * @param {string} url 
 * @param {string} method request method in upercase 'GET', 'POST', 'PUT', 'DELETE', etc.
 * @param {object} data data to be sent with request (except for GET method, where the data should be put in the request's url)
 * @param {object} callbacks callback functions
 * @param {function} callbacks.loadingOverlay function with one argument, before the request it is invoked (if defined) with this argument set to true and after the request to false
 * @param {function} callbacks.onSuccess if defined called where response.ok === true
 * @param {function} callbacks.onError if defined called where response.ok === false
 * @param {object} _op fetchOptions overrides
 */

export const request = async (url='', method = 'GET', data = null, callbacks = {}, _op = {}) => {
    
    const op = Object.assign({method: method}, _op);

    const loadingOverlayFunction = callbacks.loadingOverlay;

    const onSuccess = callbacks.onSuccess;

    const onError = callbacks.onError;

    if (data) {
        if (data instanceof File === true) {

            const formData = new FormData()
            formData.append('file', data, data.name);

            op.body = formData;
            op.headers = {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') };
        }
        else {
            op.body = JSON.stringify(data);
        }
    } 
    
    if (loadingOverlayFunction) loadingOverlayFunction(true);
    
    const response = await fetch(url, fetchOptions(op)).catch(e => {
        if (onError) onError({}, e);
    });
    
    if (loadingOverlayFunction) loadingOverlayFunction(false);

    if (response) {
        let parseResponse = 'text';
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.includes('application/json')) {
            parseResponse = 'json';
        }
     
        const body = await response[parseResponse](); // parses JSON response into native JavaScript objects

        if (response.ok === true && onSuccess) onSuccess(response, body);
        if (response.ok === false && onError) onError(response, body);

        return response.ok === true ? body : false;
    }
    
    return false;
}

/**
 * Convert array of objects to associative object
 * @param {array<object>} arr 
 * @param {string} key the key in objects of input array to be used as associative key in the result
 */

export const arrayToObject = (arr, key) => {
    if (!arr) return {};
    return arr.reduce((result, current) => {
        result[current[key]] = current;
        return result;
    }, {});
}

export const lastItem = arr => {
    if (Array.isArray(arr) && arr.length>0) {
        return arr[arr.length-1];
    }
}

export const customMarkerIcon = (color = '#583470') => {
    //https://stackoverflow.com/questions/23567203/leaflet-changing-marker-color
    const markerHtmlStyles = `
        background-color: ${color};
        width: 3rem;
        height: 3rem;
        display: block;
        left: -1.5rem;
        top: -1.5rem;
        position: relative;
        border-radius: 3rem 3rem 0;
        transform: rotate(45deg);
        border: 1px solid #FFFFFF`

    const icon = L.divIcon({
        className: "my-custom-pin",
        iconAnchor: [0, 24],
        labelAnchor: [-6, 0],
        popupAnchor: [0, -36],
        html: `<span style="${markerHtmlStyles}" />`
    });

    return icon;
}

export const createRightAlignedButton = $btnParent => {
    const $btnDiv = $('<div/>', {style: 'display: flex; float: right; margin-top:20px'});

    $btnDiv.append($btnParent);

    $btnDiv.append('<div style="clear: both;"></div>');

    return $btnDiv;
}

export const t = globals.t;
