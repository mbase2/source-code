import * as exports from './exports';

/**
 * Translation form
 * 
 * @param {object} op
 * @param {object} op.$parent a jQuery $container for the form
 * @param {object} op.cm ComponentManager object instance
 * @param {array|object} op.refValues values referenced by the attributes that are shown in select element; if array is passed as argument it gets converted to associative object of arrays based on 'id' property
 */

export default async op => {    

    const [
        utils, 
        mutils,
        ComponentManager,
        Button
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        exports.ComponentManager(),
        exports.Button()
    ]);

    const t = utils.t;

    const attributes = op.cm.model.attributes = utils.convertToAssocArray(op.cm.model.attributes, 'key_name_id');

    const {componentDefinitions, definitionAliases} = (await import('./components/componentDefinitions'));
    const importDefinitions = componentDefinitions(exports);
    const aliases = definitionAliases(utils.patterns);
    
    const {$parent} = op;

    /** if container is modal window it can have $body, $title and $footer parts */
    const $body = $parent.$body || $parent;
    const $title = $parent.$title;
    const $footer = $parent.$footer;

    const cm = op.cm || new ComponentManager.default();
    
    for (const key of Object.keys(attributes)) {
        const v = attributes[key];
        if (v.visible===false) continue;
        const cdef = await mutils.getComponentDefinition(v, importDefinitions, aliases, op.refValues, true, $body);
        cdef.key = key;
        cdef.$parent = $body;
        const cmp = await cm.add(cdef);
        if (v._component && v._component.onComponentAdded) v._component.onComponentAdded(cmp);
    };

    await cm.add({
        key: 'btn_save',
        component: Button.default,
        options: {
            label: t`Save`, 
            classes: 'btn-primary btn-lg',
            style:'float:right',
            onClick: op.onSave ? () => op.onSave(cm) : onSave //create new button for every module change otherwise onSave holds the initial options
        },
        $parent: $footer || $body
    }, true);

    let add;
    if (cm.model.values && cm.model.values[cm.model.primaryKey]) {
        $title && (op.title ? $title.html(op.title) : $title.text(t`Edit record`));
        cm.updateComponentsValue();
        add = false;
    }
    else {
        add = true;
        $title && (op.title ? $title.html(op.title) : $title.text(t`New record`));
        initComponentsSelectedValues();
    }

    if (op.onInit) {
        op.onInit(cm);
    }

    cm.saveOptions.onSuccessCallbacks = [onSuccessfullySaved];
    if (op.saveCallbacks) {
        cm.saveOptions.onSuccessCallbacks = [...cm.saveOptions.onSuccessCallbacks, ...op.saveCallbacks];
    }

    if (op.saveOptions && op.saveOptions.onSuccessCallbacks) {
        cm.saveOptions.onSuccessCallbacks = [...cm.saveOptions.onSuccessCallbacks, ...op.saveOptions.onSuccessCallbacks];
    }

    const saveOptions = Object.assign(await mutils.saveButtonHelper(op.saveOptions), op.saveOptions || {});

    function initComponentsSelectedValues() {
        if (cm.model.select) {
            cm.model.select.map(s => {
                const c = cm.get(s.key);
                c && c.val(s.value);
            });
        }

        if (cm.model.values) {
            Object.keys(cm.model.values).map(key => {
                const c = cm.get(key);
                c && c.val(cm.model.values[key]);
            })
        }
    }

    function onSuccessfullySaved(cm, result, model) {
        op.onSuccessfullySaved && op.onSuccessfullySaved(cm, result, model, add);
        
        if (add === true) {
            cm.val(null);
            cm.model.values = null;
            initComponentsSelectedValues();
        }
    }
    
    function onSave() {
        op.beforeSave && op.beforeSave(cm);
        cm.save(saveOptions);
    }

    function onDataFileSelect(e) {
        if (!fileUploadCm) return;
        console.log(fileUploadCm.get('select').val())
    }
}