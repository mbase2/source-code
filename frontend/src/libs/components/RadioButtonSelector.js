function RadioButtonSelector(op={}) {
    this.op = op;
    const id = ('radio_id-'+Math.random()).replace('0.','');
    const $el = $('<div/>');

    this.$inputs = {};

    this.selectedKey = null;

    const self = this;

    op.items.map(item => {

        const $item = $(`<div class="radio">
                    <label><input type="radio" name="${id}">${item.label}</label>
                  </div>`);
        
        const $input = $item.find('input:first');
        
        this.$inputs[item.key] = $input;
        
        if (op.onChange) {
            $input.on('change', function() {
                self.selectedKey = item.key;
                op.onChange(item, $(this));
            });
        }
        $el.append($item);
    });

    this.$el = () => $el;
}

RadioButtonSelector.prototype.val = function(key, silent = true) {
    if (key!==undefined) {
        const $input = this.$inputs[key];
        $input.prop('checked', true);
        silent===false && $input.trigger('change');
        this.selectedKey = key;
        return;
    }

    return this.selectedKey;
}

export default RadioButtonSelector;