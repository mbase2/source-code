
const FileUploadButton = function(op = {}) {
    const $input = this.$input = $("<input accept='.xlsx,.csv' style='display:none' type='file' />");
    
    const $el = $(`
    <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-upload"></i>
        <span>${op.label || 'Upload'}</span>
    </span>
    `);

    $el.on('click', () => this.$input.trigger('click'));

    $input.on('change', () => {
		const files = $input[0].files;

		for (var i = 0; i < files.length; i++) {
            const file = files[i];
            if (op.onChange) op.onChange(file);
		}
		
	});

    this.$el = () => $el;
}

export default FileUploadButton;