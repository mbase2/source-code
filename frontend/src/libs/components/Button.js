/**
 * 
 * @param {<object>} op 
 * @param {string} op.type type of button defaults to btn-default
 * @param {string} op.classes additional classes to use, defaults to empty
 * @param {string} op.style DOM element style
 */
const Button = function (op) {
    const type = op.type ? op.type : 'btn-default';
    const classes = op.classes ? op.classes : '';
    const style = op.style ? op.style : '';

    const $el = $('<div/>');

    const $btn = $(`<button style="${style}" type="button" class="btn ${classes} ${type}"></button>`);

    if (op.iconClass) {
        const $icon=$('<i/>',{class:`fa fa-${op.iconClass}`,'aria-hidden':'true'});
        $btn.append($icon);
    }

    if (op.label) $btn.append((op.iconClass ? "&nbsp;" : "") +op.label);
    
    $el.append($btn);
    
    this.$el = () => $el;

    this.setDisabled = disabled => {
        $btn.prop('disabled', disabled);
    };

    if (op.onClick) {
        $btn.click(op.onClick);
    }
}

export default Button;