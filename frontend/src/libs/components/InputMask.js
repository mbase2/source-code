//import Inputmask from "inputmask";
import IMask from 'imask';

export const InputMask = function (op = {}) {
    const $el = op.$input || $('<input>');
    
    const maskOptions = op.maskOptions || {};

      /*
      $("#my_input").bind("keydown click focus", function() {
        console.log(cursor_changed(this));
    });
    */

    var mask = this.mask = IMask($el[0], maskOptions);
    
    op.onAccept && mask.on("accept", () => op.onAccept(mask, $el));
    op.onComplete && mask.on("complete", () => op.onComplete(mask, $el));

    this.$el = () => $el;
}

export default InputMask;