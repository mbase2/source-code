import globals from '../../app-globals';

const _imports = {};
let exports = null;

export const loadExports = async _exports => {
    exports = _exports;
    [
        _imports.Button,
        _imports.Select2,
        _imports.utils,
    ] = await Promise.all([
        exports.Button(),
        exports.Select2(),
        exports.utils()
    ]);
}

/**
 * 
 * @param {object} op options
 * @param {array<object>} op.attributesDefinition
 */
function DataOrder(op = {}) {
    const $el = $('<div/>',{style: 'display: flex'});
    this.$el = () => $el;

    const items = op.attributesDefinition.map(a => ({
        text: a.t_name_id || a.key_name_id,
        value: a.key_name_id
    }));

    const order = new _imports.Select2.default({
        multiple: true,
        data: items,
        selectionSort: false,
        placeholder: _imports.utils.t`Order by`
    });

    $el.append(order.$el().css('min-width','200px'));
}

DataOrder.prototype.val = function(rows) {
    
}  

export default DataOrder;