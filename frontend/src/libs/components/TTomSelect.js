import * as TomSelect from 'tom-select';
import 'tom-select/dist/css/tom-select.default.min.css'
import { helpIconHtml, findItem } from '../mbase2_utils';

const TTomSelect = function (op){
	var data = op.data || [];
	const $el = $('<div/>');
	this.$el = () => $el;

	this.additionalData = {};

	const badge = op.badge || op.label;
	const required = op.required ? 'required' : '';

	let $badge;
	
	if (badge){
		$badge = $(`<span class="label label-primary mbase2-label ${required}">`+badge+'</span>');
		$el.append($badge);
		op.helpIconText && $el.append(helpIconHtml(op.helpIconText));
	}

	if (data[0] && Array.isArray(data[0])) {
		data = data.map(d => {
			if (d[2]) {
				this.additionalData[d[0]] = d[2];
			}
			return {value: d[0], text: d[1]};
		});
	}
	else if (data[0] && typeof data[0] === 'object' && data[0].text !== undefined) {
		;
	}
	else {
		data = data.map((d,inx) => ({value: inx, text: d}));
	}

	this.data = data;
	
	const $select = $('<select/>');
	
	$el.append($select);

	this.tselect = new TomSelect($select[0],Object.assign({
		options:data,
		maxOptions: null,
		plugins: ['no_backspace_delete','dropdown_input',...(op.additionalPlugins || [])],
		maxItems: op.multiple === true ? null : 1
	}, op.configurationOverrides || {}));

	this.tselect.on('change', value => {
		if (!value) return;
		op.onSelect && op.onSelect(value, this.added);
		this.onSelectHook && this.onSelectHook(value, this.added);
		this.added = false;
	})

	this.tselect.on('option_add', (value, data) => {
		this.added = {value, data};
	});

	this.tselect.on('item_remove', (value, $item) => op.onUnselect && op.onUnselect(value, $item));

	if (op.read_only === true) {
		this.tselect.disable();
	}
}

TTomSelect.prototype.getSelectedItemsData = function () {
	const values = this.text();

	return this.val().map((id, inx) => [id, values[inx]]);
}

TTomSelect.prototype.val = function (a,silent=true) {
	if (a !== undefined) {
		this.tselect.setValue(a, silent);
	}

	return this.tselect.getValue();
}

TTomSelect.prototype.text = function(text, silent=true) {
	if (text !== undefined) {
		let value = null;
		if (Array.isArray(text)) {
			value = text.map(t => findItem(this.data,'text',t,'value'));
		}
		else {
			value = findItem(this.data,'text',text,'value');
		}

		this.val(value);

		return value;
	}

	const value = this.tselect.getValue();
	
	if (!value) return null;

	if (Array.isArray(value)) {
		return value.map(v => findItem(this.data,'value',v,'text'));
	}
	else {
		return findItem(this.data,'value',value,'text');
	}
}

TTomSelect.prototype.getAdditionalData = function(value) {
	return this.additionalData[value];
}

TTomSelect.prototype.getAdditionalDataForSelectedOption = function () {
	const value = this.val();
	if (!value) return null;
	return this.getAdditionalData(value);
}

TTomSelect.prototype.reinit=function(data,textf){
	this.tselect.clearOptions();
	if (data.length===0) return false;
	
	if (textf===undefined) textf='text';
	var valf='id';
	if (data[0][valf]===undefined) valf=textf;
	
	const options = [];

	if (data[0][valf] && data[0][textf]){
		$.each(data,function(key,op){
			options.push({value: op[valf], text: op[textf]});
		});
	}
	else if (Array.isArray(data[0])) {
		data.map(d => {
			options.push({value: d[0], text: d[1]});
		});
	}
	else{
		$.each(data,function(key,op){
			options.push({value: op, text: op});
		});
	}
	
	this.tselect.addOptions(options);
	this.tselect.refreshOptions(false);
	

	return true;
};

export default TTomSelect;