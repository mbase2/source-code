/**
 * 
 * @param {Object} op 
 * @param {string} op.label button label
 * @param {function} op.onClick onClick function
 */
export default function (op) {
    const $btn = $(`<a href="#" class="card-pf-link-with-icon">
        <span class="pficon pficon-add-circle-o"></span>${op.label}
    </a>`);
    
    if (op.onClick) {
        $btn.click(op.onClick);
    }

    this.$el = () => $btn;
}
    
    