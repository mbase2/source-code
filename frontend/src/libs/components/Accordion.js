const $panelGroup = id => $(`<div class="panel-group" id="${id}"></div>`);

const $panel = (parentId, title, index) =>
 $(`<div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#${parentId}" href="#${parentId + '-' + index}">
          ${title}
        </a>
      </h4>
    </div>
    <div id="${parentId + '-' + index}" class="panel-collapse collapse in">
      <div class="panel-body">
         
      </div>
    </div>
  </div>`);

/**
 * @param {object} op options
 * @param {string} op.id component id
 * @param {boolean} op.closeOthers 
 * @param {array<string>} op.panels
 * @param {boolean} op.headingPointer=true mouse pointer over whole header
 */

export default function Accordion (op={}) {
  this.id = op.id || ('aid-'+Math.random()).replace('0.','');

  this.onPanelRemove = op.onPanelRemove;

  this.op = op;

  op.headingPointer = op.headingPointer === undefined ? true : op.headingPointer;

  const $el = $panelGroup(op.closeOthers === false ? "" : this.id);
  this.$el = () => $el;
  if (!op.panels) op.panels = [];

  this.panels = [];

  op.panels.map((title, index) => {
    if (typeof title === 'string' || title instanceof String) {
      this.addPanel(title, index);
    }
    else {
      const obj = title;
      this.addPanel(obj.label, index, obj.key, obj.content);
    }
  });

  if (op.headingPointer) {
    $el.find('.panel-heading').css('cursor','pointer').on('click', function() {
      $(this).parent().find('.panel-collapse:first').collapse('toggle');
      return false;
    });
  }

  this.$panelGroup = $el;

  if (op.$parent) {
    op.$parent.append(this.$el());
  }
}

Accordion.prototype.addPanel = function(title, index, key=null, content=null) {
  if (index === undefined) {
    index = this.panels.length;
  }

  let $pdiv = $panel(this.id, title, index);

  if (this.op.onPanelCreated) {
    $pdiv = this.op.onPanelCreated(key, title, $pdiv);
  }

  this.$el().append($pdiv);
  const panel = {
      title: title,
      key: key || ('panel-'+index),
      $panel: $pdiv,
      $body: $pdiv.find('.panel-body')
  };
  this.panels.push(panel);

  content && panel.$body.html(content);

  if (this.onPanelRemove) {
    const $btn = $('<button style="margin-top:-0.9em" type="button" class="close" aria-hidden="true"><span class="pficon pficon-delete"></span></button>');
    $pdiv.find('.panel-heading:first').append($btn);
    $btn.on('click', () => this.onPanelRemove($pdiv, this.$el()));
  }

  return panel;

}

Accordion.prototype.getPanel = function(title) {
  return this.panels.find(p => p.title === title);
}

