
import './css/sidebar-mbase2.css';

const sidebarHtml = `<div class="sidebar collapsed">
<!-- Nav tabs -->
<div class="sidebar-tabs">
    <ul role="tablist">
  <li><a id="sb-expand" href="#home" role="tab"><i class="fa fa-caret-right"></i></a></li>
    </ul>
</div>

<!-- Tab panes -->
<div class="sidebar-content">
    <div class="sidebar-pane" id="home">
  <h2><%=title%></h2>
    <div class="sidebar-close"><i class="fa fa-caret-left"></i></div>
    <div class="container-fluid"><hr></div>
    </div>
</div>
</div>`;

var Sidebar=function(op){
  this._op=op;
  this._$el=$(sidebarHtml);
  this._$el.find('#home h2:first').html(op.title);
  this._$el.attr('id',op.id);
  op.$container.append(this._$el);
  //this.setPosition();
};
  
Sidebar.prototype.setPosition=function(){
  var nav=$('nav:first');
  var h=0;
  h=parseInt(nav.css('height'))+15;
  
  if ($('#toolbar').length!==0){
    h+=parseInt($('#toolbar').css('height'));
  }
  
  this._$el.css('top',h);
};

Sidebar.prototype.$el=function(){
  return this._$el;
};
  
export default Sidebar;