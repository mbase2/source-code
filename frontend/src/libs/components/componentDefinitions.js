import globals from '../../app-globals';
import { parseHelpIconText, parseTranslation } from '../mbase2_utils';

const variableAttributesToComponent = v => (
    {
        label: v.t_name_id || parseTranslation(v),
        required: v.required,
        pattern: v.key_pattern_id,
        read_only: v.read_only,
        helpIconText: parseHelpIconText(v, globals.language)
    }
);

export const componentDefinitions = exports => ({
    text: {
        import: () => exports.Inputs(),
        componentDefinition: v => ({
            component: 'Input', //this is needed if component is not default
            options: variableAttributesToComponent(v)//component options
        })
    },

    timestamp: {
        import: () => exports.TimeStampInput(),
        imports: async () => {    //this is used to preload the component assets
            const [
                DateInputs,
                TimePicker
            ] = await Promise.all([
                exports.DateInputs(),
                exports.TimePicker()
            ]);

            return {
                DateInput: DateInputs.DateInput,
                TimePicker: TimePicker.default
            };
        },
        componentDefinition: (v, _op, imports) => {
            const rval = {
                component: 'TimeStampInput',
                options: variableAttributesToComponent(v)
            };

            if (imports) {
                rval.options = Object.assign(rval.options, {imports: imports});
            }
            
            return rval;
        }
    },

    date: {
        import: () => exports.DateInputs(),
        componentDefinition: v => ({
            component: 'DateInput',
            options: variableAttributesToComponent(v)
        })
    },

    image: {
        import: () => exports.Media(),
        componentDefinition: v => ({
            options: Object.assign(variableAttributesToComponent(v), {type: 'img'}) //component options
        })
    },

    image_array: {
        import: () => exports.ImageArray(),
        componentDefinition: (v, _op={}) => ({
            options: {
                mediaRoot: globals.mediaRoot,
                fileUpload: file => {
                    return exports.mutils().then(mutils => {
                        return mutils.requestHelper(globals.apiRoot + _op.fileUploadPath || '/file-upload/public', 'POST', file).then(res => res.file_hash);
                    });
                }
            }
        })
    },

    boolean: {
        import: () => exports.CheckBox(),
        componentDefinition: v => ({
            options: variableAttributesToComponent(v)//component options
        })
    },

    json: {
        import: () => exports.Inputs(),
        componentDefinition: (v, _op={}) => ({
            options: Object.assign(variableAttributesToComponent(v), {inputs: _op.attributes}, {accordion: _op.accordion})
        })
    },

    location_reference: {
        import: () => exports.LocationSelector(),
        imports: async () => {    //this is used to preload the component assets

            const [
                utils,
                Button,
                Select2,
                mutils
            ] = await Promise.all([
                exports.utils(),
                exports.Button(),
                exports.Select2(),
                exports.mutils()
            ]);

            return {
                t: utils.t,
                Button: Button,
                Select2: Select2,
                mutils: mutils
            };
        },
        componentDefinition: (v, _op={}, imports=null, $container=null) => {
            const rval = {
                options: variableAttributesToComponent(v)
            };

            if (imports) {
                rval.options = Object.assign(rval.options, {imports: imports});
            }

            if ($container) {
                rval.options.$parent = $container;
            }
            
            return rval;
        }
    },

    code_list_reference: {
        import: () => exports.TTomSelect(),
        /**
         * @param {object} v variable row from module_variables
         * @param {object} _data
         * @param {array<object> | array} _data.translations
         * @param {object} _data.references codeListValues or tableReferences
         */
        componentDefinition: (v, _op={}) => ({
            data: {
                values: (_op.data && _op.data.values) || [],
                preprocess: data => {
                    let codeList = Array.isArray(data) ? data : Object.values(data);
                    codeList = codeList.filter(d => d.list_id == v.ref);
                    if (v._component && v._component.additionalPreprocessDataFilter) {
                        codeList = codeList.filter(v._component.additionalPreprocessDataFilter);
                    }
                    return codeList;
                },
                process: data => {
                    return data.map(c => {
                        let value = c.translations;
                        value = value && value[globals.language];
                        value = value || c.key;
                        return [c.id, value, c.key];
                    });
                            
                }
            },
            options: variableAttributesToComponent(v)//component options
        })
    }
});

export const definitionAliases = patterns => ({
    real: {
        base: 'text',
        op: {
            type: 'number',
            pattern: patterns.real_number,
            step: 'any'
        }
    },

    email: {
        base: 'text',
        op: {
            type: 'text',
            pattern: patterns.email
        }
    },

    password: {
        base: 'text',
        op: {
            type: 'password'
        }
    },

    phone: {
        base: 'text',
        op: {
            type: 'text',
            pattern: patterns.phone
        }
    },

    timestamp: {

    },

    time: {
        base: 'text',
        op: {
            type: 'time',
            data: '00:00:00'
        }
    },
    
    integer: {
        base: 'text',
        op: {
            type: 'number'
        }
    },
    reference: {
        base: 'code_list_reference'
    },

    table_reference_array: {
        base: 'code_list_reference',
        op: {
            multiple:true
        }
    },

    code_list_reference_array: {
        base: 'code_list_reference',
        op: {
            multiple:true
        }
    },

    table_reference: {
        base: 'code_list_reference'
    },

    location_data_json: {
        base: 'location_reference'
    },
    
    location_geometry: {
        base: 'location_reference'
    },

    jsonb: {
        base: 'json'
    }
});