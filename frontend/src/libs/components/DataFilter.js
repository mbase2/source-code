import globals from '../../app-globals';
import { deepClone } from '../utils';

const _imports = {};
let exports = null;

const t = globals.t;

export const loadExports = async _exports => {
    exports = _exports;
    [
        _imports.Button,
        _imports.ModalDialog,
        _imports.ComponentManager,
        _imports.DropDown,
        _imports.Select2,
        _imports.utils,
        _imports.mutils,
        _imports.SimpleTable,
        _imports.record,
        _imports.Inputs,
        _imports.DateInputs,
        _imports.TimePicker
    ] = await Promise.all([
        exports.Button(),
        exports.ModalDialog(),
        exports.ComponentManager(),
        exports.DropDown(),
        exports.Select2(),
        exports.utils(),
        exports.mutils(),
        exports.SimpleTable(),
        exports.record(),
        exports.Inputs(),
        exports.DateInputs(),
        exports.TimePicker()
    ]);
}

function conditionFieldManager(fieldDefinition, conditionCmp) {

    conditionCmp && conditionCmp.val(null);

    if (['code_list_reference', 'table_reference'].indexOf(fieldDefinition.dataType) !==-1) {
        conditionCmp.reinit([
            '=', '<>'
        ]);
    }
    else if (['code_list_reference_array', 'table_reference_array'].indexOf(fieldDefinition.dataType) !==-1) {
        conditionCmp.reinit([
            ['@>','=']
        ]);
    }
    else if (['short','integer','smallint','real','float','date', 'timestamp', 'time'].indexOf(fieldDefinition.dataType) !==-1) {
        conditionCmp.reinit([
            '>', '<', '=', '<>'
        ]);
    }
    else if (['varchar', 'text'].indexOf(fieldDefinition.dataType) !==-1) {
        conditionCmp.reinit([
            '=', 'LIKE'
        ]);
    }
    else {

    }
}

async function valueFieldManager(self, fieldDefinition, cm, $parent) {
    const t = _imports.utils.t;

    let valueCmp = cm.get('value');

    if (['code_list_reference', 'table_reference', 'code_list_reference_array', 'table_reference_array'].indexOf(fieldDefinition.dataType) !==-1) {
        const filtered = self.op.refValues[fieldDefinition.dataType.startsWith('code_list_reference') ? 'codeListValues' : 'tableReferences']
            .filter(clv => clv.list_id == fieldDefinition.ref);
        const values = filtered.map(v => {
            let label = (v.translations && v.translations[globals.language]) || v.key;

            if (self.op.additionalReferenceTableValues === true && v.additional !== undefined) {
                label = label + ' (' + v.additional + ')';
            } 
            
            return [v.id, label];
        });
        
        valueCmp = cm.add({
            key: 'value',
            component: _imports.Select2.default,
            options: {
                label: t`Value`,
                data: values,
                multiple: true
            },
            $parent: $parent
        }, true);
    }
    else if (['short','integer','smallint','real','float'].indexOf(fieldDefinition.dataType) !==-1) {
        valueCmp = cm.add({
            key: 'value',
            component: _imports.Inputs.Input,
            options: {
                label: t`Value`,
                type: 'number',
                step: 1e-6
            },
            $parent: $parent
        }, true);
    }
    else if (['varchar', 'text'].indexOf(fieldDefinition.dataType) !==-1) {
        valueCmp = cm.add({
            key: 'value',
            component: _imports.Inputs.Input,
            options: {
                label: t`Value`
            },
            $parent: $parent
        }, true);
    }
    else if (['date', 'timestamp'].indexOf(fieldDefinition.dataType) !==-1) {
        valueCmp = cm.add({
            key: 'value',
            component: _imports.DateInputs.DateInput,
            $parent: $parent,
            options:{
                format:'yyyy-mm-dd',
                label: t`Value`
            }
        }, true);
    }
    else if (['time'].indexOf(fieldDefinition.dataType) !==-1) {
        valueCmp = cm.add({
            key: 'value',
            component: _imports.TimePicker.default,
            $parent: $parent,
            options:{
                label: t`Value`
            }
        }, true);
    }

    return valueCmp;

}

/**
 * 
 * @param {object} op options
 * @param {array<object>} op.attributesDefinition
 * @param {object} op.apply apply button properties
 * @param {function} op.onClose
 */
function DataFilter(op = {}) {
    
    this.op = op;
    this.cm = new _imports.ComponentManager.default();
    const $el = $('<div/>');
    this.$el = () => $el;

    const t = _imports.utils.t;

    const orderByColumns = [];  //TODO: orderBy

    this.fields = {};

    this.rawValue = [];

    this.previousValue = [];

    this.op.attributesDefinition.map(a => {
        const key = a.key_name_id;
        const label = a.t_name_id || a.key_name_id;

        ['integer', 'smallint', 'short', 'text', 'date', 'timestamp'].indexOf(a.key_data_type_id) !== -1 && orderByColumns.push([key,label]);

        this.fields[a.key_name_id] = {
            label: label,
            dataType: a.key_data_type_id,
            key_data_type_id: a.key_data_type_id,
            ref: a.ref
        };

    });

    if (op.$filter) {
        op.$filter.on('click', () => this.showFilterModal());
    }
    else {
        const filterBtn = new _imports.Button.default({
            label: op.iconOnly === true ? null : `Data filter`,
            iconClass: 'filter',
            classes: 'btn-lg',
            onClick: () => this.showFilterModal()
        });
        
        $el.append(filterBtn.$el());
    }

    const cm1 = new _imports.ComponentManager.default();
    let valueCmp = null;
    let conditionCmp = null;

    const $valueContainer = $('<div/>');

    const self = this;

    const codeListValuesById = {};
    const tableReferencesById = {};

    self.op.refValues.codeListValues.map(item => codeListValuesById[item.id] = item);
    self.op.refValues.tableReferences.map(item => tableReferencesById[item._id_list_id] = item);

    const tableCmp = this.tableCmp = new _imports.SimpleTable.default({
        onRowRemoved:(row, inx) => {
            self.rawValue.splice(inx, 1);
            if (self.rawValue.length === 0) {
                self.disableApply(true);
            }
        },
        attributes: [
            {
                t_name_id:t`Operator`
            },
            {
                t_name_id:t`Field`
            },
            {
                t_name_id:t`Condition`
            },
            {
                t_name_id:t`Value`
            }
        ],
        language: globals.language,
        codeListValues: codeListValuesById,
        tableReferences: tableReferencesById
    });

    tableCmp.$el().on('click', '.operator', function() {
        const $this = $(this);
        const oldText = $this.text();
        const op = $this.data('op');

        if (op == 'AND') {
            $this.data('op', 'OR');
            $this.text(t`OR`);
        }
        else {
            $this.data('op', 'AND');
            $this.text(t`AND`);
        }

        /**
         * Get rowIndex and modify tableCmp.data
         */
        const $row = $this.parent().parent();
        const rowIndex = $row[0].rowIndex;
        tableCmp.data[rowIndex-1][0] = this.outerHTML;
    });

    this.modal = new _imports.ModalDialog.default({
        onClose: () => {
            this.previousValue && this.val(this.previousValue);
            this.op.onClose && this.op.onClose();
        }
    });

    const fieldCmp = new _imports.Select2.default({
        label: t`Field`,
        data: this.op.attributesDefinition.map(a => [a.key_name_id, this.fields[a.key_name_id].label]),
        onSelect: async e => {
            if (!e) return;
            const selectedField = fieldCmp.val();
            const fieldDefinition = this.fields[selectedField];
            valueCmp = await valueFieldManager(this, fieldDefinition, cm1, $valueContainer);
            conditionFieldManager(fieldDefinition, conditionCmp); 
        }
    });

    conditionCmp = new _imports.Select2.default({
        label: t`Condition`,
        data: []
    });

    const button = new _imports.Button.default({
        label: t`Add`,
        iconClass: 'plus',
        style: 'float:right',
        onClick: () => {
            const selectedField = fieldCmp.val();
            if (!selectedField) return false;

            const condition = conditionCmp.val();

            const operator = this.rawValue.length === 0 ? '' : 'AND';

            const value = valueCmp.val();

            this.addRowToTable([operator, selectedField, condition, value])

            this.disableApply(false);            

            this.$el().trigger('change');
        }
    });

    this.modal.$body.append(fieldCmp.$el());
    this.modal.$body.append(conditionCmp.$el());
    this.modal.$body.append($valueContainer);
    this.modal.$body.append(button.$el());
    this.modal.$body.append(tableCmp.$el());

    if (op.apply && op.apply.button) {
        const button = this.btnApply = new _imports.Button.default({
            label: op.apply.button.label || t`Apply`,
            style: 'float:right',
            onClick: () => {
                this.previousValue = deepClone(this.rawValue);
                op.apply.callback && op.apply.callback();
            }
        });

        const resetButton = this.btnReset = new _imports.Button.default({
            label: t`Reset`,
            style: 'float:right',
            onClick: () => {
                this.val([]);
                op.apply.callback && op.apply.callback();
            }
        });

        this.disableApply();
        
        this.modal.$footer.append(button.$el());
        this.modal.$footer.append(resetButton.$el());
    }
}

DataFilter.prototype.disableApply = function(disabled = true) {
    this.btnApply.$el().find('button').prop('disabled', disabled);
}

DataFilter.prototype.showFilterModal = function() {
    this.modal.show();
}

DataFilter.prototype.setValueFromString = function(filterValuesString, spatialFilterValuesString) {
    const filterArray = [];

    [filterValuesString, spatialFilterValuesString].map(x => {
        if (x) {
            JSON.parse(x).map(row => {
                filterArray.push(row);
            })
        }
    });

    filterArray.length > 0 && this.val(filterArray);
}

DataFilter.prototype.modifyQuery = function(row, setValue=false) {
    if (row[1] === '_claim_agreement_status') {
        const listId = this.op.refValues.codeLists.find(cl => cl.key === 'dmg_claim_agreement_status').id;  
        if (setValue) {
            const keyId = this.op.refValues.codeListValues.find(cl => cl.key == row[3] && cl.list_id===listId).id;
            row[3] = keyId;
        }
        else {
            const key = this.op.refValues.codeListValues.find(cl => cl.id == row[3] && cl.list_id===listId).key;
            row[3] = key;
        }
    }
}

DataFilter.prototype.addRowToTable = function(row) {
    
    this.rawValue.push(deepClone(row));

    this.modifyQuery(row,true);

    const fieldIndex = row.length === 4 ? 1 : 0;

    const attributeKey = row[fieldIndex];

    const attributeDefinition = this.fields[attributeKey];

    row[fieldIndex] = attributeDefinition.label;

    if (['code_list_reference', 'table_reference', 'code_list_reference_array', 'table_reference_array'].indexOf(attributeDefinition.dataType) !==-1) {
        
        let ids = row[row.length - 1];

        ids = Array.isArray(ids) ? ids : [ids];

        row[row.length - 1] = ids;
    }

    const operator = fieldIndex == 1 ? row[0] : '';
    if (operator) {
        const operatorTranslation = t`${operator}`;
        row[0] = `<a data-op="${operator}" class="operator" href="#">${operatorTranslation}</a>`;
    }

    this.tableCmp.add(row, [null, null, null, attributeDefinition]);
}

DataFilter.prototype.val = function(filterValue) {

    if (filterValue !== undefined) {

        this.tableCmp.val([]);
        this.rawValue = [];

        filterValue.map(row => {
            this.addRowToTable(row);
        });

        this.op.onValueSet && this.op.onValueSet(filterValue);

        this.disableApply(false);

        this.previousValue = deepClone(this.rawValue);

        return;
    }

    const operators = [];

    this.tableCmp.$el().find('tr').each(function() {
        const $td = $(this).find('td:first');
        if ($td.length === 1) {
            operators.push($td.find('a:first').data('op') || '');
        }
    });

    if (this.rawValue.length === operators.length) {
        operators.map((op,inx) => this.rawValue[inx][0] = op);
    }

    return this.rawValue;
}  

export default DataFilter;
