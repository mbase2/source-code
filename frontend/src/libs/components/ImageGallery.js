import * as exports from '../exports';
import dztemplate from './DropzoneTemplates/blueimp';

export default async op => {

    const [
        utils,
        mutils,
        DataFilter,
        Button,
        RadioButtonSelector,
        MediaScroller,
        ModalDialog,
        DropzoneLoader,
        ComponentManager,
        Inputs,
        DataOrder,
        Alert,
        Map
    ] = await Promise.all([
        exports.utils(),
        exports.mutils(),
        exports.DataFilter(),
        exports.Button(),
        exports.RadioButtonSelector(),
        exports.MediaScroller(),
        exports.ModalDialog(),
        exports.DropzoneLoader(),
        exports.ComponentManager(),
        exports.Inputs(),
        exports.DataOrder(),
        exports.Alert(),
        exports.Map()
    ]);

    const t = utils.t;

    let map = null;

    let table = null;

    let changeTableRecordSetFunction = null;

    const dialog = new ModalDialog.default({
        size: 'modal-xl',
        maxWidth: '100%'
    });
    dialog.show();

    const $container = dialog.$body;

    $container.css('display', 'flex');
    $container.css('max-height', '81vh');

    const $left =  $('<div style="width:640px"/>');
    const $mediaScroller =  $('<div style="height:100%; max-height: calc(100%); overflow-y: auto"/>');
    const $right = $('<div style="height:100%; width:calc(100% - 680px); max-height: calc(100%); border-left: solid black 1px; padding:10px"/>');
    $container.append($left);
    $container.append($right);

    $left.append($mediaScroller);

    const $map = $('<div style="height:50vh"/>');
    const $table = $('<div style="height:20vh"/>')

    $right.append($map);
    $right.append($table);

    await tableComponent();

    const mediaScroller = new MediaScroller.default({
        fetch: (offset, scope = '/') => {
            console.log('fetch')
            return mutils.requestHelper(op.globals.apiRoot + '/uploaded-files' + op.mediaRootFolder +'/' + (offset || 0) + scope);
        },
        scope: '/unprocessed',
        mediaRoot: op.globals.mediaRoot + op.mediaRootFolder,
        onMediaSelected: onMediaSelected,
        onMediaSelectionChanged: onMediaSelectionChanged,
        onDataFetched: (data, added) => {
            changeTableRecordSetFunction && changeTableRecordSetFunction(data);
        }
    });

    $mediaScroller.append(mediaScroller.$el());

    const uploadBtn = new Button.default({
        label:t`Upload images`,
        iconClass: 'upload',
        type: 'btn-primary',
        classes: 'btn-lg',
        onClick: () => {

            const loader = new DropzoneLoader.default({
                url: op.globals.apiRoot + '/file-upload' + op.mediaRootFolder,
                onComplete: onFileUploaded,
                autoProcessQueue: false,
                template: dztemplate
            });

            const dlg = new ModalDialog.default({
                onClose: () => loader.dropzone.destroy()
            });

            dlg.$body.append(loader.$el());

            const uploadBtn = new Button.default({
                label:t`Upload`,
                onClick: () => {
                    loader.startUpload();
                    //loader.dropzone.processQueue();
                }
            });

            dlg.$footer.append(uploadBtn.$el());
            dlg.show();
        }
    });

    dialog.$footer.append(uploadBtn.$el());

    //table
    async function tableComponent() {
        /**
         * Unprocessed images table
         */
        const fileAttributes = [];
        fileAttributes.push(mutils.createVariableDefinition('file_name', t`File name`, 'text'));
        fileAttributes.push(mutils.createVariableDefinition('uid', t`Uploaded by`, 'table_reference','_users'));
        fileAttributes.push(mutils.createVariableDefinition('record_created', t`Uploaded on`, 'timestamp'));
        fileAttributes.push(mutils.createVariableDefinition('photos', 'file_hash', 'jsonb'));

        fileAttributes.map(a => a.visible_in_table=true);

        const tableRecordsOptions = await mutils.generalTableRecordsOptions($table, '__unprocessed', fileAttributes); //there is no add or edit record posibility so we choose a dummy name for this table because it won't be binded directly to a database table
        
        tableRecordsOptions.tableOptions = {
            hideColumns: ['photos'],
            scrollY: '20vh'
        }

        tableRecordsOptions.btn_batch_import = false;
        tableRecordsOptions.btn_add_record = false;
        tableRecordsOptions.skipId = true;

        tableRecordsOptions.processTableData = (row, inx) => {

            row.photos = [row.file_hash];
            row.photos.map(photo => {
                row[photo] = true;
            });
            return row;
        };

        tableRecordsOptions.onTableCreated = (_table) => table = _table;

        tableRecordsOptions.changeRecordSet = (_changeRecordSet) => {
            changeTableRecordSetFunction = _changeRecordSet;
            changeTableRecordSetFunction([]);
        }

        tableRecordsOptions.cm = new ComponentManager.default();

        tableRecordsOptions.disableEdit = true;

        tableRecordsOptions.externalEditRecord = row => {
            dataTable && dataTable.__editRecord && dataTable.__editRecord();
        }
    
        const recordsTable = await import('../../modules/recordsTable');
        return await recordsTable.default(tableRecordsOptions);
    }

    function onFileUploaded(fileData, $el) {
        mediaScroller.appendImages([fileData], false);
        /*const cm = tableRecordsOptions.cm;
        const data = cm.getData('table');
        data.push(fileData);
        cm.refresh('table');*/
    }

    function onMediaSelected(mediaData) {
        map = Map.leafletPreview(map, $map, op.globals.mediaRoot + op.mediaRootFolder + mutils.filePath(mediaData.currentPhotoHash || mediaData.file_hash), mediaData.properties);
    }

    function onMediaSelectionChanged() {
        let currentTableCmp = table;
        const selectedMedia = mediaScroller.val();
        console.log('selectedMedia', selectedMedia)
        
        const photosColumnIndex = currentTableCmp.getColumnsMap()['photos'];
        //search the selected images in the table
        currentTableCmp.table
        .columns( photosColumnIndex )
        .search( selectedMedia.join('|'), true )
        .draw();
    }

}