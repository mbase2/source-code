import * as Inputs from './Inputs';
import * as InputMask from './InputMask';
import * as Accordion from './Accordion';
import globals from '../../app-globals';
import { jsonParse } from '../utils';
import { getVariableProperty } from '../mbase2_utils';

const { proj4defs, language } = globals;

/**
 * 
 * @param {object} op 
 * @param {integer} op.defaultCRS
 * @param {function} op.imports preloaded functions used in this component, {Select2,t, Button} are required
 * @param {object} op.$parent jQuery DOM $container used to render the component in
 * @param {function} op.spatialRequest if defined this function is called when the coordinates change - e.g. used to get data about spatial units of the selected point
 * @param {boolean} op.distanceFromSettlement show text box to enter distance from the settlement
 * @param {boolean} op.localName show text box to enter local name
 * @param {boolean} op.locationTypeData show drop down to select the location type
 * @param {boolean} op.revert give user possibility to revert the change - default FALSE
 * @param {boolean} op.confirmChange user has to confirm if the change in coordinates was done on purpose - default FALSE
 */

function LocationSelector (op = {}) {
    const $el = $('<div/>', {
        style:"display:flex; height: 40em"
    });
    
    const $map = this.$map = $('<div/>', {
        style: "height:100%;width:50%"
    });

    const $coords = this.$coords = $('<div/>', {
        style: "width:50%; padding-left:15px"
    });

    this.op = op;

    if (this.op.revert) {
        this.$revert = $('<div/>');
        $coords.append(this.$revert);
    }

    const t = this.t = op.imports.t || (x => x[0]);

    const $requestResults = $('<div/>');

    //this variables

    this.$requestResults = $requestResults;

    this.$el = () => $el;

    this.$el().on('change',()=>{
        this.convertToDMS();       
    });
    
    this.value = {
        id: op.id || null,
        lat: null,
        lon: null,
        _uninitialized: true 
    };

    const self = this;

    this.availableCRS = [
        [3912, 'EPSG:3912 - D48/GK'],
        [4326, 'EPSG:4326 - WGS84/φλ'],
        [3794, 'EPSG:3794 - D96/TM']
    ];

    if (op.additionalCRS) {
        this.availableCRS = [...this.availableCRS, ...op.additionalCRS];
    }

    this.moduleVariablesByKey = {};

    if (op.moduleVariables) {
        op.moduleVariables = op.moduleVariables();
        if (op.moduleVariables) {
            const variables = op.moduleVariables.filter(v => ['crs','dms'].indexOf(v.key_name_id)!==-1);
            variables.map(v => {
                v.translations = jsonParse(v.translations);
                v.help = jsonParse(v.help);

                v.t_translations = v.translations && (v.translations[language] || v.translations['en']);
                v.t_help = v.help && (v.help[language] || v.help['en']);

                console.log('v', language, v)

                this.moduleVariablesByKey[v.key_name_id] = v;
            })
        }
    }

    this.epsgToLabelKey = {};

    this.availableCRS.map(c => {
        this.epsgToLabelKey[c[0]] = c[1];
    });

    const crs = this.crsCmp = new op.imports.Select2.default({
        label: getVariableProperty(this.moduleVariablesByKey, 'crs', 't_translations') || t`Koordinatni sistem`,
        helpIconText: getVariableProperty(this.moduleVariablesByKey, 'crs', 't_help'),
        onSelect: crs => {
            const {lat, lon} = self.value;

            crs = (crs && crs.params && crs.params.data.id) || crs;

            if (self.dms) {
                if (crs == 4326) {
                    self.dms.$el().show();
                    self.dmsInputs.$el().show();
                    const dms = self.dms.val();
                    self.dmsInputs.$el().find('input').prop('disabled', !dms);
                    self.inputs.$el().find('input').prop('disabled', dms);
                }
                else {
                    self.dms.$el().hide();
                    self.dmsInputs.$el().hide();
                    self.inputs.$el().find('input').prop('disabled', false);
                }
            }

            if (!crs || !lat || !lon) return;

            self.val({lat: lat, lon: lon}, false, 4326, true, true);
            
            self.updateFields(crs);
        },
        data: this.availableCRS.map(c=>[c[0], t`${c[1]}`])
    });

    $coords.append(crs.$el());

    this.spatialRequest = op.spatialRequest;

    if (!this.spatialRequest && op.defaultSpatialRequest) {
        this.spatialRequest = (lat, lon, $el) => {
            return [
                op.imports.mutils.requestHelper(op.defaultSpatialRequest.apiRoot + `/intersection-attributes/_spatial_units?:lat=${lat}&:lon=${lon}`,'GET', null, {}, $el)
            ];
        }
    }

    this._locations_table = op._locations_table;

    $el.append($map);

    if (op.panels) {
        const acc = new Accordion.default({$parent: $el, closeOthers: true});
        acc.addPanel('Coordinate location');
        acc.getPanel('Coordinate location').$body.html($coords);
        acc.$el().css('margin-left','15px').css('width', '49%');
        $el.append(acc.$el());
    }
    else {
        $el.append($coords);
    }

    op.$parent.append($el);

    this.inputs = new Inputs.default({

        inputs:[
            {
                key: 'lat',
                label: t`Latitude (north)`,
                type: 'text',
                required: op.required,
                //step: 0.000001,
                keydown: onKeyDown,
                keyup: removeLeadingZero,
                onpaste: onPaste
            },
            {
                key: 'lon',
                label: t`Longitude (east)`,
                type: 'text',
                required: op.required,
                //step: 0.000001,
                keydown: onKeyDown,
                keyup: removeLeadingZero,
                onpaste: onPaste
            }
        ]
    });

    this.dmsInputs = new Inputs.default({

        inputs:[
            {
                key: 'dms-lat',
                label: ' ',
                type: 'text',
                required: false
            },
            {
                key: 'dms-lon',
                label: ' ',
                type: 'text',
                required: false
            }
        ]
    });

    Object.keys(this.dmsInputs.inputs).map(key=>{
        const obj = new InputMask.default({
            $input: this.dmsInputs.inputs[key].$input,
            onAccept: (mask,$el) => {
                if (mask._silent) return;
                const values = {};
                ['D','M','S','s'].map(key => {
                    values[key] = mask.masked.maskedBlock(key).rawInputValue || 0.0;
                });
                const value = parseFloat(values.D) + parseFloat(values.M)/60 + parseFloat(values.S + '.' + values.s)/3600;

                const vkey = key.replace('dms-','');

                const $input = this.$el().find('#' + vkey);

                $input.val(value.toFixed(6));

                manualEntry();
                
                //$input.length > 0 && $input[0].dispatchEvent(new Event('change'));
                //el.dispatchEvent(new Event('input', { 'bubbles': true }));
            },
            //onComplete: (mask,$el) => console.log('onComplete',mask,$el),
            maskOptions: {
                mask: "`D° `M' `S.s''",
                lazy: false,  // make placeholder always visible
                overwrite: true,
                blocks: {
                    D: {
                        mask: IMask.MaskedRange,
                        from:0,
                        to: 60,
                        maxLength: 2
                    },
                    M: {
                        mask: IMask.MaskedRange,
                        from:0,
                        to: 59
                    },
                    S: {
                        mask: IMask.MaskedRange,
                        from:0,
                        to: 59
                    },
                    s: {
                        mask: IMask.MaskedRange,
                        from:0,
                        to: 99
                    }
                }
            }
        });
        
        obj.mask.unmaskedValue = "00000000";
        this.dmsInputs.inputs[key].inputMask = obj;
    });

    this.inputs.$el().on('keyup', manualEntry);

    this.projected = {
        raw:{},
        formatted:{}
    };

    if (this.spatialRequest) {
        const btn = new op.imports.Button.default({
            label: t`Posodobi prostorske podatke`,
            onClick: () => {
                this.value.spatial_request_result = null;
                this.updateFields();
            }
        });

        this.$requestButton = btn.$el();
        this.$requestButton.css('margin-top','15px');
    }

    this.dms = new Inputs.Input({
        type: 'checkbox',
        label: getVariableProperty(this.moduleVariablesByKey, 'dms', 't_translations') || t`DMS input`,
        helpIconText: getVariableProperty(this.moduleVariablesByKey, 'dms', 't_help'),
        //helpIconText: t`unchecked: coordinates are entered as decimal degrees; checked: coordinates are entered as degrees, minutes, seconds`,
        onChange: () => {
            const dms = this.dms.val();
            this.dmsInputs.$el().find('input').prop('disabled', !dms);
            this.inputs.$el().find('input').prop('disabled', dms);
        }
    });

    this.dms.$el().find('.checkbox').css('margin-bottom', '2px');
    this.dms.$el().find('input').css('margin-top', '1px');

    this.dms.$el().css('display','flex').css('justify-content', 'flex-end');

    $coords.append(this.dms.$el());

    const $div = $('<div/>', {style:"display: flex"});
    this.inputs.$el().css('width','100%');
    this.dmsInputs.$el().css('width','100%').css('margin-left','10px');
    this.dmsInputs.$el().find('input').prop('disabled', true);
    $div.append(this.inputs.$el());
    $div.append(this.dmsInputs.$el());

    this.dmsInputs.$el().find('span').css('visibility', 'hidden');
    
    $coords.append($div);

    const _inputs = [];

    this.op.distanceFromSettlement && _inputs.push({
        key: 'sdist',
        //label: t`Oddaljenost od naselja (hiše) v metrih`
        label: t`Distance from settlement (house) in meters`
    });

    this.op.localName && _inputs.push({
        key: 'lname',
        label: t`Local name`
    });

    if (this.op.locationTypeData) {
        this.locationType = new op.imports.Select2.default({
            label: t`Location type`,
            onSelect: () => {
                this.$el().trigger('change')
            },
            data: this.op.locationTypeData.map(d => [d.key, (d.translations && d.translations[language]) || d.key])
        });
        this.locationType.val('GPS', true);
    }

    const inputs = this.additional = new Inputs.default({
        inputs: _inputs
    });

    if (this.$requestButton) {
        this.$requestButton.hide();
        $coords.append(this.$requestButton);
    }

    $coords.append(this.$requestResults);

    $coords.append('<hr>');

    $coords.append(inputs.$el());

    crs.val(this.op.defaultCRS || 4326);

    this.locationType && $coords.append(this.locationType.$el());

    //init leaflet map

    const map = L.map($map[0],{ zoomControl: false }).setView([46.119944, 14.81533], 8);
    new L.Control.Zoom({ position: 'bottomright' }).addTo(map);
    this.map = map;

    this.popup = L.popup();

    map.on('click', e => {
        const c = {
            lat: e.latlng.lat,
            lon: e.latlng.lng
        }
        self.val(c, false);
        self.showPopup(c);
        self.value.spatial_request_result = null;
        self.updateFields();
    });

    const baseMaps = {
        "OpenStreetMap": L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }),
        "DOF": L.tileLayer.wms("https://ipi.eprostor.gov.si/wms-si-gurs-dts/wms", {
            layers:"SI.GURS.ZPDZ:DOF050",
            format: "image/png",
            transparent: "true",
            maxNativeZoom: 19,
            maxZoom: 23,
            attribution: '<a target="_blank" href="https://eprostor.gov.si/imps/srv/slv/catalog.search#/metadata/a1af8b13-2a5d-45d3-ae50-62069a2d2713">Geodetska uprava Republike Slovenije</a>'
        })
      };

      const mbAttr = 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community';

      const mbUrl = 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';
  
      baseMaps['ESRI World Imagery'] = L.tileLayer(mbUrl, {
        id: 'esri',
        attribution: mbAttr
      });
  
    const overlays = {
    "Občine":  L.tileLayer.wms("https://ipi.eprostor.gov.si/wms-si-gurs-rpe/wms", {
        layers:"SI.GURS.RPE:OBCINE",
        format: "image/png",
        transparent: "true",
        attribution: '<a target="_blank" href="https://eprostor.gov.si/imps/srv/slv/catalog.search#/metadata/a1af8b13-2a5d-45d3-ae50-62069a2d2713">Geodetska uprava Republike Slovenije</a>'
    })
    };

    L.control.layers(baseMaps, overlays).addTo(this.map);

    baseMaps['OpenStreetMap'].addTo(this.map);

    /**
     * Functions
     */

    function onPaste(e) {

        let pastedText = e.originalEvent.clipboardData.getData('text');

        pastedText = pastedText.replace(/,/g, '.');

        const pastedValue = parseFloat(pastedText);

        const $this=$(this);

        const originalValue = $this.val();

        if (isNaN(pastedValue)) {
            return false;
        }

        setTimeout(() => {
            if (($this.val().match(/\./g) || []).length > 1) {
                $this.val(originalValue);
            }
        },20);
    }

    function removeLeadingZero(e) {
        const $this = $(this);

        const value = $this.val();

        if (value && value[0] === '0') {
            $this.val(value.substring(1));
            $this[0].setSelectionRange(0,0);
        }
    }

    function onKeyDown(e) {
        
        if (e.ctrlKey) return true;

        if (e.which < 32 || ['ArrowLeft','ArrowRight','ArrowUp','ArrowDown','Insert','Delete','PageUp','PageDown','Home','End'].indexOf(e.key) !==-1) return true;

        const allowedKeys = [0,1,2,3,4,5,6,7,8,9,',','.'].map(key=>key+'');
        if (allowedKeys.indexOf(e.key)===-1) return false;

        const $this = $(this);

        const value = $this.val();

        if ([',','.'].indexOf(e.key)!==-1) {
            if (value.includes('.')) {
                return false;
            }

            if (e.key === ',') {
                $this.val(value+'.');
                return false;
            }
        }
    }

    function manualEntry() {
        if (!self.map) return;
        hidePopup();
        const c = self.inputs.val();
        if (!c.lat || !c.lon) return;
        const sprememba = (!self.value || (Math.abs(c.lat - self.value.lat) > 1e-6) || (Math.abs(c.lon - self.value.lon) > 1e-6)) ? true : false;   //have coordiantes changed?
        
        self.val(c,false, crs.val(), true, true);

        self.showPopup(self.value);

        if (sprememba) {
            self.$requestResults.empty();
            self.$requestButton && self.$requestButton.show();
            if (self.value) {
                self.value.spatial_request_result = null;
            }   
        }
    }

    function hidePopup() {
        self.map &&  self.map.closePopup();
    }
}

LocationSelector.prototype.showPopup = function(c) {
    if (c && c.lat && c.lon) {
        this.popup
        .setLatLng([c.lat, c.lon])
        .setContent(this.projectedMessage)
        .openOn(this.map);
    }
}

LocationSelector.prototype.convertToDMS = function() {
    if (!this.crsCmp) return;
    if (this.crsCmp.val() != 4326) return;
    const value = this.val();

    ['lat','lon'].map(key => {
        if (value[key]) {
            const mask = this.dmsInputs.inputs['dms-'+key].inputMask.mask;
            mask._silent = true;
            mask.unmaskedValue = dec2dms(value[key]);
            mask._silent = false;
        }
    })

    function dec2dms(deg) {
        const d = Math.floor (deg);
        const mdec = (deg-d)*60;
        const m = Math.floor(mdec);
        const sdec = (mdec-m)*60;
        const s = Math.floor(sdec);
        
        return String(d).padStart(2, '0') + String(m).padStart(2, '0') + String(s).padStart(2, '0') + (sdec-s).toFixed(2).replace('0.','');
    }
}

function projections(self, value, crs = 4326) {

    const pdefs = proj4defs;

    let {lat, lon} = value;

    const selectedCrsCode = self.crsCmp.val();

    const projectTo = self.op.showAllCrsTransformationsInPopup ? self.availableCRS.map(c=>c[0]).filter(epsg=>epsg!==4326) : (selectedCrsCode == 4326 ? [] : [selectedCrsCode]);

    if (crs != 4326) {
        self.projected.raw[crs] = [lon, lat].map(c => parseFloat(c));

        //project to 4326
        const pdef = pdefs[crs];
        const lonlat = proj4(pdef, pdefs[4326], [ lon, lat].map(c => parseFloat(c)));
        lon = lonlat[0];
        lat = lonlat[1];
    }

    projectTo.map(crs => {
        self.projected.raw[crs] = proj4(pdefs[crs], [ lon, lat].map(c => parseFloat(c)));
    });

    self.projected.formatted[4326] = [lat, lon].map(c => parseFloat(c).toFixed(6));

    const msg = [`<b>EPSG:4326 - WGS84/φλ:</b> ${self.projected.formatted[4326].toString()}`];

    projectTo.map(epsg => {
        const label = self.epsgToLabelKey[epsg];
        self.projected.formatted[epsg] = self.projected.raw[epsg].reverse().map(c => c.toFixed(2));
        msg.push(`<b>${label}</b> ${self.projected.formatted[epsg].toString()}`);
    });

    self.projectedMessage = msg.join('<br>');

    return {lat,lon};
}
LocationSelector.prototype.updateFields = function(crsCode = null) {

    const _updateFields = (crs) => {
        this.inputs.val({
            lat: this.value.lat ? this.projected.formatted[crs][0] : null,
            lon: this.value.lon ? this.projected.formatted[crs][1] : null
        });
    
        this.showPopup(this.value);
    
        this.$el().trigger('change');
    }

    if (crsCode) {
        _updateFields(crsCode);
        return;
    }

    const self = this;
    const crs = this.crsCmp;

    if (self.spatialRequest) {
        self.$requestButton.hide();
        self.$requestResults.html('<hr>');

        const $container = $(`<div class="alert alert-info">
            <span class="pficon pficon-info"></span>
        </div>`);

        if (self.value.lat && self.value.lon) {
            if (self.value.spatial_request_result) {
                self.processSpatialRequest($container);
                _updateFields(crs.val());
            }
            else {
                Promise.all(self.spatialRequest(self.value.lat, self.value.lon, self.$el())).then(results => {
                    self.value.spatial_request_result = {};
                    for(const res of results) {
                        self.value.spatial_request_result = Object.assign(self.value.spatial_request_result, res[0]);
                    }
                    
                    self.processSpatialRequest($container);
                    _updateFields(crs.val());
                    self.op.onSpatialRequestFinished && self.op.onSpatialRequestFinished(Object.assign({}, self.value.spatial_request_result));
                });
            }
        }
    }
    else {
        _updateFields(crs.val());
    }
}

LocationSelector.prototype.processSpatialRequest = function ($container) {
    const self = this;

    if (self.value && self.value.spatial_request_result) {
        const srr = self.value.spatial_request_result;
        const keys = self.op.spatialAttributes || ['ob_uime', 'oe_ime', 'luo_ime', 'lov_ime', 'cntr_id']; //postgres jsonb does not preserve order
        for (const key of keys) {
            if (!srr[key]) continue;
            const value = srr[key];
            const label = self.t`${key}`;
            $container && $container.append(`
                <strong>${label}: </strong> ${value}<br>
            `);
        };

        $container && self.$requestResults.append($container);
    }
}

LocationSelector.prototype.val = function(value, updateFields = true, crsCode = 4326, skipConfirm=false, skipRevert=false) {
    if (value !== undefined) {
        
        const t = this.t;

        if (!skipConfirm && !this.value._uninitialized && this.op.confirmChange) {
            if (!confirm (t`The coordinates have changed - click OK if this was done on purpose.`)) return;
        }

        if (!skipRevert && !this.value._uninitialized && this.op.revert) {
            const $alert = $(`<div class="alert alert-warning">
                <span class="pficon pficon-warning-triangle-o"></span>
            </div>`);

            this.previousValue = Object.assign({}, this.val(undefined, false));

            const $msg = $('<div>'+t`The value has been changed. Click`+' </div>');
            const $here = $('<a href="#"><b>' + t`here` + '</b></a>');
            $msg.append($here);
            $msg.append(' ');
            $msg.append(t`to revert this change`);
            $msg.append('.');

            $here.on('click', () => {
                this.val(this.previousValue, true, 4326, true);
                this.$revert.empty();
                return false;
            });

            $alert.append($msg);

            this.$revert.html($alert);
        }

        delete this.value._uninitialized;
        
        if (value === null || value.lat === null || value.lon === null) {
            this.value = {
                id: null,
                lat: null,
                lon: null
            };
        }
        else {
            this.value.id = value.id;
            if (value.crs && value.crs != crsCode) crsCode = value.crs;
            const {lat, lon} = projections(this, value, crsCode); 
            this.value.lat = lat;
            this.value.lon = lon;
        }

        if (value && value.additional) {
            this.additional.val(value.additional);

            if (this.op.locationTypeData) {
                this.locationType && this.locationType.val(value.additional.ltype, true);
            }
        }

        if (value && value.spatial_request_result) {
            this.value.spatial_request_result = value.spatial_request_result;
        }

        if (updateFields) {
            this.updateFields();
        }
    }

    this.value.additional = this.additional && Object.assign({},this.additional.val());

    if (this.op.locationTypeData && this.locationType) {
        this.value.additional.ltype = this.locationType.val();
    }

    if (this._locations_table) this.value._locations_table = this._locations_table;

    if (this.value.id===undefined) delete this.value['id'];

    return this.value;
}

export default LocationSelector;
