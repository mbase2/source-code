import { permissions } from "../mbase2_utils";
import { t } from "../utils";

import './css/EventsLegend.css';

function checkPermissions(moduleKey) {
  return permissions(['admin', 'editor', 'consumer', 'reader', 'curator'], moduleKey);
}

const legendHtml = (rows,userRoles=[]) => {
  const banTitle = t`You don't have permissions to access detailed data for this module.`;
  return rows.map(row => {
    const detailIsAllowed = checkPermissions(row.id);
    return `
    <div style="display:flex; font-size:1.5em" id="legend-item-${row.id}">
        <label data-id="${row.id}" class="switch legend-item-control legend-item-control-disabled" style="margin-top:0px; margin-right: 10px;">
          ${
            detailIsAllowed ?
            `
            <input class="legend-item-accuracy" data-id="${row.id}" type="checkbox">
            <span class="slider-base slider fa" style="background-color: ${row.color}; border: solid 1px ${row.color};">
              &nbsp;&#xf041;
            </span>`
            :
            `<span title="${banTitle}" class="slider-base" style="background-color: ${row.color}; border: solid 1px ${row.color};">              
            </span>`
          }
        </label>
        <div data-id="${row.id}" class="legend-item-filter legend-item-control legend-item-control-disabled" style="margin-right: 10px;margin-top:-2px;cursor:pointer"><i class="fa fa-filter" aria-hidden="true"></i></div>
        <div data-id="${row.id}" class="legend-item-download legend-item-control legend-item-control-disabled" style="margin-right: 10px;margin-top:-2px;cursor:pointer"><i class="fa fa-download" aria-hidden="true"></i></div>

        <div class="checkbox" style="margin-top:-2px; font-size:0.8em">
          <label style="display: inline;vertical-align: text-top"><input class="layer-visibility" data-id="${row.id}" type="checkbox">${row.t_id || row.key_id}</label>
        </div>
    </div>
    `
  }).join('');
};

	var EventsLegend=function(op){

    if (op===undefined) op={};
    
    this._$el=$('<div/>');
			
		this._$el.html(legendHtml(op.rows));

    const self = this;
    
    if (op.onChange) {
      this._$el.find('input.layer-visibility').on('change', function() {
        const $this = $(this);
        const moduleKey = $this.data('id');
        const show = $this.prop('checked');
        const $legendItemControls = $(`#legend-item-${moduleKey} .legend-item-control`);
        
        if (show) {
          $legendItemControls.removeClass('legend-item-control-disabled');
        }
        else {
          $legendItemControls.addClass('legend-item-control-disabled');
        }

        self.disableGridDownload(moduleKey);

        op.onChange(moduleKey, show);
      });
    }

    const filters = this._$el.find('.legend-item-filter');

    if (op.onDataFilter) {
      filters.on('click', function() {
        op.onDataFilter($(this).data('id'));
      });
    }
    else {
      filters.hide();
    }

    if (op.onDataDownload) {
      this._$el.find('.legend-item-download').on('click', function() {
        op.onDataDownload($(this).data('id'));
      });
    }

    if (op.onDataAccuracy) {
      const self = this;
      this._$el.find('.legend-item-accuracy').on('click', function(e) {
        const id = $(this).data('id');
        self.disableGridDownload(id);
        op.onDataAccuracy(id);
        e.stopPropagation();
      });
    }

	};

  EventsLegend.prototype.disableGridDownload = function(moduleKey) {
    const $download = $(`#legend-item-${moduleKey} .legend-item-download`);

    if (this.isAccuracySetToGrid(moduleKey)) {
      $download.addClass('legend-item-control-disabled');  
    }
    else {
      $download.removeClass('legend-item-control-disabled');
    }
  }

  /**
   * 
   * @param {string} moduleName 
   * @returns {boolean} true if grid, false if detail
   */
  EventsLegend.prototype.isAccuracySetToGrid = function(moduleName) {
    const $item = this._$el.find(`.legend-item-accuracy[data-id='${moduleName}']`);
    //only items with detail permission have checkbox

    return $item[0] ? !$item.prop('checked') : true;
  }

  EventsLegend.prototype.setDetailAccuracy = function(moduleName, value) {
    const $item = this._$el.find(`.legend-item-accuracy[data-id='${moduleName}']`);
    $item.prop('checked', value);
  }
	
	EventsLegend.prototype.$el=function(){
		return this._$el;
	};
  
  EventsLegend.prototype.disableCheckBoxes=function(){
    this._$el.find(".row input:checkbox").prop('disabled',true);
  };
	
	EventsLegend.prototype.val=function(a){
		if (a!==undefined) {
      const self=this;
      const $inputLayerVisibilityCheckBoxes = self._$el.find('input.layer-visibility');
      $inputLayerVisibilityCheckBoxes.prop('checked', false);
      
      for (const cbox of $inputLayerVisibilityCheckBoxes) {
        const $cbox = $(cbox);
        const key = $cbox.data('id');
        if (a.indexOf(key) !== -1) {
          $cbox.prop('checked', true);
        }
      }
    }
    else{
      var checked=[];
      this._$el.find('input.layer-visibility:checked').each(function(){
        checked.push($(this).attr('data-id'));
      });
      return checked;
    }
	};
  
  EventsLegend.prototype.enable=function(enable) {
    this._$el.find('input').prop('disabled',!enable);
  };
  
export default EventsLegend;
