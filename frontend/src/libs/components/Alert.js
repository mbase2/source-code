
/**
 * 
 * @param {object} op 
 */

const Alert = function(op = {}) {
    const $alert = $(`<div class="alert"></div>`);
    const $icon = $(`<span class="pficon"></span>`);
    const $content = this.$content = $('<span/>');

    const $dissmiss = $(`<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
        <span class="pficon pficon-close"></span>
    </button>`);

    const type = op.type || 'info';

    if (op.dismissable === true) {
        $alert.append($dissmiss);
        $alert.addClass("alert-dismissable");
    }

    $alert.addClass(`alert-${type}`);

    $alert.append($icon);
    $alert.append($content);

    let iconClass;
    
    if (type === 'info') {
        iconClass = 'pficon-info';
    }
    else if (type === 'danger') {
        iconClass = 'pficon-error-circle-o';
    }
    else if (type === 'warning') {
        iconClass = 'pficon-warning-triangle-o';
    }
    else if (type === 'success') {
        iconClass = 'pficon-ok';
    }

    $icon.addClass(iconClass);

    this.$el = () => $alert;

    if (op.sections) {
        this.sections = {};
        op.sections.map(section => {
            const $div = $('<div/>');
            this.sections[section] = $div;
            $content.append($div);
        })
    }
    else {
        this.sections = null;
    }
}

Alert.prototype.val = function(v) {
    if (v) {
        if (this.sections) {
            Object.keys(v).map(sectionKey => {
                this.sections[sectionKey].html(v[sectionKey]);
            });
        }
        else {
            this.$content.html(v);
        }
    }
    else {
        if (this.sections) {
            const rval = {};
            Object.keys(this.sections).map(key => rval[key] = this.sections[key].html());
            return rval;
        }
        return this.$content.html();
    }
}

export default Alert;