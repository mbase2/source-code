function DropDown(op={}) {
    this.op = op;
    const id = ('ddid-'+Math.random()).replace('0.','');
    const $el = $('<div class="dropdown"></div>');
    const classes = op.classes ? op.classes : '';

    const $button = $(`
        <button class="btn ${classes} ${op.type || 'btn-default'} dropdown-toggle" type="button" id="${id}" data-toggle="dropdown">
        </button>
    `);

    if (op.iconClass) {
        const $icon=$('<i/>',{class:`fa fa-${op.iconClass}`,'aria-hidden':'true'});
        $button.append($icon);
        $button.append("&nbsp;");
    }

    $button.append(`<span>${op.label}</span>`);
    $button.append(`<span class="caret"></span>`);
    
    const $label = this.$label = $button.find('span:first');

    const $ul = $(`<ul class="dropdown-menu" role="menu" aria-labelledby="${id}"></ul>`);

    op.items.map(item => {
        const $li = $(`<li data-id="${item.key}" role="presentation"></li>`);
        const $a = $(`<a role="menuitem" tabindex="-1" href="#"></a>`);

        if (item.onLabelCreated) {
            $a.append(item.onLabelCreated(item.label));
        }
        else {
            $a.text(item.label);
        }

        $li.append($a);

        item.$li = $li;
        if (op.onClick) {
            $li.on('click', function() {
                op.updateButtonLabel && $label.text(item.label) && $label.data('key', item.key);
                return op.onClick(item, $(this), $li);
            });
        }
        $ul.append($li);
    });

    $el.append($button);
    $el.append($ul);

    this.$el = () => $el;
}

DropDown.prototype.val = function(key) {
    if (key!==undefined) {
        const item = this.op.items.find(item => item.key == key);
        if (item) {
            this.$label.text(item.label);
            this.$label.data('key', item.key);
        }

        return;
    }

    return this.$label.data('key');
}

export default DropDown;
