import './css/DropzoneLoader.css'
import {t} from '../utils'
import {requestHelper, assignRequestCallbackToasters} from '../mbase2_utils'

const dropZoneMenu = labels => $(`
      <span class="btn btn-success fileinput-button">
          <i class="glyphicon glyphicon-upload"></i>
          <span>${(labels && labels.add_files) || "Add files"}</span>
      </span>
`);

const template = `
<div id="template" class="file-row">
  <div class="file-delete">
    
  </div>
  <div>
      <span class="preview"><img data-dz-thumbnail /></span>
  </div>
  <div>
      <p class="name" data-dz-name></p>
      <strong class="error text-danger" data-dz-errormessage></strong>
  </div>
  <div>
      <p class="size" data-dz-size></p>
      <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
        <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
      </div>
  </div>
</div>
`;

/**
 * 
 * @param {object} op 
 * @param {boolean} [op.clickable | true] add button to upload files
 * @param {string} [op.acceptedFiles | image/*]
 * @param {string} [op.template] dropzone previewTemplate if not default
 */
const DropzoneLoader = function (op = {}) {
    let $addFilesBtn;
    
    const clickable = op.clickable !== undefined ? op.clickable : true;
    
    const $el = $('<div/>');

    if (clickable) {
      $addFilesBtn = dropZoneMenu(op.labels);
      $el.append($addFilesBtn);
    }

    const $dropZoneDiv = $(`<div class="table table-striped files dropzone" id="previews"/>`);
    $el.append($dropZoneDiv);

    this.uploads = [];

    const dzoptions = Object.assign(op, { 
      url: op.url,
      acceptedFiles: 'image/*',
      autoProcessQueue: op.autoProcessQueue===false ? false : true,
      previewTemplate: op.template || template,
      //previewsContainer: $dropZoneDiv[0], // Define the container to display the previews
      
      complete: (file, done) => {
        const response = file.xhr ? JSON.parse(file.xhr.response) : file;

          const fdata= {
            size: file.size,
            name: response.file_name,
            id: response.id,
            properties: typeof response.properties === 'object' ? response.properties : JSON.parse(response.properties)
          }
          file.id = fdata.id;
          
          if (file.previewElement) {
            const $name = $(file.previewElement).find('p.name:first');
            const filePath = `${window.location.origin + '/api/mbase2/uploaded-file/private/' + file.id}`;
            const fileName = $name.text();
            $name.html(`<a href="${filePath}">${fileName}</a>`);

            const $fileDeleteDiv = $(file.previewElement).find('div.file-delete');
            const $deleteIcon = $('<span style="cursor: pointer" class="pficon pficon-delete"></span>');
            $deleteIcon.on('click', async () => {
              const $parent = $deleteIcon.parent().parent();
              $parent.css('background', 'red');
              if (confirm(t`Do you really want to remove selected file?`)) {
                const callbacks = await assignRequestCallbackToasters({});
                const res = await requestHelper(filePath, 'DELETE', null, callbacks);
                console.log('res', res)
                if (res!==false) {
                  $parent.remove();
                }
              }
              $deleteIcon.parent().parent().css('background', 'inherit');
            });

            $fileDeleteDiv.html($deleteIcon);
          }

          this.uploads.push(fdata);
          op.onComplete && op.onComplete(response, $el);
      },

      init: function() {
          //https://stackoverflow.com/questions/31627154/dropzone-js-v4-display-existing-files-on-server-with-work-limiting-the-number/31627155#31627155
          this.addCustomFile = function(file, thumbnail_url = null , response = {status: "success"}){
            // Push file to collection
            this.files.push(file);
            // Emulate event to create interface
            this.emit("addedfile", file);
            // Add thumbnail url
            thumbnail_url && this.emit("thumbnail", file, thumbnail_url);
            // Add status processing to file
            this.emit("processing", file);
            // Add status success to file AND RUN EVENT success from responce
            this.emit("success", file, response , false);
            // Add status complete to file
            this.emit("complete", file);
        }
      }

    });

    if (op.acceptAllFiles) {
      delete dzoptions.acceptedFiles;
    }

    if (clickable) {
      dzoptions.clickable = $addFilesBtn[0]; // Define the element that should be used as click trigger to select files.
    }

    dzoptions.headers = Object.assign(dzoptions.headers || {}, {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    });

    const dropzone = this.dropzone = new Dropzone($dropZoneDiv[0], dzoptions); 

    const self = this;

    dropzone.on("addedfile", function(file) {
      // Hookup the start button
      
      if (op.allowIndividualUpload) {
        file.previewElement.querySelector(".start").onclick = function() { 
          dropzone.enqueueFile(file); 
        };
      }
    });

    // Update the total progress bar
    dropzone.on("totaluploadprogress", function(progress) {
      //document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
    });

    dropzone.on("sending", function(file) {
      // Show the total progress bar when upload starts
      //document.querySelector("#total-progress").style.opacity = "1";
      // And disable the start button
    });

    // Hide the total progress bar when nothing's uploading anymore
    dropzone.on("queuecomplete", function(progress) {
      if (op.autoProcessQueue === false) {
        this.options.autoProcessQueue = false;
      }
    });

    //otherwise it only uploads two files
    //https://stackoverflow.com/questions/18059128/dropzone-js-uploads-only-two-files-when-autoprocessqueue-set-to-false
    //https://github.com/dropzone/dropzone/issues/253#issuecomment-22184190

    dropzone.on("processing", function() {
      if (op.autoProcessQueue === false) {
        this.options.autoProcessQueue = true;
      }
    });

    this.$el = () => $el;
}

DropzoneLoader.prototype.startUpload = function() {
  this.dropzone.processQueue();
}

DropzoneLoader.prototype.val = function(values) {
  if (values !== undefined) {

    values.map(file => {
        this.dropzone.addCustomFile({
          processing: true, // flag: processing is complete
          accepted: true,// flag: file is accepted (for limiting maxFiles)
          name: file.name, // name of file on page
          file_name: file.name,
          id: file.id,
          size: file.size, // image size
          //type: null, //file.file_type, // image type
          properties: file.properties,
          status: Dropzone.SUCCESS // flag: status upload
    },
    // Thumbnail url
    (
      (file.properties && ['png', 'jpg', 'jpeg', 'gif'].indexOf(file.properties.type) !==-1)
      ||
      (file.type && ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'].indexOf(file.type))
    )
      && 
      window.location.origin + '/api/mbase2/uploaded-file/private/' + file.id + '/thumbnail')
    })
  }
  
  return this.uploads;
}

export default DropzoneLoader;

