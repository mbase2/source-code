const Media = function (op) {
    const type = this.type = op.type ? op.type : 'img';

    let $el = $('<div/>');

    if (type === 'img') {
        $el = $('<img/>');
    }

    this.$el = () => $el;
}

Media.prototype.val = function(value) {
    if (value !== undefined) {
        if (this.type === 'img') {
            this.$el().attr('src', value);
        }
    }

    return this.$el().attr('src');
}

export default Media;