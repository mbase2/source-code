export default function (op = {}) {

    const $form = $(`<form role="form" class="search-pf has-button">
        <div class="form-group has-clear">
            <div class="search-pf-input-group">
            <label for="search1" class="sr-only">Search</label>
            <input id="search1" type="search" class="form-control" placeholder="${op.placeholder || 'Search'}">
            <button type="button" class="clear" aria-hidden="true"><span class="pficon pficon-close"></span></button>
            </div>
        </div>
    </form>`);

    const $searchButton = $(`<div class="form-group" style="width:auto">
        <div class="btn" style="cursor: auto"><span class="fa fa-search"></span></div>
    </div>`);
    $form.prepend($searchButton);

    this.$el = () => $form;
    
    const $input = $form.find('input:first');
    const $clearButton = $form.find("button.clear:first");

    $clearButton.hide();

    let oldValue = $input.val();

    function search() {

        const value = $input.val();

        if (!value) {
            $clearButton.trigger('click');
        }
        else {
            $clearButton.show();
        }

        if (value != oldValue) {
            oldValue = value;
            op.onChange && op.onChange(value);
        }
    }

    $input.on('keyup change click', search);

    $clearButton.on('click', function() {
        $clearButton.hide();
        $input.val('').trigger('focus');
        op.onClear && op.onClear();
        op.onChange && op.onChange('');
    });
}
    
    