import 'bootstrap-timepicker/css/bootstrap-timepicker.min.css'
import 'bootstrap-timepicker' //https://jdewit.github.io/bootstrap-timepicker/
const TimePicker = function(op = {}) {
  
  if (!op.label) op.label = '';

  const $el = $('<div/>');

  op.required && $el.addClass('required');

  const $div = $('<div/>',{
    class: 'input-group bootstrap-timepicker timepicker'
  });

  this.$el = () => $el;
  
  const $label = this.$label = $(`<span class="label label-primary mbase2-label">${op.label}</span>`);
  const $input = this.$input = $('<input type="text" class="form-control input-small">');
  const $btn = $(`
    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
  `);

  op.disabled && $input.prop('disabled', true);

  $div.append($input);
  $div.append($btn);

  $el.append($label);
  $el.append($div);

  const options = {
    showMeridian: false,
    defaultTime: '00:00'//false
  };

  if (op.startDate) {
    options.startDate = op.startDate;
  }

  $input.timepicker(options);

  this.tp = $input.data('timepicker');

  op.onChange && $input.on('change', () => op.onChange(this.tp.getTime()));

}

TimePicker.prototype.val=function(value){

  if (value !== undefined) {
    this.tp.setTime(value);  
  }

  const rval = this.tp.getTime();

  if (!rval) return null;

  return rval;

}

export default TimePicker;