import * as exports from '../../libs/exports';
/**
 */

export default async op => {
    const [
        Select2,
        ModalDialog
    ] = await Promise.all([
        exports.Select2(),
        exports.ModalDialog()
    ]);

    const _modalDialog = new ModalDialog.default(op.modalDialogOptions || {});

    let _modalDialogSelect = null;
    
    function initModalDialogSelect(data) {
        const options = op.modalDialogSelectOptions || {};
        if (data) {
            options.data = data;
        }
        _modalDialogSelect = new Select2.default(options);
        _modalDialog.$body.append(_modalDialogSelect.$el());
    }

    op.footerButtons && Object.keys(op.footerButtons).map(key => {
        const btn = op.footerButtons[key];
        _modalDialog.$footer.append(btn.$el());
    });

    return Object.freeze({
        initModalDialogSelect,
        val: (a) => _modalDialogSelect.val(a),
        dlg: _modalDialog,
        show: () => _modalDialog.show(),
        close: () => _modalDialog.close()
    });
}