/**
 * 
 * @param {object} op 
 * @param {object} op.attributes
 * @param {object} op.codeListValues
 * @param {object} op.tableReferences
 * @param {string} op.language
 * @param {function} op.onRowRemoved
 */
function SimpleTable(op = {}) {

    const $table = $(`<table class="table table-striped table-bordered"/>`);
    const $thead = $('<thead/>');

    this.op = op;
    
    this.$el = () => $table;

    this.attributes = op.attributes;

    this.codeListValues = op.codeListValues;
    this.tableReferences = op.tableReferences;

    this.language = op.language;

    const $tr = $('<tr/>');
    op.attributes.map(a => {
        $tr.append(`<th>${a.t_name_id || a.key_name_id}</th>`);
    });

    $thead.append($tr);
    $table.append($thead);

    op.hideHeader && $thead.hide();

    this.$tbody = $('<tbody/>');
    
    $table.append(this.$tbody);

    this.data = {};
}

SimpleTable.prototype.add = function(row, attributeDefinitions = []) {
    const $tr = $('<tr/>');

    if (!Array.isArray(row)) {
        const _row = [];
        this.attributes.map(a => {
            _row.push(row[a.key_name_id]);
        });
        row = _row;
    }

    row.map((c,i) => {
        
        if (c instanceof $) {
            const $td = $('<td/>');
            $td.append(c);
            $tr.append($td);
            return;
        }

        const attributeDefinition = attributeDefinitions[i] || this.attributes[i];
        
        if (c && ['code_list_reference', 'table_reference'].indexOf(attributeDefinition.key_data_type_id) !== -1 ) {
            const options = attributeDefinition.key_data_type_id === 'code_list_reference' ? this.codeListValues : this.tableReferences;
            
            if (!Array.isArray(c)) {
                c = [c];
            }

            if (attributeDefinition.key_data_type_id === 'table_reference') {
                c = c.map(value => value + '_' + attributeDefinition.ref)   
            }
            
            c = c.map(cv => {
                let c = options[cv];
                c = c && ((c.translations && c.translations[this.language]) || c.key);
                return c || null;
            }).filter(c => c);

            c = c.length == 1 ? c[0] : JSON.stringify(c);
            
        }

        $tr.append(`<td>${c}</td>`);
    });

    const keys = Object.keys(this.data);
    const inx = keys.length === 0 ? 0 : Math.max(...keys.map(key => parseInt(key))) + 1;

    this.data[inx] = row;

    const self = this;

    if (self.op.removableRows !== false) {
        const $trash = $('<span style="cursor: pointer" class="pficon pficon-delete"></span>');
        $trash.on('click', () => {
            $tr.remove();
            const removedRowCopy = JSON.parse(JSON.stringify(this.data[inx]));
            const consecutiveIndex = Object.keys(this.data).findIndex(key => key == inx);
            delete this.data[inx];
            self.op.onRowRemoved && self.op.onRowRemoved(removedRowCopy, consecutiveIndex);
        });

        const $td = $('<td/>');
        $td.append($trash);
        $tr.append($td);
    }

    this.$tbody.append($tr);
    this.op.onRowAppended && this.op.onRowAppended(row);
}

SimpleTable.prototype.clear = function() {
    this.$tbody.empty();
    this.data = {};
}

SimpleTable.prototype.val = function(data) {
    if (data !== undefined) {
        this.clear();
        data.map(row=>this.add(row));
    }

    return Object.values(this.data);
}


export default SimpleTable;