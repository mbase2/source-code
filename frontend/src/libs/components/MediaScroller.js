//https://dev.to/trezy/loading-images-with-web-workers-49ap
const MediaScroller = function(op) {

    this.mediaRoot = op.mediaRoot;
    this.fetchFun = op.fetch;
    this.init();
    this.loadingStep = 15;

    this.loadFrom = 0;

    this.images = {};

    this.op = op;

    this.properties = {};

    this.scope = op.scope === undefined ? '/processed' : op.scope;

    this.onMediaSelected = op.onMediaSelected;
    this.onMediaSelectionChanged = op.onMediaSelectionChanged;

    this.$navigationInfo = $('<div/>',{style:'margin-right: 5px'});
    this.$navigationBck = $('<button type="button" class="btn btn-default"><</button>');
    this.$navigationFwd = $('<button type="button" class="btn btn-default">></button>');

    const $navigation = $(`<div style="display:flex;margin-left:auto;margin-right:15px;width:fit-content;align-items:center"></div>`);
    $navigation.append(this.$navigationInfo);
    $navigation.append(this.$navigationBck);
    $navigation.append(this.$navigationFwd);

    this.$navigationFwd.on('click', () => {
        this.loadFrom = this.loadFrom + this.loadingStep;
        this.appendImages();
        this.updateNavigationInfo();
    });

    this.$navigationBck.on('click', () => {
        this.loadFrom = this.loadFrom - this.loadingStep;
        this.appendImages();
        this.updateNavigationInfo();
    });

    const $el = $('<div/>');
    this.$imageContainer = $('<div class="image-container"/>');

    $el.append($navigation);
    $el.append(this.$imageContainer);

    this.$el = () => $el;

    this._fetch();
}

MediaScroller.prototype.updateNavigationInfo = function() {
    if (this.loadFrom == 0) {
        this.$navigationBck.prop('disabled', true);
    }
    else {
        this.$navigationBck.prop('disabled', false);
    }

    this.$navigationInfo.html(`${this.loadFrom + 1}-${this.loadFrom + this.loadingStep} / ${Object.keys(this.images).length}`);
}

MediaScroller.prototype.preprocessFetchedData = function (res) {
    
    const properties = this.properties = {};

    const isProcessed = this.scope === '/processed';

    if (isProcessed) {
        res[1].map(r => {
            properties[r.file_hash] = JSON.parse(r.properties);
        });
        res = res[0];
    }
    else {
        res.map(r => {
            properties[r.file_hash] = JSON.parse(r.properties);
        });
    }

    res.map(r=>{
        const im = r;

        if(isProcessed) {
            im.photos = Array.isArray(im.photos) ? JSON.stringify(im.photos) : im.photos;
            im.file_hash = im.photos;
            im.photos = JSON.parse(im.photos);
            im.currentPhotoInx = 0;
            im.currentPhotoHash = im.photos[0];
        }

        im.properties = properties[im.currentPhotoHash || im.file_hash];

        this.images[im.file_hash] = im;
    });
}

MediaScroller.prototype._fetch = function () {
    if (this.fetchFun) {
    
        this.fetchFun(this.offset, this.scope).then(res => {
            this.preprocessFetchedData(res);
            this.appendImages();
            this.updateNavigationInfo();
            this.op.onDataFetched && this.op.onDataFetched(res, Object.values(this.images));
        });        
        
        this.offset = this.offset + this.loadingStep * 2;
    }
}

MediaScroller.prototype.init = function() {
    this.selectedImages = {};
    this.images = {};
    this.select_all = false;
    this.offset = 0;
    this.scope = '/processed'; //loads processed images, the other option is 'unprocessed'
}

MediaScroller.prototype.val = function(value) {
    if (value !== undefined) {

    }

    let hashes = [];
    if (this.scope === '/processed') {
        Object.keys(this.selectedImages).map(key => hashes.push(...JSON.parse(key)));
    }
    else {
        hashes = Object.keys(this.selectedImages);
    }
    
    return hashes;
}

MediaScroller.prototype.action = function (action) {
    if (action === 'deselect') {
        this.$imageContainer.find('div.selected').removeClass('selected');
        this.selectedImages = {};
        this.select_all = false;
    }
    else if (action === 'select_all') {
        this.select_all = true;
        this.$imageContainer.find('img').addClass('selected');
        Object.keys(this.images).map(key => {
            this.selectedImages[key] = this.images[key].$img;    
        });
    }
    else if (action === 'clear') {
        this.$imageContainer.empty();
        this.init();
    }
    else if (action === 'load_all' || action === 'load_processed') {
        this.action('clear');
        this.scope = '/processed';
        this._fetch();
    }
    else if (action === 'load_unprocessed') {
        this.action('clear');
        this.scope = '/unprocessed';
        this._fetch();
    }
    
    this.onMediaSelectionChanged && this.onMediaSelectionChanged();
}

MediaScroller.prototype.appendImages = function() {

    const self = this;

    const properties = this.properties;

    const isProcessed = this.scope === '/processed';

    this.$imageContainer.empty();

    const loadTo = this.loadFrom + this.loadingStep;

    const images = Object.values(this.images);

    for (let i=this.loadFrom; i < loadTo; i++) {
        
        if (i>=images.length) continue;

        const im = images[i];

        let fileName = null;

        if (!isProcessed) {
            fileName = im.file_hash+'_thumbnail';
        }
        else {
            fileName = im.photos[0]+'_thumbnail';
        }

        const src = (fileName) => this.mediaRoot + '/m' + fileName.substring(0,2) + '/' + fileName + '/' + properties[isProcessed ? im.currentPhotoHash : im.file_hash].type;
        const $img = $(`<img style="max-height:160px; width: 160px" data-src="${im.file_hash}" src="${src(fileName)}">`);

        const $div = $('<div class="container"/>');
        $div.css('width', this.op.containerWidth || '180px');
        const $imgDiv = $('<div/>',{style: 'position: relative; margin-right: 18px; height:160px; padding:0'}); //parent has to have relative or absolute position in order to avoid child is floating around when scrolling
        const $prop = $('<div/>',{style:'overflow:auto;max-height:160px'});

        if (this.scope === '/processed' && im.photos.length > 1) {
            const $group = $('<div class="btn-group" style="position: absolute;float:left;"/>');
            const $back = $('<button type="button" class="btn btn-default"><</button>');
            const $fwd = $('<button type="button" class="btn btn-default">></button>');    
            const $info = $('<button type="button" class="btn btn-default"></button>');
            
            $info.on('change',() => {
                const inx =  im.currentPhotoInx;
                $info.text((inx+1) + '/' + im.photos.length);
                $img.attr('src', src(im.photos[inx]+'_thumbnail'));
            });

            function imgMove(direction) {
                im.currentPhotoInx = Math.abs(im.currentPhotoInx+direction) % im.photos.length;
                const fileHash = im.photos[im.currentPhotoInx];
                im.properties = properties[fileHash];
                im.currentPhotoHash = fileHash;
                $info.trigger('change')
            }

            $back.on('click', ()=>imgMove(-1));
            $fwd.on('click', ()=>imgMove(1));
            
            $group.append($back);
            $group.append($info);
            $group.append($fwd);
            $imgDiv.append($group);
            $info.trigger('change');

        }
        
        $div.on('click', function(e) {
            
            if ( !e.ctrlKey ) {
                self.action('deselect');
            }
            
            const $img = $(this).find('img:first');
            const id = im.file_hash;
            
            if (self.selectedImages[id]) {
                $div.removeClass('selected');
                delete self.selectedImages[id];
            }
            else {
                $div.addClass('selected');
                self.selectedImages[id] = $img;
                if (self.onMediaSelected) {
                    self.onMediaSelected(im);
                }
            }
            self.onMediaSelectionChanged && self.onMediaSelectionChanged();
        });

        $imgDiv.append($img);
        $div.append($imgDiv);

        this.op.processAttributeValues && $prop.html(this.op.processAttributeValues(im));

        $div.append($prop);

        this.$imageContainer.append($div);

        if (this.select_all === true) {
            $div.addClass('selected');
            this.selectedImages[im.file_hash] = $img;
        }

        im.$img = $img;
        im.$div = $div;
    }
}

export default MediaScroller;