/**
 * 
 * @param {<object>} op 
 */

 import 'patternfly-bootstrap-treeview'

 /**
  * 
  * @param {object} op 
  * @param {boolean} [op.triggerChange = true] trigers change event on $el on node select
  * @param {boolean} [op.hideMissedItemsOnSearch = true]
  * @param {string} op.valueIdKey tree item key used to compare a value to when calling val function
  */

 const TreeView = function (op = {}) {

    if (op.triggerChange === undefined) {
      op.triggerChange = true;
    }

    if (op.hideMissedItemsOnSearch === undefined) {
      op.hideMissedItemsOnSearch = true;
    }

    if (!op.valueIdKey) {
      op.valueIdKey = '_item_id';
    }

    this.op = op;

    const $el = $('<div/>');
    
    const $treeEl = $('<div/>',{class:'treeview-pf-hover treeview-pf-select'});

    const t = op.imports.t;

    if (op.imports.SearchInput) {
      const searchInput = new op.imports.SearchInput.default({
          placeholder: t`Iskanje`,
          onChange: onSearchQueryChange,
          onClear: () => {
            /* 
              recreate tree on clearSearch - hiding and again showing the node elements didn't quite work beacause you should take care of current node state.visbile (and possible parent expanded?) 
              if this was handled uncorrectly the tree went out of sync when expanding the nodes (children were always shown/hidden)
            */
            this.$tree.treeview('remove');
            this.$tree = createTree(this);
            return;
            this.$tree.treeview('clearSearch');
            this.$tree.treeview('getNodes').map(node => {
              console.log('node', node)
              node.state.visible && node.$el.show();
            });
          }
      });

      $el.append(searchInput.$el());
    }

    $el.append($treeEl);

    this.$el = () => $el;

    this.$treeEl = $treeEl;

      //https://www.patternfly.org/v3/components/patternfly/dist/tests/bootstrap-treeview.html
    this.icons = {
      collapseIcon: "fa fa-angle-down",
      expandIcon: "fa fa-angle-right",
      nodeIcon: "fa fa-folder"
    };
            /*checkedIcon = "fa fa-check-square-o",
            uncheckedIcon = "fa fa-square-o",
            partiallyCheckedIcon = "fa fa-check-square",
            loadingIcon = "glyphicon glyphicon-hourglass";
            */

      this.data = op.data || [];

      this.$tree = createTree(this);

      op.$parent && op.$parent.append($el);

      const self = this;

      function onSearchQueryChange(value) {
        const options = {
          ignoreCase: true,
          exactMatch: false,
          revealResults: true
        };

        if (value.length < 3) return;
        
        const results = self.$tree.treeview('search',value,options);    
        
        if (op.hideMissedItemsOnSearch) {
          
          const resultRootIds = new Map();
          
          results.map(r => {
            const splitted = r.nodeId.split('.');
            let sid = '';
            for (let i=0; i<splitted.length-1; i++) {
              sid = sid + splitted[i];
              resultRootIds.set(sid);
              sid = sid + '.';
            }
          });

          const nodes = self.$tree.treeview('getNodes');
          for (const node of nodes) {
            if (!node.state.visible) continue;
            if (node.selectable && !node.searchResult) {
              node.$el.hide();
            }
            else if (!node.searchResult) {
              
              if (!resultRootIds.has(node.nodeId)) {
                node.$el.hide();
              }
              
            }
            else {
              node.$el.show();
            }
          }
        }
      }

}

function createTree(self) {
  return self.$treeEl.treeview({
    collapseIcon: self.icons.collapseIcon,
    data: self.data,
    expandIcon: self.icons.expandIcon,
    nodeIcon: self.icons.nodeIcon,
    showBorder: false,
    levels: self.op.levels || 1,
    onNodeSelected: (event, data) => {
      self.op.onNodeSelected && self.op.onNodeSelected(event,data,self.$tree);
      self.op.triggerChange && !self.op.reinitTreeWithDataValues && self.$el().trigger('change');
    },
    preventUnselect: self.op.preventUnselect !== undefined ? self.op.preventUnselect : false
  });
}

TreeView.prototype.val = function (value) {
    
    if (value !== undefined) {

      if (this.op.reinitTreeWithDataValues) {
        this.$tree.treeview('remove');
        this.data = value;
        this.$tree = createTree(this);
        this.op.triggerChange && this.$el().trigger('change');
        return;
      }

      if (Array.isArray(value)) {
        value = value[0];
      }

      if (!value) {
        const nodes = this.$tree.treeview('getSelected');
        nodes.map(node => this.$tree.treeview('unselectNode', node, { silent: false } ));
        return;
      }

      const nodeId = value.nodeId || value;

      const key = this.op.reinitTreeWithDataValues ? 'nodeId' : this.op.valueIdKey;

      const nodes = this.$tree.treeview('getNodes');

      const node = nodes.find(n => n[key] == nodeId);

      if (node) {
        this.$tree.treeview('selectNode', node, { silent: false } );
      }
    }

    if (this.op.reinitTreeWithDataValues) {
      return this.data;
    }
    else {
      const selected = this.$tree.treeview('getSelected');
      return selected.length>0 ? selected[0][this.op.valueIdKey] : null;
    }
}

TreeView.prototype.getSelectedNode = function (value) {
  return this.$tree.treeview('getSelected');
}

export default TreeView;