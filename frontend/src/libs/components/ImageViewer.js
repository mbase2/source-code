import globals from '../../app-globals';
import * as exports from '../exports';

export default async (op) => {
    const [   
        ModalDialog,
        Map,
        mutils,
        MediaScroller
    ] = await Promise.all([
        exports.ModalDialog(),
        exports.Map(),
        exports.mutils(),
        exports.MediaScroller()
    ]);

        const $container = $('<div/>');

        let imageMap = null;

        const {t} = globals.t;
        
        const $left =  $('<div style="float: left; width:450px"/>');
        const $right = $('<div style="float: right; height:100%; width:calc(100% - 450px); max-height: calc(100vh - 187px); border-left: solid black 1px; padding:10px"/>');
        
        const $map = $('<div style="height:80vh"/>');
        
        const $mediaScroller =  $('<div style="height:100%; max-height: calc(80vh - 300px); overflow-y: auto"/>');

        $left.append($mediaScroller);
        
        $right.append($map);
        
        $container.append($left);
        $container.append($right);

        let selectedImage = null;
    
        const mediaScroller = new MediaScroller.default({
            mediaRoot: op.mediaRootFolder,
            scope:'unprocessed',
            fetch: async (offset, scope = '/') => {
                return op.data
            },
            containerWidth: '420px',
            
            onMediaSelected: mediaData => {
                const filePath = op.mediaRootFolder + mutils.filePath(mediaData.currentPhotoHash || mediaData.file_hash);
                
                const image = new Image()
                image.src = filePath

                image.onload = function() {
                    console.log(image)
                    mediaData.properties = {
                        width: image.width,
                        height: image.height
                    }
                    imageMap = Map.leafletPreview(imageMap, $map, op.mediaRootFolder + mutils.filePath(mediaData.currentPhotoHash || mediaData.file_hash), mediaData.properties);
                }
            },
            onDataFetched: (res, added) => {    //highlight selected image
                //selectedImage = added.find(im => im.file_hash === op.selectedMedia.file_hash);
            }
        });

        $mediaScroller.append(mediaScroller.$el());

        const modal = new ModalDialog.default({
            size: 'modal-xxl',
            onShown:()=>{
                
                if (selectedImage) {
                    selectedImage.$div.trigger('click');
                    selectedImage.$div[0].scrollIntoView();
                }
                 
                selectedImage = null;
            }
        });
        //modal.$body.css('height', '90vh');
        modal.$body.html($container);
        modal.show();

}