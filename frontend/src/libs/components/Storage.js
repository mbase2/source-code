/**
 * $.extend and store the op.data
 * 
 * @param {object} op 
 * @param {object} op.data 
 * @param {boolean} op.deep use "deep" $.extend defaults to false
 */

const Storage = function (op = {}) {
    this.deep = op.deep;
    this.data = {};
    this.op = op;
    op.data && this.store(op.data);
    this.readOnly = op.readOnly === true;
}

Storage.prototype.store = function (data) {

    const isArray = Array.isArray(data);

    if (isArray === false) {
        const primitives = ["undefined", "boolean", "number", "string", "bigint", "symbol"];
        const type = typeof data;
        if (primitives.find(primitive => primitive === type)) {
            this.data = data;
            return;
        }

        /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
            // The following are confusing, dangerous, and wasteful. Avoid them.
            typeof new Boolean(true) === 'object'; 
            typeof new Number(1) === 'object'; 
            typeof new String('abc') === 'object';
        */

        if (typeof data === 'object') {
            const confusing = [Boolean, Number, String];
            const constructor = data === null ? null : data.constructor;
            if (confusing.find(confuser => constructor === confuser)) {
                this.data = data;
                return;
            }
        }

    }
    
    if (this.deep === true) {
        this.data = $.extend(true, isArray ? [] : {}, data);
    }
    else {
        this.data = $.extend(isArray ? [] : {}, data);
    }
}

Storage.prototype.val = function (data) {
    if (data !== undefined) {
        !this.readOnly && this.store(data);
    }

    if (this.op.returnNullWhenEmpty) {
        if (this.data === undefined || (typeof this.data === 'object' && _.isEmpty(this.data))) {
            return null;
        }        
    }
    
    return this.data;
}

export default Storage;