import * as exports from '../../libs/exports';
/**
 */

export default async op => {
    const [
        Select2,
        ModalDialog
    ] = await Promise.all([
        exports.Select2(),
        exports.ModalDialog()
    ]);

    const _keys = op.keys || ['_no_key'];

    const _modalDialog = new ModalDialog.default(op.modalDialogOptions || {});

    const _modalDialogSelectors = {};
    
    function initModalDialogSelect(data) {

        const multipleSelectsOptions = op.keys ? (op.modalDialogSelectOptions || {}) : {
            _no_key: op.modalDialogSelectOptions
        };

        if (data && !op.keys) {
            data = {
                _no_key: data
            }
        }
        
        for (const key of _keys) {
            
            const options = multipleSelectsOptions[key];

            if (data && data[key]) {
                options.data = data[key]
            }

            const modalDialogSelect = new Select2.default(options);
            _modalDialog.$body.append(modalDialogSelect.$el());
            _modalDialogSelectors[key] = modalDialogSelect;
        }
        
    }

    op.footerButtons && Object.keys(op.footerButtons).map(key => {
        const btn = op.footerButtons[key];
        _modalDialog.$footer.append(btn.$el());
    });

    function val(a) {
        if (a) {
            
            if (!op.keys) {
                a = {
                    '_no_key': a
                }
            }

            Object.keys(a).map(key => {
                _modalDialogSelectors[key] && _modalDialogSelectors[key].val(a[key]);
            });
        }
        else {
            const rvals = {}
            _keys.map(key => rvals[key] = _modalDialogSelectors[key] && _modalDialogSelectors[key].val());
            return op.keys ? rvals : rvals['_no_key'];
        }
    }

    return Object.freeze({
        initModalDialogSelect,
        val: val,
        dlg: _modalDialog,
        show: () => _modalDialog.show(),
        hide: () => _modalDialog.hide()
    });
}