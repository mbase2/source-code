/**
 * Patternfly tabs
 */

const Tabs = function (op = {}) {
    const items = op.items || [];    
    const $ul = $(`<ul class="nav nav-tabs"/>`);
    const self = this;
    items.map(item => {
        const $li = $(`<li/>`);
        const $a = $(`<a href="#">${item}</a>`);

        $a.click(function() {
            $ul.find('li').removeClass('active');
            $(this).parent().addClass('active');
            if (op.onSelect !== undefined) {
                op.onSelect(self.val());
            }
        })

        $li.append($a);
        $ul.append($li);
    });

    this.$el = () => $ul;
};

Tabs.prototype.val = function (value) {
    if (value !== undefined) {
        this.$el().find(`li:nth-child(${value + 1})`).find('a').click();
        return value;
    }
    return this.$el().find(`li.active`).index();
}

export default Tabs;