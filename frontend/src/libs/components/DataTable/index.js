import 'datatables.net'
import 'datatables.net-scroller-dt'
import 'datatables.net-select-dt'
import 'datatables.net-keytable-dt'
import 'datatables.net-fixedcolumns-dt'
import 'datatables.net-buttons/js/buttons.html5'
import 'datatables.net-dt/css/jquery.dataTables.css';
import 'datatables.net-scroller-dt/css/scroller.dataTables.min.css'
import 'datatables.net-select-dt/css/select.dataTables.min.css'
import 'datatables.net-keytable-dt/css/keyTable.dataTables.min.css'
import 'datatables.net-fixedcolumns-dt/css/fixedColumns.dataTables.min.css'
import './css/datatable.css'

/**
 * @param {object} op 
 * @param {string} op.css adds class name(s) to table element (if more class names is added they are to be separated by a space)
 * @param {array} op.header array of objects {key, label} where key is the key in table data object and label is header label or array of header labels
 * @param {function} op.onEdit if function is defined column with fa-edit icon is added and this function is executed when the icon is pressed
 * @param {function} op.onRowClicked 
 * @param {boolean} op.scroller if true table is scrollable, else table is paginated
 * @param {string} op.scrollY scrollable table property, defaults to 420
 * @param {boolean} op.scrollCollapse scrollable table property, defaults to true
 */

const DataTable = function (op) {
    const defaultClass = op.defaultClass === undefined ? "display cell-border" : op.defaultClass;
    const $el = $(`<table class="${defaultClass}" style="width:100%"></table>`);

    this.op = op;

    this.iconActions = {}

    this.op.__icons && this.op.__icons.map(icon => {
        this.iconActions[icon.action] = icon.onClick;
    });

    this.rowEditOptions = op.rowEditOptions;

    if (op.css) $el.addClass(op.css);

    if (op.scroller && op.scroller === true) {
        !op.doNotAddNoWrapClass && $el.addClass("nowrap");
        op.deferRender = op.deferRender === undefined ? true : op.deferRender;
        op.scrollY = op.scrollY === undefined ? 420 : op.scrollY;
        op.scrollCollapse = op.scrollCollapse === undefined ? true : op.scrollCollapse;
    }

    if (typeof op.header[0] === 'object' && op.header[0] !== null) {
        op.columns = op.header.map(ob => {
            const cdef = {data: ob.key, title: ob.label};
            if (op.hideColumns && op.hideColumns.indexOf(cdef.data) !== -1) {
                cdef.visible = false;
                cdef.searchable = true;
            }
            return cdef;
        });
    }
    else {
        op.columns = op.header.map(title => ({title: title}));
    }

    this.editable = op.onEdit ? true : false;
    this.delatable = op.deletable === true ? true : false;

    if (op.exportButtons === true) {
        op.dom = 'frtBip';
        op.buttons = [
            { extend: 'copy', className: 'btn btn-default' },
            { extend: 'csv', className: 'btn btn-default' }
        ]
    }

    if (!op.columnDefs) op.columnDefs = [];

    const icons = this._icons(this.editable, this.delatable);

    if (icons) {

        /**
         * //this works but I don't need it currently 
            op.columnDefs.push([
                {
                    render: function ( data, type, row ) {
                        console.log(data, type, row)
                        return '<a href="#" class="table-edit-action">'+data+'&nbsp;<span class="fa fa-edit"></span></a>'
                    },
                    "targets": 0
                }
            ]);
        */

        const editColumnHeader = Object.assign(op.editColumnHeader || {}, {data:"_mbase2_edit"});

        op.columns.unshift(editColumnHeader); //add column with edit icon
        op.columnDefs.push({
            "targets": 0,
            "orderable": false
        });
        op.order = [1, 'asc'];
                
        op.data.map((row, index) => {
            row._mbase2_edit = this.rowEditOptions ? this._icons(...this.rowEditOptions(row)) : icons;
        });
    }

    Object.keys(op.columnDefsByColumnName || {}).map(key => {
        const targetIndex = op.columns.findIndex(c => c.data === ('t_' + key) || c.data === key);
        if (targetIndex !== -1) {
            op.columnDefs.push(Object.assign({},op.columnDefsByColumnName[key],{targets: targetIndex}));
        }
    });

    if (op.orderBy) {
        const index = op.columns.findIndex(c => c.data === op.orderBy[0]);
        if (index !==-1) {
            op.order = [index, op.orderBy[1] || 'asc'];
        }
    }

    const self = this;

    if (op.customSearchInput) {
        op.dom = '<"top">rt<"bottom"i><"clear">';
        
        const searchInput = new op.customSearchInput.Constructor(Object.assign(op.customSearchInput.options || {placeholder:'Search'}, {
            onChange: (value) => {
                self.table.search(value).draw();
            }
        }));
        const $searchInput = searchInput.$el();
        $searchInput.css('padding-bottom', '10px');
        op.$container.append($searchInput);    
    }

    op.$container.append($el);

    if (op.titleAttr) {
        const targetTitles = [];

        Object.keys(op.titleAttr).map(key => {
            if (op.titleAttr[key]) {
                const targetIndex = op.columns.findIndex(c => c.data === ('t_' + key) || c.data === key);
                if (targetIndex !== -1) {
                    const textTitle = op.titleAttr[key];
                    targetTitles.push({targetIndex, text: op.titleAttr[key]});
                    op.columns[targetIndex].title = `<span title="${textTitle}" class="info-tip pficon pficon-info"></span>&nbsp;` + op.columns[targetIndex].title;
                }
            }
        });
    }

    const table = this.table = $el.DataTable(op);

    /**
     *  const $fullScreen = $('<button>abc</button>');
        $fullScreen.on('click', ()=>table.table().container().requestFullscreen())
        $(table.table().container()).find('#DataTables_Table_0_filter').prepend($fullScreen);
     */

    if (op.onEdit || op.deletable) {

        $el.find('tbody').on('click', 'a.table-action', function () {onTableActionClicked.call(this)});

        function onTableActionClicked() {
            const $a = $(this);
            const tr = $a.closest('tr')[0];
            const row = table.row(tr);
            var data = row.data();
            //row.id()
            const action = $a.data('action');
            if (action === 'edit') {
                op.onEdit && op.onEdit(data, row.id(), row.index());
            }
            else if (action === 'delete') {
                op.deletable && op.onDelete && op.onDelete(data, row);
            }
            else if (self.iconActions[action]) {
                self.iconActions[action](data, row.id(), $a);
            }
            
        } 
    }

    if (op.onRowSelected) {
        $el.find('tbody').on('click', 'tr', function () {
            var data = table.row( this ).data();
            op.onRowSelected(data);
        } );
    }

    this.$el = () => $el;
}

DataTable.prototype.destroy = function() {
    this.table.clear();
    this.table.destroy(true);
    return true;
}

DataTable.prototype.getColumnsMap = function() {
    const dataTableColumns = {};
    this.table.settings()[0].aoColumns.map((c, inx) => dataTableColumns[c.data]=inx);
    return dataTableColumns;
}

DataTable.prototype.getHeaderTitles = function() {
    return this.table.settings()[0].aoColumns.map(c => c.sTitle);
}

DataTable.prototype.getHeaderKeys = function() {
    return this.table.settings()[0].aoColumns.map(c => c.data);
}

DataTable.prototype.getCell = function(headerKey, rowInx) {
    if (!rowInx) return null;
    const c = this.table.settings()[0].aoColumns.find(c => c.data == headerKey);
    if (!c) return null;
    return this.table.cell(rowInx,c.idx);
}

DataTable.prototype.getRowCellsFormattedData = function(rowInx) {
    const res = [];
    this.table.settings()[0].aoColumns.map(c => {
        if (c.data !== '_mbase2_edit') {
            res.push({
                    title: c.sTitle,
                    value: this.table.cells(rowInx,c.idx).data()[0],
                    data: c.data
            });
        }
    });
    return res;
}

DataTable.prototype.getData = function() {
    const rowsData = this.table.rows().data();
    const len = rowsData.length;
    const data = [];
    for (let i=0; i < len; i++) {
        data.push(rowsData[i]);
    }
    return data;
}

/**
 * Helper function to construct table icons cell
 * @param {boolean} editable 
 * @param {boolean} deletable 
 */
DataTable.prototype._icons = function (editable, deletable) {

    const icons = [];

    const htmlIcon = icon => `<a href="#" title="${icon.title || ''}" style="${icon.style || ''}" class="table-action ${icon.additionalClasses || ''}" data-action="${icon.action}"><span class="fa fa-${icon.class}"></span></a>`;

    if (this.op.__icons) {
        this.op.__icons.map(icn => {
            if (icn.prepend) {
                icons.push(htmlIcon(icn));
            }
        });
    }
    
    if (editable) {
        icons.push('<a href="#" class="table-action" data-action="edit"><span class="fa fa-edit"></span></a>');
    }

    if (deletable) {
        icons.push('<a href="#" class="table-action" data-action="delete"><span class="fa fa-trash"></span></a>');
    }

    if (this.op.__icons) {
        this.op.__icons.map(icon => {
            if (!icon.prepend) {
                icons.push(htmlIcon(icon));
            }
        });
    }

    return icons.join('&nbsp;');
}

DataTable.prototype.setData = function(data) {
    const icons = this._icons(this.editable, this.delatable);
    icons && data.map((row, index) => {
        row._mbase2_edit = this.rowEditOptions ? this._icons(...this.rowEditOptions(row)) : icons;
    });
    
    const datatable = this.table;
    datatable.clear();
    datatable.rows.add(data);
    datatable.draw(false); //Use draw(false) to stay on the same page after the data update.
}

DataTable.prototype.val = function(value) {
    if (value!== undefined) {
        const indexes = this.table.rows().indexes();
        for (let i=0; i<indexes.length; i++) {
            const row = this.table.row(indexes[i]);
            if (row.data().id == value) {
                row.select();
                return row;
            }
        }
        return;
    }
    
    return this.table.rows( { selected: true } ).data();
}

export default DataTable;
