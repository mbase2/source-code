define(function(){
	/**
	 * @param params {object} component parameters
	 * @param params.data {array<object>} initial data
	 * @param params.options {object} component options
	 */
	return function(options = {}) {

		var data = options.data || [];
		const op = options;
		const $el = $('<div/>');
		const badge = op.badge || op.label;
		const required = op.required ? 'required' : '';

		this.hasError = false;

		let $badge;
		
		if (badge){
			$badge = $(`<span class="label label-primary mbase2-label ${required}">`+badge+'</span>');
			$el.append($badge);
			op.helpIconText && $el.append(`&nbsp;<span class="pficon pficon-help" title="${op.helpIconText}"></span>`);
		}
		
		var key;
		
		if (data && data.key!==undefined){
			key=data.key;
			data=data.data;
		}
    
		if (op.valueTextPairData===true) {
		var _data=[];
		$.each(data,function(value,text){
			_data.push({value:value,text:text});
		});
		data=_data;
		}
		else if (Array.isArray(data[0])) {
			data = data.map(d => ({value: d[0], text: d[1], additional: d[2]}));
		}
		
		var $select=$('<select/>');
		var options="";
		for (var i=0,len=data.length;i<len;++i){
			var v=data[i];
			var val=v;
			var vtext=v;
      		if (op.textField!==undefined) v.text=v[op.textField];
			if (v.value!==undefined) val=v.value;
			if (v.text!==undefined) vtext=v.text;
			var optionData='';
			if (v.data!==undefined) optionData='data-data=\''+v.data+'\'';
			var additionalData = '';
			if (v.additional!==undefined) additionalData='data-additional=\''+v.additional+'\'';
			options+='<option '+optionData+ ' ' + additionalData + ' value="'+val+'">'+vtext+'</option>';
		}
		$select.html(options);
		
		if (op.multiple===true){
			$select.attr('multiple',"multiple");
		}
		
		$el.append($select);
		
		var selOp={width: '100%'};
		if (op.templateResult) selOp.templateResult=op.templateResult;
		if (op.templateSelection) selOp.templateSelection=op.templateSelection;
    
    if (op.placeholder) {
      selOp.placeholder=op.placeholder;
    }
    
    if (op.allowClear) {
      selOp.allowClear=true;
      selOp.placeholder=""; //workaround for some bug
    }
    
		if (op.tags===true){
		  selOp.tags=true;
	
		  if (op.createTag===undefined){
			selOp.createTag=function (params) {
					return {
					  id: params.term,
					  text: params.term,
					  newOption: true
					};
				  };
		  }
		  else{
			selOp.createTag=op.createTag;		
		  }
		  
		  selOp.templateResult = op.templateResult || function (data) {
				  var $result = $("<span></span>");
			  
				  $result.text(data.text);
			  
				  if (data.newOption) {
					$result.append(" <em>"+op.newOptionText+"</em>");
				  }
			  
				  return $result;
		  };
		}
	
		if (op.templateSelection) {
			selOp.templateSelection=op.templateSelection;
		}
		
		$select.select2(selOp);

		if (op.read_only) {
			$select.prop('disabled', true);
		}
    
		if (op.required===true) {
		  $el.data('required',"1");
		}

		this.onSelectHook = null;
		
		var onSelect=op.onSelect;
		$select.off('select2:select');
		$select.on('select2:select',function (e) {
			if (this.hasError && required && this.val()) {
				this.hasError = false;
				if ($badge) {
					$badge.removeClass('label-danger');
					$badge.addClass('label-primary');
				}
			}

			if (op.multiple && op.selectionSort === false) {
				//https://stackoverflow.com/questions/40763223/select2-multiselect-how-to-stop-automatic-sorting
				var element = e.params.data.element;
				var $element = $(element);

				$element.detach();
				$(this).append($element);
				$(this).trigger("change");
			}

			onSelect && onSelect(e);

			this.onSelectHook && this.onSelectHook(e);
		});

		op.onSelecting && $select.on('select2:selecting', op.onSelecting);

		op.onUnselect && $select.on('select2:unselect', op.onUnselect);

		$select.off('select2:close');
		op.onSelect2close && $select.on('select2:close', op.onSelect2close);
		
		var selectedValue=op.selectedValue;
		if (selectedValue===undefined) selectedValue=null;
		$select.val(selectedValue).trigger("change");
		
		if (selectedValue!==undefined && onSelect !== undefined && !op.silent) {
			$select.val() == selectedValue && onSelect(selectedValue);
		}

		this.emptyElement=function(){
			$el.empty();
		};
		this.getDataKey=function(){
			return key;
		};

		this.getOptionByValue = function(value) {
			return $select.find('option[value="'+value+'"]')[0];
		}

		this.getAdditionalDataForSelectedOption = function() {
			const op = this.getOptionByValue($select.val());
			if (!op) return null;
			const additional = op.attributes.item('additional');
			if (!additional) return null;
			return additional.value;
		}

		this.val=function(a,silent){
			if (a!==undefined){
				if (op.tags===true && $select.find('option[value="'+a+'"]').length===0){
				  var $opt=$('<option>'+a+'</option>');
				  $opt.attr('data-select2-tag','"true"');
				  $opt.val(a);
				  $select.append($opt);
				}
				$select.val(a).trigger("change");
				if (silent!==true && onSelect!==undefined) {
					$select.val() == a && onSelect(a); 
				}
			}
			return $select.val();
		};
		this.text=function(a,silent){
			if (a!==undefined){
				var id=$select.find('option').filter(function () { return $(this).html() == a; }).val();
				this.val(id,silent);
			}
			else{
				var id=$select.val();
				
				if (Array.isArray(id)) {
					return id.map(id => this.getTextFromID(id));
				}

				return this.getTextFromID(id);
			}
		};
		this.getData=function(){
			return $select.select2('data');
		};
		
		this.$select=$select;
		
		this.disabled=function(disabled){
			$select.prop('disabled',disabled);
		};
		
		this.reinit=function(data,textf){
			if (data.length===0) return false;
			if (textf===undefined) textf='text';
			var valf='id';
			if (data[0][valf]===undefined) valf=textf;
			var $tmpSelect=$('<select/>');
			
			if (data[0][valf] && data[0][textf]){
				$.each(data,function(key,op){
					$tmpSelect.append($('<option/>',{value:op[valf]}).text(op[textf]));
				});
			}
			else if (Array.isArray(data[0])) {
				data.map(d => {
					$tmpSelect.append($('<option/>',{value:d[0]}).text(d[1]));
				});
			}
			else{
				$.each(data,function(key,op){
					$tmpSelect.append($('<option/>',{value:op}).text(op));
				});
			}
			
			$select.html($tmpSelect.html()).val(null).trigger('change');
			return true;
		};
		
		this.getTextFromID=function(id){
			return $select.find('option[value="'+id+'"]').text();
		};
    
    this.$el=function(){
      return $el;
	};
	
	this.validate = function() {
		if (required && !this.val()) {
			this.hasError = true;
			if ($badge) {
				$badge.addClass('label-danger');
				$badge.removeClass('label-primary');
			}
			return false;
		}
		return true;
	};
    
    this.setError=function(className){
      this.$el().find('.select2.select2-container').addClass(className);
    };
    
    this.addOption=function(op){
      if (op.value===undefined) op={value:op,text:op};
      $select.append($('<option/>',{value:op.value}).text(op.text));
    };
		
		if (op.initialValue!==undefined){
			this.val(op.initialValue);
		}
		
	};
});