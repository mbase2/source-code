
	var TCheckBox=function(op){
		const $el = $('<div/>',{style: "margin-left:20px"});
		
		if (op.wrapperClass) {
			$el.addClass(op.wrapperClass);
		}

		const label = op.label;
	
		this._$el=$el;
		
		if (!op) op={};
			$el.empty();
		$el.addClass('checkbox');
		const $cb = this.$cb = $('<input type="checkbox">');

		if (op && op.read_only) {
			$cb.prop('disabled', true);
		}

		$el.append($cb);
		$el.append(label);
    
		var onClick=op.onClick;
		if (onClick) {
				$cb.click(function(){
				onClick($cb.prop('checked'));
			});
		}
		
		var checked=op.checked;
		
		if (checked) {
			$cb.prop('checked',checked);
		}
    
    this.$el=function(){
      return $el;
    };
};

TCheckBox.prototype.val = function(a){
	if (a) {
		this.$cb.prop('checked',a);
	}
	return this.$cb.prop('checked') ? 1 : 0;
};
  
export default TCheckBox;