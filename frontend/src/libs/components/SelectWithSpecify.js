import { Select2, Inputs } from "../exports";
import globals from '../../app-globals';

const SelectWithSpecify = function(op={}) {
    const $el = $('<div/>', {style:'display: flex'});
    this.$el = () => $el;

    this.op = op;
    
    const $selectDiv = $('<div/>', {style: 'width: 100%'});
    const $specifyDiv = this.$specifyDiv = $('<div/>', {style: 'margin-left: 15px; width: 100%'});

    $el.append($selectDiv);
    $el.append($specifyDiv);

    const selectOptions = Object.assign({}, op);

    const onSelectionChanged = (event, onSelect=true) => {
        
        if (!event) return;

        this.$specifyDiv.hide();
        const val = this.val();

        op.onChange && op.onChange();

        if (!val) return;

        if (val.specify !== undefined) {
            setTimeout(()=>this.$specifyDiv.show(), 100);
        }
    }

    selectOptions.onUnselect = (event) => {
        onSelectionChanged(event, false);
        op.onUnselect && op.onUnselect(event);
    }

    selectOptions.onSelect = (event) => {
        onSelectionChanged(event);
        op.onSelect && op.onSelect(event);
    }
    
    this.select = new op.imports.Select2.default(selectOptions);
    $selectDiv.html(this.select.$el());

    this.specify = new op.imports.Inputs.Input(op.input || {
        label: globals.t`Specify`+':'
    });

    $specifyDiv.hide();

    $specifyDiv.html(this.specify.$el());
}

SelectWithSpecify.prototype.specifyValue = function (selectVal) {
    let specify = false;

    if (Array.isArray(selectVal)) {
        for (const value of selectVal) {
            if (this.select.getOptionByValue(value).attributes.item('additional').value.startsWith('_specify')) {
                specify = true;
                break;
            }
        }
    }
    else if (this.select.getOptionByValue(selectVal).attributes.item('additional').value.startsWith('_specify')) {
        specify = true;
    }

    return specify;
}

SelectWithSpecify.prototype.val = function (value) {
    if (value !== undefined) {
        
        if (typeof value === 'object' && value !== null && !Array.isArray(value)) {
            if (this.specifyValue(value.select)) {
                this.select.val(value.select);
                this.specify.val(value.specify);
                this.$specifyDiv.show();
                return;
            }
            else {
                value = value.select;
            }
        }
        
        this.select.val(value);
        this.$specifyDiv.hide();

        return;
    }

    const selectVal = this.select.val();
    
    if (selectVal) {

        if (this.specifyValue(selectVal)) {

            const specifyVal = this.specify.val();
            return {
                select: selectVal,
                specify: specifyVal
            };
        }
    }

    return selectVal;
}

SelectWithSpecify.imports = async function () {
    const [
        _Select2,
        _Inputs
    ] = await Promise.all([
        Select2(),
        Inputs()
    ]);

    return {
        Select2: _Select2, 
        Inputs: _Inputs
    };
}

export default SelectWithSpecify;