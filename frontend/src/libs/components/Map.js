
  import 'leaflet-easybutton'
  import 'leaflet-easybutton/src/easy-button.css'

  import 'leaflet-fullscreen/dist/Leaflet.fullscreen.min.js'
  import 'leaflet-fullscreen/dist/leaflet.fullscreen.css'

  import { proj4defs, t } from '../../app-globals';
  import toggleTrackDetail from '../../helpers/toggleTrackDetail';

  //https://gis.stackexchange.com/questions/197882/is-it-possible-to-cluster-polygons-in-leaflet
	L.PolygonClusterable = L.Polygon.extend({
	  _originalInitialize: L.Polygon.prototype.initialize,
	  
	  initialize: function (bounds, options) {
		this._originalInitialize(bounds, options);
		this._latlng = this.getBounds().getCenter();
	  },
	  
	  getLatLng: function () {
		return this._latlng;
	  },
	  
	  // dummy method.
	  setLatLng: function () {}
	});

  const pdefs = proj4defs;

  const _highlightProps = {
    color: '#c71585',
    fillColor: '#c71585',
    weight:2,
    fillOpacity:0.55
  };

  const _tlm_grid_tracks = {};

  const _defaultProps = {
    fillOpacity: 0.5,
    color: '#c71585',
    fillColor: '#c71585',
    weight:1
  }

  /**
   * 
   * @param {boolean} centerPopupOnOpen 
   */
  var Map=function(op = {}){
    this.op=op;
    const center = this.op.center || [46.119944, 14.81533];
    const zoom = this.op.zoom || 8;

    this.$el = () => op.$container;

    this.$el().css('font-size','11px'); //temporary solution for patternfly css interference with leaflet font size

    const mapOptions = { zoomControl: false, preferCanvas: true};

    if (op.zoomDelta !== undefined) mapOptions.zoomDelta = op.zoomDelta;
    if (op.zoomSnap !== undefined) mapOptions.zoomSnap = op.zoomSnap;

    const map = this.map = L.map(this.op.$container[0], mapOptions).setView(center, zoom);

    const baseMaps = {
      "OpenStreetMap": L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      })
    };

    /*
    baseMaps.DOF = L.tileLayer.wms("http://prostor4.gov.si:80/ows2-m-pub/wms?", {
      layers:"SI.GURS.ZPDZ:DOF050",
      maxNativeZoom: 19,
      maxZoom: 23,
      attribution: '<a href="http://www.e-prostor.gov.si/dostop-do-podatkov/dostop-do-podatkov/#c501">Geodetska uprava Republike Slovenije</a>'
    });
    */

    const mbAttr = 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community';

    const mbUrl = 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';

    baseMaps['ESRI World Imagery'] = L.tileLayer(mbUrl, {
      id: 'esri',
      attribution: mbAttr
    });

    const overlays = {};

    L.control.layers(baseMaps, overlays).addTo(map);

    baseMaps['OpenStreetMap'].addTo(map);

    op.scaleControl && L.control.scale({
      position: 'bottomright'
    }).addTo(map);

    new L.Control.Zoom({ position: 'bottomright' }).addTo(map);

    map.addControl(new L.Control.Fullscreen({
      title: {
          'false': t`View Fullscreen`,
          'true': t`Exit Fullscreen`
      },
      position: 'topright'
    }));

    if (op.stateChangingButton) {
      map.stateChangingButton = L.easyButton({
        position:'topright',
        id:'grid-detail-switch',
        states: [{
          stateName: 'detail',
          icon:      'fa-th',
          title:     op.stateChangingButton.detail.title,
          onClick: (btn, map) => this.switchState(btn, map, 'grid')
        },
        {
          stateName: 'grid',        // name the state
          icon:      'fa-map-marker',               // and define its properties
          title:     op.stateChangingButton.grid.title,      // like its title
          onClick: (btn, map) => this.switchState(btn, map, 'detail')
        }
      ]
      }).addTo(map);
    }

	};

  Map.prototype.switchState=function(btn,map,state) {
    
  }
  
  Map.prototype.addSidebar=function(sidebarId){
    return L.control.sidebar(sidebarId).addTo(this.map);
  };

  Map.prototype.zoomToMarker = function (marker, data, moduleKey) {
    
    if (!marker.getLatLng) {
      if (marker.getBounds) {
        this.map.fitBounds(marker.getBounds());
        this.op.onMarkerClicked && this.op.onMarkerClicked(marker, [data]);
        marker.openPopup();
      }
      return;
    }

    if (!marker.getLatLng()) {
      this.map.closePopup();
      return;
    }
    
    const markerClusterGroup = this.layerGroup;

    const visibleParent = markerClusterGroup.getVisibleParent(marker); //returns marker if visible

    if (visibleParent === marker) {
        this.op.onMarkerClicked(marker, [data]);
        marker.openPopup();
    }
    else {
      if (this.map.getZoom()<this.map.getMaxZoom()) { //prevent endless loop in case visibleParent === marker condition is not reached (for example when we have two markers at the same spot)
        this.map.setView(marker.getLatLng(), this.map.getZoom()+1, true);
        setTimeout(() => this.zoomToMarker(marker,data),20)
      }
    }
    
}


  Map.prototype.getGridCellCoordinates = function(c, gridCellSize=1000) {
    
    if (!gridCellSize) gridCellSize=1000;

    c[0] = c[0] - gridCellSize/2.0;
    c[1] = c[1] - gridCellSize/2.0;

    const c0 = proj4(pdefs['3035'], pdefs['4326'], [c[1], c[0]]);
    return [c0,
        proj4(pdefs['3035'], pdefs['4326'], [c[1]+gridCellSize, c[0]]),
        proj4(pdefs['3035'], pdefs['4326'], [c[1]+gridCellSize, c[0]+gridCellSize]),
        proj4(pdefs['3035'], pdefs['4326'], [c[1], c[0]+gridCellSize]),
      c0].map(c => [c[1], c[0]]);
  }
  
  /**
   * 
   * Adds data array layer to map and assigns __marker reference to each row of the data
   * 
   * @param {Object[]} res coordinates, two types of input 
   * @param {string} [res[].geom]
   * @param {number} [res[]]
   * @param {string} moduleName module key
   * @param {Object} [props]
   * @param {string} props.grid_size grid size in meters
   * @param {string} props.color hex value of grid cell color (e.g. "#d211ec") 
   * @returns 
   */

  Map.prototype.addLayer = function(res, moduleName, isGrid, props={}) {

    if (moduleName==='tlm_tracks') {
      return this.addTrackLayer(res, moduleName, isGrid, props);
    }

    const layerGroup = this.layerGroup = isGrid ? L.layerGroup() : L.markerClusterGroup();

    const markers = [];

    const markerData = isGrid ? {} : [];

    res.map(r => {
      let c = null;
      if (r.geom) {
        const geom = JSON.parse(r.geom);
        c = geom.coordinates.reverse();
      }
      else if (r.c) {
        c = r.c;
      }

      const ckey = JSON.stringify(c);

      if (isGrid) {
        if (!markerData[ckey]) markerData[ckey] = [];
        markerData[ckey].push(r);
      }
      else {
        markerData.push({c,r});
      }
  });

  for (let [c,row] of Object.entries(markerData)) {

    let marker = null;

    if (isGrid) {
      c = JSON.parse(c);
    }
    else {
      c = row.c;
      row = row.r;
    }

    let data = row.data || row;

    if (!isGrid) {
      data = [data];
    }

    if (isGrid) {
      const cellc = this.getGridCellCoordinates(c, props && parseFloat(props.grid_size));
      const clsFun = L.Polygon; //clsFun = L.PolygonClusterable;
      
      //https://en.wikipedia.org/wiki/Alpha_compositing
      const layerOptions = {fillOpacity: 1.0 - 0.5/(data.length ? data.length : 1)};

      if (props && props.color) {
        layerOptions.fillColor = layerOptions.color = props.color;
      }

      marker = new clsFun(cellc, layerOptions);

      marker.on('popupopen', onPopupOpen);
      //marker.setStyle();
    }
    else {
      marker = L.marker(c);
    }

    marker.bindPopup();

    row.__marker = marker;
    marker.__data = data;

    if (this.op.onMarkerClicked && !this.op.centerPopupOnOpen) {// if centerPopupOnOpen===true onMarkerClicked is called after popup update
      marker.on('click', () => this.op.onMarkerClicked(marker, data, moduleName));
    }
    
    c && markers.push(marker) && layerGroup.addLayer(marker);

  }
    
    this.map.addLayer(layerGroup);

    const self = this;

    function onPopupOpen(e) {

      if (self.op.centerPopupOnOpen) {
        
        if (isGrid) {
          e.layer = e.target;
        }

        setTimeout(() => {
            e.popup.update();
            self.op.onMarkerClicked(e.layer,e.layer.__data, moduleName);
            setTimeout(() => {
                const popupPosition = e.popup.getLatLng();
                const mapCenter = self.map.getCenter();
                self.map.panTo({lat: mapCenter.lat, lng: popupPosition.lng});
            }, 300);
        }, 100);
      }

      self.op.onPopupOpened && self.op.onPopupOpened(e, moduleName);
    }

    if (this.op.centerPopupOnOpen || this.op.onPopupOpened) {
      !isGrid && layerGroup.on('popupopen', onPopupOpen); //popupopen doesn't work on layerGroup 
    }

    return {markers, group:layerGroup};
  }

  Map.prototype.addTrackLayer = function(res, moduleName, isGrid, props={}) {
    const layers = [];

    const group = new L.FeatureGroup();
    const p = props;
    const grid_size = parseFloat(p.grid_size);

    _defaultProps.color = _defaultProps.fillColor = p.color;

    const clsFun = L.Polygon; //clsFun = L.PolygonClusterable;

    res.map(r => {
      if (r.geom) {
        r.id = r.id || r.tlm_tracks_id;
        const feature = JSON.parse(r.geom);

        const track = new L.FeatureGroup();

        feature.coordinates.map(c => {
          const key = JSON.stringify(c);
          if (!_tlm_grid_tracks[key]) {
            _tlm_grid_tracks[key]={};
          }
          const cellc = this.getGridCellCoordinates([c[1],c[0]], grid_size);
          const gridCell = new clsFun(cellc, _defaultProps);
          gridCell.__key = key;
          track.addLayer(gridCell);
          _tlm_grid_tracks[key][r.id]=true;
        });

        r.__marker = track;
        track.__data = r;
        track.bindPopup();
        track.getPopup().on('remove', function() {
          
        });

        track.on('click', (e) => {

          const key = e.layer.__key;

          const ids = Object.keys(_tlm_grid_tracks[key]);

          const rows = res.filter(cr=>ids.indexOf(cr.id+'')!==-1);

          this.op.onMarkerClicked(track, rows, moduleName, (id, action, $span)=>{
            const row = res.find(r => r.id == id);
            const track = row.__marker;
            
            if (['show','hide'].indexOf(action)!==-1) {
              let show = false;
              if (action==='show') {
                show = true;
                $span.css('background-color','yellow');
                $span.data('action','hide');
              }
              else if (action==='hide') {
                show = false;
                $span.css('background-color','');
                $span.data('action','show');
              }
              
              highlightTrack(track,show);
            }
            else if (['detail','grid'].indexOf(action)!==-1) {
              toggleTrackDetail(row, this.map, $span);
            }
            
          });
        });
        group.addLayer(track);
        layers.push(track);
      }
    });

    group.addTo(this.map);

    return {markers: layers, group};
  }

  function highlightTrack(track, show=true) {
    track.__highlighted = show;
    if (show===false) {
      track.getLayers().map(layer => layer.setStyle(_defaultProps));
    }
    else {
      track.bringToFront();
      track.getLayers().map(layer => layer.setStyle(_highlightProps));
    }
  }

  export const leafletPreview = (map, $map, src, props) => {
    if (!props.height || !props.width) return null;
    if (!map) {
      map = L.map($map[0], {
          crs: L.CRS.Simple,
          minZoom: 1,
          maxZoom: 5,
          center: [0, 0],
          zoom: 1,
          zoomDelta: 0.25,
          zoomSnap: 0
      });

      L.easyButton('fa-download', function(btn, map){
        downloadImage();
      }).addTo(map);

      L.easyButton('<span style="font-size: 1.5em;">&target;</span>', function(btn, map){
        fitBounds();
      }, 'Reset to initial view').addTo(map);
    }
    
    function downloadImage() {
      var a = $("<a>")
          .attr("href", src + '/' + props.type)
          .attr("download", "img." + props.type)
          .appendTo("body");

      a[0].click();

      a.remove();
    }
                
    const h = props.height;
    const w = props.width;
    var southWest = map.unproject([0, h], map.getMaxZoom()-1);
    var northEast = map.unproject([w, 0], map.getMaxZoom()-1);
    var bounds = new L.LatLngBounds(southWest, northEast);

    map.eachLayer(function (layer) {
        map.removeLayer(layer);
    });

    //var bounds = [[0,0], [props.height, props.width]];
    var image = L.imageOverlay(src + '/' + props.type, bounds).addTo(map);

    function fitBounds() {
      map.fitBounds(bounds);
    }
    
    fitBounds();

    return map;
}
  
  export default Map;