
export const TimeStampInput = function(op = {}) {
  
  if (!op.label) op.label = '';

  const $el = $('<div/>');
  const $div = $('<div/>', {style:'display: flex'});

  op.required && $el.addClass('required');

  this.$el = () => $el;
  
  const $label = this.$label = $(`<span class="label label-primary mbase2-label">${op.label}</span>`);

  $el.append($label);
  $el.append($div);

  const onChange = () => {
    op.onChange && op.onChange(this.val(), this.dp.val(), this.tp.val());
  }

  const dateInputOptions = {onChange: onChange};

  if (op.format) {
    dateInputOptions.format = op.format;
  }

  this.dp = new op.imports.DateInput(dateInputOptions);
  this.tp = new op.imports.TimePicker({onChange: onChange});

  $div.append(this.dp.$el());
  $div.append(this.tp.$el());
}

TimeStampInput.prototype.val=function(value){

  if (value !== undefined) {
    
    if (value === null) {
      this.dp.val(null);
      this.tp.val(null);
      return;
    }

    value = value.split(' ');
    if (value.length > 1) {
      this.dp.val(value[0]);
      this.tp.val(value[1]);
    }
    return;
  }

  const dpval = this.dp.val();
  if (!dpval) return null;

  const tpval = this.tp.val();
  if (!tpval) return null;

  return dpval + ' ' + tpval;
}