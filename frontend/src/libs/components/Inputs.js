import * as Accordion from './Accordion'
import { helpIconHtml } from '../mbase2_utils';

function appendHiddenSubmitButtonToForm($form) {
    const $submit = $(`<button type="submit" class="btn btn-default">Submit</button>`);
    $submit.hide();
    $form.submit(function(event){
        // cancels the form submission
        event.preventDefault();
    });
    $form.append($submit);
    return $submit;
}

export const Input = function (op = {}, _inputs = null) {

    const $form = this.$form = op.$form || $('<form/>');

    this.type = op.type ? op.type : 'text';

    const $formGroup = this.$formGroup = $(`<div class="${op.required ? 'required' : ''}"></div>`);

    const $label = $(`<span class="label label-primary mbase2-label">${op.label}</span>`);
    
    if (this.type !== 'checkbox') {
        op.label && $formGroup.append($label);
    }

    const readOnly = op.read_only === true ? 'readonly' : '';
    const min = op.min !== undefined ? `min="${op.min}"` : '';
    const max = op.max !== undefined ? `max="${op.max}"` : '';
    
    const step = this.type === 'time' ? 'step="1"' : '';

    const $input = this.$input = op.type === 'select' ? 
        $(`<select></select>`) : 
        $(`<input ${min} ${max} ${readOnly} ${op.required ? 'required' : ''} ${step} type="${this.type}" class="form-control" id="${op.key}" placeholder="${op.placeholder ? op.placeholder : ''}">`);

    if (op.type === 'select') {
        op.options.map(value => $input.append(`<option value="${value}">${value}</option>`))
    }

    if (op.onChange) {
        $input.on('change', () => op.onChange(_inputs));
    }
    
    if (!op.key) {
        $input.removeAttr('id');
    }

    op.keyup && $input.on('keyup', op.keyup);
    op.keydown && $input.on('keydown', op.keydown);
    op.onpaste && $input.on("paste", op.onpaste);
    
    op.pattern && $input.attr('pattern', op.pattern);
    op.step && $input.attr('step', op.step);

    op.helpIconText && this.type !== 'checkbox' && $formGroup.append(helpIconHtml(op.helpIconText));

    $formGroup.append($input);

    this.$help = $(`<span class="help-block"></span>`);
    op.help && this.$help.text(op.help);
    $formGroup.append(this.$help);
    this.$error = $(`<span class="help-block has-error"></span>`);
    $formGroup.append(this.$error);

    if (this.type === 'checkbox') {
        const $label = this.$label = $('<label/>');
        $input.removeClass('form-control');
        $label.append($input);
        $label.append(op.label);
        op.helpIconText && $label.append(helpIconHtml(op.helpIconText));
        $formGroup.append($label);
        $formGroup.addClass('checkbox');
    }

    const validateControl = op.validate === undefined ? true : op.validate;
    
    if (validateControl) {
        const $submit = op.$form ? op.$form.find(':submit') : appendHiddenSubmitButtonToForm($form);
        
        $input.blur(function(event) {
            //event.target.checkValidity();
            $formGroup.removeClass('has-error');
            $label.removeClass('has-error');
            if ($input.css('display')==='none') return;
            $submit.click();
        }).bind('invalid', function(event) {
            $formGroup.addClass('has-error');
            $label.addClass('has-error');
            //event.target.setCustomValidity('Custom');
            //setTimeout(function() { $(event.target).focus();}, 50);
            //return false;
        });

        $input.on('change', function() {
            $formGroup.removeClass('has-error');
            $label.removeClass('has-error');
        });
    }

    $form.append($formGroup); 

    this.$el = () => op.$form ? $formGroup : $form;

    if (op.mask) {
        const maskOptions = Object.assign(op.mask.options || {}, {$input: $input});
        this.imask = new op.mask.component(maskOptions);
    }

    if (op.data!==undefined) {
        this.val(op.data);
    }
}

Input.prototype.setEnabled = function(enabled=true) {
    this.$input && this.$input.prop('disabled', !enabled);
    this.$label && this.$label.prop('disabled', !enabled);
    this.$formGroup && this.$formGroup.prop('disabled', !enabled);
}

Input.prototype.validate = function(reportValidity = true) {
    if (reportValidity === true) {
        return this.$form[0].reportValidity();
    }
    else {
        return this.$form[0].checkValidity();
    }
}

Input.prototype.val = function(a) {
    
    if (a!==undefined) {

        if (this.type === 'checkbox') {
            this.$input.prop('checked', a);
        }
        else {
            this.$input.val(a);
        }

        if (this.imask) {
            this.imask.mask.value = a;
        }

    }
    else {
        if (this.type === 'select') {
            let rval = this.$input.data('forced-value');

            if (rval) {
                return rval;
            }
        }
    }

    let rval;
    if (this.type === 'checkbox') {
        rval = this.$input.prop('checked');
    }
    else {
        rval = this.$input.val();
    }

    if (['number', 'date', 'time'].indexOf(this.type) !== -1 && !rval) return null;   //numbers, date and time shuldn't return an empty string

    return rval;
}

const Inputs = function (op) {
    const $form = this.$form = $('<form/>');
    
    const validateForm = op.validate === undefined ? true : op.validate;

    if (validateForm) {
        appendHiddenSubmitButtonToForm($form);
    }
    
    this.inputs = {};

    let _inputs = [];

    if (Array.isArray(op.inputs)) {
        _inputs = op.inputs;
    }
    else if (typeof op.inputs === "function") {
        _inputs = op.inputs();
    }

    _inputs.map(_op => {
        const validateControl = _op.validate === undefined ? true : _op.validate;
        _op.validate = validateForm && validateControl;
        _op.$form = $form;
        const input = new Input(_op, this.inputs);
        this.inputs[_op.key] = input;
    });

    this.$el = () => $form;

    if (op.accordion) {
        const acc = new Accordion.default();
        const label = op.accordion;
        acc.addPanel(label);
        acc.getPanel(label).$body.html($form);
        this.$el = () => acc.$el()
    }

    if (op.data) {
        this.val(op.data);
    }
}

Inputs.prototype.validate = function(reportValidity = true) {
    if (reportValidity === true) {
        return this.$form[0].reportValidity();
    }
    else {
        return this.$form[0].checkValidity();
    }
}

Inputs.prototype.val = function(values) {
    
    if (values!==undefined) {
        if (!values) {
            values = {};
            Object.keys(this.inputs).map(key => {
                values[key] = null;
            });
        }

        Object.keys(values).map(key => {
            this.inputs[key] && this.inputs[key].val(values[key]);
        });
        return values;
    }

    const rval = {};

    const self = this;
    Object.keys(this.inputs).map(key => {
        rval[key] = self.inputs[key].val();
    });

    return rval;
}

export default Inputs;