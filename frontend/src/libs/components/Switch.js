import 'bootstrap-switch';

const Switch = function (op) {
    const $el = $(`
        <div class="bootstrap-switch-on bootstrap-switch-id-bootstrap-switch-state bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate" style="width: 70px;">
            <div class="bootstrap-switch-container" style="width: 102px; margin-left: 0px;">
                <input class="bootstrap-switch" id="bootstrap-switch-state" type="checkbox" checked="">
            </div>
        </div>
    `);
    
    this.$el = () => $el;
    this.$input = $el.find('input');

    this.switch = this.$input.bootstrapSwitch();
}

Switch.prototype.val = function(value) {
    if (value !== undefined) {
        this.$input.prop('checked', value);
    }

    return this.$input.prop('checked');
}

export default Switch;