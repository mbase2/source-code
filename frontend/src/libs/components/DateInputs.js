import 'bootstrap-datepicker'

export const DateInput = function(op = {}) {
  
  if (!op.label) op.label = '';

  const $el = $('<div/>');

  op.required && $el.addClass('required');

  const $div = $('<div/>',{
    class: 'input-group date-time-picker-pf'
  });

  this.$el = () => $el;
  
  const $label = this.$label = $(`<span class="label label-primary mbase2-label">${op.label}</span>`);
  const $input = this.$input = $('<input type="text" class="form-control">');
  const $btn = $(`
  <span class="input-group-addon">
    <span class="fa fa-calendar"></span>
  </span>`);

  $btn.on('click', () => {
    this.dp.show();
  });

  op.disabled && $input.prop('disabled', true);

  $div.append($input);
  $div.append($btn);

  $el.append($label);
  $el.append($div);

  this.format = op.format || 'yyyy-mm-dd';

  const options = {
    format: this.format,
    todayBtn: 'linked',
    todayHighlight: true,
    autoclose: true,
    enableOnReadonly: false,
    forceParse: true,
    orientation:'bottom',
    zIndexOffset:1000000
  };

  if ($('#dp-container').length>0) {
	options.container = '#dp-container';
  }

  if (op.startDate) {
    options.startDate = op.startDate;
  }

  $input.datepicker(options);

  this.dp = $input.data('datepicker');

  op.onChange && $input.on('change', () => op.onChange(this.dp.getDate(), this.format));

}

DateInput.prototype.val=function(value){

  if (value !== undefined) {
    value = moment(value,'YYYY-MM-DD').toDate();
    this.dp.setDate(value);  
  }

  const rval = this.dp.getDate();

  if (!rval) return null;

  return moment(rval).format('YYYY-MM-DD');

}

export const DateSpan = function(op = {}) {
  const $el = $('<div/>', {style: "display: flex"});
  this.$el = () => $el;

  const date1 = this.date1 = new DateInput({
    label: op.labelStartDate,
    onChange: () => op.onChange && op.onChange(date1)
  });
  const date2 = this.date2 = new DateInput({
    label: op.labelEndDate,
    onChange: () => op.onChange && op.onChange(date2)
  });
  $el.append(date1.$el());
  $el.append(date2.$el());
  
  date1.$el().css('width','50%');
  date2.$el().css('margin-left', '10px').css('width','50%');

  op.date1 && date1.val(op.date1);
  op.date2 && date2.val(op.date2);
}

DateSpan.prototype.val = function(value) {
  return [
    ['event_date', '>=', this.date1.val()],
    ['event_date', '<=', this.date2.val()]
  ];
}
