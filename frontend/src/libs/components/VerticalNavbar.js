/**
 * Patternfly vertical navbar
 */

 export default (items) => {
    const $div = $(`
    <div class="
        nav-pf-vertical
    ">
    </div>`);

    const $listGroup = $(`
    <ul class="list-group">
    </ul>
    `)

    items.map(item => {
        const $li = $(`<li class="list-group-item"/>`);
        const $a = $(`
        <a href="#">
            <span class="list-group-item-value">${item}</span>
        </a>`);

        $a.click(function() {
            $listGroup.find('li').removeClass('active');
            $(this).parent().addClass('active');
        })

        $li.append($a);
        $listGroup.append($li);
    });
    
    $div.append($listGroup);

    return $div;
 }