//https://codepen.io/aanjulena/pen/ZLZjzV
import 'bootstrap-toggle'
import 'bootstrap-toggle/css/bootstrap-toggle.min.css'
const BootstrapToggle = function(op) {
    const $el = $(`<input type="checkbox" checked data-toggle="toggle" data-on="${op.values.on.label}" data-off="${op.values.off.label}" data-onstyle="success" data-offstyle="danger">`);

    this.$el = () => $el;

    $el.on('change', () => {
        op.onChange && op.onChange(this.val());
    });

    this.val = () => {
        return $el.prop('checked');
    }
}

BootstrapToggle.prototype.init = function () {
    this.$el().bootstrapToggle();
}

export default BootstrapToggle;