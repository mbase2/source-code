import Splide from '@splidejs/splide';
import '@splidejs/splide/dist/css/splide.min.css';
import './css/ImageArray.css';

import Button from './Button';

function createSplide($el) {
	return new Splide( $el[0] ,{
		//width: 200, //slider max width
		autoWidth: true,
		isNavigation: false,
		height: 80,
		gap: 10,
		arrows: true,
		pagination : false
		
	}).mount();
}

const ImageArray = function (op={}) {
	const $el = $('<div class="splide"></div>');
	const $track = $('<div class="splide__track"></div>');
	const $list = this.$list = $('<ul class="splide__list"></ul>');

	this.mediaRoot = op.mediaRoot;

	$track.append($list);
	$el.append($track);
	
    this.$el = () => $el;

	op.onClick && $el.on('click', op.onClick);
	
	this.splide = createSplide($el);

	const $input = this.$input = $("<input multiple accept='.png,.jpg' style='display:none' type='file' />");

	$el.append($input);

	const btnOptions = {
		label: op.label || 'Add image',
		onClick: () => {
			$input.trigger('click');
		}
	}

	const onImageRead = e => {
		this.add(e.target.result);
	}

	const self = this;
	$input.on('change', () => {

		const files = $input[0].files;

		for (var i = 0; i < files.length; i++) {
			const file = files[i];
			if (op.fileUpload) {
				op.fileUpload(file).then(res => {
					self.add(res, true);
				});
			}
			else {
				const reader = new FileReader();
				reader.addEventListener("load", onImageRead, false);
				reader.readAsDataURL(file);
			}
		}
	});

	if (!op.skipAddButton) {
		const btn = new Button(btnOptions);
		$el.append(btn.$el());
	}
	this.refreshVisibility();
}

ImageArray.prototype.add = function(dataSrc, refreshVisibility) {
	const fileName = dataSrc+'_thumbnail';
	const src = this.mediaRoot + '/m' + fileName.substring(0,2) + '/' + fileName;
	
	if (this.splide) {
		this.splide.add(`<li class="splide__slide"><img data-src="${dataSrc}" src="${src}"></li>`);
	}
	else {
		this.$list.append(`<li class="splide__slide"><img data-src="${dataSrc}" src="${src}"></li>`);
	}

	refreshVisibility === true && this.refreshVisibility();
}

ImageArray.prototype.refreshVisibility = function() {
	if (this.$list.find('img').length === 0) {
		this.$el().find('.splide__arrows').hide();
	}
	else {
		this.$el().find('.splide__arrows').show();
	}
}

ImageArray.prototype.val = function(value) {
	if (value !== undefined) {

		if (!Array.isArray(value)) {
			value = JSON.parse(value);
		}

		this.splide.destroy(true);
		this.$list.empty();
		this.splide = createSplide(this.$el());
		
		if (value !== null) {
			value.map && value.map(v => this.add(v));
		}

		this.refreshVisibility();

		return true;
	}

	const output = [];
	this.$list.find('img').each(function (i, img) {
		output.push(img.dataset.src);
	});

	return output;

}

export default ImageArray;


    
    