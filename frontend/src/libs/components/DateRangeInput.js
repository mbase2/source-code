import 'bootstrap-datepicker'

const tpl = `<div class="input-group input-daterange">
  <span class="label label-primary mbase2-label">Izbrani povzročitelj</span>
  <input id="date_from" type="text" class="form-control ar">
  <span class="input-group-addon"> - </span>
  <span class="label label-primary mbase2-label">Izbrani povzročitelj</span>
  <input id="date_to" type="text" class="form-control ar">
</div>`;
  
  function initDateIntervalPicker($el){
		$el.find('#date_from').val('2005-05-05');
		var d = new Date();

		var month = d.getMonth()+1;
		var day = d.getDate();
		
		var output = d.getFullYear() + '-' +
			(month<10 ? '0' : '') + month + '-' +
			(day<10 ? '0' : '') + day;
		$el.find('#date_to').val(output);
		
		$el.datepicker({
			format:'yyyy-mm-dd',
			todayBtn: 'linked',
			todayHighlight: true,
			autoclose: true,
			inputs: $('.ar'),
      orientation:'bottom'
		});
  }

  /**
 * Get the number of days in any particular month
 * @link https://stackoverflow.com/a/1433119/1293256
 * @param  {integer} m The month (valid: 0-11)
 * @param  {integer} y The year
 * @return {integer}   The number of days in the month
 */
const daysInMonth = function (m, y) {
  switch (m) {
      case 1 :
          return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
      case 8 : case 3 : case 5 : case 10 :
          return 30;
      default :
          return 31
  }
};

/**
* Check if a date is valid
* @link https://stackoverflow.com/a/1433119/1293256
* @param  {[type]}  d The day
* @param  {[type]}  m The month
* @param  {[type]}  y The year
* @return {Boolean}   Returns true if valid
*/
const _isValidDate = function (d, m, y) {
  m = parseInt(m, 10) - 1;
  return m >= 0 && m < 12 && d > 0 && d <= daysInMonth(m, y);
};

/**
 * Checks if dateText is a valid date
 * @param {string} dateText the date in text format YYYY-MM-DD
 * @return {Boolean} Returns true if valid
 */
const isValidDate = dateText => {
  const d = dateText.split('-');
  if (d.length!==3) return false;
  return _isValidDate(parseInt(d[2]), parseInt(d[1]), parseInt(d[0]));
}
  
	var Input=function(op){
		if (op===undefined) op={};
		var $el=this._$el=$(tpl);
    
    var $dateFrom=$el.find('#date_from');
    var $dateTo=$el.find('#date_to');
    
    if (op.onChange){
      $dateFrom.change(op.onChange);
      $dateTo.change(op.onChange);
    }
    
    if (op.onKeyUp){
      $dateFrom.keyup(op.onKeyUp);
      $dateTo.keyup(op.onKeyUp);
    }
    
    var self=this;
    $dateFrom.blur(function(){
      self.val();
    });
    
    $dateTo.blur(function(){
      self.val();
    });
	};
  
  Input.prototype.init=function(){
    if (this.initialized===true) return;
    this.initialized=true;
    initDateIntervalPicker(this._$el);
  };
	
	Input.prototype.$el=function(){
		return this._$el;
	};
	
	Input.prototype.val=function(a){
    var $el=this._$el;
    var $dateFrom=$el.find('#date_from');
    var $dateTo=$el.find('#date_to');
    if (a===undefined){
      var dateFrom=$dateFrom.val();
      var dateTo=$dateTo.val();
      var updateControls=false;
      if (!isValidDate(dateFrom)) {
        dateFrom=$dateFrom.data('__date');
        updateControls=true;
      }
      
      if (!isValidDate(dateTo)) {
        dateTo=$dateTo.data('__date');
        updateControls=true;
      }
      
      if (updateControls===true) this.val([dateFrom,dateTo]);
      
      return [dateFrom,dateTo];
    }
    else{
      if (isValidDate(a[0]) && isValidDate(a[1])){
        $dateFrom.val(a[0]).datepicker('update');
        $dateTo.val(a[1]).datepicker('update');
        $dateFrom.data('__date',a[0]);
        $dateTo.data('__date',a[1]);
      }
    }
  };
    
  Input.prototype.enable=function(enable){
    this._$el.find('input').prop('disabled',!enable);
  };
	
export default Input;