export default (title, content) => `
<div class="card-pf" style = "margin:1em">
  <div class="card-pf-heading">
    <h2 class="card-pf-title">
      ${title}
    </h2>
  </div>
  <div class="card-pf-body">
    ${content}
  </div>
</div>
`;