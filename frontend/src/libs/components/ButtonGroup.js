/**
 * 
 * @param {*} op 
 * @param {object} data data to be passed to the onClick event
 */

function ButtonGroup(op = {}, data = null) {
    const buttons = op.buttons || [];
    const $el = $('<div class="btn-group" style="float: right"></div>');
    const self = this;
    const highlight = op.highlight === false ? false : true;
    this.buttons = buttons.map(btn => {
        const $btn = $(`<button type="button" class="btn">${btn.label}</button>`);

        if (btn.visible === false) $btn.hide();
        
        if (op.classes) {
            $btn.addClass(op.classes);
        }
        else {
            $btn.addClass("btn-default btn-xs");
        }

        if (btn.iconClass) {
            const $icon=$('<i/>',{class:`fa fa-${btn.iconClass}`,'aria-hidden':'true'});
            $btn.prepend('&nbsp;');
            $btn.prepend($icon);
        }
    
        if (btn.title) {
            $btn.attr('title', btn.title);
        }

        if (op.onClick) {
            $btn.on('click', function() { 
                if (btn.selectable !== false) {
                    self.key = btn.key;
                    if (highlight) {
                        self.buttons.map(btn => {
                            btn.$btn.removeClass('btn-primary');
                        })
                        $btn.addClass('btn-primary');
                    }
                }
                
                op.onClick(data, btn.key, $(this));
            });
        }
        $el.append($btn);
        btn.onAppend && btn.onAppend($btn);
        return {key: btn.key, $btn: $btn}
    });

    this.$el = () => $el;
}

ButtonGroup.prototype.val = function (value) {
    if (value!==undefined) {
        const btn = this.buttons.find(b => b.key === value);
        if (btn) {
            btn.$btn.trigger('click');
            //const previous = this.buttons.find(b => b.key === this.key);
        }
    }

    return this.key;
}

export default ButtonGroup;