/**
 * 
 * @param {object} op 
 * @param {function} op.beforeComponentCreate(key, options, this)
 * @param {function} op.onComponentAppended(key, component, cm)
 * @param {function} op.onComponentChange(key, component, cm)
 */
const ComponentManager = function (op = {}) {
    this.model = {
        primaryKey: 'id',
        tableName: '',
        attributes:{},
        values: null,
        dataComponentKey: '',   //this.components[dataComponentKey] contains raw model data
        saveOptions: {
            validate: true
        }
    }

    this.op = Object.assign({}, op);
    
    this.components = {};
    this._refresh = {};
    this.data = {};
    this.saveOptions = {    //global saveOptions - these are NOT overriden by options passed to the ComponentManager.prototype.save function
        updateModelAfterSave: true,
        parseResult: (res) => res ? res[0] : null,
        onSuccessCallbacks: []  //additional on save success callbacks
    }
}

ComponentManager.prototype.remove = function(key, dataKey) {
    const cmp = this.components[key];
    cmp && cmp.$el && cmp.$el().remove();
    delete this.components[key];
    delete this._refresh[key];
    delete this.data[key];
    dataKey && delete this.data[key];
}

ComponentManager.prototype.destroy = function(key, dataKey) {
    const component = this.components[key];
    if (component && component.destroy) {
        if (component.destroy()) {
            this.remove(key, dataKey);
        }
    } 
}

/**
 * 
 * @param {object} componentDefinition 
 * @param {object} componentDefinition.save
 * @param {object} componentDefinition.save.onSuccess 
 * @param {string} componentDefinition.key
 * @param {object} componentDefinition.component component object constructor function
 * @param {object} componentDefinition.options component options
 * @param {object} componentDefinition.$parent jquery DOM object that will contain the component
 * @param {boolean} overwriteExisting replace existing component if exists
 * @param {object} $parent $container for component if not defined in component options
 */
ComponentManager.prototype.add = async function(componentDefinition, overwriteExisting = false, $parent = null) {

    if (!componentDefinition) return;
    
    const key = this.key = componentDefinition.key;
    
    if (overwriteExisting === true) {
        this.remove(key);
    }
    
    const Constructor = componentDefinition.component;
    const data = componentDefinition.data;
    const options = componentDefinition.options || {};
    let $container = componentDefinition.$parent || $parent;
    const onComponentAppended = componentDefinition.onComponentAppended;
    const beforeComponentCreate = componentDefinition.beforeComponentCreate;

    
    beforeComponentCreate && beforeComponentCreate(options, this);
    this.op.beforeComponentCreate && this.op.beforeComponentCreate(key, options, this);

    if (data && (data.model || data.values)) {
        if (data.request) {
            this.data[key] = this.data[key] || await data.request(data.model);
        }
        else if (data.values) {
            this.data[key] = typeof data.values === 'function' ? data.values() : data.values;
        }
        else {
            this.data[key] = this.data[key] || [];
        }

        if (data.preprocess) {
            this.data[key] = await data.preprocess(this.data[key], data.model);
        }
        
        options.data = data.process ? data.process(this.data[key], data.model) : this.data[key];
    }
    
    let component = this.components[key];

    if (component) {
        if (data && data.refresh) {
            data.refresh(component, options.data);
        }
    }
    else {
        
        if (Constructor.imports) {
            options.imports = Object.assign(options.imports || {}, await Constructor.imports());
        }

        component = this.components[key] = new Constructor(options);

        if (data) this._refresh[key] = data;
        
        if ($container && component.$el) {
            if (typeof $container === 'function') {
                $container = $container(this);
            }
            $container.append(component.$el());
        }

        onComponentAppended && onComponentAppended(component. $container);
        this.op.onComponentAppended && this.op.onComponentAppended(key, component, this, $container);

        if (component && this.op.onComponentChange && component.val) {
            component.$el && component.$el().on('change', ()=>{
                this.op.onComponentChange(key, component.val(), this);
            })
        }
    }
        
    return component;
}

ComponentManager.prototype.refresh = function (key, dataKey) {
    const _refresh = this._refresh[key];
    const component = this.components[key];
    if (!dataKey) dataKey = key;
    const data = this.data[dataKey];
    if (_refresh && component) {
        let processedData;
        if (_refresh.model && _refresh.process) {
            processedData = _refresh.process(data, _refresh.model);
        }
        else {
            processedData = data;
        }

        _refresh.refresh(component, processedData);
    }

    return component;
}

ComponentManager.prototype.save = async function (_save = {}) {
        let url = (_save.rootUrl ? `${_save.rootUrl}/` : '/') + this.model.tableName;
        let id = null;

        const getComponent = function(key, obj) {
            if (typeof obj[key] === "function") return null;
            return this.get(obj[key]);
        }.bind(this);

        const getValue = function(key, obj, _validate) {
            const component = getComponent(key, obj);

            if (obj[key] && !(obj[key].skipValidation === true)) {
                if (!validate(component, _validate)) return {error: true};
            }

            let value = null;
            if (typeof obj[key] === "function") {
                value = obj[key](this);
            }
            else {
                value = this.get(obj[key]) && this.get(obj[key]).val();
            }

            return {value};
                
        }.bind(this);

        const validate = function(component, _validate) {
            if (!component) return true;

            if (component.skipValidation === true) return true;

            if (_validate === true && component.validate) {
                if (!component.validate()) return false;
            }

            return true;
        }

        id = this.model.values && this.model.values.id;
        
        const variableKeys = {};
        Object.keys(this.model.attributes).map(key => {
            if (!this.model.attributes[key]._skipUpsert) {
                variableKeys[':'+key] = key;
            }
        });

        const attributes = {};

        const keys = Object.keys(variableKeys);
        
        for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            const res = getValue(key, variableKeys, this.model.saveOptions.validate === true);
            if (res.error) return false;
            attributes[key] = res.value;
        }

        let method = 'POST';

        if (id) {
            method = 'PUT';
            url = url + '/' + id;
        }

        if (_save.urlParameters) {
            url = url + '?' + _save.urlParameters.join('&');
        }

        const callbacks = {};
        if (_save.loadingOverlayFunction) {
            const $el = this.get(this.key).$el();
            callbacks.loadingOverlay = show => _save.loadingOverlayFunction(show, $el);
        }

        callbacks.onSuccess = _save.onSuccess;
        callbacks.onError = _save.onError;

        const requestParameters = {
            url: url,
            method: method,
            attributes: attributes,
            callbacks: callbacks
        };

        let makeRequest = true;
        
        if (_save.beforeRequest) {
            makeRequest = await _save.beforeRequest(requestParameters, this.saveOptions);
            if (makeRequest === undefined) makeRequest = true;
        }

        if (!makeRequest) return false;

        const result = await _save.request(requestParameters.url, requestParameters.method, requestParameters.attributes, requestParameters.callbacks);

        const parsedResult = (this.saveOptions.parseResult && this.saveOptions.parseResult(result)) || result;

        if (!parsedResult) return false;

        if (this.saveOptions.updateModelAfterSave) {
            if (this.model.values === null) this.model.values = {};
            Object.keys(parsedResult).map(key => this.model.values[key] = parsedResult[key]);
            const modelValues = Object.assign({},this.model.values);
            this.saveOptions.onSuccessCallbacks.map(f => f(this, modelValues, this.model));

            if (this.model.dataComponentKey && _save.upsertDataFunction) {
                const data = this.getData(model.dataComponentKey);
                _save.upsertDataFunction(data, modelValues, this.model);  //upsert based on the default argument 'id'
            }
        }
        else {
            this.saveOptions.onSuccessCallbacks.map(f => f(this, parsedResult, this.model));
        }

        return parsedResult;
}

ComponentManager.prototype.getData = function (key) {
    return this.data[key];
}

ComponentManager.prototype.updateComponentsValue = function () {
    
    const data = this.model.values;
    Object.keys(data).map(key => {
        const c = this.get(key);
        c && c.val(data[key]);
        this.model.values[key] = data[key];
    });
}


ComponentManager.prototype.model = function (data) {
    if (!data) return Object.assign({},this.model.values);

    Object.keys(data).map(key => {
        const c = this.get(key);
        c && c.val(data[key]);
        this.model.values[key] = data[key];
    });
}

ComponentManager.prototype.val = function(value, sameValueForAllComponents = true) {
    const self = this;
    const rval = {};
    Object.keys(this.components).map(key => {
        if (self.components[key].val) {
            if (value === undefined) {
                rval[key] = self.components[key].val();
            }
            else {
                self.components[key].val(sameValueForAllComponents ? value : value[key] );
            }
        }
    });
    return rval;
}

ComponentManager.prototype.get = function (key) {
    return this.components[key];
}

export default ComponentManager;
