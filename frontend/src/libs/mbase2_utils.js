import globals from '../app-globals';
import { jsonParse, compare as utilsCompare } from './utils';

export const aggregateDefIdToLabel = (data, id) => {
    const adef = data.find(d => d.id == id);
    return adef ? adef.label + ": " + adef.description : ''
}

export const assignAdditionalComponentOptions = (variables, additionaComponentOptions) => {
    Object.keys(additionaComponentOptions).map(key => {
        const attribute = additionaComponentOptions[key]._attribute_definition || variables.find(v => v.key_name_id === key);
        variables.push(attribute);
        if (attribute) {
            /**
             * this adds additional options to the attribute definition
             */
            attribute._component = {
                default: true,  //use default component for the data type
                additionalComponentOptions: additionaComponentOptions[key]
            }
        }
    })
}

export const findItem = (arr, key, value, rkey) => {
    const found = arr.find(a => a[key] == value);
    if (!found) return null;
    return found[rkey];
}

export const parseHelpIconText = (v, language) => {
    if (!v.help) return '';
    const help = JSON.parse(v.help);
    return help[language] || help ['en'] || '';
}

export const helpIconHtml = (helpIconText) => {
    return `&nbsp;<span class="pficon pficon-help" title="${helpIconText}"></span>`;
}

export const keyToTranslation = (key, codeListItemsByKey) => {
    const codeListItem = codeListItemsByKey[key];

    if (!codeListItem) return key;

    const translations = jsonParse(codeListItem.translations);

    return translations[globals.language] || translations['en'] || key;
}

export const getVariableProperty = (moduleVariablesByKey, key, propertyKey) => {
    return moduleVariablesByKey[key] && moduleVariablesByKey[key][propertyKey];
}

export const parseTranslation = (v) => {
    let translations = v.translations;
    const key = v.key_name_id;

    if (!translations) return key;
    translations = jsonParse(translations);
    if (!translations) return key;
    
    return translations[globals.language] || translations['en'] || key;
}

/**
 * 
 * @param {array} allowedRoles e.g. ['admin','editor','consumer'], if any of array items = '*', all possible roles are checked
 * @param {string} moduleKey 
 * @returns true if any of the user's roles fits the allowed roles
 */

export const permissions = function(allowedRoles, moduleKey) {
    if (allowedRoles.find(ar => ar==='*')) {
        allowedRoles = ['admin', 'editor', 'consumer', 'reader', 'curator'];
    }
    
    return globals.user.roles.findIndex(r => allowedRoles.filter(s => r.includes(moduleKey+'-'+s)).length>0) !==-1;
}

/**
 * Copies properties from sourceObject to targetObject
 * 
 * @param {object} targetObject 
 * @param {object} sourceObject 
 * @param {array<string>} properties list of properties to copy from sourceObject to targetObject
 */

export const copyProperties = (targetObject, sourceObject, properties) => {
    properties.map(p => targetObject[p] = sourceObject[p]);
}  

/**
 * Calls mbase2 API to retrieve code list values
 * 
 * @param {string|array} listLabel list key label or array of key labels of code list values to retrieve
 * @param {string} apiRoot 
 * @param {array} keys aray of keys to retrieve from database - if empty all keys for the specific code list will be returned
 */
export const getCodeList = async (listLabel, apiRoot=globals.apiRoot, keys = []) => {
    if (!Array.isArray(listLabel)) listLabel = [listLabel];
    const u = await import('./utils');
    const _keys = keys.length === 0 ? '' : `&:key=(${keys.join(',')})`
    const res = await u.request(apiRoot + 
        `/code_list_options?:list_key=(${listLabel.join(',')})${_keys}`);
    
    res.map(r => {
        r.translations = (r.translations && u.jsonParse(r.translations)) || {}
    });

    return res;
}

/**
 * 
 * @param {object} imports previously imported libs
 * @param {object} imports.batch batchImport.js module 
 * @param {object} imports.ModalDialog ModalDialog module
 */

export const batchImport = async (imports, moduleKey, moduleVariables = null, refValues = null, dialogTitle = null, ext=null, fkeys=null) => {

    if (!moduleVariables) return;

    const batch = imports.batch;
    const modal = new imports.ModalDialog.default({
        keyboard: false
    });

    modal.$title.text(dialogTitle || moduleKey);

    let batchModel = null;

    /**
     * batchModel is object where keys are table names and values are attributes to be imported for this table names
     */

    if (moduleVariables && !Array.isArray(moduleVariables)) {
        batchModel = Object.assign({}, moduleVariables);
    }
    else {
        batchModel = {
            [moduleKey]: moduleVariables || await getModuleVariables(moduleKey)
        };
    }

    Object.keys(batchModel).map(key => {
        batchModel[key] = batchModel[key].filter(a => a.importable === true).sort((a,b) => utilsCompare(a,b,'weight_in_import'));
    });

    let allModelAttributes = [];

    Object.keys(batchModel).map(key => {

        const allModuleAttributes = batchModel[key];
        const skip = ['_uname', '_batch_id'];

        if (allModelAttributes.findIndex(a => a.key_data_type_id==='location_data_json') !== -1) {
            skip.push('_location');
        }
        
        const moduleAttributes = allModuleAttributes.filter(a => a.importable === true).filter(a => skip.indexOf(a.key_name_id)===-1);
        allModelAttributes = [...allModelAttributes, ...moduleAttributes];
        batchModel[key] = moduleAttributes;
    });
    
    refValues = refValues || await getRefCodeListValues(allModelAttributes);
    
    batch.default(Object.assign({
        $parent: modal,
        model: batchModel,
        fkeys,
        tableName: moduleKey,
        allModelAttributes,
        refValues: refValues,
        modal:modal,
        ext: ext
    }));
    
    modal.show();
}

export const getTranslationsForList = async (listKey) => {
    if (globals.translations[listKey]) return globals.translations[listKey];

    const utils = await import('./utils');

    const res = await requestHelper(globals.apiRoot + '/code_list_options?:__filter=[["list_key","=","'+listKey+'"]]');
    const rval = {};

    const lang = globals.language;

    for (const item of res) {
        
        if (item.translations) {
            item.translations = utils.jsonParse(item.translations);
        }

        rval[item.key] = (item.translations && item.translations[lang]) || item.key; 
    }

    globals.translations[listKey] = rval;

    return rval;
}

export const getModuleId = async key => {
    const codeList = await getCodeList('modules', globals.apiRoot);
    return codeList.find(cl => cl.key === key).id;
}

/**
 * Returns variables for the moduleId
 * 
 * @param {integer|string} moduleId 
 */
export const getModuleVariables = async (moduleId, sort = true, splitRequiredOptional=false, skipDefault=true) => {
    const utils = await import('./utils');
    const batchCalls = [];
    
    batchCalls.push(moduleId === parseInt(moduleId) ? 
        `module_variables/language/${globals.language}?:module_id=${moduleId}` : 
        `module_variables_vw/language/${globals.language}?:module_name=${moduleId}`);

    const variables = await batchRequestHelper(batchCalls);

    if (skipDefault===true) {
        variables.unshift([]);
    }

    if (sort) {
        return [...sortVariables(variables[0], splitRequiredOptional), ...sortVariables(variables[1], splitRequiredOptional)];
    }
    else {
        return [...variables[0], ...variables[1]];
    }
}

export const batchRequestHelper = (batchCalls) => {

    const promisses = [];
    for (const batchCall of batchCalls) {
        if (batchCall) {
            promisses.push(requestHelper(globals.apiRoot + '/' + batchCall));
        }
        else {
            promisses.push([]);
        }
    }
    return Promise.all(promisses);
}

export const loadingOverlay = (show, $el) => {
    const action = show ? 'show' : 'hide';
    
    if ($ && $.LoadingOverlay) {
        if ($el) {
            $el.LoadingOverlay(action);
        }
        else {
            $.LoadingOverlay(action);
        }
    }
}

export const convertReferencesToAssocArray = async (refValues) => {
    let codeListValues, tableReferences;

    const utils = await import('./utils');

    if (refValues) {
        codeListValues = refValues.codeListValues;
        tableReferences = refValues.tableReferences;
        codeListValues = codeListValues && utils.convertToAssocArray(codeListValues);
        tableReferences = tableReferences && utils.convertToAssocArray(tableReferences,'_id_list_id');
    }

    return {codeListValues, tableReferences};
}

/**
 * 
 * @param {object} cm ComponentManager
 * @param {object} exports exports.js module
 * @param {string} title 
 * @param {string} key 
 * @param {DOM object} $parent 
 * @param {string} preposition preposition to be added to key
 */

export async function addSingleAccordionPanel (cm, exports, title, key, $parent, preposition = 'acc_', returnPanelObject = false, options = {}) {
    const def = {key: preposition + key, $parent: $parent, component: (await exports.Accordion()).default, options: Object.assign(exports.componentsOptions.accordion([title]), options)};
    const cnt = await cm.add(def);
    return returnPanelObject ? cnt.panels[0] : cnt.panels[0].$body;
}

export async function createFormElements(cm, $parent, attributes, refValues={}) {
    const utils = await import('./utils');

    attributes.sort((a,b) => utils.compare(a,b,'weight'));

    attributes = utils.convertToAssocArray(attributes, 'key_name_id');
    
    const {componentDefinitions, definitionAliases} = (await import('./components/componentDefinitions'));

    const exports = await import('./exports');

    const importDefinitions = componentDefinitions(exports);
    const aliases = definitionAliases(utils.patterns);

    cm = cm || new (await exports.ComponentManager()).default();
    
    for (const key of Object.keys(attributes)) {
        const v = attributes[key];
        if (v.visible===false) continue;
        const cdef = await getComponentDefinition(v, importDefinitions, aliases, refValues, true, $parent);
        cdef.key = key;
        cdef.$parent = $parent;
        const cmp = await cm.add(cdef);
        if (v._component && v._component.onComponentAdded) v._component.onComponentAdded(cmp);
    };

    return cm;
}

export const bookmark = (op) => {
    return $(`<div class="bookmark bookmark-${op.required ? 'required' : 'optional' }">
        ${op.title ? `<h5>${op.title}${op.required === false ? '' : '*'}</h5>` : op.content}
    </div>`);
}

export const scrollToElement = ($target, $scrollContainer, topOffset = 10) => {

    const $parent = $scrollContainer || $target.parent();
    
    const parent = $parent[0];
    const target = $target[0];

    $parent.scrollTop(0);

    const parentRect = parent.getBoundingClientRect();
    const targetRect = target.getBoundingClientRect();

    $parent.scrollTop(targetRect.top - parentRect.top);
}

export async function addAccordion (cm, exports, titles, key, $parent, preposition = 'acc_') {
    const def = {key: preposition + key, $parent: $parent, component: (await exports.Accordion()).default, options: exports.componentsOptions.accordion(titles)};
    const cnt = await cm.add(def);
    return cnt.panels.map(p => p.$body);
}

/**
 * Number of completed agreements by claim_id
 * @param {*} result 
 * @returns {object}
 */

export function getAgreementsStatus(result) {
    const agreementsStatus = {};

    result.map(row => {
        if (!agreementsStatus[row._claim_id]) agreementsStatus[row._claim_id] = 0;
        
        if (row._completed === true) {
            agreementsStatus[row._claim_id] += 1;
        }
    });

    return agreementsStatus;
}

/**
 * 
 * @param {string} key usaually database table column name
 * @param {string} label translation for key - this label is shown e.g. in datatables column
 * @param {string} dataType integer, text, real, table_reference, code_list_reference, ...
 * @param {string} [ref=null] reference to table or code_list (used when dataType is table_reference or code_list_reference)
 */
export function createVariableDefinition(key, label, dataType, ref = null) {
    return {
        key_name_id: key,
        t_name_id: label,
        key_data_type_id: dataType,
        ref: ref
    }
}

export function batchImportModuleName(moduleName) {
    const key = moduleName.replace('_vw','').replace('mb2data.','');

    const attributeDefinitionsNameSpace = {
        'interventions': 'interventions_batch_imports',
        'dmg': 'dmg_batch_imports',
        'cnt': '(cnt_monitoring,cnt_observation_reports,cnt_permanent_spots)'
    }

    return attributeDefinitionsNameSpace[key] || moduleName;
}


/**
 * 
 * @param {*} labelKey 
 * @param {array} variablesAttributes 
 * @returns 
 */
export function getReferenceTableListId(labelKey, variablesAttributes) {
    const v = variablesAttributes.find(v => v.reference === labelKey);
    return v ? v.ref : null;
}

/**
 * Gets a component definition suitable for appending via ComponentManager.add function
 * @param {*} variableAttributes 
 * @param {*} importDefinitions component defintioons
 * @param {*} aliases subtype aliases (cf. definitionAliases in componentDefinitions.js)
 * @param {*} references code list options and table references (for the variableAttributes of the corresponding type)
 * @param {*} load if true components are loaded before returning
 * @param {*} $container used when $container has to be passed as an option of the component - callback is handled in the `componentDefinitions.js` file
 */
export const getComponentDefinition = async (variableAttributes, importDefinitions, aliases, references = {}, load = true, $container = null) => {

    /**helper function */
    function getAliasOptions(dataType) {
        const alias = aliases[dataType];
        
        let aliasOptions = null;
        let cdef = null;

        if (alias) {
            cdef = importDefinitions[alias.base];
            if (alias.op) {
                aliasOptions = alias.op;
            }   //component options overrides
        }

        return {aliasOptions, cdef};
    }
    
    const dataType = variableAttributes.key_data_type_id;

    let cdefOverride = null;
    let aliasOptions = null;

    let cdef = importDefinitions[dataType];

    if (variableAttributes._component) {
        if (variableAttributes._component.default !== true) {
            
            cdefOverride = Object.assign({}, variableAttributes._component);
            
            if (variableAttributes._component.defaultOptions) {
                if (!cdef) {
                    ({aliasOptions, cdef} = getAliasOptions(dataType));//destructure to an already declared variable:
                }
                cdefOverride = Object.assign(cdefOverride, cdef);
                cdefOverride.import = variableAttributes._component.import;
            }

            cdef = cdefOverride;
        }
    }
    
    if (!cdef) {
        ({aliasOptions, cdef} = getAliasOptions(dataType));//destructure to an already declared variable:
    }

    if (cdef) {
        
        let _op = variableAttributes._op || {};
        
        const refValuesKey = getRefValuesKey(variableAttributes);

        if (refValuesKey) { //this is true only for data types code_list_reference and table_reference
            _op = {
                data: {
                    values: references[refValuesKey]
                }
            }
        }

        let componentDefinition = {};
        if (cdef.componentDefinition) {
            
            let imports = null;
            if (cdef.imports) {
                imports = await cdef.imports();
            }
            
            componentDefinition = cdef.componentDefinition(variableAttributes, _op, imports, $container); 
        }

        if (componentDefinition.options && componentDefinition.options.pattern) {
            const utils = await import('./utils');
            componentDefinition.options.pattern = utils.patterns[componentDefinition.options.pattern];
            if (variableAttributes.t_pattern_id && !componentDefinition.options.help) {   //if helper text is not already defined by some other means - this is currently always false
                componentDefinition.options.help = variableAttributes.t_pattern_id || variableAttributes.key_pattern_id;
            }
        }

        if (load === true) {
            const cimp = await cdef.import();
            componentDefinition.component = cimp[componentDefinition.component || 'default'];
        }

        if (aliasOptions) {
            componentDefinition.options = Object.assign(componentDefinition.options, aliasOptions);
        }

        if (variableAttributes._component && variableAttributes._component.additionalComponentOptions) {
            componentDefinition.options = Object.assign(componentDefinition.options || {}, variableAttributes._component.additionalComponentOptions);
        }

        return componentDefinition;
    }

    return null;
}

export const getPostName= (postNumber, variables, refValues) => {
    const listId = getReferenceTableListId('poste', variables);
    if (listId) {
        const option = refValues.tableReferences.find(item => item.list_id == listId && item.id == postNumber)
        if (option) {
            return option.key;
        }
    }

    return '';
}

export const compareStoredAndCurrentAffecteeData = (storedAffecteeData, currentAffecteeData) => {
    let equal = false;

    if (storedAffecteeData && currentAffecteeData) {
        Object.keys(storedAffecteeData).map(key => {
            if (storedAffecteeData[key] === null && !currentAffecteeData[key]) {
                currentAffecteeData[key] = null;
            }
        });
    
        if (currentAffecteeData._post && !storedAffecteeData._post) {
            if (storedAffecteeData._post_number == currentAffecteeData._post_number) {
                storedAffecteeData._post = currentAffecteeData._post;
            }
        }
    
        if (_.isEqual(currentAffecteeData, storedAffecteeData)) {
            equal = true;
        }
    }
    else {
        equal = true;
    }

    return equal;
}

export const buildFilterParametersString = (filterValuesString, spatialFilterValuesString, predefinedFilterValuesString, placeholderKeys = true) => {
    let filter = '';

    const fkey = placeholderKeys ? ':__filter' : 'f';
    const sfkey = placeholderKeys ? ':__sfilter' : 'sf';
    const pfkey = placeholderKeys ? ':__pfilter' : 'pf';

    function addToFilter(key, value) {
        if (!value) return;
        value = value.trim();
        if (!value) return;
        const pchar = filter ? '&' : '?';
        filter = filter + pchar + key + '=' + value;
    }

    addToFilter(fkey, filterValuesString);
    addToFilter(sfkey, spatialFilterValuesString);
    addToFilter(pfkey, predefinedFilterValuesString);

    return filter;
}

/**
 * 
 * @param {array<object>} res array of associative arrays where keys are attribute names
 * @param {array<objects>} attributes variable definitions
 */

export const convertJsonToObjects = (res, attributes) => {
    const jsonAttributes = [];
    attributes.map(a => {
        if (['json', 'jsonb', 'table_reference_array', 'code_list_reference_array', 'location_data_json'].indexOf(a.key_data_type_id) !== -1) {
            jsonAttributes.push(a.key_name_id);
        }
    });

    if (jsonAttributes.length === 0) return;

    res.map(r => {
        jsonAttributes.map(a => {
            r[a] = r[a] && jsonParse(r[a]);
        })
    });
}

/**
 * Replace quotes in filter expression to be parsable by PHP json_decode function
 * 
 * @param {array<array>} filter 
 */

export const prepareFilter = (filter, escapeDoubleQuotes=true) => {
    
    if (filter.length === 0) return '';

    filter = JSON.stringify(filter);
    
    if (escapeDoubleQuotes === true) {
        filter = filter.replace(/"/g,'\\"');
    }
    
    return `?:__filter=${filter}`;
}

/**
 * Translates attributes values
 * @param {object} row 
 * @param {object} attributesKeyed 
 */

export const processAttributeValues = (row, attributesKeyed, refValuesKeyed) => {
    Object.keys(row).map(cname => {
        const v = attributesKeyed[cname];
        if (v && row[cname]!==undefined) {

            const refValuesKey = getRefValuesKey(v);

            if (refValuesKey) {
                let values = row[cname];
                
                if (v.key_data_type_id.includes('_array')) {
                    if (!Array.isArray(values)) {
                        try {
                            values = jsonParse(values);
                            row[cname] = values;
                        }
                        catch(e) {
                            ;
                        }
                    }
                }

                if (!Array.isArray(values)) {
                    values = [values];
                }

                const labels = values.map(value => {
                    const key = refValuesKey === 'tableReferences' ? value + '_' + v.ref : value;
                    return value ? getCodeListValue(key, refValuesKeyed[refValuesKey], globals.language) : value;
                });
                
                row['t_' + cname] = labels.join('<br>');
            }
            else if (v.key_data_type_id === 'location_reference') {
                row['t_' + cname] = row['geom'];

                const geom = JSON.parse(row['geom']);
                row[cname] = {
                    id: row[cname],
                    lat: geom.coordinates[1],
                    lon: geom.coordinates[0]
                }
            }
            else if (v.key_data_type_id === 'boolean') {
                //row[cname] = `<input type="checkbox" ${row[cname] ? 'checked' : ''}>`;
                if (row[cname]===true || row[cname]===false) {
                    row['t_' + cname] = row[cname] === true ? globals.t`DA` : globals.t`NE`;
                }
            }
        }
    });
}

/**Handle saving SelectWithSpecify component data*/
export const handleSelectWithSpecifyComponentData = (request, cm) => {

    Object.keys(cm.model.attributes).map(key => {
        const attr = cm.model.attributes[key];
        const value = request.attributes[':'+key];
        if (value !== undefined &&
            (attr.key_data_type_id === "code_list_reference" || attr.key_data_type_id === "code_list_reference_array")) {
                if (value && value.specify!==undefined) {
                    request.attributes[':'+key] = value.select;
                    request.attributes[':_data'] = Object.assign(request.attributes[':_data'] || {},{[key]:value.specify});
                }
                else {
                    if (request.attributes[':_data'] && request.attributes[':_data'][key]) delete request.attributes[':_data'][key];
                }
        }
    });
}

export const translateAttributeValue = (value,a,refValues,skipEmpty=true, joinTranslatedArrays = true) => {
    if (a.key_data_type_id === 'table_reference') {
        value = getReferenceTableKeyLabel(value, a.ref, refValues)
    }
    else if (a.key_data_type_id === 'code_list_reference') {
        value = getCodeListValueFromId(value, refValues, globals.language);
    }
    else if (['table_reference_array','code_list_reference_array'].indexOf(a.key_data_type_id) !== -1 ) {
        const translatedValues = [];
        const isTableReference = a.key_data_type_id.startsWith('table_reference');
        value = jsonParse(value || []);
        for (const v of value) {
            if (skipEmpty && !v) continue;
            if (isTableReference) {
                translatedValues.push(getReferenceTableKeyLabel(v, a.ref, refValues));
            }
            else {
                translatedValues.push(getCodeListValueFromId(v, refValues, globals.language));
            }
        }

        value = joinTranslatedArrays ? translatedValues.join(', ') : translatedValues;
    }

    return value;
}

export const attributeValuesToHtml = (row, attributesKeyed, delimiter = '<br>', attributeKeysToInclude = [], skipEmpty = true, refValues = {}, skip=[]) => {
    const output = [];
    const attributeKeysToIncludeSet = new Set(attributeKeysToInclude);

    for (let key of attributeKeysToIncludeSet) {
        if (skip.indexOf(key)!==-1) continue;
            const a = attributesKeyed[key];
            if (a) {
                if (a.reference==='licence_list' && row['__licence_list']) {

                    const label =  a.t_name_id || a.key_name_id;
                    const items = [];
                    row['__licence_list'].map(item => {
                        items.push(`<a href="#" data-id="${item.id}" class="licence-link">${item.value}</a>`);
                    });

                    output.push(`<b>${label}:</b> ${items.join(', ')}`);
                    
                    continue;
                }
                const tkey = 't_'+key; //translation key (API automatically returns translations for the selected language for the base database tables but not for the views)
                let value = row[tkey]  || translateAttributeValue(row[key], a,refValues,skipEmpty); //if translation is missing we get it from the attribute definition
                if (skipEmpty && (value === undefined || value === null || value === '')) continue;
                
                const label =  a.t_name_id || a.key_name_id;

                if (a.reference==='licence_list') {
                    output.push(`<b>${label}:</b> <a href="#" data-id="${row[key]}" class="licence-link">${value}</a>`);
                    continue;
                }
                
                output.push(`<b>${label}:</b> ${value}`);
            }
            else {
                const value = row[key];
                if (skipEmpty && !value) continue;
                output.push(`<b>${key}:</b> ${value}`);
            }
    };
    
    return output.join(delimiter);
}

export const parseGeomField = (row, key='_location_reference') => {
    if (!row.geom) return;
    const geom = JSON.parse(row['geom']);
    row[key] = {
        id: row[key],
        lat: geom.coordinates[1],
        lon: geom.coordinates[0]
    }
}

/**
 * 
 * @param {*} id 
 * @param {*} refTableName 
 * @param {*} variables 
 * @param {*} refValues 
 * @returns 
 */

export const getReferenceTableKeyLabel = (id, refTableId, refValues, language = globals.language) => {
    if (refTableId) {
        const option = refValues.tableReferences.find(item => item.list_id == refTableId && item.id == id);
        if (option) {
            
            let value = '';
            if (option.translations) {
                value = jsonParse(option.translations);
                value = value[language];
            }

            if (!value) {
                value = option.key;
            }

            return value;
        }
    }
    return id;
}

export const getCodeListValue = (key, refValues, language = globals.language) => {
    const row = refValues && refValues[key];
    if (!row) return key;
    const values = row.translations;
    if (!values) return row.key;
    const value = values[language];
    if (!value) return row.key;
    return value;
}

/**
 * 
 * @param {integer} id code_list_options id
 * @param {object} refValues {codeListValues{array}}
 * @param {string} language 
 * @returns 
 */

export const getCodeListValueFromId = (id, refValues, language) => {
    const row = refValues.codeListValues.find(clval => clval.id == id);
    return getCodeListValue(id, {[id]: row}, language);
}

/**
 * 
 * @param {*} obj 
 * @param {object} values associative array of keys with translations (these are variabledDefinitions if language is not defined and code list values if language is defined)
 * @param {*} language 
 */
export const translateVariableKeys = (obj, values, language = null) => {
    const res = {};
    const keyMap = {};
    
    Object.keys(obj).map(key => {
        let nkey = null;
        if (language) {
            nkey = (values[key] && values[key].translations[language]) || key; 
            res[nkey] = obj[key];
        }
        else {
            nkey = (values[key] && values[key].t_name_id) || key; 
            res[nkey] = obj[key];
        }
        keyMap[key] = nkey;
    });
    
    return {
        translated: res,
        keyMap: keyMap
    };
}

export const groupAttributesByModule = references => {
    if (!references) return references;

    const res = {};

    references.map(r => {
        if (!res[r.key_module_id]) res[r.key_module_id] = [];
        res[r.key_module_id].push(r);
    });

    return res;
}

/**
 * Gets code lists for ref properties in the attributes argument. If reference in attributes array is passed as a table name it gets converted to the code list ID.
 * 
 * @param {object} attributes 
 * @param {string} attributes.key_data_type_id if 'code_list_reference' then the code list with id in the property 'ref' is fetched
 * @param {string|number} attributes.ref id or key of the code_list to fetch
 * @param {array<string|number>} additionalKeys additional codeLists to fetch
 * @param {boolean} replaceListKeys replace list keys with ids if present in the attributes
 */

export const getRefCodeListValues = async (attributes, additionalKeys = [], replaceListKeys = true) => {
    const utils = await import('./utils');
    const listIds = [];
    const listKeys = [];
    const trefIds = [];
    const trefKeys = [];

    const _addKey = (type, ref) => {

        if (ref) {
            if (type === "code_list_reference" || type === 'code_list_reference_array') {
                if (!isNaN(parseInt(ref))) {
                    listIds.push(ref);
                }
                else {
                    listKeys.push(ref);
                }
            }
            else if (type === 'table_reference' || type === 'table_reference_array') {
                if (!isNaN(parseInt(ref))) {
                    trefIds.push(ref);
                }
                else {
                    trefKeys.push(ref);
                }
            }
        }
        
        if (type === 'reference') {
            listKeys.push('code_lists');
            listKeys.push('referenced_tables');
        }
    }

    attributes.map(v => _addKey(v.key_data_type_id, v.ref));

    additionalKeys.map(ref => _addKey('code_list_reference', ref));

    if (listIds.length === 0 && listKeys.length === 0 &&
        trefIds.length === 0 && trefKeys.length === 0) return;

    let requests = [];
    
    listIds.length > 0 ? 
        requests.push('code_list_options?:list_key_id=(' + listIds.join(',') + ')') :
        requests.push('');

    listKeys.length > 0 ? 
        requests.push(`code_list_options?:list_key=(${listKeys.join(',')})`) :
        requests.push('');

    requests.push(`code_list_options?:list_key=code_lists`);

    trefIds.length > 0 ? 
        requests.push('table_reference_values/language/null?:id=(' + trefIds.join(',') + ')') :
        requests.push('');

    trefKeys.length > 0 ? 
        requests.push(`table_reference_values/language/null?:id:code_list_options/key=(${trefKeys.join(',')})`) :
        requests.push('');

    requests.push(`code_list_options?:list_key=referenced_tables`);

    const res = await batchRequestHelper(requests);
    
    if (res === false) return [];

    const codeListValues = [...res[0], ...res[1]].map(clv => {
        clv.list_id = clv.list_key_id;
        return clv;
    });
    const tableReferences = [...res[3], ...res[4]];

    [...codeListValues, ...tableReferences].map(r => r.translations = r.translations ? JSON.parse(r.translations) : {});

    const codeLists = res[2];
    const trefs = res[5];

    if (replaceListKeys) {
        const codeListsObj = utils.arrayToObject(codeLists, 'key');
        const tableReferencesObj = utils.arrayToObject(trefs, 'key');
        attributes.map(a => {
            if (a.ref && isNaN(parseInt(a.ref))) {
                if (a.key_data_type_id === 'code_list_reference' || a.key_data_type_id === 'code_list_reference_array') {
                    a.ref = codeListsObj[a.ref].id;
                }
                else if (a.key_data_type_id === 'table_reference' || a.key_data_type_id === 'table_reference_array') {
                    a.ref = tableReferencesObj[a.ref].id;
                }
            }
        });
    }

    tableReferences.map(tr => tr._id_list_id = tr.id+'_'+tr.list_id);    //create unique identifiers
    
    return {codeListValues: codeListValues,
            codeLists: codeLists,
            trefs: trefs,
            tableReferences: tableReferences
    };
}

export const validateConditionsForCompleteButton = function ($completeButton, bookmarks, cm) {
    if ($completeButton) {
        
        if (cm && cm.get('_status') && cm.get('_status').val() == 3) {
            $completeButton && $completeButton.prop('disabled', false);
            return;
        }

        let sectionsValidated = true;
        const keys = Object.keys(bookmarks);
        for (let key of keys) {
            const bm = bookmarks[key].parent ? bookmarks[bookmarks[key].parent] : bookmarks[key];
            if (bm.required && !bm.validated) {
                if (['_odskodnina', '_odskodnina_additional'].indexOf(key) === -1) {
                    sectionsValidated = false;
                    break;
                }
            }
        }

        if (sectionsValidated && cm && cm.get('_status') && cm.get('_status').val() != 3) { // [3, t`Obrazec 2.3 - vloga stranke`]
            if (cm && cm.get('_odskodnina').val().length===0 && cm.get('_odskodnina_additional').val().length===0) {
                sectionsValidated = false;
            }
        }

        $completeButton && $completeButton.prop('disabled', !sectionsValidated);
    }
}

export const getCountMsg = function getCountMsg(res) {
    let countMsg = '';

    const t = globals.t;
                    
    if (res.importType === 'insert') {
        countMsg = t`Number of inserted rows:` + ' ' + `${res.numberOfInsertedRows}`
    }
    else if (res.importType === 'update') {
        countMsg = t`Number of updated rows:` + ' ' + `${res.numberOfUpdatedRows}`;
    }

    countMsg+= '<br>' + t`Number of all rows in the data input:` + ' ' + `${res.numberOfInputRows}`;

    return countMsg;
}

export const importErrorsHandler = function(res, $container) {
    let criticalErrors = 0;
    const missingRequiredAreCritical = res.importType === 'insert' ? true : false;
    const values = [];
    const t = globals.t;
    res.errors.map(rowData => {
        
        let rowDataObject = {};

        if (Array.isArray(rowData)) {
            rowDataObject = jsonParse(rowData[0]);
        }
        else {
            rowDataObject = jsonParse(rowData);
        }

        if (rowDataObject.critical || (rowDataObject.required && missingRequiredAreCritical)) criticalErrors++;
        
        values.push({
            data: rowData
        });
    });

    const countMsg = getCountMsg(res);

    let msg = '';
    if (criticalErrors === 0) {
        msg = alertCode(t`The import has finished with some non critical errors.` + '<br>' + countMsg, t`All the rows were imported.`, 'warning');
    }
    else {
        msg = alertCode(t`The import has finished with some critical errors.` + '<br>' + countMsg, t`Some or all rows were NOT imported or updated.`, 'danger');
    }
    
    $container.html(msg);
    
    import('../modules/importErrors').then(async importErrors => {
        await importErrors.default({$parent: $container, batchId: null, data: {
            values: values
        }});
    })
}

export const moduleHeader = function(title, extendedTitle, $btnGroup, $status=null, subtitle = null, $breadcrumbs = null, justifyContent='space-between') {
    const $container = $('<div/>');
    const $header = $('<div/>',{style: `display: flex; justify-content: ${justifyContent}; align-items: center`});
    extendedTitle = extendedTitle ? ': ' + `<span class="extended-title">${extendedTitle}</span>` : '';
    const $left = $('<div/>');

    $breadcrumbs && $left.append($breadcrumbs);

    $left.append(`<h2 class="mbase-title" style="margin-bottom:0">${title}${extendedTitle}</h2>`);
    subtitle && $left.append(`<h3 class="mbase-subtitle" style="margin-bottom:0">${subtitle}</h3>`);
    $header.append($left);
    $status && $header.append($status);
    $btnGroup && $header.append($btnGroup);
    $btnGroup && $btnGroup.css('margin-top','22px');
    $btnGroup && $btnGroup.css('flex-wrap','wrap');
    $btnGroup && $btnGroup.css('row-gap','10px');
    $btnGroup && $btnGroup.css('justify-content','right');
    $container.append($header);
    $container.append('<div id="module-header-bottom"/>');
    $container.append('<hr style="margin-bottom:0">');
    return $container;
}

export const skipAttributes = (attributes, attributeKeysToBeSkipped) => attributes.filter(a => attributeKeysToBeSkipped.indexOf(a.key_name_id)===-1)

export const generalTableRecordsOptions = async function  ($parent, tableName, moduleVariablesAttributes, codeListValues, tableReferences) {

    if (codeListValues !== undefined) { //check if codeListValues is structure holding codeListValues and/or tableReferences
        tableReferences = codeListValues.tableReferences || tableReferences;
        codeListValues = codeListValues.codeListValues || codeListValues;
    } 
    
    if (codeListValues=== undefined && tableReferences === undefined) {
        const refValues = await getRefCodeListValues(moduleVariablesAttributes);
        if (refValues) {
            codeListValues = refValues.codeListValues;
            tableReferences = refValues.tableReferences;
        }
    }

    return  {
        $parent: $parent,
        variablesAttributes: moduleVariablesAttributes,
        tableName: tableName,
        refValues: {codeListValues, tableReferences}
    }
}

export const getFilterParametersString = (dataFilterCmp, placeholderKeys=false, predefinedFilterValuesString='') => {
    const fvals = dataFilterCmp.val();

    const f  = [];
    const sf = [];

    fvals.map(fv => {
        if (['oe_ime','ob_uime','lov_ime','luo_ime','nuts'].indexOf(fv[1]) !== -1) {
            sf.push(fv);
        }
        else {
            f.push(fv);
        }
    });

    return buildFilterParametersString(f.length>0 && JSON.stringify(f), sf.length > 0 && JSON.stringify(sf), predefinedFilterValuesString, placeholderKeys);
}

export const sortVariables = (variables, splitRequiredOptional = true, key = 'weight') => {
    
    const compare = (a,b) => utilsCompare(a,b,key);

    if (splitRequiredOptional) {
        const required = [];
        const optional = [];

        variables.map(v => {
            if (v.required)  {
                required.push(v);
            }
            else {
                optional.push(v);
            }
        });    

        return [...required.sort(compare), ...optional.sort(compare)];
    }
    else {
        return variables.sort(compare);
    }
      
}

export const moduleVariablesTableRecordsOptions = async function  ($parent, moduleId, moduleVariablesAttributes, codeListValues, tableReferences, additionalFilter = '') {
    const generalOptions = await generalTableRecordsOptions($parent, 'module_variables', moduleVariablesAttributes, codeListValues, tableReferences);
    
    if (additionalFilter) {
        additionalFilter = ',' + JSON.stringify(additionalFilter);
    }

    return  Object.assign(generalOptions, {
        skipAttributesFromTable: ['module_id'],
        url: globals.apiRoot + `/module_variables_vw/language/${globals.language}?:__filter=[["module_id","=",${moduleId}]${additionalFilter}]`
    });
};

export const getRefValuesKey = variableAttributes => {
    let refValuesKey = null;
    if (['code_list_reference', 'reference', 'code_list_reference_array'].indexOf(variableAttributes.key_data_type_id) !==-1) {
        refValuesKey = 'codeListValues';
    }
    else if (variableAttributes.key_data_type_id === 'table_reference' || variableAttributes.key_data_type_id === 'table_reference_array') {
        refValuesKey = 'tableReferences';
    }

    return refValuesKey;
}

/**
 * returns relative file path with first two characters of file name as subfolder
 */
export const filePath = (fileName) => {
    return '/m' + fileName.substring(0,2) + '/' + fileName;
}

/**
 * @param {object} target
 * @param {string} target.id id of the record to be updated, if undefined POST request will be sent
 * @param {object} target.attributes list of attributes to be sent with the request
 * @param {string} target.url url where a POST or PUT request will be sent
 */

export const saveButtonHelper = async (source = {}) => {
    const utils = await import('./utils');
    const t = utils.t;

    if (!source) source = {};
    
    return assignRequestCallbackToasters(Object.assign({
        request: utils.request,
        rootUrl: globals.apiRoot,
        upsertDataFunction: utils.upsertArray,
        loadingOverlayFunction: (show, $el) => loadingOverlay(show, $el)
    }, source));
}

export const parseCenik = async rows => {
    const cenik = {};
    const utils = await import('./utils');
    rows.map(row => {
        cenik[[row.key_kategorija, row.key_podkategorija, row.key_objekt].filter(key => key).join(' > ')] = utils.jsonParse(row.cenik);
    });
    return cenik;
}

/**
 * 
 * @param {array<string>} keys keys in the price list (text property of individual item), keys are to be ordered in descending order as present in the price list
 * @param {object} priceList price list JSON object as stored in the database (array of objects in multi levels where text property is key)
 */

export const findObjectPriceList = (keys,priceList) => {
    
    let items = priceList;

    for (const key of keys) {
        
        for (const item of items) {
            if (item.text === key) {
                items = item.nodes || item;
                break;
            }
        }
    }

    return items.cenik;
}

export const assignRequestCallbackToasters = async (target) => {
    const t = globals.t;
    return Object.assign(target, {
        onSuccess: (response, body) => $.toaster({ message : t`OK` }),
        onError: async (response, body) => $.toaster({ message : t`Error while saving` + ' ...<br>' + (await body).err, priority: 'danger' })
    });
}

export const requestCallbacksHelper = (op = {}) => ({
    loadingOverlay: (show, $el) => loadingOverlay(show, op.$el || $el)
});

/**
 * Adds t_{key} property to individual items of array of objects if translation for 'key' exists
 */
export const translateKey = (key, targetPropertyKey, aobj, codeListId, codeListValues, language = globals.language) => {
    aobj.map(obj => {
        const clist = codeListValues.find(cl => cl.key === obj[key] && cl.list_id == codeListId);
        if (clist) {
            if (!obj[targetPropertyKey]) {
                obj[targetPropertyKey] = (clist.translations && clist.translations[language]) || clist.key;
            }
        }
    })
}

/**
 * 
 * @param {*} url 
 * @param {*} method 
 * @param {*} data 
 * @param {*} callbacks 
 * @param {*} $el jQuery DOM element for loading overlay 
 */

export const requestHelper = async (url, method = 'GET', data = null, callbacks = {}, $el = null) => {
    const utils = await import('./utils');
    return utils.request(url, method, data, Object.assign(requestCallbacksHelper({$el:$el}), callbacks));
}

/**
 * Returns patternfly html code for alert box
 * 
 * @param {*} msg 
 * @param {*} title 
 * @param {string} type alert box type: info, success, warning, danger
 * @returns {string} 
 */

export const alertCode = (msg, title='', type='info') => {
    const types = {
        danger: {
            icon: "pficon-error-circle-o"
        },
        warning: {
            icon: "pficon-warning-triangle-o"
        },
        success: {
            icon: "pficon-ok"
        },
        info: {
            icon: "pficon-info"
        }
    };

    if (!types[type]) type = 'info';

    return `<div class="alert alert-${type}">
    <span class="pficon ${types[type].icon}"></span>
    <strong>${title}</strong> <span class="alert-msg">${msg}</span>
  </div>`;
}

/**
 * Returns priceLists index where valid_till field is more than examination date or empty
 * @param {*} examinationDate 
 * @param {*} priceLists 
 */
export const dmgGetPriceListIndex = (examinationDate, priceLists) => {
    
    const examinationDateMoment = moment(examinationDate);
    let inx = priceLists.findIndex(plist => plist.valid_till && !examinationDateMoment.isAfter(plist.valid_till));
    
    if (inx === -1) {
        inx = priceLists.findIndex(plist => !plist.valid_till);
    }
    
    return inx === -1 ? priceLists.length-1 : inx;
}

export const dmgGetWorkHourPrices = (examinationDate, prices) => {
    const grouped = _.groupBy(prices, p=>p.valid_till);
    const examinationDateMoment = moment(examinationDate);

    const filtered = prices.filter(p => p.valid_till && !examinationDateMoment.isAfter(p.valid_till));

    return filtered.length > 0 ? filtered : prices.filter(p => !p.valid_till);
}

export const testData = async (apiRoot='') => {
    const u = await import('./utils');
    const codeList = await getCodeList('module_names', apiRoot);
    const virtualTables = await u.request(apiRoot + `/virtual-tables`);

    codeList.map(cl => {
        const vt = virtualTables.filter(vt => vt.label_key === cl.key);
        if (vt.length === 1) {
            u.request(apiRoot + '/virtual-table-values/modules', 'POST', {module_name: cl.id, properties:JSON.stringify([123])}).then(x=>console.log(x))
        }
        //
    })
}

export const updateVTV = async (apiRoot='') => {
    const u = await import('./utils');
    const codeList = await getCodeList('module_names', apiRoot);
    const virtualTables = await u.request(apiRoot + `/virtual-tables`);

    codeList.map(cl => {
        const vt = virtualTables.filter(vt => vt.label_key === cl.key);
        if (vt.length === 1) {
            u.request(apiRoot + `/virtual-table-values/modules/${cl.id}`, 'PUT', {':properties':JSON.stringify([456])}).then(x=>console.log(x))
        }
        //
    })
}
