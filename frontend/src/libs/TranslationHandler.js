import { getCodeList, keyToTranslation } from "./mbase2_utils";

const TranslationsHandler = function () {
    this.translationsByKey = {};
}

TranslationsHandler.prototype.addCodeList = async function (translationCodeListName) {
    const translations = await getCodeList(translationCodeListName);

    translations.map(item => {
        this.translationsByKey[item.key] = item;
    });
}

TranslationsHandler.prototype.t = function(a) {
    return keyToTranslation(a[0], this.translationsByKey);
}

export default TranslationsHandler;