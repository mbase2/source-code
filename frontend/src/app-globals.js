const root = window.location.origin; //__API_ROOT__: argv.mode === 'production' ? '"http://173.212.213.157"' : '"http://localhost:8080"'

const translations = {
    '__client':{
        'New record': {
            'sl':'Nov zapis'
        },
        'New record for existing affectee': {
            'sl':'Nov zapis za obstoječega oškodovanca'
        },
        'Add new data for this affectee': {
            'sl':'Dodaj nove podatke za tega oškodovanca'
        },
        'Save': {
            'sl':'Shrani'
        },
        'Add a record': {
            'sl':'Dodaj zapis'
        },
        'Start date': {
            'sl':'Datum od:'
        },
        'End date': {
            'sl':'Datum do:'
        },
        "You don't have permissions to access detailed data for this module":{
            'sl':'Nimate pravic dostopa do detajlnih podatkov za ta modul.'
        },
        'Data overview': {
            'sl':'Pregled podatkov'
        },
        'Data overview filter':{
            'sl':'Omejitev prikaza podatkov'
        },
        'Track individual animals':{
            'sl':'Sledenje posameznim živalim'
        },
        'No samples are found for the selected reference animal ID.': {
            'sl':'Za izbrano referenčno žival ne najdem vzorcev.'
        },
        'The samples could have been collected outside the selected date span. Try changing data overview filter parameters.': {
            'sl':'Vzorci bi lahko bili zbrani izven izbranega datumskega obdobja. Poskusite spremeniti parametre filtra pregleda podatkov.'
        },
        'Share this data query': {
            'sl':'Povezava do prikazane poizvedbe'
        },
        'Copy link': {
            'sl':'Kopiraj povezavo'
        },
        'Batch import': {
            'sl':'Paketni uvoz'
        },
        'Latitude (north)': {
            'sl':'Zemljepisna širina (sever)'
        },
        'Longitude (east)': {
            'sl':'Zemljepisna dolžina (vzhod)'
        },
        'Koordinatni sistem': {
            'en':'Coordinate reference system'
        },
        'Posodobi prostorske podatke': {
            'en':'Update spatial data'
        },
        'Spatial units': {
            'sl':'Prostorske enote'            
        },
        'Species': {
            'sl':'Živalska vrsta'
        },
        'Notes': {
            'sl':'Opombe'
        },
        'Events count': {
            'sl': 'Število dogodkov'
        },
        'Right click here and copy link to the clipboard or left click here and the link will open in new tab.':{
            'sl':'Kliknite tukaj na desni gumb miške in kopirajte povezavo na odložišče ali kliknite tukaj na levi gumb miške in povezava se bo odprla v novem zavihku.'
        },
        'Specify': {
            'sl': 'Navedi'
        },
        'Do you really want to remove selected file?': {
            'sl':'Ali res želiš izbrisati izbrano datoteko?'
        },
        'Field' : {
            'sl': 'Polje'
        },
        'Condition' : {
            'sl': 'Pogoj'
        },
        'Value' : {
            'sl': 'Vrednost'
        },
        'AND' : {
            'sl': 'IN'
        },
        'OR' : {
            'sl': 'ALI'
        },
        'Add' : {
            'sl': 'Dodaj'
        },
        'Deputies' : {
            'sl': 'Pooblaščenci'
        },
        'Affectees' : {
            'sl': 'Oškodovanci'
        },
        'Module settings': {
            'sl': 'Nastavitve modula'
        },
        'Apply': {
            'sl': 'Uporabi'
        },
        'Add a new user': {
            'sl':'Dodaj novega uporabnika'
        },
        'System users': {
            'sl': 'Uporabniki sistema'
        },
        'Distance from settlement (house) in meters': {
            'sl': 'Oddaljenost od naselja (hiše) v metrih'
        },
        'The coordinates have changed - click OK if this was done on purpose.': {
            'sl': 'Koordinate so se spremenile - potrdi, če je bila sprememba narejena namenoma.'
        },
        'Local name': {
            'sl': 'Lokalno ime'
        },
        'Location type': {
            'sl': 'Tip lokacije'
        },
        'The value has been changed. Click': {
            'sl': 'Koordinate so bile spremenjene. Klikni'
        },
        'here': {
            'sl': 'tukaj'
        },
        'to revert this change': {
            'sl': 'za razveljavitev te spremembe'
        },
        'NE': {
            'en': 'NO'
        },
        'DA': {
            'en': 'YES'
        },
        'INSERT - append all the rows from the selected page to the database if possible': {
            'sl': 'INSERT - dodaj vse vrstice z izbrane strani, če je to mogoče'
        },
        'UPDATE - ignore the rows with unmatched update ID and update only the defined columns': {
            'sl': 'UPDATE - ne upoštevaj vrstic, kjer se ID za posodabljanje ne ujema z vrstico v bazi in posodobi le stolpce z določeno preslikavo'
        },
        'View Fullscreen': {
            'sl': 'Celozaslonski način'
        },
        'Exit Fullscreen':{
            'sl': 'Izhod iz celozaslonskega načina'
        },
        'Import from XLSX':{
            'sl': 'Uvoz iz XLSX'
        },
        'List of imports':{
            'sl': 'Seznam uvozov'
        },
        'Batch imports':{
            'sl': 'Paketni uvozi'
        },
        'Export':{
            'sl': 'Izvoz'
        },
        'New':{
            'sl': 'Novo'
        },
        'Telemetry':{
            'sl': 'Telemetrija'
        },
        'Import from API': {
            'sl': 'API uvoz'
        },
        'EPSG:3912 - D48/GK': {
            'sl': 'EPSG:3912 D48/GK - jugoslovanski geodetski datum 1948, sistem dvorazsežnih kartezičnih koordinat - Gauß-Krügerjeva projekcija'
        },
        'EPSG:4326 - WGS84/φλ':{
            'sl': 'EPSG:4326 WGS84/φλ - GPS - geodetski datum WGS84, sistem dvorazsežnih geodetskih/elipsoidnih koordinat'
        },
        'EPSG:3794 - D96/TM':{
            'sl': 'EPSG 3794 - D96/TM - slovenski geodetski datum 1996, sistem dvorazsežnih kartezičnih koordinat - prečna Mercatorjeva projekcija'
        },
        'ob_uime':{
            'sl': 'Občina'
        },
        'Navedba':{
            'sl': 'Opomba'
        },
        'Navedi':{
            'sl': 'Opombe'
        },
        'Full export': {'sl':'Polni izvoz'},
        'Damage objects': {'sl':'Škodni objekti'},
        'Export for monthly report': {'sl':'Izvoz za mesečno poročilo'},
        'Damage objects by RU': {'sl':'Škodni objekti po OE'},
        'oe_ime':{
            'sl': 'OE ZGS'
        },
        'luo_ime':{
            'sl': 'LUO'
        },
        'lov_ime':{
            'sl': 'Ime lovišča'
        },
        "January": {
            "sl": "januar"
          },
          "February": {
            "sl": "februar"
          },
          "March": {
            "sl": "marec"
          },
          "April": {
            "sl": "april"
          },
          "May": {
            "sl": "maj"
          },
          "June": {
            "sl": "junij"
          },
          "July": {
            "sl": "julij"
          },
          "August": {
            "sl": "avgust"
          },
          "September": {
            "sl": "september"
          },
          "October": {
            "sl": "oktober"
          },
          "November": {
            "sl": "november"
          },
          "December": {
            "sl": "december"
          },
          "Stalna števna mesta": {
            "en": "Permanent counting spots"
          }
    }
}

const globals = {
    root: root,
    apiRoot: root + '/api/mbase2',
    language: 'en',
    languages: ['en','sl','hr','de','it'],
    get mediaRoot() {
        return this.apiRoot + '/uploaded-file'
    },
    t:(x,a) => {
        const key = x[0] || a;
        if (translations.__client[key] && translations.__client[key][globals.language]) {
            return translations.__client[key][globals.language];
        }
        else {
            return key;
        }
    },
    translations,
    proj4defs: {
        3794: '+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs',
        3912: '+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=bessel +towgs84=409.545,72.164,486.872,3.085957,5.469110,-11.020289,17.919665 +units=m +no_defs',
        4326: '+proj=longlat +datum=WGS84 +no_defs',
        3765: '+proj=tmerc +lat_0=0 +lon_0=16.5 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs',
        3035: '+proj=laea +lat_0=52 +lon_0=10 +x_0=4321000 +y_0=3210000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs',
        8677: '+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=5500000 +y_0=0 +ellps=bessel +towgs84=682,-203,480,0,0,0,0 +units=m +no_defs +type=crs'
    }
}

export const cache = {};

export const proj4defs = globals.proj4defs;

export const language = globals.language;

export const t = globals.t;

export default globals;
