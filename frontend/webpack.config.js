// webpack.config.js
const HtmlWebPackPlugin = require( 'html-webpack-plugin' );
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const path = require( 'path' );

const webpack = require('webpack');

module.exports = (env, argv) => {
    return {
        context: __dirname,
        entry: './src/index.js',
        output: {
            path: path.resolve( __dirname, 'dist' ),
            filename: '[name].min.js',
            chunkFilename: '[name].[contenthash].min.js',
            library: 'mbase2modules',
            libraryTarget: 'umd',
            publicPath: argv.mode === 'production' ? "/vendor/mbase2dtl/" : "http://localhost:8081/"
            //publicPath: argv.mode === 'production' ? "/mb2/" : "/"
        },
        devServer: {
            headers: {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
              "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
            }
        },
        resolve: {
            
        },
        externals: {
            jquery: 'jQuery'
        },
        optimization: {
            minimize: false
        },
        module: {
            rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader, 
                //'style-loader',
                'css-loader',
                ],
            },
            
                // images loader
                {
                    test: /\.(png|jpe?g|gif)$/,
                    loaders: [
                        {
                            loader: "file-loader",
                            options: {
                                name: "img/[name].[ext]"
                            }
                        }
                    ],
                },

                // fonts loader
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: "fonts/[name].[ext]"
                            }
                        },
                    ],
                },

                // svg inline 'data:image' loader
                {
                    test: /\.svg$/,
                    loader: "svg-url-loader"
                },
            ],
        },
        plugins: [
            new CopyPlugin({
                patterns: [
                { from: './node_modules/patternfly/dist', to: 'customized-vendor-libs/patternfly' },
                { from: './node_modules/select2/dist', to: 'vendor/libs/select2/dist' },
                { from: './node_modules/dropzone/dist/min', to: 'vendor/libs/dropzone/dist/min' },
                { from: './node_modules/leaflet.markercluster/dist', to: 'vendor/libs/leaflet.markercluster/dist'},
                { from: './node_modules/leaflet/dist', to: 'vendor/libs/leaflet/dist'},
                
                { from: './node_modules/jquery/dist/jquery.min.js', to: 'vendor/libs/jquery.min.js' },
                { from: './node_modules/bootstrap/dist/js/bootstrap.min.js', to: 'vendor/libs/bootstrap.min.js' },
                { from: './node_modules/popper.js/dist/umd/popper.min.js', to: 'vendor/libs/popper.min.js' },
                { from: './node_modules/underscore/underscore-min.js', to: 'vendor/libs/underscore-min.js' },

                { from: './node_modules/proj4/dist/proj4.js', to: 'vendor/libs/proj4.min.js' },

                { from: './node_modules/moment/min/moment.min.js', to: 'vendor/libs/moment.min.js' },
        
                { from: './vendor/libs/sidebar-v2/css/leaflet-sidebar.min.css', to: 'vendor/libs/sidebar-v2/leaflet-sidebar.min.css' },
                { from: './vendor/libs/sidebar-v2/js/leaflet-sidebar.min.js', to: 'vendor/libs/sidebar-v2/leaflet-sidebar.min.js' },
                { from: './customized-vendor-libs/patternfly', to: 'customized-vendor-libs/patternfly' }
                
                ],
            }),
            //new CleanWebpackPlugin(),
            new HtmlWebPackPlugin({
                template: 'index.html'
            }),
            new HtmlWebPackPlugin({
                template: 'import-from-camelot.html',
                filename: 'import-from-camelot.html'
            }),
            new MiniCssExtractPlugin({
                    filename: "[name].[contenthash].css",
                    chunkFilename: "[id].[contenthash].css"
                }
            ),
            new webpack.DefinePlugin({
                //__API_ROOT__: argv.mode === 'production' ? '"https://portal.mbase.org"' : '"http://localhost:8080"'
                __API_ROOT__: argv.mode === 'production' ? '"http://173.212.213.157"' : '"http://localhost:8080"'
            })
        ]
    };
}