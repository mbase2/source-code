@section('scripts')
	<link rel="stylesheet" href="{{$frontEndPath}}/customized-vendor-libs/patternfly/css/mbase2-pf-patternfly.min.css"/>
	<link rel="stylesheet" href="{{$frontEndPath}}/customized-vendor-libs/patternfly/css/mbase2-pf-patternfly-additions.min.css"/>
	<link rel="stylesheet" href="{{$frontEndPath}}/vendor/libs/dropzone/dist/min/dropzone.min.css"/>
	<link rel="stylesheet" href="{{$frontEndPath}}/vendor/libs/select2/dist/css/select2.min.css"/>
	<link rel="stylesheet" href="{{$frontEndPath}}/vendor/libs/leaflet/dist/leaflet.css"/>
	<link rel="stylesheet" href="{{$frontEndPath}}/vendor/libs/sidebar-v2/leaflet-sidebar.min.css"/>
	<link rel="stylesheet" href="{{$frontEndPath}}/vendor/libs/leaflet.markercluster/dist/MarkerCluster.css"/>
	<link rel="stylesheet" href="{{$frontEndPath}}/vendor/libs/leaflet.markercluster/dist/MarkerCluster.Default.css"/>
	<style>
		.leaflet-popup-content {
			font-size: 12px;
		}
		#mbaselaravel {
			border-top: 1px solid #f1f1f1;
			background-color: white;
			padding-left: 10px;
			padding-right: 10px;
			font-size: 13px;
		}
		.mbase2-pf .label {
			font-size: 0.97em;
			padding:.1em .4em .1em;
		}
		.dataTables_wrapper th.sorting_asc {
			/*padding-top: 0px!important;*/
		}
		table.dataTable thead th {
			padding-top:0px!important;
		}
		table.dataTable.nowrap th {
			padding-bottom:0px!important;
		}
		header {
			display: none;
		}
	</style>

	<script src="{{$frontEndPath}}/vendor/libs/jquery.min.js"></script>
	<script src="{{$frontEndPath}}/vendor/libs/popper.min.js"></script>
	<script src="{{$frontEndPath}}/vendor/libs/moment.min.js"></script>
	<script src="{{$frontEndPath}}/vendor/libs/dropzone/dist/min/dropzone.min.js"></script>
	<script src="{{$frontEndPath}}/vendor/libs/leaflet/dist/leaflet.js"></script>
	<script src="{{$frontEndPath}}/vendor/libs/sidebar-v2/leaflet-sidebar.min.js"></script>
	<script src="{{$frontEndPath}}/vendor/libs/select2/dist/js/select2.min.js"></script>
	<script src="{{$frontEndPath}}/vendor/libs/underscore-min.js"></script>
	<script src="{{$frontEndPath}}/vendor/libs/leaflet.markercluster/dist/leaflet.markercluster.js"></script>
	<script src="{{$frontEndPath}}/vendor/libs/proj4.min.js"></script>
	<script src="{{$frontEndPath}}/vendor/libs/bootstrap.min.js"></script>
	<script src="{{$frontEndPath}}/customized-vendor-libs/patternfly/js/patternfly.min.js"></script>

	<script src="{{$frontEndPath}}/main.min.js"></script>
	<script>
		$(function() {
			const moduleContentDiv = document.getElementById('mbaselaravel');
			
			const navHeight = $('nav').height();

			const $moduleContentDiv = $(moduleContentDiv);
			$moduleContentDiv.css('height',`calc(100vh - ${navHeight}px)`);

			mbase2modules.mbase2_modules().then(module => module.default(
			$(moduleContentDiv),
				'mbase2/{{$path}}',
				'{{ app()->getLocale() }}',
				`{
					"uid":"{{$userData['uid']}}",
					"name":"{{$userData['name']}}",
					"roles":@json($userData['roles'])
				}`
			));
		});
	</script>
@endsection
<x-app-layout>
    <x-slot name="header">
    </x-slot>
        @auth
            <div id="mbaselaravel" class="mbase2-pf"></div>
			<div id="dp-container" class="mbase2-pf" style="position:absolute; top:0; left:0;width:100%"></div>
        @else
		<div class="py-12">
            <div class="p-6 bg-white border-b border-gray-200">
                <p>Please login or register.</p>
                <p>Locale is: {{ app()->getLocale() }}</p>
            </div>
		</div>
        @endauth
</x-app-layout>
