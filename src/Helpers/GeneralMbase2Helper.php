<?php
namespace Mbase2dtl\Helpers;

/**
 * General static stuff used more than once
 */

class GeneralMbase2Helper {

    /**
     * mbase2.code_list_options_fkeys materialized view is used to "automatically" translate the output of a select query in the Mbase2Database::select function
     */
    public static function updateCodeListOptionsForeignKeysMaterializedView() {
        \DB::unprepared(file_get_contents(__DIR__.'/../../docker/sql/recreate_code_list_options_fkeys.sql'));
    }
}
