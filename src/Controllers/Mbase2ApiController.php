<?php
namespace Mbase2dtl\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

require_once(__DIR__.'/../mbase2/Mbase2Api.php');
require_once(__DIR__.'/../mbase2/Mbase2Database.php');
require_once(__DIR__.'/../mbase2/Mbase2Files.php');
require_once(__DIR__.'/../mbase2/Mbase2Drupal.php');
require_once(__DIR__.'/../mbase2/Mbase2Utils.php');

class Mbase2ApiController
{
    public function __invoke($path) {
        $publicPath = Storage::path('');
        $storagePath = [
            'private'=>$publicPath.'mbase2/.private',
            'public'=>$publicPath.'mbase2/'
        ];
        
        //global $user;
        $user = auth()->user();
        if (empty($user)) {
          echo 'Unauthorised access ...';
          return;
        }

        $api = new \Mbase2Api($path, drupal_get_query_parameters(),['storagePath' => $storagePath, 'uid' => $user->id, 'user'=>$user], false);

        //__construct($data = null, $status = 200, $headers = [], $options = 0, $json = false)
        return response()->json(
          $api->getResult(), $api->getStatusCode(), [], $api->getJsonFlags()
        );
    }
}

function drupal_get_query_parameters(array $query = NULL, array $exclude = array(
    'q',
  ), $parent = '') {
  
    // Set defaults, if none given.
    if (!isset($query)) {
      $query = $_GET;
    }
  
    // If $exclude is empty, there is nothing to filter.
    if (empty($exclude)) {
      return $query;
    }
    elseif (!$parent) {
      $exclude = array_flip($exclude);
    }
    $params = array();
    foreach ($query as $key => $value) {
      $string_key = $parent ? $parent . '[' . $key . ']' : $key;
      if (isset($exclude[$string_key])) {
        continue;
      }
      if (is_array($value)) {
        $params[$key] = drupal_get_query_parameters($value, $exclude, $string_key);
      }
      else {
        $params[$key] = $value;
      }
    }
    return $params;
  }