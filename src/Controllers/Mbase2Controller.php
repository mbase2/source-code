<?php
namespace Mbase2dtl\Controllers;
use Illuminate\Support\Facades\App;

class Mbase2Controller
{
    private $userRolesArray = [];
    private $userGroupPermissions = [];

    public function __invoke($path) {
        
        $frontEndPath = '/vendor/mbase2dtl';

        if (App::environment('local')) {
            $frontEndPath = 'http://localhost:8081';
        }

        $isAdmin = false;
		$user = auth()->user();
		if($user != null){
			$isAdmin = $user->permissions != null ? $user->permissions["mbase2l.admin"] : false;
		}

        $this->userRolesArray = $user->groups->pluck('slug')->toArray();

        $userData = [
            "uid" => $user->id,
            "name" => $user->name,
            "roles" => $this->userRolesArray
        ];

        $moduleAlias = [
            'modules/genetics/samples' => 'gensam',
            'modules/genetics/samples/sample' => 'gensam',
            'modules/genetics/administration/referenced_tables/samplers' => 'gensam',
            'modules/genetics/administration/referenced_tables/gensam_studies' => 'gensam',
            'modules/genetics/administration/referenced_tables/gensam_populations' => 'gensam',
            'modules/genetics/administration/referenced_tables/gensam_organisations' => 'gensam',
            'modules/genetics/administration/referenced_tables/gensam_ref_animals' => 'gensam',
            'admin/modules/gensam' => 'gensam',
            'admin/modules/howling' => 'howling',
            'admin/modules/interventions' => 'interventions',
            'admin/modules/dmg' => 'dmg',
            'admin/modules/cnt' => 'cnt',
            'admin/modules/sop' => 'sop',
            'admin/modules/ct' => 'ct',
            'modules/ct/camelot' => 'ct',
            'modules/ct/ct_camelot_sources' => 'ct',
            'admin/modules/tlm' => 'tlm',
            'modules/tlm-api' => 'tlm',
            'modules/tlm_animals' => 'tlm',
            'admin/referenced_tables/individuals' => 'ct',
            'admin/modules/mortbiom' => 'mortbiom'
        ];

        $this->userGroupPermissions = [
            'admin/code-lists' => ['admins'],
            'modules/dmg/affectees' => ['admins'],
            'modules/dmg/ceniki' => ['admins'],
            'admin/editor/dmg'=>['dmg-admins'],
            'admin/modules/ct' =>['admins'],
            'admin/modules/gensam' => ['admins'],
            'admin/modules/howling' => ['admins'],
            'admin/modules/interventions' => ['admins'],
            'admin/modules/dmg' => ['admins'],
            'admin/modules/sop' => ['admins'],
            'admin/modules/tlm' => ['admins'],
            'admin/modules/mortbiom' => ['admins']
        ];

        if (str_starts_with($path, 'modules/genetics/administration/')) {
            $this->userGroupPermissions[$path] = ['admins'];
        }

        $group = '';

        if (isset($moduleAlias[$path])) {
            $group = $moduleAlias[$path];
        }
        else {
            $parts = explode('/', $path);
            if (count($parts) > 1) {
                $group = $parts[1];
            }
            else {
                $group = $parts[0];
            }
        }
        
        if (auth()->user()->isInGroup('mbase2', $group) || $isAdmin || $path === 'map' || $this->checkUserGroupPermission($path)) {

            if (isset($this->userGroupPermissions[$path])) {
                if (!$this->checkUserGroupPermission($path)) {
                    abort(403, "You have no (or insufficient) roles in this module.");
                }
            }

            return view('mbase2::index', compact('path','frontEndPath', 'userData'));
        }
        else {
            abort(403, "You have no roles in this module.");
        }
    }

    private static function findMatchingKeyValue($path, $arr) {
        foreach($arr as $key=>$value) {
            if (str_starts_with($path, $key)) {
                return $value;
            }
        }

        return [];
    }

    private function checkUserGroupPermission($path) {
        $groups = self::findMatchingKeyValue($path, $this->userGroupPermissions);

        if (empty($groups)) return false;
  
        foreach($this->userRolesArray as $mrole) {
            foreach($groups as $grole) {
                if (str_ends_with($mrole, '-'.$grole)) {
                    return true;
                }
            }
        }

        return false;
    }
}