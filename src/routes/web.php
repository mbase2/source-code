<?php

use Mbase2dtl\Controllers\Mbase2Controller;
use Illuminate\Support\Facades\Route;

Route::middleware(['web', 'auth'])->group(function () {
    Route::get('mbase2/{path}', Mbase2Controller::class)->where('path', '.*');
});