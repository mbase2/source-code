<?php

use Mbase2dtl\Controllers\Mbase2ApiController;
use Illuminate\Support\Facades\Route;

/**
 * This overrides definition in the main project - the parameter names (subpath) have to be exactly the same to override
 */

Route::middleware(['web'])->group(function () {
    Route::get('/api/mbase2/{subpath}', function ($path) {
        return (new Mbase2ApiController)($path);
    })->where('subpath', '.*');

    Route::post('/api/mbase2/{subpath}', function ($path) {
        return (new Mbase2ApiController)($path);
    })->where('subpath', '.*');

    Route::put('/api/mbase2/{subpath}', function ($path) {
        return (new Mbase2ApiController)($path);
    })->where('subpath', '.*');

    Route::delete('/api/mbase2/{subpath}', function ($path) {
        return (new Mbase2ApiController)($path);
    })->where('subpath', '.*');
});