<?php

namespace Mbase2dtl\Providers;

use Illuminate\Support\ServiceProvider;

class Mbase2ServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../frontend/dist' => public_path('vendor/mbase2dtl'),
        ], 'laravel-assets');

        $this->loadViewsFrom(__DIR__.'/../views', 'mbase2');

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $this->app->booted(function () {    //override routes from parent package
            $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
            $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        });
    }
}