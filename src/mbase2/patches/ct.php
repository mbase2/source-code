<?php namespace patches;

use Exception;
use Mbase2Utils;
use Mbase2SchemaPatches;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');

class ct extends Mbase2SchemaPatches  {
    static function patch_20() {
        $cnt = \DB::update("UPDATE mb2data.ct SET _licence_name=11 WHERE _licence_name=21");
        echo "UPDATE mb2data.ct SET _licence_name=11 WHERE _licence_name=21: ($cnt)";
    }

    static function patch_19() {
        self::catchQuery("update mbase2.module_variables set required=false where id = (
            select id from mbase2.module_variables_vw mvv where module_name='ct' and variable_name = '_uname'
            )");
    }

    static function patch_18() {
        file_put_contents("/var/www/html/storage/app/mbase2/import_from_camelot-win-x64.zip", 
        fopen("https://stage.mbase.org/storage/import_from_camelot_prod.zip", 'r'));
    }

    static function patch_17() {
        self::catchQuery("alter table mb2data.ct_cameras drop constraint unique_camera_name_camelot_id");
        self::catchQuery("alter table mb2data.ct_sites drop constraint unique_site_name_camelot_id");

        self::catchQuery("ALTER TABLE mb2data.ct_cameras ADD CONSTRAINT unique_camera_name_camelot_id_camelot_camera_id UNIQUE (camera_name, camelot_id, camelot_camera_id)");
        self::catchQuery("ALTER TABLE mb2data.ct_sites ADD CONSTRAINT unique_site_name_camelot_id_camelot_site_id UNIQUE (site_name, camelot_id, camelot_site_id)");
    }

    static function patch_16() {
        self::catchQuery("UPDATE mbase2.module_variables SET importable=true where id in 
        (select id from mbase2.module_variables_vw where module_name='ct' and visible_in_table=true)");
    }

    static function patch_15() {
        self::catchQuery("update mbase2.module_variables set visible=false,visible_in_table =false where id in (
            select id from mbase2.module_variables_vw mvv where variable_name = 'camelot_last_update')");
    }

    static function patch_14() {
        foreach (['_species_name',
                'event_date',
                'event_time',
                'survey_name',
                'trap_station_name',
                'camera_name',
                'sex',
                'life_stage',
                'individual_name',
                'position',
                'sighting_quantity',
                'notes',
                '_licence_name',
                '_uname'] as $i => $key) {
                    self::updateVariables([
                        [
                            'key_name_id'=>$key,
                            'visible_in_table' => true,
                            'weight'=>$i+1,
                            'weight_in_table'=>$i+1,
                            'weight_in_popup'=>$i+1,
                            'visible_in_popup'=>true
                        ]
                        ], 'ct');
                }
    }

    static function patch_13() {
        self::catchQuery("update mbase2.module_variables set visible_in_table=false where id in (
            select id from  mbase2.module_variables_vw mvv where module_name = 'ct' and variable_name  in ('_batch_id', '_uname', 'sighting_quantity', 'hair_trap', 'species_list_id')
            )");
    }

    static function patch_12() {
        self::catchQuery("ALTER TABLE mb2data.ct_surveys DROP CONSTRAINT unique_survey_name__licence_name");
        self::catchQuery("ALTER TABLE mb2data.ct_surveys ADD CONSTRAINT unique_survey_name_camelot_id UNIQUE (survey_name, camelot_id)");

        self::catchQuery("ALTER TABLE mb2data.ct_cameras DROP CONSTRAINT unique_camera_name__licence_name");
        self::catchQuery("ALTER TABLE mb2data.ct_sites DROP CONSTRAINT unique_site_name__licence_name");
        self::catchQuery("ALTER TABLE mb2data.ct_trap_stations DROP CONSTRAINT unique_trap_station_name_survey_name__licence_name_location_ref");

        self::catchQuery("ALTER TABLE mb2data.ct_cameras ADD CONSTRAINT unique_camera_name_camelot_id UNIQUE (camera_name, camelot_id)");
        self::catchQuery("ALTER TABLE mb2data.ct_sites ADD CONSTRAINT unique_site_name_camelot_id UNIQUE (site_name, camelot_id)");
        self::catchQuery("ALTER TABLE mb2data.ct_trap_stations ADD CONSTRAINT unique_trap_station_name_survey_name_camelot_id_location_ref UNIQUE (trap_station_name, survey_name, camelot_id, _location_reference)");
    }

    static function patch_11() {
        self::catchQuery("ALTER TABLE mb2data.camelot_sources rename to ct_camelot_sources");

        self::catchQuery("ALTER TABLE mb2data.ct_camelot_sources 
        ALTER COLUMN camelot_last_update TYPE timestamp;");

        self::catchQuery("update mbase2.code_list_options set key='ct_camelot_sources' where key = 'camelot_sources'");
    }

    static function patch_10() {
        self::catchQuery("CREATE TABLE mb2data.ct_camelot_backup AS 
        TABLE mb2data.ct
        WITH NO DATA;");
    }

    static function patch_9() {
        self::catchQuery("update mbase2.code_list_options set translations = :t where id in (
            select id from mbase2.code_list_options_vw where list_key='hair_trap_options' and key='not_present')", [':t'=>json_encode(["en" => "not present"])]);
    }

    static function patch_8() {
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey1;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey10;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey11;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey12;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey13;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey14;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey15;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey16;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey17;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey18;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey19;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey2;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey20;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey21;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey22;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey23;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey24;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey25;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey26;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey27;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey28;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey29;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey3;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey30;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey4;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey5;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey6;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey7;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey8;");
        self::catchQuery("ALTER TABLE mb2data.ct DROP CONSTRAINT ct__batch_id_fkey9;");

    }

    static function patch_7() {
        foreach(['_species_name',
        'survey_name',
        'event_date',
        'event_time',
        'trap_station_name',
        'camera_name',
        'individual_name',
        'sex',
        'life_stage',
        'position',
        'notes',
        '_uname'] as $inx => $key) {
            self::updateVariables([[
                'key_name_id'=>$key,
                'weight'=>$inx+1,
                'weight_in_table'=>$inx+1
            ]],'ct');
        }
    }

    static function patch_6() {

        self::updateVariables([[
            'key_name_id' => '_batch_id',
            'visible' => false,
            'visible_in_table' => false
        ]], 'ct');

        self::updateVariables([[
            'key_name_id' => 'camelot_sighting_id',
            'visible' => false,
            'visible_in_table' => false
        ]], 'ct');
    }

    static function patch_5() {
        self::importVariables([
            [
                'key_name_id'=>'nuts', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'nuts',
                'translations' => json_encode(["sl" => "Regije NUTS","en" => "Nomenclature of territorial units for statistics (NUTS)"]),
                'filterable' => true
            ]
        ], 'ct', 'modules');
    }

    static function patch_4() {
        self::catchQuery(Mbase2Utils::SQLcreateSpatialIndex('mb2data', 'ct_locations', 'geom'));
    }

    static function patch_3() {
        self::updateVariables([[
            'key_name_id' => 'sex',
            'key_data_type_id' => 'table_reference',
            'ref' => 'sex_list'
        ]], 'ct');
    }

    static function patch_2() {
        self::ct_vw();
        \DB::update("update mbase2.module_variables set visible_in_cv_detail=true, visible_in_cv_grid=true
        where id in (select id from mbase2.module_variables_vw where module_name='ct' and visible = true and visible_in_table = true)");
    }

    static function patch_1() {

        \DB::update("update mbase2.module_variables set visible_in_cv_detail=true
        where id in (select id from mbase2.module_variables_vw where module_name='ct' and visible = true and visible_in_table = true)");

        self::ct_vw();

        self::importVariables([
            [
                'key_name_id'=>'species_list_id', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'species_list',
                'translations' => json_encode(["sl" => "Živalska vrsta","en" => "Species"])
            ]
        ], 'ct', 'modules', ['required' => true, 'visible_in_cv_detail'=>true, 'visible_in_cv_grid'=>true, 'filterable' => false]);

        self::updateVariables(([[
            'key_name_id' => 'event_date',
            'visible_in_cv_detail'=>true, 
            'visible_in_cv_grid'=>true
        ]]),'ct');

        self::updateVariables([[
            'key_name_id' => 'trap_station_name',
            'visible_in_cv_detail'=>true, 
            'visible_in_cv_grid'=>false
        ]
        ],'ct');

        self::updateVariables(([[
            'key_name_id' => 'photos',
            'visible_in_cv_detail'=>true, 
            'visible_in_cv_grid'=>true
        ]]),'ct');
    }

    static function ct_vw() {
        self::catchQuery("DROP VIEW IF EXISTS mb2data.ct_vw");

        self::catchQuery("CREATE OR REPLACE VIEW mb2data.ct_vw
        AS SELECT ct.*,
            lsl.id as species_list_id,
            ts.site_name,
            ts.id AS trap_station_id,
            ts._location_reference AS trap_station_location_id
           FROM mb2data.ct ct
             LEFT JOIN mb2data.ct_trap_stations ts ON ct.trap_station_name = ts.id
             left join mbase2.code_list_options clo on clo.id = ct._species_name
             left join laravel.species_list lsl on lsl.slug = clo.key;");
             
    }
}