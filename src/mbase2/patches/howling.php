<?php namespace patches;

use Exception;
use Mbase2Api;
use Mbase2Database;
use Mbase2Utils;
use Mbase2SchemaPatches;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');

class howling extends Mbase2SchemaPatches  {
    static function patch_7() {
        self::howling_vw();
    }

    static function patch_6() {
        self::catchQuery("update mbase2.module_variables set exportable = true where id in (
            select id from mbase2.module_variables_vw mvv where exportable = false and module_name='howling' and variable_name not in ('_batch_id', '_location')
        )");
    }

    static function patch_5() {
        self::importVariables([
            [
                "key_name_id"=>'id_mas',
                'key_data_type_id' => 'text',
                'importable' => 'true',
                'weight_in_import' => 0.2
            ],
            [
                'key_name_id' => 'location_coordinate_type_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'location_coordinate_type_list',
                'required' => false,
                'importable' => true,
                'filterable' => false,
                'visible' => false,
                'translations' => ['en' => 'Location type', 'sl' => 'Tip koordinate']
            ]
        ],'howling');
        self::updateModuleDatabaseSchema('howling');
    }

    static function patch_4() {
        self::importVariables([[
            'key_name_id' => '_batch_id',
            'key_data_type_id'=>'table_reference',
            'ref' => 'import_batches',
            'filterable' => true,
            'translations' => ['en'=>'Batch import', 'sl' => 'Paketni uvoz']
        ]],'howling');
    }

    static function patch_3() {
        self::howling_vw();
    }

    static function patch_2() {

        self::addCodeListOption('code_lists','howling_response_type');
        self::addCodeListOption('howling_response_type','Adults');
        self::addCodeListOption('howling_response_type','Adults and cubs');
        self::addCodeListOption('howling_response_type','Cubs');

        self::importVariables([
            [
                'key_name_id' => 'species_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'species_list',
                'translations' => ['en' => 'Species', 'sl' => 'Živalska vrsta']
            ],
            [
                'key_name_id' => 'licence_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'licence_list',
                'translations'=>'{"en": "Licence", "sl": "Licenca"}'
            ],
            [
                'key_name_id' => 'event_date',
                'key_data_type_id' => 'date',
                'translations'=>'{"en": "Date", "sl": "Datum"}'
            ],
            [
                'key_name_id' => 'notes',
                'key_data_type_id' => 'text',
                'required' => false,
                'visible_in_cv_grid'=>false,
                'translations'=>'{"en": "Notes", "sl": "Opombe"}'
            ],
            [
                'key_name_id' => '_location_data.additional.lname',
                'key_data_type_id' => 'text',
                'required' => false,
                'translations'=>'{"en": "Local name", "sl": "Lokalno ime"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ],
            [
                'key_name_id' => 'response_type',
                'key_data_type_id' => 'code_list_reference',
                'ref'=> 'howling_response_type',
                'required' => true,
                'translations'=>'{"en": "Response type", "sl": "Tip odziva"}'
            ],
            [
                'key_name_id'=>'_location',
                'key_data_type_id'=>'location_geometry',
                'required' => true,
                'importable' => true,
                'filterable' => false,
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ],
            [
                'key_name_id'=>'_location_data',
                'translations' =>'{"en": "Location", "sl": "Lokacija"}',
                'key_data_type_id'=>'location_data_json',
                'filterable'=>false,
                'required' => true, 'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ]
            ], 'howling', 'modules', ['required' => true, 'visible_in_cv_detail'=>true, 'visible_in_cv_grid'=>true, 'filterable' => true, 'importable' => true]);
            self::updateModuleDatabaseSchema('howling');
        }

    /**
     * Add module roles
     */
    static function patch_1() {

        //ADD howling groups
        foreach(Mbase2Utils::moduleRoles('howling') as $role) {
            self::catchQuery("INSERT INTO laravel.groups(slug,name,group_type_id) values (:slug, :name, (select id from laravel.group_types WHERE slug='MBASE2-MODULE-ROLES'))",
            [
                ':slug'=>$role,
                ':name' => json_encode(['en'=>'Howling '.str_replace(['mbase2','-howling-'],'',$role)])
            ]);
        }

        //ADD howling module
        self::catchQuery("insert into laravel.mbase2l_modules(slug, name, properties, enabled) values('howling','howling','{}', true)");

        $res = \DB::select("SELECT id from laravel.mbase2l_modules WHERE slug='howling'");
        $moduleId = $res[0]->id;

        /**
         * Connect groups and roles
         */
        foreach(Mbase2Utils::moduleRoles('howling') as $role) {
            $res = \DB::select("SELECT id from laravel.groups WHERE slug=:slug",[':slug'=>$role]);
            $groupId = $res[0]->id;
            self::catchQuery("INSERT INTO laravel.groups_mbase2l_modules(group_id, mbase2l_module_id) values(:group_id,:module_id)",[':group_id'=>$groupId, ':module_id'=>$moduleId]);
        }

        //connect howling groups with countries
        self::catchQuery("insert into laravel.groups_group_types_countries (group_id,group_type_country_id)
        SELECT g2.id as group_id, g.id as group_type_country_id FROM laravel.groups g, laravel.group_types gt, laravel.groups g2 WHERE g.group_type_id = gt.id and gt.slug='COUNTRIES' and g2.slug like '%-howling-%'");
    }

    /**
     * Add module definition
     */
    static function patch_0() {
        self::addCodeListOption('modules', 'howling', ['en' => 'Howling', 'sl' => 'Izzivanje tuljenja']);
        self::catchQuery("INSERT INTO mbase2.modules(id) VALUES ((select id from mbase2.code_list_options_vw where list_key = 'modules' and key='howling'))");
    }

    static function howling_vw($dropView = true) {
        if ($dropView) {
            self::catchQuery("drop view mb2data.howling_vw");
        }

        self::catchQuery("CREATE OR REPLACE VIEW mb2data.howling_vw
        AS SELECT * FROM mb2data.howling");
    }
}