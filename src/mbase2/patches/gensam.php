<?php namespace patches;

use Exception;
use Mbase2Api;
use Mbase2Utils;
use Mbase2Database;
use Mbase2SchemaPatches;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');

class gensam extends Mbase2SchemaPatches  {
    static function patch_90() {
        \DB::statement("ALTER TABLE mb2data.gensam_log ALTER COLUMN user_id DROP NOT NULL;");
        \DB::statement("ALTER TABLE mb2data.gensam_log DROP CONSTRAINT fk_gensam;");
    }

    static function patch_89() {
        \DB::statement("CREATE VIEW mb2data.gensam_allele_labs_vw AS 
            SELECT id, _full_name
            FROM mb2data.gensam_organisations go
            WHERE EXISTS (
                SELECT 1
                FROM mb2data.gensam_imported_alleles gia
                WHERE go.id = gia.alab_id)
        ");
    }

    static function patch_88() {
        \DB::statement("ALTER TABLE mb2data.gensam_imported_alleles ADD
        COLUMN alab_id integer references mb2data.gensam_organisations(id)");

        \DB::update("UPDATE mb2data.gensam_imported_alleles SET alab_id=3");

        \DB::statement("ALTER TABLE mb2data.gensam_imported_alleles
            ALTER COLUMN alab_id SET NOT NULL;");
    }
    static function patch_87() {

        Mbase2Utils::SQLcreateIndex('mb2data','gensam_ref_animals', 'ref_animal');

        self::importVariables([[
            'key_name_id'=>'has_genotype',
            'key_data_type_id'=>'boolean',
            'visible_in_table'=>true,
            'filterable' => true,
            'weight_in_table' => 1.1,
            'translations' => json_encode(["sl" => "Ima genotip", "en" => "Has genotype"])
        ]], 'gensam');

        self::importVariables([[
            'key_name_id'=>'is_reference',
            'key_data_type_id'=>'boolean',
            'visible_in_table'=>true,
            'filterable' => true,
            'weight_in_table' => 1.2,
            'translations' => json_encode(["sl" => "Ref. genotip", "en" => "Ref. genotype"])
        ]], 'gensam');
        
    }

    static function patch_86() {
        self::gensam_vw();
    }

    static function patch_85() {
        $translations = json_decode(file_get_contents(__DIR__.'/data/gensamp_translations_v2.json'));
        
        foreach ($translations as $row) {
            \DB::update("UPDATE mbase2.module_variables SET translations=:translations WHERE id=:id",[':translations'=>json_encode($row->translations), ':id'=>$row->id]);
        }
    }

    static function patch_84() {
        self::catchQuery("CREATE TABLE IF NOT EXISTS mb2data.gensam_log (
            id SERIAL PRIMARY KEY,                       -- Unique ID for each audit entry
            gensam_id INT,                               -- Foreign key referencing the original gensam record
            operation_type VARCHAR(10) NOT NULL,         -- Type of operation: INSERT, UPDATE, DELETE
            old_values JSONB,                            -- Previous values of the record (for UPDATE/DELETE)
            new_values JSONB,                            -- New values of the record (for INSERT/UPDATE)
            user_id integer references laravel.users,    -- User or process that made the change
            changed_at TIMESTAMP DEFAULT NOW()           -- Timestamp of when the change was made
        );");

        self::catchQuery("CREATE OR REPLACE FUNCTION mb2data.log_gensam_changes()
            RETURNS TRIGGER AS \$\$
            BEGIN
                IF (TG_OP = 'UPDATE') THEN
                    INSERT INTO mb2data.gensam_log (gensam_id, operation_type, old_values, new_values, user_id)
                    VALUES (OLD.id, TG_OP, row_to_json(OLD), row_to_json(NEW), NEW.record_editor_id);

                ELSIF (TG_OP = 'DELETE') THEN
                    INSERT INTO mb2data.gensam_log (gensam_id, operation_type, old_values)
                    VALUES (OLD.id, TG_OP, row_to_json(OLD));
                END IF;
                RETURN NULL;
            END;
            \$\$ LANGUAGE plpgsql;");

        self::catchQuery("ALTER TABLE mb2data.gensam ADD COLUMN IF NOT EXISTS record_editor_id integer references laravel.users(id)");

        self::catchQuery("CREATE OR REPLACE FUNCTION mb2data.prevent_uid_change()
            RETURNS TRIGGER AS \$\$
            BEGIN
                NEW.record_editor_id := NEW._uid;
                NEW._uid := OLD._uid; -- Revert to the original value
                RETURN NEW; -- Return the updated row with `_uid` preserved
            END;
            \$\$ LANGUAGE plpgsql;");

        self::catchQuery("CREATE OR REPLACE TRIGGER preserve_uid_on_update
            BEFORE UPDATE
            ON mb2data.gensam
            FOR EACH ROW
            EXECUTE FUNCTION mb2data.prevent_uid_change();");

        self::catchQuery("CREATE OR REPLACE TRIGGER gensam_audit_trigger
            AFTER UPDATE OR DELETE
            ON mb2data.gensam
            FOR EACH ROW
            EXECUTE FUNCTION mb2data.log_gensam_changes();");
    }

    static function patch_83() {
        self::addCodeListOption('gensam_ui','Shared data filter');
        self::addCodeListOption('gensam_ui','Individuals filter');
        self::addCodeListOption('gensam_ui','Apply');
        self::addCodeListOption('gensam_ui','Reset');
        self::addCodeListOption('gensam_ui','Apply');
        self::addCodeListOption('gensam_ui','Reset');
        self::addCodeListOption('gensam_ui','Shared with me');
        self::addCodeListOption('gensam_ui','This option shows the data of other users shared with me or with organisations I am a member of.');
        self::addCodeListOption('gensam_ui','Shared by me');
        self::addCodeListOption('gensam_ui','This option shows the data uploaded by me and shared with other organisations or/and users.');
        self::addCodeListOption('gensam_ui','Uploaded by others');
        self::addCodeListOption('gensam_ui','This option shows the data uploaded by others and not shared with you - you can manage this data because you are a module administrator.');
        self::addCodeListOption('gensam_ui','Shared');
        self::addCodeListOption('gensam_ui','Individuals');
        self::addCodeListOption('gensam_ui','Export');
        self::addCodeListOption('gensam_ui','Download samples');
        self::addCodeListOption('gensam_ui','Download genotypes');
        self::addCodeListOption('gensam_ui','Samples from XLSX');
        self::addCodeListOption('gensam_ui','List of sample imports');
        self::addCodeListOption('gensam_ui','Genotypes from XLSX');
        self::addCodeListOption('gensam_ui','List of genotype imports');
        self::addCodeListOption('gensam_ui','Select individuals you would like to filter out of the shown dataset');
        self::addCodeListOption('gensam_ui','Data source properties');
        self::addCodeListOption('gensam_ui','Variable mappings');
        self::addCodeListOption('gensam_ui','Select a page in the uploaded xlsx file');
        self::addCodeListOption('gensam_ui','INSERT - append all the rows from the selected page to the database if possible');
        self::addCodeListOption('gensam_ui','UPDATE - ignore the rows with unmatched update ID and update only the defined columns');
        self::addCodeListOption('gensam_ui','How would you like to import the data?');
        self::addCodeListOption('gensam_ui','Select a variable holding an ID for update');
        self::addCodeListOption('gensam_ui','What format are the dates in?');
        self::addCodeListOption('gensam_ui','Apply import definition from previous import');
        self::addCodeListOption('gensam_ui','Apply saved import definition');
        self::addCodeListOption('gensam_ui','Saved import definitions');
        self::addCodeListOption('gensam_ui','Import');
        self::addCodeListOption('gensam_ui','Save import definition');
        self::addCodeListOption('gensam_ui','column');
        self::addCodeListOption('gensam_ui','fixed');
        self::addCodeListOption('gensam_ui','Sample properties');
        self::addCodeListOption('gensam_ui','Lab data attributes');
        self::addCodeListOption('gensam_ui','Auto generate');
        self::addCodeListOption('gensam_ui','latitude');
        self::addCodeListOption('gensam_ui','longitude');
        self::addCodeListOption('gensam_ui','CRS');
        self::addCodeListOption('gensam_ui','Import');
        self::addCodeListOption('gensam_ui','Select a page which holds the data for alleles');
        self::addCodeListOption('gensam_ui','Select a page which holds the data for genotypes');
        self::addCodeListOption('gensam_ui','Select analysis lab');
        self::addCodeListOption('gensam_ui','Sample codes follow the coding system in the database.');
        self::addCodeListOption('gensam_ui','Sample codes use a custom coding system.');
        self::addCodeListOption('gensam_ui','All the samples with a valid genotypes are going to be imported. Random database sample code will be generated and your sample code will be stored in the 2nd lab code field.');
        self::addCodeListOption('gensam_ui','username');
        self::addCodeListOption('gensam_ui','date');
        self::addCodeListOption('gensam_ui','source');
        self::addCodeListOption('gensam_ui','note');
        self::addCodeListOption('gensam_ui','Genotypes validation');
        self::addCodeListOption('gensam_ui','Empty sample code');
        self::addCodeListOption('gensam_ui','Sample code in wrong format');
        self::addCodeListOption('gensam_ui','Sample codes with denied access');
        self::addCodeListOption('gensam_ui','Sample codes with existing genotype');
        self::addCodeListOption('gensam_ui','Missing allele pair');
        self::addCodeListOption('gensam_ui','Samples without genotype');
        self::addCodeListOption('gensam_ui','Unreferenced allele name');
        self::addCodeListOption('gensam_ui','Same sample code - different genotype');
        self::addCodeListOption('gensam_ui','Same sample code - same genotype');
        self::addCodeListOption('gensam_ui','Referenced allele sequence validation');
        self::addCodeListOption('gensam_ui','Missing value');
        self::addCodeListOption('gensam_ui','Repeated sequence with different allele name');
        self::addCodeListOption('gensam_ui','Repeated allele name with different sequence');
        self::addCodeListOption('gensam_ui','Number of rows in allele seq. page');
        self::addCodeListOption('gensam_ui','Number of ALL valid and referenced rows in allele seq. page');
        self::addCodeListOption('gensam_ui','Number of UNIQUE valid and referenced rows in allele seq. page');
        self::addCodeListOption('gensam_ui','Number of allele seq. page alleles in the database before the import');
        self::addCodeListOption('gensam_ui','Number of imported referenced allele seq.');
        self::addCodeListOption('gensam_ui','Number of rows in genotypes page');
        self::addCodeListOption('gensam_ui','Number of genotypes with empty or invalid sample code');
        self::addCodeListOption('gensam_ui','Number of genotypes with different genotype data for the same sample code');
        self::addCodeListOption('gensam_ui','Number of multiplicated rows (including sample code) in the genotypes page');
        self::addCodeListOption('gensam_ui','Number of genotypes with at least one unreferenced allele');
        self::addCodeListOption('gensam_ui','Number of genotypes with one or more single alleles missing');
        self::addCodeListOption('gensam_ui','Number of genotypes for import');
        self::addCodeListOption('gensam_ui','Number of samples in the database with genotype data already present');
        self::addCodeListOption('gensam_ui','Number of samples with denied access');
        self::addCodeListOption('gensam_ui','Number of imported genotypes');
        self::addCodeListOption('gensam_ui','Number of samples in the database where the genotype data was added');
        self::addCodeListOption('gensam_ui','Number of inserted samples');
        self::addCodeListOption('gensam_ui','Download the allele mapping table');
        self::addCodeListOption('gensam_ui','Download the imported genotypes');
        self::addCodeListOption('gensam_ui','Successfully imported.');
        self::addCodeListOption('gensam_ui',"This could be because genotypes for some samples are either already present or you don't have the permissions to add genotype data to the respective samples.");
        self::addCodeListOption('gensam_ui','All the data was successfully imported.');
        self::addCodeListOption('gensam_ui','Import report');
    }

    /**
     * Test, query2 is 50x faster
     */
    static function patch_82() {
        return;
        $start_time = microtime(true);
        $rows = \DB::select("SELECT g.sample_code, b.* FROM (
            SELECT a.gensam_id, json_agg(marker) markers, json_agg(alleles) alleles from (
                SELECT gg.gensam_id, ga.marker, json_agg(ga.name ORDER BY ga.name) alleles FROM 
                    public.gensam_genotypes gg,
                    public.gensam_alleles ga
                WHERE gg.allele_id = ga.id
                GROUP BY marker, gensam_id 
                ORDER BY gensam_id,marker) a
            GROUP BY a.gensam_id) b
        LEFT JOIN public.gensam g on g.id = b.gensam_id
        WHERE g.user_id =505");

        echo "Elapsed time1: ".(microtime(true) - $start_time)." Row count:".count($rows)."\n";
        foreach($rows as $row) {
            echo $row->gensam_id." ";
        }

        $start_time = microtime(true);
        $rows = \DB::select("SELECT g.sample_code, b.* FROM (
            SELECT a.gensam_id, json_agg(marker) markers, json_agg(alleles) alleles from (
                SELECT gg.gensam_id, ga.marker, json_agg(ga.name ORDER BY ga.name) alleles FROM 
                    public.gensam_genotypes gg,
                    public.gensam_alleles ga,
                    public.gensam g
                WHERE gg.allele_id = ga.id and gg.gensam_id = g.id and g.user_id=505
                GROUP BY marker, gensam_id 
                ORDER BY gensam_id,marker) a
            GROUP BY a.gensam_id) b
        LEFT JOIN public.gensam g on g.id = b.gensam_id");

        echo "\nElapsed time2: ".(microtime(true) - $start_time)." Row count:".count($rows)."\n";
        foreach($rows as $row) {
            echo $row->gensam_id." ";
        }


    }

    /**
     * Test
     */

    static function patch_81() {
        return;
        set_time_limit(0);

        \DB::statement("drop table public.gensam_genotypes");
        \DB::statement("drop table public.gensam_alleles");
        \DB::statement("drop table public.gensam");
        \DB::statement("drop table public.user_table");
        return;
        
        if (!\Schema::hasTable('public.user_table')) {
            \Schema::create('public.user_table', function ($table) {
                $table->id();
                $table->string('name');
                $table->timestamps();
            });
        }

        if (!\Schema::hasTable('public.gensam')) 
        \Schema::create('public.gensam', function ($table) {
            $table->id();
            $table->string("sample_code");
            $table->foreignId('user_id')->constrained('public.user_table')->onDelete('cascade');
            $table->timestamps();
        });

        if (!\Schema::hasTable('public.gensam_alleles')) 
        \Schema::create('public.gensam_alleles', function ($table) {
            $table->id();
            $table->string("name");
            $table->string("marker");
            $table->string("seq");
            $table->timestamps();
        });

        if (!\Schema::hasTable('public.gensam_genotypes')) 
        \Schema::create('public.gensam_genotypes', function ($table) {
            $table->id();
            $table->foreignId('allele_id')->constrained('public.gensam_alleles')->onDelete('cascade');
            $table->foreignId('gensam_id')->constrained('public.gensam')->onDelete('cascade');
            $table->timestamps();
        });

        \DB::delete("DELETE FROM public.gensam_alleles");
        \DB::delete("DELETE FROM public.gensam_genotypes");
        \DB::delete("DELETE FROM public.gensam");
        \DB::delete("DELETE FROM public.user_table");
       
        // Seed user_table with 1000 users
        $users = [];
        for ($i = 1; $i <= 10000; $i++) {
            $users[] = ['id' => $i, 'name' => 'User ' . $i];
        }
        \DB::table('public.user_table')->insert($users);

        $gensam = [];
        for ($i = 1; $i <= 100000; $i++) {
            $gensam[] = ['id' => $i, 'sample_code' => uniqid(),'user_id' => rand(1,count($users)) ];
        }
        
        $chunks = array_chunk($gensam, 10000);

        foreach ($chunks as $chunk) {
            \DB::table('public.gensam')->insert($chunk);
        }

        $gensam_alleles = [];
        for ($i = 1; $i <= 100000; $i++) {
            $gensam_alleles[] = ['id' => $i, 'name'=>uniqid(), 'marker'=>uniqid(), 'seq'=>uniqid()];
        }

        $chunks = array_chunk($gensam_alleles, 10000);

        foreach ($chunks as $chunk) {
            \DB::table('public.gensam_alleles')->insert($chunk);
        }

        // Seed gensam_alleles with random rows (10-50 rows per gensam_genotypes)
        $gensam_genotypes = [];
        $gensam_genotypes_id = 1;
        foreach ($gensam as $inx => $t1) {
            $numRows = rand(10, 50);
            for ($i = 0; $i < $numRows; $i++) {
                $gensam_genotypes[] = [
                    'id' => $gensam_genotypes_id++,
                    'gensam_id' => $inx,
                    'allele_id' => rand(1, count($gensam_alleles)),
                    'gensam_id' => $t1['id']
                ];
            }
        }

        // Bulk insert gensam_alleles data
        $chunks = array_chunk($gensam_genotypes, 10000); // Chunk data for better performance
        foreach ($chunks as $chunk) {
            \DB::table('public.gensam_genotypes')->insert($chunk);
        }
    }

    static function patch_80() {
        ;
    }

    static function patch_79() {
        $rows = \DB::select("select distinct u.id as uid from laravel.users u, laravel.users_groups ug, laravel.groups g where u.id = ug.user_id and ug.group_id = g.id and g.slug like '%gensam%'");
        $user_ids = [];
        foreach($rows as $row) {
            $user_ids[] = $row->uid;
        }

        \DB::statement("ALTER TABLE mb2data.gensam_organisations
        ADD UNIQUE (_full_name);");

        \DB::statement("ALTER TABLE mb2data.gensam_organisations
        DROP COLUMN IF EXISTS user_ids");

        \DB::table('mb2data.gensam_organisations')->insert([
            "_full_name"=>'MBASE2_USERS',
            "gensam_organisation" => true,
            "samplers_organisation" => false,
            "gensam_organisations_users" => json_encode($user_ids)
        ]);
    }

    static function patch_78() {
        self::catchQuery("DROP TABLE IF EXISTS mb2data.gensam_genotypes");
        self::catchQuery("DROP TABLE IF EXISTS mb2data.gensam_imported_alleles");
        self::catchQuery("DROP TABLE IF EXISTS mb2data.gensam_alleles");

        self::catchQuery("CREATE TABLE mb2data.gensam_alleles (
            id bigserial primary key,
            name varchar not null,
            marker varchar not null,
            seq varchar not null,
            UNIQUE(marker, seq)
        )");

        Mbase2Database::createIndex('mb2data','gensam_alleles', 'name');
        Mbase2Database::createIndex('mb2data','gensam_alleles', 'marker');
        Mbase2Database::createIndex('mb2data','gensam_alleles', 'seq');
        \DB::statement("CREATE INDEX ON mb2data.gensam_alleles (marker, seq)");

        self::catchQuery("CREATE TABLE mb2data.gensam_imported_alleles (
            id bigserial primary key,
            inx integer not null,
            name varchar not null,
            marker varchar not null,
            seq varchar not null,
            allele_id bigint references mb2data.gensam_alleles(id) not null,
            batch_id integer references mbase2.import_batches(id) not null
        )");

        Mbase2Database::createIndex('mb2data','gensam_imported_alleles', 'name');
        Mbase2Database::createIndex('mb2data','gensam_imported_alleles', 'marker');
        Mbase2Database::createIndex('mb2data','gensam_imported_alleles', 'seq');
        Mbase2Database::createIndex('mb2data','gensam_imported_alleles', 'batch_id');
        Mbase2Database::createIndex('mb2data','gensam_imported_alleles', 'allele_id');
        \DB::statement("CREATE INDEX ON mb2data.gensam_imported_alleles (marker, seq)");

        \DB::statement("CREATE TABLE mb2data.gensam_genotypes (
            id bigserial primary key,
            allele_id bigint references mb2data.gensam_alleles(id) not null,
            gensam_id integer references mb2data.gensam(id) not null,
            batch_id integer references mbase2.import_batches(id) not null
        )");

        \DB::statement("CREATE INDEX ON mb2data.gensam_genotypes (allele_id)");
        \DB::statement("CREATE INDEX ON mb2data.gensam_genotypes (batch_id)");
        \DB::statement("CREATE INDEX ON mb2data.gensam_genotypes (allele_id, gensam_id)");
    }

    static function patch_77() {
        self::catchQuery("CREATE OR REPLACE FUNCTION mb2data.gensam_organisations_convert_array_to_numeric() 
        RETURNS TRIGGER AS \$\$
        DECLARE
            gensam_organisations_users jsonb;
            gensam_organisations_admins jsonb;
        BEGIN
            -- Convert gensam_organisations_users to numeric array if necessary
            IF NEW.gensam_organisations_users IS NOT NULL THEN
                -- Convert each element of the array to an integer
                gensam_organisations_users := jsonb_agg((elem->>0)::int)
                    FROM jsonb_array_elements(NEW.gensam_organisations_users) as elem;
                NEW.gensam_organisations_users := gensam_organisations_users;
            END IF;
        
            -- Convert gensam_organisations_admins to numeric array if necessary
            IF NEW.gensam_organisations_admins IS NOT NULL THEN
                -- Convert each element of the array to an integer
                gensam_organisations_admins := jsonb_agg((elem->>0)::int)
                    FROM jsonb_array_elements(NEW.gensam_organisations_admins) as elem;
                NEW.gensam_organisations_admins := gensam_organisations_admins;
            END IF;
        
            RETURN NEW;
        END;
        \$\$ LANGUAGE plpgsql;");

        self::catchQuery("CREATE OR REPLACE TRIGGER convert_array_to_numeric_trigger
        BEFORE INSERT OR UPDATE ON mb2data.gensam_organisations
        FOR EACH ROW
        EXECUTE FUNCTION mb2data.gensam_organisations_convert_array_to_numeric();");
    }

    static function patch_76() {
        self::addCodeListOption('gensam_ui','Shared with me', ['sl' => 'Deljeno z mano']);
        self::addCodeListOption('gensam_ui','Shared by me', ['sl' => 'Deljeni vzorci']);
        self::addCodeListOption('gensam_ui','Individuals', ['sl' => 'Osebki']);
        self::addCodeListOption('gensam_ui','All filters', ['sl' => 'Vsi filtri']);
    }

    static function patch_75() {
        self::addReferenceTable('mbase2_gensam_users','id', 'mb2data', 'name',
            "SELECT u.id, username || '(' || email || ')' as name FROM laravel.users u, laravel.users_groups ug, laravel.groups g where 
            u.id = ug.user_id and ug.group_id = g.id and g.slug like 'mbase2-gensam-%'"
        );

        self::importVariables([[
            'key_name_id'=>'user_access_ids',
            'key_data_type_id'=>'table_reference_array',
            'ref'=>'mbase2_gensam_users',
            'visible'=>true,
            'filterable' => true,
            'translations' => json_encode(["sl" => "Dostop za uporabnike", "en" => "User access"])
        ]], 'gensam');

        self::importVariables([[
            'key_name_id'=>'organisation_access_ids',
            'key_data_type_id'=>'table_reference_array',
            'ref'=>'gensam_organisations',
            'visible'=>true,
            'filterable' => true,
            'translations' => json_encode(["sl" => "Dostop za organizacije", "en" => "Organisation access"])
        ]], 'gensam');

        self::updateModuleDatabaseSchema('gensam', 'modules', 'user_access_ids');
        self::updateModuleDatabaseSchema('gensam', 'modules', 'organisation_access_ids');

        self::catchQuery("CREATE INDEX IF NOT EXISTS idx_user_access_ids_jsonb ON mb2data.gensam USING gin (user_access_ids jsonb_path_ops)");
        self::catchQuery("CREATE INDEX IF NOT EXISTS idx_organisation_access_ids_jsonb ON mb2data.gensam USING gin (organisation_access_ids jsonb_path_ops)");

        self::gensam_vw();

        self::catchQuery("CREATE OR REPLACE FUNCTION mb2data.gensam_convert_array_to_numeric() 
        RETURNS TRIGGER AS \$\$
        DECLARE
            user_access_ids_numeric jsonb;
            organisation_access_ids_numeric jsonb;
        BEGIN
            -- Convert user_access_ids to numeric array if necessary
            IF NEW.user_access_ids IS NOT NULL THEN
                -- Convert each element of the array to an integer
                user_access_ids_numeric := jsonb_agg((elem->>0)::int)
                    FROM jsonb_array_elements(NEW.user_access_ids) as elem;
                NEW.user_access_ids := user_access_ids_numeric;
            END IF;
        
            -- Convert organisation_access_ids to numeric array if necessary
            IF NEW.organisation_access_ids IS NOT NULL THEN
                -- Convert each element of the array to an integer
                organisation_access_ids_numeric := jsonb_agg((elem->>0)::int)
                    FROM jsonb_array_elements(NEW.organisation_access_ids) as elem;
                NEW.organisation_access_ids := organisation_access_ids_numeric;
            END IF;
        
            RETURN NEW;
        END;
        \$\$ LANGUAGE plpgsql;");

        self::catchQuery("CREATE OR REPLACE TRIGGER convert_array_to_numeric_trigger
        BEFORE INSERT OR UPDATE ON mb2data.gensam
        FOR EACH ROW
        EXECUTE FUNCTION mb2data.gensam_convert_array_to_numeric();");
    }

    static function patch_74() {
        self::importVariables([[
            'key_name_id'=>'gensam_organisations_users',
            'key_data_type_id'=>'table_reference_array',
            'ref'=>'mbase2_users',
            'visible'=>true,
            'translations' => json_encode(["sl" => "Člani organizacije", "en" => "Organisation members"])
        ]], 'gensam_organisations', 'referenced_tables');

        self::dropModuleVariable('gensam_organisations_admins', 'gensam_organisations');
        
        self::importVariables([[
            'key_name_id'=>'gensam_organisations_admins',
            'key_data_type_id'=>'table_reference_array',
            'ref'=>null,
            'visible'=>true,
            'translations' => json_encode(["sl" => "Administratorji organizacije", "en" => "Organisation admins"])
        ]], 'gensam_organisations', 'referenced_tables');

        
        self::updateModuleDatabaseSchema('gensam_organisations', 'referenced_tables', 'gensam_organisations_users');
        self::updateModuleDatabaseSchema('gensam_organisations', 'referenced_tables', 'gensam_organisations_admins');
    }

    static function patch_73() {
        self::updateVariables([
            [
                'key_name_id'=>'_uid',
                'importable'=>true
            ]
        ],'gensam');

        self::updateVariables([
            [
                'key_name_id'=>'ref_animal',
                'importable'=>true
            ]
        ],'gensam_ref_animals');

        foreach(['organisation','note'] as $key) {
            self::updateVariables([
                [
                    'key_name_id'=>$key,
                    'importable'=>true
                ]
            ],'gensam_samplers');
        }

        foreach(['study_name','study_description'] as $key) {
            self::updateVariables([
                [
                    'key_name_id'=>$key,
                    'importable'=>true
                ]
            ],'gensam_studies');
        }
    }

    static function patch_72() {
        self::catchQuery("create or replace view mb2data.gensam_labs_vw as
        select go.* from mb2data.gensam_organisations go, mbase2.code_list_options clo where go._type = clo.id and clo.key='lab'");

        self::addReferenceTable('gensam_labs_vw','id', 'mb2data', '_full_name');

        self::updateVariables([[
            'key_name_id'=>'analysing_lab',
            'key_data_type_id'=>'table_reference',
            'ref'=>'gensam_labs_vw'
        ]],'gensam');
    }

    static function patch_71() {
        self::catchQuery("UPDATE mb2data.gensam_organisations SET samplers_organisation=true, gensam_organisation=false where id not in (1,2,3)");
    }

    static function patch_70() {
        self::importVariables([[
                'key_name_id' => 'gensam_organisation',
                'key_data_type_id' => 'boolean',
                'visible' => true,
                'visible_in_table' => true,
                'default_value'=>true,
                'weight_in_table'=>100
            ],
            [
                'key_name_id' => 'samplers_organisation',
                'key_data_type_id' => 'boolean',
                'visible' => true,
                'visible_in_table' => true,
                'default_value'=>true,
                'weight_in_table'=>101
            ],
            [
                'key_name_id' => '_full_name',
                'weight_in_table'=>0.5
            ]
            ],'gensam_organisations','referenced_tables');

        foreach(['gensam_organisation', 'samplers_organisation'] as $cname) {
            self::catchQuery("ALTER TABLE mb2data.gensam_organisations ADD column $cname boolean default true");
        }

        self::catchQuery("CREATE OR REPLACE VIEW mb2data.gensam_organisation_vw as SELECT * from mb2data.gensam_organisations WHERE gensam_organisation=true");
        self::catchQuery("CREATE OR REPLACE VIEW mb2data.gensam_sampler_organisation_vw as SELECT * from mb2data.gensam_organisations WHERE samplers_organisation=true");

        self::addReferenceTable('gensam_organisation_vw','id', 'mb2data', '_full_name');
        self::addReferenceTable('gensam_sampler_organisation_vw','id', 'mb2data', '_full_name');

        self::updateVariables([[
            'key_name_id'=>'organisation',
            'key_data_type_id'=>'table_reference',
            'ref'=>'gensam_sampler_organisation_vw'
        ]],'gensam_samplers');

        self::updateVariables([[
            'key_name_id'=>'organisation',
            'key_data_type_id'=>'table_reference',
            'ref'=>'gensam_organisation_vw'
        ]],'gensam');
    }

    static function patch_69() {
        //gensam_samplers_unique_constraint
        self::addCodeListOption('location_types', 'country', ['en' => 'country', 'sl' => 'država']);

        self::addCodeListOption('code_lists','gensam_sampling_methods');

        foreach ([
            ['DETs', 'DETs'],
            ['ethanol','etanol'],
            ['swab','vatiranec'],
            ['whole scat', 'cel iztrebek'],
            ['silica dry', 'silika suho']
        ] as $item) {
            self::addCodeListOption('gensam_sampling_methods', $item[0],['en'=>$item[0], 'sl' => $item[1]]);
        }

        self::importVariables([[
            'key_name_id' => 'sample_properties.gensam_sampling_method',
            'key_data_type_id'=>'code_list_reference',
            'ref' => 'gensam_sampling_methods',
            'translations' => json_encode(['en'=>'Sampling method', 'sl' => 'Odvzem vzorca']),
            '_data'=>'{"sample_type": ["feces"]}',
            'visible' => true, 
            'filterable' => true,
            'importable' => true, 
            'visible_in_popup' => true
        ]], 'gensam');
    }

    static function patch_68() {
        self::updateVariables([
            [
                'key_name_id'=>'sample_properties.gensam_prey_sex',
                'key_data_type_id'=>'table_reference',
                'ref' => 'sex_list'
            ],
            [
                'key_name_id'=>'analysing_lab',
                'key_data_type_id'=>'table_reference',
                'ref' => 'gensam_organisations'
            ],
            [
                'key_name_id'=>'_data_quality',
                'key_data_type_id'=>'code_list_reference',
                'ref' => 'data_quality_options'
            ],
            [
                'key_name_id'=>'hybridization_level',
                'key_data_type_id'=>'code_list_reference',
                'ref' => 'gensam_hybridization_level'
            ]
        ], 'gensam');
    }

    static function patch_67() {

        self::updateVariables([
            [
                'key_name_id'=>'organisation', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'gensam_organisations'
            ],
            [
                'key_name_id'=>'_licence_name', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'licence_list'
            ],
            [
                'key_name_id'=>'genetic_species', 
                'key_data_type_id'=>'code_list_reference',
                'ref' => 'gensam_genetic_species'
            ],
            [
                'key_name_id'=>'genetic_sex', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'sex_list'
            ]
            ], 'gensam');

            self::updateVariables([[
                'key_name_id' => 'analysing_lab',
                'visible' => true
            ]], 'gensam');

            self::catchQuery("update mbase2.module_variables set _data = :_data where id = (select id from mbase2.module_variables_vw mvv where module_name='gensam' and variable_name = 'sample_properties.gensam_microlocation')",
                [
                    ':_data'=>json_encode([
                        "sample_type" => ["hair","feces"]
                    ])
                ]);
        
            self::catchQuery("update mbase2.module_variables set weight=(select weight + 0.01 from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name ='event_date')
                where id = (select id from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name = 'samplers')");

            foreach (['ref_animal_id',
                'analysis_status',
                'analysis_result',
                'quality_index',
                'genetic_species',
                'gensam_populations_id',
                'genetic_sex',
                'extraction_date',
                'results_date',
                'lab_notes'] as $i => $key) {
                    self::updateVariables([
                        [
                        'key_name_id' => $key,
                        'weight' => $i + 110
                        ]
                    ],'gensam');
                }

                foreach(['sample_properties.gensam_sample_age',
                'sample_properties.gensam_thickness',
                'sample_properties.gensam_track_size',
                'sample_properties.gensam_number_of_animals',
                'sample_properties.gensam_microlocation'] as $i=>$key) {
                    self::updateVariables([
                        [
                        'key_name_id' => $key,
                        'weight' => $i+1
                        ]
                    ],'gensam');
                }

    }

    static function patch_66() {
        /**
         * this is needed to automatically show code list in mbase2/admin/code-lists?m=gensam
         */
        self::importVariables([[
            'key_name_id' => 'gensam_ui_translations',
            'key_data_type_id'=>'code_list_reference',
            'ref' => 'gensam_ui'
        ]], 'gensam');
    }

    static function patch_65() {
        self::addCodeListOption('code_lists','gensam_ui');
        
        self::addCodeListOption('gensam_ui','Genetic samples', ['sl' => 'Genetski vzorci']);
        self::addCodeListOption('gensam_ui','Admin settings', ['sl' => 'Nastavitve administratorja']);
        self::addCodeListOption('gensam_ui','New sample', ['sl' => 'Nov vzorec']);
        self::addCodeListOption('gensam_ui','Filter', ['sl' => 'Filter']);

        self::addCodeListOption('gensam_ui','General data', ['sl' => 'Splošni podatki']);
        self::addCodeListOption('gensam_ui','General data attributes', ['sl' => 'Atributi splošnih podatkov']);
        self::addCodeListOption('gensam_ui','Field data', ['sl' => 'Terenski podatki']);
        self::addCodeListOption('gensam_ui','Field data attributes', ['sl' => 'Atributi terenskih podatkov']);
        self::addCodeListOption('gensam_ui','Sample properties', ['sl' => 'Lastnosti vzorca']);
        self::addCodeListOption('gensam_ui','Laboratory data', ['sl' => 'Laboratorijski podatki']);
        self::addCodeListOption('gensam_ui','Lab data attributes', ['sl' => 'Atributi laboratorijskih podatkov.']);
        self::addCodeListOption('gensam_ui','Link to other MBASE module', ['sl' => 'Povezava do drugega modula MBASE']);
        self::addCodeListOption('gensam_ui','Select module to link with', ['sl' => ' Izberite modul za povezavo']);
        self::addCodeListOption('gensam_ui','Some required attributes are not entered.', ['sl' => '']);
        self::addCodeListOption('gensam_ui','This record is COMPLETED. For editing you have to unlock it.', ['sl' => '']);
        self::addCodeListOption('gensam_ui','This record is UNCOMPLETED.', ['sl' => '']);
        self::addCodeListOption('gensam_ui','All required attributes are entered.', ['sl' => '']);
        self::addCodeListOption('gensam_ui','new', ['sl' => '']);

        self::addCodeListOption('gensam_ui','Module settings');
        self::addCodeListOption('gensam_ui','Reference animals');
        self::addCodeListOption('gensam_ui','Samplers');
        self::addCodeListOption('gensam_ui','Studies');
        self::addCodeListOption('gensam_ui','Populations');
        self::addCodeListOption('gensam_ui','Labs and other organisations');
        self::addCodeListOption('gensam_ui','Code lists');
        self::addCodeListOption('gensam_ui','edit');
        self::addCodeListOption('gensam_ui','New');
        self::addCodeListOption('gensam_ui','Edit');
        self::addCodeListOption('gensam_ui','Complete & Save & New');
        self::addCodeListOption('gensam_ui','Complete and save this entry and start a new one');
        self::addCodeListOption('gensam_ui','Save');
        self::addCodeListOption('gensam_ui','Save template');
        self::addCodeListOption('gensam_ui','Reset template');
        self::addCodeListOption('gensam_ui','Template');
    }

    static function patch_64() {
        self::updateVariables([[
            'key_name_id' => 'completed',
            'help' => json_encode([
                'en'=>'COM - completed entries with all required attributes; UNC- not all required attributes are entered',
                'sl'=>'COM - vneseni so vsi obvezni podatki; UNC- vneseni niso vsi obvezni podatki'
            ])
        ]],'gensam');
    }

    static function patch_63() {
        foreach([
            'crs'=>[
                'translations' => [
                    'en' => 'Coordinate reference system',
                    'sl' => 'Koordinatni sistem'
                ],
                'help' => [
                    'en' => 'Choose the coordinate reference system, that your coordinates are in. If it is not available, we suggest changing the coordinates to WGS84 (EPSG: 4326) or selecting the location on the map.',
                    'sl' => 'Izberite referenčni koordinatni sistem, v katerem so vaše koordinate. Če ta ni na voljo, predlagamo, da koordinate spremenite v WGS84 (EPSG: 4326) ali izberete lokacijo na zemljevidu.'
                ]
            ],
            'dms'=>[
                'translations' => [
                    'en' => 'DMS input',
                    'sl' => 'DMS vnos'
                ],
                'help' => [
                    'en' => 'Input coordinates in degrees, minutes and seconds.',
                    'sl' => 'Vnos v kotnih stopinjah, minutah in sekundah.'
                ]
            ]
        ] as $key => $prop){
            self::importVariables([[
                'key_name_id' => $key,
                'key_data_type_id'=>'text',
                'translations' => json_encode($prop['translations']),
                'help' => json_encode($prop['help'])
            ]], 'gensam');
        };
    }

    static function patch_62() {
        self::updateVariables([[
            'key_name_id' => 'samplers',
            'weight' => 110
        ]], 'gensam');

        self::catchQuery("update mbase2.module_variables set weight=(select weight + 0.1 from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name like '%species_name%')
        where id = (select id from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name = 'gensam_populations_id')");
    }

    static function patch_61() {
        self::importVariables([[
            'key_name_id' => 'user_id',
            'key_data_type_id' => 'table_reference',
            'ref' => 'mbase2_users',
            'visible'=>true,
            'unique_constraint'=>true,
            'visible_in_table'=>true,
            'importable'=>true
        ]],'gensam_samplers', 'referenced_tables');

        self::updateVariables([[
            'key_name_id'=>'slug',
            'importable'=>true
        ]],'gensam_samplers');

        Mbase2Database::updateSchema('gensam_samplers');

        self::catchQuery("ALTER TABLE mb2data.gensam_samplers ADD CONSTRAINT gensam_samplers_user_id_fkey FOREIGN KEY (user_id) references laravel.users(id)");

        self::catchQuery("alter table mb2data.gensam_samplers drop constraint gensam_samplers_slug_unique_constraint");
        self::catchQuery("CREATE UNIQUE INDEX gensam_samplers_slug_unique_constraint on mb2data.gensam_samplers using btree (LOWER(slug));");
    }

    static function patch_60() {
        self::catchQuery("update mb2data.gensam set _uid=7 where _uid=1");
    }

    static function patch_59() {
        foreach(['gensam_sample_age'=>'How old (in days) is the sample?',
        'sample_properties.gensam_damage_saliva_from' => 'Which animal was supposedly responsible for the damage?',
        'sample_properties.gensam_rain' => 'Did it rain since the animal was killed?',
        'sample_properties.gensam_prey' => 'What is the species of the killed animal?',
        'sample_properties.gensam_collected_from' => 'From which part of the killed animal was the sample taken.',
        'sample_properties.gensam_decomposition_level' => '0 - fresh; 5 - old',
        'sample_properties.gensam_damage_origin' => 'The date on which the damage occurred.',
        'sample_properties.tissue' => 'The type of tissue collected.',
        'sample_properties.storage_method' => 'How was the tissue stored?'] as $key => $value) {
            self::updateVariables([[
                'key_name_id'=>$key,
                'help'=>json_encode(['en'=>$value])
            ]],'gensam');
        }


        foreach(['sample_properties.gensam_sample_age' => 'Sample age',
        'sample_properties.gensam_number_of_animals' => 'Number of animals',
        'sample_properties.gensam_track_size' => 'Track size',
        'sample_properties.gensam_thickness' => 'Thickness',
        'sample_properties.gensam_sample_age' => 'Sample age',
        'sample_properties.gensam_number_of_animals' => 'Number of animals',
        'sample_properties.gensam_track_size' => 'Track size',
        'sample_properties.gensam_damage_naturalprey' => 'Damage/natural prey',
        'sample_properties.gensam_number_of_animals' => 'Number of animals'] as $key => $value) {
            self::updateVariables([[
                'key_name_id'=>$key,
                'translations'=>json_encode(['en'=>$value])
            ]],'gensam');
        }

        self::updateVariables([[
            'key_name_id'=>'sample_properties.gensam_prey_sex',
            'key_data_type_id'=>'table_reference',
            'ref'=>'sex_list'
        ]],'gensam');

        self::addCodeListOption('code_lists','gensam_damage_naturalprey_cl');
        self::addCodeListOption('gensam_damage_naturalprey_cl','damage');
        self::addCodeListOption('gensam_damage_naturalprey_cl','natural prey');
        
        self::updateVariables([[
            'key_name_id'=>'sample_properties.gensam_damage_naturalprey',
            'key_data_type_id'=>'code_list_reference',
            'ref'=>'gensam_damage_naturalprey_cl'
        ]],'gensam');
    }

    static function patch_58() {
        self::updateVariables([[
            'key_name_id' => 'sample_properties',
            'importable' => false
        ]], 'gensam');
    }

    static function patch_57() {
        $rows = \DB::select("SELECT * from mbase2.module_variables WHERE id in (SELECT id from mbase2.module_variables_vw where module_name like 'gensam_%_properties')");
        $variables = [];

        $res = \DB::select("select id from mbase2.code_list_options_vw clov where list_key='modules' and key='gensam'");

        $gensamModuleId = $res[0]->id;

        //https://stackoverflow.com/questions/39261409/postgres-group-by-on-jsonb-inner-field

        $res = \DB::select("select key_name_id, json_agg(a) _data from (
            select key_name_id, replace(replace(module_name,'gensam_',''),'_properties','') a from mbase2.module_variables_vw mvv where module_name like 'gensam%_%properties'
            ) t group by key_name_id ");

        $data = [];

        foreach($res as $row) {
            $data[$row->key_name_id] = $row->_data;
        }

        foreach($rows as $row) {
            $key = $row->key_name_id;
            if (isset($variables[$key])) {
                continue;
            }

            $row->module_id = $gensamModuleId;
            $row->key_name_id = 'sample_properties.'.$key;
            $row->_data = '{"sample_type":'.$data[$key].'}';

            unset($row->id);

            Mbase2Database::insert('mbase2.module_variables', (array)$row);

            $variables[$key] = true;
        }

        self::catchQuery("delete from mbase2.module_variables where id in (
            select id from mbase2.module_variables_vw mvv where module_name like 'gensam_%_properties')");

        self::catchQuery("update mbase2.module_variables set visible=true, visible_in_table = false, importable=true where key_name_id like 'sample_properties.%';");

    }

    static function patch_56() {

        self::importVariables([[
            'key_name_id' => 'samplers',
            'ref' => 'gensam_samplers'
        ]],'gensam');
    }

    static function patch_55() {
        \DB::update("UPDATE mb2data.gensam SET sample_properties=null where jsonb_typeof(sample_properties) = 'array'");
    }

    static function patch_54() {
        \DB::update("UPDATE mb2data.gensam set _uid=1 where _uid is null");
        self::catchQuery("ALTER TABLE mb2data.gensam ALTER COLUMN _uid SET NOT NULL");
    }

    static function patch_53() {
        $rows = \DB::select("SELECT * FROM mb2data.gensam");
        $attributes = \DB::select("SELECT * from mbase2.module_variables_vw WHERE module_name='gensam'");

        $requiredAttributes=[];

        foreach($attributes as $a) {
            if ($a->required) {
                $requiredAttributes[] = $a;
            }
        }

        foreach($rows as $row) {
            if (Mbase2Utils::required_attribute_values($row, $requiredAttributes,'') && !$row->completed) {
                \DB::update("UPDATE mb2data.gensam SET completed=true where id=:id",[':id'=>$row->id]);
            }
        }
    }

    static function patch_52() {
        self::catchQuery("update mbase2.module_variables set key_name_id = '_location_data.additional.lname' where id in (
            select id from mbase2.module_variables_vw where module_name in ('gensam') and variable_name='_lname')");
    }

    static function patch_51() {
        self::gensam_vw();
    }

    static function patch_50() {
        self::updateVariables([[
            'key_name_id' => '_ltype',
            'required' => false
        ]],'gensam');
    }

    static function patch_49() {
        self::gensam_vw();
    }

    static function patch_48() {
        self::catchQuery("CREATE OR REPLACE FUNCTION mbase2.gensam_update_jsonb() RETURNS trigger
            LANGUAGE plpgsql
            AS \$\$
            declare
			a jsonb;
        BEGIN
        NEW.sample_properties := COALESCE(OLD.sample_properties::jsonb||NEW.sample_properties::jsonb, OLD.sample_properties::jsonb, NEW.sample_properties::jsonb);
        a := COALESCE((old._location_data->'additional')::jsonb || (new._location_data->'additional')::jsonb, (old._location_data->'additional')::jsonb, (new._location_data->'additional')::jsonb);
       	NEW._location_data := COALESCE(OLD._location_data::jsonb || NEW._location_data::jsonb, OLD._location_data::jsonb, NEW._location_data::jsonb);
        if a is not null then
        	NEW._location_data := jsonb_set(NEW._location_data::jsonb, '{additional}', a::jsonb);
        end if;
        RETURN NEW;
        END;
        \$\$;");
        self::catchQuery("create trigger mb2data_gensam_update_jsonb before
        update
            on
        mb2data.gensam for each row execute function mbase2.gensam_update_jsonb()");
    }

    static function patch_47() {
        self::catchQuery("delete from mbase2.module_variables where id in (select id from mbase2.module_variables_vw where module_name = 'dmg_batches')");
        self::catchQuery("DROP TABLE IF EXISTS mb2data.dmg_batches");
        self::catchQuery("delete from mbase2.code_list_options where key='dmg_batches'");
    }

    static function patch_46() {
        self::gensam_vw();        
    }

    static function patch_45() {
        self::updateVariables([[
            'key_name_id' => 'samplers',
            'required' => false
        ]],'gensam');
    }

    static function patch_44() {

    foreach(['gensam_blood_properties', 'gensam_saliva_direct_properties'] as $rtname)
        self::importVariables([[
            'key_name_id'=>'sex_recorded',
            'key_data_type_id' => 'table_reference',
            'ref'=>'sex_list',
            'filterable'=>true,
            'importable'=>true,
            'visible'=>true,
            'translations'=>['en'=>'Recorded sex']
        ]],$rtname,'referenced_tables');	
    }

    static function patch_43() {
        
        foreach(['population_name', 'note'] as $i=>$key){
            self::updateVariables([
            [
                'key_name_id' => $key,
                'visible_in_table'=>true,
                'weight'=>$i+1,
                'weight_in_table'=>$i+1
            ]
            ],'gensam_populations');
        }
    }

    static function patch_42() {
        self::updateVariables([
            [
                'key_name_id' => 'gensam_populations_id',
                'weight'=>10.1
            ]
            ],'gensam');
    }

    static function patch_41() {
        self::importVariables([[
            'key_name_id' => 'genetic_sex',
            'key_data_type_id' => 'table_reference',
            'translations'=>['en'=>'Genetic sex'],
            'ref'=>'sex_list',
            'filterable'=>true,
            'dbcolumn'=>true,
            'importable'=>true,
            'visible'=>true,
            'weight'=>103.1,
            'section_in_form'=>'lab'
        ]]);
        self::updateModuleDatabaseSchema('gensam','modules','genetic_sex');
    }

    static function patch_40() {
        self::importVariables([[
            'key_name_id' => 'gensam_populations_id',
            'key_data_type_id' => 'table_reference',
            'translations'=>['en'=>'Population', 'sl'=>'Populacija'],
            'ref'=>'gensam_populations',
            'filterable'=>true,
            'dbcolumn'=>true,
            'importable'=>true,
            'visible'=>true,
            'visible_in_popup'=>true
        ]]);

        self::updateModuleDatabaseSchema('gensam','modules','gensam_populations_id');
    }

    static function patch_39() {
        self::addCodeListOption('referenced_tables','gensam_populations');
        self::importVariables([[
            'key_name_id'=>'population_name',
            'key_data_type_id'=>'text',
            'translations'=>['en'=>'Population name', 'sl'=>'Ime populacije'],
            'required'=>true,
            'unique_constraint'=> true,
            'importable'=>true,
            'visible'=>true
        ],
        [
            'key_name_id'=>'note',
            'key_data_type_id'=>'text',
            'required'=>false,
            'importable'=>true,
            'visible'=>true
        ]],'gensam_populations','referenced_tables');

        self::addReferenceTable('gensam_populations','id', 'mb2data', 'population_name');

        self::updateModuleDatabaseSchema('gensam_populations','referenced_tables');

        self::catchQuery("CREATE UNIQUE INDEX gensam_populations_name_idx ON mb2data.gensam_populations (UPPER(population_name));");
    }

    static function patch_38() {
        self::addCodeListOption('code_lists','location_types');

        self::addCodeListOption('location_types','no location');
        self::addCodeListOption('location_types','no data');
        self::addCodeListOption('location_types','GPS');
        self::addCodeListOption('location_types','grid');
        self::addCodeListOption('location_types','hunting area');
        self::addCodeListOption('location_types','toponym');

        self::importVariables([[
            'key_name_id' => '_ltype',
            'key_data_type_id' => 'code_list_reference',
            'ref' => 'location_types'
        ]]);

        self::updateVariables([[
            'key_name_id' => '_ltype',
            'importable' => true
        ]], 'gensam');
    }

    static function patch_37() {
        self::catchQuery("update mbase2.module_variables set section_in_form = 'lab' where id = (
            select id from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name = 'ref_animal_id')");
    }

    static function patch_36() {
        self::catchQuery("update mbase2.module_variables set importable = true where id in (
            select id from mbase2.module_variables_vw mvv where module_name = 'gensam' and section_in_form = 'lab')");
    }

    static function patch_35() {
        self::catchQuery("alter table mbase2.organisations rename to gensam_organisations");
        self::catchQuery("alter table mbase2.studies rename to gensam_studies");
        
        self::catchQuery("alter table mbase2.gensam_organisations set schema mb2data");
        self::catchQuery("alter table mbase2.gensam_studies set schema mb2data");

        self::catchQuery("update mbase2.referenced_tables set schema='mb2data' where id=(
            select id from mbase2.referenced_tables_vw rtv where key='studies')");
            
        self::catchQuery("update mbase2.referenced_tables set schema='mb2data' where id=(
            select id from mbase2.referenced_tables_vw rtv where key='organisations')");

        self::catchQuery("update mbase2.code_list_options set key='gensam_studies' where id=(
                select id from mbase2.code_list_options_vw where key='studies' and list_key='referenced_tables')");
                
        self::catchQuery("update mbase2.code_list_options set key='gensam_organisations' where id=(
                select id from mbase2.code_list_options_vw where key='organisations' and list_key='referenced_tables')");

        self::catchQuery("ALTER TABLE mb2data.gensam ADD CONSTRAINT gensam_organisation_fkey FOREIGN KEY (organisation) REFERENCES mb2data.gensam_organisations(id)");

        self::catchQuery("update mbase2.module_variables set importable = true where id in (
            select id from mbase2.module_variables_vw where module_name ='gensam_studies')");

        self::catchQuery("DELETE FROM mb2data.gensam_studies");

        self::catchQuery("update mbase2.module_variables set visible=true where id=(
            select id from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name = 'ref_animal_id')");

        self::catchQuery("update mbase2.module_variables set importable=true where id=(
            select id from mbase2.module_variables_vw mvv where module_name = 'gensam_ref_animals' and variable_name = 'ref_animal')");
    }

    static function patch_34() {
        self::catchQuery("update mbase2.module_variables set importable = true where id = (
            select id from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name like 'quality_index')");
    }

    static function patch_33() {
        self::catchQuery("delete from mb2data.gensam");
        self::catchQuery("delete from mb2data.gensam_ref_animals");
        self::catchQuery("delete from mb2data.gensam_samplers");
        self::catchQuery("delete from mbase2.import_batches where key_module_id = 'gensam'");
    }

    static function patch_32() {
        foreach(['extraction_date',
                'analysis_status',
                'analysis_result',
                'results_date',
                'quality_index',
                'genetic_species',
                'hybridization_level',
                'hybridization_method_detection',
                'lab_notes'] as $i=>$key_name_id) {
                    self::updateVariables([
                        [
                            'key_name_id' => $key_name_id,
                            'weight'=> 100 + $i
                        ]],'gensam');   
                }
    }

    static function patch_31() {
        self::catchQuery("update mbase2.module_variables set required=false where id in (
            select id from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name in ('hybridization_method_detection',
            'genetic_species',
            'hybridization_level',
            'nuts'))");
    }

    static function patch_30() {
        self::catchQuery("update mbase2.module_variables set section_in_form = 'lab' where id in (
            select id from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name in (
            'hybridization_method_detection',
            'genetic_species',
            'hybridization_level',
            'analysis_status',
            'analysis_result',
            'results_date',
            'quality_index',
            'lab_notes',
            'extraction_date'))");
    }

    static function patch_29() {
        self::gensam_vw();
    }

    static function patch_28() {
        self::importVariables([
            [
                'key_name_id'=>'hybridization_level', 
                'key_data_type_id' => 'code_list_reference',
                'ref' => 'gensam_hybridization_level',
                'filterable' => true,
                'visible' => true,
                'importable' => true,
                'visible_in_table' => false,
                'visible_in_popup' => false,
                'translations' => json_encode(["sl" => "Stopnja križanja","en" => "Hybridization level"])
            ],
            [
                'key_name_id'=>'genetic_species', 
                'key_data_type_id' => 'code_list_reference',
                'ref' => 'gensam_genetic_species',
                'filterable' => true,
                'visible' => true,
                'importable' => true,
                'visible_in_table' => false,
                'visible_in_popup' => false,
                'translations' => json_encode(["sl" => "Genetska vrsta","en" => "Genetic species"])
            ],
            [
                'key_name_id'=>'hybridization_method_detection', 
                'key_data_type_id' => 'text',
                'filterable' => true,
                'visible' => true,
                'importable' => true,
                'visible_in_table' => false,
                'visible_in_popup' => false,
                'translations' => json_encode(["en" => "Hybridization method detection"])
            ]
            ], 'gensam');

            self::catchQuery("ALTER TABLE mb2data.gensam ADD column hybridization_level integer references mbase2.code_list_options(id)");
            self::catchQuery("ALTER TABLE mb2data.gensam ADD column genetic_species integer references mbase2.code_list_options(id)");
            self::catchQuery("ALTER TABLE mb2data.gensam ADD column hybridization_method_detection varchar");
    }

    static function patch_27() {
        self::addCodeListOption('code_lists','gensam_hybridization_level');
        self::addCodeListOption('gensam_hybridization_level','F1');
        self::addCodeListOption('gensam_hybridization_level','F2');
        self::addCodeListOption('gensam_hybridization_level','WBX1');
        self::addCodeListOption('gensam_hybridization_level','WBX2');
        self::addCodeListOption('gensam_hybridization_level','DBX1');
        self::addCodeListOption('gensam_hybridization_level','DBX2');

        self::addCodeListOption('code_lists','gensam_genetic_species');
        self::addCodeListOption('gensam_genetic_species','hybrid', '{"en": "Hybrid", "sl": "Križanec"}');
        self::addCodeListOption('gensam_genetic_species','other_species', '{"en": "Other", "sl": "Drugo"}');
        self::addCodeListOption('gensam_genetic_species','Ursus arctos', '{"sl": "Rjavi medved", "en":"Brown bear"}');
        self::addCodeListOption('gensam_genetic_species','Canis lupus',	'{"sl": "Volk", "en": "Wolf"}');
        self::addCodeListOption('gensam_genetic_species','Canis aureus', '{"sl": "Zlati šakal"}');
        self::addCodeListOption('gensam_genetic_species','Lynx lynx', '{"sl": "Evrazijski ris", "en": "Lynx"}');
    }

    static function patch_26() {
        self::importVariables([
            [
                'key_name_id'=>'nuts', 
                'key_data_type_id' => 'table_reference',
                'ref' => 'nuts',
                'filterable' => true,
                'visible' => false,
                'importable' => false,
                'visible_in_table' => false,
                'visible_in_popup' => false,
                'translations' => json_encode(["sl" => "NUTS regije","en" => "NUTS regions"])
            ]], 'gensam');
    }

    static function patch_25() {
        self::catchQuery("update mbase2.module_variables set filterable=false where id in (
            select id from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name in ('_location',
            '_location_data',
            'sample_properties',
            'module_link',
            'completed'))");
    }

    static function patch_24() {

        self::catchQuery("update mbase2.module_variables set key_name_id = 'ref_animal_id' where id = (
            select id from mbase2.module_variables_vw mv where key_name_id ='animal_ref' and module_name = 'gensam')");

        self::catchQuery("alter table mb2data.gensam rename column animal_ref to ref_animal_id");
    }

    static function patch_23() {
        self::catchQuery("update mbase2.module_variables set key_name_id = 'ref_animal' where id = (
            select id from mbase2.module_variables_vw mv where key_name_id ='animal_ref' and module_name = 'gensam_ref_animals')");

        self::catchQuery("alter table mb2data.gensam_ref_animals rename column animal_ref to ref_animal");

        self::catchQuery("update mbase2.referenced_tables set label_key='ref_animal' where id = (
            select id from mbase2.referenced_tables_vw rtv where key='gensam_ref_animals')");
    }

    static function patch_22() {
        self::catchQuery("CREATE FUNCTION mbase2.gensam_ref_animals_ref_animal_to_upper() RETURNS trigger
            LANGUAGE plpgsql
            AS \$\$
                    BEGIN
                    NEW.ref_animal := upper(NEW.ref_animal);
                    RETURN NEW;
                    END;
                    \$\$;");


        self::catchQuery("create trigger mb2data_gensam_ref_animals_ref_animal_to_upper before
            insert
                or
            update
                on
                mb2data.gensam_ref_animals for each row execute function mbase2.gensam_ref_animals_ref_animal_to_upper()");
    }


    static function patch_21() {
        self::catchQuery("update mbase2.code_list_options set key='gensam_ref_animals' where id=(
            select id from mbase2.code_list_options_vw where list_key='referenced_tables' and key='animals')");
            
        self::catchQuery("update mbase2.referenced_tables set schema='mb2data' where id = (
            select id from mbase2.referenced_tables_vw rtv where key='gensam_ref_animals')");

        self::catchQuery("alter table mbase2.animals set schema mb2data");
        self::catchQuery("alter table mb2data.animals rename to gensam_ref_animals");
    }

    static function patch_20() {
        self::catchQuery("DROP TABLE mb2data.gensam_links");
    }

    static function patch_19() {
        self::catchQuery("CREATE TABLE mb2data.gensam_links(
                gensam_id integer primary key references mb2data.gensam(id),
                table_schema varchar,
                table_name varchar,
                record_id integer)");
    }

    static function patch_18() {
        self::catchQuery("update mbase2.module_variables set visible_in_table = false, visible_in_popup = false where id in (select id from mbase2.module_variables_vw mvv where module_name = 'gensam' and variable_name like 'species_list_id')");
    }

    static function patch_17() {
        self::catchQuery("update mbase2.module_variables set required = false where id in (select id from mbase2.module_variables_vw mvv where variable_name = 'species_list_id' and module_name = 'gensam')");
    }

    static function patch_16() {
        foreach(['_location','completed','date_record_created','_uid','date_record_modified'] as $key_name_id) {
            self::updateVariables([
                [
                    'key_name_id' => $key_name_id,
                    'importable' => false
                ]],'gensam');    
        }

        $w=1;

        foreach([
        'sample_code',
        'event_date',
        '_location_data',
        '_sample_type',
        'samplers',
        '_licence_name',
        'organisation'] as $key_name_id) {
            self::updateVariables([
                [
                    'key_name_id' => $key_name_id,
                    'weight_in_import' => $w++
                ]],'gensam');    
        }

    }

    static function patch_15() {
        self::updateVariables([[
            'key_name_id'=>'samplers',
            'ref'=>'gensam_samplers'
        ]],'gensam');
    }

    static function patch_14() {
        $res = \DB::update("update mbase2.code_list_options set translations = :trans
            where id = (select id from mbase2.code_list_options_vw clov where list_key='gensam_sample_type_cl' and key='saliva_noninvasive')", [
                ':trans' => json_encode(['en'=>'Saliva - noninvasive', 'sl'=>'Slina - neinvazivno'])
        ]);

        $res = \DB::update("update mbase2.code_list_options set translations = :trans
            where id = (select id from mbase2.code_list_options_vw clov where list_key='gensam_sample_type_cl' and key='feces')", [
                ':trans' => json_encode(['en'=>'feces', 'sl'=>'iztrebek'])
        ]);
    }

    static function patch_13() {
        self::updateVariables([[
            'key_name_id' => 'sex_recorded',
            'key_data_type_id' => 'table_reference',
            'ref' => 'sex_list'
        ]], 'gensam_tissue_properties');
    }

    static function patch_12() {
        self::addCodeListOption('gensam_sample_type_cl', 'snow_tracks', ['en'=>'Snow tracks', 'sl'=>'Sledi v snegu']);

        self::addCodeListOption('referenced_tables', 'gensam_snow_tracks_properties');

        self::importVariables([
            [
                'key_name_id'=>'photographed', 
                'key_data_type_id'=>'boolean',
                'translations' => json_encode(["sl" => "Fotografirano","en" => "Photographed"])
            ],
            [
                'key_name_id'=>'collection_time', 
                'key_data_type_id'=>'timestamp',
                'translations' => json_encode(["en" => "Time of the collection"])
            ],
            [
                'key_name_id'=>'snow_temp', 
                'key_data_type_id'=>'real',
                'translations' => json_encode(["en" => "Snow temperature"])
            ],
            [
                'key_name_id'=>'last_snfl_time', 
                'key_data_type_id'=>'timestamp',
                'translations' => json_encode(["en" => "Time of the last snowfall"])
            ],
            [
                'key_name_id'=>'snow_depth', 
                'key_data_type_id'=>'real',
                'translations' => json_encode(["en" => "Snow depth"])
            ],
            [
                'key_name_id'=>'snow_dsc', 
                'key_data_type_id'=>'text',
                'translations' => json_encode(["en" => "Snow description"])
            ],
            [
                'key_name_id'=>'num_tr_collected', 
                'key_data_type_id'=>'integer',
                'translations' => json_encode(["en" => "Number of traces collected"])
            ],
            [
                'key_name_id'=>'tr_age', 
                'key_data_type_id'=>'real',
                'translations' => json_encode(["en" => "Age of traces"])
            ],
            [
                'key_name_id'=>'tr_size', 
                'key_data_type_id'=>'real',
                'translations' => json_encode(["en" => "Size of traces"])
            ],
            [
                'key_name_id'=>'tr_dsc', 
                'key_data_type_id'=>'text',
                'translations' => json_encode(["en" => "Description of the traces"])
            ]
            
        ], 'gensam_snow_tracks_properties', 'referenced_tables',[
            'required' => false
        ]);     
    }

    static function patch_11() {
        /**2370
        2271
        2272
        2321
        2322
        2369
        */
        $res = \DB::update("UPDATE mb2data.gensam SET _sample_type=(select id from mbase2.code_list_options_vw clov where list_key='gensam_sample_type_cl' and key='saliva_direct')
        WHERE _sample_type in (select id from mbase2.code_list_options_vw clov where list_key='gensam_sample_type_cl' and key='saliva_prey')");
        echo "UPDATED $res records.\n";

        $res = \DB::delete("delete from mbase2.code_list_options where id = (select id from mbase2.code_list_options_vw clov where list_key='gensam_sample_type_cl' and key='saliva_prey')");
        echo "DELETED $res code list entry.\n";

        $res = \DB::update("update mbase2.code_list_options set translations = :trans
            where id = (select id from mbase2.code_list_options_vw clov where list_key='gensam_sample_type_cl' and key='saliva')", [
                ':trans' => json_encode(['en'=>'Saliva - noninvasive', 'sl'=>'Slina - neinvazivno'])
        ]);
        echo "UPDATED $res records.\n";

        $res = \DB::update("update mbase2.code_list_options set key = 'saliva_noninvasive' where id = (select id from mbase2.code_list_options_vw clov where list_key='gensam_sample_type_cl' and key='saliva')");
        echo "UPDATED $res records.\n";

        $res = \DB::update("update mbase2.code_list_options set key='gensam_saliva_noninvasive_properties' where 
            id=(select id from mbase2.code_list_options_vw clov where list_key='referenced_tables' and key ='gensam_saliva_properties')");
        echo "UPDATED $res records.\n";
    }

    static function patch_10() {
        self::updateVariables([
            [
                'key_name_id'=>'slug', 
                'required'=>true,
                'weight_in_table'=>1
            ],
            [
                'key_name_id'=>'_full_name', 
                'required'=>false,
                'weight_in_table'=>2
            ],
            [
                'key_name_id'=>'contact_data', 
                'required'=>false,
                'weight_in_table'=>3
            ],
            [
                'key_name_id'=>'organisation', 
                'required'=>false,
                'weight_in_table'=>4
            ],
            [
                'key_name_id'=>'note', 
                'required'=>false,
                'weight_in_table'=>5
            ]
        ],'gensam_samplers');

        self::catchQuery("update mb2data.gensam_samplers set slug=_full_name");
        self::catchQuery("ALTER TABLE mb2data.gensam_samplers ALTER COLUMN slug SET NOT NULL");
    }       

    static function patch_9() {
        self::importVariables([
            [
                'key_name_id'=>'note', 
                'key_data_type_id'=>'text',
                'translations' => json_encode(["sl" => "Opomba","en" => "Note"])
            ],
            [
                'key_name_id'=>'organisation', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'organisations',
                'translations' => json_encode(["sl" => "Organizacija","en" => "Organisation"])
            ],
            [
                'key_name_id'=>'slug', 
                'key_data_type_id'=>'text',
                'unique_constraint'=>true,
                'translations' => json_encode(["sl" => "Unikatna oznaka","en" => "Unique label"])
            ]
        ], 'gensam_samplers', 'referenced_tables');


        Mbase2Database::updateSchema('gensam_samplers');

        self::catchQuery("update mbase2.referenced_tables set label_key='slug' where id=(select id from mbase2.referenced_tables_vw rtv where key = 'gensam_samplers')");

        /*
        self::catchQuery("ALTER TABLE mb2data.gensam
        ADD CONSTRAINT unique_equip_id 
        UNIQUE USING INDEX equipment_equip_id;")
        */
    }

    static function patch_8() {
        $tname = 'gensam';
        self::catchQuery("DROP VIEW mb2data.{$tname}_vw");
        self::catchQuery("ALTER TABLE mb2data.$tname ADD column _batch_id integer references mbase2.import_batches(id)");
        self::gensam_vw(false);
        \DB::unprepared(file_get_contents(dirname(__FILE__) . '/gensam_batches_update.sql'));
    }

    static function patch_7() {
        self::catchQuery("update mb2data.gensam set samplers = null where samplers='[null]'");
    }

    static function patch_6() {
        
    }

    static function patch_5() {
        self::catchQuery("update mb2data.gensam SET completed=true where completed is null and id in (2155,2246)");
        self::catchQuery("ALTER TABLE mb2data.gensam ALTER COLUMN completed SET NOT NULL;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey1;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey10;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey11;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey12;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey13;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey2;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey3;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey4;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey5;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey6;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey7;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey8;");
        self::catchQuery("ALTER TABLE mb2data.gensam DROP CONSTRAINT gensam__batch_id_fkey9;");

    }

    static function patch_4() {
        self::catchQuery("update mbase2.module_variables set visible=false where id=(select id from mbase2.module_variables_vw mvv 
        where module_name='gensam' and variable_name like 'species_list_id')");
    }

    static function patch_3() {
        self::catchQuery("UPDATE mbase2.module_variables set filterable = true where id in (
            select id from mbase2.module_variables_vw where module_name='gensam' and data_type in ('code_list_reference', 'table_reference', 'code_list_reference_array', 'table_reference_array','short','integer','smallint','real','float','date', 'timestamp', 'time','varchar', 'text'))");
    }

    static function patch_2() {
        self::updateVariables([
            [
                'key_name_id'=>'sample_code', 
                'visible_in_cv_detail'=>true
            ]
        ], 'gensam');
    }

    static function patch_1() {
        self::updateVariables([
            [
                'key_name_id'=>'species_list_id', 
                'translations' => json_encode(["sl" => "Živalska vrsta","en" => "Species"])
            ]
        ], 'gensam');
    }

    static function patch_0() {

        self::gensam_vw();

        self::importVariables([
            [
                'key_name_id'=>'species_list_id', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'species_list',
                'translations' => json_encode(["sl" => "Živalska vrsta","en" => "Species"])
            ]
        ], 'gensam', 'modules', ['required' => true, 'visible_in_cv_detail'=>true, 'visible_in_cv_grid'=>true, 'filterable' => false]);

        self::updateVariables(([[
            'key_name_id' => 'event_date',
            'visible_in_cv_detail'=>true, 
            'visible_in_cv_grid'=>true
        ]]),'gensam');

    }

    static function gensam_vw($dropView = true) {
        if ($dropView) {
            self::catchQuery("drop view mb2data.gensam_vw");
        }

        self::catchQuery("CREATE OR REPLACE VIEW mb2data.gensam_vw
        AS SELECT gs.*,lsl.id as species_list_id,
            st_asgeojson(gs._location) geom,
                EXISTS (
                SELECT 1 
                FROM mb2data.gensam_genotypes ggs
                WHERE ggs.gensam_id = gs.id
            ) AS has_genotype,
            EXISTS (
                SELECT 1 
                FROM mb2data.gensam_ref_animals gra
                WHERE gra.ref_animal = gs.sample_code
            ) AS is_reference
            FROM mb2data.gensam gs
            left join mbase2.code_list_options clo on clo.id = gs._species_name
            left join laravel.species_list lsl on lsl.slug = clo.key;");        
    }
}