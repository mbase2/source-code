<?php namespace patches;

use Exception;
use Mbase2Utils;
use Mbase2SchemaPatches;
use Mbase2Database;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');

class interventions extends Mbase2SchemaPatches  {

    static function patch_31() {
        self::interventions_vw();
    }

    static function patch_30() {
        self::catchQuery("drop view mb2data.interventions_vw");

        foreach([
            'intervention_caller',
            'intervention_reason',
            'intervention_outcome'
        ] as $cname) {
            self::catchQuery("alter table mb2data.interventions_batch_imports drop constraint interventions_batch_imports_{$cname}_fkey");

            self::catchQuery("ALTER TABLE mb2data.interventions_batch_imports
            ALTER COLUMN $cname TYPE jsonb
            USING (('[' || $cname || ']')::jsonb);");
        }

        self::interventions_vw();
    }

    static function patch_29() {
        self::importVariables([[
            'key_name_id' => '_batch_id',
            'key_data_type_id'=>'table_reference',
            'ref' => 'import_batches',
            'filterable' => true,
            'translations' => ['en'=>'Batch import', 'sl' => 'Paketni uvoz']
        ]],'interventions_batch_imports', 'referenced_tables');
    }

    static function patch_28() {
        foreach(['intervention_caller', 'intervention_outcome', 'intervention_reason'] as $key) {
            self::updateVariables([
                [
                    'key_name_id' => $key,
                    'key_data_type_id' => 'code_list_reference_array'
                ]
            ], 'interventions_batch_imports');
        }

        self::interventions_vw();
        
    }

    static function patch_27() {
        self::importVariables([
            [
                'key_name_id' => '_location_data.lat',
                'key_data_type_id' => 'real',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Latitude (WGS84)", "sl": "Zemlj. širina (WGS84)"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ],
            [
                'key_name_id' => '_location_data.lon',
                'key_data_type_id' => 'real',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Longitude (WGS84)", "sl": "Zemlj. dolžina (WGS84)"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ]
        ], 'interventions_batch_imports', 'referenced_tables');

        self::importVariables([
            [
                'key_name_id' => '_location_data.spatial_request_result.SI-LOV',
                'key_data_type_id' => 'text',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Hunting ground", "sl": "Lovišče"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ],
            [
                'key_name_id' => '_location_data.spatial_request_result.SI-LUO',
                'key_data_type_id' => 'text',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Hunting management area", "sl": "Lovsko-upravljavsko območje"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ]
        ], 'interventions_batch_imports', 'referenced_tables');
    }

    static function patch_26() {
        self::catchQuery("UPDATE mbase2.module_variables SET filterable=true WHERE 
        id in (SELECT id from mbase2.module_variables_vw WHERE module_name='interventions_batch_imports' and visible_in_cv_detail=true)");
    }

    static function patch_25() {
        
        self::catchQuery("UPDATE mbase2.module_variables SET key_name_id='event_date' where id = (SELECT id from mbase2.module_variables_vw 
        where key_name_id='intervention_call_timestamp' and module_name='interventions_batch_imports')");

        self::catchQuery("ALTER table mb2data.interventions_batch_imports rename column intervention_call_timestamp to event_date");

        self::interventions_vw();

        $rows = \DB::select("SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = 'mb2data'
            AND table_name   = 'interventions_vw'");

        $cnames = array_column($rows, 'column_name', 'column_name');

        $rows = \DB::select("SELECT * from mbase2.module_variables_vw WHERE module_name='interventions_batch_imports'");

        foreach($rows as $row) {
            $key = $row->key_name_id;
            if (!isset($cnames[$key])) {
                echo $key."\n";
            }
        }

        self::updateVariables([[
            'key_name_id' => 'event_date',
            'visible_in_cv_grid' => true,
            'visible_in_cv_detail'=>true,
            'importable'=>true
        ]],'interventions_batch_imports');
    }

    static function patch_24() {
        self::dropModuleVariable('event_date', 'interventions_batch_imports');
        
        self::importVariables([[
            'key_name_id' => 'intervention_outcome',
            'key_data_type_id' => 'code_list_reference',
            'ref' => 'intervention_outcome_options',
            'required' => false,
            'translations' => '{"sl": "Izid intervencije"}',
            'importable' => true,
            'visible_in_cv_grid' => false,
            'visible_in_cv_detail' => false
        ]], 'interventions_batch_imports', 'referenced_tables');

        Mbase2Database::updateSchema('interventions_batch_imports', 'mb2data');

        self::interventions_vw();
    }

    static function patch_23() {
        self::updateVariables([[
                'key_name_id' => 'intervention_caller',
                'required' => true,
                'translations' => '{"sl":"Klicatelj"}'
            ],
            [
                'key_name_id' => 'intervention_measures',
                'required' => false,
            ],
            [
                'key_name_id' => 'intervention_reason',
                'required' => false,
            ]
        ], 'interventions_batch_imports');
    }

    static function patch_22() {
        self::catchQuery("update mbase2.module_variables set importable=false where id in (select id from mbase2.module_variables_vw where module_name in ('interventions', 'interventions_events'))");
    }

    static function patch_21() {
        $module_id = self::addCodeListOption('referenced_tables','interventions_batch_imports');

        \DB::update("UPDATE mbase2.module_variables SET module_id=$module_id WHERE id in (select id from mbase2.module_variables_vw where module_name='interventions_vw')");

        \DB::delete("DELETE from mbase2.code_list_options where id = (SELECT id from mbase2.code_list_options where key = 'interventions_vw')");
    }

    static function patch_20() {
        self::updateVariables([[
            'key_name_id' => 'event_date',
            'translations' => ['sl' => 'Datum klica', 'en' => 'Call date']
        ]], 'interventions_vw');
    }

    static function patch_19() {
        self::interventions_vw();
    }

    static function patch_18() {
        $res = \DB::update("UPDATE mbase2.module_variables SET visible_in_cv_grid=false, visible_in_cv_detail=false WHERE id in 
        (select id from mbase2.module_variables_vw where module_name = 'interventions_vw')");
        echo "UPDATED: $res\n";
        $w=1;

        self::importVariables([[
            'key_name_id' => 'event_date',
            'key_data_type_id' => 'date',
        ]], 'interventions_vw','referenced_tables');

        foreach(['id_mas', 'event_date', 'species_list_id', 'intervention_reason', 'intervention_measures', 'licence_list_id'] as $key) {
            self::updateVariables([[
                'key_name_id' => $key,
                'weight_in_popup' => $w++,
                'visible_in_cv_grid' => true,
                'visible_in_cv_detail' => true
            ]], 'interventions_vw');
        }

        self::updateVariables([[
            'key_name_id' => 'intervention_measures',
            'visible_in_cv_grid' => false
        ]], 'interventions_vw');
    }

    static function patch_17() {
        self::catchQuery("DELETE from mbase2.module_variables WHERE id in (select id from mbase2.module_variables_vw where module_name='interventions_view')");
        self::catchQuery("DELETE from mbase2.module_variables WHERE id in (select id from mbase2.module_variables_vw where module_name='interventions_vw')");
        self::catchQuery("delete from mbase2.code_list_options clo where key='interventions_view'");

        $module_id = self::addCodeListOption('referenced_tables','interventions_vw');

        self::catchQuery("INSERT into mbase2.module_variables
        (data_type_id, ref, module_id, required, unique_constraint, pattern_id, weight, visible, read_only, form_data, skip_from_table_view, default_value, _data, _desc, filterable, importable, weight_in_table, weight_in_popup, aggregate_operation, dbcolumn, visible_in_table, section_in_form, visible_in_popup, visible_anonymously, weight_in_import, weight_in_filter, translations, key_name_id, visible_in_cv_detail, visible_in_cv_grid, help)
        SELECT distinct on (key_name_id) data_type_id, ref, $module_id, required, unique_constraint, pattern_id, weight, visible, read_only, form_data, skip_from_table_view, default_value, _data, _desc, filterable, importable, weight_in_table, weight_in_popup, aggregate_operation, dbcolumn, visible_in_table, section_in_form, visible_in_popup, visible_anonymously, weight_in_import, weight_in_filter, translations, key_name_id, visible_in_cv_detail, visible_in_cv_grid, help
        from mbase2.module_variables WHERE id in (select id from mbase2.module_variables_vw where module_name like 'interventions%' and importable=true)");
    }

    static function patch_16() {
        self::interventions_vw();
    }

    static function patch_15() {
        self::catchQuery("ALTER TABLE mb2data.interventions_batch_imports ADD column 
        licence_list_id integer references laravel.licence_list(id)
        ");
    }

    static function patch_14() {
        self::importVariables([
            [
                'key_name_id' => 'licence_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'licence_list',
                'importable' => true,
                'weight_in_import' => 0.1,
                'translations'=>'{"en": "Licence", "sl": "Licenca"}'
            ]],'interventions');
    }

    static function patch_13() {
        self::updateVariables([[
            'key_name_id'=>'intervention_start_timestamp',
            'importable'=>false
        ],
        [
            'key_name_id'=>'intervention_end_timestamp',
            'importable'=>false
        ]
        ],'interventions_events');

        self::interventions_vw();
    }

    static function patch_12() {
        self::interventions_vw();
    }

    static function patch_11() {

        self::addCodeListOption('referenced_tables','interventions_batch_imports');
        
        \DB::update("UPDATE mbase2.module_variables SET importable=false WHERE id in (SELECT id from mbase2.module_variables_vw WHERE module_name like 'interventions%')");

        self::importVariables([[
            "key_name_id"=>'id_mas',
            'key_data_type_id' => 'text'
        ],
        [
            'key_name_id'=>'call_received_by',
            'key_data_type_id' => 'text'
        ]], 'interventions_events','referenced_tables', ['importable' => true]);

        self::importVariables([[   
            'key_name_id'=>'location_correction',
            'key_data_type_id' => 'text',
            'importable' => 'true'
        ]], 'interventions');

        foreach(['id_mas',
        'intervention_call_timestamp',
        'intervention_start_timestamp',
        'intervention_end_timestamp',
        'intervention_caller',
        'species_list_id',
        '_location_data.additional.sdist',
        '_location_data.additional.lname',
        'location_correction',
        '_location_data',
        'call_received_by',
        'intervention_reason',
        'intervention_measures',
        'notes'] as $i => $key) {
            \DB::update("UPDATE mbase2.module_variables SET weight_in_import = :i, importable=true WHERE id in (SELECT id from mbase2.module_variables_vw WHERE module_name like 'interventions%' and variable_name=:vn)",
            [':vn'=>$key,':i'=>$i+1]);
        }

        $variables = Mbase2Database::query("SELECT * FROM mbase2.module_variables_vw WHERE module_name like 'interventions%' and (importable=true or variable_name='_location')");

        Mbase2Database::updateSchema('interventions_batch_imports', 'mb2data', null, $variables);
    }

    static function patch_10() {
        self::catchQuery("drop view if exists mb2data.interventions_vw");
        self::interventions_vw();
    }

    static function patch_9() {
        self::catchQuery("update mbase2.module_variables set key_name_id = 'interventions_id' where key_name_id = 'intervention_id'");
        
        self::catchQuery("drop view if exists mb2data.interventions_vw");

        self::catchQuery("ALTER TABLE mb2data.interventions_events RENAME column intervention_id to interventions_id");

        self::catchQuery("CREATE OR REPLACE VIEW mb2data.interventions_vw AS
            SELECT i.id,
                public.st_asgeojson(i._location) AS geom,
                i.notes,
                i.date_record_created,
                i.date_record_modified,
                i.species_list_id,
                i._uname,
                min(ie.intervention_call_timestamp) AS intervention_call_timestamp_min,
                max(ie.intervention_call_timestamp) AS intervention_call_timestamp_max,
                min(ie.intervention_start_timestamp) AS intervention_start_timestamp_min,
                count(*) FILTER (WHERE (ie.intervention_end_timestamp IS NULL)) AS uncompleted_interventions,
                count(*) FILTER (WHERE (ie.interventions_id IS NOT NULL)) AS intervention_events_count,
                json_agg(DISTINCT ie.intervention_caller) AS intervention_caller,
                json_agg(DISTINCT ie.intervention_reason) AS intervention_reason,
                json_agg(ie.intervention_measures) AS intervention_measures,
                json_agg(DISTINCT ie.intervention_outcome) AS intervention_outcome,
                json_agg(DISTINCT ie.chief_interventor) AS chief_interventor,
                i._completed,
                i._location_data
            FROM (mb2data.interventions i
                LEFT JOIN mb2data.interventions_events ie ON ((i.id = ie.interventions_id)))
            GROUP BY i.id, i._location, i.notes, i.date_record_created, i.date_record_modified, i.species_list_id, i._uname;");
    }

    static function patch_8() {
        self::addReferenceTable('interventions','id','mb2data','id');
        self::updateVariables([
            [
                'key_name_id' => 'intervention_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'interventions'
            ]
        ], 'interventions_events');


        foreach(['intervention_id',
        'intervention_caller',
        'intervention_measures',
        'intervention_reason',
        'intervention_outcome',
        'chief_interventor',
        'interventors',
        'notes',
        'situation_notes',
        'intervention_call_timestamp',
        'intervention_start_timestamp',
        'intervention_end_timestamp'] as $key) {
            self::updateVariables([[
                'key_name_id'=>$key,
                'importable' => true,
            ]], 'interventions_events');
        }
    }

    static function patch_7() {
        self::updateVariables([[
            'key_name_id'=>'_completed',
            'translations'=>'{"en": "Status", "sl": "Status"}'
        ]], 'interventions');
    }

    static function patch_6() {
        self::importVariables([
            [
                'key_name_id' => '_location_data.additional.sdist',
                'key_data_type_id' => 'text',
                'importable' => true,
                'translations'=>'{"en": "Distance from settlement (house) in meters", "sl": "Oddaljenost od naselja (hiše) v metrih"}'
            ],
            [
                'key_name_id' => '_location_data.additional.lname',
                'key_data_type_id' => 'text',
                'importable' => true,
                'translations'=>'{"en": "Local name", "sl": "Lokalno ime"}'
            ]
        ],'interventions','modules',[
            'dbcolumn'=> false
        ]);
    }

    static function patch_5() {
        foreach(['species_list_id', '_completed', 'notes', '_location_data'] as $key) {
            self::updateVariables([[
                'key_name_id'=>$key,
                'importable'=>true
            ]], 'interventions');
        }

        self::updateVariables([[
            'key_name_id'=>'_completed',
            'required'=>false
        ]], 'interventions');
    }

    static function patch_4() {
        self::catchQuery("DROP VIEW mb2data.interventions_vw");
        self::catchQuery("alter table mb2data.interventions drop column _data");
        self::catchQuery("alter table mb2data.interventions drop column event_date");
        self::interventions_vw();
    }

    static function patch_3() {
        self::interventions_vw();
    }

    static function patch_2() {
        self::catchQuery("drop view mb2data.interventions_view");
        self::catchQuery("ALTER TABLE mb2data.interventions DROP COLUMN _species_name");
        Mbase2Database::updateSchema('interventions');
    }

    static function patch_1() {
        self::importVariables([
            [
                'key_name_id' => 'species_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'species_list',
                'translations' =>'{"en": "Species", "sl": "Živalska vrsta"}',
                'required' => true,
                'visible' =>true
            ]],'interventions');

            $w=1;

            foreach(['species_list_id',
            '_location_data',
            'notes'
            ] as $key_name_id) {
                self::updateVariables([
                    [
                        'key_name_id' => $key_name_id,
                        'visible'=>true,
                        'weight' => $w++
                    ]],'interventions');    
            }

            self::updateVariables([[
                'key_name_id'=>'_location_data',
                'translations' =>'{"en": "Location", "sl": "Lokacija"}'
            ]],'interventions');
    }

    static function patch_0() {
        self::catchQuery(Mbase2Utils::SQLcreateSpatialIndex('mb2data', 'interventions', '_location'));
    }

    static function interventions_vw() {
        self::catchQuery("DROP VIEW IF EXISTS mb2data.interventions_vw");
        self::catchQuery("CREATE OR REPLACE VIEW mb2data.interventions_vw AS
        SELECT i.id,
            null as _batch_id,
            '' as id_mas,
            public.st_asgeojson(i._location) AS geom,
            i.notes,
            i.date_record_created,
            i.date_record_modified,
            i.species_list_id,
            i._licence_name as licence_list_id,
            i._uname,
            min(ie.intervention_call_timestamp) AS intervention_call_timestamp_min,
            max(ie.intervention_call_timestamp) AS intervention_call_timestamp_max,
            min(ie.intervention_start_timestamp) AS intervention_start_timestamp_min,
            min(ie.intervention_start_timestamp) AS event_date,
            count(*) FILTER (WHERE (ie.intervention_end_timestamp IS NULL)) AS uncompleted_interventions,
            count(*) FILTER (WHERE (ie.interventions_id IS NOT NULL)) AS intervention_events_count,
            json_agg(DISTINCT ie.intervention_caller)::jsonb AS intervention_caller,
            json_agg(DISTINCT ie.intervention_reason)::jsonb AS intervention_reason,
            json_agg(ie.intervention_measures)::jsonb AS intervention_measures,
            json_agg(DISTINCT ie.intervention_outcome)::jsonb AS intervention_outcome,
            json_agg(DISTINCT ie.chief_interventor)::jsonb AS chief_interventor,
            i._completed,
            i._location_data,
            i._location,
            '' as location_correction,
            '' as call_received_by
        FROM (mb2data.interventions i
            LEFT JOIN mb2data.interventions_events ie ON ((i.id = ie.interventions_id)))
        GROUP BY i.id, i._location, i.notes, i.date_record_created, i.date_record_modified, i.species_list_id, i._uname
        union all
        SELECT -bi.id,
            bi._batch_id,
            bi.id_mas,
            public.st_asgeojson(bi._location) AS geom,
            bi.notes,
            null,
            null,
            bi.species_list_id,
            bi.licence_list_id,
            null,
            bi.event_date AS intervention_call_timestamp_min,
            bi.event_date AS intervention_call_timestamp_max,
            bi.event_date AS intervention_start_timestamp_min,
            bi.event_date AS event_date,
            0 AS uncompleted_interventions,
            1 AS intervention_events_count,
            intervention_caller,
            intervention_reason,
            intervention_measures,
            intervention_outcome,
            null AS chief_interventor,
            true as _completed,
            bi._location_data,
            bi._location,
            location_correction,
            call_received_by
        FROM mb2data.interventions_batch_imports bi
        ;");

    }
}