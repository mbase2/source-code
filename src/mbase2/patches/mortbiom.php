<?php namespace patches;

use Exception;
use Mbase2Utils;
use Mbase2Database;
use Mbase2Files;
use Mbase2SchemaPatches;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');

class mortbiom extends Mbase2SchemaPatches  {

    static function patch_43() {
        self::mortbiom_vw();
    }

    static function patch_42() {
        self::catchQuery("DROP VIEW IF EXISTS mb2data.mortbiom_vw");
        self::catchQuery("alter table mb2data.mortbiom alter column masa_bruto type numeric(7,3)");

        $rows = \DB::select("select column_name from information_schema.columns
        where table_name = 'mortbiom' and table_schema = 'mb2data' and data_type = 'real';");

        foreach ($rows as $row) {
            self::catchQuery("alter table mb2data.mortbiom alter column $row->column_name type numeric(7,3)");
        }

        self::mortbiom_vw();
    }

    static function patch_41() {
        self::updateVariables([[
            'key_name_id' => 'name',
            'visible_in_cv_detail' => true
        ]], 'mortbiom');
    }

    static function patch_40() {
        self::importVariables([[
            'key_name_id' => '_batch_id',
            'key_data_type_id'=>'table_reference',
            'ref' => 'import_batches',
            'filterable' => true,
            'translations' => ['en'=>'Batch import', 'sl' => 'Paketni uvoz']
        ]],'mortbiom');
    }

    static function patch_39() {

        self::dropModuleVariable('_location_data.spatial_request_result.lov_ime','mortbiom');
        self::dropModuleVariable('_location_data.spatial_request_result.luo_ime','mortbiom');
        
        self::importVariables([
            [
                'key_name_id' => '_location_data.spatial_request_result.SI-LOV',
                'key_data_type_id' => 'text',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Hunting ground", "sl": "Lovišče"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ],
            [
                'key_name_id' => '_location_data.spatial_request_result.SI-LUO',
                'key_data_type_id' => 'text',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Hunting management area", "sl": "Lovsko-upravljavsko območje"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ]
        ], 'mortbiom');
    }

    static function patch_38() {
        self::mortbiom_vw();
    }

    static function patch_37() {
        self::importVariables([
            [
                'key_name_id' => '_location_data.lat',
                'key_data_type_id' => 'real',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Latitude (WGS84)", "sl": "Zemlj. širina (WGS84)"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ],
            [
                'key_name_id' => '_location_data.lon',
                'key_data_type_id' => 'real',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Longitude (WGS84)", "sl": "Zemlj. dolžina (WGS84)"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ]
        ], 'mortbiom');
    }

    static function patch_36() {
        self::catchQuery("UPDATE mbase2.module_variables SET filterable=true WHERE 
        id in (SELECT id from mbase2.module_variables_vw WHERE module_name='mortbiom' and visible_in_cv_detail=true)");
    }

    static function patch_35() {
        self::mortbiom_vw();
    }
    
    static function patch_34() {
        self::updateVariables([[
            'key_name_id'=>'event_date',
            'key_data_type_id' =>'date',
            'importable' => true,
            'visible_in_cv_grid' => true,
            'visible_in_cv_detail' => true,
            'section_in_form'=> 'biometry_animal_handling',
            'required'=>true,
            'translations'=>'{"en": "Animal handling date", "sl": "Datum rokovanja z živaljo"}'
        ]],'mortbiom');
        
        self::catchQuery("DROP VIEW mb2data.mortbiom_vw");
        
        self::catchQuery("ALTER TABLE mb2data.mortbiom 
        ALTER COLUMN animal_handling_date TYPE date;");

        self::catchQuery("ALTER TABLE mb2data.mortbiom 
        rename COLUMN animal_handling_date to event_date;");

        self::catchQuery("DELETE from mbase2.module_variables where id=(select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name='animal_handling_date')");

        self::mortbiom_vw();
    }

    static function patch_33() {
        self::updateVariables([[
            'key_name_id'=>'event_date',
            'visible_in_cv_grid' => true,
            'visible_in_cv_detail' => true
        ]],'mortbiom');

        self::updateVariables([[
            'key_name_id'=>'animal_handling_date',
            'visible_in_cv_grid' => false,
            'visible_in_cv_detail' => false
        ]],'mortbiom');
    }

    static function patch_32() {
        $res = \DB::update("UPDATE mbase2.module_variables SET key_name_id='animal_handling_date' WHERE 
        id in (select id from mbase2.module_variables_vw where module_name='mortbiom' and key_name_id='event_date')");

        echo "Number of updated: $res\n";

        self::importVariables([[
            'key_name_id' => 'event_date',
            'key_data_type_id' => 'date',
            'translations' => ['sl' => 'Datum rokovanja z živaljo', 'en' => 'Animal handling date']
        ]], 'mortbiom');
    }

    static function patch_31() {
        self::mortbiom_vw();

        self::catchQuery("UPDATE mbase2.module_variables SET key_name_id='event_date' WHERE key_name_id='animal_handling_date'");

        $w = 1;

        self::updateVariables([[
            'key_name_id' => 'species_list_id',
            'weight_in_popup'=>$w++
        ]],'mortbiom');

        foreach(['event_date', 'biometry_loss_reason_list_id'] as $key) {
            self::updateVariables([[
                'key_name_id' => $key,
                'visible_in_cv_detail' => true, 'visible_in_cv_grid' => true,
                'weight_in_popup'=>$w++
            ]],'mortbiom');    
        }
        
        foreach (['age', 'dej_star_lab', 'masa_bruto'] as $key) {
            self::updateVariables([[
                'key_name_id' => $key,
                'visible_in_cv_grid' => false, 'visible_in_cv_detail' => true,
                'weight_in_popup'=>$w++
            ]],'mortbiom');
        }
    }

    static function patch_30() {
        self::updateVariables([[
            'key_name_id'=>'hunter_finder_country_id',
            'key_data_type_id' => 'text'
        ]],'mortbiom');

        self::catchQuery("ALTER TABLE mb2data.mortbiom 
        ALTER COLUMN hunter_finder_country_id TYPE text;");
    }

    static function patch_29() {
        self::catchQuery("alter table mb2data.mortbiom add column id_mas text");
    }

    static function patch_28() {
        foreach(['_location_data.spatial_request_result.luo_ime',
        '_location_data.spatial_request_result.lov_ime'] as $key) {
            self::updateVariables([[
                'key_name_id'=>$key,
                'importable'=>false
            ]], 'mortbiom');
        }

        self::updateVariables([[
            'key_name_id'=>'color_list_id',
            'key_data_type_id' => 'text'
        ]],'mortbiom');

        self::catchQuery("alter table mb2data.mortbiom drop constraint mortbiom_color_list_id_fkey");

        self::catchQuery("ALTER TABLE mb2data.mortbiom 
        ALTER COLUMN color_list_id TYPE text;");
    }

    static function patch_27() {
        self::catchQuery("DELETE FROM mb2data.mortbiom where _batch_id = 428");
        self::catchQuery("DELETE FROM mbase2.import_batches where id = 428");
    }

    static function patch_26() {
        self::importVariables([[
            "key_name_id"=>'id_mas',
            'key_data_type_id' => 'text',
            'importable' => 'true',
            'weight_in_import' => 0.2,
            'section_in_form' => 'biometry_animal_handling',
        ]], 'mortbiom');
    }
    static function patch_25() {
        self::updateVariables([[
            'key_name_id' => 'dol_treb',
            'key_data_type_id' => 'real'
        ]],'mortbiom');

        self::catchQuery("ALTER TABLE mb2data.mortbiom 
        ALTER COLUMN dol_treb TYPE real;");
    }
    
    static function patch_24() {
        self::catchQuery("update mbase2.module_variables set key_name_id = lower(key_name_id) where id in 
        (select id from mbase2.module_variables_vw where module_name='mortbiom')");

        self::catchQuery("DELETE FROM mbase2.module_variables WHERE ID in (SELECT id from mbase2.module_variables_vw where module_name='mortbiom' and importable=false)");
        $rows=\DB::select("SELECT *
        FROM information_schema.columns
       WHERE table_schema = 'mb2data'
         AND table_name   = 'mortbiom';");

        $defRes = \DB::select("SELECT variable_name from mbase2.module_variables_vw where module_name='mortbiom'");

        $existing = array_column($defRes,'variable_name');

        $cnt=0;

        self::catchQuery("DROP VIEW mb2data.mortbiom_vw");

        foreach($rows as $row) {
            $cname = $row->column_name;
            if (in_array($cname,['id',
            '_batch_id',
            '_location',
            '_location_data'])) {
                echo $cname."\n";
                continue;
            }

            if (!in_array($cname, $existing)) {
                $cnt++;
                echo "DROP: $cname\n";
                self::catchQuery("ALTER table mb2data.mortbiom drop column if exists $cname");
            }
            
        }

        self::mortbiom_vw();

        echo "DROP count $cnt\n";
    }

    static function patch_23() {
        self::catchQuery("ALTER TABLE mb2data.mortbiom 
            ALTER COLUMN int_star_matson TYPE text;");
        
        self::updateVariables([[
            'key_name_id' => 'INT_STAR_MATSON',
            'key_data_type_id' => 'text'
        ]],'mortbiom');
    }

    static function patch_22() {
        self::updateVariables([[
            'key_name_id' => 'licence_list_id',
            'weight_in_import' => 0.1,
            'importable' => true,
        ]],'mortbiom');
    }

    static function patch_21() {
        Mbase2Database::updateSchema('mortbiom');
    }

    static function patch_20() {
        \DB::update("UPDATE mbase2.module_variables SET importable=false WHERE id in (select id from mbase2.module_variables_vw WHERE module_name='mortbiom' and variable_name<>'_location_data')");

        $items = [['xlsx_cname'=>'ZIV_VRS', 'db_cname'=> 'species_list_id', 'translations'=>['sl'=>'Živalska vrsta']],
        ['xlsx_cname'=>'LUO', 'db_cname'=> '_location_data.spatial_request_result.luo_ime', 'translations'=>['sl'=>'Lovsko-upravljavsko območje']],
        ['xlsx_cname'=>'LOVISCE', 'db_cname'=> '_location_data.spatial_request_result.lov_ime', 'translations'=>['sl'=>'Lovišče']],
        ['xlsx_cname'=>'KRAJ', 'db_cname'=> '_location_data.additional.lname', 'translations'=>['sl'=>'Lokalno ime']],
        ['xlsx_cname'=>'Datum_Excel', 'db_cname'=> 'animal_handling_date', 'translations'=>['sl'=>'Datum rokovanja z živaljo']],
        ['xlsx_cname'=>'GKX_Mbase', 'db_cname'=> '_location', 'translations'=>[]],
        ['xlsx_cname'=>'GKY_Mbase', 'db_cname'=> '_location', 'translations'=>[]],
        ['xlsx_cname'=>'Tip_koordinate', 'db_cname'=> 'location_coordinate_type_list_id', 'translations'=>['sl'=>'Tip koordinate']],
        ['xlsx_cname'=>'NACIN_ODVZEMA_final', 'db_cname'=> 'way_of_withdrawal_list_id', 'translations'=>['sl'=>'Način odvzema']],
        ['xlsx_cname'=>'ODSTREL_Z_DOVOLJENJEM_final', 'db_cname'=> 'conflict_animal_removal_list_id', 'translations'=>['sl'=>'Vzrok za odstrel']],
        ['xlsx_cname'=>'VZROK_IZGUBE_final', 'db_cname'=> 'biometry_loss_reason_list_id', 'translations'=>['sl'=>'Vzrok izgube']],
        ['xlsx_cname'=>'KONFLIKTNOST_final', 'db_cname'=> 'animal_conflictedness', 'translations'=>['sl'=>'Konfliktnost živali']],
        ['xlsx_cname'=>'ŠT_DOVOLJENJA', 'db_cname'=> 'licence_number', 'translations'=>['sl'=>'Številka dovoljenja']],
        ['xlsx_cname'=>'OPOMBE_zdruzene', 'db_cname'=> 'observations_and_notes', 'translations'=>['sl'=>'Opazovanja in opombe']],
        ['xlsx_cname'=>'Animal_name', 'db_cname'=> 'name', 'translations'=>['sl'=>'Ime živali']],
        ['xlsx_cname'=>'Država preselitve', 'db_cname'=> 'receiving_country', 'translations'=>['sl'=>'Država, kamor je, osebek preseljen']],
        ['xlsx_cname'=>'SPOL', 'db_cname'=> 'sex_list_id', 'translations'=>['sl'=>'Spol']],
        ['xlsx_cname'=>'OCENJ_STAR_Mbase', 'db_cname'=> 'age', 'translations'=>['sl'=>'Ocena starosti']],
        ['xlsx_cname'=>'DEJ_STAR_LAB', 'db_cname'=> 'DEJ_STAR_LAB', 'translations'=>['sl'=>'Dejanska starost labor. analize']],
        ['xlsx_cname'=>'INT_STAR_MATSON', 'db_cname'=> 'INT_STAR_MATSON', 'translations'=>['sl'=>'Interval starosti Matson lab']],
        ['xlsx_cname'=>'MASA_BRUTO', 'db_cname'=> 'masa_bruto', 'translations'=>['sl'=>'Bruto telesna masa']],
        ['xlsx_cname'=>'MASA_NETO', 'db_cname'=> 'masa_neto', 'translations'=>['sl'=>'Neto telesna masa']],
        ['xlsx_cname'=>'DOL_HRBT', 'db_cname'=> 'body_length', 'translations'=>['sl'=>'Dolžina trupa (hrbtna stran)']],
        ['xlsx_cname'=>'DOL_TREB', 'db_cname'=> 'DOL_TREB', 'translations'=>['sl'=>'Dolžina trupa_trebušna stran']],
        ['xlsx_cname'=>'VIS_PLE', 'db_cname'=> 'shoulder_height', 'translations'=>['sl'=>'Plečna višina']],
        ['xlsx_cname'=>'OBS_GLA', 'db_cname'=> 'head_circumference', 'translations'=>['sl'=>'Obseg glave']],
        ['xlsx_cname'=>'OBS_VRA', 'db_cname'=> 'neck_circumference', 'translations'=>['sl'=>'Obseg vratu']],
        ['xlsx_cname'=>'OBS_PRS_KOS', 'db_cname'=> 'thorax_circumference', 'translations'=>['sl'=>'Obseg prsnega koša']],
        ['xlsx_cname'=>'OBS_TREBH', 'db_cname'=> 'abdomen_circumference', 'translations'=>['sl'=>'Obseg trebuha']],
        ['xlsx_cname'=>'DOL_SP_KOST', 'db_cname'=> 'baculum_length', 'translations'=>['sl'=>'Dolžina spolne kosti']],
        ['xlsx_cname'=>'DOL_SESK', 'db_cname'=> 'nipple_length', 'translations'=>['sl'=>'Dolžina seskov']],
        ['xlsx_cname'=>'OBRB_SESK', 'db_cname'=> 'teats_wear_list_id', 'translations'=>['sl'=>'Uporabljenost seskov']],
        ['xlsx_cname'=>'DOL_REP', 'db_cname'=> 'tail_length', 'translations'=>['sl'=>'Dolžina repa']],
        ['xlsx_cname'=>'DOL_UHL', 'db_cname'=> 'ear_length_without_hair', 'translations'=>['sl'=>'Dolžina uhlja']],
        ['xlsx_cname'=>'DOL_COP', 'db_cname'=> 'hair_tuft_length', 'translations'=>['sl'=>'Dolžina čopka na uhlju']],
        ['xlsx_cname'=>'DZLS', 'db_cname'=> 'hind_left_paw_length', 'translations'=>['sl'=>'Dolžina zadnje leve šape']],
        ['xlsx_cname'=>'DZDS', 'db_cname'=> 'hind_right_paw_length', 'translations'=>['sl'=>'Dolžina zadnje desne šape']],
        ['xlsx_cname'=>'DPLS', 'db_cname'=> 'front_left_paw_length', 'translations'=>['sl'=>'Dolžina prednje leve šape']],
        ['xlsx_cname'=>'DPDS', 'db_cname'=> 'front_right_paw_length', 'translations'=>['sl'=>'Dolžina prednje desne šape']],
        ['xlsx_cname'=>'ŠZLŠ', 'db_cname'=> 'hind_left_paw_width', 'translations'=>['sl'=>'Širina zadnje leve šape']],
        ['xlsx_cname'=>'ŠZDŠ', 'db_cname'=> 'hind_right_paw_width', 'translations'=>['sl'=>'Širina zadnje desne šape']],
        ['xlsx_cname'=>'ŠPLŠ', 'db_cname'=> 'front_left_paw_width', 'translations'=>['sl'=>'Širina prednje leve šape']],
        ['xlsx_cname'=>'ŠPDŠ', 'db_cname'=> 'front_right_paw_width', 'translations'=>['sl'=>'Širina prednje desne šape']],
        ['xlsx_cname'=>'DZGLP', 'db_cname'=> 'upper_left_canines_length', 'translations'=>['sl'=>'Dolžina levih podočnikov zgoraj']],
        ['xlsx_cname'=>'DZGDP', 'db_cname'=> 'upper_right_canines_length', 'translations'=>['sl'=>'Dolžina desnih podočnikov zgoraj']],
        ['xlsx_cname'=>'DSPLP', 'db_cname'=> 'lower_left_canines_length', 'translations'=>['sl'=>'Dolžina levih podočnikov spodaj']],
        ['xlsx_cname'=>'DSPDP', 'db_cname'=> 'lower_right_canines_length', 'translations'=>['sl'=>'Dolžina desnih podočnikov spodaj']],
        ['xlsx_cname'=>'ŠTPREDZG', 'db_cname'=> 'number_of_premolars_in_the_upper_jaw', 'translations'=>['sl'=>'Število predmeljakov zgoraj']],
        ['xlsx_cname'=>'ŠTPREDSP', 'db_cname'=> 'number_of_premolars_in_the_lower_jaw', 'translations'=>['sl'=>'Število predmeljakov spodaj']],
        ['xlsx_cname'=>'OBRABA_SEKALCEV', 'db_cname'=> 'incisors_wear_list_id', 'translations'=>['sl'=>'Obraba sekalcev']],
        ['xlsx_cname'=>'BARVA_OVRATNIK', 'db_cname'=> 'color_list_id', 'translations'=>['sl'=>'Barva kožuha in prisotnost ovratnika']],
        ['xlsx_cname'=>'UPLENITELJ_PRIIMEK_IME', 'db_cname'=> 'hunter_finder_name_and_surname', 'translations'=>['sl'=>'Ime in priimek uplenitelja / najditelja']],
        ['xlsx_cname'=>'UPLENITELJ_DRZAVA', 'db_cname'=> 'hunter_finder_country_id', 'translations'=>['sl'=>'Država uplenitelja/ najditelja']],
        ['xlsx_cname'=>'SPREMLJEVALEC', 'db_cname'=> 'witness_accompanying_person_name_and_surname', 'translations'=>['sl'=>'Ime in priimek spremljevalca']],
        ['xlsx_cname'=>'PREPARATOR', 'db_cname'=> 'taxidermist_name_and_surname', 'translations'=>['sl'=>'Ime in priimek preparatorja']]];

        $data = ['mortbiom'=>['types'=>[], 'headers'=>[]]];

        foreach($items as $i => $item) {
            $key = $item['db_cname'];
            $header = $item['xlsx_cname']; 

            if ($key === '_location') {
                
                $key = '_location_data';

                if (!isset($data['mortbiom']['headers'][$key])) {
                    $data['mortbiom']['headers'][$key] = ['crs' => 3912];
                }

                if ($header === 'GKX_Mbase') {
                    $data['mortbiom']['headers'][$key]['lon'] = $header;
                }
                else if ($header === 'GKY_Mbase') {
                    $data['mortbiom']['headers'][$key]['lat'] = $header;
                }
            }
            else {
                $data['mortbiom']['headers'][$key] = $header;
            }

            $data['mortbiom']['types'][$key] = "column";
            
            
            self::updateVariables([[
                'key_name_id' => $key,
                'weight_in_import' => $i+1,
                'importable' => true,
                'translations' => $item['translations']
            ]],'mortbiom');
        }

        /*
        \DB::insert("INSERT into mbase2.import_templates (_uid, key_module_id,note, data) values (:_uid, :key_module_id, :note, :data)",
            [
                ':_uid' => 1,
                ':key_module_id' => 'mortbiom',
                ':note' => 'SLO_Smrtnost_1994-2020_261023',
                ':data' => json_encode($data)
            ]
        );
        */
        \DB::update("UPDATE mbase2.import_templates SET data = :data where note=:note",[':data' => json_encode($data), ':note' => 'SLO_Smrtnost_1994-2020_261023']);
    }
    

    static function patch_19() {

        self::addReferenceTable('group_countries_vw','id','mbase2','slug',"", "name as translations");
        self::importVariables(
            [
                [
                    'key_name_id'=>'_location',
                    'key_data_type_id'=>'location_geometry',
                    'required' => true,
                    'importable' => true,
                    'section_in_form' => 'biometry_animal_handling',
                    'filterable' => false,
                    'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
                ],
                [
                    'key_name_id' => '_location_data.additional.lname',
                    'key_data_type_id' => 'text',
                    'section_in_form' => 'biometry_animal_handling',
                    'required' => false,
                    'translations'=>'{"en": "Local name", "sl": "Lokalno ime"}',
                    'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
                ],
                [
                    'key_name_id' => 'DEJ_STAR_LAB',
                    'key_data_type_id' => 'integer',
                    'section_in_form' => 'biometry_data',
                    'translations' => ['sl' => 'Dejanska starost labor. analize']
                ],
                [
                    'key_name_id' => 'INT_STAR_MATSON',
                    'key_data_type_id' => 'integer',
                    'section_in_form' => 'biometry_data',
                    'translations' => ['sl' => 'Interval starosti Matson lab']
                ],
                [
                    'key_name_id' => 'DOL_TREB',
                    'key_data_type_id' => 'integer',
                    'section_in_form' => 'biometry_data',
                    'translations' => ['sl' => 'Dolžina trupa_trebušna stran']
                ],
                [
                    'key_name_id' => 'hunter_finder_country_id',
                    'key_data_type_id' => 'table_reference',
                    'section_in_form' => 'biometry_animal_handling',
                    'ref' => 'group_countries_vw'
                ]
                ],'mortbiom', 'modules',['importable=>true']);
    }

    static function patch_18() {
        self::importVariables([[
                'key_name_id' => '_location_data.spatial_request_result.luo_ime',
                'key_data_type_id' => 'table_reference',
                'ref' => 'luo_vw',
                'section_in_form' => 'biometry_animal_handling',
                'required' => false,
                'translations'=>'{"en": "Hunting management area", "sl": "Lovsko-upravljavsko območje"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false,
                'importable'=>true
            ],
            [
                'key_name_id' => '_location_data.spatial_request_result.lov_ime',
                'key_data_type_id' => 'table_reference',
                'ref' => 'lov_vw',
                'section_in_form' => 'biometry_animal_handling',
                'required' => false,
                'translations'=>['en'=>'Hunting ground', 'sl'=>'Lovišče'],
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false,
                'importable'=>true
            ]], 'mortbiom');
    }

    static function patch_17() {
        self::importVariables([[
            'key_name_id' => '_batch_id',
            'key_data_type_id'=>'table_reference',
            'ref' => 'import_batches',
            'filterable' => true,
            'translations' => ['en'=>'Batch import', 'sl' => 'Paketni uvoz']
        ]],'mortbiom');
    }

    static function patch_16() {
        self::mortbiom_vw();
    }

    static function patch_15() {
        self::catchQuery("update mb2data.mortbiom set licence_list_id=22 where licence_list_id is null");
    }

    static function patch_14() {
        self::catchQuery("update mb2data.mortbiom set _batch_id = 428 where id=1");
        self::catchQuery("update mb2data.mortbiom set _batch_id = 428 where id=2");
        self::catchQuery("update mb2data.mortbiom set _batch_id = 428 where id=3");
        self::catchQuery("update mb2data.mortbiom set _batch_id = 428 where id=4");
        self::catchQuery("update mb2data.mortbiom set _batch_id = 436 where id=5");
        self::catchQuery("update mb2data.mortbiom set _batch_id = 436 where id=6");
        self::catchQuery("update mb2data.mortbiom set _batch_id = 436 where id=7");
        self::catchQuery("update mb2data.mortbiom set _batch_id = 436 where id=8");
    }
    static function patch_13() {
        self::updateVariables([[
            'key_name_id' => 'licence_list_id',
            'importable' => true,
            'required' => true
        ]],'mortbiom');

        self::mortbiom_vw();
    }

    static function patch_12() {
        self::catchQuery("alter table laravel.bears_biometry_data drop column _batch_id");
        self::catchQuery("alter table laravel.bears_biometry_animal_handling drop column _batch_id");
        self::catchQuery("alter table laravel.animal drop column _batch_id");
    }

    static function patch_11() {
        self::catchQuery("ALTER TABLE mb2data.mortbiom ADD column _batch_id integer references mbase2.import_batches(id)");
        self::mortbiom_vw();
    }

    static function patch_10() {
        print_r([
            \DB::delete("DELETE FROM laravel.bears_biometry_data where _batch_id in (428,436)"),
            \DB::delete("DELETE FROM laravel.bears_biometry_animal_handling where _batch_id in (428,436)"),
            \DB::delete("DELETE FROM laravel.animal where _batch_id in (428,436)")
        ]);
    }

    static function patch_9() {
        print_r([\DB::insert("insert into mb2data.mortbiom(_location, front_right_paw_width, tail_length, masa_neto, shoulder_height, thorax_circumference, biometry_loss_reason_list_id, unknown_hunter_finder, abdomen_circumference, hind_right_paw_length, head_circumference, hind_left_paw_length, hair_tuft_length, age, front_left_paw_width, hind_right_paw_width, abdominal_length, hunting_ground_representative, hind_left_paw_width, front_right_paw_length, sex_list_id, nipple_length, front_left_paw_length, body_length, neck_circumference, number_of_premolars_in_the_upper_jaw, collar_list_id, observations_and_notes, upper_left_canines_length, distance_between_lower_canines, testicals_right_length, testicals_left_length, color_list_id, masa_bruto, species_list_id, way_of_withdrawal_list_id, hunter_finder_name_and_surname, title, place_type_list_details, animal_handling_date, name, photos_collected, animal_conflictedness, licence_number, status, receiving_country, licence_list_id, measurer_name_and_surname, location_coordinate_type_list_id, taxidermist_name_and_surname, died_at, telemetry_uid, tooth_type_list_id, conflict_animal_removal_list_id, place_type_list_id, jaw_photos_collected, lower_left_canines_length, distance_between_upper_canines, testicals_right_width, biometry_loss_reason_description, number_of_removal_in_the_hunting_administrative_area, incisors_wear_list_id, project_name, animal_status_on_handling, tooth_type_not_sampled_reason, animal_conflictedness_details, place_of_removal, depot, testicals_left_width, lower_right_canines_length, witness_accompanying_person_name_and_surname, number_of_premolars_in_the_lower_jaw, baculum_length, upper_right_canines_length, fur_pattern_in_lynx_list_id, teats_wear_list_id, ear_length_without_hair, animal_status)
            select st_setsrid(st_makepoint(h.lng, h.lat), 4326) AS _location, front_right_paw_width, tail_length, masa_neto, shoulder_height, thorax_circumference, biometry_loss_reason_list_id, unknown_hunter_finder, abdomen_circumference, hind_right_paw_length, head_circumference, hind_left_paw_length, hair_tuft_length, age, front_left_paw_width, hind_right_paw_width, abdominal_length, hunting_ground_representative, hind_left_paw_width, front_right_paw_length, sex_list_id, nipple_length, front_left_paw_length, body_length, neck_circumference, number_of_premolars_in_the_upper_jaw, collar_list_id, observations_and_notes, upper_left_canines_length, distance_between_lower_canines, testicals_right_length, testicals_left_length, color_list_id, masa_bruto, species_list_id, way_of_withdrawal_list_id, hunter_finder_name_and_surname, title, place_type_list_details, animal_handling_date, name, photos_collected, animal_conflictedness, licence_number, d.status, receiving_country, licence_list_id, measurer_name_and_surname, location_coordinate_type_list_id, taxidermist_name_and_surname, died_at, telemetry_uid, tooth_type_list_id, conflict_animal_removal_list_id, place_type_list_id, jaw_photos_collected, lower_left_canines_length, distance_between_upper_canines, testicals_right_width, biometry_loss_reason_description, number_of_removal_in_the_hunting_administrative_area, incisors_wear_list_id, project_name, animal_status_on_handling, tooth_type_not_sampled_reason, animal_conflictedness_details, place_of_removal, depot, testicals_left_width, lower_right_canines_length, witness_accompanying_person_name_and_surname, number_of_premolars_in_the_lower_jaw, baculum_length, upper_right_canines_length, fur_pattern_in_lynx_list_id, teats_wear_list_id, ear_length_without_hair, a.status as animal_status 
            FROM laravel.bears_biometry_animal_handling h,
           laravel.bears_biometry_data d,
           laravel.animal a
          WHERE h.animal_id = a.id and d.bears_biometry_animal_handling_id = h.id and h._batch_id in (428,436)")]);

    }

    static function patch_8() {
        self::catchQuery("DROP VIEW mb2data.mortbiom_vw");

        self::catchQuery("alter table mb2data.mortbiom drop column hunting_ground;");
		self::catchQuery("alter table mb2data.mortbiom drop column hunting_management_area;");
  		self::catchQuery("alter table mb2data.mortbiom drop column number_of_removal_in_the_hunting_administration_area;");

        self::mortbiom_vw();

        print_r([
            \DB::delete("delete from mbase2.module_variables mvv where key_name_id = 'number_of_removal_in_the_hunting_administration_area'"),
            \DB::delete("delete from mbase2.module_variables mvv where key_name_id = 'hunting_ground'"),
            \DB::delete("delete from mbase2.module_variables mvv where key_name_id = 'hunting_management_area'")
        ]);
    }

    static function patch_7() {
        self::catchQuery("update mbase2.module_variables SET key_name_id='animal.animal_status' where id=
        (SELECT id from mbase2.module_variables_vw where key_name_id='animal.status' and module_name='mortbiom')");

        foreach(['biometry_data','biometry_animal_handling','animal'] as $key) {
            self::catchQuery("update mbase2.module_variables set section_in_form = '$key', key_name_id = replace(key_name_id, '$key.','') where id in
            (select id from mbase2.module_variables_vw mv where module_name='mortbiom' and variable_name like '$key.%')");
        }

        self::catchQuery("update mbase2.module_variables set importable = true where id in 
        (select id from mbase2.module_variables_vw where module_name='mortbiom' and section_in_form is not null)");

        self::catchQuery("alter table mb2data.mortbiom add column animal_status text default 'dead'");
    }

    static function patch_6() {
        self::catchQuery("update mbase2.module_variables set section_in_form='biometry_data' where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name='age')");
        self::catchQuery("update mbase2.module_variables set section_in_form='biometry_data' where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name='masa_bruto')");
        self::catchQuery("update mbase2.module_variables set section_in_form='animal' where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name='sex_list_id')");
        self::catchQuery("update mbase2.module_variables set section_in_form='animal' where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name='species_list_id')");
        self::catchQuery("update mbase2.module_variables set section_in_form='biometry_animal_handling' where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name='way_of_withdrawal_list_id')");
    }

    static function patch_5() {
        self::catchQuery("delete from mbase2.module_variables where id in (select id from mbase2.module_variables_vw mv 
        where module_name='mortbiom' and data_type like 'location_%' and variable_name not like 'biometry%')");

        self::catchQuery("delete from mbase2.module_variables where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name like '%.age')");
        self::catchQuery("delete from mbase2.module_variables where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name like '%.masa_bruto')");
        self::catchQuery("delete from mbase2.module_variables where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name like '%.sex_list_id')");
        self::catchQuery("delete from mbase2.module_variables where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name like '%.species_list_id')");
        self::catchQuery("delete from mbase2.module_variables where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name like '%.event_date')");
        self::catchQuery("delete from mbase2.module_variables where id = (select id from mbase2.module_variables_vw where module_name='mortbiom' and variable_name like '%.way_of_withdrawal_list_id')");
    }

    static function patch_4() {
        self::catchQuery("delete from mbase2.module_variables where id in 
        (select id from mbase2.module_variables_vw where variable_name in ('bears_biometry_animal_handling_id','animal_id') and module_name like '%laravel.%')");
   
        self::catchQuery("update mbase2.module_variables set key_name_id = 'animal.' || key_name_id where id in (
        select id from mbase2.module_variables_vw where module_name='laravel.animal')");

        self::catchQuery("update mbase2.module_variables set key_name_id = 'biometry_animal_handling.' || key_name_id where id in (
        select id from mbase2.module_variables_vw where module_name='laravel.bears_biometry_animal_handling')");

        self::catchQuery("update mbase2.module_variables set key_name_id = 'biometry_data.' || key_name_id where id in (
        select id from mbase2.module_variables_vw where module_name='laravel.bears_biometry_data')");

        self::catchQuery("update mbase2.module_variables set module_id = (select id from mbase2.modules_vw mv where module_key='mortbiom')
        where id in (select id from mbase2.module_variables_vw where module_name like '%laravel.%')");

        self::importVariables([[
            'key_name_id'=>'_location',
            'key_data_type_id'=>'location_geometry',
            'required' => true,
            'importable' => true,
            'filterable' => false,
            'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
        ],
        [
            'key_name_id'=>'_location_data',
            'translations' =>'{"en": "Location", "sl": "Lokacija"}',
            'key_data_type_id'=>'location_data_json',
            'filterable'=>false,
            'required' => true, 'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
        ]
        ], 'mortbiom', 'modules', ['required' => true, 'visible_in_cv_detail'=>true, 'visible_in_cv_grid'=>true, 'filterable' => true, 'importable' => true]);
        self::updateModuleDatabaseSchema('mortbiom');
    }

    static function patch_3() {

        self::addReferenceTable('location_coordinate_type_list','id','laravel','slug',"select id, name->>'default' slug, name from laravel.location_coordinate_type_list", "name as translations");
        self::importVariables([
            [
                'key_name_id' => 'location_coordinate_type_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'location_coordinate_type_list',
                'required' => false,
                'importable' => true,
                'filterable' => false,
                'visible' => false,
                'translations' => ['en' => 'Location type', 'sl' => 'Tip koordinate']
            ]
        ], 'laravel.bears_biometry_animal_handling', 'referenced_tables');

    }

    static function patch_2() {
        self::importVariables([
            [
                'key_name_id' => 'animal_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'laravel.animal',
                'required' => false,
                'importable' => true,
                'filterable' => true,
                'translations' => ['en' => 'Animal name', 'sl' => 'Ime živali']
            ]
        ], 'laravel.bears_biometry_animal_handling', 'referenced_tables');

        self::importVariables([
            [
                'key_name_id' => 'bears_biometry_animal_handling_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'laravel.bears_biometry_animal_handling',
                'required' => false,
                'importable' => true
            ]
        ], 'laravel.bears_biometry_data', 'referenced_tables');
    }

    static function patch_1() {
        self::catchQuery("SELECT setval('laravel.animal_id_seq', COALESCE((SELECT MAX(id)+1 FROM laravel.animal), 1), false);");

        self::addReferenceTable('laravel.bears_biometry_animal_handling','id','laravel','id');
        self::addReferenceTable('laravel.bears_biometry_data','id','laravel','id');
        self::updateVariables([
            [
                'key_name_id'=>'species_list_id', 
                'translations' => json_encode(["sl" => "Živalska vrsta", "en" => "Species"])
            ],
            [
                'key_name_id'=>'sex_list_id', 
                'translations' => json_encode(["sl" => "Spol", "en" => "Sex"])
            ]
        ], 'laravel.animal');
    }

    static function patch_0() {

        self::mortbiom_vw();

        self::importVariables([
            [
                'key_name_id'=>'species_list_id', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'species_list',
                'translations' => json_encode(["sl" => "Živalska vrsta","en" => "Species"])
            ],
            [
                'key_name_id'=>'event_date', 
                'key_data_type_id'=>'date',
                'translations' => json_encode(["sl" => "Datum","en" => "Date"])
            ],
            [
                'key_name_id'=>'way_of_withdrawal_list_id',
                'key_data_type_id'=>'table_reference',
                'ref' => 'way_of_withdrawal_list',
                'filterable' => true,
                'translations' => json_encode(["sl" => "Vrsta odvzema","en" => "Way of withdrawal"])
            ]
        ], 'mortbiom', 'modules', ['required' => true, 'visible_in_cv_detail'=>true, 'visible_in_cv_grid'=>true, 'filterable' => false]);


        self::importVariables([
            [
                'key_name_id'=>'age', 
                'key_data_type_id'=>'integer',
                'translations' => json_encode(["sl" => "Starost","en" => "Age"])
            ],
            [
                'key_name_id'=>'masa_bruto', 
                'key_data_type_id'=>'real',
                'translations' => json_encode(["sl" => "Masa bruto","en" => "Gross weight"])
            ],
            [
                'key_name_id'=>'sex_list_id', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'sex_list',
                'translations' => json_encode(["sl" => "Spol", "en" => "Sex"])
            ]
            
        ], 'mortbiom', 'modules', ['required' => false, 'visible_in_cv_detail'=>true, 'visible_in_cv_grid'=>false, 'filterable' => true]);
    }

    static function mortbiom_vw() {
        $predefinedVariables = [
            'id' => ['h.id', '-m.id'],
            'id_mas' => ['h.id::text','id_mas'],
            '_batch_id' => ['null', '_batch_id'],
            'event_date' => ['animal_handling_date','event_date'],
            '_uname' => ['data_entered_by_user_id', 'ib.user_id'],
            '_location' =>['st_setsrid(st_makepoint(h.lng, h.lat), 4326)', '_location'],
            '_location_data'=>["jsonb_build_object('lat',h.lat,'lon',h.lng,'additional',jsonb_build_object('lname', place_of_removal))","_location_data"],
            'geom' => ["(((('{\"coordinates\":['::text || h.lng) || ','::text) || h.lat) || ']}'::text)::json", "st_asgeojson(_location)::json"],
            'dol_treb' => ['null', 'dol_treb'],
            'int_star_matson'=>['null', 'int_star_matson'],
            'dej_star_lab'=>['null', 'dej_star_lab'],
            'color_list_id' => ['color_list.slug', 'color_list_id'],
            'hunter_finder_country_id' => ['g.slug', 'hunter_finder_country_id']   
        ];

        $rows = \DB::select("select key_name_id, section_in_form from mbase2.module_variables_vw where module_name='mortbiom' and importable=true and key_name_id <> '_location_data'");

        $select = [
            [],[]
        ];

        foreach($predefinedVariables as $key_name_id => $s) {
            $select[0][] = $s[0].' as '.$key_name_id;
            $select[1][] = $s[1].' as '.$key_name_id;
        }

        foreach ($rows as $row) {
            
            if (strpos($row->key_name_id, '_location_data.')!==FALSE) continue;

            if (isset($predefinedVariables[$row->key_name_id])) continue;

            $alias = '';
            if ($row->section_in_form==='biometry_data') {
                $alias = 'd';
            }
            else if ($row->section_in_form==='biometry_animal_handling') {
                $alias = 'h';
            }
            else {
                $alias = 'a';
            }

            $select[0][] = "$alias.".$row->key_name_id;
            $select[1][] = $row->key_name_id;
        }

        $select1 = implode(",\n",$select[0]);
        $select2 = implode(",\n",$select[1]);

        self::catchQuery("DROP VIEW IF EXISTS mb2data.mortbiom_vw");

        self::catchQuery("CREATE OR REPLACE VIEW mb2data.mortbiom_vw
        AS 
        SELECT $select1
            FROM laravel.bears_biometry_animal_handling h
            left join laravel.bears_biometry_data d
                on d.bears_biometry_animal_handling_id = h.id
            left join laravel.animal a
                on h.animal_id = a.id
            left join laravel.groups g
          	    on h.hunter_finder_country_id = g.id
            left join laravel.color_list color_list
                on color_list.id = d.color_list_id
            union all
        SELECT $select2
            FROM mb2data.mortbiom m, mbase2.import_batches ib 
            where m._batch_id = ib.id");
    }
}