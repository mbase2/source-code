<?php namespace patches;

use Exception;
use Mbase2Utils;
use Mbase2SchemaPatches;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');

class cnt extends Mbase2SchemaPatches  {
    static function patch_17() {
        self::catchQuery("alter table mb2data.cnt_observation_reports drop constraint cnt_observation_reports_monitoring_id_fkey");
        self::catchQuery("alter table mb2data.cnt_observation_reports drop constraint cnt_observation_reports_cnt_monitoring_id_fkey");
        self::catchQuery("alter table mb2data.cnt_observation_reports add constraint cnt_observation_reports_cnt_monitoring_id_fkey foreign key (cnt_monitoring_id) references mb2data.cnt_monitoring(id) on delete cascade");
    }

    static function patch_16() {
        self::copyVariableDefinition('_batch_id', 'howling', 'cnt_observation_reports');
        self::updateVariables([[
            'key_name_id' => '_batch_id',
            'filterable' => true
        ]],'cnt_observation_reports');
    }

    static function patch_15() {
        self::cnt_vw();

        self::catchQuery("update mbase2.module_variables set exportable=false, visible_in_cv_grid=false, visible_in_cv_detail =false where id in (
            SELECT id from mbase2.module_variables_vw q  WHERE q.module_name IN ('cnt_monitoring','cnt_observation_reports', 'cnt_permanent_spots') 
            and variable_name = '_location_data.additional.lname'
            )");

        self::importVariables([[
            'key_name_id'=>'_cnt_leading_female_cubs',
            'key_data_type_id'=>'integer',
            'translations'=>['sl' => 'Število medvedk z mladiči']
        ]], 'cnt_observation_reports','referenced_tables');

        self::importVariables([[
            'key_name_id'=>'event_date',
            'key_data_type_id'=>'date',
            'translations'=>['sl' => 'Datum štetja na SŠM']
        ]], 'cnt_monitoring','referenced_tables');

        foreach(['_location_data.spatial_request_result.lov_ime',
        '_location_data.spatial_request_result.ob_uime',
        '_location_data.lat',
        '_location_data.spatial_request_result.luo_ime',
        '_location_data.lon',
        '_location_data.spatial_request_result.oe_ime'] as $key_name_id){
            self::importVariables([[
                'key_name_id'=>$key_name_id,
                'key_data_type_id'=>in_array($key_name_id, ['_location_data.lat,_location_data.lon']) ? 'real' : 'text',
                'exportable'=>true
            ]],'cnt_permanent_spots','referenced_tables');
        }

        self::catchQuery("UPDATE mbase2.module_variables SET visible_in_cv_detail=true, exportable=true WHERE id in (
            SELECT id from mbase2.module_variables_vw q  WHERE q.module_name IN ('cnt_monitoring','cnt_observation_reports', 'cnt_permanent_spots')
            AND variable_name in (
                'event_date',
                '_cnt_permanent_spot_code',
                '_location_name',
                '_cnt_all',
                '_cnt_leading_female_cubs',
                '_cnt_cubs0',
                '_cnt_cubs1',
                'species_list_id',
                'licence_list_id'
            ))");

        self::catchQuery("UPDATE mbase2.module_variables SET visible_in_cv_grid=true WHERE id in (
            SELECT id from mbase2.module_variables_vw q  WHERE q.module_name IN ('cnt_monitoring','cnt_observation_reports', 'cnt_permanent_spots')
            AND variable_name in (
                'event_date',
                '_cnt_permanent_spot_code',
                '_location_name',
                '_cnt_all',
                '_cnt_leading_female_cubs',
                'species_list_id',
                'licence_list_id'
        ))");

        self::updateVariables([[
            'key_name_id'=>'_location_data',
            'exportable'=>true
        ]],'cnt_permanent_spots');

        self::updateVariables([[
            'key_name_id'=>'_location_data.spatial_request_result.oe_ime',
            'exportable'=>false
        ]],'cnt_permanent_spots');
    }

    static function patch_14() {
        self::catchQuery("ALTER TABLE mb2data.cnt_observation_reports ALTER COLUMN _cnt_camera_trap DROP DEFAULT");
    }

    static function patch_13() {
        self::catchQuery("update mbase2.module_variables set required=false where id in (
            select id from mbase2.module_variables_vw mvv where module_name like 'cnt%' and data_type = 'boolean')");
    }

    static function patch_12() { 
        self::catchQuery("delete from mb2data.cnt_observation_reports");
        self::catchQuery("delete from mb2data.cnt_estimations");
        self::catchQuery("delete from mb2data.cnt_monitoring");
    }

    static function patch_11() {
        self::updateVariables([[
            'key_name_id'=>'cnt_monitoring_id',
            'importable'=>true
        ]],'cnt_observation_reports');
    }

    static function patch_10() {
        self::catchQuery("ALTER TABLE mb2data.cnt_observation_reports DROP CONSTRAINT cnt_observation_reports_id_mas_key");
        self::catchQuery("ALTER TABLE mb2data.cnt_observation_reports DROP CONSTRAINT cnt_observation_reports_id_mas_unique_constraint");
        self::catchQuery("ALTER TABLE mb2data.cnt_observation_reports ADD CONSTRAINT cnt_observation_reports_id_mas_cnt_monitoring_unique_contraint UNIQUE (id_mas, cnt_monitoring_id)");
    }

    static function patch_9() {
        self::catchQuery("update mbase2.module_variables set required = false where id in (
            select id from mbase2.module_variables_vw mvv where module_name like 'cnt%'and importable=true and data_type = 'integer')");
    }

    static function patch_8() {
        $items = [
            'ID_GS'=> 'id_mas',
            'Podatek štetja vrnjen [DA/NE]'=> 'data_is_returned',
            'Datum'=> '_start_date',
            'ŠIF. ŠT. MESTA'=> '_cnt_location_id',
            'Uporaba fotokamer ali opazovanje'=> '_cnt_camera_trap',
            'Število videnih medvedov'=> '_cnt_all',
            'Število medvedk z mladiči 0+'=> '_cnt_leading_female_cubs0',
            'Število medvedk z mladiči 1+'=> '_cnt_leading_female_cubs1',
            'Število medvedk z mladiči'=> '_cnt_leading_female',
            'Število mladičev 0+ z medvedko'=> '_cnt_cubs0_with_leading_female',
            'Število mladičev 1+ z medvedko'=> '_cnt_cubs1_with_leading_female',
            'Število mladičev 0+'=> '_cnt_cubs0',
            'Število mladičev 1+'=> '_cnt_cubs1',
            'Skupno število mladičev'=> '_cnt_cubs',
            'Število ostalih medvedov'=> '_cnt_remaining',
            'OPOMBE'=> 'notes',
            'Živalska vrsta'=> 'species_list_id',
            'Licenca'=> 'licence_list_id'
        ];

        $data = ['cnt_observation_reports'=>['types'=>[], 'headers'=>[]]];

        foreach($items as $header => $key) {
            $data['cnt_observation_reports']['headers'][$key] = $header;
            $data['cnt_observation_reports']['types'][$key] = "column";
        }

        \DB::insert("INSERT into mbase2.import_templates (_uid, key_module_id,note, data) values (:_uid, :key_module_id, :note, :data)",
            [
                ':_uid' => 1,
                ':key_module_id' => 'cnt_monitoring',
                ':note' => 'SLO_Štetje_2004-2021',
                ':data' => json_encode($data)
            ]
        );
        
        //\DB::update("UPDATE mbase2.import_templates SET data = :data where note=:note",[':data' => json_encode($data), ':note' => 'SLO_Štetje_2004-2021']);
    }

    static function patch_7() {
        self::addReferenceTable('cnt_permanent_spots','id','mb2data','_cnt_permanent_spot_code');
        self::addReferenceTable('cnt_observation_reports','id','mb2data','id');
        $weight=1;

        self::catchQuery("alter table mb2data.cnt_observation_reports rename monitoring_id to cnt_monitoring_id");
        self::catchQuery("alter table mb2data.cnt_estimations rename monitoring_id to cnt_monitoring_id");

        self::catchQuery("update mbase2.module_variables set key_name_id ='cnt_monitoring_id' where id in (
            select id from mbase2.module_variables_vw where variable_name = 'monitoring_id')");


        self::updateVariables([
            [
                'key_name_id'=>'_start_date',
                'key_data_type_id'=>'date',
                'required' => true,
                'importable' => true
            ]
            ],'cnt_monitoring');

        self::importVariables([
            [   'key_name_id'=>'id_mas',
                'key_data_type_id'=>'text',
                'required' => true,
                'weight_in_import' => $weight++,
                'unique_constraint'=>true
            ],
            [
                'key_name_id'=>'data_is_returned',
                'key_data_type_id'=>'boolean',
                'weight_in_import' => $weight++,
                'translations'=>['sl' => 'Podatek štetja vrnjen']
            ],
            [
                'key_name_id'=>'_cnt_location_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'cnt_permanent_spots',
                'weight_in_import' => $weight++,
                'required' => true,
                'translations' => ['en' => 'Permanent counting spot', 'sl' => 'Števno mesto']
            ],
            [
                'key_name_id'=>'_cnt_camera_trap',
                'key_data_type_id'=>'boolean',
                'weight_in_import' => $weight++,
            ],
            [
                'key_name_id'=>'_cnt_all',
                'key_data_type_id'=>'integer',
                'weight_in_import' => $weight++,
                'required' => true
            ],
            [
                'key_name_id'=>'_cnt_leading_female_cubs0',
                'weight_in_import' => $weight++,
                'key_data_type_id'=>'integer'
            ],
            [
                'key_name_id'=>'_cnt_leading_female_cubs1',
                'weight_in_import' => $weight++,
                'key_data_type_id'=>'integer'
            ],
            [
                'key_name_id'=>'_cnt_leading_female',
                'weight_in_import' => $weight++,
                'key_data_type_id'=>'integer'
            ],
            [
                'key_name_id'=>'_cnt_cubs0_with_leading_female',
                'weight_in_import' => $weight++,
                'key_data_type_id'=>'integer'
            ],
            [
                'key_name_id'=>'_cnt_cubs1_with_leading_female',
                'weight_in_import' => $weight++,
                'key_data_type_id'=>'integer'
            ],
            [
                'key_name_id'=>'_cnt_cubs0',
                'weight_in_import' => $weight++,
                'key_data_type_id'=>'integer'
            ],
            [
                'key_name_id'=>'_cnt_cubs1',
                'weight_in_import' => $weight++,
                'key_data_type_id'=>'integer'
            ],
            [
                'key_name_id'=>'_cnt_cubs',
                'weight_in_import' => $weight++,
                'key_data_type_id'=>'integer'
            ],
            [
                'key_name_id'=>'_cnt_remaining',
                'weight_in_import' => $weight++,
                'key_data_type_id'=>'integer'
            ],
            [
                'key_name_id'=>'notes',
                'weight_in_import' => $weight++,
                'key_data_type_id'=>'text',
                'translations' => ['en' => 'Notes', 'sl' => 'Opombe']
            ],
            [
                'key_name_id' => 'species_list_id',
                'weight_in_import' => $weight++,
                'key_data_type_id' => 'table_reference',
                'ref' => 'species_list',
                'required' => true,
                'translations' => ['en' => 'Species', 'sl' => 'Živalska vrsta']
            ],
            [
                'key_name_id' => 'licence_list_id',
                'weight_in_import' => $weight++,
                'key_data_type_id' => 'table_reference',
                'ref' => 'licence_list',
                'required' => true,
                'translations'=>'{"en": "Licence", "sl": "Licenca"}'
            ]
        ], 'cnt_observation_reports', 'referenced_tables', [
            'importable'=>true
        ]);

        self::updateModuleDatabaseSchema('cnt_observation_reports','referenced_tables');
    }

    static function patch_6() {
        self::catchQuery("update mb2data.cnt_permanent_spots set _cnt_permanent_spot_code='043-043'  where _location_data->'additional'->>'lname'='Kaliček'");
        self::catchQuery("ALTER TABLE mb2data.cnt_permanent_spots ADD CONSTRAINT cnt_permanent_spots__cnt_permanent_spot_code_unique_constraint UNIQUE (_cnt_permanent_spot_code)");
    }

    static function patch_5() {
        self::catchQuery("DROP VIEW mb2data.cnt_view_permanent_spots");
        self::cnt_permanent_spots_vw();
    }

    static function patch_4() {
        self::catchQuery("DELETE from mbase2.module_variables WHERE id in (
            SELECT id from mbase2.module_variables_vw WHERE module_name='cnt_permanent_spots')");

        self::importVariables([
            [
                'key_name_id' => '_location_data.additional.lname',
                'key_data_type_id' => 'text',
                'required' => false,
                'visible_in_table' => true,
                'visible' => false,
                'importable' => true,
                'translations'=>'{"en": "Local name", "sl": "Lokalno ime"}'
            ],
            [
                'key_name_id' => '_cnt_active',
                'key_data_type_id' => 'boolean',
                'visible_in_table' => true,
                'visible' => true,
                'translations'=>'{"en": "Counting spot is active", "sl": "Števno mesto je aktivno"}'
            ],
            [
                'key_name_id' => '_cnt_permanent_spot_code',
                'key_data_type_id' => 'text',
                'visible_in_table' => true,
                'unique_constraint' => true,
                'visible' => true,
                'importable' => true,
                'translations'=>'{"en": "Counting spot code", "sl": "Oznaka števnega mesta"}'
            ],
            [
                'key_name_id'=>'_location',
                'key_data_type_id'=>'location_geometry',
                'required' => true,
                'visible' => true,
                'importable' => false,
                'filterable' => false
            ],
            [
                'key_name_id'=>'_location_data',
                'translations' =>'{"en": "Location", "sl": "Lokacija"}',
                'key_data_type_id'=>'location_data_json',
                'visible' => true,
                'filterable'=>false,
                'importable' => true,
                'required' => true
            ]
            ], 'cnt_permanent_spots', 'referenced_tables');
            
            self::updateModuleDatabaseSchema('cnt_permanent_spots','referenced_tables');

            \DB::update("update mb2data.cnt_permanent_spots set _location_data = _location_data || ('{\"additional\":{\"lname\":\"'|| _local_name || '\"}}')::jsonb
            where _local_name is not null or _local_name <> ''");
    }

    static function patch_3() {
        self::catchQuery("update mbase2.module_variables set importable=false where id in (select id from mbase2.module_variables_vw
        where module_name='cnt_permanent_spots')");
        
        self::catchQuery("update mbase2.module_variables set importable=true where id in (select id from mbase2.module_variables_vw
        where variable_name in ('_location_data', '_cnt_permanent_spot_code') and module_name='cnt_permanent_spots')");
    }

    static function patch_2() {
        self::catchQuery("DELETE from mbase2.module_variables where id in (select id from mbase2.module_variables_vw where module_name='cnt_observations')");
    }

    static function patch_1() {
        self::catchQuery("drop table if exists mb2data.cnt_observations");
    }

    static function patch_0() {
        self::catchQuery("update mbase2.module_variables set data_type_id = (select id from mbase2.code_list_options_vw where key='location_geometry' and list_key='data_types')
        where id=(select id from mbase2.module_variables_Vw mv where module_name ='cnt_permanent_spots' and variable_name='_location')");

        self::catchQuery("update mbase2.module_variables set data_type_id = (select id from mbase2.code_list_options_vw where key='location_data_json' and list_key='data_types')
        where id=(select id from mbase2.module_variables_Vw mv where module_name ='cnt_permanent_spots' and variable_name='_location_data')");
    }

    static function constraints() {
        self::catchQuery(Mbase2Utils::SQLforeignKeyConstraint('mb2data','cnt_observation_reports','cnt_monitoring_id', 'mb2data','cnt_monitoring','id'));
        self::catchQuery(Mbase2Utils::SQLrequiredConstraint('mb2data', 'cnt_observation_reports', 'cnt_monitoring_id'));

        self::catchQuery(Mbase2Utils::SQLforeignKeyConstraint('mb2data','cnt_estimations','cnt_monitoring_id', 'mb2data','cnt_monitoring','id'));
        self::catchQuery(Mbase2Utils::SQLrequiredConstraint('mb2data', 'cnt_estimations', 'cnt_monitoring_id'));

        self::catchQuery(Mbase2Utils::SQLforeignKeyConstraint('mb2data','cnt_observation_reports','_uid', 'mbase2','_users','id'));
        self::catchQuery(Mbase2Utils::SQLforeignKeyConstraint('mb2data','cnt_reposts','_uid', 'mbase2','_users','id'));

        self::catchQuery(Mbase2Utils::SQLtrigger_sync_date_created('mb2data', 'cnt_observation_reports'));
        self::catchQuery(Mbase2Utils::SQLtrigger_sync_date_modified('mb2data', 'cnt_observation_reports'));

        self::catchQuery(Mbase2Utils::SQLuniqueConstraint('mb2data','cnt_contacts','_full_name'));
    }

    static function cnt_vw() {
        self::catchQuery("DROP VIEW IF EXISTS mb2data.cnt_vw");
        self::catchQuery("CREATE VIEW mb2data.cnt_vw as select
        cor.id,
        cor._batch_id,
        cps._location,
        cps._location_data,
        st_asgeojson(cps._location) AS geom,
        cor.species_list_id,
        cor.licence_list_id,
        cm._start_date as event_date,
        cps._cnt_permanent_spot_code,
        cps._location_data->'additional'->>'lname' as _location_name,
        _cnt_all,
        _cnt_leading_female_cubs0 + _cnt_leading_female_cubs1 as _cnt_leading_female_cubs,
        _cnt_cubs0,
        _cnt_cubs1
        from mb2data.cnt_observation_reports cor 
        left join mb2data.cnt_monitoring cm 
        on cor.cnt_monitoring_id = cm.id
        left join mb2data.cnt_permanent_spots cps 
        on cor._cnt_location_id = cps.id");
    }

    static function cnt_permanent_spots_vw() {
       self::catchQuery("CREATE OR REPLACE VIEW mb2data.cnt_permanent_spots_vw
            AS SELECT cps.id,
                cps._location_data,
                cps._cnt_permanent_spot_code,
                cps._cnt_active,
                st_asgeojson(cps._location) AS geom
            FROM mb2data.cnt_permanent_spots cps;");
    }

    static function initPermanentSpots() {
        self::catchQuery("DELETE FROM mb2data.cnt_permanent_spots");

        self::catchQuery("alter table mb2data.cnt_permanent_spots drop column _location;");
        self::catchQuery("alter table mb2data.cnt_permanent_spots add column _location geometry;");

        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(455100,53900),3912),4326),'Dolfetov laz','036-024');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(488500,38800),3912),4326),'Stružnica','094-064');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(522400,45800),3912),4326),'Kaliček','043-043');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(524300,39900),3912),4326),'Police','043-042');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(508900,50500),3912),4326),'Klecpihl','042-041');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(480400,75690),3912),4326),'Debeli hrib','061-048');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(473545,80850),3912),4326),'Gmajna','061-050');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(480120,79740),3912),4326),'Kamnit hrib','061-049');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(476941,74760),3912),4326),'Vrhunska voda','061-047');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(485300,63300),3912),4326),'Malenski stelnik','287-143');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(482200,59700),3912),4326),'Požarnica','287-142');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(470900,58500),3912),4326),'Saduc','099-072');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(511000,42300),3912),4326),'Vusec','044-044');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(477500,86000),3912),4326),'Lavričice','059-045');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(485000,59200),3912),4326),'Javor','095-065');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(486200,66500),3912),4326),'Vrbovec','095-066');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(493100,69600),3912),4326),'Črni dol','223-124');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(508000,47500),3912),4326),'Loch pihelj','039-036');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(473000,61000),3912),4326),'Mrtalos','285-139');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(492590,65050),3912),4326),'Obora S.log -Beli kamen','100-090');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(506890,52660),3912),4326),'Cigelnica','100-079');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(501740,52350),3912),4326),'Hoberlok','100-078');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(504160,49530),3912),4326),'Hrib','100-077');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(501060,55530),3912),4326),'Janezova koča','100-081');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(496620,60330),3912),4326),'Kamenjak','100-084');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(496660,69000),3912),4326),'Obora Smuka - Klanc','100-092');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(495860,54100),3912),4326),'Onek-Bukva','100-080');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(494560,46730),3912),4326),'Peči','100-075');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(500090,63370),3912),4326),'Podstenice','100-088');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(502620,64240),3912),4326),'Pogorelec','100-089');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(496160,45640),3912),4326),'Rajndol','100-073');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(505440,61610),3912),4326),'Rampoha','100-086');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(493420,60370),3912),4326),'Rigl','100-085');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(499510,60220),3912),4326),'Rog','100-083');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(500580,46390),3912),4326),'Studeno','100-074');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(498130,49340),3912),4326),'Suhi potok','100-076');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(496070,65090),3912),4326),'Šenberg','100-091');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(505590,57060),3912),4326),'Štale','100-082');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(499530,69160),3912),4326),'Štrbenk','100-093');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(484857,46070),3912),4326),'Črpalka-Reški hrib','499-162');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(475260,61875),3912),4326),'Glavica','100-087');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(482641,57795),3912),4326),'Grčarske Gredice','499-167');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(482811,50630),3912),4326),'Lesna gorica','499-163');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(479649,54614),3912),4326),'Kotliček','499-160');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(477092,57381),3912),4326),'Lepa gorica','499-166');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(486762,43396),3912),4326),'Mošenik','499-158');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(494200,43500),3912),4326),'Muha vas','499-159');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(497300,42300),3912),4326),'Podlesje','499-157');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(480116,46037),3912),4326),'Ravne','499-165');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(491059,44908),3912),4326),'Ograja-Ajbig','499-161');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(488486,50630),3912),4326),'Sekret- Suhi hrib','499-164');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(490200,66500),3912),4326),'Rjavi pesek','146-096');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(494200,55600),3912),4326),'Mrhovišče','096-067');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(490000,62000),3912),4326),'Trikot','096-068');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(483900,41700),3912),4326),'Borič I','101-094');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(505700,45800),3912),4326),'Luščiči','097-070');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(503200,40500),3912),4326),'Vršič','097-069');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(475100,65000),3912),4326),'Geben','286-140');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(483000,65800),3912),4326),'Jeshova dolina','286-141');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(510800,38500),3912),4326),'Grobek','041-040');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(511600,34800),3912),4326),'Široki del','041-038');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(513100,37000),3912),4326),'Škrilje','041-039');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(509700,59300),3912),4326),'Ašelica','038-035');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(511100,54200),3912),4326),'Kleč','038-034');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(473100,64500),3912),4326),'Globoka dolina','284-138');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(480730,71080),3912),4326),'Špičnik','098-071');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(484850,73000),3912),4326),'Debeli hrib','063-051');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(488800,73300),3912),4326),'Leseni hrib','063-052');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(488200,77300),3912),4326),'Volčje','063-053');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(474700,83800),3912),4326),'Jelovec','060-046');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(467300,80300),3912),4326),'Beli pesek','158-103');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(466500,74000),3912),4326),'Železnica','164-105');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(477500,71800),3912),4326),'Šoba','283-137');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(520500,38500),3912),4326),'Hobiljec','040-037');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(464300,53300),3912),4326),'Beriščkova draga','035-016');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(449500,77650),3912),4326),'Senožeti','026-007');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(454300,73300),3912),4326),'Slivnica','026-006');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(451600,80100),3912),4326),'Vinji vrh','026-008');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(456300,77300),3912),4326),'Kranjče','026-011');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(447100,70000),3912),4326),'Balantove doline','027-010');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(449125,75431),3912),4326),'Puntar','027-009');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(450600,64100),3912),4326),'Sovinjšček','037-033');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(457300,72100),3912),4326),'Jelovci','029-012');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(459200,84400),3912),4326),'Jernejčkov laz','156-102');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(464400,62800),3912),4326),'Urši lazi/ Joškov kot','034-015');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(442941,68595),3912),4326),'Baba','242-056');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(443800,47600),3912),4326),'Brozinov žleb','077-057');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(450670,45285),3912),4326),'Batistov laz','036-021');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(460300,55700),3912),4326),'Bezgovice','036-027');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(449600,55100),3912),4326),'Bradlov laz','036-026');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(451900,57700),3912),4326),'Dedna gora (barka)','036-030');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(450700,60100),3912),4326),'Jurjeva dolina-Pogoreli vrh','036-031');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(455400,60500),3912),4326),'Kačičji laz','036-032');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(461100,48300),3912),4326),'Kujavič','036-023');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(454500,57000),3912),4326),'Laz pri hiši','036-028');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(455600,41000),3912),4326),'Lupova Draga','036-017');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(458000,57000),3912),4326),'Malenski lazi','036-029');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(450830,48600),3912),4326),'Jesenovci','036-022');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(452700,44000),3912),4326),'Okroglina-Palež','036-019');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(458100,54600),3912),4326),'Ovčarija','036-025');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(459300,44200),3912),4326),'Vala','036-020');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(457000,43300),3912),4326),'Vel. Dolčiči','036-018');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(447900,83250),3912),4326),'Predole','148-097');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(447500,88700),3912),4326),'Skalčna pot','148-098');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(461420,82650),3912),4326),'Colnarica','151-100');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(463210,76990),3912),4326),'Gaberje','151-101');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(464640,80850),3912),4326),'Potošlarca','151-099');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(442500,63900),3912),4326),'Zgornje Ravne','244-136');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(445800,81300),3912),4326),'Pranger','031-014');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(443900,72300),3912),4326),'Unški tali','031-013');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(457360,80900),3912),4326),'Piren pod','168-106');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(454960,82750),3912),4326),'Zagabrnice','168-107');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(454800,87070),3912),4326),'Zavodi','168-108');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(468500,82800),3912),4326),'Brezinca','163-104');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(446400,60100),3912),4326),'Ropatija','237-131');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(459000,89800),3912),4326),'Izduhe','145-095');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(448800,50700),3912),4326),'Vidovi lazi','081-063');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(450300,38400),3912),4326),'Dletvo','073-058');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(444300,40900),3912),4326),'Šentjak','078-059');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(501657,77435),3912),4326),'Gabrove doline','377-155');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(522845,66960),3912),4326),'Gajski jarek','226-127');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(518798,64979),3912),4326),'Vahta','226-126');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(505693,73975),3912),4326),'Dule','216-115');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(503990,71690),3912),4326),'Mihovka','216-114');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(527000,69550),3912),4326),'Peteršilka','224-125');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(514300,62105),3912),4326),'Kilovec','218-116');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(512750,64650),3912),4326),'Odd. 7c','218-117');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(497049,77770),3912),4326),'Čeplje – Gliha','221-121');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(492075,75215),3912),4326),'Debeli hrib – Črnagoj','221-120');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(500482,72717),3912),4326),'Dule-Andrejčič','221-119');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(530534,71389),3912),4326),'Ravna gora','220-118');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(507862,61169),3912),4326),'Pajkež','222-122');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(507282,69054),3912),4326),'Staja','222-123');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(432600,38500),3912),4326),'Kovačeva senožet','080-062');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(435500,38800),3912),4326),'Plasine','079-061');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(436900,37400),3912),4326),'Pod Kovnico','079-060');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(419665,64049),3912),4326),'Čebulovica','300-149');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(433100,61000),3912),4326),'Škrnik','243-135');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(424782,68448),3912),4326),'Kalinovec','296-146');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(429967,65439),3912),4326),'Leskovca','296-145');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(427234,61850),3912),4326),'Vremščica','296-144');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(426694,50704),3912),4326),'Javor','307-152');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(425484,45043),3912),4326),'Mavrovc','307-151');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(429282,54786),3912),4326),'Padež','299-147');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(425505,58943),3912),4326),'Sadeže','299-148');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(430312,38120),3912),4326),'Goli vrh','303-150');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(410610,118050),3912),4326),'Lisec','368-154');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(407450,111150),3912),4326),'Pleča','364-153');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(433800,78900),3912),4326),'Konfinska pot','240-133');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(440100,73500),3912),4326),'Pod Travnim vrhom','238-132');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(432200,82500),3912),4326),'Hrušica zgornja','173-111');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(412540,87520),3912),4326),'Zagriža','007-005');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(422375,88034),3912),4326),'Ožbičev laz','073-134');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(416925,88825),3912),4326),'Ilovna ravnica','006-004');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(414300,94700),3912),4326),'Bukov vrh','070-055');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(445250,83850),3912),4326),'Ajnzar','172-109');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(443750,86000),3912),4326),'Mačkovo','172-110');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(423710,74560),3912),4326),'Dolga rovna','005-003');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(427700,73840),3912),4326),'Požganina','005-002');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(438200,80200),3912),4326),'Doline','236-129');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(440700,85300),3912),4326),'Kališe','236-130');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(441200,77600),3912),4326),'Planinska gora','236-128');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(412820,90930),3912),4326),'Korenine','204-112');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(409740,94820),3912),4326),'Krajni žleb','204-113');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(421325,71320),3912),4326),'Kol','003-001');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(442650,89600),3912),4326),'Raskovec','387-156');");
        self::catchQuery("INSERT INTO mb2data.cnt_permanent_spots (_location, _local_name, _cnt_permanent_spot_code) values (st_transform(ST_setsrid(ST_Makepoint(491000,85000),3912),4326),'Sad','064-054');"); 
        
        self::catchQuery("update mb2data.cnt_permanent_spots x
        set _location_data = jsonb_build_object('_location', 
            jsonb_build_object(
            'lat',st_y(a._location), 
            'lon',st_x(a._location), 
            'spatial_request_result', json_build_object('oe_ime',a.oe_ime,'lov_ime',a.lov_ime,'luo_ime',a.luo_ime, 'ob_uime', a.ob_uime)
            ))
        FROM
        (select cps.id, cps._location, su.* from mb2data.cnt_permanent_spots cps,mbase2_ge.spatial_units su where st_intersects(cps._location, geom)) a
        where x.id = a.id");
    }
}   