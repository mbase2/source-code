<?php namespace patches;

use Exception;
use Mbase2Utils;
use Mbase2Database;
use Mbase2Files;
use Mbase2SchemaPatches;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');

class dmg extends Mbase2SchemaPatches  {
    static function patch_108() {
        $rows = \DB::select("SELECT * from mb2data.dmg");

        $cnt = 0;
        foreach($rows as $row) {
            $objects = json_decode($row->_dmg_objects);
            $update = false;
            foreach ($objects as &$o) {
                if (isset($o->cenik->valid_from)) {
                    $o->cenik = json_decode($o->cenik->cenik);
                    $update = true;
                };
            }

            if ($update) {
                $cnt++;
                \DB::update("UPDATE mb2data.dmg SET _dmg_objects=:objects WHERE id=:id", [':objects' => json_encode($objects), ':id' => $row->id]);
            }
            
        }

        echo "UPDATED: $cnt";
    }
    static function patch_107() {
        return;
        $ceniki = [];
        $rows = \DB::select("select * from mb2data.dmg_price_list_by_date a, mb2data.dmg_price_list_cl b where a.dmg_price_list_cl_id = b.id");

        foreach($rows as $row) {
            $key = mb_strtolower($row->text_key);
            if (!isset($ceniki[$key])) {
                $ceniki[$key] = [];
            }

            $ceniki[$key][] = [
                'valid_from' => $row->valid_from,
                'valid_till' => $row->valid_till,
                'cenik' => $row->cenik
            ];            
        }

        $rows = \DB::select("SELECT * from mb2data.dmg");

        $cnt = 0;
        foreach($rows as $row) {
            $objects = json_decode($row->_dmg_objects);
            $update = false;
            foreach ($objects as &$o) {
                if (!isset($o->cenik)) {
                    $okey = mb_strtolower($o->text);
                    if (isset($ceniki[$okey])) {
                        $event_date = $row->_dmg_examination_date;
                        foreach($ceniki[$okey] as $cenik) {
                            if ($event_date >= $cenik['valid_from'] && $event_date <= $cenik['valid_till']) {
                                $o->cenik = $cenik;
                                $update = true;
                                break;
                            }
                        }
                    }

                    if (!isset($o->cenik)) {
                        $event_date = date('Y-m-d', strtotime($event_date . ' -10 days'));
                        foreach($ceniki[$okey] as $cenik) {
                            if ($event_date >= $cenik['valid_from'] && $event_date <= $cenik['valid_till']) {
                                $o->cenik = $cenik;
                                $update = true;
                                break;
                            }
                        }
                    }
                }
            }

            unset($o);

            if ($update) {
                $cnt++;
                \DB::update("UPDATE mb2data.dmg SET _dmg_objects=:objects WHERE id=:id", [':objects' => json_encode($objects), ':id' => $row->id]);
            }
        }
        echo "Updated: $cnt records";
    }

    static function patch_106() {
        return;
        function flattenJson($nodes, $path = '', &$result = []) {
            foreach ($nodes as $node) {
                $currentPath = $path ? $path . ' > ' . $node['text'] : $node['text'];
                if (isset($node['dmg_object_id'])) {
                    // Check if the currentPath already exists and maps to a different dmg_object_id
                    if (isset($result[$currentPath]) && $result[$currentPath] !== $node['dmg_object_id']) {
                        throw new Exception("Duplicate path '$currentPath' maps to different dmg_object_id values.");
                    }
                    $result[$currentPath] = $node['dmg_object_id'];
                }
                if (isset($node['nodes'])) {
                    flattenJson($node['nodes'], $currentPath, $result);
                }
            }
            return $result;
        }
        
        // Assuming you fetch multiple JSON arrays from the database
        $jsonArrays = \DB::select("SELECT valid_from, valid_till, data FROM mb2data.dmg_objects_price_lists");
        
        $finalResult = [];
        foreach ($jsonArrays as $jsonArray) {
            $data = json_decode($jsonArray->data, true);
            $result = flattenJson($data);
            $finalResult = array_merge($finalResult, $result);
        }
        
        // Create the table
        \DB::statement('
            CREATE TABLE IF NOT EXISTS mb2data.dmg_price_list_cl (
                id SERIAL PRIMARY KEY,
                text_key VARCHAR(255) UNIQUE NOT NULL,
                dmg_object_id INTEGER REFERENCES mbase2.code_list_options(id)
            );
        ');

        foreach ($finalResult as $textKey => $dmgObjectId) {
            \DB::table('mb2data.dmg_price_list_cl')->insert([
                'text_key' => $textKey,
                'dmg_object_id' => $dmgObjectId,
            ]);
        }

        // Flatten the JSON data to get the paths and cenik arrays
        function extractCenik($nodes, $path = '', &$result = []) {
            foreach ($nodes as $node) {
                $currentPath = $path ? $path . ' > ' . $node['text'] : $node['text'];
                if (isset($node['cenik'])) {
                    $result[$currentPath] = $node['cenik'];
                }
                if (isset($node['nodes'])) {
                    extractCenik($node['nodes'], $currentPath, $result);
                }
            }
            return $result;
        }

        \DB::statement('
            CREATE TABLE IF NOT EXISTS mb2data.dmg_price_list_by_date (
                id SERIAL PRIMARY KEY,
                dmg_price_list_cl_id INTEGER REFERENCES mb2data.dmg_price_list_cl(id),
                valid_from DATE,
                valid_till DATE,
                cenik JSONB
            );
        ');

        \DB::statement("ALTER TABLE mb2data.dmg_price_list_by_date
            ADD CONSTRAINT unique_dmg_price_list_by_date
            UNIQUE (dmg_price_list_cl_id, valid_from, valid_till);");

        foreach ($jsonArrays as $jsonArray) {
            $data = json_decode($jsonArray->data, true);
            $validFrom = $jsonArray->valid_from;
            $validTill = $jsonArray->valid_till;

            $cenikData = extractCenik($data);

            // Insert each cenik entry into dmg_price_list_by_date
            foreach ($cenikData as $textKey => $cenik) {
                // Get the corresponding dmg_price_list_cl_id
                $dmgPriceListClId = \DB::table('mb2data.dmg_price_list_cl')
                    ->where('text_key', $textKey)
                    ->value('id');

                if ($dmgPriceListClId) {
                    if (empty($validFrom)) {
                        $validFrom='1970-01-01';
                    }

                    if (empty($validTill)) {
                        $validTill='2100-01-01';
                    }

                    \DB::table('mb2data.dmg_price_list_by_date')->insert([
                        'dmg_price_list_cl_id' => $dmgPriceListClId,
                        'valid_from' => $validFrom,
                        'valid_till' => $validTill,
                        'cenik' => json_encode($cenik),
                    ]);
                }
            }
        }
    }
    static function patch_105() {
        $where = "WHERE valid_till is null and label='Bale travne silaže - stiskanje bal'";
        $rows = \DB::select("SELECT * from mb2data.dmg_urne_postavke $where");

        if (count($rows)==1) {
            $cnt = \DB::update("update mb2data.dmg_urne_postavke set cena = 9.59 $where");
            echo "Updated $cnt records\n";
        }
        
    }

    static function patch_104() {
        \DB::delete("DELETE FROM mb2data.dmg_affectees where id = 2845");
    }

    static function patch_103() {
        $nrows = \DB::update("update mbase2.module_variables set exportable = false where id in (select id from mbase2.module_variables_vw mvv where module_name like '%dmg%' and exportable =true and variable_name in ('priimek_ime_oskodovanca_gs',
            'naslov_gs',
            'posta_gs'))");

        print_r($nrows);
    }

    static function patch_102() {
        \DB::statement("ALTER TABLE mb2data.dmg
        ALTER COLUMN _compensation_sum TYPE NUMERIC(8, 2)");

        \DB::statement("CREATE OR REPLACE FUNCTION update_compensation_sum()
        RETURNS TRIGGER AS \$\$
        DECLARE
            compensation_sum NUMERIC;
            additional_compensation_sum NUMERIC;
        BEGIN
            SELECT SUM(mbase2.convert_to_numeric(elem->5->>0))
            INTO compensation_sum
            FROM mb2data.dmg_agreements
            CROSS JOIN LATERAL jsonb_array_elements(_data->'_odskodnina') AS elem
            WHERE _claim_id = NEW._claim_id AND elem->5->>0 <> 'NaN';
        
            SELECT SUM(mbase2.convert_to_numeric(elem->5->>0))
            INTO additional_compensation_sum
            FROM mb2data.dmg_agreements
            CROSS JOIN LATERAL jsonb_array_elements(_data->'_odskodnina_additional') AS elem
            WHERE _claim_id = NEW._claim_id AND elem->5->>0 <> 'NaN';

            UPDATE mb2data.dmg
            SET _compensation_sum = coalesce(compensation_sum,0) + coalesce(additional_compensation_sum,0)
            WHERE id = NEW._claim_id;

            RETURN NEW;
        END;
        \$\$ LANGUAGE plpgsql;");

        \DB::statement("CREATE TRIGGER trg_update_compensation_sum
            AFTER INSERT OR UPDATE OF _data
            ON mb2data.dmg_agreements
            FOR EACH ROW
            EXECUTE FUNCTION update_compensation_sum();");

        \DB::update("UPDATE mb2data.dmg_agreements SET _data=_data");

        $dmg_app_table_vw = self::SQL_dmg_app_table_vw();
        \DB::statement("DROP VIEW mb2data.dmg_app_vw");
        \DB::statement("CREATE VIEW mb2data.dmg_app_vw AS $dmg_app_table_vw");
        self::dmg_vw();
    }

    static function patch_101() {
        self::cenik_animals(true);
    }

    static function patch_100() {
        /**
         * zamenjava ID-ja še v metapodatkih
         */

         $rows = \DB::select("select _dmg_objects objects from mb2data.dmg where id=1127");

         $objects = json_decode($rows[0]->objects);

         foreach($objects as &$obj) {
            if ($obj->data->affectee == 1286) {
                $obj->data->affectee = 639;
            }

            $obj->nodes[0]->text = str_replace("ID: 1286", "ID: 639", $obj->nodes[0]->text);
         }

         \DB::update("UPDATE mb2data.dmg SET _dmg_objects=:objects WHERE id=1127", [':objects' => json_encode($objects)]);

    }

    static function patch_99() {
        $dmg_app_table_vw = self::SQL_dmg_app_table_vw();
        \DB::statement("CREATE VIEW mb2data.dmg_app_vw AS $dmg_app_table_vw");
    }

    static function patch_98() {
        \DB::statement(Mbase2Utils::SQLcreateIndex('mb2data', 'dmg', '_culprit'));
        \DB::statement(Mbase2Utils::SQLcreateIndex('mb2data', 'dmg_batch_imports', '_batch_id'));
        \DB::statement(Mbase2Utils::SQLcreateIndex('mb2data', 'dmg_batch_imports', 'culprit_id'));
    }

    static function patch_97() {
        $rows = \DB::select("select conname as name from pg_constraint where conname like 'dmg_claims__claim_id_%'");

        foreach ($rows as $row) {
            if ($row->name === 'dmg_claims__claim_id_key') continue;
            \DB::statement("ALTER TABLE mb2data.dmg DROP CONSTRAINT {$row->name};");
        }
    }

    static function patch_96() {
        self::dmg_vw();
    }

    static function patch_95() {
        return;
        \DB::statement("CREATE OR REPLACE VIEW mb2data.dmg_narcis_cl_afeectees_vw as SELECT da.id,
                null as _full_name,
                null as _street,
                null as _house_number,
                0 as _post_number,
                null as _phone,
                null as _email,
                da._iban,
                da._tax_id,
                regexp_replace(da._tax_id, '[^0-9]+'::text, ''::text, 'g'::text) AS tax_num
            FROM mb2data.dmg_affectees da;");

        \DB::statement("CREATE OR REPLACE VIEW mb2data.dmg_narcis_others_vw AS WITH others_data AS (
                    SELECT dmg.id AS id_dmg,
                        null AS drugi_udelezenci
                    FROM mb2data.dmg
                    )
            SELECT others_data.id_dmg,
                others_data.drugi_udelezenci
            FROM others_data;");
    }

    static function patch_94() {
        return;
        $rows = \DB::select("SELECT schemaname, tablename
            FROM pg_catalog.pg_tables
            WHERE tablename IN ('dmg_cl_affectees', 'cl_affectees')
            UNION
            SELECT schemaname, viewname AS tablename
            FROM pg_catalog.pg_views
            WHERE viewname IN ('dmg_cl_affectees', 'cl_affectees');");

        ob_clean();
        header('Content-Type: application/json');
        echo json_encode($rows);
        ob_end_flush();
        exit();
    }

    static function patch_93() {
        return;
        $rows = \DB::select("SELECT
            schemaname AS schema_name,
            viewname AS view_name,
            definition AS view_definition
        FROM
            pg_views
        WHERE
            viewname ILIKE '%narcis%'
        ORDER BY
            schema_name, view_name;
        ");

        ob_clean();
        header('Content-Type: application/json');
        echo json_encode($rows);
        ob_end_flush();
        exit();
    }

    static function patch_92() {
        self::dmg_vw();
        foreach([
            'id_mas' => 0.1,
            '_location_data.lat' =>  0.2,
            '_location_data.lon' =>  0.3,
            'location_coordinate_type_list_id' =>  0.35,
            '_location_data.spatial_request_result.lov_ime' =>  0.4,
            '_location_data.spatial_request_result.luo_ime' =>  0.5,
            'oe_list_id' =>  0.6,
            '_location_data.additional.lname' => 0.7
        ] as $key => $value) {
            \DB::update("update mbase2.module_variables set weight_in_export = $value where id =
            (select id from mbase2.module_variables_vw mvv where module_name like 'dmg_batch_imports' and variable_name='$key')");
        }

        self::importVariables([[
            'key_name_id' => '_agreement_status',
            'key_data_type_id'=>'integer',
            'exportable' => true,
            'weight_in_export' => 19.5,
            'translations' => ['en'=>'Agreement status', 'sl' => 'Status sporazuma']
        ],
        [
            'key_name_id' => '_compensation_sum_for_object',
            'key_data_type_id'=>'real',
            'exportable' => true,
            'weight_in_export' => 18.5,
            'translations' => ['en'=>'Compensation sum for object', 'sl' => 'Vrednost odškodnine za objekt']
        ]
        ],'dmg_batch_imports', 'referenced_tables');
    }

    static function patch_91() {
        self::catchQuery("update mbase2.module_variables set visible = false, required=false where id in (select id from mbase2.module_variables_vw where variable_name in ('_iban', '_tax_id') and module_name='dmg_affectees')");
        \DB::update("UPDATE mb2data.dmg_affectees SET _iban=null, _tax_id=null");
    }

    static function patch_90() {
        self::catchQuery("CREATE INDEX idx_dmg_examination_date ON mb2data.dmg(_dmg_examination_date);");
        self::catchQuery("CREATE INDEX idx_event_date ON mb2data.dmg_batch_imports(event_date);");
        self::catchQuery("CREATE INDEX idx_valid_from ON laravel.spatial_unit_filter_type_versions(valid_from);");
        self::catchQuery("CREATE INDEX idx_valid_to ON laravel.spatial_unit_filter_type_versions(valid_to);");
    }

    static function patch_89() {

        self::addCodeListOption('referenced_tables','dmg_objects_price_lists');

        self::importVariables([[
            'key_name_id' => 'valid_from',
            'key_data_type_id' => 'date',
            'visible' => true,
            'visible_in_table'=>true
        ],
        [
            'key_name_id' => 'valid_till',
            'key_data_type_id' => 'date',
            'visible' => true,
            'visible_in_table'=>true
        ],
        [
            'key_name_id' => 'data',
            'key_data_type_id' => 'text',
            'visible' => true,
            'visible_in_table'=>false
        ]
    
    ], 'dmg_objects_price_lists', 'referenced_tables');
    }

    static function patch_88() {
        require_once __DIR__.'/../vendor/shuchkin/simplexlsx/src/SimpleXLSX.php';

        $xlsx = \SimpleXLSX::parse(__DIR__.'/data/Zbirnik_cene_2023.xlsx');
        $xlsx->sheetName(0);

        $noviCenik = [];

        foreach ( $xlsx->rows() as $row ) {
            foreach($row as &$r) {
                $r = trim($r);
            }
            unset($r);
            
            if (count($row)!==6) continue;
            $key=mb_strtolower($row[5].$row[0]);

            $cene = [];

            $c1 = $row[2];
            $e1 = $row[1];

            $c2=$row[4];
            $e2=$row[3];

            foreach([[$c1,$e1],[$c2, $e2]] as $values) {
                $c = $values[0];
                $e = $values[1];

                $a = [];
                if (!empty($e)) {
                    $a['e'] = $e;
                    //$c = str_replace(['.',','],['','.'],$c);
                    if (is_numeric($c)) {
                        $a['c'] = $c;
                    }

                    $cene[] = $a;
                }
            }

            $noviCenik[$key] = $cene;
        }

        //print_r($noviCenik);

        $nkeys = array_keys($noviCenik);

        $rows = \DB::select("SELECT data from mb2data.dmg_objects_price_lists WHERE valid_till is null");
        $data = json_decode($rows[0]->data);

        $popravki=[
            'Sadje - travniški sadovnjak' => 'Sadje – travniški sadovnjak',
            'Drugo sveže sadje (navedi)' => 'Drugo sveže sadje',
            'Sadike sadnega drevja – druge (navedi)' => 'Sadike sadnega drevja - druge'
        ];

        $existingKeys = [];

        foreach ($data as &$kategorija) {
            if (!in_array($kategorija->text, ["Rastlina na kmetijski površini / Rastlinski pridelek"])) continue;
            foreach($kategorija->nodes as &$node) {

                if (isset($popravki[$node->text])) {
                    $node->text = $popravki[$node->text];
                }

                $key = $kategorija->text!=="Čebelarstvo" ? mb_strtolower($node->text) : 'čebelarstvo';
                if ($kategorija->text!=="Čebelarstvo" && !isset($node->nodes)) continue;

                //if ($key!=='živali za pleme') continue;
                foreach(($kategorija->text === 'Čebelarstvo' ? [$node] : $node->nodes) as &$leaf) {
                    
                    if (isset($popravki[$leaf->text])) {
                        $leaf->text = $popravki[$leaf->text];
                    }

                    $ukey = $key.mb_strtolower($leaf->text);
                    $existingKeys[] = $ukey;
                    if (!isset($noviCenik[$ukey]) && strpos($leaf->text, 'navedi')===FALSE) {
                        //echo $ukey."\n";
                        //print_r($leaf);
                    }
                    else if (strpos($leaf->text, 'navedi')===FALSE) {
                        $leaf->cenik = $noviCenik[$ukey];
                    }
                }
                unset($leaf);
            }
            unset($node);
        }
        unset($kategorija);

        //print_r(array_diff($nkeys, $existingKeys));
        //print_r(array_diff($existingKeys, $nkeys));

        $rows = \DB::update("UPDATE mb2data.dmg_objects_price_lists SET valid_till=:valid_till  WHERE valid_till is null", [':valid_till' => '2024-04-30']);
        \DB::insert("INSERT INTO mb2data.dmg_objects_price_lists (data, valid_from) values (:data, '2024-05-01')", [':data'=>json_encode($data)]);

        //print_r($data);
    }

    static function patch_87() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/dmg_narcis_update3.sql'));
    }

    static function patch_86() {
        $res = \DB::delete("DELETE from mbase2.module_variables WHERE key_name_id='_claim_end_date'");
        echo "DELETED: $res";
    }

    static function patch_85() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/narcis/dmg_narcis_cl_afeectees_vw_2.sql'));
    }

    static function patch_84() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/narcis/dmg_narcis_cl_afeectees_vw.sql'));
        \DB::unprepared(file_get_contents(__DIR__.'/sql/narcis/dmg_narcis_cl_deputies_vw.sql'));
    }

    static function patch_83() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/narcis/dmg_narcis_data_vw.sql'));
        \DB::unprepared(file_get_contents(__DIR__.'/sql/narcis/dmg_narcis_protection_code_list.sql'));
        \DB::unprepared(file_get_contents(__DIR__.'/sql/narcis/dmg_narcis_protection_data_vw.sql'));
    }

    static function patch_82() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/dmg_narcis_update2.sql'));
    }

    static function patch_81() {

        self::changeAffectee(1636, 2733, '01/RP3/12/2023');
        /*
        self::catchQuery("ALTER TABLE mb2data.dmg_affectees drop constraint if exists dmg_affectees__tax_id_unique_constraint");

        $res = self::catchQuery("SELECT column_name, is_nullable
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = 'dmg_affectees' and column_name='affectee_id'");

        print_r($res);

        $res = self::catchQuery('INSERT INTO mb2data.dmg_affectees
        ("_full_name", "_street", "_house_number", "_country", "_post_number", "_phone", "_email", "_citizenship", "_tax_id", "_iban", "_foreign_post", "_notes", affectee_id)
        select "_full_name", "_street", "_house_number", "_country", "_post_number", "_phone", "_email", "_citizenship", "_tax_id", "_iban", "_foreign_post", "_notes", affectee_id
        from mb2data.dmg_affectees where id=1636 returning id;');

        print_r($res);

        $newId = $res[0]['id'];

        echo "New ID: $newId";
        */
    }

    static function patch_80() {
        self::updateVariables([[
            'key_name_id' => '_tax_id',
            'translations' => ['en' => 'Tax identification number', 'sl' => 'Davčna številka']
        ]], 'dmg_affectees');
    }

    static function patch_79() {
        self::importVariables([[
            'key_name_id' => 'affectee_id',
            'key_data_type_id' => 'integer',
            'visible' => false
        ]], 'dmg_affectees', 'referenced_tables');

        self::dmg_affectees_vw();
    }

    static function patch_78() {
        self::catchQuery("CREATE OR REPLACE FUNCTION mbase2.on_affectee_upsert()
        RETURNS TRIGGER AS \$\$
        begin

            IF NEW.affectee_id IS NULL THEN
                NEW.affectee_id := NEW.id;
            END IF;

            NEW._tax_id := REPLACE(NEW._tax_id, ' ', '' );
            IF NEW._tax_id IS NOT NULL and EXISTS (
                SELECT 1
                FROM mb2data.dmg_affectees
                WHERE _tax_id is not null and affectee_id = NEW.affectee_id AND _tax_id IS DISTINCT FROM NEW._tax_id
            ) THEN
                RAISE EXCEPTION 'Each affectee_id can have only one tax_id';
            END IF;
            RETURN NEW;
        END;
        \$\$ LANGUAGE plpgsql;");
    }

    static function patch_77() {
        self::catchQuery("alter table mb2data.dmg_affectees add column affectee_id integer");
        self::catchQuery("update mb2data.dmg_affectees set affectee_id = id");
        self::catchQuery("alter table mb2data.dmg_affectees alter column affectee_id set not null");
    }

    static function patch_76() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/dmg_narcis_update2.sql'));
    }

    static function patch_75() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/dmg_narcis_update.sql'));
    }

    static function patch_74() {
        $cnt = \DB::update("UPDATE mb2data.dmg SET _claim_id='14/ŽA8/1/2023' WHERE _claim_id='14/ŽA8/1/2022'");
        echo "UPDATED: $cnt";
    }

    static function patch_73() {
        $cenik = file_get_contents(__DIR__.'/data/cenik-popravek.json');
        $cnt = \DB::update("UPDATE mb2data.dmg_objects_price_lists SET data=:data where id=4 and valid_from='2024-01-15' and valid_till is null",[':data' => $cenik]);
        echo "UPDATED: $cnt";
    }

    static function patch_72() {
        require_once __DIR__.'/../vendor/shuchkin/simplexlsx/src/SimpleXLSX.php';

        $xlsx = \SimpleXLSX::parse(__DIR__.'/data/cenik_20240111.xlsx');
        $xlsx->sheetName(0);

        $noviCenik = [];

        foreach ( $xlsx->rows() as $row ) {
            foreach($row as &$r) {
                $r = trim($r);
            }
            unset($r);
            
            if (count($row)!==6) continue;
            $key=mb_strtolower($row[5].$row[0]);

            $cene = [];

            $c1 = $row[2];
            $e1 = $row[1];

            $c2=$row[4];
            $e2=$row[3];

            foreach([[$c1,$e1],[$c2, $e2]] as $values) {
                $c = $values[0];
                $e = $values[1];

                $a = [];
                if (!empty($e)) {
                    $a['e'] = $e;
                    $c = str_replace(['.',','],['','.'],$c);
                    if (is_numeric($c)) {
                        $a['c'] = $c;
                    }

                    $cene[] = $a;
                }
            }

            $noviCenik[$key] = $cene;
        }

        $nkeys = array_keys($noviCenik);

        $rows = \DB::select("SELECT data from mb2data.dmg_objects_price_lists WHERE valid_till is null");
        $data = json_decode($rows[0]->data);

        $popravki=[
            'Teleta (do starosti 1 meseca) - do 100 kg'=>'Teleta (do starosti 1 meseca) – do 100 kg',
            'Teleta (M stara 1-3 mesece ) - od 100 do 175 kg'=>'Teleta (M stara 1-3 mesece ) – od 100 do 175 kg',
            'Pitovni piščanci – brojlerji (samci. samice)' => 'Pitovni piščanci – brojlerji (samci, samice)',
            'Pure (samci. samice)' => 'Pure (samci, samice)',
            'Kunci. zajci' => 'Kunci, zajci',
            'Teličke (do starosti 1 meseca) - do 100 kg' => 'Teličke (do starosti 1 meseca) – do 100 kg',
            'Teličke (stare 1-3 mesecev) - 100-175 kg' => 'Teličke (stare 1-3 mesecev) – od 100 do 175 kg',
            'Teličke (stare 3-6 mesecev) - 175-250 kg' => 'Teličke (stare 3-6 mesecev) – od 175 do 250 kg',
            'Teličke (stare 6-12 mesecev) - 250 - 375 kg' => 'Teličke (stare 6-12 mesecev) – od 250 do 375 kg',
            'Plemenske telice (12-18 mesecev) - 375 - 500 kg' => 'Plemenske telice (12-18 mesecev) – od 375 do 500 kg',
            'Plemenske telice (18-24 mesecev) - 500-600 kg' => 'Plemenske telice (18-24 mesecev) – od 500 do 600 kg',
            'Breje telice - nad 550 kg' => 'Breje telice – nad 550 kg'
        ];

        $existingKeys = [];

        foreach ($data as &$kategorija) {
            if (!in_array($kategorija->text, ["Čebelarstvo", "Rejna žival"])) continue;
            foreach($kategorija->nodes as &$node) {
                $key = $kategorija->text!=="Čebelarstvo" ? mb_strtolower($node->text) : 'čebelarstvo';
                if ($kategorija->text!=="Čebelarstvo" && !isset($node->nodes)) continue;
                //if ($key!=='živali za pleme') continue;
                foreach(($kategorija->text === 'Čebelarstvo' ? [$node] : $node->nodes) as &$leaf) {
                    if (isset($popravki[$leaf->text])) {
                        $leaf->text = $popravki[$leaf->text];
                    }

                    $ukey = $key.mb_strtolower($leaf->text);
                    $existingKeys[] = $ukey;
                    if (!isset($noviCenik[$ukey]) && strpos($leaf->text, 'navedi')===FALSE) {
                        echo $ukey."\n";
                        print_r($leaf);
                    }
                    else if (strpos($leaf->text, 'navedi')===FALSE) {
                        $leaf->cenik = $noviCenik[$ukey];
                    }
                }
                unset($leaf);
            }
            unset($node);
        }
        unset($kategorija);

        print_r(array_diff($nkeys, $existingKeys));
        print_r(array_diff($existingKeys, $nkeys));

        $rows = \DB::update("UPDATE mb2data.dmg_objects_price_lists SET valid_till=:valid_till  WHERE valid_till is null", [':valid_till' => '2024-01-14']);
        \DB::insert("INSERT INTO mb2data.dmg_objects_price_lists (data, valid_from) values (:data, '2024-01-15')", [':data'=>json_encode($data)]);

        \DB::update("update mb2data.dmg_urne_postavke set valid_till='2024-01-14' WHERE valid_till is null");
        foreach ([
            [':label' => 'Delovna ura – ročna – enostavna dela', ':enota' => 'h', ':cena' => '8.30'],
            [':label' => 'Delovna ura – ročna – zahtevnejša dela', ':enota' => 'h', ':cena' => '10.70'],
            [':label' => 'Delovna ura – ročna – posebna usposobljenost', ':enota' => 'h', ':cena' => '14.80'],
            [':label' => 'Strošek traktorja 60 KW – štirikolesni pogon', ':enota' => 'h', ':cena' => '27.00'],
            [':label' => 'Drugo',':enota' => '', ':cena' => null],
            [':label' => 'Bale travne silaže - zavijanje bal', ':enota' => 'kom', ':cena' => '7.88'],
            [':label' => 'Bale travne silaže - stiskanje bal', ':enota' => 'kom', ':cena' => '8.41']
        ] as $p) {
            \DB::insert('INSERT into mb2data.dmg_urne_postavke (label, enota, cena) values (:label, :enota, :cena)',$p);
        }

        //print_r($data);
    }
    
    static function patch_71() {
        /*
        print_r([
            \DB::delete("DELETE FROM mb2data.dmg_agreements WHERE _claim_id=(SELECT id FROM mb2data.dmg WHERE _claim_id='14/TM1/17/2023/1')"),
            \DB::delete("DELETE FROM mb2data.dmg WHERE _claim_id='14/TM1/17/2023/1'"),
            \DB::update("UPDATE mb2data.dmg SET _claim_id='14/TM1/17/2023/1' WHERE _claim_id='14/TM1/17/2023/2'"),
            \DB::update("UPDATE mb2data.dmg SET __indirect_counter = 0 WHERE _claim_id='14/TM1/17/2023'")
        ]);
        */
    }

    static function patch_70() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/dmg_narcis_view_update.sql'));
    }

    static function patch_69() {
        self::importVariables([[
            'key_name_id' => '_batch_id',
            'key_data_type_id'=>'table_reference',
            'ref' => 'import_batches',
            'filterable' => true,
            'translations' => ['en'=>'Batch import', 'sl' => 'Paketni uvoz']
        ]],'dmg_batch_imports', 'referenced_tables');
    }

    static function patch_68() {
        self::dmg_vw();
    }

    static function patch_67() {
        self::importVariables([
            [
                'key_name_id' => '_location_data.lat',
                'key_data_type_id' => 'real',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Latitude (WGS84)", "sl": "Zemlj. širina (WGS84)"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ],
            [
                'key_name_id' => '_location_data.lon',
                'key_data_type_id' => 'real',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Longitude (WGS84)", "sl": "Zemlj. dolžina (WGS84)"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ]
        ], 'dmg_batch_imports', 'referenced_tables');

        self::importVariables([
            [
                'key_name_id' => '_location_data.spatial_request_result.SI-LOV',
                'key_data_type_id' => 'text',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Hunting ground", "sl": "Lovišče"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ],
            [
                'key_name_id' => '_location_data.spatial_request_result.SI-LUO',
                'key_data_type_id' => 'text',
                'importable' => false,
                'exportable' => true,
                'translations'=>'{"en": "Hunting management area", "sl": "Lovsko-upravljavsko območje"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ]
        ], 'dmg_batch_imports', 'referenced_tables');
    }

    static function patch_66() {
        
        self::addReferenceTable('dmg_culprits_rt','id', 'mbase2', 'key',
        "select clo.* from mbase2.code_list_options clo, mbase2.code_lists cl where cl.id = clo.list_id and label_key = 'culprit_options'",
        "translations as translations");

        self::updateVariables([[
            'key_name_id' => 'culprit_id',
            'key_data_type_id' => 'table_reference',
            'ref' => 'dmg_culprits_rt'
        ]],'dmg_batch_imports');
    }

    static function patch_65() {
        self::catchQuery("UPDATE mbase2.module_variables SET filterable=true WHERE 
        id in (SELECT id from mbase2.module_variables_vw WHERE module_name='dmg_batch_imports' and visible_in_cv_detail=true)");

        foreach(['kol_rnd', 'enota_gs', 'protection_measure'] as $key) {
            self::updateVariables([[
                'key_name_id' => $key,
                'filterable' => false
            ]],'dmg_batch_imports');
        }
    }
    
    static function patch_64() {
        
        foreach(['id_mas', 'culprit_id', 'event_date', '_dmg_object_list_ids', 'protection_measure', '_compensation_sum', '_claim_id'] as $inx => $key) {
            self::updateVariables([[
                'key_name_id' => $key,
                'weight_in_popup' => $inx+1,
                'visible_in_cv_detail' => true
            ]],'dmg_batch_imports');
        }
    }

    static function patch_63() {

        $rows = \DB::select("select * from mb2data.dmg_agreements");

        foreach ($rows as $row) {
            $data = json_decode($row->_data);

            $update = false;
            
            foreach (['_odskodnina','_odskodnina_additional'] as $key) {
                $odskodnina = $data->$key;
                $keyupdate = false;
                foreach ($odskodnina as &$o) {
                    if ($key==='_odskodnina_additional' && isset($o->row)) {
                        $o=[
                            [
                                'key' => $o->row[0]
                            ],
                            $o->row[1],
                            $o->row[2],
                            $o->row[3],
                            str_replace(',','.',$o->row[4]),
                            [
                                floatval($o->row[4]),
                                ""
                            ]
                        ];
                        $update = true;
                        $keyupdate = true;
                    }

                    $value = $o[5][0];
                    if (strpos($value,',') !== FALSE) {
                        $update = true;
                        $keyupdate = true;
                        $o[5][0] = str_replace(',', '.', $value);
                    }
                    else if (strpos(strtolower($value),'null') !== FALSE) {
                        $update = true;
                        $keyupdate = true;
                        $o[5][0] = NULL;
                    }
                    else if (empty(trim($value))) {
                        $update = true;
                        $keyupdate = true;
                        $o[5][0] = NULL;
                    }
                }
                unset($o);

                if ($keyupdate) {
                    $data->$key = $odskodnina;
                }
            }

            if ($update) {
                $updated = \DB::update("UPDATE mb2data.dmg_agreements SET _data = :data WHERE id=:id", [':data' => json_encode($data), ':id' => $row->id]);
                echo "UPDATED: $updated\n";
            }
        }

        \DB::unprepared(file_get_contents(__DIR__.'/sql/convert_to_numeric.sql'));
        self::dmg_vw();
        /*
        select * from (
            select id, _claim_id,
            jsonb_agg(j.value->4),
            jsonb_agg(j.value->5),
            (jsonb_agg(j.value->5->0)->>0) as x
            from mb2data.dmg_agreements 
            LEFT JOIN LATERAL jsonb_array_elements(_data->'_odskodnina') j(value) ON true
            group by id
            ) a where x like '%,%'
        */

        /*
        select id, _claim_id,
        jsonb_agg(j.value->4),
        jsonb_agg(j.value->5),
        sum(replace(j.value->5->>0,',','.')::numeric)
        from mb2data.dmg_agreements 
        LEFT JOIN LATERAL jsonb_array_elements(_data->'_odskodnina') j(value) ON true
        group by id
        */
    }

    static function patch_62() {
        //http://localhost/mbase2/map?qe=eyJnIjp7ImQxIjoiMjAyMS0wMi0xMyIsImQyIjoiMjAyMS0wMi0xMyJ9LCJxIjp7ImRtZyI6e319fQ==
        foreach(['_compensation_sum', 'id_mas', 'kol_rnd', 'enota_gs', 'culprit_id', 'protection_measure'] as $key) {
            self::updateVariables([[
                'key_name_id' => $key,
                'visible_in_cv_detail' => true
            ]],'dmg_batch_imports');
        }

        self::dmg_vw();
    }

    static function patch_61() {
        return;
        print_r([
            \DB::select("select count(*) from mb2data.dmg_vw where id>0;"),
            \DB::select("select count(*) from mb2data.dmg where _claim_status <> 0;"),

            \DB::select("select count(*) from mb2data.dmg_vw where _dmg_examination_date >= '2021-01-01' and _dmg_examination_date <= '2023-12-15';"),
            \DB::select("select count(*) from mb2data.dmg_vw where _claim_status <> 0 and _dmg_examination_date >= '2021-01-01' and _dmg_examination_date <= '2023-12-15';"),
            \DB::select("select count(*) from mb2data.dmg where _claim_status <> 0 and _dmg_examination_date >= '2021-01-01' and _dmg_examination_date <= '2023-12-15';")
        ]);
    }
    
    static function patch_60() {
        self::dmg_vw();
    }

    static function patch_59() {
        self::importVariables([
            [
                'key_name_id' => 'species_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'species_list',
                'translations' =>'{"en": "Species", "sl": "Živalska vrsta"}',
                'importable'=>false,
                'dbcolumn'=>false,
                'visible_in_cv_detail' => true,
                'visible_in_cv_grid' => true
            ]],'dmg_batch_imports', 'referenced_tables');
    }

    static function patch_58() {
        //izbrišeš oškodovanca ID 2546, ostane pa njegov pravi dvojnik ID 2542
        self::changeAffectee(2546, 2542);
        \DB::delete("DELETE FROM mb2data.dmg_affectees where id=:id", [':id' => 2546]);
    }

    static function patch_57() {
        self::catchQuery("UPDATE mbase2.module_variables SET key_name_id='_claim_id' where id = (SELECT id from mbase2.module_variables_vw 
        where key_name_id='st_primera' and module_name='dmg_batch_imports')");

        \DB::update("UPDATE mbase2.import_templates SET data=REPLACE(data::text, 'st_primera', '_claim_id' )::jsonb;");
    }

    static function patch_56() {
        self::catchQuery("ALTER TABLE mb2data.dmg_batch_imports rename column st_primera to _claim_id");
        self::catchQuery("ALTER TABLE mb2data.dmg_batch_imports ADD column _batch_id integer references mbase2.import_batches(id)");
    }

    static function patch_55() {
        self::importVariables([
            [
                'key_name_id'=>'_dmg_object_list_ids', 
                'key_data_type_id'=>'code_list_reference_array',
                'ref'=>'dmg_objects_options',
                'translations' => json_encode(["sl" => "Škodni objekt", "en" => "Damage object"])
            ]
        ], 'dmg_batch_imports', 'referenced_tables', ['importable'=>false, 'required' => false, 'visible_in_cv_detail'=>true, 'visible_in_cv_grid'=>true]);

        self::updateVariables([
            [
                'key_name_id' => 'dmg_object_id',
                'visible_in_cv_detail' => false,
                'visible_in_cv_grid' => false
            ],
            [
                'key_name_id' => 'culprit_id',
                'visible_in_cv_detail' => false,
                'visible_in_cv_grid' => false
            ],
            [
                'key_name_id' => 'id_mas',
                'visible_in_cv_detail' => true,
                'visible_in_cv_grid' => false
            ]
            ],'dmg_batch_imports');

            self::dmg_vw();
    }

    static function patch_54() {
        $rows = \DB::select("select * from mbase2.module_variables_vw where module_name ='dmg_batch_imports' and importable=true and variable_name not in (
            SELECT column_name
                    FROM information_schema.columns
                    WHERE table_schema = 'mb2data'
                    AND table_name   = 'dmg_batch_imports')");

        print_r($rows);

        $rows = \DB::select("SELECT column_name
                    FROM information_schema.columns
                    WHERE table_schema = 'mb2data'
                    AND table_name   = 'dmg_batch_imports'");

        print_r($rows);
    }

    static function patch_53() {

        self::catchQuery("UPDATE mbase2.module_variables SET key_name_id='culprit_id' where id = (SELECT id from mbase2.module_variables_vw 
        where key_name_id='species_list_id' and module_name='dmg_batch_imports')");

        self::catchQuery("UPDATE mbase2.module_variables SET key_name_id='licence_list_id' where id = (SELECT id from mbase2.module_variables_vw 
        where key_name_id='_licence_name' and module_name='dmg_batch_imports')");

        self::importVariables([
            [
                'key_name_id' => 'species_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'species_list',
                'translations' =>'{"en": "Species", "sl": "Živalska vrsta"}',
                'importable'=>false,
                'dbcolumn'=>false,
            ]],'dmg_batch_imports');

        foreach (['event_date','dmg_object_id', 'culprit_id','licence_list_id'] as $key_name_id) {
            self::updateVariables([[
                'key_name_id' => $key_name_id,
                'visible_in_cv_grid' => true,
                'visible_in_cv_detail' => true
            ]],'dmg_batch_imports');
        }

        foreach (['id_mas','st_primera'] as $key_name_id) {
            self::updateVariables([[
                'key_name_id' => $key_name_id,
                'visible_in_cv_grid' => false,
                'visible_in_cv_detail' => true
            ]],'dmg_batch_imports');
        }

        self::dmg_vw();
        
    }

    static function patch_52() {
        $res = \DB::update("UPDATE mb2data.dmg SET _claim_id=:n WHERE _claim_id=:o",[':n' => '13/ZD2/3/2023', ':o'=>'13/ZD2/3/2020']);
        echo "UPDATED: $res records\n";
    }

    static function patch_51() {
        self::importVariables([
            [
                'key_name_id' => 'location_coordinate_type_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'location_coordinate_type_list',
                'required' => false,
                'importable' => true,
                'filterable' => false,
                'visible' => false,
                'translations' => ['en' => 'Location type', 'sl' => 'Tip koordinate']
            ]
        ], 'dmg_batch_imports', 'referenced_tables');

        $id = self::addCodeListOption('culprit_options','Canis aureus','{"sl":"Šakal"}');

        \DB::update("UPDATE mbase2.code_list_options SET enabled=false where id=:id",[':id'=>$id]);
    }

    static function patch_50() {
        self::catchQuery("create or replace view mb2data.oe_list_vw as
        select right(slug,2)::integer as id, name->>'sl' as name from laravel.groups where slug like 'SI-OEZGS%'");

        self::addReferenceTable('oe_list_vw','id', 'mb2data', 'name');

        self::catchQuery("ALTER table mb2data.dmg_batch_imports ADD column oe_list_id integer");
            
        self::importVariables([
            [
                'key_name_id' => 'oe_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'oe_list_vw',
                'required' => false,
                'importable' => true,
                'translations'=>'{"en": "Regional unit SFS", "sl": "Območna enota ZGS"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ],
            [
                'key_name_id' => '_location_data.additional.lname',
                'key_data_type_id' => 'text',
                'required' => false,
                'importable' => true,
                'translations'=>'{"en": "Local name", "sl": "Lokalno ime"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ],
            [
                'key_name_id' => '_location_data.additional.sdist',
                'key_data_type_id' => 'text',
                'required' => false,
                'importable' => true,
                'translations'=>'{"en": "Distance from the settlement", "sl": "Oddaljenost od naselja"}',
                'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ]],'dmg_batch_imports', 'referenced_tables');
    }

    static function patch_49() {

        self::catchQuery("alter table mb2data.dmg_affectees add column _notes text");
        self::catchQuery("update mb2data.dmg_affectees set _notes=_tax_id, _tax_id=null where length(_tax_id) <> 10");

        self::catchQuery("ALTER TABLE mb2data.dmg_affectees  
            ADD CONSTRAINT tax_id_length 
            CHECK (length(_tax_id) = 10)");

        $rows = \DB::select("SELECT * from mb2data.dmg_affectees where _notes is not null");

        echo json_encode($rows);
    }

    static function patch_48() {
        self::changeAffectee(521, 522);
        \DB::delete("DELETE FROM mb2data.dmg_affectees where id=:id", [':id' => 521]);
    }

    static function patch_47() {
        \DB::delete("DELETE FROM mb2data.dmg_affectees WHERE id=-1");
    }

    static function patch_46() {
        \DB::update("UPDATE mb2data.dmg_affectees SET _phone = '0' || _phone where substring(_phone from 1 for 1) <> '0'");
    }

    static function patch_45() {

        self::catchQuery("update mbase2.code_list_options
        set translations=coalesce (translations,'{}'::jsonb) || '{\"sl\":\"ne\"}'::jsonb where key='no'");

        self::catchQuery("update mbase2.code_list_options
        set translations=coalesce (translations,'{}'::jsonb) || '{\"sl\":\"da\"}'::jsonb where key='yes'");

        self::catchQuery("DROP TABLE IF EXISTS mb2data.dmg_batch_imports");

        self::addCodeListOption('referenced_tables','dmg_batch_imports');

        \DB::update("UPDATE mbase2.module_variables SET importable=false WHERE id in (select id from mbase2.module_variables_vw WHERE module_name='dmg')");

        $items = [
            [
                'key_name_id' => 'licence_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'licence_list',
                'translations'=>'{"en": "Licence", "sl": "Licenca"}'
            ],
            [
                'xlsx_cname' => 'ID_MAS', 
                'key_data_type_id' => 'text', 
                'key_name_id' => 'id_mas', 
                'translations' => ['sl'=>'ID_MAS']
            ],

            [
                'key_name_id'=>'_location_data',
                'translations' =>'{"en": "Location", "sl": "Lokacija"}',
                'key_data_type_id'=>'location_data_json',
                'required' => true
            ],
            [
                'key_name_id' => 'location_coordinate_type_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'location_coordinate_type_list',
                'required' => true
            ],
            ['xlsx_cname' => 'KRAJ', 'key_name_id' => '_location_data.additional.lname', 'translations' => ['sl'=>'Lokalno ime']],
            ['xlsx_cname' => 'ODD_NASELJ', 'key_name_id' => '_location_data.additional.sdist', 'translations' => ['sl'=>'Oddaljenost od naselja']],
            
            ['required' => true, 'xlsx_cname' => 'ZIV_VRS_GS_NP', 'key_name_id' => 'culprit_id', 'key_data_type_id'=>'code_list_reference','ref'=> 'culprit_options', 'translations' => ['sl'=>'Povzročitelj']],

            ['xlsx_cname' => 'Genetic_assessment', 'key_data_type_id' => 'text', 'key_name_id' => 'genetic_assessment', 'translations' => ['sl'=>'Genetic_assessment']],
            ['required' => false, 'xlsx_cname' => 'DATUM', 'key_data_type_id' => 'date', 'key_name_id' => 'event_date', 'translations' => ['sl'=>'Datum ogleda škode']],
            ['xlsx_cname' => 'Koordinata x', 'key_name_id' => '_location_data.lon'],
            ['xlsx_cname' => 'Koordinata y', 'key_name_id' => '_location_data.lat'],

            ['xlsx_cname' => 'SK_OBJEKT_mbase_ENG', 'key_data_type_id'=>'code_list_reference', 'ref' => 'dmg_objects_options', 'key_name_id' => 'dmg_object_id', 'translations' => ['sl'=>'Škodni objekt po kategoriji ZGS']],

            ['xlsx_cname' => 'VAROV_1', 'key_data_type_id'=>'text', 'key_name_id' => 'protection_measure', 'translations' => ['sl'=>'Varovanje']],
            ['xlsx_cname' => 'DOKAZI', 'key_data_type_id'=>'text', 'key_name_id' => 'evidence', 'translations' => ['sl'=>'Znaki povzočitelja']],
            ['xlsx_cname' => 'OPOMBE', 'key_data_type_id'=>'text', 'key_name_id' => 'notes', 'translations' => ['sl'=>'Opombe pooblaščenca']],
            ['xlsx_cname' => 'KOL_RND', 'key_data_type_id'=>'real', 'key_name_id' => 'kol_rnd', 'translations' => ['sl'=>'Količina']],
            ['xlsx_cname' => 'ENOTA_GS', 'key_data_type_id'=>'text', 'key_name_id' => 'enota_gs', 'translations' => ['sl'=>'Enota']],
            ['xlsx_cname' => 'VRED_GS', 'key_data_type_id'=>'real', 'key_name_id' => '_compensation_sum', 'translations' => ['sl'=>'Vrednost odškodnine']],

            ['xlsx_cname' => 'SPORZM', 'key_name_id' => 'sporazum', 'key_data_type_id'=>'code_list_reference', 'ref'=>'yes_no', 'translations' => ['sl'=>'Sporazum dosežen (DA/NE)']],
            ['xlsx_cname' => 'PRIIMEK_IME_CENLC', 'key_data_type_id'=>'text', 'key_name_id' => 'priimek_ime_cenilca', 'translations' => ['sl'=>'Pooblaščenec']],
            ['xlsx_cname' => 'ST_PRIMR', 'key_data_type_id' => 'text', 'key_name_id' => 'st_primera', 'translations' => ['sl'=>'Številka škodnega primera']],
            ['xlsx_cname' => 'PRIIMEK_IME_OSKOD_GS', 'key_data_type_id' => 'text', 'key_name_id' => 'priimek_ime_oskodovanca_gs', 'translations' => ['sl'=>'Ime in priimek oškodovanca']],
            ['xlsx_cname' => 'NASLOV_GS', 'key_data_type_id' => 'text', 'key_name_id' => 'naslov_gs', 'translations' => ['sl'=>'Ulica oškodovanca']],
            ['xlsx_cname' => 'POŠTA_GS', 'key_data_type_id' => 'text', 'key_name_id' => 'posta_gs', 'translations' => ['sl'=>'Pošta oškodovanca']]
        ];


        $data = ['dmg'=>['types'=>[], 'headers'=>[]]];

        foreach($items as $i => $item) {
            $key = $item['key_name_id'];
            $header = isset($item['xlsx_cname']) ? $item['xlsx_cname'] : null; 

            if (strpos($key,'_location_data')) {
                
                $key === '_location_data';

                if (!isset($data['dmg_batch_imports']['headers'][$key])) {
                    $data['dmg_batch_imports']['headers'][$key] = ['crs' => 3912];
                }

                if ($header === 'Koordinata x') {
                    $data['dmg_batch_imports']['headers'][$key]['lon'] = $header;
                }
                else if ($header === 'Koordinata y') {
                    $data['dmg_batch_imports']['headers'][$key]['lat'] = $header;
                }
            }
            else {
                $data['dmg_batch_imports']['headers'][$key] = $header;
            }

            $data['dmg_batch_imports']['types'][$key] = "column";

            if (isset($item['key_data_type_id'])) {
                self::importVariables([$item
                    ],'dmg_batch_imports','referenced_tables',['weight_in_import'=>$i+1, 'importable'=>true,'required'=>false]);
            }
            
        }

        //Mbase2Database::updateSchema('dmg_batch_imports', 'mb2data');

        \DB::insert("INSERT into mbase2.import_templates (_uid, key_module_id,note, data) values (:_uid, :key_module_id, :note, :data)",
            [
                ':_uid' => 1,
                ':key_module_id' => 'dmg',
                ':note' => 'SLO_Skode_94do20-2020_261023',
                ':data' => json_encode($data)
            ]
        );

        //\DB::update("UPDATE mbase2.import_templates SET data = :data where note=:note",[':data' => json_encode($data), ':note' => 'SLO_Skode_94do20-2020_261023']);

        $variables = Mbase2Database::query("SELECT * FROM mbase2.module_variables_vw WHERE (module_name = 'dmg_batch_imports' and importable=true) or 
        (module_name='dmg' and variable_name='_location')");

        Mbase2Database::updateSchema('dmg_batch_imports', 'mb2data', null, $variables);
    }

    static function patch_44() {
        print_r([
            \DB::delete("DELETE FROM mb2data.dmg_agreements where _claim_id=:id",[':id'=>3573]),
            \DB::delete("DELETE FROM mb2data.dmg where id=:id",[':id'=>3573]),
            \DB::delete("DELETE FROM mb2data.dmg_affectees WHERE id in (2636, 808)")
        ]);
    }

    static function patch_43() {
        self::updateVariables([[
            'key_name_id' => '_licence_name',
            'weight_in_import' => 0.1,
            'importable' => true,
        ]],'dmg');
    }
    static function patch_42() {
        $variables = Mbase2Database::query("SELECT * FROM mbase2.module_variables_vw WHERE module_name = 'dmg' and (importable=true or variable_name='_location')");

        Mbase2Database::updateSchema('dmg_batch_imports', 'mb2data', null, $variables);
    }

    static function patch_41() {
        self::dmg_vw();
    }

    static function patch_40() {
        \DB::update("UPDATE mb2data.dmg SET affectees_data = null");
    }

    static function patch_39() {
        self::dmg_affectees_vw();
    }

    static function patch_38() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/dmg_affectees_trigger.sql'));
    }

    static function patch_37() {
        return;
        $rows = \DB::select("select * from mb2data.dmg_affectees where _tax_id in (
            select _tax_id from mb2data.dmg_affectees where _tax_id is not null group by _tax_id  having count(*)>1 
            ) order by _tax_id, id");

        $ttid = [];
        foreach($rows as $row) {
            $taxid = $row->_tax_id;
            if (!isset($ttid[$taxid])) {
                $ttid[$taxid] = [];
            }
            $ttid[$taxid][] = $row->id;
        }

        print_r($ttid);

        foreach($ttid as $taxid=>$ids) {
            foreach($ids as $inx=>$id) {
                if ($inx % 2 === 0) {
                    $res = \DB::update("UPDATE mb2data.dmg_affectees SET _tax_id=null where id=:id",[':id' => $id]);
                    echo $res." ".count($ids)." ".$taxid." ".$id."\n";
                    
                }
            }
        }
        /*
        foreach($rows as $inx => $row) {
            if ($inx % 2 === 0) {
                $res = \DB::update("UPDATE mb2data.dmg_affectees SET _tax_id=null where id=:id",[':id' => $row->id]);
                echo $res." -> ".$row->id."\n";
            }
        }
        */

        self::catchQuery("ALTER TABLE mb2data.dmg_affectees ADD CONSTRAINT dmg_affectees__tax_id_unique_constraint UNIQUE (_tax_id)");
    }

    static function patch_36() {
        self::catchQuery("alter table mb2data.dmg_affectees
        drop constraint if exists dmg_affectees__tax_id_key");

        for ($i=0;$i<30;$i++) {
            self::catchQuery("alter table mb2data.dmg_affectees
            drop constraint if exists dmg_affectees__tax_id_key$i");
        }

        \DB::update("update mb2data.dmg_affectees set _tax_id = REPLACE(_tax_id, ' ', '' )");

        \DB::update("update mb2data.dmg_affectees set _tax_id = 'SI' || _tax_id 
        where length(_tax_id) = 8 and _country = 203");

        self::catchQuery("ALTER TABLE mb2data.dmg add column if not exists affectees_data jsonb");

        $rows = \DB::select("SELECT * from mb2data.dmg");

        foreach($rows as $row) {
            $a = json_decode($row->_affectees);
            if (is_null($a)) continue;
            
            $adata = \DB::table('mb2data.dmg_affectees')
            ->whereIn('id', $a)
            ->get()->all();
            
            \DB::update("UPDATE mb2data.dmg SET affectees_data = :adata WHERE id=:id",[':id' => $row->id, ':adata' => json_encode($adata)]);
        }
    }

    static function patch_35() {
        return [
            \DB::delete("DELETE FROM mb2data.dmg_agreements WHERE _claim_id=:dmg_id",[':dmg_id' => 3370]),
            \DB::delete("DELETE FROM mb2data.dmg WHERE _claim_id=:_claim_id",[':_claim_id' => '14/KAN/20/2023/1']),
            \DB::update("UPDATE mb2data.dmg SET __indirect_counter=0 WHERE _claim_id=:_claim_id",[':_claim_id' => '14/KAN/20/2023']),
        ];
    }

    static function patch_34() {
        return [
            \DB::update("UPDATE mb2data.dmg SET __indirect_counter=0 WHERE _claim_id=:_claim_id",[':_claim_id' => '06/PMJ/5/2023/1']),
            \DB::delete("DELETE FROM mb2data.dmg_agreements WHERE _claim_id=:dmg_id",[':dmg_id' => 3422]),
            \DB::delete("DELETE FROM mb2data.dmg WHERE _claim_id=:_claim_id",[':_claim_id' => '06/PMJ/5/2023']),
            \DB::update("UPDATE mb2data.dmg SET _claim_id=:nclid WHERE _claim_id=:_claim_id",[
                ':_claim_id' => '06/PMJ/5/2023/1',
                ':nclid' => '06/PMJ/5/2023'
            ])
        ];
    }

    static function patch_33() {
        $res = \DB::delete("DELETE FROM mb2data.dmg_affectees where id in (2363,2364,2365)");
        print_r($res);
    }

    static function patch_32() {
        $res = \DB::delete("DELETE FROM mb2data.dmg where id in (3033, 3034, 3035)");
        print_r($res);
    }

    static function patch_31() {
        $res = \DB::delete("DELETE FROM mb2data.dmg_agreements where _claim_id in (3033, 3034, 3035)");
        print_r($res);
    }

    static function patch_30() {
        self::catchQuery("update mbase2.module_variables set key_name_id = '_location_data.additional.sdist' where id in (
            select id from mbase2.module_variables_vw where module_name='dmg' and variable_name='dmg_sdist')");

        self::catchQuery("update mbase2.module_variables set key_name_id = '_location_data.additional.lname' where id in (
                select id from mbase2.module_variables_vw where module_name in ('dmg') and variable_name='_lname')");
    }

    static function patch_29() {
        \DB::update("UPDATE mb2data.dmg SET _culprit = 2007 WHERE id=2868");
    }

    static function patch_28() {
        $rows = \DB::select("SELECT * from mb2data.dmg_objects_price_lists where valid_from=:valid_from",[':valid_from'=>'2023-06-06']);

        $data = str_replace(', []','', $rows[0]->data);

        \DB::update("UPDATE mb2data.dmg_objects_price_lists SET data=:data WHERE id=:id",[':id' => $rows[0]->id, ':data'=>$data]);       
    }

    static function patch_27() {

        self::catchQuery("delete from mbase2.module_variables where id=(select id from mbase2.module_variables_vw where variable_name='dmg_cost')");

        self::updateVariables([[
            'key_name_id'=>'_claim_id',
            'importable' => false
        ]],'dmg');

        self::updateVariables([[
            'key_name_id'=>'_compensation_sum',
            'importable' => true
        ]],'dmg');

        self::catchQuery("update mbase2.module_variables set importable = false where id in (
            select id from mbase2.module_variables_vw where module_name='dmg' and importable=true and variable_name like '%status%')");

        self::catchQuery("update mbase2.module_variables set importable = true where id in (
            select id  from mbase2.module_variables_vw where module_name = 'dmg' and variable_name = '_dmg_object_list_ids')");

        foreach(['_dmg_start_date',
        '_location_data',
        '_lname',
        'dmg_sdist',
        '_culprit',
        'dmg_protection_measures',
        '_dmg_object_list_ids',
        'dmg_object_quantity',
        '_dmg_object_unit',
        '_compensation_sum',
        'bim_affectee_name',
        'bim_affectee_address',
        'bim_affectee_post',
        '_claim_id',
        '_deputy',
        '_deputy_notes'] as $inx=>$key) {
            self::updateVariables([[
                'key_name_id'=>$key,
                'weight_in_import'=>$inx+1
            ]],'dmg');
        }
    }

    /**
     * DELETE affectee by ID if not exists in dmg claims
     */
    static function patch_26() {
        if (!isset($_GET['p'])) return;
        $p=intval($_GET['p']);
        echo "select id from mb2data.dmg where _affectees ? '$p'\n";
        $rows = \DB::select("select id,_claim_id from mb2data.dmg where _affectees ?? '$p'");
        print_r($rows);
        echo "\n";

        if (count($rows) === 0) {
            \DB::delete("DELETE FROM mb2data.dmg_affectees WHERE id = :p", [':p' => $p]);
        }
    }

    static function patch_25() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/dmg_narcis_data_vw.sql'));
    }

    static function patch_24() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/dmg_narcis_views.sql'));
    }

    static function patch_23() {
        self::catchQuery("ALTER table mb2data.dmg_objects_price_lists ADD column valid_from date");
        \DB::update("UPDATE mb2data.dmg_objects_price_lists SET valid_from='2022-08-01' WHERE valid_till='2023-06-05'");
        \DB::update("UPDATE mb2data.dmg_objects_price_lists SET valid_from='2023-06-06' WHERE valid_till is null");
    }

    static function patch_22() {
        require_once __DIR__.'/../vendor/shuchkin/simplexlsx/src/SimpleXLSX.php';

        $xlsx = \SimpleXLSX::parse(__DIR__.'/data/cenik.xlsx');
        $xlsx->sheetName(0);

        $noviCenik = [];

        foreach ( $xlsx->rows() as $row ) {
            foreach($row as &$r) {
                $r = trim($r);
            }
            unset($r);
            
            if (count($row)!==7) continue;
            $key=mb_strtolower($row[5].$row[0]);

            $cene = [];

            $c1 = $row[2];
            $e1 = $row[1];

            $c2=$row[4];
            $e2=$row[6];

            foreach([[$c1,$e1],[$c2, $e2]] as $values) {
                $c = $values[0];
                $e = $values[1];

                $a = [];
                if (!empty($e)) {
                    $a['e'] = $e;
                    if (is_numeric($c)) {
                        $a['c'] = $c;
                    }
                }

                $cene[] = $a;
            }

            $noviCenik[$key] = $cene;
        }

        $rows = \DB::select("SELECT data from mb2data.dmg_objects_price_lists WHERE valid_till is null");
        $data = json_decode($rows[0]->data);

        $popravki=[
            'Sončnica. seme'=>'Sončnica, seme',
            'Začimbe. dišavnice in zelišča'=>'Začimbe, dišavnice in zelišča',
            'Krompir. zgodnji'=>'Krompir, zgodnji',
            'Krompir. pozni'=>'Krompir, pozni',
            'Krompir. semenski'=>'Krompir, semenski',
            'Fižol. stročji - visok'=>'Fižol, stročji - visok',
            'Grah.svež'=>'Grah,svež',
            'Kumare. solatne'=>'Kumare, solatne',
            'Solata (zgodnja. poletna. jesenska)'=>'Solata (zgodnja, poletna, jesenska)',
            'Krmne korenovke (pesa. koleraba.'=>'Krmne korenovke (pesa, koleraba, korenje)',
            'Češplje. slive'=>'Češplje, slive',
            'Jabolka. namizna'=>'Jabolka, namizna',
            'Breskve. namizne'=>'Breskve, namizne',
            'Jagode. gojene'=>'Jagode, gojene',
            'Naktarine. namizne'=>'Naktarine, namizne',
            'Borovnice – gojene'=>'Borovnice - gojene',
            'Sadike sadnega drevja – jablane'=>'Sadike sadnega drevja - jablane',
            'Sadike sadnega drevja – hruške'=>'Sadike sadnega drevja - hruške',
            'Sadike sadnega drevja – breskve'=>'Sadike sadnega drevja - breskve',
            'Grozdje. rdeče'=>'Grozdje, rdeče',
            'Grozdje. belo'=>'Grozdje, belo'
        ];

        foreach ($data as &$kategorija) {
            if ($kategorija->text!=="Rastlina na kmetijski površini / Rastlinski pridelek") continue;
            foreach($kategorija->nodes as &$node) {
                $key = mb_strtolower($node->text);
                if (!isset($node->nodes)) continue;
                foreach($node->nodes as &$leaf) {
                    if (isset($popravki[$leaf->text])) {
                        $leaf->text = $popravki[$leaf->text];
                    }

                    $ukey = $key.mb_strtolower($leaf->text);
                    if (!isset($noviCenik[$ukey]) && strpos($leaf->text, 'navedi')===FALSE) {
                        echo $key."\n";
                        print_r($leaf);
                    }
                    else if (strpos($leaf->text, 'navedi')===FALSE) {
                        $leaf->cenik = $noviCenik[$ukey];
                    }
                }
                unset($leaf);
            }
            unset($node);
        }
        unset($kategorija);

        print_r($data);return;

        $rows = \DB::update("UPDATE mb2data.dmg_objects_price_lists SET valid_till=:valid_till  WHERE valid_till is null", [':valid_till' => '2023-06-05']);
        \DB::insert("INSERT INTO mb2data.dmg_objects_price_lists (data) values (:data)", [':data'=>json_encode($data)]);
    }

    static function patch_21() {
        if (!isset($_GET['fun'])) return;
        if (intval($_GET['fun']) === 1) {
            $uid = intval($_GET['uid']);
            $a = \DB::insert("INSERT into mb2data.dmg_deputies (_uname) values (:uid)",[':uid' => $uid]);
            print_r($a);
            echo "\n";
        }
    }

    static function patch_20() {
        if (!isset($_GET['fun'])) return;
        if (intval($_GET['fun']) === 1) {
            echo "Brez lokacije\n";
            $rows = \DB::select("SELECT id from mb2data.dmg where _location is null");
            print_r($rows);  
            echo "\n";  
        }
        else if (intval($_GET['fun']) === 2) {
            echo "Oškodovanci\n";
            $rows = \DB::select("SELECT * from mb2data.dmg_affectees WHERE lower(_full_name) like :p",[':p'=>$_GET['p']]);
            print_r($rows);
            echo "\n";
        }
        else if (intval($_GET['fun']) === 3) {
            $p=intval($_GET['p']);
            echo "select id from mb2data.dmg where _affectees ? '$p'\n";
            $rows = \DB::select("select id,_claim_id from mb2data.dmg where _affectees ?? '$p'");
            print_r($rows);
            echo "\n";
        }
        else if (intval($_GET['fun']) === 4) {
            $ao = intval($_GET['ao']);
            $a = intval($_GET['a']);
            $id = intval($_GET['id']);
            echo "update mb2data.dmg set _affectees = '[\"$a\"]' where id=:id\n";
            $rows = \DB::update("update mb2data.dmg set _affectees = '[\"$a\"]',
            _dmg_objects = replace(_dmg_objects::text, '\"affectee\": \"$ao\"', '\"affectee\": \"$a\"')::jsonb
            where id=:id",[':id'=>$id]);
            print_r($rows);
            echo "\n";
        }
        else if (intval($_GET['fun']) === 5) {
            $ao = intval($_GET['ao']);
            $a = intval($_GET['a']);
            $id = intval($_GET['id']);
            echo "update mb2data.dmg_agreements SET _affectee=$a, _data = jsonb_set(_data, '{_affectee}', '\"$a\"') where _claim_id=$id and _affectee=$ao\n";
            $rows = \DB::update("update mb2data.dmg_agreements SET _affectee=:a, _data = jsonb_set(_data, '{_affectee}', '\"$a\"') where _claim_id=:id and _affectee=:ao",[':id'=>$id,':ao'=>$ao,':a'=>$a]);
            print_r($rows);
            echo "\n";
        }
        else if (intval($_GET['fun']) === 6) {
            $id = intval($_GET['id']);
            $fn = $_GET['fn'];
            echo "update mb2data.dmg_affectees SET _full_name='$fn' WHERE id=$id\n";
            $rows = \DB::update("update mb2data.dmg_affectees SET _full_name=:fn WHERE id=:id",[':id'=>$id, ':fn'=>$fn]);
            print_r($rows);
            echo "\n";
        }
    }

    static function patch_19(){
        self::catchQuery("update mb2data.dmg set _licence_name = 11");
        self::catchQuery("ALTER TABLE mb2data.dmg ALTER COLUMN _licence_name SET DEFAULT 11;");
    }

    static function patch_18() {
        self::catchQuery("update mbase2.module_variables set weight_in_table = 1 where id = (
            select id  from mbase2.module_variables_vw where module_name = 'dmg' and variable_name like '_claim_agreement_status')");
    }

    static function patch_17() {
        $tname = 'dmg';
        self::catchQuery("DROP VIEW mb2data.{$tname}_vw");
        self::catchQuery("ALTER TABLE mb2data.$tname ADD column _batch_id integer references mbase2.import_batches(id)");
        self::dmg_vw(false);
    }

    static function patch_16() {

        $res = \DB::select("SELECT translations->>'sl' as zgs_key, key as slug, id from mbase2.code_list_options_vw WHERE list_key = 'dmg_objects_options'");

        $clmap = array_column($res,'id', 'zgs_key');

        $res = \DB::select("SELECT * from mb2data.dmg");

        foreach ($res as $row) {
            
            $dmgObjects = json_decode($row->_dmg_objects);
            if (empty($dmgObjects)) continue;

            foreach($dmgObjects as &$obj) {
                
                if (isset($clmap[$obj->zgsKey])) {
                    $obj->dmg_object_id = $clmap[$obj->zgsKey];
                }
            }

            \DB::update("UPDATE mb2data.dmg SET _dmg_objects=:dmg_objects WHERE id=:id", [':id' => $row->id, ':dmg_objects' => json_encode($dmgObjects)]);
        }
    }

    static function patch_15() {

    }

    static function patch_14() {
        $res = \DB::delete("DELETE FROM mb2data.dmg_agreements da where _claim_id = (select id from mb2data.dmg where _claim_id = :ocid)",[':ocid'=>'06/BM3/13/2023']);
        echo "delete from mb2data.dmg_agreements da where _claim_id = (select id from mb2data.dmg where _claim_id = :ocid),[':ocid'=>'06/BM3/13/2023']': ".$res."\n";

        $res = \DB::delete("DELETE FROM mb2data.dmg WHERE _claim_id=:ocid",[':ocid'=>'06/BM3/13/2023']);
        echo "06/BM3/13/2023 --> 06/BM3/12/2023, number of deleted records WHERE _claim_id='06/BM3/13/2023': ".$res."\n";

        $res = \DB::delete("DELETE FROM mb2data.dmg_agreements da where _claim_id = (select id from mb2data.dmg where _claim_id = :ocid)",[':ocid'=>'00/PN2/2/2022']);
        echo "delete from mb2data.dmg_agreements da where _claim_id = (select id from mb2data.dmg where _claim_id = :ocid),[':ocid'=>'00/PN2/2/2022']': ".$res."\n";

        $res = \DB::delete("DELETE FROM mb2data.dmg WHERE _claim_id=:ocid",[':ocid'=>'00/PN2/2/2022']);
        echo "DELETE FROM mb2data.dmg WHERE _claim_id=:ocid,[':ocid'=>'00/PN2/2/2022']: ".$res."\n";
    }

    static function patch_13() {
        //06/BM3/13/2023 --> 06/BM3/12/2023
        //$res = \DB::update("UPDATE mb2data.dmg SET _claim_id=:ncid WHERE _claim_id=:ocid",[':ncid'=>'06/BM3/12/2023', ':ocid'=>'06/BM3/13/2023']);
        //echo "06/BM3/13/2023 --> 06/BM3/12/2023, number of updates (you are in troubles if not 1 or 0): ".$res."\n";
    }

    static function patch_12() {
        self::updateVariables([
            [
                'key_name_id'=>'_compensation_sum', 
                'visible_in_table' => false,
                'visible_in_popup' => false
            ],
            [
                'key_name_id'=>'_dmg_object_list_ids', 
                'visible_in_table' => false,
                'visible_in_popup' => false
            ],
            [
                'key_name_id'=>'species_list_id', 
                'visible_in_table' => false,
                'visible_in_popup' => false
            ],
        ], 'dmg');
    }

    static function patch_11() {
        self::updateVariables([
            [
                'key_name_id'=>'species_list_id', 
                'translations' => json_encode(["sl" => "Živalska vrsta","en" => "Species"])
            ]
        ], 'dmg');
    }

    static function patch_10() {
        self::dmg_vw();
    }

    static function patch_9() {

        self::importVariables([
            [
                'key_name_id'=>'species_list_id', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'species_list',
                'translations' => json_encode(["sl" => "Živalska vrsta","en" => "Species"])
            ]
        ], 'dmg', 'modules', ['required' => true, 'visible_in_cv_detail'=>true, 'filterable' => false]);

        \DB::update("UPDATE mbase2.referenced_tables SET additional = 'name as translations' WHERE id = (SELECT id from mbase2.referenced_tables_vw WHERE key='species_list')");

        //commone view GRID
        foreach (['event_date','_dmg_object_list_ids', 'species_list_id'] as $key_name_id) {
            self::updateVariables([[
                'key_name_id' => $key_name_id,
                'visible_in_cv_grid' => true
            ]],'dmg');
        }
    }

    static function patch_8() {
        self::updateVariables([[
            'key_name_id' => '_dmg_object_unit',
            'translations' => json_encode(["sl" => "Enota škodnega objekta", "en" => "Damage object unit"])
        ],
        [
            'key_name_id' => '_claim_id',
            'translations' => json_encode(["sl" => "ID zahtevka", "en" => "ID of damage case"])
        ],
        [
            'key_name_id' => '_dmg_object_list_ids',
            'translations' => json_encode(["sl" => "Škodni objekt", "en" => "Damage object"])
        ],
        [
            'key_name_id' => 'event_date',
            'translations' => json_encode(["sl" => "Datum", "en" => "Date"])
        ]        
        ], 'dmg');

        self::updateVariables([
        [
            'key_name_id' => '_dmg_object_list_ids',
            'filterable' => true
        ]], 'dmg');

    }

    static function patch_7() {
        
        self::catchQuery("delete from mbase2.module_variables where id = (select id from mbase2.module_variables_vw where variable_name='_species_name' and module_name='dmg')");
        self::catchQuery("delete from mbase2.module_variables where id = (select id from mbase2.module_variables_vw where variable_name='_zgs_damage_objects' and module_name='dmg')");
        self::catchQuery("delete from mbase2.module_variables where id = (select id from mbase2.module_variables_vw where variable_name='_dmg_objects_fil_imp' and module_name='dmg')");

        \DB::update("update mbase2.module_variables set visible_in_cv_detail=false, visible_in_cv_grid=false
        where id in (select id from mbase2.module_variables_vw where module_name='dmg')");

        self::importVariables([
            [
                'key_name_id'=>'_compensation_sum', 
                'key_data_type_id'=>'real',
                'translations'=>['en' =>'Compensation value', 'sl' => 'Vrednost odškodnine']
            ],
            [
                'key_name_id'=>'_dmg_object_list_ids', 
                'key_data_type_id'=>'code_list_reference_array',
                'ref'=>'dmg_objects_options'
            ]
        ], 'dmg', 'modules', ['required' => false, 'visible_in_cv_detail'=>true]);

        self::catchQuery("ALTER table mb2data.dmg add column if not exists _compensation_sum real");
        self::catchQuery("ALTER table mb2data.dmg add column if not exists _dmg_object_list_ids jsonb");
        
        //commone view DETAIL
        foreach(['event_date', '_dmg_object_unit','_dmg_object_list_ids','dmg_object_quantity','_claim_id','_culprit'] as $key_name_id) {
            \DB::update("update mbase2.module_variables set visible_in_cv_detail=true
        where id in (select id from mbase2.module_variables_vw where module_name='dmg' and variable_name=:key_name_id)",[':key_name_id' => $key_name_id]);
        }

        self::dmg_vw();
    }

    static function patch_6() {
        self::catchQuery("DROP VIEW mb2data.dmg_culprits_vw");
        self::catchQuery("ALTER TABLE mb2data.dmg_culprits DROP COLUMN key_mbase2"); 
        self::catchQuery("drop table mb2data.dmg_skodni_objekti");
        
        self::catchQuery("CREATE OR REPLACE VIEW mb2data.dmg_culprits_vw
        AS SELECT dc.id,
            dc.key_item,
            dc.key_parent,
            clo1.id AS _item_id
        FROM mb2data.dmg_culprits dc
            LEFT JOIN mbase2.code_list_options_vw clo1 ON dc.key_item::text = clo1.key::text and clo1.list_key='culprit_options'");
    }

    /**
     * culprits
     */
    static function patch_5() {
        $beforeReplacement = [
            'Drozgi (npr. kos, cikovt, carar, …)' => 'Drozgi (npr. kos, cikovt, carar)',
            'Druga vrsta ptice pevke (navedi)' => 'Druga vrsta ptice pevke',
            'culprit_options.Drugo' => 'Drugo',
            'Neugotovljivo / neznano' => 'Neugotovljivo/ neznano'];
        
        foreach($beforeReplacement as $before=>$after) {
            \DB::update("UPDATE mbase2.code_list_options SET key=:after 
            where id = (select id from mbase2.code_list_options_vw where key=:before and list_key='culprit_options')", [
                ':after' => $after,
                ':before' => $before
            ]);

            \DB::update("UPDATE mb2data.dmg_culprits SET key_item=:after where key_item=:before",[
                ':after' => $after,
                ':before' => $before
            ]);
        }

        $rows = \DB::select("SELECT * from mb2data.dmg");

        foreach($rows as $row) {
            if (isset($beforeReplacement[$row->_selected_culprit])) {
                \DB::update("UPDATE mb2data.dmg SET _selected_culprit = null WHERE id=:id", [':id'=>$row->id]);
            }    
        }

        $replacementMap = ['Bela štorklja' => 'Ciconia ciconia',
        'Bober' => 'Castor fiber',
        'Črna vrana' => 'Corvus corone',
        'Črna žolna' => 'Dryocopus martius',
        'Dihur' => 'Mustela putorius',
        'Divja mačka' => 'Felis silvestris',
        'Drozgi (npr. kos, cikovt, carar)' => 'Turdidae (npr. Turdus merula, T. viscivorus, T. philomelos)',
        'Druga vrsta ptice pevke' => 'Other species of a songbird',
        'Druga vrsta ujede ali sove' => 'Other species of a bird of prey or an owl',
        'Drugo' => 'Other',
        'Evrazijski ris' => 'Lynx lynx',
        'Kanja' => 'Buteo buteo',
        'Kavka' => 'Corvus monedula',
        'Kragulj' => 'Accipiter gentilis',
        'Krokar' => 'Corvus corax',
        'Labod grbec' => 'Cygnus olor',
        'Mala podlasica' => 'Mustela nivalis',
        'Navadna veverica' => 'Sciurus vulgaris',
        'Neugotovljiva vrsta ptice pevke' => 'Unidentifiable species of a songbird',
        'Neugotovljiva vrsta žolne' => 'Unidentifiable species of Picidae - Woodpeckers',
        'Neugotovljivo/ neznano' => 'Unidentifiable/ unknown',
        'Nezavarovana vrsta (tudi pes)' => 'Unprotected species (also a dog)',
        'Pivka' => 'Picus canus',
        'Planinski orel' => 'Aquila chrysaetos',
        'Poljska vrana' => 'Corvus frugilegus',
        'Rjavi medved' => 'Ursus arctos',
        'Škorec' => 'Sturnus vulgaris',
        'Velika podlasica - hermelin' => 'Mustela erminea',
        'Veliki detel' => 'Dendrocopus major',
        'Vidra' => 'Lutra lutra',
        'Volk' => 'Canis lupus',
        'Zelena žolna' => 'Picus viridis'];

        foreach($replacementMap as $before=>$after) {
            \DB::update("UPDATE mbase2.code_list_options SET key=:after, translations=:translations || translations
            where id = (select id from mbase2.code_list_options_vw where key=:before and list_key='culprit_options')", [
                ':after' => $after,
                ':before' => $before,
                ':translations' => json_encode(['sl'=>$before])
            ]);

            \DB::update("UPDATE mb2data.dmg_culprits SET key_item=:after where key_item=:before",[
                ':after' => $after,
                ':before' => $before
            ]);
        }

        $rows = \DB::select("SELECT * from mb2data.dmg");

        foreach($rows as $row) {
            if (isset($replacementMap[$row->_selected_culprit])) {
                \DB::update("UPDATE mb2data.dmg SET _selected_culprit = null WHERE id=:id", [':id'=>$row->id]);
            }    
        }
    }

    static function patch_2() {
        self::catchQuery("ALTER TABLE mb2data.dmg_damage_objects rename to dmg_objects_price_lists");

        $res = \DB::select("SELECT translations->>'sl' as zgs_key, key as slug, id from mbase2.code_list_options_vw WHERE list_key = 'dmg_objects_options'");

        $clmap = array_column($res,'id', 'zgs_key');
        
        $rows = \DB::select("SELECT * from mb2data.dmg_objects_price_lists");

        $callback = function (&$node) use($clmap) {
            if (!isset($node->zgsKey)) {
                echo "Error: zgsKey not defined\n";
                return;
            }

            if (!isset($clmap[$node->zgsKey])) {
                echo "Error: zgsKey code list map not defined\n";
                return;
            }

            $node->dmg_object_id=$clmap[$node->zgsKey];
        };

        foreach($rows as $row) {
            $data = json_decode($row->data);
            Mbase2Utils::priceListLeavesIterator($data, $callback);
            \DB::update("UPDATE mb2data.dmg_objects_price_lists SET data=:data WHERE id=:id", [
                ':data' => json_encode($data),
                ':id' => $row->id
            ]);
        }

        self::catchQuery("ALTER table mb2data.dmg add column if not exists _dmg_object_list_ids jsonb");
        
        $rows = \DB::select("SELECT * from mb2data.dmg_vw");
        
        foreach($rows as $row) {
            $b = [];
            if (!isset($row->_zgs_damage_objects)) continue;
            $a = json_decode($row->_zgs_damage_objects);
            foreach($a as $key) {
                if (empty($key)) continue;
                if (!isset($clmap[$key])) continue;
                $b[] = $clmap[$key];
            }
            
            \DB::update("UPDATE mb2data.dmg SET _dmg_object_list_ids=:b WHERE id=:id", [':b'=>json_encode($b), ':id' => $row->id]);
        }
    }

    //damage objects

    static function patch_1() {

        $replacementMap = [
            "Čebelarstvo" => "Beekeeping",
            "Človek" => "Human",
            "Delovni psi in hišni ljubljenčki" => "Working dogs and pets",
            "Divjad v obori" => "Game in enclosure",
            "Drobnica" => "Sheep and goats",
            "Druge vrste rejnih živali" => "Other livestock for breed",
            "Drugo" => "Other",
            "Govedo" => "Cattle",
            "Gradbeni ali varovalni objekt" => "Facility or protective measure",
            "Grozdje" => "Grapes",
            "Konji in osli" => "Horses and donkeys",
            "Koruza in druge žitarice" => "Corn and other cereals",
            "Krmilnik za divjad" => "Automatic feeder for wildlife",
            "Perutnina" => "Poultry",
            "Poljščine in vrtnine razen žit" => "Crops and vegetables (except cereals)",
            "Prašič" => "Domestic pig",
            "Ribe" => "Fish",
            "Sadje, sadno drevje" => "Fruits, fruit trees",
            "Travna silaža, bale, seno" => "Grass silage, bales and hay",
            "Travne mešanice in travna ruša" => "Grass mixtures and grass sod",
            "Vozilo" => "Vehicle"];

        foreach ($replacementMap as $sl => $en) {
            \DB::update("UPDATE mbase2.code_list_options SET key=:slug, translations=:translations WHERE id=
            (select id from mbase2.code_list_options_vw where key=:key and list_key='dmg_objects_options')",[
                ':slug' => $en,
                ':translations'=>json_encode(['sl'=>$sl]),
                ':key' => $sl]);
        }
    }

    static function SQL_dmg_app_table_vw() {
        return "SELECT dmg.id,
            null as id_mas,
            lsl.id as species_list_id,
            dmg._claim_id,
            dmg._claim_status,
            dmg._uid,
            dmg._deputy,
            dmg._affectees,
            dmg._culprit as culprit_id,
            dmg._culprit as _culprit,
            dmg._culprit_signs,
            dmg._deputy_notes,
            dmg._affectee_notes,
            dmg._affectee_claim_notes,
            dmg._attachments,
            dmg._dmg_objects,
            dmg._dmg_start_date,
            dmg._dmg_end_date,
            dmg._dmg_noticed_date,
            dmg._dmg_examination_date,
            dmg._genetic_samples,
            dmg._selected_culprit,
            dmg.date_record_created,
            dmg.date_record_modified,
            dmg._note,
            dmg.__indirect_counter,
            dmg._dmg_date_type,
            dmg._others,
            dmg._batch_id,
            dmg._expected_event,
            dmg._location_data,
            dmg._licence_name as licence_list_id,
            dmg._licence_name,
            dmg._location,
            st_asgeojson(dmg._location) AS geom,
            dmg._uid AS _uname,
            dmg._dmg_examination_date AS event_date,
            dmg._dmg_object_list_ids,
            dmgl.kol_rnd,
            dmgl.enota_gs,
            dmgl.protection_measure,
            dmg._compensation_sum,
            CASE 
            WHEN _claim_status = 1 and jsonb_array_length(_affectees) = a.c THEN 2
            WHEN _claim_status = 1 and jsonb_array_length(_affectees) <> a.c THEN 1
            ELSE _claim_status
            end as _claim_agreement_status
            FROM mb2data.dmg dmg
                LEFT JOIN mb2data.dmg_agreements_completed_count_vw a on a._claim_id = dmg.id
                left join mbase2.code_list_options clo on clo.id = dmg._culprit
                left join laravel.species_list lsl on lsl.slug = clo.key
                left join (
                    select id,
                        jsonb_agg((j.value ->'data'->>'kolicina')::text) AS kol_rnd,
                        jsonb_agg((j.value ->'data'->>'enota')::text) AS enota_gs,
                        string_agg(protection_measure,'|') AS protection_measure
                        from mb2data.dmg
                        LEFT JOIN LATERAL jsonb_array_elements(dmg._dmg_objects) j(value) ON true
                        left join LATERAL jsonb_object_keys(j.value ->'data'->'_nacini_varovanja'->0) protection_measure on true
                        group by id
                ) dmgl on dmgl.id = dmg.id";
    }

    static function dmg_vw($dropView = true) {
        /*

         jsonb_agg(a3.value) AS protection_measure,
            coalesce (sum(mbase2.convert_to_numeric(j_odskodnina.value->5->>0)),0) + coalesce(sum(mbase2.convert_to_numeric(j_odskodnina_additional.value->5->>0)),0) as _compensation_sum,
            CASE 
            WHEN _claim_status = 1 and jsonb_array_length(_affectees) = a.c THEN 2
            WHEN _claim_status = 1 and jsonb_array_length(_affectees) <> a.c THEN 1
            ELSE _claim_status
            end as _claim_agreement_status
        FROM mb2data.dmg dmg
        	LEFT JOIN LATERAL jsonb_array_elements(dmg._dmg_objects->'data'->'_nacini_varovanja') a1(value) ON true
            LEFT JOIN LATERAL jsonb_array_elements(a1.value) a2(value) ON true
            LEFT JOIN LATERAL jsonb_object_keys(a2.value->0) a3(value) ON true
            LEFT JOIN LATERAL jsonb_array_elements(dmg._dmg_objects) j(value) ON true

            */

        //sum((replace((j.value->5)::text,'"null"','null')::jsonb->>0)::numeric)
        /*
        select id, _claim_id,
        sum((j.value->5->>0)::numeric) s1,
        sum((ja.value->5->>0)::numeric) s2
        from mb2data.dmg_agreements 
        LEFT JOIN LATERAL jsonb_array_elements(_data->'_odskodnina') j(value) ON true
        LEFT JOIN LATERAL jsonb_array_elements(_data->'_odskodnina_additional') ja(value) ON true
        group by id
        */

        if ($dropView) {
            self::catchQuery("drop view if exists mb2data.dmg_vw");
        }

        $appTableView = self::SQL_dmg_app_table_vw();
        $batchImportTableView = self::SQL_dmg_batch_import_table_vw();

        self::catchQuery("CREATE OR REPLACE VIEW mb2data.dmg_vw
        AS 
        $appTableView
        union all 
        $batchImportTableView");
    }

    private static function SQL_dmg_batch_import_table_vw() {
        return "select 
            -dbi.id as id,
            id_mas,
            lsl.id as species_list_id,
            _claim_id,
            1 as _claim_status,
            ib.user_id,
            null as _deputy,
            null as _affectees,
            culprit_id,
            culprit_id as _culprit,
            null as _culprit_signs,
            notes as _deputy_notes,
            null as _affectee_notes,
            null as _affectee_claim_notes,
            null as _attachments,
            '[{}]'::jsonb as _dmg_objects,
            event_date as _dmg_start_date,
            event_date as _dmg_end_date,
            event_date as _dmg_noticed_date,
            event_date _dmg_examination_date,
            ('[\"' || genetic_assessment || '\"]')::jsonb as _genetic_samples,
            null as _selected_culprit,
            null as date_record_created,
            null as date_record_modified,
            null as notes,
            null as __indirect_counter,
            null as _dmg_date_type,
            null as _others,
            _batch_id,
            null as _expected_event,
            _location_data,
            licence_list_id,
            licence_list_id as _licence_name,
            _location,
            st_asgeojson(_location) AS geom,
            ib.user_id as _uname,
            event_date,
            ('[\"' || dmg_object_id || '\"]')::jsonb as _dmg_object_list_ids,
            ('[\"' || kol_rnd || '\"]')::jsonb as kol_rnd,
            ('[\"' || enota_gs || '\"]')::jsonb as enota_gs,
            protection_measure::text as protection_measure,
            _compensation_sum,
            null as _claim_agreement_status
            from mb2data.dmg_batch_imports dbi 
                left join mbase2.import_batches ib on dbi._batch_id = ib.id
                left join mbase2.code_list_options clo on clo.id = dbi.culprit_id
                left join laravel.species_list lsl on lsl.slug = clo.key";
    }

    private static function changeAffectee($oldId, $newId, $claimId=null) {
        $oldId = intval($oldId);
        $newId = intval($newId);

        $rows = \DB::select("select * from mb2data.dmg where (_affectees @> '[\"$oldId\"]' or _affectees @> '[$oldId]') and _claim_id=:claim_id",[':claim_id' => $claimId]);
        //
        foreach($rows as $row) {
            $affectees = json_decode($row->_affectees);

            foreach($affectees as $inx => $aid) {
                if ($aid == $oldId) {
                    $affectees[$inx] = $newId;
                }
            }

            $affectees = json_encode($affectees);

            $objects = json_decode($row->_dmg_objects);

            foreach($objects as $inx => $object) {
                $data = $object->data;
                if ($data->affectee == $oldId) {
                    $data->affectee = $newId;
                }
                $object->data = $data;
                $objects[$inx] = $object;
            }

            $objects = str_replace("(ID: $oldId)", "(ID: $newId)", json_encode($objects));

            $num = \DB::update("UPDATE mb2data.dmg SET _affectees=:affectees, _dmg_objects=:objects WHERE id=:id", [':affectees' => $affectees, ':objects'=>$objects, ':id' => $row->id]);

            echo "UPDATE mb2data.dmg: $num\n";

            self::changeAffecteeAgreement($oldId, $newId, $row->id);
        }
    }

    private static function changeAffecteeAgreement($oldId, $newId, $claimId) {
        $claimId = intval($claimId);
        $rows = \DB::select("select * from mb2data.dmg_agreements where _affectee = $oldId and _claim_id=$claimId");
        foreach($rows as $row) {
            $data = json_decode($row->_data);
            $data->_affectee = $newId;
            $data = json_encode($data);

            $num = \DB::update("UPDATE mb2data.dmg_agreements SET _data=:data, _affectee=:aid WHERE id=:id", [':data' => $data, ':aid'=>$newId, ':id' => $row->id]);

            echo "UPDATE mb2data.dmg_agreements: $num\n";
        }
    }

    static function dmg_affectees_vw() {
        self::catchQuery("DROP VIEW mb2data.dmg_affectees_vw");
        self::catchQuery("
            CREATE OR REPLACE VIEW mb2data.dmg_affectees_vw
            AS SELECT a.id,
            a.affectee_id,
            a._full_name,
            a._phone,
            a._street,
            a._house_number,
            a._country,
            a._email,
            a._post_number,
            (p._post_number || ' '::text) || p._post_name AS _post,
            a._foreign_post,
            a._tax_id,
            a._iban,
            a._citizenship
            from mb2data.dmg_affectees a
            left join
            mbase2.poste p on a._post_number = p._post_number");
    }

    static function cenik_animals($update = false) {
        require_once __DIR__.'/../vendor/shuchkin/simplexlsx/src/SimpleXLSX.php';

        $xlsx = \SimpleXLSX::parse(__DIR__.'/data/cenik_20250120.xlsx');
        $xlsx->sheetName(0);

        $noviCenik = [];

        foreach ( $xlsx->rows() as $row ) {
            foreach($row as &$r) {
                $r = trim($r);
            }
            unset($r);
            
            if (count($row)!==6) continue;
            $key=mb_strtolower($row[5].$row[0]);

            $cene = [];

            $c1 = $row[2];
            $e1 = $row[1];

            $c2=$row[4];
            $e2=$row[3];

            foreach([[$c1,$e1],[$c2, $e2]] as $values) {
                $c = $values[0];
                $e = $values[1];

                $a = [];
                if (!empty($e)) {
                    $a['e'] = $e;
                    $c = str_replace(['.',','],['','.'],$c);
                    if (is_numeric($c)) {
                        $a['c'] = $c;
                    }

                    $cene[] = $a;
                }
            }

            $noviCenik[$key] = $cene;
        }

        $nkeys = array_keys($noviCenik);

        $rows = \DB::select("SELECT data from mb2data.dmg_objects_price_lists WHERE valid_till is null");
        $data = json_decode($rows[0]->data);

        $popravki=[
            'Teleta (do starosti 1 meseca) - do 100 kg'=>'Teleta (do starosti 1 meseca) – do 100 kg',
            'Teleta (M stara 1-3 mesece ) - od 100 do 175 kg'=>'Teleta (M stara 1-3 mesece ) – od 100 do 175 kg',
            'Pitovni piščanci – brojlerji (samci. samice)' => 'Pitovni piščanci – brojlerji (samci, samice)',
            'Pure (samci. samice)' => 'Pure (samci, samice)',
            'Kunci. zajci' => 'Kunci, zajci',
            'Teličke (do starosti 1 meseca) - do 100 kg' => 'Teličke (do starosti 1 meseca) – do 100 kg',
            'Teličke (stare 1-3 mesecev) - 100-175 kg' => 'Teličke (stare 1-3 mesecev) – od 100 do 175 kg',
            'Teličke (stare 3-6 mesecev) - 175-250 kg' => 'Teličke (stare 3-6 mesecev) – od 175 do 250 kg',
            'Teličke (stare 6-12 mesecev) - 250 - 375 kg' => 'Teličke (stare 6-12 mesecev) – od 250 do 375 kg',
            'Plemenske telice (12-18 mesecev) - 375 - 500 kg' => 'Plemenske telice (12-18 mesecev) – od 375 do 500 kg',
            'Plemenske telice (18-24 mesecev) - 500-600 kg' => 'Plemenske telice (18-24 mesecev) – od 500 do 600 kg',
            'Breje telice - nad 550 kg' => 'Breje telice – nad 550 kg'
        ];

        $existingKeys = [];

        $steviloOdstranjenih = 0;

        foreach ($data as &$kategorija) {
            if (!in_array($kategorija->text, ["Čebelarstvo", "Rejna žival"])) continue;
            foreach($kategorija->nodes as &$node) {
                $key = $kategorija->text!=="Čebelarstvo" ? mb_strtolower($node->text) : 'čebelarstvo';
                if ($kategorija->text!=="Čebelarstvo" && !isset($node->nodes)) continue;
                //if ($key!=='živali za pleme') continue;
                $leaves = $kategorija->text === 'Čebelarstvo' ? [$node] : $node->nodes;
                foreach($leaves as $leafKey => &$leaf) {
                    if (isset($popravki[$leaf->text])) {
                        $leaf->text = $popravki[$leaf->text];
                    }

                    $ukey = $key.mb_strtolower($leaf->text);
                    $existingKeys[] = $ukey;
                    if (!isset($noviCenik[$ukey]) && strpos($leaf->text, 'navedi')===FALSE) {
                        echo $ukey."\n";
                        print_r($leaf);
                        unset($leaves[$leafKey]);
                        $steviloOdstranjenih++;
                    }
                    else if (strpos($leaf->text, 'navedi')===FALSE) {
                        $leaf->cenik = $noviCenik[$ukey];
                    }
                }
                unset($leaf);

                if ($kategorija->text !== 'Čebelarstvo') {
                    $node->nodes = array_values($leaves);
                }
            }
            unset($node);
        }
        unset($kategorija);

        echo "\n--------------------------\n";
        print_r(array_diff($nkeys, $existingKeys));
        print_r(array_diff($existingKeys, $nkeys));
        echo "\nŠtevilo odstranjenih elementov: $steviloOdstranjenih--------------------------\n";
        echo json_encode($data, JSON_PRETTY_PRINT);
        
        if (!$update) {
            return;
        }

        $rows = \DB::update("UPDATE mb2data.dmg_objects_price_lists SET valid_till=:valid_till  WHERE valid_till is null", [':valid_till' => '2025-01-19']);
        \DB::insert("INSERT INTO mb2data.dmg_objects_price_lists (data, valid_from) values (:data, '2025-01-20')", [':data'=>json_encode($data)]);

        \DB::update("update mb2data.dmg_urne_postavke set valid_till='2025-01-19' WHERE valid_till is null");
        foreach ([
            [':label' => 'Delovna ura – ročna – enostavna dela', ':enota' => 'h', ':cena' => '10.50'],
            [':label' => 'Delovna ura – ročna – zahtevnejša dela', ':enota' => 'h', ':cena' => '13.40'],
            [':label' => 'Delovna ura – ročna – posebna usposobljenost', ':enota' => 'h', ':cena' => '18.50'],
            [':label' => 'Strošek traktorja 60 KW – štirikolesni pogon', ':enota' => 'h', ':cena' => '33.10'],
            [':label' => 'Drugo',':enota' => '', ':cena' => null],
            [':label' => 'Bale travne silaže - zavijanje bal', ':enota' => 'kom', ':cena' => '8.90'],
            [':label' => 'Bale travne silaže - stiskanje bal', ':enota' => 'kom', ':cena' => '8.90']
        ] as $p) {
            \DB::insert('INSERT into mb2data.dmg_urne_postavke (label, enota, cena) values (:label, :enota, :cena)',$p);
        }


    }
}