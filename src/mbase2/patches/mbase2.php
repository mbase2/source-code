<?php namespace patches;

use Exception;
use Mbase2Database;
use Mbase2Drupal;
use Mbase2Utils;
use Mbase2SchemaPatches;
use Mbase2dtl\Helpers\GeneralMbase2Helper;
use Illuminate\Support\Facades\Storage;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');

class mbase2 extends Mbase2SchemaPatches  {
    static function patch_46() {
    	self::catchQuery("CREATE OR REPLACE VIEW mb2data.dmg_narcis_tax_code_list AS SELECT * from mb2data.narcis_tax_code_list");
    }
    static function patch_45() {
        /**
         * ID           POVZROCITELJ  TAX_ID
            2063     Ciconia ciconia   1389
            14068  Canis aureus      744
        */
        
        $res = \DB::delete("delete from mb2data.narcis_tax_code_list where id_mbase=2036 and key_mb='Bela štorklja'");
        echo "\nDELETED: $res\n";

        \DB::insert("INSERT INTO mb2data.narcis_tax_code_list (id_mbase, key_mb)
        SELECT id, translations->>'sl'
        FROM mbase2.code_list_options_vw
        WHERE id IN (2063, 14068);");

        \DB::update("UPDATE mb2data.narcis_tax_code_list SET tax_id=1389, slovenskoimetax=lower(key_mb) WHERE id_mbase=2063");
        \DB::update("UPDATE mb2data.narcis_tax_code_list SET tax_id=744, slovenskoimetax=lower(key_mb) WHERE id_mbase=14068");
    }

    static function patch_44() {
        \DB::update("update laravel.spatial_unit_filter_type_versions set valid_to = '2100-01-01' where valid_to > '2025-01-01'");
    }

    static function patch_43() {
        self::catchQuery("UPDATE laravel.spatial_unit_filter_elements SET name=name::jsonb->>'name' WHERE spatial_unit_filter_type_version_id in (6,7)");
    }

    static function patch_42() {
        //new spatial units april 2024

        \DB::unprepared(file_get_contents(dirname(__FILE__) . '/sql/slo_lovd_2024_wgs84.sql'));
		\DB::unprepared(file_get_contents(dirname(__FILE__) . '/sql/add_spatial_units_april_2024.sql'));
    }

    static function patch_41() {
        self::catchQuery("alter table mbase2.module_variables add column weight_in_popup_cv real");
        self::catchQuery("update mbase2.module_variables set weight_in_popup_cv = weight_in_popup");

        self::module_variables_vw();
    }

    static function patch_40() {
        self::catchQuery("update mbase2.module_variables set key_name_id = '_location_data.spatial_request_result.lov_ime' where key_name_id = '_location_data.spatial_request_result.SI-LOV'");
        self::catchQuery("update mbase2.module_variables set key_name_id = '_location_data.spatial_request_result.luo_ime' where key_name_id = '_location_data.spatial_request_result.SI-LUO'");
    }

    static function patch_39() {
        /**
         * cleaning up the unused photo references and duplicates of user_id 1411
         */

        $deletedCount = [];

        $res = \DB::delete("delete from mbase2.uploads where id in (
            SELECT t1.id
            FROM mbase2.uploads t1
            JOIN mbase2.uploads t2 ON t1.file_hash = t2.file_hash
            WHERE t1.uid = 1411
            AND t1.uid <> t2.uid)");

        $deletedCount[] = $res;

        foreach (['sop','ct'] as $tname) {
            $cname = $tname ==='sop' ? '_photos' : 'photos';

            $res = \DB::delete("DELETE from mbase2.uploads where file_hash in (
                SELECT DISTINCT file_hash
                    FROM (select file_hash from mbase2.uploads where subfolder='$tname') u
                    LEFT JOIN (
                        SELECT DISTINCT jsonb_array_elements_text($cname) AS fh
                        FROM mb2data.$tname
                    ) photos ON u.file_hash = photos.fh
                    WHERE photos.fh IS NULL) and subfolder='$tname'");

            $deletedCount[] = $res;
        }

        /**alternative which works way slower */
        /*
        SELECT DISTINCT u.file_hash
        FROM (select file_hash from mbase2.uploads where subfolder='sop') u
        LEFT JOIN mb2data.sop s ON s._photos @> ('["' || u.file_hash || '"]')::jsonb
        WHERE s._photos IS NULL;
        */

        print_r($deletedCount);
    }

    static function patch_38() {
        self::catchQuery("CREATE OR REPLACE VIEW mbase2.code_list_options_vw
            AS SELECT clo.id,
                clo.list_id,
                clo.key,
                clo.translations,
                clo.enabled,
                cl.label_key AS list_key,
                cl.list_key_id
            FROM mbase2.code_list_options clo
                LEFT JOIN ( SELECT cl_1.id,
                        cl_1.label_key,
                        clo_1.id AS list_key_id
                    FROM mbase2.code_lists cl_1,
                        mbase2.code_list_options clo_1
                    WHERE clo_1.key::text = cl_1.label_key::text) cl ON clo.list_id = cl.id");
    }

    static function patch_37() {
        self::catchQuery("DROP VIEW mbase2.module_variables_vw");
        self::catchQuery("ALTER table mbase2.module_variables ADD column exportable boolean default false");
        self::catchQuery("UPDATE mbase2.module_variables SET exportable = importable");
        self::module_variables_vw();
    }

    static function patch_36() {
        self::catchQuery("drop view if exists mbase2_ge.spatial_units_by_date");
        self::catchQuery("
        create view mbase2_ge.spatial_units_by_date as
        select filter_types.slug, filter_elements.name name,
        filter_type_versions.valid_from, filter_type_versions.valid_to, su.geom
        from 
                    mbase2_ge.spatial_units su, 
                    laravel.spatial_units_spatial_unit_filter_elements su_filter_elements,
                    laravel.spatial_unit_filter_elements filter_elements,
                    laravel.spatial_unit_filter_type_versions filter_type_versions,
                    laravel.spatial_unit_filter_types filter_types
                    where
                    su.gid = su_filter_elements.spatial_unit_gid
                    and su_filter_elements.spatial_unit_filter_element_id = filter_elements.id
                    and filter_elements.spatial_unit_filter_type_version_id = filter_type_versions.id
                    and filter_type_versions.spatial_unit_filter_type_id = filter_types.id");
    }

    static function patch_35() {
        self::catchQuery("DROP VIEW mbase2.module_variables_vw");
        self::catchQuery("ALTER table mbase2.module_variables ADD column weight_in_export real");
        self::catchQuery("UPDATE mbase2.module_variables SET weight_in_export = weight_in_import");
        self::module_variables_vw();
    }

    static function patch_34() {
        self::catchQuery("CREATE OR REPLACE VIEW mbase2.code_list_options_vw
        AS SELECT clo.id,
            clo.list_id,
            clo.key,
            clo.translations,
            clo.enabled,
            cl.label_key AS list_key,
            cl.list_key_id
        FROM mbase2.code_list_options clo
            LEFT JOIN ( SELECT cl_1.id,
                    cl_1.label_key,
                    clo_1.id AS list_key_id
                FROM mbase2.code_lists cl_1,
                    mbase2.code_list_options clo_1
                WHERE clo_1.key::text = cl_1.label_key::text) cl ON clo.list_id = cl.id
                where enabled=true;");
    }

    static function patch_33() {
        self::catchQuery("drop view mbase2.luo_vw");
        self::catchQuery("create view mbase2.luo_vw as
        select distinct split_part(slug,'-',4) code, (name::jsonb)->>'name' slug from laravel.spatial_unit_filter_elements where slug like 'SI-LUO%'");

        self::catchQuery("drop view mbase2.lov_vw");
        self::catchQuery("create view mbase2.lov_vw as
        select distinct split_part(slug,'-',4) code, (name::jsonb)->>'name' slug from laravel.spatial_unit_filter_elements where slug like 'SI-LOV%'");

        self::addReferenceTable('luo_vw','code', 'mbase2', 'slug');
        self::addReferenceTable('lov_vw','code', 'mbase2', 'slug');
    }
    
    static function patch_32() {
        self::catchQuery("drop view mbase2.group_countries_vw");
        self::catchQuery("
        create view mbase2.group_countries_vw as
        select groups.id, groups.slug, groups.name from 
                laravel.groups, 
                laravel.group_types_countries, 
                laravel.group_types 
            where 
            group_types_countries.country_id=groups.id and
            group_types_countries.group_type_id = group_types.id and 
            group_types.slug='COUNTRIES'");
    }

    static function patch_31() {
        self::catchQuery("drop view mbase2.laravel_lov_vw");
        self::catchQuery("drop view mbase2.laravel_luo_vw");

        foreach(['SI-LUO', 'SI-LOV'] as $key) {
            $rows = \DB::select("select min(a.id) id from laravel.spatial_unit_filter_type_versions a, laravel.spatial_unit_filter_types b
            where a.spatial_unit_filter_type_id  = b.id and b.slug = :slug",[':slug' => $key]);

            $id=$rows[0]->id;

            $res = \DB::update("update laravel.spatial_unit_filter_elements set slug = '$key-' ||  $id || '-' || split_part(slug,'-',3)  
            where slug like '$key%'
            and array_length(string_to_array(slug,'-'),1) = 3");

            echo $res."\n";
        }

        self::catchQuery("delete from mbase2.referenced_tables where id=(select id from mbase2.referenced_tables_vw where key = 'laravel_lov_vw')");
        self::catchQuery("delete from mbase2.referenced_tables where id=(select id from mbase2.referenced_tables_vw where key = 'laravel_luo_vw')");
        self::catchQuery("delete from mbase2.referenced_tables where id=(select id from mbase2.referenced_tables_vw where key = 'view_lov')");
        self::catchQuery("delete from mbase2.referenced_tables where id=(select id from mbase2.referenced_tables_vw where key = 'view_luo')");

        self::catchQuery("drop view mbase2.luo_vw");
        self::catchQuery("create view mbase2.luo_vw as
        select distinct split_part(slug,'-',4) code, (name::jsonb)->>'name' slug from laravel.spatial_unit_filter_elements where slug like 'SI-LUO%'");

        self::catchQuery("drop view mbase2.lov_vw");
        self::catchQuery("create view mbase2.lov_vw as
        select distinct split_part(slug,'-',4) code, (name::jsonb)->>'name' slug from laravel.spatial_unit_filter_elements where slug like 'SI-LOV%'");

        self::addReferenceTable('luo_vw','code', 'mbase2', 'slug');
        self::addReferenceTable('lov_vw','code', 'mbase2', 'slug');
    }

    static function patch_30() {
        self::catchQuery(Mbase2Utils::SQLfunction_sync_date_created());
        self::catchQuery("DROP VIEW IF EXISTS mbase2.import_templates_vw");
        self::catchQuery("DROP TABLE IF EXISTS mbase2.import_templates");
        self::catchQuery("CREATE TABLE IF NOT EXISTS mbase2.import_templates(
            id serial primary key,
            _uid integer references laravel.users(id),
            key_module_id varchar,
            note varchar,
            date_record_created timestamp,
            data jsonb
        )");

        self::catchQuery(Mbase2Utils::SQLtrigger_sync_date_created('mbase2', 'import_templates'));

        self::catchQuery("CREATE OR REPLACE VIEW mbase2.import_templates_vw AS 
        SELECT 
            imp.id, 
            imp.data,
            imp.date_record_created,
            imp.key_module_id, 
            imp.note, 
            u.name user_name FROM mbase2.import_templates imp, laravel.users u where imp._uid = u.id");
    }

    static function patch_29() {
        self::catchQuery("create or replace view mb2data.mbase2_users_vw as
        select u.id, u.name,u.username, u.email,jsonb_agg(g.id) groups from laravel.users u
        left join laravel.users_groups ug on ug.user_id=u.id 
        left join laravel.groups g on ug.group_id = g.id
        group by u.id, u.name,u.username, u.email");
    }

    static function patch_28() {
        self::catchQuery("CREATE OR REPLACE VIEW mbase2.user_groups_vw AS select g.id, g.slug, g.name from laravel.groups g, laravel.group_types gt where g.group_type_id = gt.id and gt.slug='MBASE2-MODULE-ROLES'");
        self::addReferenceTable('user_groups_vw','id', 'mbase2', 'slug');
    }

    static function patch_27() {
        self::catchQuery("CREATE or REPLACE VIEW mbase2.licence_list_vw as select * from laravel.licence_list");
    }

    static function patch_26() {
        
        //cleaning up

        self::catchQuery("update mbase2.module_variables set ref=(select id from mbase2.referenced_tables_vw rtv where key='licence_list') where id in
        (select id from mbase2.module_variables_vw mvv where reference='licences')");

        self::catchQuery("delete from mbase2.referenced_tables where id = (select id from mbase2.referenced_tables_vw rtv where key='licences')");

        self::catchQuery("delete from mbase2.code_list_options where id = (select id from mbase2.code_list_options_vw clov where key='licences' and list_key='referenced_tables')");
    }

    static function patch_25() {
        self::addCodeListOption('code_lists','mbase2_ui');

        self::addCodeListOption('mbase2_ui','Data source properties');
        self::addCodeListOption('mbase2_ui','Variable mappings');
        self::addCodeListOption('mbase2_ui','Select a page in the uploaded xlsx file');
        self::addCodeListOption('mbase2_ui','INSERT - append all the rows from the selected page to the database if possible');
        self::addCodeListOption('mbase2_ui','UPDATE - ignore the rows with unmatched update ID and update only the defined columns');
        self::addCodeListOption('mbase2_ui','How would you like to import the data?');
        self::addCodeListOption('mbase2_ui','Select a variable holding an ID for update');
        self::addCodeListOption('mbase2_ui','What format are the dates in?');
        self::addCodeListOption('mbase2_ui','Apply import definition from previous import');
        self::addCodeListOption('mbase2_ui','Import');
        self::addCodeListOption('mbase2_ui','column');
        self::addCodeListOption('mbase2_ui','fixed');
        self::addCodeListOption('mbase2_ui','latitude', ['en'=>'Latitude (north)']);
        self::addCodeListOption('mbase2_ui','longitude', ['en'=>'Longitude (east)']);
        self::addCodeListOption('mbase2_ui','CRS');
    }

    static function patch_24() {
        foreach (['gensam', 'howling'] as $moduleKey) {
            self::importVariables([[
                'key_name_id' => '_location_data.lat',
                'key_data_type_id'=>'real',
                'translations' => ['en' => 'Lattitude', 'sl'=>'Zemljepisna širina']
            ],
            [
                'key_name_id' => '_location_data.lon',
                'key_data_type_id'=>'real',
                'translations' => ['en' => 'Longitude', 'sl'=>'Zemljepisna dolžina']
            ],
            [
                'key_name_id' => '_location_data.spatial_request_result.oe_ime',
                'key_data_type_id'=>'text',
                'translations' => ['en'=>'SFS regional unit', 'sl'=>'Območna enota ZGS']
            ],
            [
                'key_name_id' => '_location_data.spatial_request_result.lov_ime',
                'key_data_type_id'=>'text',
                'translations' => ['en'=>'Hunting ground', 'sl'=>'Lovišče']
            ],
            [
                'key_name_id' => '_location_data.spatial_request_result.luo_ime',
                'key_data_type_id'=>'text',
                'translations' => ['en'=>'Hunting management area', 'sl'=>'LUO']
            ],
            [
                'key_name_id' => '_location_data.spatial_request_result.ob_uime',
                'key_data_type_id'=>'text',
                'translations' => ['en'=>'Minicipality', 'sl'=>'Občina']
            ],
            
            ], $moduleKey);
        }
    }

    static function patch_23() {
        self::catchQuery("DROP VIEW mbase2.module_variables_vw");
        self::catchQuery("ALTER table mbase2.module_variables ADD column help jsonb");
        self::module_variables_vw();
    }

    static function patch_22() {
        self::catchQuery("update mbase2.referenced_tables set additional = 'name as translations' where id in (
            select id from mbase2.referenced_tables_vw where key in (
            'fur_pattern_in_lynx_list',
            'sex_list',
            'species_list',
            'sample_type_list',
            'way_of_withdrawal_list',
            'conflict_animal_removal_list',
            'licence_list',
            'biometry_loss_reason_list',
            'place_type_list',
            'tooth_type_list',
            'teats_wear_list',
            'incisors_wear_list',
            'color_list',
            'collar_list'))");

        self::catchQuery("update mbase2.module_variables set importable=false where id in (select id from mbase2.module_variables_vw where data_type='location_geometry')");

        self::catchQuery("update mbase2.module_variables set required = false where id in (select id from mbase2.module_variables_vw where module_name='dmg' and importable=true)");
    }

    static function patch_21() {
       
    }

    static function patch_20() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/_spatial_units.sql'));
    }

    static function patch_19() {
        self::catchQuery("CREATE OR REPLACE VIEW mbase2.module_variables_vw
            AS SELECT clo1.key AS module_name,
                mv.key_name_id AS variable_name,
                clo3.key AS data_type,
                clo4.key AS reference,
                mv.id,
                mv.data_type_id,
                mv.ref,
                mv.module_id,
                mv.required,
                mv.unique_constraint,
                mv.pattern_id,
                mv.weight,
                mv.visible,
                mv.read_only,
                mv.form_data,
                mv.skip_from_table_view,
                mv.default_value,
                mv._data,
                mv._desc,
                mv.filterable,
                mv.importable,
                mv.weight_in_table,
                mv.weight_in_popup,
                mv.aggregate_operation,
                mv.dbcolumn,
                mv.visible_in_table,
                mv.section_in_form,
                mv.visible_in_popup,
                mv.visible_anonymously,
                mv.weight_in_import,
                mv.weight_in_filter,
                mv.translations,
                mv.key_name_id,
                mv.visible_in_cv_detail,
                mv.visible_in_cv_grid,
                clo3.key AS key_data_type_id
            FROM mbase2.module_variables mv
                LEFT JOIN mbase2.code_list_options clo1 ON mv.module_id = clo1.id
                LEFT JOIN mbase2.code_list_options clo3 ON mv.data_type_id = clo3.id
                LEFT JOIN mbase2.code_list_options clo4 ON mv.ref = clo4.id;");
    }

    static function patch_18() {
        self::catchQuery("update mbase2.import_batches a set key_module_id = sq.key
        from 
        (select jsonb_object_keys(data->'dataDefinitions') key, * from mbase2.import_batches) sq
        where a.key_module_id is null and sq.id=a.id");
    }

    static function patch_17() {
        self::catchQuery("ALTER TABLE mbase2.module_variables ALTER COLUMN required SET DEFAULT false;");
        self::catchQuery("ALTER TABLE mbase2.module_variables ALTER COLUMN filterable SET DEFAULT false;");
    }

    static function patch_16() {
        self::catchQuery("update mbase2.module_variables set translations=:trans where id in (
            select id from mbase2.module_variables_vw mvv where reference='licence_list')",[':trans'=>'{"en":"Licence","sl":"Licenca"}']);
    }

    static function patch_15() {
        self::catchQuery("update mbase2.module_variables set visible_in_cv_grid=true,visible_in_cv_detail=true where id in (
            select id from mbase2.module_variables_vw mvv where reference='licence_list')");
    }

    static function patch_14() {
        self::catchQuery("ALTER TABLE mbase2.module_variables ALTER COLUMN visible SET DEFAULT false;");
        self::catchQuery("ALTER TABLE mbase2.module_variables ALTER COLUMN visible_in_popup SET DEFAULT false;");
        self::catchQuery("ALTER TABLE mbase2.module_variables ALTER COLUMN visible_in_table SET DEFAULT false;");
    }

    static function patch_13() {
        self::catchQuery("update mbase2.module_variables set required = false where id in (select id from mbase2.module_variables_vw mvv where data_type = 'location_geometry')");
    }

    static function patch_12() {
        self::catchQuery("update mbase2.referenced_tables set select_from = :select_from
        where id =
        (select id from mbase2.referenced_tables_vw rt where key = 'laravel.animal')",[':select_from' => "SELECT id, coalesce(name, 'ANIMAL_ID_' || id::varchar) name from laravel.animal"]);
    }

    static function patch_11() {
        self::catchQuery("update mbase2.code_list_options set key='Not known' where key='unknown' and list_id = (select id from mbase2.code_lists where label_key='sex_options')");

        foreach (['individuals' => ['fkey'=>'individuals_sex_fkey','cname' => 'sex'], 'ct'=>['fkey'=>'spm_sex_fkey', 'cname'=>'sex']] as $tname => list('fkey'=>$fkey, 'cname'=>$cname)) {
            self::catchQuery("alter table mb2data.$tname drop constraint $fkey");

            self::catchQuery("update mb2data.$tname a set sex=b.nid
            from (select a.id as old_id,b.id as nid from mbase2.code_list_options_vw a, laravel.sex_list b where lower(a.key)=lower(b.slug) and a.list_id=1) b
            where a.$cname = b.old_id");

            self::catchQuery("alter table mb2data.$tname add constraint $fkey FOREIGN KEY($cname)
                    REFERENCES laravel.sex_list(id)");

        }

        self::catchQuery("update mb2data.gensam a set sample_properties['sex_recorded'] = to_jsonb(b.nid)
            from (select a.id as old_id,b.id as nid from mbase2.code_list_options_vw a, laravel.sex_list b where lower(a.key)=lower(b.slug) and a.list_id=1) b
            where a.sample_properties->>'sex_recorded' = b.old_id::text");

        self::deleteCodeList('sex_options');
        
        GeneralMbase2Helper::updateCodeListOptionsForeignKeysMaterializedView();
    }

    static function patch_10() {
        self::updateVariables([[
            'key_name_id' => 'sex',
            'key_data_type_id' => 'table_reference',
            'ref' => 'sex_list'
        ]], 'individuals');
    }

    static function patch_9() {
        self::catchQuery("DROP TABLE IF EXISTS mbase2.import_batches_module_rows");
    }

    static function patch_8() {
        self::catchQuery("delete from mbase2.module_variables where id in (select id from mbase2.module_variables_vw mvv where variable_name = '_batch_id')");
    }

    static function patch_7() {
        self::catchQuery("CREATE TABLE mbase2.import_batches_module_rows (
            id bigserial primary key,
            batch_id integer references mbase2.import_batches(id),
            tname varchar,
            row_id integer
        )");
    }

    static function patch_6() {
        self::catchQuery("drop view if exists mbase2.module_variables_vw");
        \DB::statement("ALTER TABLE mbase2.module_variables DROP column IF exists filterable_in_cv_detail");
        \DB::statement("ALTER TABLE mbase2.module_variables DROP column IF exists filterable_in_cv_grid");
        self::module_variables_vw();
    }

    static function patch_5() {
        \DB::update("UPDATE mbase2.module_variables SET visible_in_popup = true WHERE visible_in_cv_grid = true");
    }

    /**
     * Some variable translations were double json encoded due to bug in Mbase2SchemaPatches::importVariables which was resolved
     */
    static function patch_4() {
        $rows = \DB::select("SELECT id, translations from mbase2.module_variables");
        foreach ($rows as $row) {
            if (empty($row->translations)) continue;

            $t = json_decode($row->translations, true);

            if (!is_array($t)) {
                if (is_string($t)) {
                    $t = json_decode($t, true);
                    if (is_array($t)) {
                        \DB::update("UPDATE mbase2.module_variables SET translations=:t where id=:id",[':id' => $row->id, ':t' => json_encode($t)]);
                    }
                }
            }
        }
    }

    static function patch_3() {
        self::addReferenceTable('nuts','gid','mbase2_ge','slug');
    }

    static function patch_2() {
        \DB::unprepared(file_get_contents(__DIR__.'/sql/exp_nuts.sql'));
    }

    static function patch_1() {
        foreach([
            'Wolf' => 'Canis lupus',
            'Brown bear' => 'Ursus arctos',
            'Eurasian lynx' => 'Lynx lynx'
        ] as $en=>$slug)
        \DB::update("UPDATE laravel.species_list SET slug=:slug where slug=:en",[':slug'=>$slug,':en'=>$en]);
    }

    static function patch_0() {
        \DB::update("UPDATE mbase2.modules SET patch_id=-1");
        GeneralMbase2Helper::updateCodeListOptionsForeignKeysMaterializedView();

        \DB::statement("ALTER TABLE mbase2.module_variables ADD column IF NOT exists visible_in_cv_detail boolean default false");
        \DB::statement("ALTER TABLE mbase2.module_variables ADD column IF NOT exists visible_in_cv_grid boolean default false");

        self::module_variables_vw();

        \DB::update("update mbase2.module_variables set visible_in_cv_detail=true
        where id in (select id from mbase2.module_variables_vw where module_name='ct' and visible = true and visible_in_table = true)");        
    }

    static function create_spatial_units_by_date_mv() {
        self::catchQuery("drop view if exists mbase2_ge.spatial_units_by_date");
        self::catchQuery("drop MATERIALIZED view if exists mbase2_ge.spatial_units_by_date");
        self::catchQuery("
        create MATERIALIZED view mbase2_ge.spatial_units_by_date as
        select filter_types.slug, filter_elements.name name,
        filter_type_versions.valid_from, filter_type_versions.valid_to, su.geom
        from 
                    mbase2_ge.spatial_units su, 
                    laravel.spatial_units_spatial_unit_filter_elements su_filter_elements,
                    laravel.spatial_unit_filter_elements filter_elements,
                    laravel.spatial_unit_filter_type_versions filter_type_versions,
                    laravel.spatial_unit_filter_types filter_types
                    where
                    su.gid = su_filter_elements.spatial_unit_gid
                    and su_filter_elements.spatial_unit_filter_element_id = filter_elements.id
                    and filter_elements.spatial_unit_filter_type_version_id = filter_type_versions.id
                    and filter_type_versions.spatial_unit_filter_type_id = filter_types.id");
        
        self::catchQuery("CREATE INDEX idx_valid_from_to_spatial_units_by_date ON mbase2_ge.spatial_units_by_date (valid_from, valid_to);");
        self::catchQuery("CREATE INDEX idx_valid_to ON mbase2_ge.spatial_units_by_date (valid_to);");
        self::catchQuery("CREATE INDEX idx_geom_spatial_units_by_date ON mbase2_ge.spatial_units_by_date USING GIST (geom);");
    }

    static function module_variables_vw() {
        self::catchQuery("drop view if exists mbase2.module_variables_vw");

        self::catchQuery("CREATE OR REPLACE VIEW mbase2.module_variables_vw
            AS SELECT clo1.key AS module_name,
            mv.key_name_id AS variable_name,
            clo3.key AS data_type,
            clo3.key AS key_data_type_id,
            clo4.key AS reference,
            mv.id,
            mv.data_type_id,
            mv.ref,
            mv.module_id,
            mv.required,
            mv.unique_constraint,
            mv.pattern_id,
            mv.weight,
            mv.visible,
            mv.read_only,
            mv.form_data,
            mv.skip_from_table_view,
            mv.default_value,
            mv._data,
            mv._desc,
            mv.filterable,
            mv.importable,
            mv.weight_in_table,
            mv.weight_in_popup,
            mv.aggregate_operation,
            mv.dbcolumn,
            mv.visible_in_table,
            mv.section_in_form,
            mv.visible_in_popup,
            mv.visible_anonymously,
            mv.weight_in_import,
            mv.weight_in_filter,
            mv.translations,
            mv.key_name_id,
            mv.visible_in_cv_detail,
            mv.visible_in_cv_grid,
            mv.help,
            mv.weight_in_export,
            mv.exportable,
            mv.weight_in_popup_cv
            FROM mbase2.module_variables mv
            LEFT JOIN mbase2.code_list_options clo1 ON mv.module_id = clo1.id
            LEFT JOIN mbase2.code_list_options clo3 ON mv.data_type_id = clo3.id
            LEFT JOIN mbase2.code_list_options clo4 ON mv.ref = clo4.id;
        ");
    }

}
