<?php namespace patches;

use Exception;
use Mbase2Utils;
use Mbase2SchemaPatches;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');

class sop extends Mbase2SchemaPatches  {
    static function patch_16() {
        $updated = \DB::update("update mb2data.sop 
            set _sign_of_presence = (select id from mbase2.code_list_options_vw clov where key='sop.prey')
            where _sign_of_presence = (select id from mbase2.code_list_options_vw clov where key='sop.kill')");

        $deleted = \DB::delete("delete from mbase2.code_list_options where id in (select id from mbase2.code_list_options_vw clov where key='sop.kill')");

        print_r([$updated, $deleted]);
    }

    static function patch_15() {
        self::updateVariables([[
            'key_name_id' => '_uname',
            'exportable' => true
        ]], 'sop');

        self::importVariables([[
            'key_name_id' => 'id',
            'key_data_type_id' => 'text',
            'exportable'=>true,
            'weight_in_export'=>0.1
        ]], 'sop');

        self::catchQuery("update mbase2.module_variables set weight_in_export=100 where id in (select id from mbase2.module_variables_vw where module_name='sop' and weight_in_export is null)");

        self::importVariables([[
            'key_name_id' => '_location_data.lat',
            'key_data_type_id'=>'real',
            'weight_in_export'=>1,
            'translations' => ['en' => 'Lattitude', 'sl'=>'Zemljepisna širina']
        ],
        [
            'key_name_id' => '_location_data.lon',
            'key_data_type_id'=>'real',
            'weight_in_export'=>2,
            'translations' => ['en' => 'Longitude', 'sl'=>'Zemljepisna dolžina']
        ],
        [
            'key_name_id' => '_location_data.spatial_request_result.lov_ime',
            'key_data_type_id'=>'text',
            'weight_in_export'=>3,
            'translations' => ['en'=>'Hunting ground', 'sl'=>'Lovišče']
        ],
        [
            'key_name_id' => '_location_data.spatial_request_result.luo_ime',
            'key_data_type_id'=>'text',
            'weight_in_export'=>4,
            'translations' => ['en'=>'Hunting management area', 'sl'=>'LUO']
        ],
        [
            'key_name_id' => '_location_data.spatial_request_result.ob_uime',
            'key_data_type_id'=>'text',
            'weight_in_export'=>5,
            'translations' => ['en'=>'Municipality', 'sl'=>'Občina']
        ]
        
        ], 'sop', 'modules', ['exportable' => true]);

    }

    static function patch_14() {
        self::updateVariables([[
            'key_name_id' => 'event_date',
            'translations' => ['en' => 'Event date', 'sl'=>'Datum dogodka']
        ],
        [
            'key_name_id' => 'event_time',
            'translations' => ['en' => 'Event time', 'sl'=>'Čas dogodka']
        ]], 'sop');
    }

    static function patch_13() {
        self::catchQuery("UPDATE mb2data.sop
        SET _uname = batches.user_id
        FROM mbase2.import_batches as batches
        WHERE sop._batch_id = batches.id and sop._uname is null;");

        self::catchQuery("update mbase2.module_variables set importable=true where id = (select id from mbase2.module_variables_vw mvv where module_name = 'sop' and variable_name = '_uname')");
    }

    static function patch_12() {
        self::importVariables([[
            'key_name_id' => '_batch_id',
            'key_data_type_id'=>'table_reference',
            'ref' => 'import_batches',
            'filterable' => true,
            'translations' => ['en'=>'Batch import', 'sl' => 'Paketni uvoz']
        ]],'sop');

        self::sop_vw();
    }

    static function patch_11() {
        //$fileHash = '7f532c840a7b7eeb511d6ec7205816086583ea3e';
        $publicPath = \Storage::path('');
        $storagePath = [
            'private'=>$publicPath.'mbase2/.private',
            'public'=>$publicPath.'mbase2/'
        ];

        $subfolder = 'sop';

        $res = \DB::select("SELECT * from mb2data.sop");

        foreach($res as $row) {
            $photos = json_decode($row->_photos);
            if ($photos) {
                foreach ($photos as $fileHash) {
                    $filePath = 'm'.substr($fileHash, 0, 2);
            
                    $path = $storagePath['private'].'/'.$subfolder.'/'.$filePath.'/'.$fileHash;
    
                    if (file_exists($path)) {
                        echo "OK----------------<br>";
                    }
                    else {
                        //$updated = \DB::update("UPDATE mbase2.uploads SET subfolder = 'sop' WHERE file_hash=:file_hash",[':file_hash' => $fileHash]);
                        
                        $existing = $storagePath['private'].'/ct/'.$filePath.'/'.$fileHash;

                        echo "MISSING.............$path | $existing$<br>";

                        if (file_exists($existing)) {
                            $folder = $storagePath['private'].'/sop/'.$filePath;
                            if (!file_exists($folder)) {
                                mkdir($folder,0755,true);
                            }
                            
                            copy($existing, $folder.'/'.$fileHash);
                            copy($existing.'_thumbnail', $folder.'/'.$fileHash.'_thumbnail');
                            echo "COPY<br>";
                        }
                    }
                }
            }
        }

        

        //echo $path;
    }

    static function patch_10() {
        self::importVariables([
            [
                'key_name_id'=>'nuts', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'nuts',
                'translations' => json_encode(["sl" => "Regije NUTS","en" => "Nomenclature of territorial units for statistics (NUTS)"]),
                'filterable' => true
            ]
        ], 'sop', 'modules');
    }

    static function patch_9() {
        self::updateVariables([
        [
            'key_name_id' => '_location',
            'filterable' => false
        ],
        [
            'key_name_id' => '_location_data',
            'filterable' => false
        ]
        ],'sop');
    }

    static function patch_8() {
        self::updateVariables([[
            'key_name_id' => 'species_list_id',
            'visible'=>false,
            'importable'=>false,
            'visible_in_table'=>false,
            'visible_in_cv_detail'=>true,
            'visible_in_cv_grid'=>true
        ],
        [
            'key_name_id' => '_species_name',
            'visible'=>true,
            'importable'=>true,
            'visible_in_table'=>true,
            'visible_in_cv_detail'=>false,
            'visible_in_cv_grid'=>false
        ]   
        ], 'sop');
    }

    static function patch_7() {
        self::updateVariables([
            [
                'key_name_id' => 'species_list_id',
                'importable' => false,
                'visible' => false,
                'required' => false,
                'visible_in_popup' => false
            ],
            [
                'key_name_id' => '_uname',
                'importable' => false,
                'visible' => false,
                'required' => false,
                'visible_in_popup' => false
            ],
            [
                'key_name_id'=>'_location_data',
                'visible_in_cv_detail'=>false,
                'visible_in_cv_grid'=>false
            ]
            ],'sop');
    }

    static function patch_6() {
        self::catchQuery("update mbase2.module_variables set required = false where id=(
            select id from mbase2.module_variables_vw where module_name ='sop' and variable_name ='_uname')");
    }

    static function patch_5() {
        self::catchQuery("update mbase2.module_variables set importable = true where id in (
            select id from mbase2.module_variables_vw mvv where module_name = 'sop' and variable_name not in ('_genetics'))");
    }

    static function patch_4() {
        \DB::delete("delete from mbase2.module_variables where id in (select id from mbase2.module_variables_vw where module_name in ('sop_sighting_table','sop_footprints_table'))");
        \DB::delete("delete from mbase2.referenced_tables where id in (select id from mbase2.code_list_options_vw where key in ('sop_sighting_table','sop_footprints_table'))");
        \DB::delete("delete from mbase2.code_list_options where id in (select id from mbase2.code_list_options_vw where key in ('sop_sighting_table','sop_footprints_table'))");
    }

    static function patch_3() {
        $tname = 'sop';
        self::catchQuery("DROP VIEW mb2data.{$tname}_vw");
        self::catchQuery("ALTER TABLE mb2data.$tname ADD column _batch_id integer references mbase2.import_batches(id)");
        self::sop_vw(false);
        \DB::unprepared(file_get_contents(dirname(__FILE__) . '/sop_batches_update.sql'));
    }

    static function patch_2() {
            
    }

    static function patch_1() {
        \DB::update("update mbase2.module_variables set visible_in_cv_detail=true
        where id in (select id from mbase2.module_variables_vw where module_name='sop' and visible = true and visible_in_table = true)");
    }
    
    static function patch_0() {

        self::sop_vw();

        self::importVariables([
            [
                'key_name_id'=>'species_list_id', 
                'key_data_type_id'=>'table_reference',
                'ref' => 'species_list',
                'translations' => json_encode(["sl" => "Živalska vrsta","en" => "Species"])
            ]
        ], 'sop', 'modules', ['required' => true, 'visible_in_cv_detail'=>true, 'visible_in_cv_grid'=>true, 'filterable' => false]);

        self::updateVariables(([[
            'key_name_id' => 'event_date',
            'visible_in_cv_detail'=>true, 
            'visible_in_cv_grid'=>true
        ]]),'sop');

    }

    static function sop_vw() {
        self::catchQuery("DROP view if EXISTS mb2data.sop_vw;");
        self::catchQuery("
        CREATE OR REPLACE VIEW mb2data.sop_vw
        AS SELECT sop.id,
            lsl.id as species_list_id,
            sop._species_name,
            sop._licence_name,
            sop._uname,
            sop.event_date,
            sop.event_time,
            sop._data_quality,
            sop._animals_number,
            sop._juvenile_number,
            sop.individual_name,
            sop._genetics,
            sop._gps_colar_id,
            sop._sign_of_presence,
            sop._marking_object,
            sop._prey_species,
            sop._photos,
            sop._notes,
            sop._location,
            sop._location_data,
            st_asgeojson(sop._location) AS geom,
            sop._batch_id
           FROM mb2data.sop
           left join mbase2.code_list_options clo on clo.id = sop._species_name
           left join laravel.species_list lsl on lsl.slug = clo.key;");
    }
}