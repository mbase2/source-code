<?php namespace patches;

use Exception;
use Mbase2Drupal;
use Mbase2Utils;
use Mbase2SchemaPatches;
use Mbase2dtl\Helpers\GeneralMbase2Helper;
use Mbase2dtl\Mail\CustomMail;

//use Illuminate\Support\Facades\Artisan;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');
/**
 * api/mbase2/schema-patch/run/utils/0
 */
class utils extends Mbase2SchemaPatches  {
    static function patch_24() {
        self::import_spatial_layer_data_import();
        self::import_spatial_layer_remove();
        self::import_spatial_layer_lov();
        self::import_spatial_layer_luo();
        self::import_spatial_layer_ob();
        self::import_spatial_layer_cntry();
        self::import_spatial_layer_oe();
        self::create_spatial_units_by_date_mv();
    }

    static function import_spatial_layer_oe() {
        $spatialTableName = 'mbase2_ge.oe';
        $slug = 'SI-OE';
        $name = 'ZGS OE';
        $validFrom = '1970-01-01';
        $validTo = '2100-01-01';

        $idField = 'ggo';
        $nameField = 'naziv';

        self::importSpatialLayer($spatialTableName, $slug, $name, $validFrom, $validTo, $idField, $nameField, 'gid');
    }

    static function import_spatial_layer_ob() {
        $spatialTableName = 'mbase2_ge.ob';
        $slug = 'SI-OB';
        $name = 'Original setup OB';
        $validFrom = '1970-01-01';
        $validTo = '1970-01-01';

        $idField = 'ob_id';
        $nameField = 'ob_uime';

        $existing['suFilterTypeId'] = 3;
        $existing['suFilterTypeVersionId'] = 3;
        
        \DB::update("UPDATE laravel.spatial_unit_filter_type_versions SET title=:name WHERE id=3", [':name' => $name]);

        self::importSpatialLayer($spatialTableName, $slug, $name, $validFrom, $validTo, $idField, $nameField, 'gid', $existing);
    }

    static function import_spatial_layer_luo() {
        $spatialTableName = 'mbase2_ge.lov_2018_fixed';
        $slug = 'SI-LUO';
        $name = 'Original setup LUO';
        $validFrom = '1970-01-01';
        $validTo = '1970-01-01';

        $idField = 'luo_id';
        $nameField = 'luo_ime';

        $existing['suFilterTypeId'] = 2;
        $existing['suFilterTypeVersionId'] = 2;
        
        \DB::update("UPDATE laravel.spatial_unit_filter_type_versions SET title=:name WHERE id=2", [':name' => $name]);

        self::importSpatialLayer($spatialTableName, $slug, $name, $validFrom, $validTo, $idField, $nameField, 'id', $existing, "group by luo_id,luo_ime");

        \DB::update("UPDATE laravel.bears_biometry_animal_handling
        SET hunting_management_area_id = a.id
        FROM laravel.spatial_unit_filter_elements a
        WHERE hunting_management_area_id IS NULL
        AND hunting_management_area = a.name
        AND a.slug LIKE 'SI-LUO-2-%';");

    }

    static function import_spatial_layer_lov() {
        $spatialTableName = 'mbase2_ge.lov_2018_fixed';
        $slug = 'SI-LOV';
        $name = 'Original setup LOV';
        $validFrom = '1970-01-01';
        $validTo = '1970-01-01';

        $idField = 'lov_id';
        $nameField = 'lovisce';

        $existing['suFilterTypeId'] = 1;
        $existing['suFilterTypeVersionId'] = 1;
        
        \DB::update("UPDATE laravel.spatial_unit_filter_type_versions SET title=:name WHERE id=1", [':name' => $name]);

        self::importSpatialLayer($spatialTableName, $slug, $name, $validFrom, $validTo, $idField, $nameField, 'id', $existing);
    }

    static function import_spatial_layer_cntry() {
        $spatialTableName = 'mbase2_ge.countries';
        $slug = 'CNTRY';
        $name = 'Countries';
        $validFrom = '1970-01-01';
        $validTo = '2100-01-01';

        $idField = 'cntr_id';
        $nameField = 'cntr_id';

        self::importSpatialLayer($spatialTableName, $slug, $name, $validFrom, $validTo, $idField, $nameField);
    }

    static function import_spatial_layer_remove() {

        \DB::statement("ALTER TABLE laravel.spatial_units_spatial_unit_filter_elements DROP CONSTRAINT spatial_units_spatial_unit_filter_elements_spatial_unit_gid_for");

        \DB::update("UPDATE laravel.bears_biometry_animal_handling
        SET hunting_management_area_id = NULL
        FROM laravel.spatial_unit_filter_elements a
        WHERE hunting_management_area_id = a.id
        AND a.slug LIKE 'SI-LUO-2-%'
        AND hunting_management_area = a.name;");
        
        foreach (['SI-OB-%', 'SI-LOV-1-%', 'SI-LUO-2-%'] as $key) {

            $ndeleted = \DB::delete("DELETE from laravel.spatial_unit_groups WHERE spatial_unit_gid in (SELECT gid from mbase2_ge.spatial_units where gid in (
                select spatial_unit_gid  from laravel.spatial_units_spatial_unit_filter_elements where spatial_unit_filter_element_id in (select id from laravel.spatial_unit_filter_elements where slug like '$key')
            ))");
            echo "\nDELETED $key: $ndeleted\n";

            $ndeleted = \DB::delete("DELETE from mbase2_ge.spatial_units where gid in (
                select spatial_unit_gid  from laravel.spatial_units_spatial_unit_filter_elements where spatial_unit_filter_element_id in (select id from laravel.spatial_unit_filter_elements where slug like '$key')
            )");
            echo "\nDELETED $key: $ndeleted\n";

            $ndeleted = \DB::delete("DELETE from laravel.spatial_units_spatial_unit_filter_elements where spatial_unit_filter_element_id in (select id from laravel.spatial_unit_filter_elements where slug like '$key')");
            echo "DELETED $key: $ndeleted\n";

            $ndeleted = \DB::delete("DELETE from laravel.spatial_unit_filter_elements where slug like '$key'");
            echo "DELETED $key: $ndeleted\n";
        }
        
        \DB::statement("ALTER TABLE laravel.spatial_units_spatial_unit_filter_elements ADD CONSTRAINT spatial_units_spatial_unit_filter_elements_spatial_unit_gid_for FOREIGN KEY (spatial_unit_gid) REFERENCES mbase2_ge.spatial_units(gid) ON UPDATE CASCADE ON DELETE RESTRICT");
    }

    static function import_spatial_layer_data_import() {
        self::catchQuery("truncate table mbase2_ge.countries");
        self::catchQuery("truncate table mbase2_ge.lov_2018_fixed");
        \DB::unprepared(file_get_contents(__DIR__.'/data/countries.sql'));
        \DB::unprepared(file_get_contents(dirname(__FILE__) . '/data/lov_2018_fixed.sql'));
    }

    static function patch_16() {
        self::create_spatial_units_by_date_mv();
    }

    static function patch_15() {
        foreach(['dmg_affectees'] as $tname) {
            $data = json_decode(file_get_contents("/mbase2dtl/.json_exports/$tname.json"), true);
            \DB::delete("DELETE from mb2data.$tname");
            
            \DB::table("mb2data.$tname")->insert($data);
        }

        $data = json_decode(file_get_contents("/mbase2dtl/.json_exports/dmg.json"), true);
        \DB::delete("DELETE from mb2data.dmg");
        foreach($data as $row) {
            foreach ($row as $cname => $value) {
                if (str_starts_with($cname, 't__') || str_starts_with($cname, 'key__')) {
                    unset($row[$cname]);
                }
            }
            \DB::table("mb2data.dmg")->insert($row);
        }

        foreach(['dmg_agreements'] as $tname) {
            $data = json_decode(file_get_contents("/mbase2dtl/.json_exports/$tname.json"), true);
            \DB::delete("DELETE from mb2data.$tname");
            
            \DB::table("mb2data.$tname")->insert($data);
        }
    }

    /**
     * Test email functionalitx
     */
    static function patch_14() {
        $data = [
            'subject' => 'Your Custom Subject',
            'name' => 'John Doe',
            'message' => 'This is a custom email sent from Laravel.'
        ];

        $email = $_GET['email'];

        \Mail::to($email)->send(new CustomMail($data));

        return "Email to $email sent successfully!";
    }

    static function patch_13() {
        return;
        // Define the number of queries to run
        $numQueries = 10000;

        // Function to generate random user IDs (in the same range as in your data)
        function generateRandomUserId() {
            return rand(1, 1000000);  // Adjust the range to match your data set
        }

        $values = [];
        for ($i = 0; $i < $numQueries; $i++) {
            $values[] = generateRandomUserId();
        }

        // Measure the time for int[] queries
        $intStart = microtime(true);
        $array1 = $array2 = [];
        for ($i = 0; $i < $numQueries; $i++) {
            // Generate a random user ID
            $randomUserId = $values[$i];

            // Run the int[] query
            $results = \DB::select(\DB::raw("SELECT count(*) c FROM organisations WHERE user_ids_int @> ARRAY[$randomUserId]"));

            $array1[] = $results[0]->c;
        }

        $intEnd = microtime(true);
        $intDuration = $intEnd - $intStart;
        echo "Total time for int[] queries: {$intDuration} seconds\n";

        // Measure the time for jsonb queries
        $jsonbStart = microtime(true);

        for ($i = 0; $i < $numQueries; $i++) {
            // Generate a random user ID
            $randomUserId = $values[$i];

            // Run the jsonb query
            $results = \DB::select(\DB::raw("SELECT count(*) c FROM organisations WHERE user_ids_jsonb @> '$randomUserId'::jsonb")); // WHERE user_ids_jsonb @> '[$randomUserId]'::jsonb

            $array2[] = $results[0]->c;
        }

        $jsonbEnd = microtime(true);
        $jsonbDuration = $jsonbEnd - $jsonbStart;
        echo "Total time for jsonb queries: {$jsonbDuration} seconds\n";

        $array3=[];

        $joinStart = microtime(true);
            for ($i = 0; $i < $numQueries; $i++) {
                // Generate a random user ID
                $randomUserId = $values[$i];

                // Run the query on organisations_join table
                $results = \DB::select(\DB::raw("SELECT count(*) c FROM organisations_join WHERE user_id = $randomUserId"));

                $array3[] = $results[0]->c;
            }

            $joinEnd = microtime(true);
            $joinDuration = $joinEnd - $joinStart;
            echo "Total time for organisations_join queries: {$joinDuration} seconds\n";

        if ($array1 == $array2 && $array1 == $array3) {
            echo "The arrays are equal.\n";
        } else {
            echo "The arrays are not equal.\n";
        }


        // Compare the performance
        if ($intDuration < $jsonbDuration) {
            echo "int[] queries were faster by " . ($jsonbDuration - $intDuration) . " seconds\n";
        } else {
            echo "jsonb queries were faster by " . ($intDuration - $jsonbDuration) . " seconds\n";
        }

        print_r(array_slice($array1, 0, 10));
        print_r(array_slice($array2, 0, 10));
        print_r(array_slice($array3, 0, 10));
    }

    static function patch_12() {
        return;
        /**
         * Test int[] vs jsonb
         */
            \Schema::dropIfExists('organisations');
            \Schema::create('organisations', function ($table) {
                $table->id();
                $table->string('name');
                $table->integer('user_count')->default(0);
                $table->jsonb('user_ids_jsonb')->nullable(); // JSONB array of user IDs
                $table->timestamps();
            });

            \Schema::dropIfExists('organisations_join');
            \Schema::create('organisations_join', function ($table) {
                $table->id();
                $table->integer('user_id');
                $table->integer('user_count')->default(0);
                $table->integer('organisation_id');
                $table->timestamps();
            });

            \DB::statement('ALTER TABLE organisations ADD COLUMN user_ids_int integer[];');

            // Indexes for performance testing
            \DB::statement('CREATE INDEX idx_user_ids_jsonb ON public.organisations USING gin (user_ids_jsonb jsonb_path_ops)');
            \DB::statement('CREATE INDEX idx_user_ids_int ON public.organisations USING gin (user_ids_int gin__int_ops)'); //CREATE EXTENSION IF NOT EXISTS intarray;

            \DB::statement('CREATE INDEX idx_user_id ON organisations_join(user_id)');
            \DB::statement('CREATE INDEX idx_organisation_id ON organisations_join(organisation_id)');

            function generateRandomUserIds()
            {
                $numUsers = rand(0, 25);

                // If zero users, return an empty array
                if ($numUsers === 0) {
                    return [];
                }

                // Generate an array of random user IDs, based on the desired number of users
                $userIds = [];
                for ($i = 0; $i < $numUsers; $i++) {
                    $userIds[] = rand(1, 1000000);  // Generating random user IDs between 1 and 1,000,000
                }

                return $userIds;
            }

            $organisations = [];
            for ($i = 0; $i < 100000; $i++) {
                $userIds = generateRandomUserIds();
                $name = 'Organisation ' . $i;

                $organisations[] = [
                    'name' => $name,
                    'user_count' => count($userIds),
                    'user_ids_jsonb' => json_encode($userIds),
                    'user_ids_int' => '{' . implode(',', $userIds) . '}', // PostgreSQL int[] format
                ];
            }

            // Insert the generated data in chunks to avoid memory issues
            $chunks = array_chunk($organisations, 1000);
            foreach ($chunks as $chunk) {
                \DB::table('organisations')->insert($chunk);
            }

            $organisationsJoin = [];

            foreach ($organisations as $oid => $organisation) {
                $userIds = json_decode($organisation['user_ids_jsonb'], true); // Decode the JSON array of user IDs

                foreach ($userIds as $userId) {
                    $organisationsJoin[] = [
                        'user_id' => $userId,
                        'user_count' => count($userIds), // Can be kept the same for each user
                        'organisation_id' => $oid, // Assuming `id` is known or auto-incremented in DB
                    ];
                }
            }

            // Insert into 'organisations_join' in chunks to avoid memory issues
            $chunks = array_chunk($organisationsJoin, 1000);
            foreach ($chunks as $chunk) {
                \DB::table('organisations_join')->insert($chunk);
            }

    }

    static function patch_11() {
        if (hash('sha256', $_GET['key']) === '09a7f9d133e7cef03a1a0ecaa53eead41c791d303a80379bc527d4180da788f8') {
            $rows = \DB::select("select distinct _claim_id, _dmg_start_date from 
            (SELECT _claim_id,_dmg_start_date, obj->>'parentGroupKey' AS parentGroupKey
            FROM mb2data.dmg,
            LATERAL jsonb_array_elements(_dmg_objects) AS obj
            WHERE obj ?? 'parentGroupKey' and _dmg_start_date > '2024-04-30') a where parentgroupkey = 'Rastlina na kmetijski površini / Rastlinski pridelek'
            ");

            foreach($rows as $row) {
                echo $row->_claim_id. " ".$row->_dmg_start_date."\n";
            }
            
        }
    }

    static function patch_10() {
        if ($_SERVER['HTTP_HOST'] == 'test.mbase.org' && $_SERVER['APP_URL'] == 'https://test.mbase.org' && hash('sha256',$_GET['key']) === 'e3c2db6487f3b5f93cec4bb56a725ac7ebcc66f59f6b6bd5576a7459fa8b6e09'){

            echo "mb2data:\n";
            $rows = \DB::select("select * from mb2data.mortbiom");
            print_r($rows);

            echo "LARAVEL:\n";
            $rows = \DB::select("select * from laravel.bears_biometry_animal_handling");
            print_r($rows);

            self::catchQuery("TRUNCATE mb2data.cnt_estimations CASCADE");
            self::catchQuery("TRUNCATE mb2data.cnt_observation_reports CASCADE");
            self::catchQuery("TRUNCATE mb2data.ct CASCADE");
            self::catchQuery("TRUNCATE mb2data.dmg CASCADE");
            self::catchQuery("TRUNCATE mb2data.dmg_affectees CASCADE");
            self::catchQuery("TRUNCATE mb2data.dmg_deputies CASCADE");
            self::catchQuery("TRUNCATE mb2data.dmg_others CASCADE");
            self::catchQuery("TRUNCATE mb2data.gensam CASCADE");
            self::catchQuery("TRUNCATE mb2data.gensam_organisations CASCADE");
            self::catchQuery("TRUNCATE mb2data.gensam_populations CASCADE");
            self::catchQuery("TRUNCATE mb2data.gensam_ref_animals CASCADE");
            self::catchQuery("TRUNCATE mb2data.gensam_samplers CASCADE");
            self::catchQuery("TRUNCATE mb2data.howling CASCADE");
            self::catchQuery("TRUNCATE mb2data.individuals CASCADE");
            self::catchQuery("TRUNCATE mb2data.interventions CASCADE");
            self::catchQuery("TRUNCATE mb2data.mortbiom CASCADE");
            self::catchQuery("TRUNCATE mb2data.sop CASCADE");
            self::catchQuery("TRUNCATE mb2data.tlm CASCADE");
            self::catchQuery("TRUNCATE mb2data.tlm_animals CASCADE");
            self::catchQuery("TRUNCATE mb2data.tlm_deployments CASCADE");
            self::catchQuery("TRUNCATE mb2data.tlm_keys CASCADE");
            self::catchQuery("TRUNCATE mb2data.tlm_tracks CASCADE");
        }
    }

    static function patch_9() {
        $tname = $_GET['tname'];

        if (!in_array($tname,['laravel.spatial_unit_filter_elements', 'laravel.spatial_unit_filter_type_versions', 'mbase2_ge.spatial_units'])) {
            return;
        }

        if ($tname === 'mbase2_ge.spatial_units') {
            $rows = [
                'count' => \DB::select("SELECT count(*) from mbase2_ge.spatial_units"),
                'max(gid)' => \DB::select("SELECT max(gid) from mbase2_ge.spatial_units")
            ];
        }
        else {
            $rows = \DB::select("select * from $tname");
        }
        
        header('Content-Type: application/json');
        return $rows;
    }

    static function patch_8() {
        /*
        $data = json_decode(file_get_contents('/var/www/html/file_properties.json'));
        foreach($data as $row) {
            \DB::table('mbase2.file_properties')->insert((array)$row);
        }
        $data = json_decode(file_get_contents('/var/www/html/uploads-2024-04-16.json'));
        foreach($data as $row) {
            \DB::table('mbase2.uploads')->insert((array)$row);
        }

        self::catchQuery("SELECT setval('uploads_id_seq', (SELECT MAX(id) FROM mbase2.uploads)+1)");
        */

        /*
        $data = json_decode(file_get_contents('/var/www/html/dmg.json'));
        foreach($data as $row) {
            unset($row->t__culprit);
            unset($row->key__culprit);
            \DB::table('mb2data.dmg')->insert((array)$row);
        }
        */

        /*
        $data = json_decode(file_get_contents('/var/www/html/dmg_agreements.json'));
        foreach($data as $row) {
            \DB::table('mb2data.dmg_agreements')->insert((array)$row);
        }
        */
    }

    static function patch_7() {
        $rows = \DB::select("select * from laravel.spatial_unit_filter_elements");
        header('Content-Type: application/json');
        return $rows;
    }

    static function patch_6() {
        $rows = \DB::select("select * from mbase2_ge.spatial_units_by_date");
        header('Content-Type: application/json');
        return $rows;
    }

    static function patch_5() {
        $rows = \DB::select("select * from mbase2.module_variables_vw mvv where module_name='gensam' and key_data_type_id like '%_reference%' and ref is null");
        header('Content-Type: application/json');
        return $rows;
    }

    static function patch_4() {
        $schema = $_GET['schema'];
        if (!in_array($schema, ['mbase2','mb2data', 'mbase2_ge'])) {
            return;
        }

        $rows = \DB::select('SELECT table_name, table_type FROM information_schema.tables 
        WHERE table_schema = :schema',[':schema' => $schema]);
        
        header('Content-Type: application/json');
        return $rows;
    }

    static function patch_3() {
        $rows = \DB::select("SELECT * from mbase2.uploads");
        print_r($rows);
    }

    static function patch_2() {
        putenv("COMPOSER_HOME=/usr/bin");
        chdir('/var/www/html');
        echo get_current_user();
        $a = shell_exec('composer update mbase2/dtl 2>&1');
        echo $a;
    }

    static function patch_1() {
        return;
        \Artisan::call('migrate', array('--path' => 'app/migrations', '--force' => true));
        \DB::statement("SET search_path TO laravel");
        //$res = Artisan::call('migrate', array('--force' => true));

        //print_r($res);
    }

    static function patch_0() {
        GeneralMbase2Helper::updateCodeListOptionsForeignKeysMaterializedView();
    }

    static function importSpatialLayer($spatialTableName, $slug, $name, $validFrom, $validTo, $idField, $nameField, $geomId = 'id', $existing = [], $groupBy = '') {
        
        self::catchQuery("ALTER TABLE $spatialTableName ADD COLUMN id integer;");

        try {
            \DB::transaction(function () use ($spatialTableName, $slug, $name, $validFrom, $validTo, $idField, $nameField, $geomId, $existing, $groupBy) {
                $gidSerial = uniqid('_gid_serial') . rand(1000, 9999);
                \DB::unprepared("CREATE temporary SEQUENCE $gidSerial;
                SELECT setval('$gidSerial', max(gid)) FROM mbase2_ge.spatial_units;
                update $spatialTableName  set $geomId = nextval('$gidSerial');");
            
                $suFilterTypeId = 
                    isset($existing['suFilterTypeId']) ? $existing['suFilterTypeId'] :
                    \DB::selectOne("insert into laravel.spatial_unit_filter_types (slug) values ('$slug') returning id;")->id;

                \DB::statement("SELECT SETVAL(
                    'laravel.spatial_unit_filter_type_versions_id_seq',
                    (SELECT COALESCE(MAX(id), 0) FROM laravel.spatial_unit_filter_type_versions),
                    true
                );");
            
                $suFilterTypeVersionId = 
                    isset($existing['suFilterTypeVersionId']) ? $existing['suFilterTypeVersionId'] :
                    \DB::selectOne("INSERT INTO laravel.spatial_unit_filter_type_versions
                    (spatial_unit_filter_type_id, title, valid_from, valid_to)
                    VALUES($suFilterTypeId, '$name', '$validFrom', '$validTo') returning id")->id;
            
                $res = \DB::insert("insert into laravel.spatial_unit_filter_elements (slug, name, spatial_unit_filter_type_version_id) 
                select '$slug-$suFilterTypeVersionId-' || $idField, $nameField, $suFilterTypeVersionId from $spatialTableName $groupBy;");

                echo "\nINSERTED: $res spatial_unit_filter_elements\n";

                $res = \DB::insert("insert into mbase2_ge.spatial_units (gid, geom) select $geomId,geom from $spatialTableName;");

                echo "INSERTED: $res spatial_units\n";
                
                \DB::statement("REINDEX TABLE mbase2_ge.spatial_units;");

                $res = \DB::insert("insert into laravel.spatial_units_spatial_unit_filter_elements(spatial_unit_gid, spatial_unit_filter_element_id)
                select slw.$geomId,suf.id from $spatialTableName slw, laravel.spatial_unit_filter_elements suf where spatial_unit_filter_type_version_id=$suFilterTypeVersionId
                and name = slw.$nameField");

                echo "INSERTED: $res spatial_units_spatial_unit_filter_elements\n";
            });
            
            echo "Transaction completed successfully.\n";
        } catch (\Throwable $e) {
            echo "Transaction failed: " . $e->getMessage() . "\n";
            // You can also log the error here if necessary.
        }
    }

    static function create_spatial_units_by_date_mv() {
        self::catchQuery("drop view if exists mbase2_ge.spatial_units_by_date");
        self::catchQuery("drop MATERIALIZED view if exists mbase2_ge.spatial_units_by_date");
        self::catchQuery("
        create MATERIALIZED view mbase2_ge.spatial_units_by_date as
        select filter_types.slug, filter_elements.name name,
        date(filter_type_versions.valid_from) valid_from, date(filter_type_versions.valid_to) valid_to, su.geom
        from 
                    mbase2_ge.spatial_units su, 
                    laravel.spatial_units_spatial_unit_filter_elements su_filter_elements,
                    laravel.spatial_unit_filter_elements filter_elements,
                    laravel.spatial_unit_filter_type_versions filter_type_versions,
                    laravel.spatial_unit_filter_types filter_types
                    where
                    su.gid = su_filter_elements.spatial_unit_gid
                    and su_filter_elements.spatial_unit_filter_element_id = filter_elements.id
                    and filter_elements.spatial_unit_filter_type_version_id = filter_type_versions.id
                    and filter_type_versions.spatial_unit_filter_type_id = filter_types.id");
        
        self::catchQuery("CREATE INDEX idx_valid_from_to_spatial_units_by_date ON mbase2_ge.spatial_units_by_date (valid_from, valid_to);");
        self::catchQuery("CREATE INDEX idx_valid_to ON mbase2_ge.spatial_units_by_date (valid_to);");
        self::catchQuery("CREATE INDEX idx_geom_spatial_units_by_date ON mbase2_ge.spatial_units_by_date USING GIST (geom);");
    }

    static function run($module = null, $patchId = null) {
        parent::run($module, true, $patchId);
        return self::$errors;
    }
}