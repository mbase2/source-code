<?php namespace patches;

use Exception;
use Mbase2Api;
use Mbase2Database;
use Mbase2Utils;
use Mbase2SchemaPatches;

require_once(__DIR__.'/../Mbase2SchemaPatches.php');

class tlm extends Mbase2SchemaPatches  {
    static function patch_48() {

        self::updateVariables([
            [
                'key_name_id' => 'collar_id',
                'translations' => null
            ],
            [
                'key_name_id' => 'id_mas',
                'translations' => null
            ]
        ], 'tlm');
        
    }

    static function patch_47() {
        self::tlm_vw();

        self::importVariables([[
            'key_name_id' => '_location_data.lat',
            'key_data_type_id'=>'real',
            'translations' => ['en' => 'Lattitude', 'sl'=>'Zemljepisna širina']
        ],
        [
            'key_name_id' => '_location_data.lon',
            'key_data_type_id'=>'real',
            'translations' => ['en' => 'Longitude', 'sl'=>'Zemljepisna dolžina']
        ]], 'tlm','modules',['exportable' => true]);

        self::updateVariables([[
            'key_name_id' => 'animal_id',
            'exportable' => true
        ]], 'tlm');

        foreach (['tlm_tracks_id',
            'animal_id',
            'point_time_stamp',
             '_location_data.lat',
            '_location_data.lon',
            'altitude',
            'temperature',
            'collar_id',
            'id_mas'] as $inx => $key) {
            self::updateVariables([[
                'key_name_id' => $key,
                'weight_in_export' => $inx+1
            ]], 'tlm');
        }
    }

    static function patch_46() {
        self::tlm_vw();
    }

    static function patch_45() {
        self::catchQuery("drop view if exists mb2data.tlm_tracks_vw");
        self::catchQuery("drop view if exists mb2data.tlm_named_animals_vw");

        foreach (['species_list_id','licence_list_id','event_date','animal_id'] as $cname) {
            self::catchQuery("ALTER table mb2data.tlm DROP column if exists $cname");
        }

        self::catchQuery("drop view if exists mb2data.tlm_vw");
                
        self::catchQuery("ALTER TABLE mb2data.tlm_tracks ALTER COLUMN animal_id TYPE varchar (24)");

        self::catchQuery("create view mb2data.tlm_named_animals_vw as
        select id::varchar, name, species_list_id, sex_list_id  from laravel.animal where mbase2.convert_to_numeric(name) is null
        union all 
        select id || '.B' as id, name, species_list_id, sex_list_id  from mb2data.mortbiom where name is not null
        union all
        select id || '.T' as id, animal_name as name, species_list_id, sex_list_id  from mb2data.tlm_animals where animal_name is not null");

        self::catchQuery("update mb2data.tlm_tracks set animal_id = SUBSTRING(animal_id FROM 2) || '.T' where animal_id like '-%'");

        self::tlm_vw();
        self::tlm_tracks_vw();
    }

    static function patch_44() {
        self::copyVariableDefinition('_batch_id', 'howling', 'tlm');
        self::updateVariables([[
            'key_name_id' => '_batch_id',
            'filterable' => true
        ]],'tlm');
    }

    static function patch_43() {
        self::tlm_vw();
    }

    static function patch_42() {
        self::importVariables([[
            'key_name_id' => 'collar_id',
            'key_data_type_id' => 'text'
        ],
        [
            'key_name_id' => 'id_mas',
            'key_data_type_id' => 'text'
        ]
        ], 'tlm', 'modules',[
            'visible_in_cv_detail' => true,
            'visible_in_cv_grid' => false,
            'importable' => true,
            'exportable' => true,
            'visible' => true,
            'visible_in_popup' => true
        ]);

        self::updateModuleDatabaseSchema('tlm','modules');

        self::catchQuery("ALTER TABLE mb2data.tlm
            ADD CONSTRAINT tlm_id_mas_tlm_tracks_id_unique UNIQUE (tlm_tracks_id, id_mas);");
    }

    static function patch_41() {
        self::copyVariableDefinition('species_list_id', 'tlm_animals', 'tlm_tracks');
        self::copyVariableDefinition('sex_list_id', 'tlm_animals', 'tlm_tracks');
        self::updateVariables([[
            'key_name_id' => 'slug',
            'weight_in_table' => '1.1',
            'weight_in_popup' => '1.1',
            'filterable' => true
        ]],'tlm_tracks');

        self::updateVariables([[
            'key_name_id' => 'species_list_id',
            'weight_in_table' => '1.2',
            'weight_in_popup' => '1.2',
            'visible_in_table'=>true,
            'visible_in_popup'=>true,
            'filterable' => true
        ]],'tlm_tracks');


        self::updateVariables([[
            'key_name_id' => 'sex_list_id',
            'weight_in_table' => '1.3',
            'weight_in_popup' => '1.3',
            'visible_in_popup'=>true,
            'visible_in_table'=>true,
            'filterable' => true
        ]],'tlm_tracks');

        self::updateVariables([[
            'key_name_id'=>'date_record_created',
            'filterable'=>false
        ]], 'tlm_tracks');

        self::updateVariables([[
            'key_name_id'=>'date_record_modified',
            'filterable'=>false
        ]], 'tlm_tracks');
    }

    static function patch_40() {
        self::updateVariables([[
            'key_name_id'=>'date_record_created',
            'visible_in_popup' => false
        ],
        [
            'key_name_id'=>'date_record_modified',
            'visible_in_popup' => false
        ],
        [
            'key_name_id' => '_licence_name',
            'translations' => '{"en": "Licence", "sl": "Licenca"}'
        ]
    ], 'tlm_tracks');
    }

    static function patch_39() {
        self::tlm_tracks_vw();
    }

    static function patch_38() {
        self::catchQuery("
            create or replace view mb2data.tlm_named_animals_vw as
            select id, name, species_list_id, sex_list_id  from laravel.animal where mbase2.convert_to_numeric(name) is null
            union all 
            select -id as id, animal_name as name, species_list_id, sex_list_id  from mb2data.tlm_animals
        ");

        self::catchQuery("UPDATE mbase2.code_list_options clo SET key = 'tlm_named_animals_vw' where key='named_animals'");

        self::catchQuery("UPDATE mbase2.referenced_tables SET schema='mb2data', select_from=null WHERE id=(
            SELECT id from mbase2.referenced_tables_vw where key = 'tlm_named_animals_vw')");
    }

    static function patch_37() {
        self::catchQuery('ALTER TABLE mb2data.tlm DROP CONSTRAINT tlm__batch_id_fkey;');
        self::catchQuery('ALTER TABLE mb2data.tlm_tracks DROP CONSTRAINT tlm_tracks__batch_id_fkey;');
        self::catchQuery('ALTER TABLE mb2data.tlm ADD CONSTRAINT tlm__batch_id_fkey FOREIGN KEY (_batch_id) REFERENCES mbase2.import_batches(id)  on delete cascade;');
        self::catchQuery('ALTER TABLE mb2data.tlm_tracks ADD CONSTRAINT tlm_tracks__batch_id_fkey FOREIGN KEY (_batch_id) REFERENCES mbase2.import_batches(id) on delete cascade;');
    }
    static function patch_36() {
        self::updateVariables([[
            'key_name_id'=>'animal_id',
            'key_data_type_id' => 'table_reference',
            'ref' => 'named_animals'
        ]], 'tlm_tracks');
    }

    static function patch_35() {
        $cnt = \DB::delete("DELETE from mbase2.module_variables WHERE id=(select id from mbase2.module_variables_vw mvv where module_name ='tlm_tracks' and variable_name ='track_type_id')");
        echo "DELETED: $cnt\n";
    }

    static function patch_34() {
        $res = \DB::update("UPDATE mbase2.referenced_tables SET select_from=:select_from WHERE id=(select id from mbase2.referenced_tables_vw rtv where key='named_animals')",
        [':select_from' => 'select id, name from laravel.animal where mbase2.convert_to_numeric(name) is null
            union all 
            select -id, animal_name from mb2data.tlm_animals']
        );

        echo "UPDATED: $res\n";
    }

    static function patch_33() {
        self::addCodeListOption('referenced_tables','tlm_animals');

        $weight=1;
        self::importVariables([
            [
                'key_name_id' => 'species_list_id',
                'weight_in_import' => $weight++,
                'importable' => true,
                'key_data_type_id' => 'table_reference',
                'ref' => 'species_list',
                'required' => true,
                'translations' => ['en' => 'Species', 'sl' => 'Živalska vrsta']
            ],
            [
                'key_name_id'=>'sex_list_id', 
                'weight_in_import' => $weight++,
                'importable' => true,
                'key_data_type_id'=>'table_reference',
                'ref' => 'sex_list',
                'translations' => json_encode(["sl" => "Spol", "en" => "Sex"])
            ],
            [
                'key_name_id'=>'animal_name', 
                'weight_in_import' => $weight++,
                'required' => true,
                'importable' => true,
                'key_data_type_id'=>'text',
                'translations' => json_encode(["sl" => "Ime živali", "en" => "Animal name"])
            ],
            [
                'key_name_id'=>'animal_id', 
                'weight_in_import' => $weight++,
                'importable' => true,
                'key_data_type_id'=>'text',
                'translations' => json_encode(["sl" => "ID živali", "en" => "Animal ID"])
            ]],'tlm_animals', 'referenced_tables',['visible_in_table' => true, 'visible' => true]);

            self::addReferenceTable('tlm_animals','id', 'mb2data', 'animal_name');
            self::updateModuleDatabaseSchema('tlm_animals','referenced_tables');

            self::catchQuery("CREATE UNIQUE INDEX tlm_animals_unique_animal_name on mb2data.tlm_animals (LOWER(animal_name));");
    }

    static function patch_32() {
        self::addReferenceTable('named_animals','id','laravel','name', "select id, name from laravel.animal where mbase2.convert_to_numeric(name) is null");
        self::updateVariables([[
            'key_name_id'=>'animal_id',
            'key_data_type_id' => 'table_reference',
            'ref' => 'named_animals'
        ]], 'tlm');
    }

    static function patch_31() {
        self::updateVariables([
            [
                'key_name_id'=>'licence_list_id',
                'translations'=>'{"en": "Licence", "sl": "Licenca"}'
            ],
            [
                'key_name_id'=>'event_date',
                'translations'=>'{"en": "Date", "sl": "Datum"}'
            ]
        ],'tlm');
    }
    static function patch_30() {
        self::updateVariables([[
            'key_name_id' => 'animal_id',
            'visible_in_cv_detail' => true,
            'visible_in_cv_grid' => true
        ]], 'tlm_tracks');

        self::importVariables([[
            'key_name_id' => 'animal_id',
            'key_data_type_id' => 'table_reference',
            'ref' => 'laravel.animal',
            'required' => false,
            'importable'=> false,
            'visible' => false,
            'visible_in_table' => false,
            'translations' => ['en' => 'Animal name', 'sl' => 'Ime živali'],
            'filterable'=>true,
            'visible_in_cv_detail' => true,
            'visible_in_cv_grid' => true
        ]], 'tlm');

        self::tlm_vw();
    }

    static function patch_29() {
        self::importVariables([
            [
                'key_name_id'=>'_location_data',
                'translations' =>'{"en": "Location", "sl": "Lokacija"}',
                'key_data_type_id'=>'location_data_json',
                'filterable'=>false,
                'importable' => true,
                'visible' => false,
                'visible_in_table'=>false,
                'required' => true, 'visible_in_cv_detail'=>false, 'visible_in_cv_grid'=>false
            ]
            ], 'tlm');

        self::catchQuery("ALTER table mb2data.tlm add column _location_data jsonb");
    }

    static function patch_28() {
        $rows = \DB::select("SELECT * from mbase2.import_batches");
        print_r($rows);
    }

    static function patch_27() {
        //DROP TRIGGER [ IF EXISTS ] name ON table_name 
        self::catchQuery("ALTER TABLE mb2data.tlm_tracks ALTER COLUMN slug SET NOT NULL;");
        self::catchQuery("ALTER TABLE mb2data.tlm_tracks ALTER COLUMN animal_id SET NOT NULL;");
    }

    static function patch_26() {
        self::catchQuery("ALTER TABLE mb2data.tlm_tracks DROP CONSTRAINT tlm_tracks_slug_key");
        self::catchQuery("ALTER TABLE mb2data.tlm_tracks ADD CONSTRAINT tlm_tracks_slug_animal_id_key UNIQUE (slug, animal_id)");

        self::catchQuery("update mbase2.referenced_tables set select_from = 'SELECT id,(slug || ''-'' || animal_id) slug from mb2data.tlm_tracks' where id = (select id from mbase2.referenced_tables_vw where key='tlm_tracks')");
    }

    static function patch_25() {
    
        self::addCodeListOption('referenced_tables', 'tlm_keys');
        self::importVariables([
            [
                'key_name_id' => 'collar_id',
                'key_data_type_id' => 'text',
                'required' => true,
                'unique_constraint' => true,
            ],
            [
                'key_name_id' => 'collar_key',
                'key_data_type_id' => 'text',
                'required' => true
            ],
            [
                'key_name_id' => 'note',
                'key_data_type_id' => 'text'
            ]
        ],'tlm_keys', 'referenced_tables',[
            'visible' => true
        ]);
        self::addReferenceTable('tlm_keys','id','mb2data',"label_key", "select id, (collar_id || COALESCE(note, '')) as label_key FROM mb2data.tlm_keys");
        self::updateModuleDatabaseSchema('tlm_keys','referenced_tables');

        self::addCodeListOption('referenced_tables', 'tlm_deployments');
        self::importVariables([
                [
                    'key_name_id' => 'animal_id',
                    'key_data_type_id' => 'table_reference',
                    'ref' => 'laravel.animal',
                    'required' => true
                ],
                [
                    'key_name_id' => 'tlm_keys_id',
                    'key_data_type_id' => 'table_reference',
                    'ref' => 'tlm_keys',
                    'required' => true
                ],
                [
                    'key_name_id' => 'note',
                    'key_data_type_id' => 'text'
                ],
                [
                    'key_name_id' => 'start_timestamp',
                    'key_data_type_id' => 'timestamp',
                    'required' => true
                ],
                [
                    'key_name_id' => 'end_timestamp',
                    'key_data_type_id' => 'timestamp',
                    'required' => false,
                    'read_only' => true
                ]
        ], 'tlm_deployments', 'referenced_tables', ['visible' => true]);

        self::addReferenceTable('tlm_deployments','id','mb2data',"label_key",
                    "select tlmd.id, (animal.aname || '(COLLAR ID: ' || collar_id || ') ' || tlmd.note) label_key from mb2data.tlm_deployments tlmd
                        left join 
                        (SELECT id, coalesce(name, 'ANIMAL_ID_' || id::varchar) aname from laravel.animal) animal
                        on animal.id = tlmd.animal_id
                        left join
                        mb2data.tlm_keys tlmk
                        on tlmk.id = tlmd.tlm_keys_id");
        self::updateModuleDatabaseSchema('tlm_deployments','referenced_tables');
    }

    static function patch_24() {
        self::tlm_tracks_vw();
        self::tlm_vw();
    }

    static function patch_23() {
        self::tlm_tracks_vw();
    }

    static function patch_22() {
        /**
         * used internally for developement
         */
        return;
        \DB::delete("DELETE from mb2data.tlm");
        \DB::delete("DELETE from mb2data.tlm_tracks");

        $animals = \DB::select("select * from laravel.animal");
        $animalsById = array_column($animals, null, 'id');

        $a = file_get_contents(__DIR__.'/../../../docker/.private/tlm_tracks.json');
        $a = json_decode($a);
        
        foreach($a as $line) {
            if (!isset($animalsById[$line->animal_id])) {
                \DB::insert("INSERT into laravel.animal (id) values :id", [':id'=>$line->animal_id]);
            }

            Mbase2Database::insert("mb2data.tlm_tracks",[
                ':id' => $line->id,
                ':slug' => $line->slug,
                ':animal_id' => $line->animal_id,
                ':_licence_name' => $line->_licence_name,
                ':track_type_id' => $line->track_type_id
            ]);
        }

        $a = file_get_contents(__DIR__.'/../../../docker/.private/tlm_vw.json');

        $a = json_decode($a);
        
        foreach($a as $line) {
            $g=json_decode($line->geom);
            $p=[
            ':id' => $line->id,
            ':point_time_stamp' => $line->point_time_stamp,
            ':tlm_tracks_id' => $line->tlm_tracks_id,
            ':altitude' => $line->altitude,
            ':temperature' => $line->temperature,
            ':lon' => $g->coordinates[0],
            ':lat' => $g->coordinates[1]
            ];

            $cnames=[];
            $placeholders=[];

            foreach ($p as $placeholder => $value) {
                if (in_array($placeholder, [':lon', ':lat'])) continue;
                $cnames[] = trim($placeholder,':');
                $placeholders[] = $placeholder;
            }

            $cnames[] = '_location';
            $placeholders[]='ST_SetSRID(ST_MakePoint(:lon,:lat),4326)';

            $cnames = implode(',', $cnames);
            $placeholders = implode(',', $placeholders);

            \DB::insert("INSERT into mb2data.tlm ($cnames) values ($placeholders)", $p);
        }

        \DB::select("SELECT setval('mb2data.tlm_id_seq', COALESCE((SELECT MAX(id)+1 FROM mb2data.tlm), 1), false);");
        \DB::select("SELECT setval('mb2data.tlm_tracks_id_seq', COALESCE((SELECT MAX(id)+1 FROM mb2data.tlm_tracks), 1), false);");
    }

    static function patch_21() {
        self::tlm_tracks_vw();
    }

    static function patch_20() {
        self::catchQuery("update mbase2.modules set properties = :p where id=
        (select id from mbase2.modules_vw mv where module_key = 'tlm')",[':p'=>'{"color": "#033500", "grid_size": 1000}']);
    }

    static function patch_19() {
        self::tlm_vw();
    }

    static function patch_18() {
        self::updateVariables([[
            'key_name_id'=>'species_list_id',
            'translations' =>'{"en": "Species", "sl": "Živalska vrsta"}'
        ]],'tlm');
    }

    static function patch_17() {
        self::updateVariables([[
            'key_name_id' => 'tlm_tracks_id',
            'visible_in_cv_detail' => true,
            'visible_in_cv_grid' => true
        ],
        [
            'key_name_id' => 'point_time_stamp',
            'visible_in_cv_detail' => true,
            'visible_in_cv_grid' => false
        ],
        [
            'key_name_id' => 'altitude',
            'visible_in_cv_detail' => true,
            'visible_in_cv_grid' => false

        ],
        [
            'key_name_id' => 'temperature',
            'visible_in_cv_detail' => true,
            'visible_in_cv_grid' => false

        ]],'tlm');
    }

    static function patch_16() {
        self::tlm_vw();
        
        self::importVariables([
            [
                'key_name_id' => 'species_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'species_list',
                'required' => false,
                'importable' => false,
                'filterable' => true,
                'visible' => false,
                'visible_in_cv_detail' => true,
                'visible_in_cv_grid' => true
            ],
            [
                'key_name_id' => 'licence_list_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'licence_list',
                'required' => false,
                'importable' => false,
                'filterable' => true,
                'visible' => false,
                'visible_in_cv_detail' => true,
                'visible_in_cv_grid' => true
            ],
            [
                'key_name_id' => 'event_date',
                'key_data_type_id' => 'date',
                'required' => false,
                'importable' => false,
                'filterable' => true,
                'visible' => false,
                'visible_in_cv_detail' => true,
                'visible_in_cv_grid' => true
            ]
            ], 'tlm');
    }

    static function patch_15() {

        self::catchQuery("update mbase2.code_list_options set key = 'convex_hull', translations='{\"sl\": \"Konveksna ovojnica\",\"en\": \"Convex hull\"}' where id = (select id from mbase2.code_list_options_vw clov where key = 'convex_hull')");
        self::catchQuery("update mbase2.code_list_options set key = 'concave_hull', translations='{\"sl\": \"Konkavna ovojnica\",\"en\": \"Concave hull\"}' where id = (select id from mbase2.code_list_options_vw clov where key = 'concave_hull')");

        self::catchQuery("update mbase2.code_list_options set key = 'convex_hull', translations='{\"sl\": \"Konveksna ovojnica\",\"en\": \"Convex hull\"}' where id = (select id from mbase2.code_list_options_vw clov where key = 'polyline')");
        self::catchQuery("update mbase2.code_list_options set key = 'concave_hull', translations='{\"sl\": \"Konkavna ovojnica\",\"en\": \"Concave hull\"}' where id = (select id from mbase2.code_list_options_vw clov where key = 'polygon')");

        self::catchQuery("DROP VIEW IF EXISTS mb2data.tlm_tracks_vw");
        self::catchQuery("DROP materialized VIEW IF EXISTS mb2data.tlm_tracks_vw");
        self::catchQuery(Mbase2Utils::SQLfunction_refresh_mat_view());

        self::tlm_tracks_vw();

        self::catchQuery("DROP TRIGGER refresh_tlm_tracks_vw on mb2data.tlm_tracks");

        self::catchQuery("CREATE TRIGGER refresh_tlm_tracks_vw
            AFTER INSERT OR UPDATE OR DELETE
            ON mb2data.tlm_tracks
            FOR EACH STATEMENT
            EXECUTE PROCEDURE mbase2.refresh_mvw('mb2data.tlm_tracks_vw');");
    }

    static function patch_14() {
        self::tlm_vw();
    }

    static function patch_13() {
        self::tlm_tracks_vw();
    }

    static function patch_12() {
        self::updateVariables([[
            'key_name_id'=>'date_record_created',
            'visible' => false
        ]], 'tlm_tracks');

        self::updateVariables([[
            'key_name_id'=>'date_record_modified',
            'visible' => false
        ]], 'tlm_tracks');
    }

    static function patch_11() {
        self::tlm_tracks_vw();
    }

    static function patch_10() {
        self::addCodeListOption('code_lists','tlm_track_type');
        self::addCodeListOption('tlm_track_type','polyline');
        self::addCodeListOption('tlm_track_type','polygon');

        self::importVariables([
            [
                'key_name_id' => 'track_type_id',
                'key_data_type_id' => 'code_list_reference',
                'ref'=>'tlm_track_type',
                'required' => true,
                'importable' => true,
                'filterable' => true,
                'translations' => ['en' => 'Track type','sl'=>'Način prikaza']
            ]], 'tlm_tracks', 'referenced_tables');

        self::catchQuery("ALTER table mb2data.tlm_tracks add column track_type_id integer references mbase2.code_list_options(id)");

        self::catchQuery("delete from mbase2.module_variables where id = (select id from mbase2.module_variables_vw where module_name='tlm_tracks' and variable_name='tlm_track_type_id')");
    }

    static function patch_9() {
        self::catchQuery("ALTER TABLE mb2data.tlm DROP CONSTRAINT tlm_track_id_fkey");
        self::catchQuery("ALTER TABLE mb2data.tlm ADD CONSTRAINT tlm_track_id_fkey FOREIGN KEY (tlm_tracks_id) REFERENCES mb2data.tlm_tracks(id) on delete cascade");

        self::catchQuery("update mbase2.module_variables set importable = true where id = (
            select id from mbase2.module_variables_vw where module_name='tlm_tracks' and variable_name  = '_licence_name')");
    }

    static function patch_8() {
        self::catchQuery(Mbase2Utils::SQLtrigger_sync_date_created('mb2data','tlm_tracks'));
        self::catchQuery(Mbase2Utils::SQLtrigger_sync_date_modified('mb2data','tlm_tracks'));
    }

    static function patch_7() {
        self::updateVariables([[
            'key_name_id'=>'_location',
            'importable' => true
        ]], 'tlm');
    }

    static function patch_6() {
        self::tlm_vw();
    }

    static function patch_5() {
        self::catchQuery("DROP VIEW mb2data.tlm_vw");
        self::catchQuery("ALTER TABLE mb2data.tlm RENAME track_id to tlm_tracks_id");
        self::catchQuery("update mbase2.module_variables mv set key_name_id='tlm_tracks_id' where key_name_id = 'track_id' and module_id = (select id from mbase2.modules_vw where module_key='tlm')");

        self::updateVariables([[
            'key_name_id'=>'tlm_tracks_id',
            'translations'=>json_encode(['sl' => 'Enolična oznaka sledenja','en'=>'Unique track ID'])
        ],
        [
            'key_name_id' => 'tlm_tracks_id',     
            'importable' => true,
            'required' => false   
        ]],'tlm');
    }

    static function patch_4() {
        self::catchQuery("DROP VIEW mb2data.tlm_vw");
        self::catchQuery("ALTER TABLE mb2data.tlm RENAME column height to altitude");
        self::catchQuery("update mbase2.module_variables mv set key_name_id='altitude' where key_name_id = 'height' and module_id = (select id from mbase2.modules_vw where module_key='tlm')");
    }

    static function patch_3() {
        self::updateVariables([[
            'key_name_id'=>'_location',
            'key_data_type_id'=>'location_geometry',
            'visible_in_table' => false,
            'required' => true,
            'importable' => true,
            'filterable' => false
        ]],'tlm');
        self::tlm_vw();
    }

    static function patch_2() {

        self::addCodeListOption('referenced_tables', 'tlm_tracks');
        self::importVariables([
            [
                'key_name_id' => 'slug',
                'key_data_type_id' => 'text',
                'required' => true,
                'importable' => true,
                'filterable' => true,
                'unique_constraint' => true,
                'translations' => ['en' => 'Unique track name', 'sl' => 'Enolična oznaka sledenja']
            ],
            [
                'key_name_id' => 'animal_id',
                'key_data_type_id' => 'table_reference',
                'ref' => 'laravel.animal',
                'required' => true,
                'importable' => true,
                'filterable' => true,
                'translations' => ['en' => 'Animal name', 'sl' => 'Ime živali']
            ],
            [
                'key_name_id' => '_licence_name',
                'key_data_type_id' => 'table_reference',
                'ref'=>'licences',
                'required' => true
            ],
            [
                'key_name_id' => 'date_record_created',
                'key_data_type_id' => 'timestamp'
            ],
            [
                'key_name_id' => 'date_record_modified',
                'key_data_type_id' => 'timestamp'
            ]
        ], 'tlm_tracks', 'referenced_tables');

        self::addReferenceTable('tlm_tracks','id','mb2data','slug');
        self::updateModuleDatabaseSchema('tlm_tracks','referenced_tables');
        
        self::catchQuery(Mbase2Utils::SQLtrigger_sync_date_created('mb2data','tlm_tracks'));
        self::catchQuery(Mbase2Utils::SQLtrigger_sync_date_modified('mb2data','tlm_tracks'));

        self::importVariables([[
            'key_name_id' => 'point_time_stamp',
            'key_data_type_id' => 'timestamp',
            'required' => true,
            'importable' => true,
            'filterable' => true
        ],
        [
            'key_name_id' => 'track_id',
            'key_data_type_id' => 'table_reference',
            'ref' => 'tlm_tracks',
            'required' => true,
            'importable' => false,
            'filterable' => true
        ],
        [
            'key_name_id' => 'altitude',
            'key_data_type_id' => 'real',
            'required' => false,
            'importable' => true,
            'filterable' => true
        ],
        [
            'key_name_id' => 'temperature',
            'key_data_type_id' => 'real',
            'required' => false,
            'importable' => true,
            'filterable' => true
        ],
        [
            'key_name_id'=>'_location',
            'key_data_type_id'=>'location_geometry',
            'visible_in_table' => false,
            'required' => true,
            'importable' => true,
            'filterable' => false
        ]], 'tlm');

        self::updateModuleDatabaseSchema('tlm');
    }

    /**
     * Add module roles
     */
    static function patch_1() {

        //ADD tlm groups
        foreach(Mbase2Utils::moduleRoles('tlm') as $role) {
            self::catchQuery("INSERT INTO laravel.groups(slug,name,group_type_id) values (:slug, :name, (select id from laravel.group_types WHERE slug='MBASE2-MODULE-ROLES'))",
            [
                ':slug'=>$role,
                ':name' => json_encode(['en'=>'Telemetry '.str_replace(['mbase2','-tlm-'],'',$role)])
            ]);
        }

        //ADD tlm module
        self::catchQuery("insert into laravel.mbase2l_modules(slug, name, properties, enabled) values('tlm','tlm','{}', true)");

        $res = \DB::select("SELECT id from laravel.mbase2l_modules WHERE slug='tlm'");
        $moduleId = $res[0]->id;

        /**
         * Connect groups and roles
         */
        foreach(Mbase2Utils::moduleRoles('tlm') as $role) {
            $res = \DB::select("SELECT id from laravel.groups WHERE slug=:slug",[':slug'=>$role]);
            $groupId = $res[0]->id;
            self::catchQuery("INSERT INTO laravel.groups_mbase2l_modules(group_id, mbase2l_module_id) values(:group_id,:module_id)",[':group_id'=>$groupId, ':module_id'=>$moduleId]);
        }

        //connect tlm groups with countries
        self::catchQuery("insert into laravel.groups_group_types_countries (group_id,group_type_country_id)
        SELECT g2.id as group_id, g.id as group_type_country_id FROM laravel.groups g, laravel.group_types gt, laravel.groups g2 WHERE g.group_type_id = gt.id and gt.slug='COUNTRIES' and g2.slug like '%-tlm-%'");
    }

    /**
     * Add module definition
     */
    static function patch_0() {
        self::addCodeListOption('modules', 'tlm', ['en' => 'Telemetry', 'sl' => 'Telemetrija']);
        self::catchQuery("INSERT INTO mbase2.modules(id) VALUES ((select id from mbase2.code_list_options_vw where list_key = 'modules' and key='tlm'))");
    }

    static function tlm_tracks_vw() {

        self::catchQuery("DROP TRIGGER if exists refresh_tlm_tracks_vw on mb2data.tlm_tracks");

        self::catchQuery("DROP materialized VIEW IF EXISTS mb2data.tlm_tracks_vw");
        self::catchQuery("DROP VIEW IF EXISTS mb2data.tlm_tracks_vw");
        
        self::catchQuery("CREATE VIEW mb2data.tlm_tracks_vw
        AS 
        select a1.*, a2.species_list_id, a2.sex_list_id from
        (select id, track_type_id, slug, animal_id, _licence_name, date_record_created, date_record_modified, st_asgeojson(st_makeline(distinct geom)) geom from
                (select tt.id, track_type_id, slug, animal_id, _licence_name, date_record_created, date_record_modified,
                public.ST_SnapToGrid(public.ST_Transform(_location, 3035) , 0, 0, 1000, 1000) geom
                from 
                mb2data.tlm_tracks tt
                left join mb2data.tlm t on tt.id = t.tlm_tracks_id) a
                group by id, track_type_id, slug, animal_id, _licence_name, date_record_created, date_record_modified) a1
        left join
        mb2data.tlm_named_animals_vw a2 on a2.id = a1.animal_id");
    }

    static function tlm_vw() {
        
        self::catchQuery("DROP view if exists mb2data.tlm_vw");

        self::catchQuery("CREATE OR REPLACE VIEW mb2data.tlm_vw
        AS SELECT tlm.id,
        	animal.species_list_id,
            tlm_tracks.animal_id,
        	tlm_tracks._licence_name as licence_list_id,
            date(tlm.point_time_stamp) as event_date,
            tlm.point_time_stamp,
            tlm.tlm_tracks_id,
            tlm.altitude,
            tlm.temperature,
            tlm._location as _location,
            public.st_asgeojson(tlm._location) as geom,
            tlm._batch_id,
            tlm.id_mas,
            tlm.collar_id,
            '{}'::jsonb as _location_data
        FROM mb2data.tlm
        left join mb2data.tlm_tracks tlm_tracks on tlm_tracks.id = tlm.tlm_tracks_id
        left join mb2data.tlm_named_animals_vw animal on animal.id::varchar = tlm_tracks.animal_id");
    }
}