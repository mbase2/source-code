<?php

class Mbase2OutputFilter {

    function __construct($input, &$result, $tname=null)
    {
      $this->input = $input;
      $this->result = &$result;
      $this->tname = $tname;
      
      if (isset($this->module) && Mbase2Drupal::isAdmin($this->module)) return;
      if (method_exists($this, 'whenToFilter')) {
        if ($this->whenToFilter()) {
          $this->filter();
        }
      }
      else {
        $this->filter();
      }
    }
}
