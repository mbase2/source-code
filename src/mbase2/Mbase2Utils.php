<?php

    final class Mbase2Utils {

        const ROLES = ['admins', 'editors', 'consumers', 'readers', 'curators'];

        const MODULE_VARIABLES = [
            'data_type_id',
            'key_name_id',
            'ref',
            'module_id',
            'required',
            'unique_constraint',
            'pattern_id',
            'weight',
            'visible',
            'read_only',
            'default_value',
            '_data',
            '_desc',
            'filterable',
            'importable',
            'dbcolumn',
            'visible_in_table',
            'weight_in_table',
            'weight_in_import',
            'weight_in_popup',
            'aggregate_operation',
            'section_in_form',
            'visible_in_popup',
            'visible_anonymously',
            'translations',
            'visible_in_cv_detail',
            'visible_in_cv_grid',
            'translations',
            'help',
            'exportable',
            'weight_in_export'
        ];

        const DEFAULT_VARIABLES = [
                [
                    'key_name_id' => 'event_date',
                    'key_data_type_id' => 'date',
                    'required' => true
                ],
                [
                    'key_name_id' => 'event_time',
                    'key_data_type_id' => 'date',
                    'required' => false
                ],
                [
                    'key_name_id' => '_batch_id',
                    'key_data_type_id' => 'table_reference',
                    'ref' => 'import_batches',
                    'required' => false,
                    'visible' => false,
                    'visible_in_table'=>false
                ],
                [
                    'key_name_id' => '_species_name',
                    'key_data_type_id' => 'code_list_reference',
                    'ref' => 'species'
                ],
                [
                    'key_name_id' => '_licence_name',
                    'key_data_type_id' => 'table_reference',
                    'ref'=>'licences',
                    'required' => true
                ],
                [
                    'key_name_id'=>'_location',
                    'key_data_type_id'=>'location_geometry',
                    'visible_in_table' => false
                ],
                [
                    'key_name_id'=>'_location_data',
                    'key_data_type_id'=>'location_data_json',
                    'required' => true,
                    'visible' => false
                ]
                ];

        /**
         * Returns input array with keys preceeded with colon
         */
        static function parameters($p) {
            $rval = [];

            foreach ($p as $key => $value) {
                $rval[':'.$key] = $value;
            }

            return $rval;
        }

        static function mbase2allRoles() {
            $mbase2allRoles = [];

            foreach(self::ROLES as $role) {
                foreach(self::moduleKeys() as $module)
                    $mbase2allRoles[] = "mbase2-{$module}-{$role}";
            }

            return $mbase2allRoles;
        }

        static function genotypes_filter($a, $genotypesData) {
            $filtered = [];

            foreach($genotypesData as $sampleCode => $row) {
                if (isset($a[$row[0]])) continue;
                $filtered[$sampleCode] = $row;
            }

            return $filtered;
        }

        static function moduleRoles($moduleKey) {
            $moduleRoles = [];

            foreach(self::ROLES as $role) {
                $moduleRoles[] = "mbase2-{$moduleKey}-{$role}";
            }

            return $moduleRoles;
        }

        static function moduleKeys() {
            return ['dmg', 'ct', 'sop', 'interventions', 'cnt', 'gensam', 'mortbiom', 'tlm'];
        }

        static function SQL_withSpatialUnitEventDate($tableName) {
            return "with x as (SELECT jsonb_agg(jsonb_build_object('s',su.slug,'n',su.name)) _x, vw.id from $tableName vw
                left join mbase2_ge.spatial_units_by_date su 
                on public.st_intersects(su.geom, _location) 
                WHERE
                vw.event_date <= valid_to and vw.event_date >= valid_from
                group by id
            )";
        }

        static function SQL_whereUserCanAccessGensam($gensamTableAlias, $uid) {
            return "$gensamTableAlias._uid=$uid
                or $gensamTableAlias.user_access_ids @> '$uid'::jsonb 
                or exists (
                    SELECT 1
                    FROM mb2data.gensam_organisations o
                    WHERE o.gensam_organisations_users @> '$uid'::jsonb
                    AND $gensamTableAlias.organisation_access_ids @> o.id::text::jsonb)";    
        }

        static function SQL_whereGensamIsSharedWithMe($gensamTableAlias, $uid) {
            return "EXISTS (
                SELECT 1
                FROM mb2data.gensam_organisations o
                WHERE o.gensam_organisations_users @> '$uid'::jsonb
                  AND $gensamTableAlias.organisation_access_ids IS NOT NULL AND $gensamTableAlias.organisation_access_ids @> o.id::text::jsonb
              )
              OR ($gensamTableAlias.user_access_ids IS NOT NULL AND $gensamTableAlias.user_access_ids @> '$uid'::jsonb)";
        }

        static function SQL_whereGensamAnalysingLabIsUserOrganisation($gensamTableAlias, $uid) {
            return "$gensamTableAlias.analysing_lab IN (
                SELECT id
                FROM mb2data.gensam_organisations
                WHERE gensam_organisations_users @> '$uid'::jsonb
            )";
        }

        static function batchImportModuleKey($moduleKey) {
            $attributeDefinitionsNamespace = [
                'interventions' => 'interventions_batch_imports',
                'dmg' => 'dmg_batch_imports'
            ];

            if (isset($attributeDefinitionsNamespace[$moduleKey])) {
                $moduleKey = $attributeDefinitionsNamespace[$moduleKey];
            }

            return $moduleKey;
        }

        static function checkArraysEqual($a) {
            // If the array is empty, return an empty array
            if (empty($a)) {
                return [];
            }
        
            // Use the first array as a reference for comparison
            $firstArray = $a[0];
        
            // Loop through each array in $a
            foreach ($a as $array) {
                // If any array is not equal to the first array, return an empty array
                if ($array !== $firstArray) {
                    return [];
                }
            }
        
            // If all arrays are equal, return one of them (they are all identical)
            return $firstArray;
        }

        static function mbase2Roles($role) {
            $mbase2RoleRoles = [];

            foreach(self::moduleKeys() as $module) {
                $mbase2RoleRoles[] = "mbase2-{$module}-{$role}";
            }

            return $mbase2RoleRoles;
        }

        static function SQLforeignKeyConstraint($sourceSchemaName, $sourceTableName, $sourceFieldName, $targetSchemaName, $targetTableName, $targetFieldName) {
            return "ALTER TABLE $sourceSchemaName.$sourceTableName
            ADD CONSTRAINT {$sourceTableName}_{$sourceFieldName}_fkey
            FOREIGN KEY ($sourceFieldName) REFERENCES $targetSchemaName.$targetTableName ($targetFieldName)";
        }

        static function SQLfunction_refresh_mat_view() {
            return "CREATE OR REPLACE FUNCTION mbase2.refresh_mvw()
                RETURNS trigger AS 
                \$\$
                DECLARE
                _tbl text := TG_ARGV[0];   
                BEGIN
                    execute format('REFRESH MATERIALIZED VIEW CONCURRENTLY %1\$s', _tbl);
                    RETURN NULL;        
                END;
                \$\$LANGUAGE plpgsql;";
        }

        static function SQLfunction_sync_date_modified() {
            return 'CREATE OR REPLACE FUNCTION mbase2.sync_date_modified() RETURNS trigger AS $$
            BEGIN
            NEW.date_record_modified := NOW()::timestamp;
            RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;';
        }

        static function SQLcreateSpatialIndex($schema, $tname, $cname) {
            return "CREATE INDEX idx_{$tname}_{$cname} ON $schema.$tname USING gist ($cname);";
        }

        static function SQLcreateIndex($schema, $tname, $cname) {
            return "CREATE INDEX IF NOT EXISTS idx_{$tname}_{$cname} ON $schema.$tname($cname);";
        }

        static function SQLfunction_sync_date_created() {
            return 'CREATE OR REPLACE FUNCTION mbase2.sync_date_created() RETURNS trigger AS $$
            BEGIN
            NEW.date_record_created := NOW()::timestamp;
            BEGIN
                NEW.date_record_modified := NEW.date_record_created;
                EXCEPTION WHEN undefined_column then
            END;
            RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;';
        }

        static function SQLrequiredConstraint($schema, $tname, $cname) {
            return "ALTER TABLE $schema.$tname ALTER COLUMN $cname SET NOT NULL";
        }

        static function SQLuniqueConstraint($schema, $tname, $cname) {
            return "ALTER TABLE $schema.$tname ADD CONSTRAINT {$tname}_{$cname}_unique_constraint UNIQUE ($cname)";
        }

        static function SQLtrigger_sync_date_modified($schemaName, $tableName) {
            return "CREATE TRIGGER
            ${schemaName}_${tableName}_modified
            BEFORE UPDATE ON
            $schemaName.$tableName
            FOR EACH ROW EXECUTE PROCEDURE
            mbase2.sync_date_modified();";
        }

        static function SQLtrigger_sync_date_created($schemaName, $tableName) {
            return "CREATE TRIGGER
            ${schemaName}_${tableName}_created
            BEFORE INSERT ON
            $schemaName.$tableName
            FOR EACH ROW EXECUTE PROCEDURE
            mbase2.sync_date_created();";
        }

        static function userRoleExists($moduleKey, $roleKeys, $currentUserRoles=[]) {
            foreach($roleKeys as $roleKey) {
                if (isset($currentUserRoles['mbase2-'.$moduleKey.'-'.$roleKey])) {
                    return true;
                }
            }

            return false;
        }

        static function checkSampleCode($sampleCode) {
            if (preg_match('/^[A-Z]{3}[012345678ACEFHJKLMPTUX]{3}$/', $sampleCode) === 1) {
                return true;
            }

            return false;
        }

        static function generateSampleCode() {
            // Define character sets
            $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $allowedSet = '012345678ACEFHJKLMPTUX';

            // Generate the first 3 characters (random letters from A-Z)
            $firstPart = '';
            for ($i = 0; $i < 3; $i++) {
                $firstPart .= $letters[random_int(0, strlen($letters) - 1)];
            }

            // Generate the last 3 characters (random chars from the allowed set)
            $secondPart = '';
            for ($i = 0; $i < 3; $i++) {
                $secondPart .= $allowedSet[random_int(0, strlen($allowedSet) - 1)];
            }

            // Combine both parts and return the result
            return $firstPart . $secondPart;
        }

        static function getModuleNameFromTableName($tname) {

            if (empty($tname)) return null;

            $pos = strpos($tname,'_');

            if ($pos !== false) {
                $moduleName = substr($tname, 0, $pos);
            }
            else {
                $moduleName = $tname;   //if table name equals module name, e.g. mb2data.dmg
            }

            $pos = strpos($moduleName,'.');

            if ($pos === false) return $moduleName;

            return substr($moduleName, $pos+1);
        }

        //https://stackoverflow.com/questions/173400/how-to-check-if-php-array-is-associative-or-sequential
        static function isAssoc(array $arr)
        {
            if (array() === $arr) return false;
            return array_keys($arr) !== range(0, count($arr) - 1);
        }

        static function get_tree($args) {
            global $cenik;
            $tname = $args[count($args)-1];

            $res = Mbase2Database::query("SELECT * FROM $tname order by id");

            $cenik = false;

            if (isset($res[0]) && isset($res[0]['cenik'])) {
                $cenik = true;
            }
            
            /**
             * https://stackoverflow.com/questions/8587341/recursive-function-to-generate-multidimensional-array-from-database-result/8587437#8587437
             */
            function buildTree($rows, $parentId = null) {
                global $cenik;
                $branch = array();
            
                foreach ($rows as $row) {
                    $parent = $row['key_parent'];
                    $item = $row['key_item'];
                    if ($parent === $parentId) {
                        $nodes = buildTree($rows, $row['key_item']);
                        $node = ['text' => $item];
                        
                        if (isset($row['_item_id'])) {
                            $node['_item_id'] = $row['_item_id'];
                        }

                        if (isset($row['_mbase2_id'])) {
                            $node['_mbase2_id'] = $row['_mbase2_id'];
                        }

                        if (isset($row['key_mbase2'])) {
                            $node['zgsKey'] = $row['key_mbase2'];
                        }

                        if (!empty($nodes)) {
                            $node['nodes'] = $nodes;
                            $node['selectable'] = false;
                        }
                        else {
                            $node['icon'] = 'fa fa-file-o';
                            if ($cenik) {
                                if (isset($row['cenik'])) {
                                    $node['cenik'] = $row['cenik'];
                                }

                                if (isset($row['navedi'])) {
                                    $node['navedi'] = $row['navedi'];
                                }
                            }
                        }
                        $branch[] = $node;
                    }
                }
            
                return $branch;
            }
            
            $tree = buildTree($res);

            return $tree;
        }

        static function priceListLeavesIterator(&$data, $callback) {    
            foreach($data as $d) {
                if (isset($d->nodes)) {
                    self::priceListLeavesIterator($d->nodes, $callback);
                }
                else {
                    $callback($d);
                }
            }           
        }

        static function parsePriceList($id, $input=[]) {
            $fileData = Mbase2Files::getFileData($id);
            $filePath = $fileData[0].$fileData[2];

            require_once __DIR__.'/vendor/shuchkin/simplexlsx/src/SimpleXLSX.php';

            $xlsx = SimpleXLSX::parse($filePath);
            $xlsx->sheetName(0);

            $tree = [];

            function cenik($row) {
                $cenik = [];
                for ($i=5; $i < count($row); $i = $i+2) {
                    $item = [];
                    
                    if (!empty($row[$i])) {
                        $item["e"] = trim($row[$i]);
                        if (isset($row[$i+1]) && !empty($row[$i+1]) ) {
                            $item["c"] = floatval($row[$i+1]);
                        }
                        $cenik[] = $item;
                    }
                }

                return $cenik;
            }

            function leafData($row) {
                return [
                    "icon" => "fa fa-file-o",
                    'text' => trim($row[2]),
                    'cenik' => cenik($row),
                    'navedi' => intval($row[4]) === 1 ? true : false,
                    'zgsKey' => trim($row[3])  
                ];
            }

            foreach ( $xlsx->rows() as $row ) {
                if (count($row) < 6) continue; //row has to have at least 6 entries - 1st and last entry is not to be an empty cell
                
                $c1 = trim($row[0]);
                $c2 = trim($row[1]);
                $c3 = trim($row[2]);
                
                if (empty($c1)) continue;
                if (empty($c3)) continue;

                
                if (!isset($tree[$c1])) {
                    $tree[$c1] = [];
                }

                if (empty($c2)) {
                    if (!isset($tree[$c1][$c3])) {
                        $tree[$c1][$c3] = [];
                    }
                    $tree[$c1][$c3] = leafData($row);
                }
                else {
                    if (!isset($tree[$c1][$c2])) {
                        $tree[$c1][$c2] = [];
                    }
                    $tree[$c1][$c2][$c3] = leafData($row);
                }   
            }

            $treeArray = [];

            function parseNodes($tree) {
                $parsedNodes = [];
                foreach ($tree as $text => $nodes) {
                    $parsedNodes[] = isset($nodes['zgsKey']) ? $nodes : [
                        "text" => $text,
                        "selectable" => false,
                        "nodes" => parseNodes($nodes)
                    ];
                }
                return $parsedNodes;
            }

            $treeArray = parseNodes($tree);

            //print_r($tree);
            //echo json_encode($treeArray, JSON_PRETTY_PRINT);
            if (empty($input) || empty($input['valid_till'])) {
                Mbase2Database::query("UPDATE mb2data.dmg_objects_price_lists SET data=:data WHERE valid_till is null",[':data'=>json_encode($treeArray)]);
            }
            else {
                Mbase2Database::query("UPDATE mb2data.dmg_objects_price_lists SET data=:data WHERE valid_till = :valid_till",[':data'=>json_encode($treeArray),':valid_till' => $input['valid_till']]); 
            }

            return;
        }

        static function readExcel($filePath, &$data, $sheetInx) {
            require_once __DIR__.'/vendor/shuchkin/simplexlsx/src/SimpleXLSX.php';
            $xlsx = SimpleXLSX::parse($filePath);

            $sheetInxArray = is_array($sheetInx) ? $sheetInx : [$sheetInx];

            foreach ($sheetInxArray as $dataInx => $pageInx) {
                $data[$dataInx] = []; 
                foreach ( $xlsx->rows($pageInx) as $row ) {
                    $data[$dataInx][] = $row;
                }
            }

            if (!is_array($sheetInx)) {
                $data = $data[0];
            }
        }
    
        static function readCSV($filePath, &$data) {
            $i = 0;
            if (($handle = fopen($filePath, "r")) !== FALSE) {
                while (($row = fgetcsv($handle, 2048, ",")) !== FALSE) {
                    $data[$i] = $row;
                    $i++;
                }
                fclose($handle);
            }
        }

        /**
         * SQLSTATE[23505]: Unique violation: 7 ERROR: duplicate key value violates unique constraint "gensam_sample_code_unique_constraint" DETAIL: Key (sample_code)=(AX134RW2) already exists.
         */
        static function parseDatabaseErrorMessage($err) {
            $r = [];
            
            if (preg_match('/SQLSTATE\[(.*?)\]/', $err, $match) == 1) {
                $r['code'] = $match[1];
            }

            if (preg_match('/(.*)ERROR:/', $err, $match) == 1) {
                $r['sqlstate'] = trim($match[1]);
            }
            

            if (preg_match('/ERROR:(.*?)DETAIL:/', $err, $match) == 1) {
                $r['error'] = trim($match[1]);
            }

            if (preg_match('/DETAIL:(.*)/', $err, $match) == 1) {
                $r['detail'] = trim($match[1]);
            }

            return $r;
        }

        static function getDatabaseErrorMessageError($err) {

            if (preg_match('/ERROR:(.*)/', $err, $match) == 1) {
                return trim($match[1]);
            }

            return '';
        }

        static function createIndirectClaim($id) {
            $id = intval($id);
            
            Mbase2Database::query("BEGIN");

            $res = null;
            
            try {
                $result = Mbase2Database::query("UPDATE mb2data.dmg SET __indirect_counter=__indirect_counter+1 WHERE id=:id AND _claim_status > 0 RETURNING *", [':id'=>$id]);

                if (empty($result) || count($result) > 1) {
                    throw new Exception('Unable to create indirect claim with the provided ID.');
                }
    
                $affectees = json_decode($result[0]['_affectees']);
                
                $result2 = Mbase2Database::query("SELECT count(*) as cnt FROM mb2data.dmg_agreements WHERE _claim_id = :id AND _completed=true", [':id'=>$id]);
    
                if (empty($result2) || $result2[0]['cnt'] != count($affectees)) {
                    throw new Exception('Indirect claim is only possible with fully completed claims (all agreements resolved)');
                }
    
                $input = $result[0];
                unset($input['id']);
                unset($input['_location']);
                if (isset($input['_location_data']) && !is_array($input['_location_data'])) {
                    $input['_location_data'] = json_decode($input['_location_data'], true);
                }
                
                $input['_claim_id'] = $input['_claim_id'].'/'.$input['__indirect_counter'];
                $input['_uid'] = true;
                $input['_dmg_objects'] = null;
                $input['_claim_status'] = 0;
    
                //add : to params keys
                $params = [];
    
                foreach ($input as $key => $value) {
                    $params[':'.$key] = $value;
                }

                $res = Mbase2Database::insert('mb2data.dmg', $params);
            }
            catch (Exception $e) {
                Mbase2Database::query("ROLLBACK");
                throw new Exception($e->getMessage());  
            }
    
            Mbase2Database::query("COMMIT");
            
            return $res;
        }

        /**
         * Add mbase2_dmg_editor user role to users in the mb2data.dmg_deputies table
         */

        static function dmg_editor_role() {
            $res = Mbase2Database::query("SELECT _uname as uid from mb2data.dmg_deputies where _uname not in (SELECT ur.uid FROM public.users_roles ur, public.role r WHERE r.rid=ur.rid and r.name = 'mbase2_dmg_editor')");

            $role = Mbase2Drupal::getRoleByName('mbase2_dmg_editor');

            $rid = $role->rid;
            
            $rval = [];
            foreach ($res as $r) {
                $rval[] = Mbase2Drupal::addRoleToUser($rid, $r['uid']);
            }

            return $rval;
        }

        public static function dmgAdminEditor($procName, $inputParameters, $requestType) {
            if (!in_array($procName, ['delete', 'rename'])) {
                throw new Exception("Unknown procedure.");
                return;
            }

            $currentUserRoles = Mbase2Drupal::currentUserRoles();

            if (!isset($currentUserRoles['mbase2-dmg-admins'])) {
                throw new Exception("Only module admins can access this functionality.");
                return;
            }

            if ($procName === 'delete') {
                if ($requestType !== 'DELETE') {
                    throw new Exception("Wrong request type for this functionality.");
                    return;
                }

                $res = \DB::select("SELECT id from mb2data.dmg WHERE _claim_id=:claimId", $inputParameters);

                if (empty($res)) {
                    throw new Exception("The claim ".$inputParameters['claimId']." does not exist.");
                    return;
                }

                $id = $res[0]->id;

                \DB::delete("DELETE FROM mb2data.dmg_agreements WHERE _claim_id=:id", [':id' => $id]);

                $num = \DB::delete("DELETE FROM mb2data.dmg WHERE id=:id", [':id' => $id]);

                return $num;

            }
            else if ($procName === 'rename') {
                if ($requestType !== 'PUT') {
                    throw new Exception("Wrong request type for this functionality.");
                    return;
                }

                $num = \DB::update("UPDATE mb2data.dmg SET _claim_id=:claimIdNew WHERE _claim_id=:claimIdOld", $inputParameters);

                return $num;
            }
        }

        public static function getFilePropertiesByHash($subfolder) {
            $fileProperties = Mbase2Database::query("SELECT fp.*, up.file_name from mbase2.file_properties fp, mbase2.uploads up where up.file_hash = fp.file_hash and up.subfolder=:subfolder",[':subfolder' => $subfolder]);
            return array_column($fileProperties, null,'file_hash');
        }

        /**
         * Extracts module name from batch import json data
         * @param $res result set $row from table mbase2.import_batches
         */

        public static function getBatchModuleKey($res) {
            $data = $res['data'];

            $moduleKey = '';

            if (!empty($data)) {
                $data = json_decode($data);
                $dataDefinitions = $data->dataDefinitions;

                if (!empty($dataDefinitions)) {
                    $moduleKey = array_keys((array)$dataDefinitions)[0];
                }
            }

            return $moduleKey;
        }

        public static function dmg_get_deputy_oeid() {
            $res = Mbase2Database::query("SELECT zgs._oe_id as oeid FROM mb2data.dmg_deputies d, mbase2.obmocne_enote_zgs zgs WHERE zgs.id = d._oe_id AND _uname = :uid",[':uid'=>Mbase2Drupal::getCurrentUserData()['uid']]);
      
            if (empty($res)) return $res;
      
            return $res[0]['oeid'];
        }

        /**
         * 0 and '0' are not treated as empty in this function
         */

        public static function empty($a) {
            if ($a === '0' || $a === 0) return false;
            return empty($a);
        }

        public static function getDistinctSpatialUnitsList($cname) {
            
        }

        /**
         * Helper function
         */
        private static function translationsKey($ref, $lang) {
            $key = null;
            $translations = isset($ref['translations']) ? json_decode($ref['translations'], true) : [];
            if (isset($translations[$lang])) {
                $key = $translations[$lang];
            }
            return $key;
        }

        /**
         * Checks if object has hierarchical key (e.g. object[key1.key2.key3] = value) and sets the value of object[key1] to {
         * $key2 => {
         * $key3 => value}}
         */
        public static function setObjectChildren(&$values, $deleteOriginalKey = true) {
            foreach ($values as $key=>$value) {
                if (self::empty($value) && $value!==false) {
                    if ($deleteOriginalKey) {
                        unset($values[$key]);
                    }
                    continue;
                }
                if (strpos($key,'.') !== FALSE) {
                    $keys = explode('.', $key);
            
                    if (!isset($values[$keys[0]])) {
                        $values[$keys[0]] = [];
                    }
            
                    $objValue = &$values[$keys[0]];
            
                    array_shift($keys);
            
                    $lastKey = $keys[count($keys)-1];
            
                    foreach($keys as $objKey) {
            
                        if (!isset($objValue[$objKey])) {
                            $objValue[$objKey] = [];
                        }
            
                        if ($objKey === $lastKey) {
                            $objValue[$lastKey] = $value;
                        }
                        else {
                            $objValue = &$objValue[$objKey];
                        }
                    }
                    unset($objValue);

                    if ($deleteOriginalKey) {
                        unset($values[$key]);
                    }
                }
            }
        }

        public static function getProxyKeyValueInArray($row, $key, $a) {
            return isset($row[$key]) && !empty($row[$key]) && isset($a[$row[$key]]) ? $a[$row[$key]] : '';
        }

        /**
         * Gets code_list_options for selected language for specified $refListIf (list_id)
         * @param array $references 
         * @param boolean $translationToId build values assoc array as $translation => id
         * @param string $slug array key in reference - if $slug !== 'key' then $slug denotes the language code for translation
         */
        public static function getReferencedValues($references, $refListId, $translationToId = true, $slug = 'key', $languageCode = null) {
            
            $values = [];
            foreach ($references as $i => $ref) {
                if ($ref['list_id'] === $refListId) {
                    $key = $ref['key'];
                    if ($translationToId) {
                        if ($slug !== 'key') {
                            $key = self::translationsKey($ref, $slug);                            
                        }

                        if (!is_null($key)) {
                            $values[mb_strtolower($key)] = $ref['id'];
                        }
                    }
                    else {
                        
                        $value = $ref['id'];
                        
                        if (!is_null($languageCode) && isset($ref['translations'])) {
                            $translations = json_decode($ref['translations'],true);
                            if (isset($translations[$languageCode])) {
                                $value = $translations[$languageCode];
                            }
                            else if (isset($translations['en'])) {
                                $value = $translations['en'];
                            }
                        }
                        else {
                            $value = mb_strtolower($key);
                        }

                        $values[$ref['id']] = $value;
                    }
                    
                }
            }
            return $values;
        }

        /**
         * Gets attributes for module and adds 'value' property to attributes array with referenced values resolved.
         * @param boolean $translationToId table reference value array attached to single attribute item is associative array translation=>ID
         */

        public static function getModuleAttributes($moduleKey, $attributes =[], $languageCode=null,$translationToId=true) {

            if (empty($attributes)) {
                $attributes = Mbase2Database::query('SELECT * FROM mbase2.module_variables_vw WHERE module_name=:module_name order by weight_in_table', [':module_name' => $moduleKey]);
            }
            
            $referencedTableIds = [];
            $codeListOptionsIds = [];
            //$referencedTableIds = Mbase2Database::query("SELECT ref FROM mbase2.module_variables_vw WHERE module_name = :module_name and data_type like 'table_reference%'", [':module_name' => 'gensam']);
            foreach ($attributes as $a) {

                $dataType = $a['data_type'];

                if (strpos($dataType,'table_reference') !== FALSE) {
                    $referencedTableIds[] = $a['ref'];
                }
                else if (strpos($dataType,'code_list_reference') !== FALSE) {
                    $codeListOptionsIds[] = $a['ref'];
                }
            }

            $referencedTables = [];
            $referencedTableValues = [];
            $referencedCodeListValues = [];

            if (!empty($referencedTableIds)) {
                $referencedTables = Mbase2Database::select('mbase2.referenced_tables',[':id' => $referencedTableIds]);
                $referencedTableValues = Mbase2Utils::getReferencedTableValues($referencedTables);
            }

            if (!empty($codeListOptionsIds)) {
                $referencedCodeListValues = Mbase2Database::select('mbase2.code_list_options_vw', [':list_key_id' => $codeListOptionsIds]);
            }

            /**
             * ref is actually a reference to code_list_options id of code_list key
             */

            foreach($referencedCodeListValues as &$value) {
                $value['list_id'] = $value['list_key_id'];
            }
            unset($value);

            foreach($attributes as &$a) {
                if ($a['data_type'] === 'code_list_reference' || $a['data_type'] === 'code_list_reference_array') {
                    $a['values'] = Mbase2Utils::getReferencedValues($referencedCodeListValues, $a['ref'], $translationToId, 'key', $languageCode);
                }
                else if ($a['data_type'] === 'table_reference' || $a['data_type'] === 'table_reference_array') {
                    $a['values'] = Mbase2Utils::getReferencedValues($referencedTableValues, $a['ref'], $translationToId, 'key', $languageCode);
                }
            }

            return $attributes;
        }

        /**
         * returns true if user has any role in the $moduleSlug
         */

        public static function hasUserRoleInModule($moduleSlug, $moduleRole='') {
            $roles = array_keys(Mbase2Drupal::currentUserRoles());

            $e = explode('_',$moduleSlug);

            $moduleSlug = $e[0];

            foreach ($roles as $role) {
                if (strpos($role,'-'.$moduleSlug.'-'.$moduleRole) !== FALSE) {
                    return true;
                }
            }
            return false;
        }

        /**
         * @param {array} associative array of TRANSLATED(! - attribute "key_id" is expected to be present in each row) rows from mbase2.referenced_tables
         * @param {object} overrides (for camelot) - sql statements to be executed instead of primery definition in referenced tables
         */

        public static function getReferencedTableValues($tableReferences, $overrides = []) {

            $res = [];
            
            foreach($tableReferences as $tr) {
                $tname = $tr['key_id'];
                $schema = $tr['schema'];
                $cname = $tr['label_key'];
                $key = $tr['pkey'];
                $listId = $tr['id'];
                $additional = $tr['additional'];
                $from = empty($tr['select_from']) ? $schema.'.'.$tname : '('.$tr['select_from'].') a';

                $additional = empty($additional) ? "" : ", $additional";

                try {
                    if (isset($overrides[$tname])) {
                        $r = Mbase2Database::query($overrides[$tname]['sql'],$overrides[$tname]['params']);
                    }
                    else {
                        $r = Mbase2Database::query("SELECT $key as id, $listId as list_id, '$tname' as list_key, $cname as key $additional FROM $from");
                    }
                    
                    if (!empty($r)) {
                        $res = array_merge($res, $r);
                    }
                } catch (Exception $e) {
                    return ['err' => ['error'=>$e, 'tr'=>$tr]];
                }
            }

            return $res;
        }

        public static function isReference($dataType) {
            return in_array($dataType, ['code_list_reference', 'table_reference', 'code_list_reference_array', 'table_reference_array']) !== FALSE;
        }

        public static function replaceCodeListValuesWithTranslation($a, $value) {
            $value = trim($value);
            if (self::empty($value)) return $value;

            if (in_array($a['data_type'], ['code_list_reference', 'table_reference'])) {
                return isset($a['values'][$value]) ? $a['values'][$value] : $value;
            }
            else if (in_array($a['data_type'], ['code_list_reference_array', 'table_reference_array'])) {
                $values = json_decode($value);
                $value = [];
                foreach ($values as $v) {
                    $value[] = isset($a['values'][$v]) ? $a['values'][$v] : $v;
                }
                return implode(',', $value);
            }
        }

        function debug_output($msg, $title='') {
            if (!empty($title)) {
                echo "\n--------BEGIN $title--------\n";
            }

            print_r($msg);

            if (!empty($title)) {
                echo "\n--------END $title--------\n";
            }
        }

        public static function required_attribute_values($row, $attributes, $prepend=':') {
            foreach($attributes as $a) {
                if (!$a->required) continue;
                $key = $prepend.$a->key_name_id;
                
                if (!isset($row->$key)) {
                    return false;
                }

                if (empty(trim($row->$key))) {
                    return false;
                }
            }

            return true;
        } 

        /**
         * $p WGS 84 coordinates of a point [':lat' => double, ':lon' => double]
         */
        public static function getSpatialUnitsIntersectingPoint($p) {
            $res = \DB::select("select filter_types.slug, filter_elements.name::jsonb->>'name' name from mbase2_ge.spatial_units su, 
            laravel.spatial_units_spatial_unit_filter_elements su_filter_elements,
            laravel.spatial_unit_filter_elements filter_elements,
            laravel.spatial_unit_filter_type_versions filter_type_versions,
            laravel.spatial_unit_filter_types filter_types
            where
            su.gid = su_filter_elements.spatial_unit_gid
            and su_filter_elements.spatial_unit_filter_element_id = filter_elements.id
            and filter_elements.spatial_unit_filter_type_version_id = filter_type_versions.id
            and filter_type_versions.spatial_unit_filter_type_id = filter_types.id
            and public.ST_Intersects(geom,public.ST_SetSRID(public.ST_MakePoint(:lon,:lat),4326)) 
            and date(valid_from) <= :date and date(valid_to) > :date", $p);

            return $res;
        }
    }
