<?php
require_once(__DIR__.'/Mbase2Database.php');
require_once(__DIR__.'/Mbase2Utils.php');

class Mbase2SchemaPatches {
    protected static $errors = [];
    protected static $module = null;

    protected static $tableReferences = [
        'individual_name' => [
            'referenced_table' => 'individuals',
            'pkey' => 'id',
            'schema' => 'mb2data',
            'label_key' => 'individual_name'
        ],
        '_uid' => [
            'referenced_table' => '_users',
            'pkey' => '_uid',
            'schema' => 'mbase2',
            'label_key' => '_uname'
        ]
    ];

    public static function getVariableDefinition($keyNameId, $additional=[]) {
        $variableDefinitions = [
            '_post_number' => [
                "key_data_type_id"=> "table_reference",
                "ref"=> "poste"
            ],
            '_country' => [
                "key_data_type_id"=> "table_reference",
                "ref"=>"countries"
            ],  
            '_full_name' => [
                "key_data_type_id"=> "text"
            ],
            "_street" => [
                "key_data_type_id"=> "text"
            ],
            "_house_number" => [
                "key_data_type_id"=> "text"
            ],
            "_phone" => [
                "key_data_type_id"=> "phone"
            ],
            "_email" => [
                "key_data_type_id"=> "email"
            ],
        ];

        $rval = [];

        if (isset($variableDefinitions[$keyNameId])) {
            $rval = $variableDefinitions[$keyNameId];
            $rval['key_name_id'] = $keyNameId;
        }

        return array_merge($rval, $additional);
    }

    public static function catchQuery($query, $params = []) {
        try {
            return Mbase2Database::query($query, $params);
        }
        catch(Exception $e) {
            //print_r(['sql'=>$query, 'error'=>$e->getMessage()]);
            self::$errors[] = ['sql'=>$query, 'error'=>$e->getMessage()];
        }

        return [];
    }

    public static function removeCodeList($codeListKey) {
        Mbase2Database::query("DELETE FROM mbase2.code_list_options WHERE list_id = (select id from mbase2.code_lists where label_key = :codeListKey)", [':codeListKey' => $codeListKey]);
        Mbase2Database::query("DELETE FROM mbase2.code_list_options WHERE key = :codeListKey and list_id = (SELECT id from mbase2.code_lists where label_key = 'code_lists')",[':codeListKey' => $codeListKey]);
        Mbase2Database::query("DELETE FROM mbase2.code_lists WHERE label_key = :codeListKey", [':codeListKey' => $codeListKey]);
    }

    public static function removeCodeListItem($codeListKey, $codeListItemKey) {
        self::catchQuery("delete from mbase2.code_list_options where id = (select id from mbase2.code_list_options_vw where key=:key and list_key=:list_key)",
            [':key' => $codeListItemKey, ':list_key' => $codeListKey]
        );
    }

    /**
     * $module {string} module name
     * $patchId {integer} run specific patch
     */

    public static function run($module, $patchId = null) {

        if (is_null($module)) return; 

        /**
         * if $module === '___all' runs all patches
         */
        if ($module === '___all') {
            foreach (glob(__DIR__."/patches/*.php") as $filename) {
                $nfo = pathinfo($filename);
                if ($nfo['filename'] === 'mbase2') continue;
                self::run($nfo['filename']);
            }
            return;
        }

        require_once(__DIR__."/patches/$module.php");

        self::catchQuery("set schema 'mbase2'");

        self::catchQuery("SET search_path TO public, mbase2;");

        self::$module = $module;

        if ($module != 'utils') {
            echo "Patching module $module\n";
        }

        $displayCaughtErrors=false;

        if (!is_null($patchId) && $patchId !== '___all') {
            $res = call_user_func_array(['patches\\'.$module, 'patch_'.$patchId],[]);
            
            return ['result' => $res, 'errors' => self::$errors];
        }
        else if ($patchId === '___all') {
            $displayCaughtErrors=true;
        }

        self::catchQuery("ALTER TABLE mbase2.modules add column if not exists patch_id integer not null default -1");

        $qres = Mbase2Database::query("SELECT id from mbase2.modules_vw WHERE module_key=:module",[':module' => $module]);

        if (empty($qres)) {
            echo "$module is not a database module. Skipping ...\n";
            return;
        }

        $moduleId = $qres[0]['id'];

        echo "Module ID: $moduleId\n";

        $currentPatchId = intval(Mbase2Database::query("SELECT patch_id FROM mbase2.modules WHERE id=:moduleId",[':moduleId' => $moduleId])[0]['patch_id']);

        $reflection = new \ReflectionClass('patches\\'.$module);

        $methods = [];

        foreach ($reflection->getMethods(\ReflectionMethod::IS_STATIC) as $method) {
            if ($method->class !== 'patches\\'.$module) continue;
            if (strpos($method->name,'patch_') === 0) {
                $patchId = intval(str_replace('patch_','', $method->name));
                $methods[$patchId] = ['class' => $method->class, 'name' => $method->name];
            }
        }

        ksort($methods);

        $patchIds = array_keys($methods);

        $lastPatchId = $patchIds[count($patchIds)-1];

        echo "Current patch ID: $currentPatchId, highest patch ID: $lastPatchId (all patches between current patch ID and highest patch ID are run)\n";
        
        foreach ($methods as $patchId => $method) {
            if ($patchId > $currentPatchId) {
                echo "{$method['class']}::{$method['name']}\n";
                call_user_func_array([$method['class'], $method['name']],[]);
                Mbase2Database::query("UPDATE mbase2.modules set patch_id=:patch_id WHERE id=:moduleId",[':patch_id' => $patchId,':moduleId'=>$moduleId]);
            }
        }

        if ($displayCaughtErrors) {
            print_r(self::$errors);
        }
    }

    /**
     * @param {object} $translations key values pairs, array of language code and translation values, e.g. [['en' => 'translation for en'],['sl' => 'prevod za sl']]
     */

    public static function translate($translations, $listKey) {
        foreach ($translations as $key => $value) {
            self::catchQuery("UPDATE mbase2.code_list_options 
                SET translations=COALESCE(mbase2.code_list_options.translations||:translations::jsonb, mbase2.code_list_options.translations, :translations::jsonb)
                WHERE key=:key and list_id=(SELECT id from mbase2.code_lists WHERE label_key = :label_key)",[':key' => $key, ':label_key'=>$listKey, ':translations' => json_encode($value)]);
        }
    }

    public static function createCrossReferenceTable($listOfTables, $listOfTablesIdColumns) {
        $tname = implode('_', $listOfTables);
        Schema::create($tableName, function (Blueprint $table) use ($listOfTables, $listOfTablesIdColumns) {
            // Add a serial primary key column
            $table->id();

            // Loop through the list of tables and their corresponding ID columns
            foreach ($listOfTables as $index => $tableName) {
                $columnName = $listOfTablesIdColumns[$index];

                // Add a foreign key column for each table
                $table->unsignedBigInteger($columnName);

                // Define the foreign key constraint
                $table->foreign($columnName)
                    ->references('id')
                    ->on($tableName);
                    //->onDelete('cascade'); // Optional: set cascade delete behavior
            }
        });
    }

    public static function addCodeListOption($label_key, $key_name_id, $translations=null) {
        echo "Adding code list option: $label_key $key_name_id\n";

        $res = self::catchQuery("SELECT id from mbase2.code_lists where label_key=:label_key",[':label_key'=>$label_key]);
        if (empty($res)) {
            echo "Adding code list to the database: $label_key\n";
            self::catchQuery("INSERT into mbase2.code_lists (label_key) values (:label_key) returning id",[':label_key'=>$label_key]);
        }
        
        $res = Mbase2Database::query("insert into mbase2.code_list_options (translations, key, list_id) values (:translations, :key_name_id, (select id from mbase2.code_lists where label_key=:label_key))
            ON CONFLICT(key, list_id) DO NOTHING returning id;", [':translations'=>is_array($translations) ? json_encode($translations) : $translations, ':key_name_id'=>$key_name_id, ':label_key'=>$label_key]);

        if (empty($res)) {
            $res = Mbase2Database::query("SELECT id from mbase2.code_list_options_vw where list_key=:label_key and key=:key_name_id",[':label_key' => $label_key, ':key_name_id' => $key_name_id]);
        }

        echo "Code list option '$label_key -> $key_name_id' added: ".json_encode($res[0])."\n";

        return $res[0]['id'];
    }

    public static function removeReferencedTable($tname) {
        $r = self::catchQuery("select schema from mbase2.referenced_tables where id = (SELECT id from mbase2.code_list_options_vw where key=:key and list_key='referenced_tables')",[':key'=>$tname]);
        if (empty($r)) return;
        $schema = $r[0]['schema'];
        echo "schema: $schema\n";
        if (empty($schema)) return;
        self::catchQuery("delete from mbase2.referenced_tables where id = (SELECT id from mbase2.code_list_options_vw where key=:key and list_key='referenced_tables')",[':key' => $tname]);
        self::catchQuery("DROP TABLE IF EXISTS $schema.$tname");
    }

    public static function importCodeLists($codeLists) {
        foreach($codeLists as $codeList => $codeListKeys) {
            self::catchQuery("insert into mbase2.code_lists (label_key) values ('$codeList')");
            self::addCodeListOption('code_lists', $codeList);
            foreach ($codeListKeys as $key) {
                self::addCodeListOption($codeList, $key);
            }
        }
    }

    public static function getModuleKey($moduleKey = null) {
        return is_null($moduleKey) ? self::$module : $moduleKey;
    }

    public static function dropModuleVariables($moduleKey=null, $listKey='modules') {
        $moduleKey = self::getModuleKey($moduleKey);
        self::catchQuery("with cl as (select * from mbase2.code_list_options_vw clo)
            delete from mbase2.module_variables where module_id = (select id from cl where key=:moduleKey and list_key=:list_key)",[':moduleKey'=>$moduleKey, ':list_key'=>$listKey]);
    }

    public static function dropModuleVariable($variableKey, $moduleKey=null) {
        self::catchQuery("DELETE FROM mbase2.module_variables WHERE id=(SELECT id from mbase2.module_variables_vw WHERE module_name=:moduleKey and variable_name=:variableKey)",
        [':moduleKey'=>$moduleKey, ':variableKey'=>$variableKey]);
    }

    public static function bulkInsertCodeListValues($codeLists, $deleteExisting = true) {
        foreach($codeLists as $codeListName => $codeListOptions) {
            self::catchQuery("insert into mbase2.code_lists (label_key) values (:code_list_name)", [':code_list_name' => $codeListName]);
            self::addCodeListOption('code_lists', $codeListName);

            if ($deleteExisting) {
                self::catchQuery("DELETE FROM mbase2.code_list_options WHERE list_id = (SELECT id from mbase2.code_lists WHERE label_key=:codeListName)", [':codeListName'=>$codeListName]);
            }

            foreach($codeListOptions as $key => $translations) {
                self::addCodeListOption($codeListName, $key, $translations);
            }
        }
    }

    public static function copyVariableDefinition($variableName, $fromModule, $toModule) {

        $toModuleId = \DB::select("select id from mbase2.code_list_options_vw clov where list_key in ('modules', 'referenced_tables') and key=:key",[':key' => $toModule])[0]->id;

        $v = \DB::select("SELECT key_name_id, translations, ref, data_type_id, module_id from mbase2.module_variables_vw WHERE module_name=:fromModule and variable_name=:variableName",[':fromModule' => $fromModule, ':variableName' => $variableName])[0];

        $v->module_id=$toModuleId;

        \DB::table('mbase2.module_variables')->insert((array)$v);
    }

    /**
     * The schema name defaults to mb2data, except for the referenced tables where the schema name is read from mbase2.referenced_tables table
     * $key_name_id = variable column name to update
     */

    public static function updateModuleDatabaseSchema($moduleKey = null, $listKey = 'modules', $key_name_id = null) {
        $moduleKey = self::getModuleKey($moduleKey);
        $res = self::catchQuery("SELECT * from mbase2.code_list_options_vw where key = :moduleKey and list_key = :listKey", [':moduleKey' => $moduleKey, ':listKey'=>$listKey]);
        $id = $res[0]['id'];
        $schema = 'mb2data';
        if ($listKey === 'referenced_tables') {

            $res = self::catchQuery("SELECT * from mbase2.referenced_tables where id=:id",[':id'=>$id]);
            $schema = $res[0]['schema'];
        }

        if (empty($schema)) $schema = 'mb2data';

        Mbase2Database::updateSchema($moduleKey, $schema, $key_name_id);
    }

    public static function addReferenceTable($tname, $pkey, $schema, $labelKey, $selectFrom = null, $additional = null) {
        self::addCodeListOption('referenced_tables', $tname);
        $idSQL = "SELECT ID FROM mbase2.code_list_options_vw where key=:tname and list_key='referenced_tables'";
        return self::catchQuery("INSERT INTO mbase2.referenced_tables (id, pkey,schema,label_key, select_from, additional) values (($idSQL), :pkey,:schema,:label_key, :select_from, :additional)
        ON CONFLICT(id) DO UPDATE SET pkey=:pkey, schema=:schema, label_key=:label_key, select_from=:select_from, additional=:additional WHERE mbase2.referenced_tables.id = ($idSQL) returning id", [
            ':tname' => $tname,
            ':pkey' => $pkey,
            ':schema' => $schema,
            ':label_key' => $labelKey,
            ':select_from' => $selectFrom,
            ':additional' => $additional
        ]);
    }

    public static function deleteCodeList($codeListLabel) {
        self::catchQuery("delete from mbase2.code_list_options where id=(select id from mbase2.code_list_options_vw where key=:clabel and list_key='code_lists');",[':clabel' => $codeListLabel]);
        self::catchQuery("delete from mbase2.code_list_options where list_id=(select id from mbase2.code_lists where label_key=:clabel);",[':clabel' => $codeListLabel]);
        self::catchQuery("delete from mbase2.code_lists where label_key = :clabel;",[':clabel' => $codeListLabel]);
    }

    public static function updateVariables($variables, $moduleKey) {

        foreach($variables as $v) {
            $set=[];
            $p=[];
            foreach($v as $key => $value) {
                if ($key === 'key_name_id') continue;
                if ($key === 'key_data_type_id') {
                    $res = \DB::select("SELECT id, key from mbase2.code_list_options_vw WHERE list_key='data_types' and key=:key_data_type_id",[':key_data_type_id' => $value]);
                    if (empty($res)) {
                        self::$errors[] = ['error'=>"Error: data type $value is not defined."];
                        break;
                    }
                    
                    $value = $res[0]->id;
                    $key = 'data_type_id';
                }
                else if ($key == 'ref') {
                    if (strpos($v['key_data_type_id'], 'code_list')!==FALSE) {
                        $res = \DB::select("select * from mbase2.code_list_options_vw where key=:ref and list_key='code_lists'",[':ref'=>$value]);
                    }
                    else {
                        $res = \DB::select("select id,key from mbase2.referenced_tables_vw where key = :ref",[':ref'=>$value]);
                    }
                    
                    if (empty($res)) {
                        self::$errors[] = ['error'=>"Error: reference $value is not defined."];
                        break;
                    }

                    $value = $res[0]->id;
                }
                else if ($key === 'translations') {
                    $value = is_array($value) ? json_encode($value) : $value;
                }

                $set[] = "$key=:$key";
                $p[':'.$key] = $value;
            }
            
            $set = implode(',', $set);
            $p[':moduleKey'] = $moduleKey;
            $p[':keyNameId'] = $v['key_name_id'];

            $rows = Mbase2Database::query("UPDATE mbase2.module_variables SET $set WHERE id=(SELECT id from mbase2.module_variables_vw WHERE 
            module_name=:moduleKey AND variable_name=:keyNameId) returning id",$p);

            if (empty($rows)) {
                self::$errors[] = ['error'=>$v['key_name_id'].": undefined variable"];
            }
        }
    }

    public static function importDefaultVariables($moduleKey, $filter = null, $override = null, $listKey='modules') {
        $variables = Mbase2Utils::DEFAULT_VARIABLES;

        if (!empty($override)) {
            foreach ($variables as &$variable) {
                if (isset($override[$variable['key_name_id']])) {
                    if (isset($override[$variable['key_name_id']]['required'])) {
                        $variable['required'] = $override[$variable['key_name_id']]['required'];
                    }
                }
            }
            unset($variable);
        }

        if (!empty($filter)) {
            $filtered = [];
            foreach($variables as $variable) {
                if (in_array($variable['key_name_id'], $filter)) {
                    $filtered[] = $variable;
                }
            }
            
            $variables = $filtered;
        }

        self::importVariables($variables, $moduleKey, $listKey);
        
    }

    public static function importVariables($variables, $moduleKey = null, $listKey = 'modules', $defaultValues = []) {
        $moduleKey = self::getModuleKey($moduleKey);

        $moduleId = self::catchQuery("SELECT id from mbase2.code_list_options_vw WHERE key=:moduleKey and list_key=:list_key",[':moduleKey' => $moduleKey,':list_key'=>$listKey]);

        if (empty($moduleId)) {
            self::$errors[] = ['error'=>"Error: module id for $moduleKey is not defined."];
            return false;
        }

        $moduleId = $moduleId[0]['id'];
            
        $weight = 1;

        foreach ($variables as $inx => $v) {

            if (!Mbase2Utils::isAssoc($v)) {
                /**
                 * variable is defined in a short form
                 * [key,[props]]
                */
                $av = [];
                $keyDataTypeId = null;
                $ref = null;

                if (!isset($v[1])) $v[1] = [];

                if (!isset($v[1]['key_data_type_id'])) {
                    $def = self::getVariableDefinition($v[0]);
                    if (isset($def['key_data_type_id'])) {
                        $keyDataTypeId = $def['key_data_type_id'];
                        if (isset($def['ref'])) $ref = $def['ref'];
                    }
                }
                else {
                    $keyDataTypeId = $v[1]['key_data_type_id'];
                    $ref = $v[1]['ref'];
                }

                if (is_null($keyDataTypeId)) {
                    self::$errors[] = ['error'=>"Data type is not defined"];
                    continue;
                }

                $av = [
                    'key_name_id' => $v[0],
                    'key_data_type_id' => $keyDataTypeId
                ];

                if (!is_null($ref)) {
                    $av['ref'] = $ref;
                }
                
                $v = array_merge($v[1], $av);
                
            }

            $skip = false;
            foreach (['key_name_id', 'key_data_type_id'] as $keyName) {
                if (!isset($v[$keyName])) {
                    if ($keyName === 'key_data_type_id') {
                        $tmpr = Mbase2Database::query("SELECT data_type from mbase2.module_variables_vw where 
                            module_id=:module_id and variable_name=:key_name_id",[':module_id'=>$moduleId, ':key_name_id'=>$v['key_name_id']]);

                        if (!empty($tmpr)) {
                            $v['key_data_type_id'] = $tmpr[0]['data_type'];
                            continue;
                        }
                        
                    }
                    self::$errors[] = ['error'=>"$keyName is not defined in variable definition provided."];
                    $skip = true;
                    break;
                } 
            }

            if ($skip) continue;

            $skip = false;

            foreach (['data_types' => 'key_data_type_id'] as $codeListLabel => $keyNameId) {
                $tmpr = Mbase2Database::query("SELECT id from mbase2.code_list_options_vw WHERE key = :key and list_key=:list_key", 
                [
                    ':key' => $v[$keyNameId],
                    ':list_key' => $codeListLabel
                ]);

                if (empty($tmpr)) {
                    self::$errors[] = ['error'=>"'{$v[$keyNameId]}' is not defined in '$codeListLabel' code list."];
                    $skip = true;
                    break;
                }

                $v[str_replace('key_','',$keyNameId)] = $tmpr[0]['id'];
            }

            if ($skip) continue;

            $ref = null;

            if (in_array($v['key_data_type_id'], ['table_reference_array', 'table_reference'])) {
                if (!isset($v['ref']) && isset(self::$tableReferences[$v['key_name_id']])) $v['ref'] = self::$tableReferences[$v['key_name_id']];
                if (isset($v['ref'])) {
                    
                    $idFromTheCodeListSQL = "SELECT ID FROM mbase2.code_list_options_vw where key=:tname and list_key='referenced_tables'";
                    
                    if (!is_array($v['ref'])) {
                        $ref = Mbase2Database::query("SELECT id from mbase2.referenced_tables WHERE id = ($idFromTheCodeListSQL)", [':tname' => $v['ref']]);
                    }
                    else {
                        self::addCodeListOption('referenced_tables', $v['ref']['referenced_table']);
                        $ref = self::addReferenceTable(
                            $v['ref']['referenced_table'],
                            $v['ref']['pkey'],
                            $v['ref']['schema'],
                            isset($v['ref']['label_key']) ? $v['ref']['label_key'] : 'label'
                        );
                    }
                    $ref = $ref[0]['id'];
                }
            }
            else if ($v['key_data_type_id']==='code_list_reference' || $v['key_data_type_id'] === 'code_list_reference_array') {
                $ref = self::catchQuery("SELECT id from mbase2.code_list_options where key = :key", [':key' => $v['ref']]);
                $ref = $ref[0]['id'];
            }

            $v['ref'] = $ref;

            foreach ($defaultValues as $cname=>$value) {
                if (!isset($v[$cname])) {
                    $v[$cname] = $value;
                }
            }

            foreach (['_batch_id', '_uname', '_genetics', '_location','_sample_entry_clerk_uid'] as $tmpKey) {
                if ($v['key_name_id'] === $tmpKey) {
                    $v['importable'] = false;
                }
            }

            foreach (['weight', 'weight_in_table', 'weight_in_popup', 'weight_in_import'] as $tmpKey) {
                if (!isset($v[$tmpKey])) {
                    $v[$tmpKey] = $inx;
                }
            }

            $insert = ['module_id' => $moduleId];

            foreach(Mbase2Utils::MODULE_VARIABLES as $keyName) {
                if (isset($v[$keyName])) {
                    $insert[$keyName] = in_array($keyName, ['translations']) && is_array($v[$keyName]) ? json_encode($v[$keyName]) : $v[$keyName];
                }
            }

            $cnames = implode(',',array_keys($insert));
            $values = ':'.implode(',:',array_keys($insert));
            $set = [];

            $action = 'NOTHING';

            foreach(array_keys($insert) as $tmpKey) {
                if (in_array($tmpKey, ['module_id', 'key_name_id'])) continue;
                $set[] = "$tmpKey = :{$tmpKey}";
            }

            if (count($set)>0) {
                $set = implode(',', $set);
                $action = "UPDATE SET $set";
            }

            self::catchQuery("INSERT INTO mbase2.module_variables ($cnames) VALUES ($values)
                ON conflict (module_id, key_name_id) DO $action;", Mbase2Utils::parameters($insert)
            );
        }
    }
}
