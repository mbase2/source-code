<?php
class Mbase2Api {
  private $result_type = PGSQL_ASSOC;
  private $args = [];
  private $json_flags = JSON_NUMERIC_CHECK;
  private $table = '';
  private $result = [];
  private $statusCode = 200;
  private $request_type = 'GET';
  private $input = [];
  private $uid = null;  //user ID
  private $storagePath = [];

  function __construct($request, $query_parameters = [], $options = [], $echo = true, $result_type = PGSQL_ASSOC) {
    
    $storagePath = $options['storagePath'];
    $this->uid = $options['uid'];

    if (empty($storagePath)) $storagePath = [];

    foreach(['public', 'private'] as $pathKey) {
      $this->storagePath[$pathKey] = isset($storagePath[$pathKey]) ? $storagePath[$pathKey] : sys_get_temp_dir();
    }
    
    $this->result_type = $result_type;

    $this->args = $args = explode('/', $request);

    $this->request_type = $_SERVER['REQUEST_METHOD'];

    Mbase2Database::$uid = $this->uid;

    Mbase2Files::$storagePath = $this->storagePath;
    
    try {
      if ($args[0] === 'code_lists') {
        $this->table = 'mbase2.code_lists';
        $this->run();

        if (isset($_GET['m'])) {
          require_once(__DIR__.'/filters/CodeListsOutputFilter.php');
          new CodeListsOutputFilter($this->input, $this->result, $_GET['m']);
        }
      }
      else if ($args[0] === 'globals') {
        $this->result = [
          'language' => Mbase2Drupal::getCurrentLanguageCode()
        ];
      }
      else if ($args[0] === 'aggregate_defs') {
        $this->table = 'mbase2.aggregate_defs';
        $this->run();
      }
      else if ($args[0] === 'test') {
        
      }
      else if ($args[0] === 'mbase2call') {
        require_once(__DIR__.'/Mbase2ApiCallableFunctions.php');
        $this->result = call_user_func(["Mbase2ApiCallableFunctions", $args[1]],$args);
      }
      else if ($args[0] === 'attributes') {
        $this->table = 'mbase2.attributes';
        $this->run();
      }
      else if ($args[0] === 'dmg-admin-editor') {
        $this->parseInputParameters();
        $this->result = Mbase2Utils::dmgAdminEditor($args[1], $this->input, $this->request_type);
      }
      else if ($args[0] === 'export') {
        $module = $args[1];

        $tableNameArgsIndex = 1;
        $exportOptions = [];

        if ($module === 'dmg' && count($args)>2) {
          if (in_array($args[2], ['objects', 'monthly-reports', 'dmg_objects_by_oe'])) {
            $exportOptions[] = $args[2];
          }
          else {
            return;
          }
          $tableNameArgsIndex = 2;
        }

        $this->table = 'mb2data.'.$module.'_vw';
        $dbFunctionParameters = $this->run($tableNameArgsIndex, TRUE);
        
        require_once(__DIR__.'/Mbase2Export.php');
        Mbase2Export::exportModuleData($module, $dbFunctionParameters, false, $exportOptions);
        
        return;
      }
      else if ($args[0] === 'general-export') {
        $module = $args[1];

        $this->table = 'mb2data.'.$module.'_vw';
        $dbFunctionParameters = $this->run(1, TRUE);
        
        require_once(__DIR__.'/Mbase2Export.php');
        Mbase2Export::exportModuleData($module, $dbFunctionParameters, true);
        
        return;
      }
      else if ($args[0] === 'dmg-indirect-claim') {
        $this->result = Mbase2Utils::createIndirectClaim($args[1]);
      }
      else if ($args[0] === 'dmg-parse-price-list') {
        $this->parseInputParameters();
        $this->result = Mbase2Utils::parsePriceList($args[1], $this->input);
      }
      else if ($args[0] === 'camelot') {
        $this->parseInputParameters();
        require_once(__DIR__.'/Mbase2Camelot.php');
        $camelotId = intval($args[1]);
        $camelot = new Mbase2Camelot($camelotId, $this->input);
        $camelot->import();

        require_once(__DIR__.'/Mbase2Import.php');
        $import = new Mbase2Import(null, null, 'ct', null, $this->uid, $camelotId, $this->input);
        $this->result = [
          'errors' => $import->errors,
          'numberOfInputRows' => $import->numberOfInputRows,
          'numberOfInsertedRows' => $import->numberOfInsertedRows,
          'numberOfUpdatedRows' => $import->numberOfUpdatedRows,
          'importType' => $import->importType
        ];
      }
      else if ($args[0] === 'mb2data') {
        $tname = $args[1];
        $this->table = 'mb2data.'.$tname;
        $this->run(1);

        require_once(__DIR__.'/filters/DmgOutputFilter.php');

        new DmgOutputFilter($this->input, $this->result, $tname);
      }
      else if ($args[0] === 'list_distinct_spatial_units') {
        $this->runExt('Mbase2Utils','getDistinctSpatialUnitsList',[$args[1]]);
      }
      else if ($args[0] === 'mbase2') {
        $tname = $args[1];
        $this->table = 'mbase2.'.$tname;
        $this->run(1);
      }
      else if ($args[0] === 'protected') {
        $key = $args[1];
        if (hash('sha256', $key) === '55fb79e33bf066603ff7f2169378437fc1bb4c216604ae91d27d65f756f04da5') {
          //require_once(__DIR__.'/Mbase2Export.php');
          //Mbase2Export::exportModuleData($args[2],$args[3]);
          return;
        }
        else {
          throw new Exception("Wrong key...");
        }
      }
      else if ($args[0] === 'code_list_options') {
        $this->table = $this->request_type === 'GET' ? 'mbase2.code_list_options_vw' : 'mbase2.code_list_options';
        $this->run();
      }
      else if ($args[0] === 'import_batches_vw') {
        $this->table = 'mbase2.import_batches_vw';
        $this->run();

        require_once(__DIR__.'/filters/ImportBatchesOutputFilter.php');
        new ImportBatchesOutputFilter($this->input, $this->result, $this->table);
      }
      else if ($args[0] === 'import_errors') {
        $this->table = 'mbase2.import_errors';
        $this->run();
      }
      else if ($args[0] === 'table_reference_values') {
        $this->table = 'mbase2.referenced_tables';
        $this->run();
        
        $this->result = Mbase2Utils::getReferencedTableValues($this->result);
      }
      else if ($args[0] === 'referenced_tables') {
        $this->table = 'mbase2.referenced_tables';
        $this->run();
      }
      else if ($args[0] === 'intersection-attributes') {
        $this->parseInputParameters();
        $this->result = Mbase2Database::getIntersectionAttributes($args[1], $this->input);
      }
      else if ($args[0] === 'get_spatial_units_intersecting_point') {
        $this->parseInputParameters();
        $this->result = Mbase2Utils::getSpatialUnitsIntersectingPoint($this->input);  
      }
      else if ($args[0] === 'module_references') {
        $nodef = false; 
        if (isset($args[3]) && $args[3] === 'nodef') {
          $nodef = true;
          unset($args[3]);
        }

        $this->parseInputParameters();

        $numericParameter = ':module_id';
        $keyParameter = ':module_id:code_list_options/key';

        $moduleId = null;

        if (isset($this->input[$numericParameter])) {
          $moduleId = $this->input[$numericParameter];
        }
        else if (isset($this->input[$keyParameter])) {
          $moduleKey = $this->input[$keyParameter];
          $res = Mbase2Database::query("SELECT id from mbase2.code_list_options_vw WHERE key=:moduleKey and list_key='modules'",[':moduleKey'=>$moduleKey]);
          $moduleId = Mbase2Database::fetchField($res, 'id');
        }

        $language = $this->getRequestArgumentsLanguage();

        $this->result = $this->moduleReferences($moduleId, $nodef, $language);
      }
      else if ($args[0] === 'get_data_source_pages') {
        $this->result = Mbase2Files::get_data_source_pages($args[1]);
      }
      else if ($args[0] === 'users') {

        if (!Mbase2Drupal::isAnyModuleAdmin()) throw new Exception("Insufficient privilege to modify the data.");
        
        $this->parseInputParameters();

        if (!in_array($this->request_type, ['POST', 'PUT'])) {
          throw new Exception("Only POST and PUT methods are allowed.");
        }

        $this->result = Mbase2Drupal::upsertUser($this->input);

      }
      else if ($args[0] === 'get_data_source_page_columns') {
        $this->result = Mbase2Files::get_data_source_page_columns($args[1], $args[2]);
      }
      else if ($args[0] === 'import' && $this->request_type === 'POST') {
        $this->parseInputParameters();
        require_once(__DIR__.'/Mbase2Import.php');

        $language = 'en';
        if ($args[count($args)-2] === 'language') {
          $language = $args[count($args)-1];
        } 

        $moduleName = $args[3];

        require_once(__DIR__.'/Mbase2UAC.php');
        $mbase2uac = new Mbase2UAC('Mbase2Import', $moduleName);
        $mbase2uac->check($this->request_type);
        
        $import = new Mbase2Import($args[1], $args[2], $moduleName, $this->input, $this->uid, null, null, $language);
        $this->result = [
          'errors' => $import->errors,
          'numberOfInputRows' => $import->numberOfInputRows,
          'numberOfInsertedRows' => $import->numberOfInsertedRows,
          'numberOfUpdatedRows' => $import->numberOfUpdatedRows,
          'importType' => $import->importType
        ];
      }
      else if ($args[0] === 'import-genotypes' && $this->request_type === 'POST') {
        require_once(__DIR__.'/Mbase2UAC.php');
        $mbase2uac = new Mbase2UAC('Mbase2ImportGenotypes', 'genotypes');
        $mbase2uac->check($this->request_type);
        require_once(__DIR__.'/Mbase2ImportGenotypes.php');
        $p = [];
        foreach(['upid', 'page1', 'page2', 'lab', 'cs'] as $key) {
          $p[$key] = isset($_GET[$key]) ? intval($_GET[$key]) : ''; 
        }

        $import = new Mbase2ImportGenotypes($p);

        $this->result = [
          $import->allelePageErrors,
          $import->genTypePageErrors,
          $import->importReport,
          $import->errorException,
          $import->batch_id
        ];
      }
      else if ($args[0] === 'template') {
        $this->table = 'mbase2.import_templates';
        $this->run();
      }
      else if ($args[0] === 'module_variables' || $args[0] === 'module_variables_vw') {
        $this->table = 'mbase2.'.$args[0];
        $this->run();

        /**
         * Translate variable names
         */
        if (isset($args[1]) && $args[1] === 'language') {
          $language = trim($args[2]);
          foreach ($this->result as &$row) {
            if (!empty($row['translations'])) {
              $translations = json_decode($row['translations']);
              if (!empty($translations) && isset($translations->$language)) {
                $row['t_name_id'] = $translations->$language;
              }
            }
          }
        }

        /**schema updates are currently not possible via GUI */
        /*
        if (in_array($this->request_type,['POST', 'PUT'])) {
          $moduleId = empty($this->result[0]) ? null : intval($this->result[0]['module_id']);
          if (!empty($moduleId)) {
            Mbase2Database::updateSchema($moduleId);
          }
        }*/

      }
      else if ($args[0] === 'uac') {
        $this->table = 'mb2data.'.$args[1];
        $this->run();
      }
      else if ($args[0] === 'mbase2_users') {
        $this->table = 'mbase2.mbase2_users';
        $this->run();
      }
      else if ($args[0] === 'modules') {
        $this->table = 'mbase2.modules';
        $this->run();
      }
      else if ($args[0] === 'schema-patch') {
        require_once(__DIR__.'/Mbase2SchemaPatches.php');
        if ($this->uid === 1) {
          if ($args[1] === 'run') {
            $this->result = Mbase2SchemaPatches::run($args[2], isset($args[3]) ? $args[3] : null);
          }
        }
        else {
          throw new Exception("Access denied.");
        }
      }
      else if ($args[0] === 'camelot-image-upload' && $this->request_type === 'POST') {
        $this->result = Mbase2Files::fileUploadFromCamelot($this->uid, $args[1], $args[2]);
      }
      else if ($args[0] === 'uploaded-files') {
        $this->runExt('Mbase2Files','getUploadedFiles', [
          $this->uid, 
          $args[1]==='private', 
          $args[2], 
          isset($args[3]) ? $args[3] : null, //offset
          isset($args[4]) ? $args[4] : null, //scope
          isset($args[5]) ? $args[5] : '', //limit
          isset($_GET['_ext']) ? $_GET['_ext'] : ''], $args[2]);
      }
      else if ($args[0] === 'drop-batch-data') {
        $this->runExt('Mbase2Import','dropBatchData',[$args[1]]);
      }
      else if ($args[0] === 'delete-genotypes') {
        $this->parseInputParameters();
        if (!isset($this->input[':ids']) || empty($this->input[':ids'])) {
          return;
        }
        $this->runExt('Mbase2Gensam','deleteGenotypes', [$this->input[':ids']], 'gensam');
      }
      else if ($args[0] === 'gensam-batch-update-uac') {
        $this->parseInputParameters();
        if (!isset($this->input[':ids']) || empty($this->input[':ids'])) {
          return;
        }
        
        $organisations = $this->input[':organisations'] ?? [];
        $users = $this->input[':users'] ?? [];

        $remove = $this->input[':remove'] ?? 0;
        $remove = intval($remove) > 0 ? true : false;
        
        $this->runExt('Mbase2Gensam','batchUpdateUac', [$this->input[':ids'], $organisations, $users, $remove], 'gensam');
      }
      else if ($args[0] === 'delete-gensam') {
        $this->parseInputParameters();
        if (!isset($this->input[':ids']) || empty($this->input[':ids'])) {
          return;
        }
        $this->runExt('Mbase2Gensam','deleteGensam', [$this->input[':ids']], 'gensam');
      }
      else if ($args[0] === 'download-allele-map') {
        require_once(__DIR__.'/Mbase2UAC.php');
        $mbase2uac = new Mbase2UAC('Mbase2ExportGenotypes', 'genotypes');
        $mbase2uac->check($this->request_type);
        require_once(__DIR__.'/Mbase2Export.php');
        Mbase2Export::alleleMap($args[1]);
        return;
      }
      else if ($args[0] === 'download-genotypes') { //batch data export
        require_once(__DIR__.'/Mbase2UAC.php');
        $mbase2uac = new Mbase2UAC('Mbase2ExportGenotypes', 'genotypes');
        $mbase2uac->check($this->request_type);
        require_once(__DIR__.'/Mbase2Export.php');
        Mbase2Export::genotypes($args[1]);
        return;
      }
      else if ($args[0] === 'export-allele-name-map') {
        $alab_id = $args[1] ?? null;
        if (!$alab_id) return;

        $this->runExt('Mbase2Export', 'exportAlleleNameMap', [
          $alab_id
        ], 'gensam');
      }
      else if($args[0] === 'export-genotypes')
      {
        require_once(__DIR__.'/Mbase2UAC.php');
        $mbase2uac = new Mbase2UAC('Mbase2ExportGenotypes', 'genotypes');
        $mbase2uac->check($this->request_type);
        require_once(__DIR__.'/Mbase2Export.php');
        $this->table = 'mb2data.gensam';
        $dbFunctionParameters = $this->run(1, TRUE);
        Mbase2Export::filteredGenotypesExport($dbFunctionParameters);
      }
      else if ($args[0] === 'uploaded-file') {
        
        $id = null;
        $file = null;

        $count = count($args);

        $thumbnail = false;

        if ($count === 3) {
          $id = $args[2];
        }
        else if ($count === 4) {
          $id = $args[2];
          if ($args[3] === 'thumbnail') {
            $thumbnail = true;
          }
        }
        else if ($count > 4) {
          $file = [
            'path' => $args[2].'/'.$args[3].'/'.$args[4]
          ];

          if (isset($args[5])) {
            $file['imageType'] = trim($args[5]);
          }

        }
        else {
          $this->result = null;
          return;
        }

        if (isset($args[5]) && !is_null($file)) $file['type'] = $args[5];

        $this->runExt('Mbase2Files', $this->request_type === 'DELETE' ? 'deleteUploadedFile' : 'getUploadedFile', [$this->uid, $args[1]==='private', $id, $file, $thumbnail]);
        return; //don't echo the result - getUploadedFile function handles the echo in appropriate format
      }
      else if ($args[0] === 'storage') {
        //$args[1] ... public or private
        if (count($args)!==2) {
          throw new Exception("Wrong number of parameters.");
        }
        $this->runExt('Mbase2Files', 'getPublicStorageFile', [$args[1]]);
      }
      else if ($args[0] === 'update_schema') {
        $this->result = Mbase2Database::updateSchema(intval($args[1]));
      }
      else if ($args[0] === 'file-upload' && $this->request_type === 'POST') {
        $subfolder = isset($args[2]) ? $args[2] : 'mbase2';
        $this->result = Mbase2Files::fileUpload($this->uid, $args[1], false, true, $subfolder);
      }
      else if ($args[0] === 'file-delete' && $this->request_type === 'DELETE') {
        $this->parseInputParameters();
        //$this->result = Mbase2Files::fileDelete($this->input, $this->uid);
      }
      else if ($args[0] === 'batch') {
        $this->parseInputParameters();
        $urls = json_decode($this->input[':urls']);
        $r = [];
        unset($_GET[':urls']);
        foreach($urls as $url0) {
          $_GET = [];
          $pos = strpos($url0, '?');
          $url = $url0;
          
          if ($pos !== false) {
            $url = substr($url0,0,$pos);
            $keyValues = explode('&', substr($url0, $pos));
            foreach ($keyValues as $keyValue) {
              $pos1 = strpos($keyValue, '=');
              if ($pos1 !== false) {
                $_GET[substr($keyValue,1,$pos1-1)] = substr($keyValue, $pos1+1);
              }
            }
          }

          $api = new Mbase2Api($url,$query_parameters, $options, false);
          $r[] = $api->getResult();
        }
        $this->result = $r;
      }
    }
    catch (Exception $e) {
      $this->statusCode = 400;
      
      if ($echo) {
        http_response_code(400);
      }
      
      $this->result = ['err' => $e->getMessage() ];
    }

    if ($echo) {
      header("Access-Control-Allow-Origin: *");
      header('Content-Type: application/json; charset=UTF-8');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
      header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With, Cache-Control");
      echo json_encode($this->result, $this->json_flags);
    }
  }

  /**
   * @param {boolean} $nodef //if nodef then returns only array [module_id => table-name]
   */

  public function moduleReferences($moduleId, $nodef = false, $language = null) {
    
    $ids = [];

    Mbase2Database::moduleReferences($moduleId, $ids);
    
    array_unshift($ids, $moduleId);

    $res = $this->moduleVariables($ids, $language);

    $result = array_flip($ids);
    foreach($res as $r) {
      $result[$r['module_id']] = $r['key_module_id'];
    }

    if ($nodef === false) {

      $result = array_flip($result);

      foreach($result as &$r) {
        $r=[];
      }
      unset($r);

      foreach($res as $r) {
        $result[$r['key_module_id']][] = $r;
      }
    }
    
    return $result;
  }

  /**
   * Helper function for calling Mbase2Database::select('mbase2.module_variables',$p)
   */
  private function moduleVariables($moduleIds = [], $language=null) {
    $p=[];
    if (!empty($moduleIds)) $p=[':module_id' => $moduleIds];
    return Mbase2Database::select('mbase2.module_variables', $p, $language);
  }

  private function getRequestArgumentsLanguage() {
    if ($this->request_type === 'GET') {
      $inx = in_array('language',$this->args);
      if ($inx!==false) return $this->args[$inx+1];
    }
    return null;
  }

  public function getResult() {
    return $this->result;
  }

  public function getStatusCode() {
    return $this->statusCode;
  }

  private function runExt($class, $function, $functionParameters=[], $moduleName=null) {
    require_once(__DIR__.'/Mbase2UAC.php');
    $table = "$class.$function";  //this is not actually a table but serves as a key for subsequent actions
    $mbase2uac = new Mbase2UAC($table, $moduleName, $functionParameters);
    
    $mbase2uac->check($this->request_type);

    if (in_array($class, ['Mbase2Import', 'Mbase2Export', 'Mbase2Gensam'])) {
      require_once(__DIR__."/$class.php");
    }

    $this->result = call_user_func_array([$class, $function], $functionParameters);
    
    require_once(__DIR__.'/Mbase2Triggers.php');
    new Mbase2Triggers(false, $this->request_type, $table, $this->result, $this->input, $this->json_flags);
  }

  private function run($tableNameArgsIndex = 0, $onlyReturnFunctionParameters = FALSE) {
    
    $this->parseInputParameters();

    $id = null;
    $language = null;

    if (isset($this->args[$tableNameArgsIndex + 1])) {
      if ($this->args[$tableNameArgsIndex + 1] === 'language') {
        $language = @$this->args[$tableNameArgsIndex + 2];
        $id = @$this->args[$tableNameArgsIndex + 3];
      }
      else {
        $id = $this->args[$tableNameArgsIndex + 1];
      }
    }

    $dbFunctionName = null;
    $dbFunctionParameters = null;

    if ($this->request_type === 'GET') {
      $dbFunctionName = 'select';
      if (!is_null($id)) {
        $id = $this->input[':id'] = $id[0]==='(' ? explode(',', trim($id, "()")) : intval($id);
      }

      $params = $this->input;
      require_once(__DIR__.'/Mbase2DataQuery.php');
      $isGrid = isset($params[':__grid']) && intval($params[':__grid'])===1 ? true : false;
      if (isset($params[':__count'])) {
        foreach([':__filter', ':__sfilter', ':__pfilter'] as $key) {
          if (isset($params[$key])) {
            unset($params[$key]);
          }
        }
      }
      $select = Mbase2DataQuery::SelectColumns($this->table, $isGrid);
      Mbase2DataQuery::AdditionalFilters($this->table, $params, $select, $isGrid);
      $subquery = Mbase2DataQuery::GeometryDataSubquery($this->table, $params, $isGrid, $select);
      
      $dbFunctionParameters = [$this->table, $params, $language, $subquery, $isGrid, $this->result_type];
    }
    else if ($this->request_type === 'POST') {
      $dbFunctionName = 'insert';
    }
    else if ($this->request_type === 'PUT') {
      if (!is_null($id)) {
        $id = $this->input[':id'] = intval($id);
        $dbFunctionName = 'update';
      }
    }
    else if ($this->request_type === 'DELETE') {
      if (!is_null($id)) {
          $id = $this->input[':id'] = intval($id);
          $dbFunctionName = 'delete';
      }
    }

    require_once(__DIR__.'/Mbase2UAC.php');

    $mbase2uac = new Mbase2UAC($this->table, null, $dbFunctionParameters);

    $mbase2uac->check($this->request_type, $id);

    if (!is_null($dbFunctionName)) {
      
      $dbFunctionParameters = is_null($dbFunctionParameters) ? [$this->table, $this->input, $language, $this->result_type] : $dbFunctionParameters;
      if ($onlyReturnFunctionParameters) {
        return $dbFunctionParameters;
      }

      require_once(__DIR__.'/Mbase2Triggers.php');

      $triggerBefore = new Mbase2Triggers(true, $this->request_type, $this->table, $this->result, $this->input, $this->json_flags);

      $this->json_flags = $triggerBefore->json_flags;

      if (!empty($triggerBefore->updateParameters)) {
        foreach($triggerBefore->updateParameters as $key => $value) {
          $dbFunctionParameters[1][$key] = $value;
        }
      }

      if (!empty($triggerBefore->removeParameters)) {
        foreach($triggerBefore->removeParameters as $key ) {
          unset($dbFunctionParameters[1][$key]);
        }
      }

      if (!empty($triggerBefore->dbFunctionName)) {
        $dbFunctionName = $triggerBefore->dbFunctionName;
      }

      $this->result = call_user_func_array(['Mbase2Database', $dbFunctionName], $dbFunctionParameters);
      
      new Mbase2Triggers(false, $this->request_type, $this->table, $this->result, $this->input, $this->json_flags);
    }
    
  }  
  
  public function getJsonFlags() {
    return $this->json_flags;
  }

  private function parseInputParameters() {
    if (array_key_exists('json_flags', $_GET)) {
      $this->json_flags = intval($_GET['json_flags']);
      unset($_GET['json_flags']);
    }

    if ($this->request_type === 'GET') {
      $input = $_GET;
      foreach($input as $key => $value) {
        if ($key[0] !== ':') {
          unset($input[$key]);
        }
      }
      $this->input = $input;
    }
    else {
      $input = file_get_contents('php://input');
      $this->input = json_decode($input, true);
    }

  }
}
