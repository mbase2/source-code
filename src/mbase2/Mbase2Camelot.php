<?php

class Mbase2Camelot {
    //{"types":{"_species_name":"column","_licence_name":"fixed","event_date":"column","_batch_id":"column","photos":"column","sighting_quantity":"column","trap_station_name":"column","session_name":"column","event_time":"column","sex":"column","individual_name":"column","life_stage":"column","position":"column","hair_trap":"column","notes":"column"},"values":{"_species_name":"46","event_date":"7","_batch_id":null,"photos":"9","sighting_quantity":"26","trap_station_name":"59","session_name":"62","event_time":"7","sex":"64","individual_name":"66","life_stage":"65","position":"68","hair_trap":"69","notes":"67","_licence_name":"10"}}
    public $input = null;
    private $camelotId = null;
    private $_licence_name = null;
    private $header = null;

    function __construct($camelotId, $input) {
        if (count($input) < 2) {    //we have to have at least header and one row of data 
            return;
        }
        
        if (empty($camelotId)) return;

        $res = Mbase2Database::query("SELECT _licence_name FROM mb2data.ct_camelot_sources WHERE id=:id", [':id' => $camelotId]);
        $this->_licence_name = Mbase2Database::fetchField($res, '_licence_name');

        $this->camelotId = $camelotId;

        $this->input = $input;
        $this->header = $input[0];
        unset($this->input[0]);
    }
    
    public function import() {
        $this->upsertIndividuals();
        $this->upsertSurveys();
        $this->upsertCameras();
        $this->upsertSites();
        $this->upsertTraps();
        //$this->upsertSessions();
    }

    private function updateNeeded($id, $indices, $camelotTableValues, $row) {
        foreach($indices as $cname=>$inx) {
            if (isset($camelotTableValues[$id]) && isset($camelotTableValues[$id][$cname]) && $camelotTableValues[$id][$cname] !== $row[$inx]) return true; 
        }
        return false;
    }

     /**
     * $tname <string> table name to update
     * $tableId <object> ['mbase2_column_name_id' => 'full export column name id"], e.g.: ['camelot_trap_station_id'=>"Trap Station ID"]
     * $columns <object> column maps for other columns than $tableId
     * $references <object> ['column_name' => [['camelot_id' => ['id'=>'mbase2_id']]] ...]
     */
    
    private function upsert($tname, $tableId, $columns, $references = []) {

        $indices = [];
        $set = [];
        $insert = [];

        foreach($columns as $c=>$cname) {
            $indices[$c] = array_search($cname, $this->header);
            $insert[":$c"] = $c;
            $set[] = "$c = :$c";
        }

        $set = implode(',', $set);
        $insertCnames = implode(',', array_values($insert));
        $insertPlaceholders = implode(',', array_keys($insert));

        $tableIdInx = array_search(array_values($tableId)[0], $this->header);
        $tableIdCname = array_keys($tableId)[0];
        
        $tableValues = Mbase2Database::query("SELECT * FROM mb2data.$tname WHERE camelot_id=:camelot_id", [':camelot_id' => $this->camelotId]);

        $camelotTableValues = [];

        foreach ($tableValues as $tableValue) {
            if (!empty($tableValue['camelot_id'])) {
                $camelotTableValues[$tableValue['camelot_id'].'_'.$tableValue[$tableIdCname]] = $tableValue;
            }
        }

        $updated = [];

        foreach($this->input as $row) {
            $id = $this->camelotId.'_'.$row[$tableIdInx];
            if (isset($updated[$id]) && $updated[$id] === true) continue;
            
            $p = [
                ':camelot_id' => $this->camelotId,
                ':_licence_name' => $this->_licence_name,
                ":$tableIdCname" => $row[$tableIdInx]
            ];

            foreach($columns as $c => $cname) {
                
                $value = trim($row[$indices[$c]]);

                if (isset($references[$c]) && isset($references[$c][$value])) {
                    $value = $references[$c][$value]['id'];
                }
                
                $p[":$c"] = $value === '' ? null : $value;
            }
            
            if (isset($camelotTableValues[$id])) {
                if ($this->updateNeeded($id, $indices, $camelotTableValues, $row)) {
                    Mbase2Database::query("UPDATE mb2data.$tname SET _licence_name=:_licence_name, $set WHERE camelot_id=:camelot_id AND $tableIdCname=:$tableIdCname",$p);
                }
            }
            else {
                Mbase2Database::query("INSERT INTO mb2data.$tname (camelot_id, _licence_name, $tableIdCname, $insertCnames) VALUES (:camelot_id, :_licence_name, :$tableIdCname, $insertPlaceholders)",$p);
            }

            $updated[$id] = true;
        }
    }

    private function upsertSurveys() {
        $this->upsert('ct_surveys', ['camelot_survey_id'=>"Survey ID"], ['survey_name' => "Survey Name"]);
        return;
        $camelot_survey_id = array_search("Survey ID", $this->header);
        $survey_name = array_search("Survey Name", $this->header);

        $surveys = Mbase2Database::query("SELECT camelot_id, camelot_survey_id, survey_name FROM mb2data.surveys WHERE camelot_id=:camelot_id", [':camelot_id' => $this->camelotId]);

        $camelotSurveys = [];

        foreach ($surveys as $survey) {
            if (!empty($survey['camelot_id'])) {
                $camelotSurveys[$survey['camelot_id'].'_'.$survey['camelot_survey_id']] = $survey;
            }
        }

        $updated = [];

        foreach($this->input as $row) {
            $id = $this->camelotId.'_'.$row[$camelot_survey_id];
            if (isset($updated[$id]) && $updated[$id] === true) continue;
            
            $p = [
                ':survey_name'=>$row[$survey_name],
                ':camelot_id' => $this->camelotId,
                ':camelot_survey_id' => $row[$camelot_survey_id]
            ];

            if (isset($camelotSurveys[$id])) {
                if ($camelotSurveys[$id]['survey_name'] !== $row[$survey_name]) {
                    Mbase2Database::query("UPDATE mb2data.surveys SET survey_name=:survey_name WHERE camelot_id=:camelot_id AND camelot_survey_id=:camelot_survey_id",$p);
                }
            }
            else {
                Mbase2Database::query("INSERT INTO mb2data.surveys (camelot_id, camelot_survey_id, survey_name) VALUES (:camelot_id, :camelot_survey_id, :survey_name)",$p);
            }

            $updated[$id] = true;
        }
    }

    private function updateCodeList($labelKey, $inx) {

        $res = Mbase2Database::query("SELECT clv.id, clv.key FROM mbase2.code_list_options_vw clv WHERE list_key=:label_key", [':label_key' => $labelKey]);
        
        $codeList = array_column($res, null, 'key');

        list($listId) = Mbase2Database::query("SELECT id FROM mbase2.code_lists where label_key = :label_key",[':label_key' => $labelKey]);
        $listId = $listId['id'];
        
        foreach($this->input as $row) {
            $key = trim($row[$inx]);
            if (empty($key)) continue;
            if (!isset($codeList[$key])) {

                $res = Mbase2Database::query("INSERT INTO mbase2.code_list_options (list_id, key, list_key) values (:list_id, :key, :list_key) RETURNING *", [
                    ':list_id' => $listId,
                    ':key' => $key,
                    ':list_key' => $labelKey
                ]);

                $codeList[$key] = $res[0];
            }
        }

        return $codeList;
    }

    private function upsertIndividuals() {

        $individualInx = array_search("Individual name", $this->header);
        $speciesNameInx = array_search("Species Common Name", $this->header);
        $sexInx = array_search("Sex", $this->header);

        $res = Mbase2Database::query("select id, key from mbase2.code_list_options_vw clo where list_key='species'");
        foreach($res as &$row) {
            $row['key'] = strtolower($row['key']);
        }
        unset($row);

        $speciesNames = array_column($res, null, 'key');

        $res = Mbase2Database::query("select id, slug as key from laravel.sex_list");
        foreach($res as &$row) {
            $row['key'] = strtolower($row['key']);
        }
        unset($row);

        $sexOptions = array_column($res, null, 'key');

        $res = Mbase2Database::query("SELECT id, individual_name from mb2data.individuals");
        foreach($res as &$row) {
            $row['individual_name'] = strtolower($row['individual_name']);
        }
        unset($row);

        $individuals = array_column($res, null, 'individual_name');

        foreach($this->input as $row) {
            $individual = trim($row[$individualInx]);
            $speciesName = trim($row[$speciesNameInx]);
            $sexOption = trim($row[$sexInx]);
            if (empty($individual)) continue;
            if (!isset($individuals[strtolower($individual)])) {
                $key = strtolower($speciesName);
                $speciesNameId = null;
                $sexId = null;

                if (isset($speciesNames[$key])) {
                    $speciesNameId = $speciesNames[$key]['id'];
                }

                $key = strtolower($sexOption);
                if (isset($sexOptions[$key])) {
                    $sexId = $sexOptions[$key]['id'];
                }

                Mbase2Database::query("INSERT INTO mb2data.individuals (individual_name, _species_name, sex) values (:individual_name, :_species_name, :sex)", 
                [
                    ':individual_name' => $individual, 
                    ':_species_name' => $speciesNameId,
                    ':sex' => $sexId
                ]);

                $individuals[strtolower($individual)] = true;
            }
        }
    }

    private function upsertCameras() {
        
        $cameraMakeCodeList = $this->updateCodeList('camera_make_options', array_search("Make", $this->header));
        $cameraModelCodeList = $this->updateCodeList('camera_model_options', array_search("Model", $this->header));

        $this->upsert('ct_cameras', ['camelot_camera_id' => "Camera ID"], [
            'camera_name' => "Camera Name", 	
            'camera_make' => "Make",
            'camera_model' => "Model",
        ],[
            'camera_make' => $cameraMakeCodeList,
            'camera_model' => $cameraModelCodeList
        ]);
    }

    private function upsertSites() {
        $this->upsert('ct_sites', ['camelot_site_id'=>"Site ID"], ['site_name' => "Site Name", 'area' => 'Site Area (km2)']);
    }

    private function upsertSessions() {
        $this->upsert('sessions', ['camelot_session_id'=>"Trap Station Session ID"], [
            'trap_station_name' => 'Trap Station ID',
            'survey_name' => 'Survey ID',
            'camera_name' => 'Camera ID',
            'start_date' => 'Session Start Date',
            'end_date'=> 'Session End Date'
        ],
        [
            'trap_station_name' => $this->getReferenceTable('trap_stations', 'camelot_trap_station_id'),
            'survey_name' => $this->getReferenceTable('surveys', 'camelot_survey_id'),
            'camera_name' => $this->getReferenceTable('cameras', 'camelot_camera_id')
        ]
        );
    }

    private function updateLocations() {
        $res = Mbase2Database::query("SELECT l.id, camelot_trap_station_id, public.st_x(geom) lon, public.st_y(geom) lat FROM mb2data.ct_trap_stations ts, mb2data.ct_locations l WHERE camelot_id = :camelot_id and ts._location_reference = l.id", [':camelot_id' => $this->camelotId]);
        $res = array_column($res, null, 'camelot_trap_station_id');

        $tsidInx = array_search('Trap Station ID', $this->header);
        $latInx = array_search('Camelot GPS Latitude', $this->header);
        $lonInx = array_search('Camelot GPS Longitude', $this->header);

        $locations = [];

        foreach($this->input as $row) {
            $tsid = $row[$tsidInx];
            
            if (!isset($locations[$tsid])) {

                $point = isset($res[$tsid]) ? $res[$tsid] : null;

                if (empty($point)) {
                    
                        $a = Mbase2Database::query("INSERT INTO mb2data.ct_locations (geom) values (public.ST_SetSRID(public.ST_MakePoint(:lon,:lat),4326)) RETURNING id",
                            [
                                ':lon' => $row[$lonInx],
                                ':lat' => $row[$latInx]
                            ]
                        );
                        $locations[$tsid] = ['id' => $a[0]['id']];
                }
                else {
                    $rowLon = $row[$lonInx];
                    $rowLat = $row[$latInx];

                    $lat = $point['lat'];
                    $lon = $point['lon'];
                    
                    if (abs($rowLon - $lon) > 1e-6 || abs($rowLat - $lat) > 1e-6) {
                        
                        Mbase2Database::query("UPDATE mb2data.ct_locations SET geom = (public.ST_SetSRID(public.ST_MakePoint(:lon,:lat),4326)) WHERE id=:id",
                            [
                                ':lon' => $rowLon,
                                ':lat' => $rowLat,
                                ':id' => $point['id']
                            ]
                        );
                    }

                    $locations[$tsid] = ['id' => $point['id']];
                }
            }
        }

        return $locations;
    }

    private function getReferenceTable($tableName, $camelotKey) {
        $rtable = [];
        $res = Mbase2Database::query("SELECT * FROM mb2data.$tableName WHERE camelot_id=:camelot_id", [':camelot_id' => $this->camelotId]);
        foreach ($res as $r) {
            $rtable[$r[$camelotKey]] = ['id' => $r['id']];
        }
        return $rtable;
    }

    private function upsertTraps() {
        
        $sites = $this->getReferenceTable('ct_sites', 'camelot_site_id');
        $surveys = $this->getReferenceTable('ct_surveys', 'camelot_survey_id');
        $locations = $this->updateLocations();

        $this->upsert('ct_trap_stations', ['camelot_trap_station_id'=>"Trap Station ID"], [

            'trap_station_name' => "Trap Station Name",
            'site_name' => 'Site ID',
            'survey_name' => "Survey ID",
            '_location_reference' => 'Trap Station ID'

        ], 
        ['_location_reference' => $locations,
        'site_name' => $sites,
        'survey_name' => $surveys
        ]);
    }
}
