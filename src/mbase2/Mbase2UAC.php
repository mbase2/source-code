<?php

final class Mbase2UAC {

    private $currentUserRoles = [];
    private $tname = null;
    private $module = '';
    private $protected = true;  //protected data is accessible to logged users only
    private $arguments = [];

    /**
     * In constructor we check if user is sys admin or module admin
     */

    function __construct($tname, $moduleName = null, $arguments = [], $protected = true) {
        $this->protected = $protected;
        $this->arguments = $arguments;
        $roles = $this->currentUserRoles = Mbase2Drupal::currentUserRoles();
        $this->tname = $tname;

        if (is_null($moduleName)) {
            $moduleName = Mbase2Utils::getModuleNameFromTableName($this->tname);
        }

        $moduleName = Mbase2Utils::getModuleNameFromTableName($moduleName);

        $this->module = $moduleName;
    }

    /**
     * Here you can set specific rules to access table data or functions
     */

    private function specificRules($action='GET', $id=null) {

        $defaults = [
            'mb2data.dmg_affectees' => [
                'roles' => ['mbase2-dmg-admins','mbase2-dmg-editors']
            ]
        ];

        $rval = [];

        if ($action === 'GET') {
            $rval = [
                'mbase2.mbase2_users' => [
                    'roles' => Mbase2Utils::mbase2Roles('admins')
                ],
                'mbase2.lov_vw' => true, 
                'mbase2.luo_vw' => true, 
                'mbase2.luo_lov_vw' => true,
                'mb2data.ct_view_file_properties' => true,
                'mb2data.individuals' => [
                    'roles' => array_merge(Mbase2Utils::mbase2Roles('admins'), Mbase2Utils::mbase2Roles('editors'))
                ],
                'Mbase2Files.getUploadedFiles' => [
                    'roles' => array_merge(Mbase2Utils::mbase2Roles('admins'), Mbase2Utils::mbase2Roles('curators'), Mbase2Utils::mbase2Roles('editors'))
                ],
                'mb2data.ct_camelot_sources' => [
                    'roles' => array_merge(['mbase2-ct-admins', 'mbase2-ct-editors', 'mbase2-ct-curators'])
                ],
                'Mbase2ExportGenotypes' => [
                    'roles' => [
                        'mbase2-gensam-admins', 
                        'mbase2-gensam-editors',
                        'mbase2-gensam-curators']
                ],
                'Mbase2Files.getPublicStorageFile' => true,
                'Mbase2Files.getUploadedFile' => true   //handled in function
            ];
        }
        else if ($action === 'POST' || $action === 'PUT' || $action === 'DELETE') {
            $rval = [
                'mb2data.ct_camelot_sources' => [
                    'roles' => array_merge(['mbase2-ct-admins', 'mbase2-ct-editors', 'mbase2-ct-curators'])
                ],
                'mb2data.individuals' => [
                    'roles' => array_merge(['mbase2-ct-admins'])
                ],
                'mbase2.code_list_options' => [
                    'roles' => Mbase2Utils::mbase2Roles('admins')
                ],
                'mbase2.modules' => [   //only module admin can change module data
                    'roles' => (function($id) {
                        $res = Mbase2Database::query("SELECT * from mbase2.modules_vw WHERE id=:id", [':id'=>$id]);
                        if (empty($res)) return [];
                        $moduleKey = $res[0]['module_key'];
                        return ["mbase2-{$moduleKey}-admins"];
                    })($id)
                ],
                'mbase2.module_variables' => [   //only module admin can change module data
                    'roles' => (function($id) {
                        $res = Mbase2Database::query("SELECT module_name from mbase2.module_variables_vw WHERE id=:id", [':id'=>$id]);
                        if (empty($res)) return [];
                        $moduleKey = $res[0]['module_name'];
                        return ["mbase2-{$moduleKey}-admins"];
                    })($id)
                ],
                'Mbase2Import' => [
                    'roles' => (function($moduleKey){
                        $moduleRoles = [];
                        foreach(['admins','editors','curators'] as $group) {
                            $moduleRoles[] = "mbase2-{$moduleKey}-{$group}";
                        }
                        return $moduleRoles;
                    })($this->module)
                ],
                'Mbase2ImportGenotypes' => [
                    'roles' => [
                        'mbase2-gensam-admins', 
                        'mbase2-gensam-editors',
                        'mbase2-gensam-curators']
                ]
            ];
        }

        return array_merge($defaults, $rval);
    }

    private function isModuleAdmin($module) {
        return isset($this->currentUserRoles["mbase2-{$module}-admins"]);
    }

    private function delete($id=null) {
        $tname = $this->tname;
        $module = $this->module;

        /**
         * module admin can delete the data - if the module for the table is known
         */
        if ($this->isModuleAdmin($module)) return true;

        $currentUserId = Mbase2Drupal::getCurrentUserData()['uid'];

        if (empty($currentUserId)) throw new Exception('You have to be logged in to delete records.');

        /**
         * user can edit its own data even if not module admin
         */

        if ($tname === 'mb2data.dmg') {
            $res = Mbase2Database::query("SELECT _uid,_claim_status from mb2data.dmg WHERE id=:id",[':id' => $id]);
            $uid = Mbase2Database::fetchField($res,'_uid');

            if ($uid == $currentUserId) {
                return true;
            }
        }
        else if ($tname === 'Mbase2Files.deleteUploadedFile') {
            $id = intval($this->arguments[2]);
            $res = Mbase2Database::query("SELECT * from mbase2.uploads u where u.id = :id", [':id'=>$id]);
            if (empty($res)) throw new Exception('Missing database file record.');

            $uid = Mbase2Database::fetchField($res,'uid');

            /**
             * module admin can delete the file
             */
            $module = Mbase2Database::fetchField($res,'subfolder');

            if ($this->isModuleAdmin($module)) return true;

            if ($uid == $currentUserId) {
                return true;
            }
        }
        else if ($tname === 'Mbase2Import.dropBatchData') {
            /**
             * user can delete batch data if it owns the row or if it is current row module editor or admin
             */
            $tname = 'mbase2.import_batches';
            $uid = 'user_id';

            if (empty($id)) {
                $id = intval($this->arguments[0]);
            }

            $res = Mbase2Database::query("SELECT * from $tname WHERE id=:id",[':id' => $id]);

            if (!empty($res)) {
                $res = $res[0];
                $resUid = $res['user_id'];

                if ($resUid == $currentUserId) {
                    return true;
                }

                if ($this->isModuleAdmin($res['key_module_id'])) return true;
            }
        }
        else if ($tname === 'Mbase2Gensam.deleteGensam' || $tname === 'Mbase2Gensam.deleteGenotypes' || $tname === 'Mbase2Gensam.batchUpdateUac') {
            if ($this->hasGensamEditRole()) { //permissions to delete individual records are handled in Mbase2Gensam, here I check only if user is entitled to edit the records
                return true;
            }
        }
        else {
            $uid = '_uname';
            
            if (in_array($tname, ['mb2data.interventions_events', 'mb2data.gensam'])) $uid = '_uid';   //some tables store user id in _uid field and some in _uname

            $res = Mbase2Database::query("SELECT $uid from $tname WHERE id=:id",[':id' => $id]);
            $uid = Mbase2Database::fetchField($res,$uid);

            if ($uid == $currentUserId) {
                return true;
            }
        }

        throw new Exception('Insufficient privilege to delete the data.');
    }

    private function checkSpecificRules($action='GET', $id = null) {

        $tname = $this->tname;
        $specificRules = $this->specificRules($action, $id);

        if (isset($specificRules[$tname])) {
            
            if (is_callable($specificRules[$tname])) {
                if ($specificRules[$tname]() === true) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else if (isset($specificRules[$tname]['roles'])) {
                $allowedRoles = $specificRules[$tname]['roles'];
                foreach (array_keys($this->currentUserRoles) as $role) {
                    if (in_array($role, $allowedRoles)) return true;
                }
            }
            
            if ($specificRules[$tname] === true) {
                return true;
            }
            else {
                throw new Exception("You are not allowed to access this table or function ($tname).");
            }
     
        }
    }

    private function hasGensamEditRole() {
        return isset($this->currentUserRoles["mbase2-gensam-admins"]) || isset($this->currentUserRoles["mbase2-gensam-editors"]);
    }

    private function linkGeneticSamplesPermissions($action) {
        if ($action !== 'GET') return false;
        
        if ($this->tname === 'mb2data.dmg' || $this->tname === 'mb2data.dmg_vw') {
            if (isset($this->arguments) && is_array($this->arguments) && isset($this->arguments[1]) && isset($this->arguments[1][':__select'])) {
                $e = explode(',', $this->arguments[1][':__select']);
                $e = array_flip($e);

                foreach(['id','_claim_id','_genetic_samples','_dmg_start_date','_location_data'] as $allowed) {
                    if (!isset($e[$allowed])) return false;
                }

                if ($this->hasGensamEditRole()) {
                    return true;
                }
            }
        }
        else if ($this->tname === 'mb2data.sop' || $this->tname === 'mb2data.sop_vw') {
            if (isset($this->arguments) && is_array($this->arguments) && isset($this->arguments[1]) && isset($this->arguments[1][':__select'])) {
                $e = explode(',', $this->arguments[1][':__select']);
                $e = array_flip($e);

                foreach(['id','_genetics','event_date','_location_data'] as $allowed) {
                    if (!isset($e[$allowed])) return false;
                }

                if ($this->hasGensamEditRole()) {
                    return true;
                }
            }
        }

        return false;
    }

    private function select() {

        $tname = $this->tname;

        if (strpos($tname, 'mbase2.') !== FALSE || strpos($tname, 'mbase2_ge.') !== FALSE) return true;  //you are allowed to select data from mbase2 and mbase2_ge schema

        /**
         * check if user has reader or consumer or editor role on module
         */

        $roles = $this->currentUserRoles;

        $module = $this->module;

        if (!empty($module)) $module = "mbase2-$module";

        $len = strlen($module);

        foreach (array_keys($roles) as $role) {
            if (substr($role, 0, $len) === $module) return true;
        }

        if (isset($_GET[':__grid']) && intval($_GET[':__grid']) === 1) return true;

        return false;
    }

    private function post() {
        $roles = $this->currentUserRoles;
        $needles = [$this->module.'-admins', $this->module.'-editors'];

        foreach (array_keys($roles) as $role) {
            foreach ($needles as $needle) {
                if (strstr($role, $needle)) return true;
            }
        }

        if ($this->tname==='mbase2.import_templates') {
            return true;
        }

        throw new Exception("Insufficient privilege to post the data.");
    }

    private function put($id) {
        return $this->post();
    }

    /**
     * here we check if access was previously granted, if specific rules apply and call generel access function for the specific request type
     * @param id is passed from Mbase2Api->run method after being parsed from the DELETE request URL arguments, Mbase2Api->runExec is calling Mbase2UAC constructor with called function arguments
     */

    public function check($requestType, $id=null) {

        if (Mbase2Drupal::isAdmin($this->module)) {
            return true;
        }

        //you have to be logged in to get any private data
        if ($this->protected) {
            if (!Mbase2Drupal::userIsLoggedIn()) {
                throw new Exception("You have to be logged in to access this data.");
            }
        }
        
        if ($this->checkSpecificRules($requestType, $id)) return true;

        $accessGranted = false;

        if ($requestType === 'GET') {
            $accessGranted = $this->select();

            if ($accessGranted !== TRUE) {
                $accessGranted = $this->linkGeneticSamplesPermissions($requestType);
            }
        }
        else if ($requestType === 'DELETE') {
            $accessGranted = $this->delete($id);
        }
        else if ($requestType === 'POST') {
            $accessGranted = $this->post();
        }
        else if ($requestType === 'PUT') {
            $accessGranted = $this->put($id);
        }
        else {
            throw new Exception("Unsupported request type.");    
        }

        if ($accessGranted === TRUE) return TRUE;
        
        throw new Exception("Insufficient privilege to $requestType the data.");
    }
}
