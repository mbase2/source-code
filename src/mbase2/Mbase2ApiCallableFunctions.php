<?php

final class Mbase2ApiCallableFunctions {

    public static function get_camelot_image_names($a) {
        
        $camelotId = $a[count($a)-1];

        return array_column(\DB::select("select distinct(u.file_name) fn from mbase2.uploads u
        left join
        mb2data.ct ct 
            on ct.photos ?? u.file_hash 
        where ct.id is not null
        and ct._batch_id in (select id from mbase2.import_batches WHERE (data->>'camelotId')::integer = :camelot_id)
        ", [':camelot_id'=>$camelotId]), 'fn');
    }

    public static function get_tree($a) {
        return Mbase2Utils::get_tree($a);
    }
}