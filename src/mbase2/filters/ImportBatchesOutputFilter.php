<?php
require_once(__DIR__.'/../Mbase2OutputFilter.php');
class ImportBatchesOutputFilter extends Mbase2OutputFilter {
    private $roles = [];

    function filter() {
        $roles = $this->roles = Mbase2Drupal::currentUserRoles();
        
        if (isset($roles['administrator'])) return;

        $filtered = [];

        foreach ($this->result as $row) {
            $module = $row['key_module_id'];
            
            if ($module === 'genotypes') {
                $module = 'gensam';
            }

            if ($this->accessGranted($module) || $this->accessGranted(Mbase2Utils::getModuleNameFromTableName($module))) {
                $filtered[] = $row;
            }
        }

        $this->result = $filtered;
    }

    private function accessGranted($module) {
        $roles = $this->roles;
        return isset($roles["mbase2-{$module}-admins"]) || isset($roles["mbase2-{$module}-editors"]) || isset($roles["mbase2-{$module}-curators"]);
    }
}
