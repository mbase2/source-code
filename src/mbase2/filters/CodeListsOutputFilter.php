<?php
require_once(__DIR__.'/../Mbase2OutputFilter.php');
class CodeListsOutputFilter extends Mbase2OutputFilter {

    function filter() {

        $rows = \DB::select("select reference from mbase2.module_variables_vw mvv where module_name like :m and data_type like 'code_list_reference%'",[':m'=>"%{$this->tname}%"]);

        $filter = array_column($rows, null, 'reference');

        $out=[];

        foreach($this->result as $item) {
            
            $labelKey = $item['label_key'];

            if (isset($filter[$labelKey])) {
                $out[] = $item;
            }
        }

        $this->result = $out;

    }
}
