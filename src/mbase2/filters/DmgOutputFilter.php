<?php

require_once(__DIR__.'/../Mbase2OutputFilter.php');

class DmgOutputFilter extends Mbase2OutputFilter{

  protected $module = 'dmg';

  function whenToFilter() {
    if (in_array($this->tname,['dmg', 'dmg_vw', 'dmg_deputies_vw'])) {
      if (isset($this->input[':__select'])) return false; //this is needed to reconstruct claim_id when viewing claims from other OE
      return true;
    }
    return false;
  }

    function filter() {
      $_oe_id = Mbase2Utils::dmg_get_deputy_oeid();
      //if (Mbase2Utils::empty($_oe_id)) throw new Exception('You have to be a deputy to access this data.');
      if (Mbase2Utils::empty($_oe_id)) {
        $oeid = '';
        $_oe_id = null;
      }
      else {
        $oeid = str_pad($_oe_id, 2, '0', STR_PAD_LEFT);
      }
      
      if ($this->tname === 'dmg' || $this->tname === 'dmg_vw') {
        foreach($this->result as &$row) {
          if (!isset($row['_claim_id']) || !isset($row['_dmg_objects'])) continue;
          $claimId = $row['_claim_id'];
          if (!empty($claimId)) {
            if (substr($claimId, 0, 2) !== $oeid) {
              $row['_affectees'] = '["*******"]';

              $row['__access'] = 404;

              $objekti = json_decode($row['_dmg_objects']);
              if (!empty($objekti)) {
                foreach ($objekti as &$objekt) {
                  $objekt->nodes[0]->text = '<b>Oškodovanec</b>&nbsp**********';
                  $objekt->data->affectee = null;
                }
              }

              unset($objekt);
              $row['_dmg_objects'] = $objekti;
            }
          }
        }
        unset($row);
      }
      else if ($this->tname === 'dmg_deputies_vw') {
        $filtered = [];
        foreach($this->result as $row) {
          if ($row['_oe_id' ] == $_oe_id) {
            $filtered[] = $row;
          }
        }
        $this->result = $filtered;
      }

    }
      }
