<?php
final class Mbase2Database {
  public static $uid = null;

  public static function convertNamedParametersToPositional($sql, $params) {
    $parNames = array_keys($params);

    $positions = [];

    foreach ($parNames as $i => $par) {
      $pos = '\$'.($i+1);
      $positions[] = $pos;

      $sql = preg_replace('/'.$par.'\b/', $pos, $sql); //The \b matches a word boundary. Parentheses are not counted as part of a word.
      /**
       * e.g.: 
       * command:
       *  echo preg_replace('/:a1\b/','X',':a1) :a10');
       * output: 
       *  X) :a10
       */
    }

    return [$sql, array_values($params)];
  }

  public static function insertAndReturnNumberOfInsertedRows($sql, $params=[]) {
    $sql = rtrim($sql,';');
    return \DB::selectOne("WITH rows AS ( $sql RETURNING 1)
                    SELECT count(*) c from rows;", $params)->c;
  }

  public static function createIndex($schema, $tableName, $fieldName) {
    \DB::statement("CREATE INDEX IF NOT EXISTS idx_$fieldName ON $schema.$tableName($fieldName);");
  }

  /**
   * Helper function to get the field from resultset array
   * @param {array} $res resultset as returned from Mbase2Database::query
   * @field {string} $field field name to retrieve
   * $inx {integer} row index to retrieve
   */

  public static function fetchField ($res, $field, $inx=0) {
    if (empty($res)) return null;

    if (isset($res[$inx])) {

      $r = $res[$inx];

      if (array_key_exists($field, $r)) {
        return $r[$field];
      }
      
    }

    return null;
  }

  /**
   * Returns intersection attributes (country code, hunting ground, ...)
   */

  public static function getIntersectionAttributes($tname, $point, $latLon = []) {

    $sql = "";

    $select = '*';
    $whereAdditional = '';
    $postProcess = null;

    if ($tname === '_spatial_units') {
      $select = 'slug, name';
      $tname = 'spatial_units_by_date';
      $whereAdditional = "AND valid_to = '2100-01-01'";
      
      $postProcess = function ($res) {
        $keyMap = [
          'SI-OB' => 'ob_uime',
          'SI-OE' => 'oe_ime',
          'SI-LUO' => 'luo_ime',
          'SI-LOV' => 'lov_ime',
          'CNTRY' => 'cntr_id'
        ];
        $out = [];
        
        foreach ($res as $row) {
          $key = $row['slug'];
          $value = $row['name'];
          $out[$keyMap[$key] ?? $key] = $value;
        }

        return [$out];
      };
    }

    if (empty($latLon)) {
      $sql = "SELECT $select FROM mbase2_ge.$tname WHERE public.ST_Intersects(geom,public.ST_SetSRID(public.ST_MakePoint(:lon, :lat),4326)) $whereAdditional";
      $latLon = $point;
    }
    else{
      $sql = "SELECT $select FROM mbase2_ge.$tname WHERE public.ST_Intersects(geom,$point) $whereAdditional";
    }

    $res = self::query($sql, $latLon);

    if ($postProcess != null) {
      $res = $postProcess($res);
    }

    return $res;
  }

  /**
   * Connects directly to the database - currently not used as we are connecting to the Drupal db connection
   */
  public static function connect($host = 'localhost', $port = '5432', $dbname = 'portal_mbase_org', $mb2sys = 'mbase2',  $username = 'mbase_org', $password = 'some_password') {
    self::$conn = pg_connect("host={$host} port={$port} dbname={$dbname} user={$username} password={$password}");
  }

  public static function query($sql, $params = [], $result_type = PGSQL_ASSOC) {

    if (function_exists('db_query')) {
      
      $result = db_query($sql, $params);

      if ($result_type === PGSQL_ASSOC) {
        $result_type = PDO::FETCH_ASSOC;
      }
      else if ($result_type === PGSQL_NUM) {
        $result_type = PDO::FETCH_NUM;
      }
      else if ($result_type === PGSQL_BOTH) {
        $result_type = PDO::FETCH_BOTH;
      }

      return $result->fetchAll($result_type);

    }
    else {
      $result = DB::select($sql, $params);
      foreach ($result as &$row) {
        $row = (array)$row;
      }
    }
    

    return $result;
  }

  public static function tableExists($schema, $tname) {

    $res = self::query("SELECT EXISTS (
      SELECT FROM pg_tables
      WHERE  schemaname = :schema
      AND    tablename  = :tname
      );", [':schema'=>$schema, ':tname'=>$tname]);
      
      return $res[0]['exists'];
  }

  public static function updateSchema($tname, $schema = 'mb2data', $key_name_id = null, $variables = null) {
    
    $mb2sys = 'mbase2';
    
    if (empty($tname)) throw new Exception("Missing table name.");

    $res = is_null($variables) ? self::query("SELECT * FROM $mb2sys.module_variables_vw WHERE module_name=:tname" , [':tname' => $tname]) : $variables;

    if (empty($res)) return $res;

    self::query("CREATE TABLE IF NOT EXISTS $schema.$tname (id serial PRIMARY KEY)");
    
    /*
    $constraints = self::getConstraints($schema, $tname);
    
    foreach ($constraints as $constraint) {
      self::query("ALTER TABLE $schema.$tname DROP CONSTRAINT $constraint");
    }
    */

    $locationReferenceColumn = null;

    $tableReferences = null;

    foreach ($res as $cdef) {
      
      if ($cdef['dbcolumn'] === false) {
        continue;
      }

      $cname = $cdef['variable_name'];

      if (!is_null($key_name_id) && $cname !== $key_name_id) {
        continue;
      }

      $unique = $cdef['unique_constraint'] ? "UNIQUE" : "";
      //$required = $cdef['required'] ? "NOT NULL" : "";
      $required = '';
      $type = $cdef['data_type'];
      $default = $cdef['default_value'] ? " DEFAULT {$cdef['default_value']}" : '';
      $references = "";

      if (($pos = strpos($cname, '.')) !== FALSE) {
        $cname = substr($cname, $pos+1);
      }

      if ($type === 'code_list_reference') { 
        $type = 'integer';
        $references = "references $mb2sys.code_list_options(id)";
      }
      else if ($type === 'table_reference') {
        $type = 'integer';

        if (is_null($tableReferences)) {
          $tableReferences = self::query("SELECT * from mbase2.referenced_tables_vw");
          $tableReferences = array_column($tableReferences, null, 'id');
        }

        if (isset($tableReferences[$cdef['ref']])) {
          $ref = $tableReferences[$cdef['ref']];
          
          if (self::tableExists($ref['schema'], $ref['key'])) {
            $references = "references {$ref['schema']}.{$ref['key']}({$ref['pkey']})";
          }
        }
      }
      else if ($type === 'location_reference') {
        $locationReferenceColumn = $cname;
        $type = 'integer';
        //$references = "references mb2data._locations(id)";  //geom tables are now splitted between modules
      }
      else if ($type === 'image_array') {
        $type = 'jsonb';
        //create index on mb2data.ct using gin ((photos)); //https://stackoverflow.com/questions/19925641/check-if-a-postgres-json-array-contains-a-string
      }
      else if ($type === 'image') {
        $type = 'text';
      }
      else if ($type ==='table_reference_array') {
        $type = 'jsonb';
      }
      else if ($type === 'code_list_reference_array') {
        $type = 'jsonb';
      }
      else if ($type === 'email') {
        $type = 'text';
      }
      else if ($type === 'phone') {
        $type = 'text';
      }
      else if ($type === 'json') {
        $type = 'jsonb';
      }
      else if ($type === 'location_data_json') {
        $type = 'jsonb';
      }

      if (!self::columnExists($schema, $tname, $cname)) {
        if ($type === 'location_geometry') {
          self::quiet_query("ALTER TABLE $schema.$tname
            ADD COLUMN $cname
            geometry(Geometry,4326);");

          self::quiet_query("CREATE INDEX idx_{$tname}_{$cname} ON $schema.$tname USING gist ($cname);");
        }
        else {
          self::quiet_query("ALTER TABLE $schema.$tname ADD COLUMN $cname $type $unique $required $default");
          !empty($references) && self::quiet_query("ALTER TABLE $schema.$tname ADD FOREIGN KEY ($cname) $references");  
        }
        
        continue;
      }

      if (empty($default)) {
        self::quiet_query("ALTER TABLE $schema.$tname ALTER COLUMN $cname DROP DEFAULT;");
      }
      else {
        self::quiet_query("ALTER TABLE $schema.$tname ALTER COLUMN $cname SET $default;");
      }

      self::quiet_query("ALTER TABLE $schema.$tname ALTER COLUMN $cname TYPE $type USING $cname::$type");    
      
      //change uniqueness
      if (empty($unique)) {
        self::query("ALTER TABLE $schema.$tname DROP CONSTRAINT IF EXISTS {$tname}_{$cname}_unique_constraint");
      }
      else {
        self::quiet_query("ALTER TABLE $schema.$tname ADD CONSTRAINT {$tname}_{$cname}_unique_constraint UNIQUE ($cname)");
      }
      
      //nullable
      if (empty($required)) {
        self::query("ALTER TABLE $schema.$tname ALTER COLUMN $cname DROP NOT NULL;");
      }
      else {
        self::quiet_query("ALTER TABLE $schema.$tname ALTER COLUMN $cname SET NOT NULL");
      }
      
      //references
      $fkey = self::getForeignKey($schema, $tname, $cname);
      if (!empty($fkey)) {
        self::query("ALTER TABLE $schema.$tname DROP CONSTRAINT $fkey");
      }

      if (!empty($references)) {
        self::quiet_query("ALTER TABLE $schema.$tname ADD FOREIGN KEY ($cname) $references");
      }
      
    }

    self::quiet_query("set schema 'mbase2';
      SET search_path TO public, mbase2;

      DROP MATERIALIZED VIEW IF EXISTS mbase2.code_list_options_fkeys;
      CREATE MATERIALIZED VIEW mbase2.code_list_options_fkeys AS
      SELECT * FROM (
      SELECT conrelid::regclass AS FK_Table
            ,CASE WHEN pg_get_constraintdef(c.oid) LIKE 'FOREIGN KEY %' THEN substring(pg_get_constraintdef(c.oid), 14, position(')' in pg_get_constraintdef(c.oid))-14) END AS FK_Column
            ,CASE WHEN pg_get_constraintdef(c.oid) LIKE 'FOREIGN KEY %' THEN substring(pg_get_constraintdef(c.oid), position(' REFERENCES ' in pg_get_constraintdef(c.oid))+12, position('(' in substring(pg_get_constraintdef(c.oid), 14))-position(' REFERENCES ' in pg_get_constraintdef(c.oid))+1) END AS PK_Table
            ,CASE WHEN pg_get_constraintdef(c.oid) LIKE 'FOREIGN KEY %' THEN substring(pg_get_constraintdef(c.oid), position('(' in substring(pg_get_constraintdef(c.oid), 14))+14, position(')' in substring(pg_get_constraintdef(c.oid), position('(' in substring(pg_get_constraintdef(c.oid), 14))+14))-1) END AS PK_Column
      FROM   pg_constraint c
      JOIN   pg_namespace n ON n.oid = c.connamespace
      WHERE  contype = 'f' 
      AND (nspname = 'mbase2' OR nspname = 'mb2data')
      AND pg_get_constraintdef(c.oid) LIKE 'FOREIGN KEY %'
      ORDER  BY pg_get_constraintdef(c.oid), conrelid::regclass::text, contype DESC
      )a where PK_Table = 'code_list_options';");

    return $res;
  }

  public static function quiet_query($sql, $params=[]) {
    try{
      self::query($sql, $params);
    }
    catch (Exception $e) {
    }
  }

  public static function getForeignKey($schema, $tname, $cname) {
    //https://stackoverflow.com/questions/1152260/postgres-sql-to-list-table-foreign-keys
    $res = self::query("SELECT conname,
      pg_catalog.pg_get_constraintdef(r.oid, true) as condef
      FROM pg_catalog.pg_constraint r
      WHERE r.conrelid = '$schema.$tname'::regclass AND 
      conname like '%_{$cname}_%' AND
      r.contype = 'f' ORDER BY 1");

    if (empty($res)) return null;

    return $res[0]['conname'];
  }

  public static function columnExists($schema, $tname, $cname) {
    $res = self::query("SELECT column_name 
      FROM information_schema.columns 
      WHERE table_schema = :schema AND
      table_name=:tname AND 
      column_name=:cname;",
      [
        ':schema'=>$schema,
        ':tname'=>$tname,
        ':cname'=>$cname
      
      ]);
    
    return !empty($res);
  }

  /**
   * function paramsToConditions(&$params)
   * Returns WHERE statement and turns $params input argument to appropriate placeholder array of values
   * Example:
   * $params = [':list_id' => [6,1,2,3,4]];
   * 
   * Returns: list_id IN (:__mbase2_list_id_0,:__mbase2_list_id_1,:__mbase2_list_id_2,:__mbase2_list_id_3,:__mbase2_list_id_4)
   * 
   * and changes $params to:
   * Array (
    * [:__mbase2_list_id_0] => 6
    * [:__mbase2_list_id_1] => 1
    * [:__mbase2_list_id_2] => 2
    * [:__mbase2_list_id_3] => 3
    * [:__mbase2_list_id_4] => 4
    * )
   */
  public static function paramsToConditions(&$params, $cnamePreposition='') {
    
    $conditions = [];

    foreach ($params as $key => $value) {
      $pos = strpos($key, ':', 1);  //if $pos !== false we have a foreign key condition

      if (is_string($value) && $value[0] === '(') {
        $value = explode(',', trim($value,"()"));
      }

      $conditionOperator = '=';
      $conditionPlaceholder = $conditionPlaceholderKey = $pos === false ? $key : substr($key, 0, $pos);
      $placeholderValues = [];

      $key0 = trim($conditionPlaceholder, ':');

      //api accepts multiple values in parantheses
      
      if (is_array($value)) {
        foreach ($value as $i => $v) {
          $placeholderValues[':__mbase2_'.str_replace('.','_',$key0).'_'.$i] = $v;
        }
        $conditionOperator = ' IN ';
        $conditionPlaceholder = '('.implode(',', array_keys($placeholderValues)).')';
      }

      if ($pos === false) {
        $conditions[] = $cnamePreposition.$key0.$conditionOperator.$conditionPlaceholder; //label_id=:label_id : label_id IN (:__mbase2_1, :__mbase2_2, ...)
      }
      else { //foreign key value
        $p = explode('/', substr($key, $pos+1));
        $subq = "(SELECT id FROM mbase2.{$p[0]} WHERE {$p[1]} $conditionOperator $conditionPlaceholder)";
        $conditions[] = $cnamePreposition.$key0.$conditionOperator.$subq;
        $params[$conditionPlaceholderKey] = $value;  //this assignment will be unset if $value is array (because then $placeholderValues will are set)
        unset($params[$key]);
      }

      if (count($placeholderValues) > 0) {
        foreach ($placeholderValues as $placeholder => $placeholderValue) {
          $params[$placeholder] = $placeholderValue;
        }
        unset($params[$conditionPlaceholderKey]);
      }
    }

    return implode('AND ', $conditions);
  }

  public static function paramsToUpdateSet(&$params) {
    $cols = [];

    foreach (array_keys($params) as $key) {
      
      if ($key === ':id') continue;

      $value = &$params[$key];

      if ($key === ':_location_data') {
        if (isset($value['lat']) && isset($value['lon'])) {
          $point = self::makePostgisPoint($params);
          $cols[] = "_location = $point";
          $params[':lat'] = $value['lat'];
          $params[':lon'] = $value['lon'];
        }
        
        $cols[] = '_location_data = :_location_data';
        $value = json_encode($value);
        
      }
      else {
        $cols[] = trim($key,':').'='.$key;
        if (is_array(($value))) $value = json_encode($value);
      }
    }
    unset($value);
    return implode(',', $cols);
  }

  private static function getLocationsTable(&$location) {
    $locationsTable =  '_locations';
      
    if (isset($location['_locations_table'])) {
      $locationsTable = $location['_locations_table'];
      unset($location['_locations_table']);
    }

    return $locationsTable;
  }

  private static function updateLocationReferenceGeometry(&$params) {
    $res = null;
    $rlocation = null;
    if (isset($params[':_location_reference'])) {
      $location = $params[':_location_reference'];
      $locationsTable = self::getLocationsTable($location);
      $point = self::makePostgisPoint($params);
      if (isset($location['id']) && strlen($location['id'])) {
        $res = self::query("UPDATE mb2data.$locationsTable 
          SET geom = $point
          WHERE id = :id RETURNING id, public.st_asgeojson(geom) geom",[
            ':id'=>$location['id'],
            ':lat'=>$location['lat'],
            ':lon'=>$location['lon']
          ]);
      }
      else {
        $res = self::query("INSERT INTO mb2data.$locationsTable (location_accuracy, geom) 
            VALUES (:location_accuracy, $point RETURNING id, public.st_asgeojson(geom) geom", 
            [
              ':location_accuracy' => $location['location_accuracy'],
              ':lat' => $location['lat'],
              ':lon' => $location['lon']
          ]);
      }
      $rlocation = $res[0]['geom'];
    }

    if (!is_null($res)) {
      $id = $res[0]['id'];
      $params[':_location_reference'] = $id;
      return ['id' => $id,'geom'=>$rlocation];
    }

    return null;
  }

  public static function delete($tname, $params = [], $language = null, $result_type = PGSQL_ASSOC) {

    if (empty($params)) return [];

    $id = $params[':id'];

    if (empty($id)) return [];
    
    $res = self::query("DELETE FROM {$tname} WHERE id=:id RETURNING *", $params, $result_type);

    return $res;
  }

  /**
   * Helper function to store uid in the right column name (_uid, _uname or custum defined)
   */

  private static function setUID(&$key, &$params, &$value) {
    if (is_string($value) && $value[0]===':') {
      $params[$value] = self::$uid;
      unset($params[$key]);
      $key = $value;
    }
    else {
      $value = self::$uid;
    }
  }

  public static function update($tname, $params = [], $language = null, $result_type = PGSQL_ASSOC, $columnHoldingKey = 'id') {

    if (empty($params)) return [];

    $params = self::removeTableNameFromParams($params);

    $id = $params[':'.$columnHoldingKey];

    if (empty($id)) return [];

    foreach ($params as $key => &$value) {
      if (!is_array($value) && !is_bool($value) && strlen($value)===0) {
        $params[$key] = null;
      }
      else if (self::recordUserName($key, $tname)) {
        
        self::setUID($key, $params, $value);

      }
    }

    unset($value);

    $g = self::updateLocationReferenceGeometry($params);

    $cols = self::paramsToUpdateSet($params);

    if (empty($cols)) throw new Exception("Columns for update are not defined.");

    $columnHoldingKey = str_ireplace([' ','*','delete','select','update','insert'], '', $columnHoldingKey);
    
    $res = self::query("UPDATE {$tname} SET $cols WHERE $columnHoldingKey=:$columnHoldingKey RETURNING *", $params, $result_type);

    if (!is_null($g)) {
      $res[0]['geom'] = $g['geom'];
    }

    return $res;
  }

  private static function makePostgisPoint(&$params, $key=':_location_data') {
    
    $point = 'public.ST_SetSRID(public.ST_MakePoint(:lon,:lat),4326)';
    
    if (isset($params[$key])) {
      if (isset($params[$key]['crs'])) {
        $crs = intval($params[$key]['crs']);
        if ($crs != 4326) {
          $point = "public.ST_Transform(public.ST_SetSRID(public.ST_MakePoint(:lon,:lat),$crs),4326)";
        }
      }
    }

    if (isset($params[':_location_data']) && !isset($params[':_location_data']['spatial_request_result'])) {
      $srr = [];
      $latLon = [
        ':lat' => $params[':_location_data']['lat'],
        ':lon' => $params[':_location_data']['lon']
      ];
      
      $su=self::getIntersectionAttributes('_spatial_units', $point, $latLon);

      $params[':_location_data']['spatial_request_result'] = empty($su) ? [] : $su[0];
    }

    return $point;
  }

  public static function insert($tname, $params = [], $language = null, $result_type = PGSQL_ASSOC) {
    
    if (empty($params)) return [];

    $params = self::removeTableNameFromParams($params);

    $multipleInsert = [];

    foreach ($params as $key => $value) {
      if (is_numeric($key) && is_array($value)) {
        try {
          $multipleInsert[] = self::insert($tname, $value, $language, $result_type);
        }
        catch (Exception $e) {
          ;
        }
      }
      else {
        if (!is_array($value) && !is_bool($value) && strlen($value)===0) {
          $params[$key] = null;
        }
      }
    }

    if (!empty($multipleInsert)) return $multipleInsert;

    $cnames = [];

    $geom = null;

    foreach (array_keys($params) as $key) {

      $value = &$params[$key];
      
      if ($key === ':_location_reference') {
        
        $locationsTable = self::getLocationsTable($value);

        if (strlen($value['id'])>0) {
          $value = $value['id'];
        }
        else {
          $lat = $value['lat'];
          $lon = $value['lon'];
          if (empty($lat) || empty($lon)) {
            unset($params[$key]);
            continue;
          }

          $point = self::makePostgisPoint($params);

          $res = self::query("INSERT INTO mb2data.$locationsTable (location_accuracy, geom) 
            VALUES (:location_accuracy, $point) RETURNING id, public.st_asgeojson(geom) geom", 
            [
              ':location_accuracy' => $value['location_accuracy'],
              ':lat' => $lat,
              ':lon' => $lon
          ]);
          
          $value = $res[0]['id'];
        }
      }
      else if (self::recordUserName($key, $tname)) {
        self::setUID($key, $params, $value);
      } 
      else if ($key === ':_location_data' || $key === ':_location') {
        
        if (isset($value['lat']) && isset($value['lon'])) {
          $geom = self::makePostgisPoint($params, $key);
          $params[':lat'] = $value['lat'];
          $params[':lon'] = $value['lon'];
        }

        if ($key === ':_location' && isset($params[$key])) {
          unset($params[$key]);
        }
      }
    
      if (is_array(($value))) $value = json_encode($value);
      $cnames[trim($key, ':')] = true;
    }

    unset($value);

    $valueKeys = [];

    if (isset($cnames['_location'])) unset($cnames['_location']);

    $cnames = array_keys($cnames);

    foreach ($cnames as $cname) {
      $valueKeys[] = ':'.$cname;
    }

    if (!is_null($geom)) {
      $valueKeys[] = $geom;
      $cnames[] = '_location';
    }

    $cnames = implode(',', $cnames);
    $valueKeys = implode(',', $valueKeys);
    
    $res = self::query("INSERT INTO {$tname} ($cnames) VALUES ($valueKeys) RETURNING *", $params, $result_type);   

    return $res;
  }

  /**
   * Returns true if provided uid or uname value should be ignored and replaced by self::$uid
   */
  private static function recordUserName($key, $tname) {
    return ($key === ':_uid' || $key === ':_uname' || $key === '_uid' || $key === '_uname') && !in_array($tname, ['mb2data.dmg_deputies']);
  }

  /**
   * If column name has dot in its name, the dot and what preceeds the dat is removed.
   */

  private static function removeTableNameFromParams($params) {
    $pret = [];
    foreach ($params as $key => $value) {
      $pos = strpos($key, '.');
      if ($pos !== FALSE) {
        $key = ($key[0] === ':' ? ':' : '').substr($key, $pos+1);
      }
      $pret[$key] = $value;
    }
    return $pret;
  }

  private static function getFilterParameters($key, &$params) {
    $filter = [];
    
    if (isset($params[$key])) {
      $value = trim($params[$key]);
      $filter = json_decode($value);
      if (is_null($filter)) {
        throw new Exception("Error in filter parameters.");
      }
      unset($params[$key]);
    }
    
    return $filter;
  }

  /**
   * If subquery is defined the selection is made on subquery
   * runQuery: if false then only prepared statement is returned
   */

  public static function select($tname, $params = [], $language = null, $subquery = null, $isGrid=false, $result_type = PGSQL_ASSOC, $onlyPrepare = false, $bindParams = []) {
    $params = self::removeTableNameFromParams($params);

    $filter = self::getFilterParameters(':__filter', $params);
    $pfilter = self::getFilterParameters(':__pfilter', $params);

    if (isset($params[':__grid'])) {
      unset($params[':__grid']);
    }

    $spatialFilter = self::getFilterParameters(':__sfilter', $params);

    $cols = 'q.*';

    $from = is_null($subquery) ? $tname.' q' : "($subquery) q";

    if($isGrid) {
      $cols = str_replace('module_view.','q.', $params[':__select']);
      unset($params[':__select']);
    }
    else if (isset($params[':__select'])) {
      $select = explode(',', $params[':__select']);
      $cols = [];
      
      foreach ($select as $cname) {
        foreach (['DELETE', 'INSERT', 'UPDATE', 'SELECT'] as $forbidden) {
          if (stripos($cname, $forbidden) !== FALSE) {
            throw new Exception("Forbidden column name.");
          }
        }
        
        $cols[] = "q.$cname";
      }

      $cols = implode(',', $cols);
      unset($params[':__select']);
    }

    if ($isGrid) {
      //limit filter by event_date
      if (isset($params[':event_date'])) {
        unset($params[':event_date']);
      }
    }

    $count = false;

    if (isset($params[':__count'])) {
      $cols = 'count(*)';
      unset($params[':__count']);
      $count = true;
    }
    
    $filterWhere = [];

    if (!empty($params)) {
      $filterWhere = [self::paramsToConditions($params, 'q.')];
    }

    if (!empty($filter)) {
      
      if (!empty($filterWhere)) {
        $filterWhere[] = 'AND';
      }

      $pinx = -1;
      foreach($filter as $i => $f) {
        /**
         * this is for backward compatibility (default operator was AND without possibility to change it and there were only 3 elements in one filter row)
         * later on the DataFilter component was added which calls the select with 4 element in filter row (logical operator, columne name, operation, value)
         */
        $operator = 'AND';

        if (count($f)===3) {
          $operator = 'AND';
          array_unshift($f, $operator);
        }
        else {
          $operator = $f[0];
        }

        if ($i===0) {
          $operator = '';
        }

        /**
         * check for forbidden SQL commands in filter
         */
        
        foreach (['DELETE', 'INSERT', 'UPDATE', 'SELECT', 'DROP', 'TRUNCATE'] as $forbidden) {
          if (stripos($f[1], $forbidden) !== FALSE || stripos($f[2], $forbidden) !== FALSE) {
            throw new Exception("Forbidden column name.");
          }
        }

        if ($i > 0 && empty($operator)) {
          $operator = 'AND';
        }

        if ($isGrid && $f[1]==='event_date') {  //limit search by event_date
          $dateParts = explode('-',$f[3]);
          $f[3] = $dateParts[0].'-'.$dateParts[1].'-01';
        }

        if (is_string($f[3]) && strtolower($f[3])==='null' || is_null($f[3])) {
          $filterWhere[] = "$operator q.{$f[1]} {$f[2]} null";
        }
        else if (is_array($f[3])) {
          $condition = '';
          if ($f[2]=='=') {
            $condition = 'IN';
          }
          else  {
            $condition = 'NOT IN';
          }

          $placeholders = [];
          foreach ($f[3] as $value) {
            $pinx++;
            $p = ":__filter$pinx";
            $params[$p] = $value;
            $placeholders[] = $p;
          }

          $placeholders = implode(',', $placeholders);

          $filterWhere[] = "$operator q.{$f[1]} $condition ($placeholders)";
        }
        else {
          $pinx++;
          $p = ":__filter$pinx";
          $filterWhere[] = "$operator q.{$f[1]} {$f[2]} $p"; //operator field condition
          $params[$p] = $f[3];  //value
        }
      }

    }

    self::accessFilters($tname, $pfilter, $filterWhere);

    $where = '';

    if (!empty($filterWhere)) {
      $where = 'WHERE '.implode(' ', $filterWhere);
    }

    if (count($spatialFilter) > 0) {
      self::spatialFilter($spatialFilter, $params, $from, $where);
    }

    if ($isGrid) {
      $cols = str_replace('q.event_date','make_date(extract(year from q.event_date)::integer,extract(month from q.event_date)::integer,1) as event_date',$cols);
    }

    $q = "SELECT $cols from $from $where";
    if ($isGrid && $tname==='mb2data.tlm_vw') {
      $q = "SELECT animal_id, to_char(min(event_date),'YYYY-MM') as ed1, to_char(max(event_date),'YYYY-MM') as ed2, tlm_tracks_id, species_list_id, licence_list_id, public.st_asgeojson(public.st_makeline(distinct geom)) geom FROM ($q) a group by tlm_tracks_id, species_list_id, licence_list_id, animal_id";
    }

    $params = empty($bindParams) ? $params : $bindParams;

    if ($onlyPrepare) {
      return [
        'cols' => $cols,
        'from' => $from,
        'where' => $where,
        'params' => $params
      ];
    }
    
    $res = self::query($q, $params, $result_type);
    return $count ? $res : self::translateRows($res, $language, $tname);
  }

  private static function accessFilters($tname, $pfilter, &$filterWhere) {
    $accessFilter = [];

    if (count($filterWhere) === 1 && $filterWhere[0] === 'q.id=:id') {
      return;
    }

    if (in_array($tname, ['mb2data.gensam', 'mb2data.gensam_vw'])) {
      
      if ($_SERVER['HTTP_HOST'] == 'portal.mbase.org' && $_SERVER['APP_URL'] == 'https://portal.mbase.org') {
        return;
      }
      $cud = Mbase2Drupal::getCurrentUserData();
      $uid = $cud['uid'];

      if (!is_array($pfilter)) {
        $pfilter = [];
      }

      $pfilter = array_diff([1, 2, 3, 4], $pfilter);
      
      if (empty($pfilter)) {
        return;
      }

      /**
       * 1 ... shared_with_me
       * 2 ... shared_by_me
       * 3 ... others
       * 4 ... my private data
       */

      if (in_array(4, $pfilter)) {
        $accessFilter[] = "(_uid=$uid and user_access_ids is null and organisation_access_ids is null)"; //my private data
      }

      if (in_array(2, $pfilter)) {
        $accessFilter[] = "(_uid=$uid and (user_access_ids is not null or organisation_access_ids is not null))"; //my data shared with others
      }

      $whereGensamIsSharedWithMe = Mbase2Utils::SQL_whereGensamIsSharedWithMe('q', $uid);

      if (in_array(3, $pfilter) && Mbase2Drupal::isModuleAdmin('gensam')) {
        $accessFilter[] = "(_uid<>$uid AND NOT ($whereGensamIsSharedWithMe))";
      }

      if (in_array(1, $pfilter)) {
        $accessFilter[] = "(_uid<>$uid AND ($whereGensamIsSharedWithMe))"; //data of others shared with me or my organisations
      }

      if (!empty($accessFilter)) {
        
        $filterWhere[] = (empty($filterWhere) ? '(' : 'AND (').implode(' OR ', $accessFilter).')';
      }
      else {
        $filterWhere[] = (empty($filterWhere) ? '(' : 'AND (').'false)';
      }
    }
  }

  /**
   * Add a spatial filter to query if __sfilter parameter is set
   */

   private static function spatialFilter($spatialFilter, &$params, &$tnames, &$where) {

    $filterWhere = [];
    $spatialFilterWhere = [];

    $locationGeometryTableAlias = 'q';

    $vmap = [
      'oe_ime' => 'mbase2_ge.oe',
      'ob_uime' => 'mbase2_ge.ob',
      'lov_ime' => 'mbase2_ge.luo_lov',
      'luo_ime' => 'mbase2_ge.luo',
      'nuts' => 'mbase2_ge.nuts'
    ];

    $pinx = 0;

    foreach ($spatialFilter as $i => $f) {
      $key = $f[1];
      if (!isset($vmap[$key])) continue;
      $tname = $vmap[$key];
      
      if (count($filterWhere)===0) {
        $f[0] = '';
      }

      $tname = str_replace([" ","(",")","-"],'_', $tname);

      $spatialFilterWhere[$tname] = "public.st_intersects( $locationGeometryTableAlias.__location_geometry, $tname.geom)";

      $values = is_array($f[3]) ? $f[3] : array($f[3]);

      $placeholders = [];
      foreach ($values as $value) {
        if (strtolower($value)==='null') $value = null;
        $p = ":__sfilter$pinx";
        $placeholders[] = $p;
        $params[$p] = $value;  //value
        $pinx++;
      }

      $f[2] = trim($f[2]) == '=' ? 'IN' : 'NOT IN';
      
      $placeholders = implode(',', $placeholders);
      $filterWhere[] = "{$f[0]} $tname.gid {$f[2]} ($placeholders)";
    }

    $tbls = array_keys($spatialFilterWhere);

    if (count($tbls) > 0) {
      $tnames.=','.implode(',',$tbls);
      $operator = empty($where) ? '' : ' AND ';
      
      if (empty($where)) {
        $where = ' WHERE ';
      }
      
      $where.=$operator.'('.implode(' AND ', $spatialFilterWhere).' AND ('.implode(' ', $filterWhere).'))';
    }
  }

  /**
   * Translates columns found in the mbase2.code_list_options_fkeys materialized view 
   */

  public static function translateRows($res, $language, $tname) {
    if (empty($res)) return $res;

    $cnames = self::query("SELECT fk_column from mbase2.code_list_options_fkeys where fk_table = :tname::regclass", [':tname'=>$tname]); //column names for translation

    if (empty($cnames)) return $res;

    $cnames = array_column($cnames, 'fk_column');

    self::translateRowsColumns($res, $cnames, $language);

    return $res;
  }

  /**
   * Translates $cnames in array of rows objects
   * 
   * @param &$res {array<object>} array of associative object rows
   * @param $cnames {array<string>} column names for translations
   * @param $language {string} language code
   */
  private static function translateRowsColumns(&$res, $cnames, $language) {
    
    $ids = [];
    foreach ($res as $row) {
      foreach ($cnames as $cname) {
        if (empty($row[$cname])) continue;
        $ids[$row[$cname]] = true;
      }
    }

    if (empty($ids)) return;

    $p = [':id' => array_keys($ids)];

    $translations = self::query("SELECT id, key, translations->>:language t FROM mbase2.code_list_options WHERE ".self::paramsToConditions($p),array_merge($p, [':language' => $language]));

    $translations = array_column($translations, null, 'id');

    foreach ($res as &$row) {
      foreach ($cnames as $cname) {
        if (empty($row[$cname])) continue;
        $trans = $translations[$row[$cname]];
        $row['t_'.$cname] = $trans['t'];
        $row['key_'.$cname] = $trans['key'];
      }
    }

    unset($row);
  }

  public static function getConstraints($schema, $tname, $cond = "contype <> 'p'") {
    if (!empty($cond)) $cond = "AND $cond";
    $res = self::query("SELECT con.conname
      FROM pg_catalog.pg_constraint con
          INNER JOIN pg_catalog.pg_class rel
                      ON rel.oid = con.conrelid
          INNER JOIN pg_catalog.pg_namespace nsp
                      ON nsp.oid = connamespace
      WHERE nsp.nspname = :schema $cond
            AND rel.relname = :tname",[':schema' => $schema, ':tname'=>$tname]);

    return array_column($res, 'conname');
  }

  public static function moduleReferences($moduleId, &$ids) {
      $res = self::query("SELECT v.ref FROM mbase2.module_variables v, mbase2.code_list_options clv, mbase2.referenced_tables rt WHERE 
      module_id = :module_id AND 
      ref is not null AND
      clv.list_key = 'referenced_tables' AND
      rt.id = clv.id AND 
      rt.static = false AND
      ref = clv.id", 
      [':module_id' => $moduleId]);
      
      foreach ($res as $r) {
        $id = $r['ref'];
        $ids[] = $id;
        self::moduleReferences($id, $ids);
      }
  }

  // }}}
  
}
