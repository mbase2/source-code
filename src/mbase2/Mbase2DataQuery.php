<?php
    final class Mbase2DataQuery {

        private static function extractModuleKey($tname) {
            return str_replace(['mb2data.','_vw'],'',$tname);
        }

        private static function variablesModuleNames($moduleKey) {
            $attributeDefinitionsNamespace = [
                'interventions' => ['interventions_batch_imports'],
                'dmg' => ['dmg_batch_imports'],
                'cnt'=>['cnt_monitoring','cnt_observation_reports','cnt_permanent_spots']
            ];

            if (isset($attributeDefinitionsNamespace[$moduleKey])) {
                $moduleKey = $attributeDefinitionsNamespace[$moduleKey];
            }
            else {
                if (!in_array($moduleKey, Mbase2Utils::moduleKeys())) {
                    $moduleKey = null;
                }
                else {
                    $moduleKey = [$moduleKey];
                }
            }

            return "'".implode("','", $moduleKey)."'";
        }


        public static function SelectColumns($tname, $isGrid) {
            if ($isGrid) {
                
                $moduleKeys = self::variablesModuleNames(self::extractModuleKey($tname));

                $rows = \DB::select("SELECT variable_name FROM mbase2.module_variables_vw WHERE module_name in ($moduleKeys) and visible_in_cv_grid = true");
                $cols = ["module_view.event_date as event_date"];
                foreach($rows as $row) {
                    $variableName = $row->variable_name;
                    if ($variableName === 'event_date') continue;
                    $cols[] = 'module_view.'.$variableName;
                }
    
                return $cols;
            }
            else if (str_starts_with($tname, 'mb2data.gensam')) {
                
            }
            
            return ['module_view.*'];
        }

        public static function AdditionalFilters($tname, &$params, &$select, $isGrid) {
            if (isset($params[':__grid'])) {
                $moduleKey = self::extractModuleKey($tname);
                if ($moduleKey === 'dmg') {

                    $filter = isset($params[':__filter']) ? json_decode($params[':__filter'],true) : [];

                    $filter[] = ["AND", "_claim_status", "<>", "0"];

                    $params[':__filter'] = json_encode($filter);

                    //additional fields used in filters
                    if ($isGrid) {
                        $select[] = 'module_view._claim_status';
                    }
                }
            }
        }
        
        public static function GeometryDataSubquery($tname, &$params, $isGrid, $select) {
            $subquery = null;
            if (isset($params[':__grid']) || isset($params[':__sfilter'])) {
                
                if (isset($params[':__sfilter'])) {
                    $sf = $params[':__sfilter'];
                    if (is_string($sf) && empty(json_decode($sf))) {
                        return null;
                    }
                }
                
                $moduleKey = self::extractModuleKey($tname);

                $tableHoldingGeometry = "mb2data.$moduleKey";
                $joinOn = "module_view.id=table_holding_geometry.id";
                $geometryFieldName = '_location';

                if ($moduleKey === 'ct') {//camera trap locations are stored in mb2data.ct_locations
                    $tableHoldingGeometry = 'mb2data.ct_locations';
                    $joinOn = "module_view.trap_station_location_id=table_holding_geometry.id";
                    $geometryFieldName = 'geom';
                }
                else if ($moduleKey === 'mortbiom') {
                    $tableHoldingGeometry='mb2data.mortbiom_vw';
                }
                else if ($moduleKey === 'interventions') {
                    $tableHoldingGeometry='mb2data.interventions_vw';
                }  
                else if ($moduleKey === 'dmg') {
                    $tableHoldingGeometry='mb2data.dmg_vw';
                }
                else if ($moduleKey === 'cnt') {
                    $tableHoldingGeometry='mb2data.cnt_vw';
                }
                else if ($moduleKey === 'tlm') {
                    $tableHoldingGeometry='mb2data.tlm_vw';
                }     

                $cols = implode(',',$select);

                $leftJoin = "left join $tableHoldingGeometry table_holding_geometry on $joinOn";
                $alias = 'table_holding_geometry';

                if ($tname === $tableHoldingGeometry) {
                    $leftJoin = '';
                    $alias = 'module_view';
                }


                if ($isGrid) {
                    $res = Mbase2Database::query("select * from mbase2.modules_vw vm where module_key=:key",[':key' => $moduleKey]);
                    $grid = 10000;  //default
                    if (!empty($res)) {
                        $grid = json_decode($res[0]['properties'])->grid_size;
                    }

                    $locationGeometry = '';
                    if (isset($params[':__sfilter'])) {
                        $locationGeometry = ",$alias.$geometryFieldName as __location_geometry";
                    }

                    $subquery = '';

                    if ($moduleKey === 'tlm') {
                        $subquery = "select $cols, 
                        public.ST_SnapToGrid(public.ST_Transform($alias.$geometryFieldName, 3035) , 0, 0, $grid, $grid) geom$locationGeometry from $tname module_view $leftJoin WHERE public.ST_X($alias.$geometryFieldName)<361";
                    }
                    else {
                        $subquery = "select $cols, 
                        public.ST_AsGeoJSON(public.ST_SnapToGrid(public.ST_Transform($alias.$geometryFieldName, 3035) , 0, 0, $grid, $grid)) geom$locationGeometry from $tname module_view $leftJoin WHERE public.ST_X($alias.$geometryFieldName)<361";
                    }

                    $select[0] = 'module_view.event_date';
                    $select[] = 'module_view.geom';
                    $params[':__select'] = implode(',', $select);
                }
                else {
                    $locationGeometry = '';
                    if (isset($params[':__sfilter'])) {
                        $locationGeometry = ",$alias.$geometryFieldName as __location_geometry";
                    }

                    $subquery = "select $cols, 
                    public.ST_AsGeoJSON($alias.$geometryFieldName) geom$locationGeometry from $tname module_view $leftJoin";
                }
            }
            return $subquery;
        }
    }
