<?php

    require 'vendor/autoload.php';

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

    use proj4php\Proj4php;
    use proj4php\Proj;
    use proj4php\Point;

    final class Mbase2Export {
        static public function filteredGenotypesExport($dbFunctionParameters) {
            $dbFunctionParameters[] = true;
            $res = call_user_func_array(['Mbase2Database', 'select'], $dbFunctionParameters);
            
            $where = trim($res['where']);
            $where = stripos($where, 'WHERE') === 0 ? substr($where, 5) : $where;
            $where = trim($where);
            
            if (!empty($where)) {
                $where = "AND ($where)";
            }

            $p = $res['params'];

            $res = \DB::select("SELECT g.sample_code, b.* FROM (
                SELECT a.gensam_id, json_agg(marker) markers, json_agg(alleles) alleles from (
                    SELECT gg.gensam_id, ga.marker, json_agg(ga.name ORDER BY ga.name) alleles FROM 
                        mb2data.gensam_genotypes gg,
                        mb2data.gensam_alleles ga,
                        mb2data.gensam q
                    WHERE gg.allele_id = ga.id
                    AND gg.gensam_id = q.id $where
                    GROUP BY marker, gensam_id 
                    ORDER BY gensam_id,marker) a
                GROUP BY a.gensam_id) b
            LEFT JOIN mb2data.gensam g on g.id = b.gensam_id", $p);

            $allelesRows = \DB::SELECT("select name as allele, marker, seq as sequence from mb2data.gensam_alleles ga where exists (select 1 from 
                mb2data.gensam_genotypes gg, mb2data.gensam q
            where ga.id = gg.allele_id and gg.gensam_id = q.id $where)", $p);
            
            self::outputGenotypes($res, $allelesRows);
        }

        static public function genotypes($batch_id) {
            $batch_id = intval($batch_id);
            $uid = Mbase2Drupal::getCurrentUserData()['uid'];
            $where = Mbase2Utils::SQL_whereUserCanAccessGensam('g', $uid);
            $res = \DB::select("SELECT g.sample_code, b.* FROM (
                SELECT a.gensam_id, json_agg(marker) markers, json_agg(alleles) alleles from (
                    SELECT gg.gensam_id, ga.marker, json_agg(ga.name ORDER BY ga.name) alleles FROM 
                        mb2data.gensam_genotypes gg,
                        mb2data.gensam_alleles ga,
                        mb2data.gensam g
                    WHERE gg.batch_id = :batch_id
                    AND gg.allele_id = ga.id
                    AND gg.gensam_id = g.id
                    AND ($where)
                    GROUP BY marker, gensam_id 
                    ORDER BY gensam_id,marker) a
                GROUP BY a.gensam_id) b
            LEFT JOIN mb2data.gensam g on g.id = b.gensam_id",[':batch_id' => $batch_id]);

            $allelesRows = \DB::SELECT("select name as allele, marker, seq as sequence from mb2data.gensam_alleles ga where exists (select 1 from mb2data.gensam_genotypes gg
            where ga.id = gg.allele_id and gg.batch_id = :batch_id)", [':batch_id' => $batch_id]);

            self::outputGenotypes($res, $allelesRows);
            
        }

        static private function outputGenotypes($res, $allelesRows) {
            $output = [];
            $markersHeader = [];
            foreach ($res as $row) {
                $markers = json_decode($row->markers);
                $alleles = json_decode($row->alleles);

                $genotype = [];
                foreach ($markers as $index => $marker) {
                    $genotype[$marker] = implode(' ', $alleles[$index]);
                }

                $output[] = ['sample_code' => $row->sample_code] + $genotype;
                $markersHeader += array_flip($markers);
            }

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setTitle("allele_seq");

            self::outputToSpreadsheet(['allele', 'marker', 'sequence'], array_map(fn($obj) => (array)$obj, $allelesRows), [], ['spreadsheet' => $spreadsheet, 'sheet' => $sheet]);
            
            $genotypesSheet = self::createSheet('genotypes', $spreadsheet);

            $header = array_keys($markersHeader);

            array_unshift($header, 'sample_code');

            self::outputToSpreadsheet($header, $output, [], [
                'spreadsheet' => $spreadsheet,
                'sheet' => $genotypesSheet
            ]);
            
            self::streamSpreadsheetToExcel($spreadsheet, 'mbase_genotypes.xlsx');
        }

        static private function createSheet($sheetName, $spreadsheet) {
            // Create a new sheet
            $newSheet = new Worksheet($spreadsheet, $sheetName); // Name the new sheet
            $spreadsheet->addSheet($newSheet); // Add it to the spreadsheet
            return $newSheet;
        }

        static public function alleleMap($batch_id) {
            $batch_id = intval($batch_id);
            $res = \DB::select("select gia.name name0, ga.name name, ga.marker, ga.seq from 
                (select distinct allele_id from mb2data.gensam_genotypes where batch_id = :batch_id) gg,
                mb2data.gensam_alleles ga,
                mb2data.gensam_imported_alleles gia 
                where
                gia.allele_id = ga.id
                and gia.batch_id  = :batch_id
                and gg.allele_id = ga.id",[':batch_id' => $batch_id]);
            
            if (!empty($res)) {
                $resArray = array_map(fn($obj) => (array) $obj, $res);
                self::outputToExcel([['name0','imported name'],['name','database name'],['marker','marker'], ['seq','sequence']], $resArray, 'allele-database-map');
            }
        }

        static private function outputToExcel($header, $data, $filename='mbase-export', $format=[]) {
            $spreadsheet = self::outputToSpreadsheet($header, $data, $format);
            self::streamSpreadsheetToExcel($spreadsheet, $filename);
        }

        static private function streamSpreadsheetToExcel($spreadsheet, $filename) {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
            header('Cache-Control: max-age=0');

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

            $writer->save('php://output');

            exit();
        }

        static private function outputToSpreadsheet($header, $data, $format=[], $spreadsheetToWrite = []) {
            if (empty($spreadsheetToWrite)) {
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
            }
            else {
                $spreadsheet = $spreadsheetToWrite['spreadsheet'];
                $sheet = $spreadsheetToWrite['sheet'];
            }
            
            $keys = [];

            $cinx = 1;
            $rinx = 1;
            foreach ($header as $value) {
                $sheet->setCellValueByColumnAndRow($cinx,$rinx, is_array($value) ? $value[1] : $value);
                $keys[] = is_array($value) ? $value[0] : $value;
                $cinx++;
            }

            $rinx = 2;
            foreach($data as $dataRow) {
                $cinx = 1;
                foreach ($keys as $key) {
                    $value = isset($dataRow[$key]) ? $dataRow[$key] : '';
                    if (!empty($value) && isset($format[$key])) {
                        if ($format[$key] == 'date') {
                            $datetime = new DateTime($value, new DateTimeZone('UTC'));
                            $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($datetime->getTimestamp());
                            $sheet->getStyleByColumnAndRow($cinx,$rinx)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_YYYYMMDD2);
                        }
                    }

                    $sheet->setCellValueByColumnAndRow($cinx,$rinx, $value);
                    $cinx++;
                }
                $rinx++;
            }

            $spreadsheet->setActiveSheetIndex(0);

            return $spreadsheet;
        }

        static private function dmg_groupObjectsByAffectee($damageObjects) {
            $result = [];
            foreach ($damageObjects as $obj) {
                $data = $obj->data;
                $affecteeId = $data->affectee;
                if (!isset($result[$affecteeId])) {
                    $result[$affecteeId] = [];
                }
                $result[$affecteeId][$obj->oid] = $obj;
            }
            return $result;
        }

        static private function dmg_object_price($object) {
            $data = $object->data;
            $enota = $data->enota;
            $kolicina = $data->kolicina;

            $cenik = $object->cenik ?? [];

            if (empty($cenik)) return 0.0;

            $cena_na_enoto = 0;
            if (empty($kolicina)) {
                $kolicina = 0;
            }

            foreach($cenik as $item) {
                if ($item->e == $enota) {
                    $cena_na_enoto = $item->c ?? 0.0; //npr. žival, "lovski pes"
                    break;
                }
            }

            return floatval($cena_na_enoto) * floatval($kolicina);
        }

        static private function handleUnmatchedAgreementObjects(&$damageObjects, $agreementData, &$unmatchedAgreementsObjectsReport) {
            $affecteeId = $agreementData->_affectee ?? null;
            if (is_null($affecteeId)) return;

            $unmatchedAgreementDamageObjects = [];
            $matchedObjectIds = [];
            foreach($agreementData->_odskodnina as $row) {
                $oid = $row[0]->oid;
                if (isset($damageObjects[$oid])) {
                    $damageObjects[$oid]->price = $row[5][0];
                    $matchedObjectIds[$oid] = $oid; 
                }
                else {
                    if (floatval($row[5][0]) > 1e-4) {
                        $unmatchedAgreementDamageObjects[$oid] = $row;
                    }
                }
            }

            $replacements = [];

            foreach ($unmatchedAgreementDamageObjects as $uoid => $row) {
                $kolicina = floatval($row[1]);
                $enota = $row[2];
                foreach ($damageObjects as $oid=>$object) {
                    if (!isset($matchedObjectIds[$oid])
                        && $object->text == $row[0]->key
                        && isset($object->data) 
                        && $object->data->enota == $enota 
                        && abs(floatval($object->data->kolicina) - $kolicina) < 1e-5) {
                            //našel je škodni objekt iz sporazuma na popisnem obrazcu, izbiral je med tistimi objekti, za katere še nisem našel povezave (!isset($matchedObjectIds))
                            $damageObjects[$oid]->price = $row[5][0];
                            $replacements[$oid] = $uoid;
                            $matchedObjectIds[$uoid] = $uoid;
                            break;
                    }
                }
            }

            foreach($replacements as $oid => $uoid) {
                $damageObjects[$uoid] = $damageObjects[$oid];
                unset ($damageObjects[$oid]);
                $damageObjects[$uoid]->oid = $uoid;
            }

            //check if agreement data and claim data correspond

            if (count($unmatchedAgreementDamageObjects)>0) {
                $unmatchedObjects = [];
                
                foreach($agreementData->_odskodnina as $row) {
                    $oid = $row[0]->oid;
                    if (!isset($damageObjects[$oid])) {
                        $unmatchedObjects[$oid] = [
                            'text' => $row[0]->key,
                            'kolicina' => floatval($row[1]),
                            'enota' => $row[2]
                        ];
                    }
                }

                if (!empty($unmatchedObjects)) {

                    $unmatchedAgreementsObjectsReport[] = [
                        'affecteeId' => $affecteeId,
                        'unmatchedObjects' => $unmatchedObjects
                    ];
                    
                    /**
                     * Tukaj nastavim ceno tudi za objekte, ki jih ne najdem v sporazumu
                     */

                    foreach ($damageObjects as $oid => $object) {
                        if (!isset($matchedObjectIds[$oid])) {
                            $damageObjects[$oid]->price = self::dmg_object_price($object);
                            $matchedObjectIds[$oid] = $oid;
                        }
                    }
                }
            }
        }

        static private function dmg_damageObjectPrices(&$damageObjectsByAffectees, $claim_id, $agreements) {
            $unmatchedAgreementsObjectsReport = [];
            foreach($damageObjectsByAffectees as $affecteeId => &$damageObjects) {
                $agreementData = isset($agreements[$claim_id.'_'.$affecteeId]) ? $agreements[$claim_id.'_'.$affecteeId] : (object)[];
                if (isset($agreementData->_odskodnina)) {
                    self::handleUnmatchedAgreementObjects($damageObjects, $agreementData, $unmatchedAgreementsObjectsReport);
                }
                else { //ni shranil sporazuma
                    foreach ($damageObjects as $oid => $object) {
                        $damageObjects[$oid]->price = self::dmg_object_price($object);
                    }
                }
            }

            return $unmatchedAgreementsObjectsReport;
        }

        static private function getCodeListValue(&$clist, $id) {
            if (empty($id)) return '';
            if (!isset($clist[$id])) return '';
            $values = $clist[$id]['translations'];
            if (!empty($values)) {
                if (is_string($values)) {
                    $values = json_decode($values);
                    $clist[$id]['translations'] = $values;
                }
                
                if (isset($values->sl)) {
                    return $values->sl;
                }
            }

            return $clist[$id]['key'];
        }

        static private function getUserName($users, $id) {
            if (empty($id)) return '';
            if (!isset($users[$id])) return '';
            $user = $users[$id];
            return $user['name'].' ('.$user['username'].')';
        }

        static private function selectWithSpecify($clist, $row, $edata, $key) {
            return self::getCodeListValue($clist, $row[$key]).(isset($edata->$key) ? " ({$edata->$key})" : '');
        }

        static private function getCodeLists() {
            $users = Mbase2Database::query("SELECT * FROM laravel.users");
            $users = array_column($users, null, 'id');

            $clist = Mbase2Database::query("SELECT * FROM mbase2.code_list_options_vw");
            $clist = array_column($clist, null, 'id');

            return [$users, $clist];
        }

        static private function interventionsExport($dbFunctionParameters) {
            
            list($users, $clist) = self::getCodeLists();

            $proj4 = new Proj4php();
            $proj4->addDef("EPSG:3912",'+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=bessel +towgs84=409.545,72.164,486.872,3.085957,5.469110,-11.020289,17.919665 +units=m +no_defs');

            $projFrom = new Proj('EPSG:4326', $proj4);
            $projTo = new Proj('EPSG:3912', $proj4);

            $output = [];
            ////////////////////////////////////////////////////
            $interventions = Mbase2Database::query("SELECT * from mb2data.interventions");
            $interventionsEventsRaw = Mbase2Database::query("SELECT * from mb2data.interventions_events ORDER by interventions_id");

            $interventionsEvents = [];
            foreach ($interventionsEventsRaw as $ier) {
                $interventionId = $ier['interventions_id'];
                if (!isset($interventionsEvents[$interventionId])) $interventionsEvents[$interventionId] = [];
                $interventionsEvents[$interventionId][] = $ier;
            }
            
            foreach ($interventions as $intervention) {
                $outputRow = [];
                $outputRow['Živalska vrsta'] = self::getCodeListValue($clist, $intervention['_species_name']);
                $data = json_decode($intervention['_data']);
                $spatialData = $data->_location;
                $spatialRequestResult = $spatialData->spatial_request_result;
                
                $outputRow['Območna enota ZGS'] = $spatialRequestResult->oe_ime;
                $outputRow['Občina'] = $spatialRequestResult->ob_uime;
                $outputRow['LUO'] = $spatialRequestResult->luo_ime;
                $outputRow['Lovišče'] = $spatialRequestResult->lov_ime;

                $outputRow['Opombe'] = $intervention['notes'];

                if (isset($interventionsEvents[$intervention['id']])) {
                    foreach ($interventionsEvents[$intervention['id']] as $einx => $e) {
                        $edata = json_decode($e['_data']);

                        $outputRow["Klic ($einx)"] = self::selectWithSpecify($clist, $e, $edata, 'intervention_caller');
                        $outputRow["Vzrok intervencije ($einx)"] = self::selectWithSpecify($clist, $e, $edata, 'intervention_reason');

                        $outputRow["Opis situacije ($einx)"] = $e['situation_notes'];

                        $measures = [];
                        if (!empty($e['intervention_measures'])) {
                            $ims = json_decode($e['intervention_measures']);
                            
                            foreach($ims as $im) {
                                $measure = self::getCodeListValue($clist, $im);
                                $mkey = isset($clist[$im]) ? $clist[$im]['key'] : null;
                                if (!empty($mkey)) {
                                    if (strpos($mkey, '_specify')!==FALSE) {
                                        if (isset($edata->intervention_measures)) {
                                            $measure = $measure." ({$edata->intervention_measures})";
                                        }
                                    }
                                }
                                $measures[] = $measure;
                            }    
                        }
                        
                        $outputRow["Intervencijski ukrepi ($einx)"] = implode(', ', $measures);
                        
                        $outputRow["Izid intervencije ($einx)"] = self::selectWithSpecify($clist, $e, $edata, 'intervention_outcome');

                        $outputRow["Vodja intervencije ($einx)"] = self::getUserName($users, $e['chief_interventor']);

                        $interventorsOut = [];
                        if (!empty($e['interventors'])) {
                            $interventors = json_decode($e['interventors']);
                            foreach ($interventors as $interventor) {
                                $interventorsOut[] = self::getUserName($users, $interventor);
                            }
                        }

                        $outputRow["Člani intervencijske skupine ($einx)"] = implode(', ', $interventorsOut);

                        $outputRow["Datum in čas klica ($einx)"] = $e['intervention_call_timestamp']; 
                        $outputRow["Datum in čas začetka ($einx)"] = $e['intervention_start_timestamp']; 
                        $outputRow["Datum in čas konca ($einx)"] = $e['intervention_end_timestamp']; 

                        $outputRow["Opombe ($einx)"] = $e['notes'];
                    }
                }

                $outputRow['Uporabnik'] = self::getUserName($users, $intervention['_uname']);
                $outputRow['Datum in čas prvega vpisa'] = $intervention['date_record_created'];
                $outputRow['Datum in čas zadnje spremembe'] = $intervention['date_record_modified'];

                $output[] = $outputRow;
            }

            //////////////////////////////////////////////

            $coloumnCount = 0;
            $header = [];

            foreach($output as $outputRow) {
                $cheader = array_keys($outputRow);
                if (count($cheader) > $coloumnCount) {
                    $coloumnCount = count($cheader);
                    $header = $cheader;
                }
            }

            self::outputToExcel($header, $output);
        }

        static private function sortAuxHeader($inputArray) {
           
            $batchOrder = [
                "Oškodovanec (", //item related to the whole batch
                "Status sporazuma (", //item related to the whole batch
                "Ali je sporazum zaključen? (", //item related to the whole batch
                "Škodni objekt ZGS (oškodovanec: ", 
                "Škodni objekt (oškodovanec: ",
                "Količina (oškodovanec: ",
                "Enota (oškodovanec: ",
                "Odškodnina (oškodovanec: ",
                "Odškodnina dodatno (oškodovanec: ",
                "Odškodnina skupaj (oškodovanec: ",
                "Škoda dodatno (oškodovanec: ",
                "Varovanje (oškodovanec: ",
                "Lastnosti varovanja (oškodovanec: "
            ];

            function getNumberOfBatches($inputArray) {
                $maxBatch = 0;
                foreach ($inputArray as $item) {
                    if (strpos($item, "Oškodovanec (") === 0) {
                        preg_match('/Oškodovanec \((\d+)\)/', $item, $matches);
                        if (isset($matches[1])) {
                            $batchNumber = (int)$matches[1];
                            if ($batchNumber > $maxBatch) {
                                $maxBatch = $batchNumber;
                            }
                        }
                    }
                }
                return $maxBatch;
            }
            
            function getBatchHeaders($inputArray, $batchNumber) {
                $batchHeaders = [];
                foreach ($inputArray as $item) {
                    // Match items like "Oškodovanec (1)", "Status sporazuma (1)", etc.
                    if (preg_match('/\((\d+)\)/', $item, $matches) && $matches[1] == $batchNumber) {
                        $batchHeaders[] = $item;
                    }
                    // Match items like "(oškodovanec: 1, ...)"
                    elseif (preg_match('/\(oškodovanec: ' . $batchNumber . '(,|\))/', $item)) {
                        $batchHeaders[] = $item;
                    }
                }
                return $batchHeaders;
            }
            
            function sortBatchHeaders($batchHeaders, $batchOrder) {
                $sortedHeaders = [];
                $objektGroups = [];
            
                // Group headers by "objekt" number
                foreach ($batchHeaders as $header) {
                    if (preg_match('/objekt: (\d+)/', $header, $matches)) {
                        $objektNumber = (int)$matches[1];
                        if (!isset($objektGroups[$objektNumber])) {
                            $objektGroups[$objektNumber] = [];
                        }
                        $objektGroups[$objektNumber][] = $header;
                    } else {
                        // Headers without "objekt" (e.g., "Oškodovanec (1)", "Status sporazuma (1)")
                        $sortedHeaders[] = $header;
                    }
                }
            
                // Sort "objekt" groups by their number
                ksort($objektGroups);
            
                // Sort each "objekt" group according to $batchOrder
                foreach ($objektGroups as $objektNumber => $headers) {
                    foreach ($batchOrder as $orderItem) {
                        foreach ($headers as $header) {
                            if (strpos($header, $orderItem) === 0) {
                                $sortedHeaders[] = $header;
                            }
                        }
                    }
                }
            
                return $sortedHeaders;
            }
            
            $numberOfBatches = getNumberOfBatches($inputArray);

            $sortedBatchHeaders = [];
            for ($ainx = 1; $ainx <= $numberOfBatches; $ainx++) {
                $batchHeaders = getBatchHeaders($inputArray, $ainx);
                $sortedBatchHeaders = array_merge($sortedBatchHeaders, sortBatchHeaders($batchHeaders, $batchOrder));
            }
            
            return $sortedBatchHeaders;
            
        }

        static private function generalExport($module, $dbFunctionParameters) {
            self::export($module, $dbFunctionParameters);
        }
       
        static private function dmgExport($dbFunctionParameters, $oeid = null, $exportOptions=[]) {
            ini_set('memory_limit', '5G');

            $isAdmin = Mbase2Drupal::isAdmin('dmg');

            $dbFunctionParameters[0] = 'mb2data.dmg_app_vw';

            $dbFunctionParameters[1][':_claim_status'] = 1;

            $filter = isset($dbFunctionParameters[1][':__filter']) ? json_decode($dbFunctionParameters[1][':__filter']) : [];

            if (!is_null($oeid)) {
                $filter[] = [count($filter) > 0 ? "AND" : "", "_claim_id", "LIKE", "$oeid/%"];
            }

            if (count($filter) > 0) {
                $dbFunctionParameters[1][':__filter'] = json_encode($filter);
            }

            $withSpatialUnitEventDate = Mbase2Utils::SQL_withSpatialUnitEventDate($dbFunctionParameters[0]);

            $exportOption = $exportOptions[0] ?? '';

            if (empty($exportOption) || in_array($exportOption, ['objects', 'dmg_objects_by_oe'])) {
                $dbFunctionParameters[3] = "$withSpatialUnitEventDate
                    select _x, vw.* from 
                    x, {$dbFunctionParameters[0]} vw where x.id = vw.id and vw.id>0";
            }

            $claimsRes = call_user_func_array(['Mbase2Database', 'select'], $dbFunctionParameters);

            $clist = Mbase2Database::query("SELECT * FROM mbase2.code_list_options_vw");
            $clist = array_column($clist, null, 'id');

            if ($exportOption === 'dmg_objects_by_oe') {
                self::dmgObjectsByRegionalUnit($claimsRes, $clist);
                return;
            }

            $obmocneEnote = Mbase2Database::query("SELECT * FROM mbase2.obmocne_enote_zgs");
            $obmocneEnote = array_column($obmocneEnote, null, '_oe_id');

            $agreementStatuses = [
                1 => 'Sporazum JE dosežen.',
                2 => 'Sporazum NI dosežen',
                3 => 'Obrazec 2.3 - vloga stranke'
            ];

            $agreementsRes = Mbase2Database::query("SELECT * from mb2data.dmg_agreements");
            $agreements = [];
            foreach($agreementsRes as $r) {
                $agreementData = json_decode($r['_data']);
                $agreementData->_completed = $r['_completed'];
                $agreementData->_claim_id = $r['_claim_id'];
                $statusSporazuma = $agreementStatuses[$agreementData->_status] ?? '';
                $agreementData->status_sporazuma = $statusSporazuma;
                $agreementData->sporazum_zakljucen = $agreementData->_completed ? 'DA' : 'NE';
                $agreements[$r['_claim_id'].'_'.$r['_affectee']] = $agreementData;
            }

            $users = Mbase2Database::query("SELECT * FROM laravel.users");
            $users = array_column($users, null, 'id');

            $affectees = Mbase2Database::query("SELECT id, _full_name || ', ' || _street || ' ' || _house_number || ', ' || _post_number as naslov FROM mb2data.dmg_affectees");
            $affectees = array_column($affectees, null, 'id');

            $proj4 = new Proj4php();
            $proj4->addDef("EPSG:3912",'+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=bessel +towgs84=409.545,72.164,486.872,3.085957,5.469110,-11.020289,17.919665 +units=m +no_defs');

            $projFrom = new Proj('EPSG:4326', $proj4);
            $projTo = new Proj('EPSG:3912', $proj4);

            if (empty($exportOptions)) {
                self::dmgFullExport($oeid, $isAdmin, $claimsRes, $agreements, $users, $obmocneEnote, $clist, $affectees, $proj4, $projFrom, $projTo);
            }
            else if ($exportOptions[0] === 'objects') {
                self::dmgObjectsExport($oeid, $isAdmin, $claimsRes, $agreements, $users, $obmocneEnote, $clist, $affectees);
            }
            else if ($exportOptions[0] === 'monthly-reports') {
                self::dmgMonthlyReports($oeid, $isAdmin, $claimsRes, $agreements, $users, $obmocneEnote, $clist, $affectees, $proj4, $projFrom, $projTo);
            }

        }

        private static function dmgObjectsByRegionalUnit($claimsRes, $clist) {
            $countByRegionalUnit = [];
            $countByCulprit = [];

            foreach($claimsRes as $row) {
                $culprit = self::getCodeListValue($clist,$row['_culprit']);
                if (empty($culprit)) continue;

                if (!isset($countByCulprit[$culprit])) {
                    $countByCulprit[$culprit] = 0;
                }

                $spatialUnits = self::getSpatialUnits($row);
                if (isset($spatialUnits['SI-OE'])) {
                    $ru = $spatialUnits['SI-OE'];
                    if (!isset($countByRegionalUnit[$ru])) {
                        $countByRegionalUnit[$ru] = [];
                    }

                    if (!isset($countByRegionalUnit[$ru][$culprit])) {
                        $countByRegionalUnit[$ru][$culprit] = 0;
                    }

                    $countByRegionalUnit[$ru][$culprit]++;
                    $countByCulprit[$culprit]++;
                }
            }

            $header = ['OE' => 'OE'];
            $data = [];

            foreach($countByRegionalUnit as $ru => $cnts) {
                $dataRow=[];
                $dataRow['OE'] = $ru;
                
                foreach ($cnts as $culprit => $cnt) {
                    $header[$culprit] = $culprit;
                    $dataRow[$culprit] = $cnt;
                }
                $data[] = $dataRow;
            }

            $countByCulprit['OE'] = 'SKUPAJ';
            $data[] = $countByCulprit;

            self::outputToExcel($header, $data, 'mbase-dmg-claims-by-species-and-ru');
        }

        private static function dmgMonthlyReports($oeid, $isAdmin, $claimsRes, $agreements, $users, $obmocneEnote, $clist, $affectees, $proj4, $projFrom, $projTo) {
            $header = [
                ['_claim_id' , 'ŠTEVILKA ŠKODNEGA DOGODKA'], //
                ['_claim_agreement_status', 'Status zapisa v bazi'], //
                ['deputy' , 'Pooblaščenec'],
                ['oe_deputy' , 'OE pooblaščenca'],
                ['culprit' , 'Povzročitelj'],//
                ['_dmg_examination_date' , 'Datum ogleda škode'], //
                ['lat' , 'Zemlj.širina WGS84'],//
                ['lon' , 'Zemlj.dolžina WGS84']//
            ];

            $statusZapisaCodeList = [
                2 => 'ZAKLJUČEN 2',
                1 => 'ZAKLJUČEN 1',
                0 => 'NEZAKLJUČEN'
            ];

            $data = [];
            $napake = [];

            $header1 = [];
            $header2 = [];

            foreach($claimsRes as $row) {

                $dataRow = [];
                foreach (['_claim_id', '_dmg_examination_date'] as $key) {
                    $dataRow[$key] = $row[$key];
                }

                list($oePooblascenca, $pooblascenec) = self::dmgDeputyData($row, $users, $obmocneEnote);

                $dataRow['oe_deputy'] = $oePooblascenca;
                $dataRow['deputy'] = $pooblascenec;

                $dataRow['_claim_agreement_status'] = $statusZapisaCodeList[$row['_claim_agreement_status']] ?? '';

                $geom = json_decode($row['geom']);
                $dataRow['lat'] = $geom->coordinates[1];
                $dataRow['lon'] = $geom->coordinates[0];
                $dataRow['culprit'] = self::getCodeListValue($clist,$row['_culprit']);

                $dmgObjects = json_decode($row['_dmg_objects']);
                $skodniObjektiPoOskodovancih = self::dmg_groupObjectsByAffectee($dmgObjects);

                $affectees = json_decode($row['_affectees']);

                if (!is_array($affectees)) {
                    $affectees = [];
                }

                $rowId = $row['id'];
                $ainxHeaderInx = 0;
                foreach($affectees as $aid) {
                    $ainxHeaderInx++;
                    $key = "{$rowId}_{$aid}";
                    if (isset($agreements[$key]) && isset($agreements[$key]->_odskodnina_additional)) {
                        $additional = $agreements[$key]->_odskodnina_additional;
                        $pinxHeaderInx = 0;
                        foreach ($additional as $postavka) {
                            $pinxHeaderInx++;
                            $key = "Sporazum -> Odškodnina -> Dodatno (oškodovanec $ainxHeaderInx, postavka: $pinxHeaderInx)";
                            $header1[$key] = $key;
                            $postavkaKey = $postavka[0]->key;
                            $dataRow[$key] = $postavkaKey.($postavkaKey === 'Drugo' ? ': '.($postavka[0]->value ?? '') : '' );
                        }
                    }
                }

                $affecteeHeaderIndex=0;

                foreach($skodniObjektiPoOskodovancih as $objects) {
                    $affecteeHeaderIndex++;
                    $objectHeaderIndex = 0;
                    foreach ($objects as $objectIndex => $obj) {
                        $objectHeaderIndex++;
                        $key = "Škodni objekt ZGS (oškodovanec $affecteeHeaderIndex, objekt: $objectHeaderIndex)";
                        $header2[$key] = $key;
                        $dataRow[$key] = self::getCodeListValue($clist, $obj->dmg_object_id);

                        $objectData = $obj->data;

                        $naciniVarovanja = $objectData->_nacini_varovanja ?? [];

                        $varovanjeHeaderIndex = 0;

                        foreach ($naciniVarovanja as $nacinVarovanja) {
                            foreach($nacinVarovanja as $nacin => $lastnosti) {
                                $varovanjeHeaderIndex++;
                                $key = "Škodni objekt ZGS (oškodovanec $affecteeHeaderIndex, objekt: $objectHeaderIndex, varovanje: $varovanjeHeaderIndex)";
                                $header2[$key] = $key;
                                $dataRow[$key] = $nacin;

                                $keySteviloZic = "$key: št. žic";
                                $header2[$keySteviloZic] = $keySteviloZic;
                                $dataRow[$keySteviloZic] = $lastnosti->{"Število žic"} ?? '';

                                $keyNapetost = "$key: napetost";
                                $header2[$keyNapetost] = $keyNapetost;
                                $dataRow[$keyNapetost] = $lastnosti->{"Povprečna napetost toka [kV] (zg. meja 20 kV)"} ?? '';
                            }
                        }
                    }
                }

                $data[] = $dataRow;
            }

            sort($header2);

            $outputHeader = array_merge($header, array_values($header1), array_values($header2));

            $format = ['_dmg_examination_date' => 'date'];

            self::outputToExcel($outputHeader, $data, 'mbase-monthly-export', $format);
                
        }

        private static function dmgZapisNapake($row, $napaka) {
            return [
                'ID zapisa' => $row['id'],
                'številka škodnega zahtevka' => $row['_claim_id'],
                'napaka' => $napaka
            ];    
        }

        private static function getSpatialUnits($row) {
            $spatialUnits = [];
            foreach (json_decode($row['_x']) as $su) {
                $spatialUnits[$su->s] = $su->n;
            }
            return $spatialUnits;
        }

        private static function dmgGetCompensationSum($dmgObjects, $dmgAgreements) {
            $sum = 0.0;

            foreach($dmgObjects as $object) {
                $sum+= floatval($object->price ?? 0.0);
            }

            foreach($dmgAgreements as $a) {
                $sum+= self::dmgDodatnePostavkeCena($a);
            }

            return $sum;
        }

        private static function dmgDodatnePostavkeCena($agreement) {
            $sum = 0.0;
            foreach($agreement->_odskodnina_additional as $row) {
                $sum+= floatval($row[5][0]);
            }
            return $sum;
        }

        private static function dmgObjectsExport($oeid, $isAdmin, $claimsRes, $agreements, $users, $obmocneEnote, $clist, $affectees) {
            $statusZapisaCodeList = [
                2 => 'ZAKLJUČEN 2',
                1 => 'ZAKLJUČEN 1',
                0 => 'NEZAKLJUČEN'
            ];

            $data = [];
            $napake = [];
            $unmatchedAgreementsObjectsReport=[];

            $agreementsByClaimId = [];

            foreach ($agreements as $a) {
                $claimId = $a->_claim_id;
                if (!isset($agreementsByClaimId[$claimId])) {
                    $agreementsByClaimId[$claimId] = [];
                }

                $agreementsByClaimId[$claimId][] = $a; 
            }

            foreach($claimsRes as $row) {

                $dataRow = [];
                foreach (['_claim_id', '_dmg_examination_date'] as $key) {
                    $dataRow[$key] = $row[$key];
                }

                $dataRow['_claim_agreement_status'] = $statusZapisaCodeList[$row['_claim_agreement_status']] ?? '';

                $spatialUnits = self::getSpatialUnits($row);

                $dataRow['oe'] = $spatialUnits['SI-OE'] ?? '';
                $dataRow['luo']= $spatialUnits['SI-LUO'] ?? '';
                $dataRow['lov']= $spatialUnits['SI-LOV'] ?? '';

                foreach(['SI-OE', 'SI-LUO', 'SI-LOV'] as $key) {
                    if (isset($spatialUnits[$key])) {
                        $dataRow[$key] = $spatialUnits[$key];
                    }
                    else {
                        $napake[] = self::dmgZapisNapake($row, "$key ni določen.");
                    }
                }

                $geom = json_decode($row['geom']);
                $dataRow['lat'] = $geom->coordinates[1];
                $dataRow['lon'] = $geom->coordinates[0];
                $dataRow['culprit'] = self::getCodeListValue($clist,$row['_culprit']);

                $dmgObjects = json_decode($row['_dmg_objects']);
                $skodniObjektiPoOskodovancih = self::dmg_groupObjectsByAffectee($dmgObjects);
                $unmatchedObjects = self::dmg_damageObjectPrices($skodniObjektiPoOskodovancih, $row['id'], $agreements);
                self::unmatchedObjectsReport($row, $oeid, $isAdmin, $affectees, $unmatchedObjects, $unmatchedAgreementsObjectsReport);
                $dataRow['_compensation_sum'] = self::dmgGetCompensationSum($dmgObjects, $agreementsByClaimId[$row['id']] ?? []);

                $keysBefore = array_keys($dataRow);
                
                foreach ($dmgObjects as $object) {
                    $dataRow['dodatna_postavka'] = 'NE';
                    $dataRow['affectee'] = '';
                    $dataRow['dmg_object_zgs'] = '';
                    $dataRow['dmg_object_arso_full'] = '';
                    $dataRow['dmg_object_arso_last'] = '';
                    $dataRow['dmg_object_quantity'] = '';
                    $dataRow['dmg_object_unit'] = '';
                    $dataRow['dmg_object_compensation'] = '';
                    $dataRow['dmg_object_varovanje'] = '';
                    $dataRow['dmg_object_varovanje_min_height'] = '';
                    $dataRow['dmg_object_varovanje_n_wires'] = '';
                    $dataRow['dmg_object_varovanje_elektrika'] = '';
                    $dataRow['dmg_object_varovanje_napetost'] = '';
                    $dataRow['dmg_object_varovanje_damage_on_electrofence'] = '';
                    $dataRow['dmg_object_varovanje_vzdrzevano'] = '';
                    $dataRow['dmg_object_varovanje_sent_mail'] = 'NE';
                    $dataRow["status_sporazuma"] = '';
                    $dataRow["sporazum_zakljucen"] = '';


                    $objectData = $object->data;
                    $affecteeId = $objectData->affectee;

                    $naslov='';
                    if (isset($affectees[$affecteeId])) {
                        $naslov = $affectees[$affecteeId]['naslov'];
                    }
                    else {
                        $napake[] = self::dmgZapisNapake($row, "Napačen ID oškodovanca: $affecteeId.");
                    }
                    
                    $dataRow['affectee'] = (is_null($oeid) && !$isAdmin) ? '*****' : $naslov;
                    $dataRow['dmg_object_zgs'] = self::getCodeListValue($clist, $object->dmg_object_id);
                    $dataRow['dmg_object_arso_full'] = $object->text;
                    $dataRow['dmg_object_arso_last'] = $object->objectKey;
                    
                    $dataRow['dmg_object_quantity'] = $objectData->kolicina;
                    $dataRow['dmg_object_unit'] = $objectData->enota;
                    $dataRow['dmg_object_compensation'] = $object->price ?? '';
                    
                    $naciniVarovanja = $objectData->_nacini_varovanja ?? [];

                    $prvoVarovanje = empty($naciniVarovanja) ? (object)[] : $naciniVarovanja[0];
                    
                    foreach ($prvoVarovanje as $nacin => $lastnosti) {
                        $dataRow['dmg_object_varovanje'] = $nacin;
                        
                        $minHeight = $lastnosti->{"Najnižja višina ograje"} ?? '';
                        if (empty($minHeight)) {
                            $minHeight = $lastnosti->{"Najnižja višina zgornje žice"} ?? '';
                        }

                        $dataRow['dmg_object_varovanje_min_height'] = $minHeight;
                        $dataRow['dmg_object_varovanje_n_wires'] = $lastnosti->{"Število žic"} ?? '';
                        $napetost = $lastnosti->{"Povprečna napetost toka [kV] (zg. meja 20 kV)"} ?? '';
                        $dataRow['dmg_object_varovanje_elektrika'] = empty($napetost) ? 'NE' : 'DA';
                        $dataRow['dmg_object_varovanje_napetost'] = $lastnosti->{"Povprečna napetost toka [kV] (zg. meja 20 kV)"} ?? '';
                        $dataRow['dmg_object_varovanje_damage_on_electrofence'] = strtolower($nacin) === 'elektromreža' && in_array($minHeight, ['140 - 145 cm', '160 - 170 cm']) ? 'DA' : 'NE';

                        $dataRow['dmg_object_varovanje_vzdrzevano'] = strtoupper($lastnosti->{"Ustrezna vzdrževanost"} ?? '');
                        break;
                    }

                    $agreement = $agreements[$row['id'].'_'.$affecteeId] ?? null;
                    $dataRow["status_sporazuma"] = is_null($agreement) ? '' : $agreement->status_sporazuma;
                    $dataRow["sporazum_zakljucen"] = is_null($agreement) ? 'NE' : $agreement->sporazum_zakljucen;

                    $data[] = $dataRow;
                }

                $keysAfter = array_keys($dataRow);

                foreach(array_diff($keysAfter, $keysBefore) as $key) {
                    $dataRow[$key] = '';
                }

                if (isset($agreementsByClaimId[$row['id']])) {
                    $claimAgreements = $agreementsByClaimId[$row['id']];
                    
                    $dataRow['affectee'] = '';
                    $dataRow['status_sporazuma'] = '';
                    $dataRow['sporazum_zakljucen'] = '';
                    $dataRow['dodatna_postavka'] = 'NE';
                    $dataRow['dmg_object_quantity'] = '';
                    $dataRow['dmg_object_unit'] = '';
                    $dataRow['dmg_object_compensation'] = '';


                    foreach($claimAgreements as $agreement) {
                        $additional = $agreement->_odskodnina_additional;
                        if (empty($additional)) {
                            continue;
                        }

                        $naslov='';
                        $affecteeId = $agreement->_affectee;
                        if (isset($affectees[$affecteeId])) {
                            $naslov = $affectees[$affecteeId]['naslov'];
                        }

                        $dataRow['affectee'] = (is_null($oeid) && !$isAdmin) ? '*****' : $naslov;
                        $dataRow["status_sporazuma"] = is_null($agreement) ? '' : $agreement->status_sporazuma;
                        $dataRow["sporazum_zakljucen"] = is_null($agreement) ? 'NE' : $agreement->sporazum_zakljucen;

                        foreach($additional as $postavka) {
                            if (!isset($postavka[0]->key)) continue;
                            $postavkaKey = $postavka[0]->key;
                            $dataRow['dodatna_postavka'] = $postavkaKey.($postavkaKey === 'Drugo' ? ': '.($postavka[0]->value ?? '') : '' );
                            $dataRow['dmg_object_quantity'] = $postavka[1] ?? '';
                            $dataRow['dmg_object_unit'] = $postavka[2] ?? '';
                            $compensation = is_array($postavka[5]) ? $postavka[5][0] : $postavka[5];
                            $dataRow['dmg_object_compensation'] = $compensation;
                            $data[] = $dataRow;
                        }
                    }
                }
            }
            
            $header = [
                ['_claim_id' , 'ŠTEVILKA ŠKODNEGA DOGODKA'],
                ['_claim_agreement_status', 'Status zapisa v bazi'],
                ['status_sporazuma', 'Status sporazuma'],
                ['sporazum_zakljucen', 'Ali je sporazum zaključen?'],
                ['oe' , 'OE lokacije škodnega dogodka'],
                ['luo' , 'LUO'],
                ['lov' , 'LOVIŠČE'],
                ['_dmg_examination_date' , 'Datum ogleda škode'],
                ['lat' , 'Zemlj.širina WGS84'],
                ['lon' , 'Zemlj.dolžina WGS84'],
                ['culprit' , 'POVZROČITELJ'],
                ['affectee' , 'OŠKODOVANEC'],
                ['dodatna_postavka', 'Dodatna postavka'],
                ['dmg_object_zgs' , 'ŠKODNI OBJEKT ZGS'],
                ['dmg_object_arso_full' , 'ŠKODNI OBJEKT ARSO_full'],
                ['dmg_object_arso_last' , 'ŠKODNI OBJEKT ARSO_zadnja kat'],
                ['dmg_object_quantity' , 'KOLIČINA'],
                ['dmg_object_unit' , 'ENOTA'],
                ['dmg_object_compensation' , 'VREDNOST ŠKODNEGA OBJEKTA'],
                ['_compensation_sum' , 'Vrednost škodnega dogodka'],
                ['dmg_object_varovanje' , 'VAROVANJE'],
                ['dmg_object_varovanje_min_height' , 'Najnižja višina varovanja'],
                ['dmg_object_varovanje_n_wires' , 'ELEKTROOGRAJA_ŠTEVILO ŽIC'],
                ['dmg_object_varovanje_elektrika' , 'PRISOTNOST ELEKTRIKE'],
                ['dmg_object_varovanje_napetost' , 'NAPETOST'],
                ['dmg_object_varovanje_damage_on_electrofence' , 'ŠKODA NA visoki elektromreži'],
                ['dmg_object_varovanje_vzdrzevano' , 'Ustrezna vzdrževanost varovanja'],
                ['dmg_object_varovanje_sent_mail' , 'Poslano obvestilo administratorjem'],

            ];

            $format = ['_dmg_examination_date'=>'date'];

            self::dmgOutputToExcel('mbase-dmg-objects-export', $header, $data, $format, $napake, $unmatchedAgreementsObjectsReport);
        }

        private static function outputSheetsToExcel($outputSheetsData, $fileName) {
            if (empty($outputSheetsData)) return;

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $outputData = $outputSheetsData[0];

            if (isset($outputData['name'])) {
                $sheet->setTitle($outputData['name']);
            }

            self::outputToSpreadsheet($outputData['header'], $outputData['data'], $outputData['format'] ?? [], ['spreadsheet' => $spreadsheet, 'sheet' => $sheet]);

            for($i=1; $i < count($outputSheetsData); $i++) {
                $outputData = $outputSheetsData[$i];
                $sheet = self::createSheet($outputData['name'], $spreadsheet);

                self::outputToSpreadsheet($outputData['header'], $outputData['data'], $outputData['format'] ?? [], [
                    'spreadsheet' => $spreadsheet,
                    'sheet' => $sheet
                ]);
            }
        
            self::streamSpreadsheetToExcel($spreadsheet, $fileName);
        }

        private static function dmgDeputyData($row, $users, $obmocneEnote) {

            $claimOE = '';
            if (!empty($row['_claim_id'])) {
                $claimOE = explode('/',$row['_claim_id']);
                if (count($claimOE)>0) {
                    $claimOE = ltrim($claimOE[0],'0');
                }
            }
                
            $deputy = Mbase2Utils::getProxyKeyValueInArray($row, '_deputy',$users);
            
            return [
                isset($obmocneEnote[$claimOE]) ? $obmocneEnote[$claimOE]['_oe_ime'] : '',
                !empty($deputy) ? $deputy['name'].' ('.$deputy['username'].')' : ''
            ];
        }

        private static function unmatchedObjectsReport($row, $oeid, $isAdmin, $affectees, $unmatchedObjects, &$unmatchedAgreementsObjectsReport) {
            if (!empty($unmatchedObjects)) {
                foreach($unmatchedObjects as $obj) {
                    $objectDataTemplate = [
                        'id' => $row['id'],
                        'claim_id' => $row['_claim_id'],
                        'affectee' => (is_null($oeid) && !$isAdmin) ? '*****' : $affectees[$obj['affecteeId']]['naslov'],
                    ];

                    foreach($obj['unmatchedObjects'] as $objectData) {
                        $e = $objectDataTemplate;
                        foreach($objectData as $objDataKey=>$value) {
                            $e[$objDataKey] = $value;
                        }
                        $unmatchedAgreementsObjectsReport[] = $e;
                    }
                }
            }
        }

        private static function dmgFullExport(
            $oeid,
            $isAdmin,
            $claimsRes,
            $agreements,
            $users,
            $obmocneEnote,
            $clist,
            $affectees,
            $proj4,
            $projFrom,
            $projTo
        ) {
            $output = [];
            $napake = [];

            $expectedEventDomain = [
                1 => 'DA', 
                2 => 'NE'
            ];

            $mainHeader = [
                'ID_record',
                'Koda škodnega zahtevka',
                'Povezava do zahtevka (URL)',
                'Ali je bil dogodek objektivno pričakovan?',
                ['SI-OE','OE lokacije škodnega primera'],
                'OE pooblaščenca',
                'Pooblaščenec',
                ['SI-OB','Občina'],
                ['SI-LUO','LUO'],
                ['SI-LOV','Lovišče'],
                'Povzročitelj',
                'Povzročitelj - navedba',
                'Datum ogleda škode',
                'Zemlj.širina D48/GK',
                'Zemlj.dolžina D48/GK',
                'Zemlj.širina WGS84',
                'Zemlj.dolžina WGS84',
                'Oddaljenost od naselja (hiše)',
                'Lokalno ime',
                "Škodni objekti ZGS (vsi oškodovanci)",
                "Odškodnina skupaj",
                "Odvzet vzorec za DNK (DA/NE)",
                "Genetika",
                "Opombe"
            ];

            $unmatchedAgreementsObjectsReport = [];
            
            foreach ($claimsRes as $row) {
                $outputRow = [];
                $outputRow['ID_record'] = $row['id'];
                $outputRow['Koda škodnega zahtevka'] = $row['_claim_id'];

                $outputRow['Povezava do zahtevka (URL)'] = 'https://portal.mbase.org/mbase2/modules/dmg/zahtevek?fid='.$row['id'];

                $outputRow['Ali je bil dogodek objektivno pričakovan?'] = Mbase2Utils::getProxyKeyValueInArray($row, '_expected_event',$expectedEventDomain);

                $spatialData = json_decode($row['_location_data']);
                
                $spatialUnits = self::getSpatialUnits($row);

                foreach(['SI-OE', 'SI-LUO', 'SI-LOV', 'SI-OB'] as $key) {
                    if (isset($spatialUnits[$key])) {
                        $outputRow[$key] = $spatialUnits[$key];
                    }
                    else {
                        $napake[] = self::dmgZapisNapake($row, "$key ni določen.");
                    }
                }

                list($oePooblascenca, $pooblascenec) = self::dmgDeputyData($row, $users, $obmocneEnote);

                $outputRow['OE pooblaščenca'] = $oePooblascenca;
                $outputRow['Pooblaščenec'] = $pooblascenec;

                $outputRow['Povzročitelj'] = self::getCodeListValue($clist,$row['_culprit']);
                $outputRow['Povzročitelj - navedba'] = $outputRow['Povzročitelj'] != $row['_selected_culprit'] ? $row['_selected_culprit'] : '';
                $outputRow['Datum ogleda škode'] = $row['_dmg_examination_date'];

                $pointDest = (object)['x' => null, 'y' => null];
                
                try {
                    $pointSrc = new Point($spatialData->lon, $spatialData->lat, $projFrom);
                    $pointDest=$proj4->transform($projTo, $pointSrc);
                } catch (Exception $e) {
                    ;
                }

                $outputRow['Zemlj.širina D48/GK'] = $pointDest->y;
                $outputRow['Zemlj.dolžina D48/GK'] = $pointDest->x;
                $outputRow['Zemlj.širina WGS84'] = $spatialData->lat;
                $outputRow['Zemlj.dolžina WGS84'] = $spatialData->lon;

                $outputRow['Oddaljenost od naselja (hiše)'] = isset($spatialData->additional) ? $spatialData->additional->sdist : null;
                $outputRow['Lokalno ime'] = isset($spatialData->additional) ? $spatialData->additional->lname : null; 

                $skodniObjektiPoOskodovancih = self::dmg_groupObjectsByAffectee(json_decode($row['_dmg_objects']));

                $unmatchedObjects = self::dmg_damageObjectPrices($skodniObjektiPoOskodovancih, $row['id'], $agreements);
                self::unmatchedObjectsReport($row, $oeid, $isAdmin, $affectees, $unmatchedObjects, $unmatchedAgreementsObjectsReport);

                $oskodovanci = json_decode($row['_affectees']);
                $odskodninaSkupaj = 0.0;

                $naciniVarovanjaPoOskodovancihInSkodnihObjektih = [];

                $outputRow["Škodni objekti ZGS (vsi oškodovanci)"] = '';

                $skodniObjektiZGSvsi = [];

                if (empty($row['_batch_id'])) {
                    foreach ($oskodovanci as $inx => $aid) {
                        $ainx = $inx+1;
                        $naciniVarovanjaPoOskodovancihInSkodnihObjektih[$ainx] = [];
                        $outputRow["Oškodovanec ($ainx)"] = (is_null($oeid) && !$isAdmin) ? '*****' : $affectees[$aid]['naslov'];
                        $skodniObjektiOskodovanca = isset($skodniObjektiPoOskodovancih[$aid]) ? $skodniObjektiPoOskodovancih[$aid] : [];

                        $agreement = $agreements[$row['id'].'_'.$aid] ?? null;

                        $kodaStatusa = is_null($agreement) ? null : $agreement->_status;
                        
                        $outputRow["Status sporazuma ($ainx)"] = is_null($agreement) ? '' : $agreement->status_sporazuma;
                        $outputRow["Ali je sporazum zaključen? ($ainx)"] = is_null($agreement) ? 'NE' : $agreement->sporazum_zakljucen;

                        $price = 0.0;

                        $ooinx = 1;
                        
                        foreach ($skodniObjektiOskodovanca as $oid => $skodniObjekt) {
                            $data = $skodniObjekt->data;
                            
                            $naciniVarovanjaPoOskodovancihInSkodnihObjektih[$ainx][$ooinx] = isset($data->_nacini_varovanja) ? $data->_nacini_varovanja : [];

                            $skodniObjektZgsKey = $skodniObjekt->zgsKey;
                            
                            if ($skodniObjektZgsKey == 'Drugo' && isset($data->navedba) && (!empty($data->navedba))) {
                                $skodniObjektZgsKey = "Drugo ($data->navedba)";
                            }

                            $outputRow["Škodni objekt ZGS (oškodovanec: $ainx, objekt: $ooinx)"] = $skodniObjektZgsKey;
                            $skodniObjektiZGSvsi[$skodniObjektZgsKey] = isset($skodniObjektiZGSvsi[$skodniObjektZgsKey]) ? $skodniObjektiZGSvsi[$skodniObjektZgsKey] + 1 : 1;
                            $outputRow["Škodni objekt (oškodovanec: $ainx, objekt: $ooinx)"] = $skodniObjekt->text;
                            $outputRow["Količina (oškodovanec: $ainx, objekt: $ooinx)"] = $data->kolicina;
                            $outputRow["Enota (oškodovanec: $ainx, objekt: $ooinx)"] = $data->enota;

                            if (!isset($skodniObjekt->price)) $skodniObjekt->price = 0.0;
                            $outputRow["Odškodnina (oškodovanec: $ainx, objekt: $ooinx)"] = $skodniObjekt->price;
                            $price = $price + floatval($skodniObjekt->price);

                            $ooinx++;
                        }

                        //dodatna škoda za posameznega oškodovanca $ainx
                        
                        $odskodninaDodatno = 0.0;
                        $additional = isset($agreement->_odskodnina_additional) ? $agreement->_odskodnina_additional : [];

                        $additionalItems = [];

                        foreach($additional as $arow) {
                            if (isset($arow->row)) {
                                $odskodninaDodatno = $odskodninaDodatno + floatval($arow->row[4]);
                                $additionalItems[] = $arow->row[0];
                            }
                            else if ( isset( $arow[5] ) && isset( $arow[5][0] ) && isset($arow[0]->key)) {
                                $odskodninaDodatno = $odskodninaDodatno + floatval($arow[5][0]);
                                $additionalItems[] = isset($arow[0]->value) ? $arow[0]->value : $arow[0]->key;
                            }
                        }
                        
                        $outputRow["Škoda dodatno (oškodovanec: $ainx)"] = implode(';', $additionalItems);
                        
                            $outputRow["Odškodnina dodatno (oškodovanec: $ainx)"] = $odskodninaDodatno;
                            $outputRow["Odškodnina skupaj (oškodovanec: $ainx)"] = $odskodninaDodatno + $price;

                            $odskodninaSkupaj = $odskodninaSkupaj + $odskodninaDodatno + $price;
                    }
                    $outputRow["Odškodnina skupaj"] = $odskodninaSkupaj;
                }

                $tmpOut = [];
                foreach ($skodniObjektiZGSvsi as $objekt => $steviloPrimerov) {
                    $tmpOut[] = "$objekt ($steviloPrimerov)";
                }

                $outputRow["Škodni objekti ZGS (vsi oškodovanci)"] = implode(', ', $tmpOut);

                foreach ($naciniVarovanjaPoOskodovancihInSkodnihObjektih as $ainx => $naciniVarovanjaPoSkodnihObjektihOskodovanca) {
                    if (empty($naciniVarovanjaPoSkodnihObjektihOskodovanca)) continue;
                    foreach ($naciniVarovanjaPoSkodnihObjektihOskodovanca as $ooinx => $naciniVarovanjaPoSkodnemObjektu) {
                        $nvinx = 1;
                        if (empty($naciniVarovanjaPoSkodnemObjektu)) continue;
                        foreach ($naciniVarovanjaPoSkodnemObjektu as $nacinVarovanja) {
                            $imeVarovanja = array_keys(get_object_vars($nacinVarovanja))[0];
                            $outputRow["Varovanje (oškodovanec: $ainx, objekt: $ooinx, varovanje: $nvinx)"] = $imeVarovanja;
                            $outputRow["Lastnosti varovanja (oškodovanec: $ainx, objekt: $ooinx, varovanje: $nvinx)"] = json_encode($nacinVarovanja->$imeVarovanja);
                            $nvinx++;
                        }
                    }
                }
                
                $outputRow["Odškodnina skupaj"] = $odskodninaSkupaj;

                $tmpOut = [];
                foreach ($skodniObjektiZGSvsi as $objekt => $steviloPrimerov) {
                    $tmpOut[] = "$objekt ($steviloPrimerov)";
                }

                $outputRow["Škodni objekti ZGS (vsi oškodovanci)"] = implode(', ', $tmpOut);

                foreach ($naciniVarovanjaPoOskodovancihInSkodnihObjektih as $ainx => $naciniVarovanjaPoSkodnihObjektihOskodovanca) {
                    if (empty($naciniVarovanjaPoSkodnihObjektihOskodovanca)) continue;
                    foreach ($naciniVarovanjaPoSkodnihObjektihOskodovanca as $ooinx => $naciniVarovanjaPoSkodnemObjektu) {
                        $nvinx = 1;
                        if (empty($naciniVarovanjaPoSkodnemObjektu)) continue;
                        foreach ($naciniVarovanjaPoSkodnemObjektu as $nacinVarovanja) {
                            $imeVarovanja = array_keys(get_object_vars($nacinVarovanja))[0];
                            $hkey="Varovanje (oškodovanec: $ainx, objekt: $ooinx, varovanje: $nvinx)";
                            $outputRow[$hkey] = $imeVarovanja;
                            $hkey="Lastnosti varovanja (oškodovanec: $ainx, objekt: $ooinx, varovanje: $nvinx)";
                            $outputRow[$hkey] = json_encode($nacinVarovanja->$imeVarovanja);
                            $nvinx++;
                        }
                    }
                }
                
                $geneticSamples = !empty($row['_genetic_samples']) ? json_decode($row['_genetic_samples']) : [];

                $outputRow["Odvzet vzorec za DNK (DA/NE)"] = count($geneticSamples) > 0 ? 'DA' : 'NE';

                $outputRow["Opombe"] = $row['_deputy_notes'];

                foreach ($geneticSamples as &$sample) {
                    if (isset($sample[1])) {
                        $sample[1] = self::getCodeListValue($clist,$sample[1]);
                    }
                }

                $outputRow["Genetika"] = json_encode($geneticSamples, JSON_UNESCAPED_UNICODE);

                $output[] = $outputRow;
            }

            $headers = empty($output) ? [] : self::retrieveDistinctKeys($output);
            
            $auxHeader = self::sortAuxHeader($headers);
            
            //array_splice($mainHeader, array_search("Odškodnina skupaj", $mainHeader), 0, $oskodovanciHeader);
            //array_splice($mainHeader, array_search("Odvzet vzorec za DNK (DA/NE)", $mainHeader), 0, $varovanjaHeader);

            $header = array_merge($mainHeader, $auxHeader);

            $outputHeader = [];

            foreach($header as $key) { //all keys
                
                $label = $key;
                if (is_array($key)) {
                    $label = $key[1];
                    $key = $key[0];
                }

                foreach ($output as $outRow) {
                    if (isset($outRow[$key])) {
                        $outputHeader[] = [$key, $label];
                        break;
                    }
                }
            }

            $format = ['Datum ogleda škode' => 'date'];

            self::dmgOutputToExcel('mbase-dmg-full-export', $outputHeader, $output, $format, $napake, $unmatchedAgreementsObjectsReport);
        }

        private static function retrieveDistinctKeys($output) {
            $keys = [];

            foreach($output as $row) {
                foreach(array_keys($row) as $key) {
                    $keys[$key] = True;
                }
            }

            return array_keys($keys);
        }

        private static function dmgOutputToExcel($fileName, $outputHeader, $output, $format, $napake, $unmatchedAgreementsObjectsReport) {
            if (empty($napake) && empty($unmatchedAgreementsObjectsReport)) {
                self::outputToExcel($outputHeader, $output, $fileName, $format);
            }
            else {
                $excelOutput = [[
                    'header' => $outputHeader,
                    'data' => $output,
                    'format' => $format
                ]];

                if (!empty($napake)) {
                    $excelOutput[] = [
                        'header' => array_keys($napake[0]),
                        'data' => $napake,
                        'name' => 'NAPAKE'
                    ];
                }

                if (!empty($unmatchedAgreementsObjectsReport)) {
                    $excelOutput[] = [
                        'header' => array_keys($unmatchedAgreementsObjectsReport[0]),
                        'data' => $unmatchedAgreementsObjectsReport,
                        'name' => 'Neznani objekti v sporazumu'
                    ];
                }

                self::outputSheetsToExcel($excelOutput, $fileName.'.xlsx');
            }
        }

        private static function ctExport($dbFunctionParameters) {
            $languageCode = Mbase2Drupal::getCurrentLanguageCode();

            $attributes = Mbase2Utils::getModuleAttributes('ct', [], $languageCode, false);

            $attributes=array_column($attributes, null, 'variable_name');

            $fileProperties = Mbase2Database::query("SELECT fp.*, up.file_name from mbase2.file_properties fp, mbase2.uploads up where up.file_hash = fp.file_hash and up.subfolder='sop'");
            $filePropertiesByHash = array_column($fileProperties, null,'file_hash');

            $res = call_user_func_array(['Mbase2Database', 'select'], $dbFunctionParameters);

            $output = [];

            $additionalColumns = []; //columns parsed out of json fields

            foreach($res as $row) {
                $outputRow=[];
                foreach($row as $key=>$value) {
                    if ($key==='id') {
                        $outputRow[$key] = $value;
                        continue;
                    }
                    else if ($key==='geom') {
                        $value = json_decode($value);
                        $c = $value->coordinates;
                        $outputRow['lat'] = $c[1];
                        $outputRow['lon'] = $c[0];
                    }
                    
                    if (!isset($attributes[$key])) {
                        continue;
                    }
                    
                    $a = $attributes[$key];
                    
                    if (Mbase2Utils::isReference($a['data_type'])) {
                        $outputRow[$key] = Mbase2Utils::replaceCodeListValuesWithTranslation($a, $value);
                    }
                    else if ($a['data_type'] === 'location_geometry') {
                        continue;
                    }
                    else if ($a['variable_name'] === 'photos' && !empty($value)) {
                        $v = [];
                        $fileNames = [];
                        $photos = json_decode($value);

                        foreach ($photos as $fileHash) {
                            if (isset($filePropertiesByHash[$fileHash])) {
                                if (isset($filePropertiesByHash[$fileHash]['properties'])) {
                                    $p = json_decode($filePropertiesByHash[$fileHash]['properties']);
                                    $folder = 'm'.substr($fileHash,0,2);
                                    $v[] = "https://portal.mbase.org/api/mbase2/uploaded-file/private/ct/$folder/$fileHash/".$p->type;
                                    $fileNames[] = $filePropertiesByHash[$fileHash]['file_name'];
                                };
                            }
                        }

                        $outputRow[$key] = implode(',', $v);
                        $outputRow['__file_names'] = implode(',', $fileNames);

                    }
                    else {
                        $outputRow[$key] = $value;
                    }
                }
                $output[] = $outputRow;
            }

            $header = [['id', 'ID']];
            $header[] = ['lat', 'lat'];
            $header[] = ['lon', 'lon'];

            $variableClo = Mbase2Database::query("SELECT * from mbase2.code_list_options_vw WHERE list_key='variables'");
            $variableCloByKey = array_column($variableClo, null, 'key');  //variables code list options
            $variableClo = array_column($variableClo, null, 'id');  //variables code list options

            /**
             * create headers
             */
            foreach ($attributes as $a) {

                if (in_array($a['data_type'], ['location_data_json', 'location_geometry']) !== FALSE) continue;

                $trans = [];
                
                if (isset($a['translations'])) {
                    $trans = json_decode($a['translations'], true);
                }

                if (isset($trans[$languageCode])) {
                    $cname = $trans[$languageCode];
                }
                else {
                    $cname = $a['variable_name'];
                }
                
                $header[] = [$a['variable_name'], $cname]; //cname === $a['variable_name'] if translations does not exist

                if ($a['variable_name']==='photos') {
                    $header[] = ['__file_names', 'Original file name'];
                }
            }

            foreach($additionalColumns as $key) {
                $trans = [];

                if (isset($variableCloByKey[$key]) && isset($variableCloByKey[$key]['translations'])) {
                    $trans = json_decode($variableCloByKey[$key]['translations'], true);
                }

                $header[] = [$key, isset($trans[$languageCode]) ? $trans[$languageCode] : $key];
            }

            self::outputToExcel($header, $output);
        }

        private static function urls(&$outputRow, $key, $value, $filePropertiesByHash, $moduleKey) {
            $v = [];
            $fileNames = [];
            $photos = json_decode($value);

            foreach ($photos as $fileHash) {
                if (isset($filePropertiesByHash[$fileHash])) {
                    if (isset($filePropertiesByHash[$fileHash]['properties'])) {
                        $p = json_decode($filePropertiesByHash[$fileHash]['properties']);
                        $folder = 'm'.substr($fileHash,0,2);
                        $v[] = "https://portal.mbase.org/api/mbase2/uploaded-file/private/$moduleKey/$folder/$fileHash/".(isset($p->type) ? $p->type : '');
                        $fileNames[] = $filePropertiesByHash[$fileHash]['file_name'];
                    };
                }
            }

            $outputRow[$key] = implode(',', $v);
            $outputRow['__file_names'] = implode(',', $fileNames);

        }

        private static function sopExport($dbFunctionParameters) {

            $languageCode = Mbase2Drupal::getCurrentLanguageCode();

            $attributes = Mbase2Utils::getModuleAttributes('sop', [], $languageCode, false);

            $attributes=array_column($attributes, null, 'variable_name');

            $filePropertiesByHash = Mbase2Utils::getFilePropertiesByHash('sop');

            $res = call_user_func_array(['Mbase2Database', 'select'], $dbFunctionParameters);

            $output = [];

            $additionalColumns = []; //columns parsed out of json fields

            foreach($res as $row) {
                $outputRow=[];
                foreach($row as $key=>$value) {
                    if ($key==='id') {
                        $outputRow[$key] = $value;
                        continue;
                    }
                    else if ($key === 'geom') {
                        $value = json_decode($value);

                        if (!empty($value)) {
                            $coords = $value->coordinates;
                            $outputRow['lat'] = $coords[1];
                            $outputRow['lon'] = $coords[0];
                            
                            $additionalColumns['lat'] = 'lat';
                            $additionalColumns['lon'] = 'lon';
                        }
                        continue;
                    }
                    else if (!isset($attributes[$key])) {
                        continue;
                    }
                    
                    $a = $attributes[$key];
                    
                    if (Mbase2Utils::isReference($a['data_type'])) {
                        $outputRow[$key] = Mbase2Utils::replaceCodeListValuesWithTranslation($a, $value);
                    }
                    else if ($a['data_type'] === 'location_geometry') {
                        continue;
                    }
                    else if ($a['data_type'] === 'location_data_json') {
                        $value = json_decode($value);
                        
                        if (isset($value->spatial_request_result)) {
                            foreach ($value->spatial_request_result as $k => $v) {
                                $v = trim($v);
                                if (empty($v)) continue;
                                $outputRow[$k] = $v;
                                $additionalColumns[$k] = $k;
                            }
                        }

                        if (isset($value->additional)) {
                            foreach($value->additional as $k=>$v) {
                                $v = trim($v);
                                if (empty($v)) continue;
                                $outputRow[$k] = $v;
                                $additionalColumns[$k] = $k;
                            }
                        }

                        /*
                        foreach (['lat', 'lon'] as $k) {
                            if (isset($value->$k)) {
                                $v = trim($value->$k);
                                if (empty($v)) continue;
                                $outputRow[$k] = $v;
                                $additionalColumns[$k] = $k;
                            }
                        }
                        */
                    }
                    else if ($a['variable_name'] === '_photos' && !empty($value)) {
                        self::urls($outputRow, $key, $value, $filePropertiesByHash, 'sop');

                    }
                    else {
                        $outputRow[$key] = $value;
                    }
                }
                $output[] = $outputRow;
            }

            $header = [['id', 'ID']];

            $variableClo = Mbase2Database::query("SELECT * from mbase2.code_list_options_vw WHERE list_key='variables'");
            $variableCloByKey = array_column($variableClo, null, 'key');  //variables code list options
            $variableClo = array_column($variableClo, null, 'id');  //variables code list options

            /**
             * create headers
             */
            foreach ($attributes as $a) {

                if (in_array($a['data_type'], ['location_data_json', 'location_geometry']) !== FALSE) continue;

                $trans = [];
                
                if (isset($a['translations'])) {
                    $trans = json_decode($a['translations'], true);
                }

                if (isset($trans[$languageCode])) {
                    $cname = $trans[$languageCode];
                }
                else {
                    $cname = $a['variable_name'];
                }

                $header[] = [$a['variable_name'], $cname]; //cname === $a['variable_name'] if translations does not exist

                if ($a['variable_name']==='_photos') {
                    $header[] = ['__file_names', 'Original file name'];
                }
            }

            foreach($additionalColumns as $key) {
                $trans = [];

                if (isset($variableCloByKey[$key]) && isset($variableCloByKey[$key]['translations'])) {
                    $trans = json_decode($variableCloByKey[$key]['translations'], true);
                }

                $header[] = [$key, isset($trans[$languageCode]) ? $trans[$languageCode] : $key];
            }

            self::outputToExcel($header, $output);
        }

        private static function afterSelect($moduleKey, $res) {
            
            if ($moduleKey == 'dmg') {
                $agreementStatuses = [
                    1 => 'Sporazum JE dosežen.',
                    2 => 'Sporazum NI dosežen',
                    3 => 'Obrazec 2.3 - vloga stranke'
                ];

                $dmgObjectsById = [];
                $dodatnePostavkeCompensationByAffecteeId = [];
                $dodatnePostavkeCompensationById = [];

                foreach($res as &$row) {
                    $id = $row['id'];

                    if ($id < 1) {
                        continue;
                    }

                    $adata = $row['agreement_data'];
                    $adata = empty($adata) ? (object)[] : json_decode($adata);
                    $row['agreement_data'] = $adata;

                    $aid = $adata->_affectee ?? null;

                    if (isset($row['_dmg_objects'])) {
                        $oid = $row['oid'];

                        $dmgObjects = $dmgObjectsById[$id] ?? json_decode($row['_dmg_objects']);
                        
                        if (!isset($dmgObjectsById[$id])) {
                            $dmgObjectsById[$id] = $dmgObjects;
                        }

                        foreach ($dmgObjects as &$object) {
                            if ($oid == $object->oid) {
                                if (empty($adata) || !(isset($adata->_odskodnina))) {
                                    $object->price = self::dmg_object_price($object);
                                }
                                else {
                                    $objectByObjectId = [$oid => $object];
                                    self::handleUnmatchedAgreementObjects($objectByObjectId, $adata, $unmatchedAgreementsObjectsReport);
                                }
                                break;
                            }
                        }
                        unset($object);
                    }

                    if (!isset($dodatnePostavkeCompensationById[$id])) {
                        $dodatnePostavkeCompensationById[$id] = 0.0;
                    }

                    if (!empty($aid) && !isset($dodatnePostavkeCompensationByAffecteeId[$id.'_'.$aid])) {
                        $cenaDodatnihPostavk = self::dmgDodatnePostavkeCena($adata);
                        $dodatnePostavkeCompensationByAffecteeId[$id.'_'.$aid] = $cenaDodatnihPostavk;
                        $dodatnePostavkeCompensationById[$id]+= $cenaDodatnihPostavk;
                    }

                    if (isset($adata->_status) && !empty($adata->_status)) {
                        $row['_agreement_status'] = $agreementStatuses[$adata->_status];
                    }
                }
                
                unset($row);

                foreach($res as &$row) {
                    $id = $row['id'];
                    if ($id < 1) {
                        continue;
                    }
                    $oid = $row['oid']; //= id vpisa v zapisnik

                    $dmgObjects = $dmgObjectsById[$id] ?? [];

                    if (!isset($compensationByDmgId[$id])) {
                        $sum = 0.0;
                        foreach($dmgObjects as $o) {
                            $sum+= floatval($o->price ?? 0.0);
                        }
                        $compensationByDmgId[$id] = $sum + ($dodatnePostavkeCompensationById[$id] ?? 0.0);
                    }

                    $row['_compensation_sum'] = $compensationByDmgId[$id];

                    foreach($dmgObjects as $o) {
                        if ($o->oid == $oid) {
                            $row['_compensation_sum_for_object'] = floatval($o->price ?? 0.0);
                            break;
                        }
                        
                    }
                }

                unset($row);
            }

            return $res;
        }

        private static function beforeSelect($moduleKey) {
            $batchImportColumns = '';
            $batchImportJoin = '';

            $rows = \DB::select("select * from mbase2.code_list_options_vw where list_key='yes_no'");

            $yes_no = [];

            foreach($rows as $row) {
                $yes_no[$row->key] = $row->id;
            }

            if ($moduleKey == 'dmg') { //_dmg_object_list_ids
                $batchImportColumns = ','.implode(',',[
                    "CASE WHEN vw.id < 0 THEN dbi.priimek_ime_oskodovanca_gs ELSE da._full_name END priimek_ime_oskodovanca_gs",
                    "CASE WHEN vw.id < 0 THEN dbi.naslov_gs ELSE da._street || ' ' || (da._house_number::text) END naslov_gs",
                    "CASE WHEN vw.id < 0 THEN dbi.posta_gs ELSE da._post END posta_gs",
                    "CASE WHEN vw.id < 0 THEN dbi.sporazum ELSE 
                        CASE WHEN agr._completed = true THEN {$yes_no['yes']} ELSE {$yes_no['no']} END
                    END sporazum",
                    "CASE WHEN vw.id < 0 THEN dbi.priimek_ime_cenilca ELSE dpt._uname END priimek_ime_cenilca",
                    "CASE WHEN vw.id < 0 THEN dbi.dmg_object_id ELSE (obj->>'dmg_object_id')::integer END dmg_object_id",
                    "CASE WHEN vw.id < 0 THEN dbi.kol_rnd::text ELSE obj->'data'->>'kolicina' END kol_rnd",
                    "CASE WHEN vw.id < 0 THEN dbi.enota_gs ELSE obj->'data'->>'enota' END enota_gs",
                    "CASE WHEN vw.id < 0 THEN dbi.genetic_assessment ELSE 
                        CASE WHEN vw._genetic_samples is not null and vw._genetic_samples <> '[]' THEN 'DA' ELSE null END 
                    END genetic_assessment",
                    'dbi.location_coordinate_type_list_id',
                    "CASE WHEN vw.id < 0 THEN dbi.oe_list_id::text ELSE vw._location_data->'spatial_request_result'->>'oe_ime' END oe_list_id",
                    "CASE WHEN vw.id < 0 THEN dbi.evidence ELSE vw._culprit_signs::text END evidence",
                    "CASE WHEN vw.id < 0 THEN null ELSE agr._data END agreement_data",
                    "CASE WHEN vw.id < 0 THEN null ELSE obj->>'oid' END oid",
                    "CASE WHEN vw.id < 0 THEN dbi.id_mas ELSE vw._claim_id END id_mas"
                ]);

                $batchImportJoin = "left join mb2data.dmg_batch_imports dbi on vw.id = -dbi.id and vw.id < 0
                left join mb2data.dmg_deputies_vw dpt on vw._deputy = dpt._uid and vw.id > 0
                LEFT JOIN LATERAL jsonb_array_elements(vw._dmg_objects) AS obj ON true
                LEFT JOIN mb2data.dmg_affectees_vw da ON da.id = (obj->'data'->>'affectee')::integer and vw.id > 0
                LEFT JOIN mb2data.dmg_agreements agr ON agr._claim_id = vw.id AND (agr._affectee = da.id OR da.id IS NULL) AND vw.id > 0";
            }

            return [$batchImportColumns, $batchImportJoin];
        }

        private static function export($moduleKey, $dbFunctionParameters) {

            if ($moduleKey === 'genotypes') {
                self::filteredGenotypesExport($dbFunctionParameters);
                return;
            }

            $languageCode = Mbase2Drupal::getCurrentLanguageCode();

            $attributeDefinitionsNameSpace = [
                'interventions' => 'interventions_batch_imports',
                'dmg' => 'dmg_batch_imports'
            ];

            if ($moduleKey === 'cnt') {
                $attributes = Mbase2Database::query("SELECT * FROM mbase2.module_variables_vw WHERE module_name in ('cnt_monitoring','cnt_observation_reports', 'cnt_permanent_spots')
                and exportable = true");
            }
            else {
                $attributes = Mbase2Database::query("SELECT * FROM mbase2.module_variables_vw WHERE module_name=:module_name 
                and exportable = true", [':module_name' => isset($attributeDefinitionsNameSpace[$moduleKey]) ? $attributeDefinitionsNameSpace[$moduleKey] : $moduleKey]);
            }
            
            $attributes = Mbase2Utils::getModuleAttributes($moduleKey, $attributes, $languageCode, false);

            $attributes=array_column($attributes, null, 'variable_name');

            $filePropertiesByHash = [];
            if (isset($attributes['_photos'])) {
                $filePropertiesByHash = Mbase2Utils::getFilePropertiesByHash($moduleKey);
            }

            function cmp($a, $b) {
                return $a['weight_in_export'] > $b['weight_in_export'];
            }
            
            usort($attributes, "cmp");

            //optimisation
            if ($moduleKey !== 'tlm') {
                $dbFunctionParameters[6] = TRUE;
                $res = call_user_func_array(['Mbase2Database', 'select'], $dbFunctionParameters);
                $where = $res['where'];
                $from = $res['from'];

                foreach($dbFunctionParameters[1] as $key => $value) {
                    unset($dbFunctionParameters[1][$key]);
                }

                $dbFunctionParameters[6] = FALSE;
                $dbFunctionParameters[7] = $res['params'];

                $filteredTableName = "(SELECT * FROM $from $where)";

                $locationGeometryAlias = isset($dbFunctionParameters[1][':__sfilter']) ? ", vw._location as __location_geometry" : '';

                [$batchImportColumns, $batchImportJoin] = self::beforeSelect($moduleKey);

                $withSpatialUnitEventDate = Mbase2Utils::SQL_withSpatialUnitEventDate($filteredTableName);

                $dbFunctionParameters[3] = "$withSpatialUnitEventDate 
                    select _x, vw.*$locationGeometryAlias $batchImportColumns from 
                    x, {$dbFunctionParameters[0]} vw $batchImportJoin where x.id = vw.id";
            }

            $res = call_user_func_array(['Mbase2Database', 'select'], $dbFunctionParameters);

            $res = self::afterSelect($moduleKey, $res);

            $output = [];

            $srrMap = [
                'SI-LOV'=>'lov_ime',
                'SI-LUO'=>'luo_ime',
                'SI-OB'=>'ob_uime',
                'SI-OE'=>'oe_ime',
                'CNTRY'=>'cntr_id'
            ];

            foreach($res as $row) {
                $outputRow=[];

                if (isset($row['_location_data'])) {
                    
                    $row['_location_data'] = json_decode($row['_location_data']);

                    if (!empty($row['_x'])) {
                        $srr = [];

                        $row['_x'] = json_decode($row['_x']);

                        foreach ($row['_x'] as $x) {
                            $key = isset($srrMap[$x->s]) ? $srrMap[$x->s] : $x->s;
                            $srr[$key] = $x->n;
                        }
                        
                        $row['_location_data']->spatial_request_result = $srr;
                    }

                    if (!empty($row['geom'])) {
                        $geom = json_decode($row['geom']);
                        $coords = $geom->coordinates;
                        $row['_location_data']->lon = $coords[0];
                        $row['_location_data']->lat = $coords[1];
                    }
                }

                foreach ($attributes as $a) {
                    $key = $a['key_name_id'];
                    if (!isset($row[$key])) continue;

                    $value = $row[$key];

                    if (self::handleLocationAndReferences($key, $a, $value, $outputRow)) {
                        continue;
                    }
                    else if ($key === '_photos') {
                        if (!empty($filePropertiesByHash)) {
                            self::urls($outputRow, $key, $value, $filePropertiesByHash, $moduleKey );
                        }
                    }
                    else {
                        $outputRow[$key] = $value;
                    }
                }
        
                $output[] = $outputRow;
            }
            
            $header = self::createHeader($attributes, $languageCode);
            $format = [];
            foreach ($attributes as $a) {
                if ($a['data_type'] == 'date') {
                    $format[$a['variable_name']] = 'date';
                }
            }

            if ($moduleKey == 'dmg') {
                list($groupedHeader, $groupedOutput) = self::groupedOutputDmg($header, $output);
                self::outputSheetsToExcel([
                    [
                        'header' => $groupedHeader,
                        'data' => $groupedOutput,
                        'format' => $format,
                        'name' => 'škodni dogodki'
                    ],
                    [
                        'header' => $header,
                        'data' => $output,
                        'format' => $format,
                        'name' => 'škodni objekti'
                    ]
                ], 'mbase-export.xlsx');
            }
            else {
                self::outputToExcel($header, $output, 'mbase-export', $format);
            }
        }

        private static function groupedOutputDmg($header, $output) {
            $groupedOutput = [];
            $groupedHeader = [];

            $headerByKey = [];

            foreach($header as $h) {
                $headerByKey[$h[0]] = $h;
            }

            $commonKeys = ['id_mas',
                    '_location_data.lat',
                    '_location_data.lon',
                    'location_coordinate_type_list_id',
                    '_location_data.spatial_request_result.lov_ime',
                    '_location_data.spatial_request_result.luo_ime',
                    'oe_list_id',
                    '_location_data.additional.lname',
                    'licence_list_id',
                    '_location_data.additional.sdist',
                    'culprit_id',
                    'genetic_assessment',
                    'event_date',
                    'notes',
                    '_compensation_sum',
                    'priimek_ime_cenilca'];

            foreach($commonKeys as $key) {
                $groupedHeader[] = $headerByKey[$key];
            }

            foreach ($output as $inx => $row) {
                $idmas = $row['id_mas'] ?? $inx.'-'.random_int(0, 10000000);
                $grow = [];
                if (!isset($groupedOutput[$idmas])) {
                    foreach($commonKeys as $key) {
                        if (isset($row[$key])) {
                            $grow[$key] = $row[$key];
                        }
                    }
                    $groupedOutput[$idmas] = $grow;
                }
            }
            return [$groupedHeader, array_values($groupedOutput)];
        }

        private static function handleLocationData($value, &$outputRow) {
            $value = is_string($value) ? json_decode($value) : $value;
                        
            if (isset($value->spatial_request_result)) {
                $pre = '_location_data.spatial_request_result.';
                foreach ($value->spatial_request_result as $k => $v) {
                    $v = trim($v);
                    if (empty($v)) continue;
                    $outputRow[$pre.$k] = $v;
                }
            }

            if (isset($value->additional)) {
                $pre = '_location_data.additional.';
                foreach($value->additional as $k=>$v) {
                    $v = trim($v);
                    if (empty($v)) continue;
                    $outputRow[$pre.$k] = $v;
                }
            }

            foreach (['lat', 'lon'] as $k) {
                $pre = '_location_data.';
                if (isset($value->$k)) {
                    $v = trim($value->$k);
                    if (empty($v)) continue;
                    $outputRow[$pre.$k] = $v;
                }
            }
        }

        private static function handleLocationAndReferences($key, $attributeDefinition, $value, &$outputRow) {
            if (Mbase2Utils::isReference($attributeDefinition['data_type'])) {
                $outputRow[$key] = Mbase2Utils::replaceCodeListValuesWithTranslation($attributeDefinition, $value);
                return true;
            }
            else if ($attributeDefinition['data_type'] === 'location_geometry') {
                return true;
            }
            else if ($attributeDefinition['data_type'] === 'location_data_json') {
                self::handleLocationData($value, $outputRow);
                return true;
            }

            return false;
        }

        private static function createHeader($attributes, $languageCode) {
            $header = [];

            /**
             * create headers
             */
            foreach ($attributes as $a) {

                if (in_array($a['data_type'], ['location_data_json', 'location_geometry']) !== FALSE) continue;

                $trans = [];
                
                if (isset($a['translations'])) {
                    $trans = json_decode($a['translations'], true);
                }

                if (isset($trans[$languageCode]) && !empty($trans[$languageCode])) {
                    $cname = $trans[$languageCode];
                }
                else {
                    $cname = $a['variable_name'];
                }

                $header[] = [$a['variable_name'], $cname]; //cname === $a['variable_name'] if translations does not exist
            }

            return $header;
        }

        public static function exportAlleleNameMap($alab_id) {
            $alab_id = intval($alab_id);

            $rows = \DB::select('SELECT distinct gia.name as "imported allele name", ga.name as "database allele name", ga.marker, ga.seq as sequence FROM 
                mb2data.gensam_alleles ga,
                mb2data.gensam_imported_alleles gia 
                where alab_id=:alab_id
                and ga.id = gia.allele_id', [':alab_id' => $alab_id]);

            foreach($rows as &$row) {
                $row = (array)$row;
            }

            if (count($rows) > 0) {
                $header = array_keys($rows[0]);
                self::outputToExcel($header, $rows);
            }    
        }

        private static function gensamExport($dbFunctionParameters) {
            $languageCode = Mbase2Drupal::getCurrentLanguageCode();

            $attributes = Mbase2Database::query("SELECT * FROM mbase2.module_variables_vw WHERE module_name=:module_name and variable_name not like 'sample_properties.%' order by weight_in_export", [':module_name' => 'gensam']);

            $attributes = Mbase2Utils::getModuleAttributes('gensam', $attributes, $languageCode, false);

            $attributesOfProperties = Mbase2Utils::getModuleAttributes(
                '',Mbase2Database::query("select * from mbase2.module_variables_vw mvv where variable_name like 'sample_properties.%' and module_name = 'gensam' order by weight"), $languageCode, false);

            $attributes=array_column($attributes, null, 'variable_name');

            $attributesOfProperties=array_column($attributesOfProperties, null, 'variable_name');

            $res = call_user_func_array(['Mbase2Database', 'select'], $dbFunctionParameters);

            $output = [];

            foreach($res as $row) {
                $outputRow=[];

                //if ($row['completed']===false) continue;

                foreach($row as $key=>$value) {
                    if (!isset($attributes[$key])) continue;
                    
                    $a = $attributes[$key];
                    
                    if (self::handleLocationAndReferences($key, $a, $value, $outputRow)) {
                        continue;
                    }
                    else if ($a['variable_name']==='sample_properties') {
                        $value = json_decode($value);
                        if (!empty($value)) {
                            foreach($value as $k=>$v) {
                                if (!is_string($v)) continue;
                                $v = trim($v);
                                if (empty($v)) continue;
                                if (isset($attributesOfProperties[$k]) && Mbase2Utils::isReference($attributesOfProperties[$k]['data_type'])) {
                                    $v = Mbase2Utils::replaceCodeListValuesWithTranslation($attributesOfProperties[$k], $v);
                                }
                                $outputRow[$k] = $v;
                                $additionalColumns[$k] = $k;
                            }
                        }
                    }
                    else {
                        $outputRow[$key] = $value;
                    }
                }
                $output[] = $outputRow;
            }

            $header = self::createHeader($attributes, $languageCode);

            self::outputToExcel($header, $output);
            
        }

        private static function isExportAllowed($module) {
            $currentUserRoles = Mbase2Drupal::currentUserRoles();
            
            if ($module === 'tlm_tracks') {
                $module = 'tlm';
            }

            return Mbase2Utils::userRoleExists($module, ['admins','editors','consumers','curators'], $currentUserRoles);
        }

        static function narcisExport($vname) {
            return;
            if (!in_array($vname, 
                [
                    'narcis_dmg_affectees_vw',
                    'narcis_dmg_agreements_vw',
                    'narcis_dmg_culprit_signs_vw',
                    'narcis_dmg_damage_data_vw',
                    'narcis_dmg_data_vw',
                    'narcis_dmg_others_vw',
                    'narcis_dmg_protection_data_vw'
                ])) {
                    throw new Exception('The provided view name is not allowed. Allowed views are: narcis_dmg_affectees_vw, narcis_dmg_agreements_vw, narcis_dmg_culprit_signs_vw, narcis_dmg_damage_data_vw, narcis_dmg_data_vw,narcis_dmg_others_vw,narcis_dmg_protection_data_vw.');
                }

            $rows = Mbase2Database::query("SELECT * FROM $vname");

            //header("Content-type: text/csv");

            if (count($rows) > 0) {
                $header = array_keys($rows[0]);
                echo implode("\t", $header);
                foreach($rows as $row) {
                    echo implode("\t", $row)."\n";
                }
            }

        }

        static function exportModuleData($module, $dbFunctionParameters, $general = false, $exportOptions = []) {

            if ($module === 'narcis') {
                //self::narcisExport($tname);
                return;
            }

            if (!self::isExportAllowed($module)) {
                echo "Insufficient permissions to download the data.";
                return;
            }

            if ($general === true) {
                self::generalExport($module, $dbFunctionParameters);
                return;
            }

            if ($module === 'dmg') {
                if (Mbase2Drupal::isAdmin($module)) {
                    self::dmgExport($dbFunctionParameters, null, $exportOptions);
                }
                else {
                    $_oe_id = Mbase2Utils::dmg_get_deputy_oeid();
                    //if (empty($_oe_id)) throw new Exception('You have to be a deputy to access this data.');
                    self::dmgExport($dbFunctionParameters,empty($_oe_id) ? null : str_pad($_oe_id, 2, '0', STR_PAD_LEFT), $exportOptions);
                }
            }
            else if ($module === 'interventions') {
                self::interventionsExport($dbFunctionParameters);
            }
            else if ($module === 'gensam') {
                self::gensamExport($dbFunctionParameters);
            }
            else if ($module === 'sop') {
                self::sopExport($dbFunctionParameters);
            }
            else if ($module === 'ct') {
                self::ctExport($dbFunctionParameters);
            }
            else if ($module === 'howling') {
                self::export($module, $dbFunctionParameters);
            }
            else if ($module === 'tlm_tracks') {
                $dbFunctionParameters[0] = 'mb2data.tlm_vw';
                self::export('tlm', $dbFunctionParameters);
            }
        }
    }
