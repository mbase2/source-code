<?php

    /**
     * $p ['upid', 'page1', 'page2']
     */
    class Mbase2ImportGenotypes {
        private $alleles = [];
        private $gtypes = [];

        public $batch_id;

        private $allelePageColumnIndices;

        private $customSampleCodes;

        private $batchData = [];

        public $errorException;
        private $currentUserId;

        public $allelePageErrors;
        public $genTypePageErrors;
        public $importReport;

        private $alab_id;

        private $genotypeRowsWithUnreferencedAllele = [];
        private $genotypeRowsWithMissingAllelePair = [];
        
        private $sampleCodeStatus = []; //data about the existing samples (permission access and if genotype exists)

        function __construct($p) {
            $this->customSampleCodes = intval($p['cs']) === 1 ? true : false;

            $this->alab_id = intval($p['lab']);

            $this->allelePageColumnIndices = (object)[
                'sequence' => 2,
                'marker' => 1,
                'name' => 0
            ];
    
            $this->allelePageErrors = (object)[
                'missing_value' => [],
                'same_seq_diff_name_same_marker' => [],
                'diff_seq_same_name_same_marker' => []
            ];

            $this->genTypePageErrors = (object)[
                'empty_sample_code' => [],
                'wrong_sample_code' => [],
                'empty_markers' => [],
                'access_denied_samples' => [],
                'genotype_exists' => [],
                'missing_allele_pair' => [],
                'unreferenced_allele' => [],
                'multiplicated_sample_row' => [],
                'multiplicated_sample_row_same_genotype' => []
            ];

            $this->importReport = (object)[
                'reference_allele_count' => 0, /**1 */
                'all_referenced_reference_allele_count' => 0, /**2 */
                'unique_referenced_reference_allele_count' => 0, /**3 */
                'existing_alleles_count' => 0, /**4 */
                'insertedAlleleSequencesCount' => 0, /**5 */
                'all_sample_genotypes_count' => 0, /**6 */
                'invalid_sample_code_count' => 0, /**7 */
                'same_sample_code_different_genotype_count' => 0, /**8 */
                'multiplicated_genotype_row_count' => 0, /**9 */
                'gtypes_with_unreferenced_allele_count' => 0, /**10 */
                'gtypes_with_one_allele_pair_count' => 0, /**11 */
                'valid_sample_genotypes_count' => 0, /**12 */
                'existing_samples_with_genotypes_count' => 0, /**13 */
                'access_denied_samples_count' => 0, /**14 */
                'insertedGenotypesCount' => 0, /**15 */
                'found_samples_count' => 0, /**16 */
                'numberOfSamplesCreatedCount' => 0 /**17 */
            ];

            $this->errorException = null;

            $dsid = intval($p['upid']);
            list($filePath, $fileName, $fileHash) = Mbase2Files::getFileData($dsid);
            $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
            $data=[];

            $sheetInx = [intval($p['page1']), intval($p['page2'])];

            Mbase2Utils::readExcel($filePath.$fileHash, $data, $sheetInx);
            $this->alleles = $data[0];
            $this->gtypes = $data[1];

            $this->currentUserId = $currentUserId = Mbase2Drupal::getCurrentUserData()['uid'];

            $this->batchData = [
                "dsid" => $dsid,
                "fileName" => $fileName,
                "sheetInx" => $sheetInx
            ];

            $rows = \DB::select("INSERT INTO mbase2.import_batches (user_id, key_module_id, data) VALUES (:user_id, :key_module_id, :data) RETURNING id",[
                ':user_id' => $currentUserId,
                ':key_module_id' => 'genotypes',
                ':data' => json_encode($this->batchData)
            ]);

            $this->batch_id = $rows[0]->id;

            $genotypesData = $this->parseGenTypePage();

            $this->importReport->invalid_sample_code_count = count($this->genTypePageErrors->empty_sample_code) + count($this->genTypePageErrors->wrong_sample_code);

            $genotypesData = $this->filterDuplicateGenotypeRows($genotypesData);

            $this->importReport->same_sample_code_different_genotype_count =
            array_reduce($this->genTypePageErrors->multiplicated_sample_row, function ($carry, $item) {
                return $carry + count($item);
            }, 0);

            $this->importReport->multiplicated_genotype_row_count =
            array_reduce($this->genTypePageErrors->multiplicated_sample_row_same_genotype, function ($carry, $item) {
                return $carry + count($item);
            }, 0);

            $genotypesData = $this->filterEmptyGenotypeRows($genotypesData);

            $referenceTableData = $this->getReferenceTableData();
            $this->importReport->reference_allele_count = count($referenceTableData);

            /**
             * $this->genotypeRowsWithUnreferencedAllele
             * $this->genotypeRowsWithMissingAllelePair
             */
            $referencedReferenceTableData = $this->getReferencedReferenceTableData($referenceTableData, $genotypesData);

            $countBeforeUAC = count($genotypesData);

            if (!$this->customSampleCodes) {
                $genotypesData = $this->filterForExistingGenotypesAndAccessPermission($genotypesData);
                $this->importReport->access_denied_samples_count = count($this->genTypePageErrors->access_denied_samples);
                $this->importReport->existing_samples_with_genotypes_count = count($this->genTypePageErrors->genotype_exists);
            }

            $countAfterUAC = count($genotypesData);

            /**
             * Filter rows with ureferfenced allele and missing allele pair
             */
            $genotypesData = Mbase2Utils::genotypes_filter($this->genotypeRowsWithUnreferencedAllele, $genotypesData);
            $genotypesData = Mbase2Utils::genotypes_filter($this->genotypeRowsWithMissingAllelePair, $genotypesData);

            $this->importReport->gtypes_with_unreferenced_allele_count = count($this->genotypeRowsWithUnreferencedAllele);
            $this->importReport->gtypes_with_one_allele_pair_count = count($this->genotypeRowsWithMissingAllelePair);
            
            $this->importReport->all_referenced_reference_allele_count = count($referencedReferenceTableData);
            $this->importReport->all_sample_genotypes_count = count($this->gtypes)-1;
            
            /**
             * valid samples that might not be imported due to UAC or genotypes already present
             */

            $this->importReport->valid_sample_genotypes_count = count($genotypesData) + $countBeforeUAC - $countAfterUAC;
            
            if ($this->validateAllelePage($referenceTableData, $referencedReferenceTableData)) {
                \DB::beginTransaction();
                try {
                    $this->importData($genotypesData);
                    \DB::commit();
                } catch (\Exception $e) {
                    \DB::rollBack();
                    $this->errorException = $e->getMessage(); 
                }
            }
            else {
                $this->removeDuplicateAlleleReferences(); //needed for unique sequences count
            }

            $this->batchData['import_status'] = [
                $this->allelePageErrors,
                $this->genTypePageErrors,
                $this->importReport,
                $this->errorException
            ];

            \DB::update("UPDATE mbase2.import_batches SET data=:data WHERE id=:batch_id", [
                ':batch_id' => $this->batch_id,
                ':data' => json_encode($this->batchData)
            ]);
        }

        private function getReferencedReferenceTableData($referenceTableData, $genotypesDataForImport) {

            $referencedReferenceTableData = [];
            
            foreach ($genotypesDataForImport as $sampleCode => $dataRow) {
                
                [$rowInx, $markers] = $dataRow;

                foreach ($markers as $markerData) {
                    $marker = $markerData[0];
                    $alleles = $markerData[1];
                
                    $res = $this->getReferencedTableDataWithAllelePair($referenceTableData, $rowInx, $marker, $alleles);
                    if ($res !== false) {
                        list($allelePairReferencedReferenceTableData, $allelePair) = $res;
                        $dataRow[] = [$marker, $allelePair];
                        foreach($allelePairReferencedReferenceTableData as $referenceTableDataRow) {
                            $inx = $referenceTableDataRow['inx'];
                            if (!isset($referencedReferenceTableData[$inx])) {
                                $referencedReferenceTableData[$inx] = $referenceTableDataRow;
                            }
                        }
                    }
                }
            }

            return $referencedReferenceTableData;
        }

        private function filterEmptyGenotypeRows($data) {
            $filtered = [];

            foreach ($data as $sampleCode => $dataRow) {
                
                [$rowInx, $markers] = $dataRow;

                if (empty($markers)) {
                    $this->genTypePageErrors->empty_markers[$rowInx + 1] = $sampleCode;
                    continue;
                }
                else {
                    $filtered[$sampleCode] = $dataRow;
                }
            }

            return $filtered;
        }

        private function filterForExistingGenotypesAndAccessPermission($data) {
            $sampleCodeStatus = [];

            $filtered = [];
            
            $ttn = 'tmp_table_imp_sample_codes_'.$this->batch_id;
            \DB::statement("CREATE TEMP TABLE $ttn (id serial primary key, sample_code varchar unique)");
            \DB::statement("CREATE INDEX ON $ttn (sample_code)");

            $insertData = array_map(function($sample_code) {
                return ["sample_code" => $sample_code];
            }, array_keys($data));

            \DB::table($ttn)->insert($insertData);

            $accessWhere = Mbase2Utils::SQL_whereUserCanAccessGensam('g', $this->currentUserId);
            $rows = \DB::select("SELECT 
                    ttn.sample_code,
                    CASE WHEN ($accessWhere) THEN true ELSE false END as user_has_access,
                    CASE WHEN gg.gensam_id IS NULL THEN false ELSE true END as has_genotype
                FROM 
                    $ttn ttn
                LEFT JOIN mb2data.gensam g
                    ON ttn.sample_code = g.sample_code
                LEFT JOIN (select distinct gensam_id from mb2data.gensam_genotypes) gg
                    ON g.id = gg.gensam_id
                WHERE g.id IS NOT null");
            /**
             * add has_genotype column by joining to mb2data.gensam g, mb2data.gensam_genotypes
             */
            foreach($rows as $row) {
                $sampleCodeStatus[$row->sample_code] = $row;
            }

            $this->sampleCodeStatus = $sampleCodeStatus;

            foreach ($data as $sampleCode => $dataRow) {
                
                [$rowInx, $markers] = $dataRow;

                if (isset($sampleCodeStatus[$sampleCode])) {
                    $sampleCodeData = $sampleCodeStatus[$sampleCode];
                    if (!$sampleCodeData->user_has_access) {
                        $this->genTypePageErrors->access_denied_samples[$rowInx + 1] = $sampleCode;
                        continue;
                    }
                    else if ($sampleCodeData->has_genotype) {
                        $this->genTypePageErrors->genotype_exists[$rowInx + 1] = $sampleCode;
                        continue;
                    }
                    else {
                        $filtered[$sampleCode] = $dataRow;
                    }
                }
                else { // these samples are going to be newly imported
                    $filtered[$sampleCode] = $dataRow;
                }
            }

            return $filtered;
        }

        private function filterDuplicateGenotypeRows($data) {
            /**
             * filter out duplicates
             */

            $filtered = [];

            foreach($data as $sampleCode => $dataRows) {
                if (count($dataRows) > 1) {
                    
                    $a = [];
                    foreach($dataRows as $dataRow) {
                        $a[] = $dataRow[1];
                    }

                    $a = Mbase2Utils::checkArraysEqual($a);
                    
                    if (empty($a)) { //different genotypes for the same sample
                        $this->genTypePageErrors->multiplicated_sample_row[$sampleCode] = [];
                        foreach($dataRows as $dataRow) {
                            $this->genTypePageErrors->multiplicated_sample_row[$sampleCode][] = $dataRow[0] + 1;
                        }
                    }
                    else { //the same genotypes for the same sample
                        $filtered[$sampleCode] = [$dataRows[0][0], $a]; //include row inx of a first row of duplicated rows
                        $this->genTypePageErrors->multiplicated_sample_row_same_genotype[$sampleCode] = [];
                        foreach($dataRows as $dataRow) {
                            $this->genTypePageErrors->multiplicated_sample_row_same_genotype[$sampleCode][] = $dataRow[0] + 1;
                        }
                    }
                }
                else {
                    $filtered[$sampleCode] = $dataRows[0];
                }   
            }

            return $filtered;
        }

        private function importGenotypes($genotypesDataForImport) {

            $gtn = 'tmp_table_gen_'.$this->batch_id;
            \DB::statement("CREATE TEMP TABLE $gtn (
                id serial primary key,
                sample_code varchar,
                marker varchar,
                allele varchar)");

            \DB::statement("CREATE INDEX ON $gtn (sample_code)");
            \DB::statement("CREATE INDEX ON $gtn (marker)");
            \DB::statement("CREATE INDEX ON $gtn (allele)");

            $this->importGenotypesToTable($gtn, $genotypesDataForImport);

            $batch_id = intval($this->batch_id);
            \DB::insert("INSERT INTO mb2data.gensam_genotypes (allele_id, gensam_id, batch_id)
                SELECT 
                    gia.allele_id, 
                    g.id as gensam_id,
                    $batch_id
                FROM $gtn AS gtn
                    JOIN mb2data.gensam_imported_alleles AS gia
                        ON gtn.allele = gia.name
                        AND gtn.marker = gia.marker
                    JOIN mb2data.gensam AS g
                        ON g.sample_code = gtn.sample_code
                WHERE gia.batch_id=:batch_id",[':batch_id' => $batch_id]);

            $this->importReport->insertedGenotypesCount = \DB::selectOne("select count(*) c from (
                select gensam_id, count(*) from mb2data.gensam_genotypes gg where batch_id = :batch_id group by gensam_id) a", [':batch_id' => $this->batch_id])->c;
        }

        private function importGenotypesToTable($tname, $genotypesDataForImport) {
            $sampleCodeStatus = $this->sampleCodeStatus;

            $gensamNew = [];
            $insertData = [];

            foreach ($genotypesDataForImport as $sampleCode => $dataRow) {

                [$rowInx, $markers] = $dataRow;
                
                if ($this->customSampleCodes) {
                    $secondLabCode = $sampleCode;
                    $sampleCode = Mbase2Utils::generateSampleCode();
                    $gensamNew[] = [
                        'second_lab_code' => $secondLabCode,
                        'sample_code' => $sampleCode,
                        '_uid' => $this->currentUserId
                    ];
                }
                else {
                    if (!isset($sampleCodeStatus[$sampleCode])) {
                        $gensamNew[] = [
                            'sample_code' => $sampleCode,
                            '_uid' => $this->currentUserId
                        ];
                    }
                }

                foreach ($markers as $markerData) {
                    $marker = $markerData[0];
                    $alleles = $markerData[1];

                    // Loop through alleles to insert each allele separately
                    foreach ($alleles as $allele) {
                        $insertData[] = [
                            'sample_code' => $sampleCode,
                            'marker' => $marker,
                            'allele' => $allele
                        ];
                    }
                }
            }

            if (!empty($gensamNew)) {
                \DB::table('mb2data.gensam')->insert($gensamNew);
                $this->importReport->numberOfSamplesCreatedCount = count($gensamNew);
            }

            if (!empty($insertData)) {
                \DB::table($tname)->insert($insertData);
            }

            $this->importReport->found_samples_count = count($genotypesDataForImport) - count($gensamNew);
        }

        private function countExistingAlleles($atn) {
            return \DB::selectOne("SELECT 
                count(*) c
            FROM $atn atn
                LEFT JOIN 
            (SELECT marker, seq
                    FROM mb2data.gensam_alleles) es
                ON atn.marker = es.marker AND atn.seq = es.seq
                WHERE es.seq IS NOT NULL")->c;
        }

        private function importData($genotypesDataForImport) {
            $atn = 'tmp_table_bid_'.$this->batch_id;

            $this->removeDuplicateAlleleReferences();

            $this->importReport->existing_alleles_count = $this->countExistingAlleles($atn);

            $this->importReport->insertedAlleleSequencesCount = Mbase2Database::insertAndReturnNumberOfInsertedRows("WITH existing_sequences AS (
                        -- Get existing (marker, seq) pairs and their names from mb2data.gensam_alleles
                        SELECT marker, seq, LENGTH(seq) AS seq_length, name
                        FROM mb2data.gensam_alleles
                    ),
                    counts_by_length AS (
                        -- Calculate the count of sequences grouped by marker and sequence length in gensam_alleles
                        SELECT marker, LENGTH(seq) AS seq_length, COUNT(*) AS existing_count
                        FROM mb2data.gensam_alleles
                        GROUP BY marker, LENGTH(seq)
                    ),
                    row_numbered_tmp AS (
                        -- Add a row number to each sequence within $atn grouped by marker and sequence length
                        -- and exclude already existing sequences from the temp table
                        SELECT 
                            atn.*,
                            LENGTH(atn.seq) AS seq_length,  -- Calculate the sequence length for each row
                            ROW_NUMBER() OVER (PARTITION BY atn.marker, LENGTH(atn.seq) ORDER BY atn.inx) - 1 AS row_num
                        FROM $atn atn
                        LEFT JOIN existing_sequences es
                            ON atn.marker = es.marker AND atn.seq = es.seq
                        WHERE es.seq IS NULL  -- Exclude sequences that already exist in gensam_alleles
                    )
                    -- Insert new sequences into mb2data.gensam_alleles with correctly generated names
                    INSERT INTO mb2data.gensam_alleles (name, marker, seq)
                    SELECT
                        -- Generate names correctly, considering existing counts
                        CASE
                            -- Handle the case when there are no existing sequences in gensam_alleles (i.e., cbl.existing_count is NULL)
                            WHEN COALESCE(cbl.existing_count, 0) = 0 THEN rnt.seq_length::text || CASE WHEN rnt.row_num > 0 THEN '_' || rnt.row_num::text ELSE '' END
                            ELSE rnt.seq_length::text || '_' || (COALESCE(cbl.existing_count, 0) + rnt.row_num)::text
                        END AS generated_name,
                        rnt.marker,
                        rnt.seq
                    FROM row_numbered_tmp rnt
                    LEFT JOIN counts_by_length cbl
                        ON rnt.marker = cbl.marker AND rnt.seq_length = cbl.seq_length");

            \DB::update("UPDATE $atn atn
                SET allele_id = ga.id
                FROM mb2data.gensam_alleles ga
                WHERE atn.marker = ga.marker
                AND atn.seq = ga.seq;");

            $batch_id = $this->batch_id;
            $alab_id = $this->alab_id;

            \DB::insert("INSERT INTO mb2data.gensam_imported_alleles (inx, name, marker, seq, allele_id, batch_id, alab_id)
                SELECT inx, name, marker, seq, allele_id, $batch_id, $alab_id
                FROM $atn;");

            $this->importGenotypes($genotypesDataForImport);
        }

        private function removeDuplicateAlleleReferences() {
            
            $atn = 'tmp_table_bid_'.$this->batch_id;

            \DB::DELETE("WITH CTE AS (
                SELECT inx,
                    ROW_NUMBER() OVER (PARTITION BY name, marker, seq ORDER BY inx) AS rn
                FROM $atn
            )
            DELETE FROM $atn
            WHERE inx IN (
                SELECT inx
                FROM CTE
                WHERE rn > 1
            )");

            $this->importReport->unique_referenced_reference_allele_count = \DB::selectOne("SELECT count(*) c from $atn")->c;
        }

        private function checkSampleCode($inx, $sampleCode) {
            if (empty($sampleCode)) {
                $this->genTypePageErrors->empty_sample_code[] = [$inx + 1];
                return false;
            }

            if (Mbase2Utils::checkSampleCode($sampleCode)) {
                return true;
            }
            else {
                $this->genTypePageErrors->wrong_sample_code[] = [$inx + 1, $sampleCode];
                return false;
            }
            return false;
        }

        /**
         * Returns referenced reference table data
         */

        private function getReferencedTableDataWithAllelePair($referenceTableData, $inx, $marker, $alleles) {

            if (empty($alleles)) {
                return false;
            }

            $referencedReferenceTableData = [];
            
            foreach($alleles as &$allele) {
                $allele = ltrim($allele,'0');

                if (isset($referenceTableData[$allele.'.'.$marker])) {
                    foreach($referenceTableData[$allele.'.'.$marker] as $row) {
                        $referencedReferenceTableData[] = $row;
                    }
                }
                else {
                    $this->genTypePageErrors->unreferenced_allele[($inx+1).'|'.$allele.'|'.$marker] = 1;
                    $this->genotypeRowsWithUnreferencedAllele[$inx] = true;
                }
            }

            unset($allele);
                
            if (count($alleles) !== 2) {
                $this->genTypePageErrors->missing_allele_pair[] = [$inx + 1, json_encode($alleles), $marker];
                $this->genotypeRowsWithMissingAllelePair[$inx] = true;
                return false;
            }

            return [$referencedReferenceTableData, $alleles];
        }

        private function parseAllelePair($gpair) {
            if (empty($gpair)) {
                return false;
            }

            $parts = preg_split('/\s+/', $gpair);
            $parts = array_map('trim', $parts);
            $parts = array_map('strtoupper', $parts);

            return $parts;
        }

        function parseGenTypePage() {
            $markers = [];

            $data = [];

            foreach($this->gtypes as $rowInx => $row) {
                if ($rowInx == 0) {
                    $markers = $row;
                    foreach($markers as &$marker) {
                        $marker = strtoupper(trim($marker));
                    }
                    unset($marker);
                    continue;
                }

                $dataRow = [];
                $sampleCode = null;

                foreach ($markers as $markerIndex => $marker) {

                    /**
                     * check sample code
                     */
                    if ($markerIndex == 0) {
                        
                        $sampleCode = strtoupper(trim($row[$markerIndex]));
                        if ($this->customSampleCodes) {
                            if (empty($sampleCode)) {
                                $sampleCode = '_cst'.$rowInx;
                            }
                            continue;
                        }

                        if (!$this->checkSampleCode($rowInx, $sampleCode)) {
                            $sampleCode = null;
                            break;
                        }
                        continue;
                    }

                    /**
                     * parse allele pair
                     */
                    $gpair = strtoupper(trim($row[$markerIndex]));
                    $pgpair = $this->parseAllelePair($gpair);

                    if ($pgpair !== false) { //false is returned if there is no data for the specific marker
                        $dataRow[] = [$marker, $pgpair];
                    }
                }

                if (empty($sampleCode)) {
                    continue;
                }

                if (!isset($data[$sampleCode])) {
                    $data[$sampleCode] = [];
                }

                $data[$sampleCode][] = [$rowInx, $dataRow];
            }

            return $data;
        }

        function getReferenceTableData() {
            
            $apInx = $this->allelePageColumnIndices;

            $data = [];

            foreach ($this->alleles as $inx => $row) {
                if ($inx == 0) continue;

                $name = strtoupper(trim($row[$apInx->name]));
                $marker = strtoupper(trim($row[$apInx->marker]));
                $sequence = strtoupper(trim($row[$apInx->sequence]));
                
                if (empty($name) || empty($marker) || empty($sequence)) {
                    $this->allelePageErrors->missing_value[] = [$inx, $name, $marker, $sequence];
                    continue;
                }

                $key =  ltrim($name,'0').'.'.$marker;
                if (!isset($data[$key])) {
                    $data[$key] = [];
                }

                $data[$key][] = [
                    'inx' => $inx+1,
                    'name' => $name,
                    'marker' => $marker,
                    'seq' => $sequence
                ];
            }

            return $data;
        }

        function validateAllelePage($referenceTableData, $referencedReferenceTableData) {
            $data = [];

            foreach($referenceTableData as $key => $dataRows) {
                foreach ($dataRows as $dataRow) {
                    if (isset($referencedReferenceTableData[$dataRow['inx']])) {
                        $dataRow['referenced'] = true;
                    }
                    else {
                        $dataRow['referenced'] = false;
                    }
                    $data[] = $dataRow;
                }
                
            }

            $atn = 'tmp_table_bid_'.$this->batch_id;

            \DB::statement("CREATE TEMP TABLE $atn (
                id bigserial primary key,
                inx integer not null,
                name varchar not null,
                marker varchar not null,
                seq varchar not null,
                allele_id bigint,
                referenced boolean
            )");

            \DB::statement("CREATE INDEX ON $atn (name)");
            \DB::statement("CREATE INDEX ON $atn (marker)");
            \DB::statement("CREATE INDEX ON $atn (seq)");
            \DB::statement("CREATE INDEX ON $atn (marker, seq)");
                
            \DB::table($atn)->insert($data);

            $this->allelePageErrors->same_seq_diff_name_same_marker=
            \DB::select("SELECT t1.marker, t1.seq, jsonb_agg(DISTINCT jsonb_build_array(t1.inx, t1.name)) found_rows
                        FROM $atn t1
                        LEFT JOIN $atn same_seq_diff_name_same_marker
                            ON t1.marker = same_seq_diff_name_same_marker.marker
                            AND t1.name <> same_seq_diff_name_same_marker.name
                            AND t1.seq = same_seq_diff_name_same_marker.seq
                        WHERE same_seq_diff_name_same_marker.id IS NOT NULL
                        GROUP BY t1.marker, t1.seq");

            $this->allelePageErrors->diff_seq_same_name_same_marker=
            \DB::select("SELECT t1.marker, t1.name, jsonb_agg(DISTINCT jsonb_build_array(t1.inx, t1.seq)) found_rows
                        FROM $atn t1
                        LEFT JOIN $atn diff_seq_same_name_same_marker
                            ON t1.marker = diff_seq_same_name_same_marker.marker
                            AND t1.name = diff_seq_same_name_same_marker.name
                            AND t1.seq <> diff_seq_same_name_same_marker.seq
                        WHERE diff_seq_same_name_same_marker.id IS NOT NULL
                        GROUP BY t1.marker, t1.name");

            \DB::delete("DELETE FROM $atn WHERE referenced = false");

            if (!empty($this->allelePageErrors->same_seq_diff_name_same_marker) || !empty($this->allelePageErrors->diff_seq_same_name_same_marker)) {
                return false;
            }

            return true;
        }
    }