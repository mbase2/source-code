<?php
    
    require 'vendor/autoload.php';

    use Aspera\Spreadsheet\XLSX\Reader;

    const IMAGE_HANDLERS = [
      IMAGETYPE_JPEG => [
          'load' => 'imagecreatefromjpeg',
          'save' => 'imagejpeg',
          'quality' => 100,
          'mime' => 'jpeg'
      ],
      IMAGETYPE_PNG => [
          'load' => 'imagecreatefrompng',
          'save' => 'imagepng',
          'quality' => 0,
          'mime' => 'png'
      ],
      IMAGETYPE_GIF => [
          'load' => 'imagecreatefromgif',
          'save' => 'imagegif',
          'mime' => 'gif'
      ]
  ];

    final class Mbase2Files {
        public static $storagePath;

        /**
         * Returns physical path with starting two characters from file hash for public or private access 
         * if path doesn't exist it will be created
         */
        private static function getFileStoragePath($access, $fileHash=null, $createPath = false, $subfolder='') {
            if (function_exists('drupal_realpath')) {
                if (empty($fileHash)) return drupal_realpath(self::$storagePath[$access]);
            }
            else {
                if (empty($fileHash)) return self::$storagePath[$access];
            }

            $filePath = 'm'.substr($fileHash, 0, 2);

            if (function_exists('drupal_realpath')) {
                $path = drupal_realpath(self::$storagePath[$access]).'/'.$subfolder.'/'.$filePath.'/';
            }
            else {
                $path = self::$storagePath[$access].'/'.$subfolder.'/'.$filePath.'/';
            }
            
            if ($createPath && !file_exists($path)) {
                mkdir($path, 0777, true);
            }

            return $path;
        }

        /**
         * currently returns only files from public storage path
         */

        public static function getPublicStorageFile($fname) {
            $fname = basename($fname);
            $fullFilePath = self::$storagePath['public'].$fname;

            if (file_exists($fullFilePath)) {
                if (basename(dirname($fullFilePath))==='mbase2' && strpos($fullFilePath, '.private')===FALSE) {

                    header("Content-Description: File Transfer"); 
                    header("Content-Type: application/octet-stream"); 
                    header("Content-Disposition: attachment; filename=\"". basename($fullFilePath) ."\""); 

                    readfile ($fullFilePath);
                    exit(); 
                }
            }
        }

        public static function createThumbnail($src, $dest, $targetWidth, $targetHeight = null) {
          
          //https://gist.github.com/pqina/7a42bf0833d988dd81d3c9438009da21#file-create-thumbnail-php-L29

          // 1. Load the image from the given $src
          // - see if the file actually exists
          // - check if it's of a valid image type
          // - load the image resource

          // get the type of the image
          // we need the type to determine the correct loader
          $type = exif_imagetype($src);

          // if no valid type or no handler found -> exit
          if (!$type || !IMAGE_HANDLERS[$type]) {
              return null;
          }

          // load the image with the correct loader
          $image = call_user_func(IMAGE_HANDLERS[$type]['load'], $src);

          // no image found at supplied location -> exit
          if (!$image) {
              return null;
          }


          // 2. Create a thumbnail and resize the loaded $image
          // - get the image dimensions
          // - define the output size appropriately
          // - create a thumbnail based on that size
          // - set alpha transparency for GIFs and PNGs
          // - draw the final thumbnail

          // get original image width and height
          $width = imagesx($image);
          $height = imagesy($image);

          // maintain aspect ratio when no height set
          if ($targetHeight == null) {

              // get width to height ratio
              $ratio = $width / $height;

              // if is portrait
              // use ratio to scale height to fit in square
              if ($width > $height) {
                  $targetHeight = floor($targetWidth / $ratio);
              }
              // if is landscape
              // use ratio to scale width to fit in square
              else {
                  $targetHeight = $targetWidth;
                  $targetWidth = floor($targetWidth * $ratio);
              }
          }

          // create duplicate image based on calculated target size
          $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);

          // set transparency options for GIFs and PNGs
          if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_PNG) {

              // make image transparent
              imagecolortransparent(
                  $thumbnail,
                  imagecolorallocate($thumbnail, 0, 0, 0)
              );

              // additional settings for PNGs
              if ($type == IMAGETYPE_PNG) {
                  imagealphablending($thumbnail, false);
                  imagesavealpha($thumbnail, true);
              }
          }

          // copy entire source image to duplicate image and resize
          imagecopyresampled(
              $thumbnail,
              $image,
              0, 0, 0, 0,
              $targetWidth, $targetHeight,
              $width, $height
          );


          // 3. Save the $thumbnail to disk
          // - call the correct save method
          // - set the correct quality level

          // save the duplicate version of the image to disk
          call_user_func(
              IMAGE_HANDLERS[$type]['save'],
              $thumbnail,
              $dest,
              IMAGE_HANDLERS[$type]['quality']
          );
          return ['width'=>$width, 'height' => $height, 'type' => IMAGE_HANDLERS[$type]['mime']];
        }

        /**
         * @param {string} $uid user id 
         * @param {string} $name name of uploaded file
         */

        public static function fileUploadFromCamelot($uid, $access, $name) {
            $fname = tempnam(sys_get_temp_dir(), 'camelot');
            
            $input = fopen('php://input', 'rb');
                $file = fopen($fname, 'wb');
                    stream_copy_to_stream($input, $file);
                fclose($file);
            fclose($input);

            $_FILES = ['file' => [
            'tmp_name' => $fname,
            'name' => $name
            ]];

            return self::fileUpload($uid, $access, true, false, 'ct');
        }

        public static function deleteUploadedFile($uid, $private, $id) {
            $res = Mbase2Database::query("SELECT u.*, fp.properties from mbase2.uploads u, mbase2.file_properties fp where u.file_hash = fp.file_hash AND u.id = :id AND private=:private", [':id'=>$id, ':private'=>$private]);
            if (empty($res)) return null;
            $res = $res[0];

            Mbase2Database::query("DELETE from mbase2.uploads WHERE id=:id",[':id' => $res['id']]);

            //delete file if it is not referenced any more
            $count = Mbase2Database::query("SELECT count(1) as cnt from mbase2.uploads where file_hash=:file_hash",[':file_hash'=>$res['file_hash']]);
            $cnt = $count[0]['cnt'];

            if (intval($cnt) === 0) {
                $filePath = self::getFileStoragePath($res['private'] == 1 ? 'private' : 'public', $res['file_hash'], false, $res['subfolder']).$res['file_hash'];
                unlink($filePath);
                Mbase2Database::query("DELETE from mbase2.file_properties WHERE file_hash=:file_hash",[':file_hash' => $res['file_hash']]);
            }

            return $res;
        }

        /**
         * $file {array} ['subfolder', 'hash', 'type']
         */

        public static function getUploadedFile($uid, $private, $id = null, $file = null, $thumbnail = false) {
            
            $res = [];

            $imageType = null;

            $watermark = false;

            $filePath = null;

            $moduleSlug = '';
            
            if (is_null($file) && !is_null($id)) {
                $res = Mbase2Database::query("SELECT u.*, fp.properties from mbase2.uploads u, mbase2.file_properties fp where u.file_hash = fp.file_hash AND u.id = :id AND private=:private", [':id'=>$id, ':private'=>$private]);
                if (empty($res)) return null;
                $res = $res[0];

                $moduleSlug = explode('_',$res['subfolder'])[0];

                if (isset($res['properties'])) {
                    $props = json_decode($res['properties']);
                    if (isset($props->type)) {
                        if (in_array($props->type, ['jpeg', 'png', 'gif'])) {
                            $imageType = $props->type;
                            $filePath = self::getFileStoragePath($res['private'] == 1 ? 'private' : 'public', $res['file_hash'], false, $res['subfolder']).$res['file_hash'].($thumbnail ? '_thumbnail' : '');
                        }
                    }
                    
                    if (is_null($filePath)) {
                        
                        $fileSize = null;
                        if (isset($props->size)) { //for backward compatibility
                            $fileSize = $props->size;
                        }
                        else if (isset($props->file_size)) {
                            $fileSize = $props->file_size;
                        }

                        header('Content-Type: application/octet-stream');
                        
                        if (!is_null($fileSize)) {
                            header('Content-Length: ' . $fileSize);
                        }
                        
                        header('Content-Disposition: attachment; filename=' . $res['file_name']); 
                        $filePath = self::getFileStoragePath($res['private'] == 1 ? 'private' : 'public', $res['file_hash'], false, $res['subfolder']).$res['file_hash'];
                    }

                }
            }
            else if (!is_null($file)) {
                
                $imageType = isset($file['type']) ? $file['type'] : null;
                $filePath = self::getFileStoragePath($private ? 'private' : 'public').'/'.$file['path'];
                if ($private) {
                    $moduleSlug = explode('/',$file['path'])[0];
                }

                if (is_null($imageType)) {
                    header('Content-Type: application/octet-stream');
                }
            }

            if ($private === true) {
                $userHasRoles = Mbase2Utils::hasUserRoleInModule($moduleSlug);

                $forbidden = true;

                if (in_array($moduleSlug, ['ct', 'sop'])) {
                    $forbidden = false;
                    if (!$userHasRoles) {
                        $watermark = true;
                    }
                }
                else if ($userHasRoles) {
                    //if admin allow everything, else uid has to match
                    $currentUserRoles = Mbase2Drupal::currentUserRoles();
                    if (Mbase2Utils::userRoleExists($moduleSlug, ['admins'], $currentUserRoles)) {
                        $forbidden = false;
                    }
                    else {
                        if (array_key_exists('uid', $res)) {
                            $userData = Mbase2Drupal::getCurrentUserData();
                            
                            if ($res['uid'] === $userData['uid']) {
                                $forbidden = false;
                            }
                        }    
                    }
                }

                if ($forbidden) {
                    throw new Exception("Insufficient privilege to get the data.");
                    return null;
                }
            }

            if (!is_null($imageType)) {
                header("Content-type: image/$imageType");
            }

            if (is_null($filePath)) return null;

            if (!file_exists($filePath)) {
                throw new Exception("File does not exist.");
                return null;
            }

            $fileContents = file_get_contents($filePath);

            if ($watermark === true && !is_null($imageType) && $thumbnail === false) {
                $im = imagecreatefromstring($fileContents);
                // First we create our stamp image manually from GD
                $stamp = imagecreatefromstring(file_get_contents(__DIR__.'/assets/lynx-logo.jpg'));
                $stamp = imagescale($stamp, imagesx($im));

                // Set the margins for the stamp and get the height/width of the stamp image

                $sx = imagesx($stamp);
                $sy = imagesy($stamp);

                // Merge the stamp onto our photo with an opacity of 50%
                imagecopymerge($im, $stamp, intval(imagesx($im)/2 - $sx/2), 0, 0, 0, imagesx($stamp), imagesy($stamp), 25);

                //ob_start();
                imagejpeg($im);
                imagedestroy($im);
                //echo ob_get_clean();
                return;
            }

            echo $fileContents;
        }

        /**
         * @param {string} $scope ct specific parameter
         */

        public static function getUploadedFiles($uid, $private, $subfolder, $offset, $scope, $limit='', $ext=''){
            
            $uidCondition = ''; //$private ? "uid = :uid and" : '';
            
            if (empty(strlen($offset))) $offset = 0;

            $params = [':private' => $private, ':offset' => $offset, ':subfolder' => $subfolder];

            $limitCondition = empty(strlen($limit)) ? '' : 'limit :limit';

            if (!empty($limitCondition)) {
                $params[':limit'] = $limit;
            }

            if (!empty($uidCondition)) {
                $params[':uid'] = $uid;
            }

            $whereExt = '';

            if (!empty($ext)) {
                $params[':ext'] = '%'.strtolower($ext);
                $whereExt = "AND lower(u.file_name) like :ext";
            }
            
            if ($scope === 'unprocessed') {
            return Mbase2Database::query("SELECT u.*, fp.properties FROM mbase2.uploads u, mbase2.file_properties fp 
                WHERE $uidCondition private=:private AND u.subfolder = :subfolder AND u.file_hash = fp.file_hash $whereExt AND
                properties->>'type' in ('jpeg', 'png', 'gif') AND
                NOT EXISTS (
                SELECT * 
                FROM mb2data.ct ct
                WHERE jsonb_exists(photos, u.file_hash)
                )
                order by id
                $limitCondition
                offset :offset
                ", $params);
            }
            else if ($scope === 'processed') {
                return Mbase2Database::query("SELECT u.*, fp.properties FROM mbase2.uploads u, mbase2.file_properties fp 
                WHERE $uidCondition private=:private AND u.subfolder = :subfolder AND u.file_hash = fp.file_hash $whereExt AND
                properties->>'type' in ('jpeg', 'png', 'gif') AND
                EXISTS (
                SELECT * 
                FROM mb2data.ct ct
                WHERE jsonb_exists(photos, u.file_hash)
                )
                order by id
                $limitCondition
                offset :offset
                ", $params);
            }

            return Mbase2Database::query("SELECT u.*, fp.properties from mbase2.uploads u, mbase2.file_properties fp where u.file_hash = fp.file_hash 
                $whereExt
                AND $uidCondition private=:private
                AND u.subfolder like :subfolder
                order by id
                $limitCondition
                offset :offset
                ", $params);
        }

        /**
         * 
         * @param {boolean} $saved true if file is already saved (e.g. when uploading from Camelot)
         */

        private static function fileUploadDatabase($uid, $fileName, $subfolder, $fileHash, $isPrivate, $properties = null) {

            if (!is_null($properties)) {
            $properties = json_encode($properties);
            }

            Mbase2Database::query("BEGIN");
            list($res) = Mbase2Database::query("SELECT max(file_number) file_number from mbase2.uploads WHERE file_name = :file_name AND uid = :uid AND private = :private", 
            [
                ':file_name' => $fileName,
                ':uid' => $uid,
                ':private' => $isPrivate
            ]);
            
            $fileNumber = $res['file_number'];

            if (is_null($fileNumber)) { 
            $fileNumber = 0;
            }
            else { //file with this name already exists in the system
            $fileNumber++;
            }

            try {
            $res = Mbase2Database::query("INSERT INTO mbase2.file_properties(file_hash, properties) VALUES (:file_hash, :properties) 
                ON CONFLICT (file_hash) DO UPDATE SET properties=:properties;",[':file_hash'=>$fileHash, ':properties' => $properties]);
            $res = Mbase2Database::query("INSERT INTO mbase2.uploads (file_hash, file_name, subfolder, file_number, uid, private) VALUES(:file_hash, :file_name, :subfolder, :file_number, :uid, :private) RETURNING *",
            [
                ':file_hash' => $fileHash,
                ':file_name' => $fileName,
                ':subfolder' => $subfolder,
                ':uid' => $uid,
                ':file_number' => $fileNumber,
                ':private' => $isPrivate
            ]);
            }
            catch (Exception $e) {
            Mbase2Database::query("ROLLBACK");
            throw new Exception($e->getMessage());  
            }

            Mbase2Database::query("COMMIT");

            $rval = $res[0];

            $rval['properties'] = $properties;
            
            return $rval;
        }

        /**
         * @param {integer} $uid user id
         * @param {string} $access public | private
         */

        public static function fileUpload($uid, $access = 'private', $saved = false, $isImageExtensionCompulsory = true, $subfolder = '') {
            
            if (empty($_FILES)) return [];

            $tempFile = $_FILES['file']['tmp_name'];
            $name = $_FILES['file']['name'];
            $fileHash = sha1_file($tempFile);
            if ($fileHash === false) throw new Exception("File hash error");
            
            $fileStoragePath = self::getFileStoragePath($access, $fileHash, true, $subfolder);
            $targetFile =  $fileStoragePath. $fileHash;
            
            if ($saved === true) {  //file was already saved by some other mechanism (used when uploading images from camelot)
                rename($tempFile, $targetFile);
                chmod($targetFile, 0644); // Read and write for owner, read for everybody else
            }
            else {
                move_uploaded_file($tempFile, $targetFile);
            }
    
            $ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));

            $imageProperties = null;
    
            if (!$isImageExtensionCompulsory ||
                ($isImageExtensionCompulsory && in_array($ext, ['jpg', 'png', 'jpeg', 'gif']))
                ) {
                $imageProperties = self::createThumbnail($targetFile, $targetFile."_thumbnail", 100);
            }

            $properties = [
                'file_size' => filesize($targetFile)
            ];

            if (!is_null($imageProperties)) {
                $properties['height'] = $imageProperties['height'];
                $properties['width'] = $imageProperties['width'];
                $properties['type'] = $imageProperties['type'];
            }

            return self::fileUploadDatabase($uid, $name, $subfolder, $fileHash, $access === 'private', $properties);
        }

        public static function getFileData($dsid) {
          $storagePath = self::$storagePath;
          $res = Mbase2Database::query("SELECT * FROM mbase2.uploads WHERE id=:id", [':id'=>$dsid]);

          if (empty($res)) return $res;

          $res = $res[0];
          
          $subfolder = empty($res['subfolder']) ? '' : $res['subfolder'];
          
          $filePath = self::getFileStoragePath($res['private'] == 1 ? 'private' : 'public', $res['file_hash'], false, $subfolder);

          $fileName = $res['file_name'];
          $fileHash = $res['file_hash'];

          return [$filePath, $fileName, $fileHash];
        }

        public static function get_data_source_pages ($dsid){

          list($filePath, $fileName, $fileHash) = self::getFileData($dsid);

          $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

          if ($ext === 'csv') return [$fileName];

          $reader = new Reader();
          $reader->open($filePath.$fileHash);
          $sheets = $reader->getSheets();
          
          $pages = [];
          foreach ($sheets as $index => $sheet_data) {
            $reader->changeSheet($index);
            $pages[]=$sheet_data->getName();
          }

          return $pages;
        }
          
        public static function get_data_source_page_columns ($dsid, $sheetInx, $filePath=null, $fileName=null){

          if (is_null($filePath) || is_null($fileName)) {
            list($filePath, $fileName, $fileHash) = self::getFileData($dsid);
          }
          
          $reader = new Reader();
          $reader->open($filePath.$fileHash);

          $reader->changeSheet($sheetInx);
          
          foreach ($reader as $row) {
            return $row;
          }
        }
    }
