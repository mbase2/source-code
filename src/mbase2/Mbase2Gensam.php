<?php

final class Mbase2Gensam {
    public static function deleteGenotypes($sampleIds) {
        $tmpTableName = self::prepareSampleIdsTempTable($sampleIds);
        // Step 3: Delete using a join
        \DB::delete("DELETE FROM mb2data.gensam_genotypes gg
            WHERE EXISTS (
                SELECT 1
                FROM $tmpTableName
                WHERE $tmpTableName.id = gg.gensam_id and $tmpTableName.is_editable = true
        );");
    }

    public static function batchUpdateUac($sampleIds, $organisationIds=[], $userIds=[], $remove = false) {
        
        if (empty($userIds)) {
            $userIds = [];
        }

        if (empty($organisationIds)) {
            $organisationIds = [];
        }

        $organisationIds = implode(',', array_map('intval', $organisationIds));
        $userIds = implode(',', array_map('intval', $userIds));
        
        $tmpTableName = self::prepareSampleIdsTempTable($sampleIds);

        $res = null;
        $where = "WHERE EXISTS (
                    SELECT 1
                    FROM $tmpTableName
                    WHERE $tmpTableName.id = g.id and $tmpTableName.is_editable = true";

        if ($remove) {
            $res = \DB::update("UPDATE mb2data.gensam g
            SET organisation_access_ids = null,
            user_access_ids = null
            $where
            );");
        }
        else {
            $res = \DB::update("UPDATE mb2data.gensam g
            SET organisation_access_ids = (
                SELECT jsonb_agg(DISTINCT value)
                FROM jsonb_array_elements(COALESCE(organisation_access_ids, '[]'::jsonb) || '[$organisationIds]'::jsonb) AS value
            ),
            user_access_ids = (
                SELECT jsonb_agg(DISTINCT value)
                FROM jsonb_array_elements(COALESCE(user_access_ids, '[]'::jsonb) || '[$userIds]'::jsonb) AS value
            )
            $where
            );");
        }
        
        return ['updated' => $res];
    }

    private static function prepareSampleIdsTempTable($sampleIds) {
        $tmpTableName = 'tmp_'.str_replace('.','',uniqid('', true));

        \DB::statement("CREATE TEMPORARY TABLE $tmpTableName (id BIGINT PRIMARY KEY, is_editable boolean default false)");

        // Step 2: Insert IDs in chunks
        $chunkSize = 1000;
        collect($sampleIds)->chunk($chunkSize)->each(function ($chunk) use ($tmpTableName) {
            \DB::table($tmpTableName)->insert(
                collect($chunk)->map(fn ($id) => ['id' => $id])->toArray()
            );
        });

        $isAdmin = Mbase2Utils::hasUserRoleInModule('gensam', 'admin');

        $where = '';

        if (!$isAdmin) {
            $currentUserId = Mbase2Drupal::getCurrentUserData()['uid'];
            $where = "WHERE _uid = $currentUserId
            OR EXISTS (
                SELECT 1
                FROM jsonb_array_elements_text(g.organisation_access_ids) AS org_id
                JOIN mb2data.gensam_organisations org ON org.id = org_id::int
                WHERE org.gensam_organisations_admins @> '$currentUserId'::jsonb
            )";
        }

        \DB::update("UPDATE $tmpTableName t
            SET is_editable = TRUE
            WHERE t.id IN (
                SELECT g.id
                FROM mb2data.gensam g
                $where
            )");

        \DB::statement("CREATE INDEX idx_tmp_table_eligible_ids
            ON $tmpTableName (id, is_editable);");

        return $tmpTableName;
    }

    public static function deleteGensam($sampleIds) {
        $tmpTableName = self::prepareSampleIdsTempTable($sampleIds);

        \DB::delete("DELETE FROM mb2data.gensam g
            WHERE EXISTS (
                SELECT 1
                FROM $tmpTableName
                WHERE $tmpTableName.id = g.id and $tmpTableName.is_editable = true
            )
            AND NOT EXISTS (
                SELECT 1
                FROM mb2data.gensam_genotypes gg
                WHERE gg.gensam_id = g.id
            );");
    }
}