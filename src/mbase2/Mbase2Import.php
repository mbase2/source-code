<?php

use proj4php\Proj4php;
use proj4php\Proj;
use proj4php\Point;

class Mbase2Import {
    private $uid;
    private $languageOfTheImport;
    private $keyModuleId;
    private $currentRow;
    private $data;
    private $locationDataPlaceholder; //here I store the placeholder for location data to be modified before database operation
    private $additionalVariables = [];
    public $errors;
    public $numberOfInputRows;
    public $numberOfInsertedRows;
    public $numberOfUpdatedRows;
    public $importType;
    private $tempData = null;
    private $fkeys = [];
    private $types = [];
    private $dateFormat;
    private $tableNames; //tnames without schema
    private $schemaTableNames; //schema$tname
    private $placeholderAttributesBySchemaTablenames;
    private $requiredAttributesBySchemaTablenames = [];
    private $referenceArrayDelimiter=null; //auto find reference array delimiter, can be '|' or ','

    /**
     * @param integer $dsid data source id (reference to mbase2.uploads)
     * @param integer $sheetInx xlsx sheet inx
     * @param string $keyModuleId module or reference table slug
     * @param array $dataDefinitions
     * @param integer $uid user id - used for fetching upload references only for the specific user and in camelot imports
     */
        
    function __construct($dsid, $sheetInx, $keyModuleId, $dataDefinitions, $uid, $camelotId = null, $camelotData = null, $languageOfTheImport='en') {

        $placeholderAttributes = [];
        $fixedValues = [];
        $indices = [];
        $importType = 'insert';
        $this->importType = $importType;
        $updateIds = [];
        $updateKey = '';

        $tnames = [];

        if (!is_null($camelotId)) {
            $this->keyModuleId = $keyModuleId;
            $dataDefinitions = $this->camelotDataDefinitions($camelotId, $uid, $camelotData[0]);
        }
        
        foreach (array_keys($dataDefinitions) as $tname) {
            list('placeholderAttributes' => $modulePlaceholderAttributes,
            'fixedValues' => $moduleFixedValues,
            'indices' => $moduleIndices,
            'importType' => $importType,
            'updateIds' => $updateIds,
            'updateKey' => $updateKey,
            'media' => $media,
            'requiredAttributes' => $requiredAttributes,
            'schema' => $schema) = $this->preprocessAttributeData($tname, $dataDefinitions, $uid, $camelotId, $camelotData, $languageOfTheImport);

            $tname = str_replace($schema.'.','', $tname);

            $schema_tname = $schema.'.'.$tname;
            $tnames[$schema_tname] = $tname;

            if (isset($dataDefinitions[$tname]['fkeys']) && !empty($dataDefinitions[$tname]['fkeys'])) {
                $this->fkeys[$tname] = $dataDefinitions[$tname]['fkeys']; 
            }

            $this->requiredAttributesBySchemaTablenames[$schema_tname] = $requiredAttributes;

            $placeholderAttributes[$schema_tname] = $modulePlaceholderAttributes;
            $fixedValues[$schema_tname] = $moduleFixedValues;
            $indices[$schema_tname] = $moduleIndices;
        }

        list ($batchId, $batchData) = $this->readBatchData($keyModuleId, $dsid, $sheetInx, $dataDefinitions, $camelotId, $camelotData);

        $this->schemaTableNames = array_keys($tnames);
        $this->tableNames = array_values($tnames);
        $this->placeholderAttributesBySchemaTablenames = $placeholderAttributes;

        Mbase2Database::query("BEGIN");

        $insertedRows = 0;
        $updatedRows = 0;

        $crows = count($this->data);

        $errors = [];

        $this->types = $dataDefinitions[$keyModuleId]['types'];
        
        for ($rinx = 1; $rinx < $crows; $rinx++) {

            $row = $this->data[$rinx];
            $this->currentRow = $row;
            $this->locationDataPlaceholder = null;

            $values = [];

            $criticalError = false;

            foreach ($tnames as $schema_tname=>$tname) {
                $rowErrors = [];
                $beforeParseRowPassed = $this->beforeParseRow($tname, $rinx, $row, $indices[$schema_tname], $fixedValues[$schema_tname], $placeholderAttributes[$schema_tname], $media, $rowErrors);
                
                $parsedRow = false;

                if ($beforeParseRowPassed) {
                    $parsedRow = $this->parseRow($tname, $rinx, $row, $indices[$schema_tname], $fixedValues[$schema_tname], $placeholderAttributes[$schema_tname], $media, $rowErrors);
                    $this->afterParseRow($parsedRow);
                }
                
                if (!empty($rowErrors)) {
                    $errors[] = $rowErrors;
                }

                if ($parsedRow === false) {
                    $criticalError = true;
                    break;
                }
                
                $values[$schema_tname] = $parsedRow;

                $this->modifyValuesBeforeDatabase($schema_tname, $tname, $placeholderAttributes[$schema_tname], $values[$schema_tname]);
            }

            if ($criticalError) continue;

            $rowForUpdate = false;
            $databaseAction = false;

            try {
                if ($importType === 'insert') {
                    $this->batchInsert($batchId, $values);

                    $databaseAction = true;
                }
                else if ($importType === 'update') {
                    foreach ($tnames as $schema_tname=>$tname) {
                        if (isset($values[$schema_tname][':'.$updateKey])) {
                            if (isset($updateIds[strtolower($values[$schema_tname][':'.$updateKey])])) {
                                $rowForUpdate = true;
                                Mbase2Database::update($schema_tname, $values[$schema_tname], null, PGSQL_ASSOC, $updateKey);
                                $databaseAction = true;
                            }
                        }
                    }
                }
                
                Mbase2Database::query("COMMIT");
                if ($databaseAction === true) {
                    if ($rowForUpdate) {
                        $updatedRows++;
                    }
                    else {
                        $insertedRows++;
                    }
                }
            }
            catch (Exception $e) {
                Mbase2Database::query("ROLLBACK");
                $detail = Mbase2Utils::getDatabaseErrorMessageError($e->getMessage());
                $msg = $this->errorMessage("", $rinx, null, "Database error.", null, '', false);
                $msg['value'] = $detail;
                $msg['critical'] = true;
                $errors[] = [json_encode($msg)];
            }
        }

        Mbase2Database::query("COMMIT");

        if (count($errors) > 0 && !is_null($batchId)) {
            Mbase2Database::query("BEGIN");
            foreach($errors as $error) {
                try {
                    Mbase2Database::query("INSERT INTO mbase2.import_errors (batch_id, data) VALUES (:batch_id, :data)", [':batch_id' => $batchId, ':data' => json_encode($error)]);
                }
                catch (Exception $e) {
                    Mbase2Database::query("ROLLBACK");
                    throw new Exception($e->getMessage());  
                }
            }
            Mbase2Database::query("COMMIT");
        }

        if (!is_null($batchId)) {
            $batchData['crows'] = $crows - 1;
            $batchData['insertedRows'] = $insertedRows;
            $batchData['updatedRows'] = $updatedRows;
            $batchData['schema'] = $schema;
            $batchData['importType'] = $importType;
            $batchData['updateKey'] = $updateKey;
            Mbase2Database::query("UPDATE mbase2.import_batches SET data = :data WHERE id=:id",[':id'=>$batchId, ':data' => json_encode($batchData)]);
        }

        $this->numberOfInputRows = $crows - 1;
        $this->numberOfInsertedRows = $insertedRows;
        $this->numberOfUpdatedRows = $updatedRows;
        $this->importType = $importType;

        $this->errors = $errors;
    }

    private function handleLocationData($required, $rinx, $placeholder, $currentAttributeProperties, $cinx, $row, &$value, &$validationResult) {
        $cinxLat = isset($cinx['lat']) ? trim($cinx['lat']) : null;
        $cinxLon = isset($cinx['lon']) ? trim($cinx['lon']) : null;
        
        if (!Mbase2Utils::empty($cinxLat) && !Mbase2Utils::empty($cinxLon) && !empty($row[$cinxLat]) && !empty($row[$cinxLon])) {

            if (is_numeric($row[$cinxLat]) && is_numeric($row[$cinxLon])) {

                $this->locationDataPlaceholder = $placeholder;
                $value = [
                    'lat' => trim($row[$cinxLat]),
                    'lon' => trim($row[$cinxLon]),
                    '_locations_table' => isset($currentAttributeProperties['_locations_table']) ? $currentAttributeProperties['_locations_table'] : null
                ];

                $crs = isset($cinx['crs']) ? $cinx['crs'] : 4326;
                if ($crs != 4326) {
                    $value['crs'] = $crs;
                }
            }
            else {
                $validationResult = $this->errorMessage($placeholder, $rinx, $cinxLat . ',' . $cinxLon, 'Coordinate values are to be numeric.', $required);
            }
        }
    }

    private function batchInsert($batchId, $paramsBySchemaTable) {
        
        $insertedRows = [];

        $count = count($this->schemaTableNames);

        for($i=0; $i<$count; $i++) {

            $schema_tname = $this->schemaTableNames[$i];
            $tableName = $this->tableNames[$i];
            $nextSchemaTableName = isset($this->schemaTableNames[$i+1]) ? $this->schemaTableNames[$i+1] : null;

            $params = $paramsBySchemaTable[$schema_tname];

            $doInsert = true;   //gets to false if referenced table value already exists

            $referenceMap = null;

            $fkeyValues = [];

            if (!isset($this->fkeys[$tableName])) {
                $this->fkeys[$tableName] = ['slug'];
            }
            
            foreach($this->fkeys[$tableName] as $k) {
                if (isset($params[':'.$k])) {
                    $fkeyValues[] = strtolower($params[':'.$k]);
                }
            }

            /**
             * if nextSchemaTableName is referenced by this tableName we get the reference map
             * if reference already exists the paramsBySchemaTable of the next table is updated with the id
             * we don't perform the insert
             */

            if (!is_null($nextSchemaTableName) && isset($this->placeholderAttributesBySchemaTablenames[$nextSchemaTableName][':'.$tableName.'_id'])) {
                $referenceMap = &$this->placeholderAttributesBySchemaTablenames[$nextSchemaTableName][':'.$tableName.'_id'];

                if (!empty($fkeyValues)) {
                    $fkeyValue = strtolower(implode('-', $fkeyValues));
                    if (isset($referenceMap['values'][$fkeyValue])) { //don't insert if already exists
                        $doInsert = false;
                        $id = $referenceMap['values'][$fkeyValue];
                        $paramsBySchemaTable[$nextSchemaTableName][':'.$tableName.'_id'] = $id;
                        unset($referenceMap);
                    }
                }
            }

            if (!$doInsert) continue;

            $params[':_batch_id'] = $batchId;

            $insertedRows = Mbase2Database::insert($schema_tname, $params);
            if (!is_null($referenceMap) && !empty($insertedRows)) {
                
                $fkeyValues = [];
                foreach($this->fkeys[$tableName] as $fkey) {
                    if (isset($insertedRows[0][$fkey])) {
                        $fkeyValues[] = $insertedRows[0][$fkey];
                    }
                }

                $fkeyValue = strtolower(implode('-', $fkeyValues));

                $referenceMap['values'][$fkeyValue] = $insertedRows[0]['id'];
                $paramsBySchemaTable[$nextSchemaTableName][':'.$tableName.'_id'] = $insertedRows[0]['id'];
                unset($referenceMap);
            }
        }
    }

    private function handleBooleanValue(&$value) {
        if (strtolower($value)==='false' || $value === '0' || $value === 0) {
            $value = false;
            return;
        }
        $value = true;
    }

    private function handleDateTime($dataType, &$value, &$validationResult) {
        if (is_numeric($value)){   //excel values - don't know why is_int does not work, so I am using is_numeric
            $value = self::excelDate2iso($value);
            if ($value !== false) $validationResult = true;
        }

        if (self::convertAndCheckIfIsoValid($value, $this->dateFormat, $dataType)) {    //YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or HH:mm:ss
            $validationResult = true;
        }
    }

    private function beforeParseRow($tname, $rinx, &$row, $indices, $fixedValues, $placeholderAttributes, $media, &$rowErrors) {
        
        if ($this->importType === 'insert' && $this->keyModuleId === 'gensam' && isset($indices[':sample_code'])) {
            
            $cinx = $indices[':sample_code'];
            if (Mbase2Utils::empty($cinx)) return;

            $sampleCode = trim($row[$cinx]);
            if (!Mbase2Utils::checkSampleCode($sampleCode)) {
                if ($this->types['sample_code'] === 'auto_generate') {
                    $this->tempData = $row[$cinx];
                    $row[$cinx] = Mbase2Utils::generateSampleCode();
                }
                else {
                    $errorMsg = 'Sample code '.$sampleCode.' does not match database rules for sample codes.';
                    $rowErrors[] = $this->errorMessage(':sample_code', $rinx, $cinx, $errorMsg, true);
                    return false;
                }
            }
        }

        return true;
    }

    private function afterParseRow(&$parsedRow) {
        if ($this->importType === 'insert' && $this->keyModuleId === 'gensam') {
            if (empty($parsedRow)) return false;
            if ($this->types['sample_code'] === 'auto_generate') {
                $parsedRow[':second_lab_code'] = empty($this->tempData) ? $parsedRow[':sample_code'] : $this->tempData;
            }
        }
    }

    /**
     * A loop trough $indices array and row columns.
     */

    private function parseRow($tname, $rinx, $row, $indices, $fixedValues, &$placeholderAttributes, $media, &$rowErrors) {
        $values = [];

        foreach($indices as $placeholder => $cinx) {

            $currentAttributeProperties = $placeholderAttributes[$placeholder];
            $dataType = $currentAttributeProperties['data_type'];
            $validationResult = null;

            $required = intval($placeholderAttributes[$placeholder]['required']) === 1;

            $errorMsgAlt = ''; //alternative error message
            $value = null;

            if ($cinx === false) {
                $value = isset($fixedValues[$placeholder]) ? $fixedValues[$placeholder] : null;
            }
            else if (is_callable($cinx)) {  
                $value = $cinx($row);
            }
            else if (!Mbase2Utils::empty($cinx)) {

                if ($dataType === 'location_reference' || $dataType === 'location_data_json' || $dataType === 'location_geometry') {
                    $this->handleLocationData($required, $rinx, $placeholder, $currentAttributeProperties, $cinx, $row, $value, $validationResult);

                }
                else {
                    $value = trim($row[$cinx]);
                }

                if (!Mbase2Utils::empty($value)) {
                    if ($dataType === 'code_list_reference' || $dataType === 'table_reference' || $dataType === 'table_reference_array' || $dataType === 'code_list_reference_array') {
                        $this->handleReferences($tname, $dataType,$currentAttributeProperties, $value, $errorMsgAlt);
                        $placeholderAttributes[$placeholder] = $currentAttributeProperties;
                    }
                    else if ($dataType === 'date' || $dataType === 'time') {
                        $this->handleDateTime($dataType, $value, $validationResult);
                    }
                    else if ($dataType === 'image_array') {
                        $this->handleImageArray($media, $value);
                    }
                    else if ($dataType === 'boolean') {
                        $this->handleBooleanValue($value);
                    }
                }
                else {
                    $value = null;
                }
            }

            /**
             * $data are prevalidated in locationData and date/time parser - if there were no errors in this fields then $this->validateAttributeValue function is called
             */

            if (is_null($validationResult)) {
                $validationResult = $this->validateAttributeValue($placeholder, $dataType, $value, $rinx, $cinx, $required, $errorMsgAlt);
            }

            if ($validationResult !== true) {
                $rowErrors[] = $validationResult;
                if ($required && $this->keyModuleId !== 'gensam') return false; //skip row
                continue;   //continue with next column
            }

            if ($required && Mbase2Utils::empty($value)) continue; //continue with next column

            $values[$placeholder] = $value;
        }

        return $values;
    }

    private function handleImageArray($media, &$value) {
        $fileNames = explode(',', $value);
        $value = [];
        foreach ($fileNames as $fileName) {
            if (isset($media[$fileName])) {
                $value[] = $media[$fileName];
            }
        }

        $value = empty($value) ? null : json_encode($value);
    }

    /**
     * Helper function to get code list values based on translation
     */

    private static function getReferenceId($currentAttributeProperties, $refValue, $isTableReference) {
        $refValue = trim(mb_strtolower($refValue));
        $id = null;

        foreach(['values', 'values_en', 'values_local'] as $key) {
            
            if (!isset($currentAttributeProperties[$key])) continue;

            $refs = $currentAttributeProperties[$key];
            
            if (isset($refs[$refValue])) {
                $id = $refs[$refValue];        
            }

            if (!is_null($id)) return $id;  //return id if found
        }    

        return $id;
    }

    private static function prehandleReferences($tname, $dataType, &$currentAttributeProperties, &$value) {

    }

    private function handleReferences($tname, $dataType, &$currentAttributeProperties, &$value, &$errorMsgAlt) {

        $isTableReference = str_starts_with($dataType, 'table') ? true : false;

        $errorMsgAlt = '';

        $originalValue = $value;

        if ($dataType === 'table_reference_array' || $dataType === 'code_list_reference_array') {
            $rvalues = json_decode($value);
            if (empty($rvalues)) {
                $rvalues=[];

                if (is_null($this->referenceArrayDelimiter)) {
                    if (strpos($value, ',') !== false) {
                        $this->referenceArrayDelimiter = ',';
                    }
                    else if (strpos($value, '|') !== false) {
                        $this->referenceArrayDelimiter = '|';
                    }
                }

                foreach((is_null($this->referenceArrayDelimiter) || empty($this->referenceArrayDelimiter)) ? [$value] : explode($this->referenceArrayDelimiter,$value) as $rv) {
                    $rvalues[trim($rv)] = true;
                }

                $rvalues = array_keys($rvalues);
            }
            
            if (!empty($rvalues) && count($rvalues)>0) {
                $value = [];
                $errors = [];
                foreach($rvalues as $v) {
                    $rv = self::getReferenceId($currentAttributeProperties, $v, $isTableReference);
                    if (!is_null($rv)) {
                        $value[] = $rv;
                    }
                    else if ($this->keyModuleId === 'gensam' && $currentAttributeProperties['variable_name']==='samplers') {
                        $v = trim($v);
                        if (!empty($v)) {
                            $id = \DB::table('mb2data.gensam_samplers')->insertGetId([
                                'slug' => $v
                            ]);

                            $currentAttributeProperties['values'][mb_strtolower($v)] = $id;
                            $value[] = $id;
                        }
                    }
                    else {
                        $errors[] = $v;
                    }
                }
                
                $value = empty($value) ? null : json_encode($value);

                $errorMsgAlt = empty($errors) ? $errorMsgAlt : 'Some cell values '.json_encode($errors).' could not be found in the code list.';
            }
            else {
                $value = null;
            }

            $errorMsgAlt = is_null($value) ? "Array [$originalValue] is empty or cannot be parsed." : $errorMsgAlt;

        }
        else {
            $value = self::getReferenceId($currentAttributeProperties, $value, $isTableReference);
            if (is_null($value)) {
                self::prehandleReferences($tname, $dataType, $currentAttributeProperties, $value);
            }
            $errorMsgAlt = is_null($value) ? 'The cell value "'.$originalValue.'" could not be found in the code list' : $errorMsgAlt;
        }
    }

    /**
     * Module specific row modifications before getting to the database
     */

    private function modifyValuesBeforeDatabase($schema_tname, $keyModuleId, $placeholderAttributes, &$values) {
        if ($keyModuleId === 'dmg') {
            
            if (isset($values[':_dmg_object_list_ids'])) {
                $zgsKey = json_decode($values[':_dmg_object_list_ids'])[0];
            }

            $values[':_claim_status'] = 1;
            $values[':_affectees'] = [null];
            $values[':_dmg_objects'] = json_encode(
                [
                    [
                        "oid" => null,
                        "data" => [
                            "enota" => isset($values[':_dmg_object_unit']) ? $placeholderAttributes[':_dmg_object_unit']['keys'][$values[':_dmg_object_unit']] : null,
                            "affectee" => null,
                            "kolicina" => $values[':dmg_object_quantity'],
                            "_nacini_varovanja" => isset($values[':dmg_protection_measures']) ? [
                                [
                                    $placeholderAttributes[':dmg_protection_measures']['keys'][$values[':dmg_protection_measures']] => [
                                        "navedba" => ""
                                    ]
                                ]
                            ] : [],
                            "varovanjePrisotno" => isset($values[':dmg_protection_measures']) ? true: false
                        ],
                        "zgsKey" => $placeholderAttributes[':_dmg_object_list_ids']['keys'][$zgsKey]
                    ]
            ]);

            if (isset($values[':_dmg_object_unit'])) {
                unset($values[':_dmg_object_unit']);
            }

            if (isset($values[':dmg_object_quantity'])) {
                unset($values[':dmg_object_quantity']);
            }

            if (isset($values[':dmg_protection_measures'])) {
                unset($values[':dmg_protection_measures']);
            }

            if (isset($values[':_dmg_agreement_status'])) unset($values[':_dmg_agreement_status']);
        }
        else if ($keyModuleId === 'gensam') {
            $sampleProperties = [];
            foreach ($values as $placeholder => $value) {
                $valueKey = trim($placeholder,':');
                if ($valueKey === 'sample_code') {
                    $values[$placeholder] = strtoupper($value);
                }
                else if (isset($this->additionalVariables[$valueKey])) {
                    $sampleProperties[$valueKey] = $value;
                    unset($values[$placeholder]);
                }
            }

            if ($this->importType === 'insert') {
                $completed = true;

                foreach ($this->requiredAttributesBySchemaTablenames[$schema_tname] as $a) {
                    if (!isset($values[':'.$a['variable_name']])) {
                        $completed = false;
                        break;
                    }
                }

                $values[':completed'] = $completed;
            }
        }
        else if ($keyModuleId === 'bears_biometry_animal_handling') {
            if (isset($values[':_location_data'])) {
                $locationData = $values[':_location_data'];
                $crs = isset($locationData['crs']) ? intval($locationData['crs']) : 4326;
                if ($crs !== 4326) {
                    $proj4 = new Proj4php();
                    $proj4->addDef("EPSG:3912",'+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=bessel +towgs84=409.545,72.164,486.872,3.085957,5.469110,-11.020289,17.919665 +units=m +no_defs');
                    $proj4->addDef("EPSG:3794", '+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
                    $proj4->addDef("EPSG:32632", '+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 +units=m +no_defs');

                    $projTo = new Proj('EPSG:4326', $proj4);
                    $projFrom = new Proj('EPSG:'.$crs, $proj4);

                    try {
                        $pointSrc = new Point($locationData['lon'], $locationData['lat'], $projFrom);
                        $pointDest=$proj4->transform($projTo, $pointSrc);
                        $values[':lng'] = $pointDest->x;
                        $values[':lat'] = $pointDest->y;
                        unset($values[':_location_data']);
                    } catch (Exception $e) {
                        ;
                    }
                }
                else {
                    $values[':lng'] = $locationData['lon'];
                    $values[':lat'] = $locationData['lat'];
                }

                unset($values[':_location_data']);
            }
        }

        foreach(['ltype'] as $additionalKey) {
            $placeholderKey = ':_'.$additionalKey;
            if (array_key_exists($placeholderKey, $values)) {

                if (is_null($this->locationDataPlaceholder)) {
                    $this->locationDataPlaceholder = ':_location_data';
                    $values[$this->locationDataPlaceholder] = [];
                }

                if (!is_null($this->locationDataPlaceholder)) {
                    
                    if (!isset($values[$this->locationDataPlaceholder]['additional'])) {
                        $values[$this->locationDataPlaceholder]['additional'] = [];
                    }

                    if ($additionalKey === 'ltype') {
                        $ltype = array_flip($placeholderAttributes[$placeholderKey]['values']);
                        $values[$placeholderKey] = $ltype[$values[$placeholderKey]];    //put key instead of id
                        if ($values[$placeholderKey]==='gps') {
                            $values[$placeholderKey] = 'GPS';
                        }
                    }

                    $values[$this->locationDataPlaceholder]['additional'][$additionalKey] = $values[$placeholderKey];

                }
                
                unset($values[$placeholderKey]);
            }
        }
        
        Mbase2Utils::setObjectChildren($values);
    }

    private static function dropGenotypeData($id) {
        $ndeleted = \DB::delete("delete from mb2data.gensam_genotypes where batch_id=:id", [':id' => $id]);
        \DB::delete("delete from mb2data.gensam_imported_alleles where batch_id=:id", [':id' => $id]);
        \DB::delete("DELETE from mbase2.import_batches WHERE id=:id", [':id' => $id]);

        return [
            'deleted' => $ndeleted
        ];
    }

    public static function dropBatchData($id) {
        $id = intval($id);
        $res = Mbase2Database::query("SELECT * from mbase2.import_batches where id=:id", [':id' => $id]);
        if (empty($res)) return null;

        $row = $res[0];

        $moduleKey = $row['key_module_id'];
        
        if ($moduleKey === 'genotypes') {
            return self::dropGenotypeData($id);
        }

        $data = json_decode($row['data']);
        $schema = isset($data->schema) ? $data->schema : 'mb2data';

        $ndeleted = 0;

        if (!empty($moduleKey)) {
            $moduleKey = Mbase2Utils::batchImportModuleKey($moduleKey);
            $ndeleted = \DB::delete("DELETE from $schema.$moduleKey WHERE _batch_id=:id", [':id' => $id]);
        }
        
        \DB::delete("DELETE from mbase2.import_batches WHERE id=:id", [':id' => $id]);

        return [
            'deleted' => $ndeleted
        ];
    }

    private static function checkIfAllRequiredAttributesAreSet($keyModuleId, $attributes, $dataDefinitions){
        if ($keyModuleId === 'gensam') {
            return;
        }

        $types = $dataDefinitions[$keyModuleId]['types'];

        $missingRequiredAttributes = [];

        foreach($attributes as $key => $a) {
            if ($a['required'] && $a['importable'] && !isset($types[$key])) {
                $missingRequiredAttributes[$key] = is_null($a['translation']) ? $key : $a['translation'];
            }
        }

        if (!empty($missingRequiredAttributes)) {
            $a='<ul><li>'.implode('</li><li>', $missingRequiredAttributes).'</li></ul>';
            throw new Exception("Required attributes were not set: $a");
        }

    }

    private function preprocessAttributeData($keyModuleId, $dataDefinitions, $uid, $camelotId = null, $camelotData = null, $languageOfTheImport='en') {
        $this->dateFormat = isset($dataDefinitions[$keyModuleId]['dateFormat']) ? $dataDefinitions[$keyModuleId]['dateFormat'] : "Y-m-d";
        
        if (isset($dataDefinitions[$keyModuleId]['multivalue_delimiter'])) {
            $this->referenceArrayDelimiter = $dataDefinitions[$keyModuleId]['multivalue_delimiter'];
        }

        $this->uid = $uid;
        $this->languageOfTheImport = $languageOfTheImport;
        //$dataDefinitions = [];
        //$dataDefinitions[$keyModuleId] = json_decode('{"types":{"_species_name":"column","_licence_name":"fixed","event_date":"column","photos":"column","sighting_quantity":"column","trap_station_name":"column","session_name":"column","event_time":"column","sex":"column","individual_name":"column","life_stage":"column","position":"column","hair_trap":"column","notes":"column"},"values":{"_species_name":"42","event_date":"7","photos":"9","sighting_quantity":"26","trap_station_name":"55","session_name":null,"event_time":null,"sex":null,"individual_name":null,"life_stage":null,"position":null,"hair_trap":null,"notes":null,"_licence_name":"10"}}', true);

        $this->keyModuleId = $keyModuleId;

        list('clKey' => $clKey, 'clId' => $clId) = self::getCodeLists(['referenced_tables', 'data_types', 'variables', 'modules']);
        
        $moduleId = $this->getModuleId($clKey);
        $schema = $this->getModuleSchema($clKey);

        $tableName = str_replace($schema.'.', '', $keyModuleId);

        if (empty($moduleId)) {
            throw new Exception("Import for $keyModuleId is not defined.");
        }

        if (empty($schema)) {
            throw new Exception("Can not find database schema for $keyModuleId.");
        }

        $additionalVariables = $this->additionalVariables = self::getAdditionalVariables($keyModuleId);

        //check if table has uname attribute - if not then skip it from attributes
        $skipAttributes = [];

        if (!Mbase2Database::columnExists($schema, $tableName, '_batch_id')) {
            \DB::statement("ALTER TABLE $schema.$tableName ADD column _batch_id integer references mbase2.import_batches(id)");
        }

        list('importType' => $importType,
        'updateIds' => $updateIds,
        'updateKey' => $updateKey) = self::getImportTypeData($schema, $keyModuleId, $dataDefinitions);

        $this->importType = $importType;
        $this->updateKey = $updateKey;

        $attributes = self::getModuleAttributes($languageOfTheImport, $moduleId, $additionalVariables, $skipAttributes);

        if ($importType === 'insert') {
            self::checkIfAllRequiredAttributesAreSet($keyModuleId, $attributes, $dataDefinitions);
        }
        else if ($importType === 'update') {
            if (!isset($dataDefinitions[$keyModuleId]['types'][$updateKey])) {
                throw new Exception("Column mapping for update key ($updateKey) is not defined.");
            }
        }

        list('codeListOptionsIds' => $codeListOptionsIds, 
        'referencedTableIds' => $referencedTableIds,
        'media' => $media) = self::getReferenceValues($attributes, $keyModuleId);
        
        $referencedCodeListValues = self::getReferencedCodeListValues($codeListOptionsIds);
        
        list('referencedTablesResults' => $referencedTablesResults,
        'referencedTableNamesAssocId' => $referencedTableNamesAssocId) = self::getReferencedTablesData($referencedTableIds, $clId);

        /**
         * Import from camelot uses camelot ids 
         */
        $camelotReferencesOverrides = [];

        if (!is_null($camelotId)) {
            foreach ([
                'ct_trap_stations' => 'camelot_trap_station_id',
                'ct_cameras' => 'camelot_camera_id',
                'ct_surveys' => 'camelot_survey_id'] 
                as $tname => $key) {
                
                    $listId = $referencedTableNamesAssocId[$tname];
                    $camelotReferencesOverrides[$tname] = [
                        'sql' => "SELECT id as id, $listId as list_id, $key as key from mb2data.$tname WHERE camelot_id=:camelot_id",
                        'params' => [':camelot_id' => $camelotId]
                    ];
            }
        }
        
        $referencedTableValues = Mbase2Utils::getReferencedTableValues($referencedTablesResults, $camelotReferencesOverrides);

        $this->addReferencedValuesToAttributes($attributes, $referencedCodeListValues, $referencedTableValues, $clId);

        $this->data = [];

        self::setAdditionalImportables($keyModuleId, $dataDefinitions, $attributes);
        
        $values = $dataDefinitions[$keyModuleId]['values'];
        $types = $dataDefinitions[$keyModuleId]['types'];

        foreach(['_uname', '_uid'] as $cname) {
            if (Mbase2Database::columnExists($schema, $keyModuleId, $cname)) {
                if (!isset($types[$cname])) {
                    $types[$cname] = 'fixed';
                    $values[$cname] = $uid;
                }
            }
        }   

        $placeholderAttributes = self::getPlaceholderAttributes($attributes, $types);

        $requiredAttributes = [];

        foreach($attributes as $a) {
            if ($a['required'] === true) {
                $requiredAttributes[] = $a;
            }
        }

        list('fixedValues' => $fixedValues,
        'indices' => $indices) = self::getFixedValuesAndIndices($placeholderAttributes, $types, $values);

        $this->beforeImport($keyModuleId, $types, $indices, $fixedValues, $placeholderAttributes);

        return [
            'placeholderAttributes' => $placeholderAttributes,
            'fixedValues' => $fixedValues,
            'indices' => $indices,
            'importType' => $importType,
            'updateIds' => $updateIds,
            'updateKey' => $updateKey,
            'media' => $media,
            'schema' => $schema,
            'requiredAttributes' => $requiredAttributes
        ];
    }

    /**
     * @param array $codeListNames code lists to return
     * @return array see example
     * @example The returned associative array contains two arrays:
     * clKey: assoc array, code lists by key (key = {code_list_name.code_list_item_slug})
     * clId: assoc array, code lists by id
     */

    private static function getCodeLists($codeListNames) {
        $codeListNamesImploded = implode("','", $codeListNames);
        $res = Mbase2Database::query("SELECT clv.id, clv.key, clv.list_id, clv.list_key label_key FROM mbase2.code_list_options_vw clv WHERE clv.list_key IN ('$codeListNamesImploded')");
        $clKey = [];    //this is needed only to get $moduleId from $moduleKey
        $clId = []; //associative array, key is code list id and value equals to code_list key
        foreach($res as $r) {
            $clKey[$r['label_key'].'.'.$r['key']] = $r['id'];
            $clId[$r['id']] = $r['key'];
        }
        return [
            'clKey' => $clKey,
            'clId' => $clId
        ];
    }

    private function getModuleId($clKey) {
        $moduleId = null;

        $keyModuleId = $this->keyModuleId;
        
        if (isset($clKey["modules.$keyModuleId"])) {
            $moduleId = $clKey["modules.$keyModuleId"];
        }
        else if (isset($clKey["referenced_tables.$keyModuleId"])) {
            $moduleId = $clKey["referenced_tables.$keyModuleId"];
        }

        return $moduleId;
    }

    private function getModuleSchema($clKey) {
        $schema = 'mb2data';
        $keyModuleId = $this->keyModuleId;
        
        if (isset($clKey["modules.$keyModuleId"])) {
            ;
        }
        else if (isset($clKey["referenced_tables.$keyModuleId"])) {
            $moduleId = $clKey["referenced_tables.$keyModuleId"];
            $res = Mbase2Database::query("SELECT * from mbase2.referenced_tables WHERE id=:id",[':id' => $moduleId]);
            if (!empty($res)) {
                $schema = $res[0]['schema'];
            }
        }

        return $schema;

    }

    private static function getModuleAttributes($languageOfTheImport, $moduleId, $additionalVariablesResult=[], $skipAttributes=[]) {
        
        $attributes = [];
        $res = Mbase2Database::query("SELECT * FROM mbase2.module_variables_vw WHERE module_id = :module_id and importable=true", [':module_id' => $moduleId]);
        
        $res = array_merge(
            $res,
            $additionalVariablesResult
        );

        foreach($res as $r) {
            if (!isset($r['variable_name'])) {
                print_r($r);exit;
            }
            $variableName = $r['variable_name'];
            
            if (in_array($variableName, $skipAttributes)) continue;

            $dataType = $r['data_type'];

            $translations = $r['translations'];

            $translation = null;

            if (!empty($translations)) {
                $translations = json_decode($translations, true);
                if (isset($translations[$languageOfTheImport])) {
                    $translation = $translations[$languageOfTheImport];
                }
            }

            $attributes[$variableName] = [
                'variable_name' => $variableName,
                'data_type' => $dataType,
                'importable' => $r['importable'],
                'required' => $r['required'], //empty($r['required']) ? false : true,
                'unique' => $r['unique_constraint'], //empty($r['unique_constraint']) ? false : true,
                'ref' => $r['ref'],
                'translation' => $translation
            ];
        }

        return $attributes;
    }

    private static function getReferencedCodeListValues($codeListOptionsIds) {
        $params = [':clv.id' => $codeListOptionsIds];
        $where = Mbase2Database::paramsToConditions($params);
        $referencedCodeListValues = empty($codeListOptionsIds) ? [] :
            Mbase2Database::query("SELECT clo.*,cl.list_id list_id FROM mbase2.code_list_options clo, (SELECT cl.id, clv.id list_id FROM mbase2.code_list_options clv, mbase2.code_lists cl WHERE clv.key = cl.label_key AND $where) cl WHERE cl.id = clo.list_id", $params);

        return $referencedCodeListValues;

    }

    /**
     * Reads batch data from the XLSX or from the camelot data source
     * Data are read into $this->data
     * Adds a batch import metadata into import_batches table
     * @return array [$batchId, $batchData] used later for batch import metadata update
     */

    private function readBatchData($keyModuleId, $dsid, $sheetInx, $dataDefinitions, $camelotId = null, $camelotData=[]) {
        $batchData = [];
        $batchId = null;

        if (is_null($camelotId)) {
            list($filePath, $fileName, $fileHash) = Mbase2Files::getFileData(intval($dsid));
            $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
            $batchData['dsid'] = $dsid;
            $batchData['sheetInx'] = $sheetInx;
            $batchData['fileName'] = $fileName;

            if ($ext === 'csv') {
                Mbase2Utils::readCSV($filePath.$fileHash, $this->data);
            }
            else {
                Mbase2Utils::readExcel($filePath.$fileHash, $this->data, $sheetInx);
            }
        }
        else {
            $batchData['camelotId'] = $camelotId;
            $this->data = $camelotData;
        }

        $batchData['dataDefinitions'] = $dataDefinitions;

        list($res) = Mbase2Database::query("INSERT INTO mbase2.import_batches (user_id, key_module_id) VALUES (:user_id,:keyModuleId) RETURNING id",[':user_id'=>$this->uid, ':keyModuleId'=>$keyModuleId]);
        $batchId = $res['id'];

        return [$batchId, $batchData];
    }

    private static function getPlaceholderAttributes($attributes, $types) {
        $placeholderAttributes = [];
        
        foreach($attributes as $aname=>$a) {
            $type = isset($types[$aname]) ? $types[$aname] : null;
            if (empty($type)) continue;
            $placeholder = ':'.$aname;
            $placeholderAttributes[$placeholder] = $a;
        }
        
        return $placeholderAttributes;
    }

    /**
     * If variable is fixed value, then $fixedValues[:placeholder] gets its fixed value, else
     * $indices[:placeholder] gets defined xlsx column index for this variable
     * @return array [
     *       'fixedValues' => $fixedValues,
     *       'indices' => $indices
     *   ]
     */

    private static function getFixedValuesAndIndices($placeholderAttributes, $types, $values) {
        $indices = [];
        $fixedValues = [];

        foreach($placeholderAttributes as $placeholder=>$a) {
            $aname = $a['variable_name'];
            $type = isset($types[$aname]) ? $types[$aname] : null;
            $placeholder = ':'.$aname;

            if ($type === 'fixed') {
                $indices[$placeholder] = false;
                $fixedValues[$placeholder] = $values[$aname];
            }
            else {
                $indices[$placeholder]=$values[$aname];
            }
        }

        return [
            'fixedValues' => $fixedValues,
            'indices' => $indices
        ];
    }

    /**
     * Adds module specific additional variables to the model
     */

    private static function getAdditionalVariables($keyModuleId) {
        $additionalVariables = [];
        if ($keyModuleId === 'interventions_batch_imports') {
            $additionalVariables = Mbase2Database::query("SELECT * FROM mbase2.module_variables_vw WHERE 
            module_name in ('interventions', 'interventions_events') and importable=true order by weight_in_import");
        }
        else if ($keyModuleId === 'cnt_monitoring') {
            $additionalVariables = Mbase2Database::query("SELECT * FROM mbase2.module_variables_vw WHERE 
            id in (select id from mbase2.module_variables_vw mvv  where module_name = 'cnt_monitoring' and variable_name ='_vrsta_opazovanja')");
        }
        
        return $additionalVariables;
    }

    private static function setAdditionalImportables($keyModuleId, &$dataDefinitions, $attributes) {
        if ($keyModuleId === 'cnt_monitoring') {
            $dataDefinitions[$keyModuleId]['types']['_vrsta_opazovanja']='fixed';
            $dataDefinitions[$keyModuleId]['values']['_vrsta_opazovanja']=$attributes['_vrsta_opazovanja']['values']['stalna števna mesta'];
        }
    }

    /**
     * Returns import type data
     * @return array [
     *   'importType' => $importType, (insert, update)
     *   'updateIds' => $updateIds, associative array of existing ids in the table
     *   'updateKey' => $updateKey
     *    ];
     */

    private static function getImportTypeData($schema, $keyModuleId, $dataDefinitions) {
        $importType = 'insert';
        $updateIds = [];
        $updateKey = '';

        if ($keyModuleId === 'gensam' && isset($dataDefinitions[$keyModuleId]['import_type']) && $dataDefinitions[$keyModuleId]['import_type'] !== 'insert') {
            $updateKey = $dataDefinitions[$keyModuleId]['update_key'];
            $res = Mbase2Database::select("$schema.$keyModuleId",[':__select' => $updateKey]);

            foreach ($res as $r) {
                $updateId = trim($r[$updateKey]);
                if (!Mbase2Utils::empty($updateId)) {
                    $updateIds[strtolower($updateId)] = true;
                }
            }

            $importType = strtolower($dataDefinitions[$keyModuleId]['import_type']);
        }

        return [
            'importType' => $importType,
            'updateIds' => $updateIds,
            'updateKey' => $updateKey
        ];
    }

    /**
     * Module specific modifications of $indices, $fixedValues and $placeholderAttributes
     */

    private function beforeImport($keyModuleId, $types, &$indices, &$fixedValues, &$placeholderAttributes) {
        if ($keyModuleId === 'ct') {
            if ($types['trap_station_name'] === 'from_location') {
                
                $uniquetname = 't'.uniqid();
                //Mbase2Database::query("CREATE TEMP TABLE $uniquetname IF NOT EXISTS (id serial, geom geometry)");

                Mbase2Database::query("CREATE TEMP TABLE IF NOT EXISTS $uniquetname (id serial, geom public.geometry)");
                Mbase2Database::query("CREATE INDEX {$uniquetname}_geom_idx
                ON $uniquetname
                USING GIST (geom);");

                $latInx = $indices[':trap_station_name']['lat'];
                $lonInx = $indices[':trap_station_name']['lon'];
                $crs = $indices[':trap_station_name']['crs'];

                $values = [];

                $crows = count($this->data);
        
                for ($rinx = 1; $rinx < $crows; $rinx++) {
                    $lat = trim($this->data[$rinx][$latInx]);
                    $lon = trim($this->data[$rinx][$lonInx]);
                    if (!empty($lon) && !empty($lat)) {
                        $values[] = "(public.St_transform(public.St_setsrid(public.ST_MakePoint($lon,$lat),$crs), 3035))";
                    }
                    else {
                        $values[] = 'null';
                    }
                }
                
                $values = implode(',', $values);

                if (!empty($values)) {
                    Mbase2Database::query("INSERT INTO $uniquetname (geom) values $values");
                }

                $res = Mbase2Database::query("SELECT id,
                    public.ST_ClusterDBSCAN(geom, eps := 30, minpoints := 1) OVER() AS clst_id FROM $uniquetname order by id;");

                $res = array_column($res, null, 'id');

                $ccols = count($this->data[0]);

                for ($rinx = 1; $rinx < $crows; $rinx++) {
                    $vstation = $uniquetname.'-'.$res[$rinx]['clst_id'];
                    $this->data[$rinx][$ccols] = $vstation;
                }

                Mbase2Database::query("
                CREATE TEMP TABLE vpoints_$uniquetname AS
                select clst_id, public.st_transform(public.st_centroid(geom),4326) geom from 
                (
                SELECT id,geom,
                public.ST_ClusterDBSCAN(geom, eps := 30, minpoints := 1) OVER() AS clst_id
                FROM $uniquetname) a
                group by clst_id,geom
                ");

                $res = Mbase2Database::query("
                insert into mb2data.ct_locations (geom) 
                select geom from vpoints_$uniquetname order by clst_id
                returning id;
                ");

                $locationReferences = array_column($res,'id');

                $res = Mbase2Database::query("select * from vpoints_$uniquetname order by clst_id");

                Mbase2Database::query("BEGIN");
                foreach($res as $inx => $row) {
                    $vpointName = $uniquetname.'-'.$row['clst_id'];
                    $idres = Mbase2Database::query("INSERT INTO mb2data.ct_trap_stations(trap_station_name,_location_reference) values (:name,:ref) returning id",[':name'=>$vpointName, ':ref'=>$locationReferences[$inx]]);
                    $placeholderAttributes[':trap_station_name']['values'][$vpointName] = $idres[0]['id'];
                }
                Mbase2Database::query("COMMIT");

                $indices[':trap_station_name'] = $ccols;

                /*
                    [:trap_station_name] => Array
                    (
                        [lat] => 1
                        [lon] => 2
                        [crs] => 4326
                    )
                */
            }
        }
        else if ($keyModuleId === 'dmg') {
            $placeholderAttributes[':_dmg_object_unit']['keys'] = array_flip($placeholderAttributes[':_dmg_object_unit']['values']);
            $placeholderAttributes[':dmg_protection_measures']['keys'] = array_flip($placeholderAttributes[':dmg_protection_measures']['values']);
            $placeholderAttributes[':_dmg_object_list_ids']['keys'] = array_flip($placeholderAttributes[':_dmg_object_list_ids']['values']);
        }
        else if ($keyModuleId === 'cnt_monitoring') {
            ;
        }
    }

    /**
     * Adds referenced values associative array to the 'values' key of the respective attribute definition
     */

    private function addReferencedValuesToAttributes(&$attributes, $referencedCodeListValues, $referencedTableValues, $clId) {
        foreach($attributes as &$a) {
            if (in_array($a['data_type'], ['code_list_reference', 'code_list_reference_array', 'table_reference', 'table_reference_array'])) {

                $referencedValues = in_array($a['data_type'], ['code_list_reference', 'code_list_reference_array']) ? $referencedCodeListValues : $referencedTableValues;

                $a['values'] = Mbase2Utils::getReferencedValues($referencedValues, $a['ref']);
                $a['values_en'] = Mbase2Utils::getReferencedValues($referencedValues, $a['ref'], true, 'en');
                if ($this->languageOfTheImport !== 'en') {
                    $a['values_local'] = Mbase2Utils::getReferencedValues($referencedValues, $a['ref'], true, $this->languageOfTheImport);
                }
            }
            else if ($a['data_type'] === 'location_reference') {
                $a['_locations_table'] = $clId[$a['ref']];
                if (empty($a['_locations_table'])) {
                    $a['_locations_table'] = $this->keyModuleId.'_locations';
                }
            }
        }

        unset($a);
    }

    private static function getReferencedTablesData($referencedTableIds, $clId) {
        $res = empty($referencedTableIds) ? [] : Mbase2Database::select('mbase2.referenced_tables',[':id' => $referencedTableIds]);
        
        $tableNameAssocId = [];
        foreach($res as &$r) {
            $listId = $r['key_id'] = $clId[$r['id']];
            $tableNameAssocId[$listId] = $r['id'];
        }

        return [
            'referencedTablesResults' => $res,
            'referencedTableNamesAssocId' => $tableNameAssocId
        ];
    }

    private function getReferenceValues($attributes, $keyModuleId) {
        $codeListOptionsIds = [];
        $referencedTableIds = [];

        $preloadImageFileNames = false;

        foreach ($attributes as $a) {
            $dataType = $a['data_type'];

            if ($dataType === 'image' || $dataType === 'image_array') {
                $preloadImageFileNames = true;
            }
            
            if (!empty($a['ref'])) {
                if ($dataType === 'code_list_reference' || $dataType === 'code_list_reference_array') {
                    $codeListOptionsIds[] = $a['ref'];
                }
                else if ($dataType === 'table_reference' || $dataType === 'table_reference_array') {
                    $referencedTableIds[] = $a['ref'];
                }
            }
        }

        $media = [];

        if ($preloadImageFileNames) {
            $res = Mbase2Database::query("SELECT * FROM mbase2.uploads WHERE private = true and subfolder=:subfolder order by record_created", [':subfolder' => $keyModuleId]);
            foreach ($res as $r) {
                $media[$r['file_name']] = $r['file_hash'];
            }
        }

        return [
            'codeListOptionsIds' => $codeListOptionsIds,
            'referencedTableIds' => $referencedTableIds,
            'media' => $media
        ];
    }

    public static function explodeTimeStamp($dataType, $value) {
        $value = preg_replace('!\s+!', ' ', $value); //replace multiple spaces with a single space

        $value = explode(' ', $value);
        if (count($value)===1) {
            $value = $value[0];
        }
        else {
            if ($dataType === 'date') {
                $value = $value[0];
            }
            else {
                $value = $value[1];
            }
        }

        return $value;
    }

    public static function convertAndCheckIfIsoValid(&$timestamp, $format, $dataType){
        if (strpos($timestamp, ':') !== FALSE) $format = "Y-m-d H:i:s";
        $dt = DateTime::createFromFormat($format, $timestamp);

        $r = $dt !== false && !array_sum($dt::getLastErrors());

        if ($r === TRUE && $format !== 'Y-m-d') {
            
            $f = null;
            
            if ($dataType === 'date') {
                $f = 'Y-m-d';
            }
            else if ($dataType === 'time') {
                $f = 'H:i:s';
            }
            else {
                $f = 'Y-m-d H:i:s';
            }

            $timestamp = $dt->format($f);
        }

        return $r;
    }

    public static function excelDate2iso($date){
        if (empty($date)) return null;
        $UNIX_DATE = ($date - 25569) * 86400;
        return gmdate("Y-m-d H:i:s", $UNIX_DATE);
      }

    function camelotDataDefinitions($camelotId, $uid, $header) {
        $dataDefinitions = [
            $this->keyModuleId => ['types' => [], 'values' => []]
        ];

        $types = [
            "_uname" => "fixed",
            "_species_name"=>"column",
            "_licence_name"=>"fixed",
            "event_date"=>"column",
            "photos"=>"column",
            "sighting_quantity"=>"column",
            "trap_station_name"=>"column",
            "event_time"=>"column",
            "sex"=>"column",
            "individual_name"=>"column",
            "life_stage"=>"column",
            "position"=>"column",
            "hair_trap"=>"column",
            "notes"=>"column",
            "survey_name"=>"column",
            "camera_name"=>"column"
        ];

        $dataDefinitions[$this->keyModuleId]['types'] = $types;

        $typeValues = [
            "_species_name" => "Species Common Name",
            "event_date"=>"Date/Time",
            "photos"=>"Media Filename",
            "sighting_quantity"=>"Sighting Quantity",
            "trap_station_name"=>"Trap Station ID",
            "event_time"=>"Date/Time",
            "sex"=>"Sex",
            "individual_name"=>"Individual name",
            "life_stage"=>"Life stage",
            "position"=>"Position",
            "hair_trap"=>"Hair trap",
            "notes"=>"Remarks",
            "survey_name"=>"Survey ID",
            "camera_name"=>"Camera ID"
        ];

        foreach ($types as $cname => $type) {
            if ($type === 'fixed') continue;
            $typeValues[$cname] = array_search($typeValues[$cname], $header);

        }

        $res = \DB::select("SELECT _licence_name as lid from mb2data.ct_camelot_sources WHERE id=:id", [':id' => $camelotId]);

        $typeValues["_licence_name"] = $res[0]->lid; //Mbase licence
        $typeValues["_uname"] = $uid; //user id
        
        $dataDefinitions[$this->keyModuleId]['values'] = $typeValues;
                
        return $dataDefinitions;
    }

    function validateAttributeValue($placeholder, $dataType, $value, $rinx, $cinx, $required, $errorMsgAlt='') {

        $validationResult = true;

        if (empty($errorMsgAlt) && (empty($value) && !$required || ($this->importType === 'update'))) {
            return true;
        }

        if (is_null($value) && $required) {
            $validationResult = "The required value is empty.";
            if ($dataType === 'location_reference' || $dataType === 'location_data_json') {
                if (is_array($cinx)) {
                    $cinx = $cinx['lat'].','.$cinx['lon'];
                }
            }
        }
        else if ($dataType === 'code_list_reference' || $dataType === 'table_reference' || $dataType === 'table_reference_array' || $dataType === 'code_list_reference_array') {
            if (empty($value) && $value !== 0) {
                $validationResult = 'Missing reference.';
            }
        }
        else if ($dataType === 'integer') {
            if ( filter_var($value, FILTER_VALIDATE_INT) === false ) { //https://stackoverflow.com/questions/6416763/checking-if-a-variable-is-an-integer-in-php
                $validationResult = 'Not an integer.';
            }
        }
        else if ($dataType === 'real') {
            if (!is_numeric($value)) {
                $validationResult = 'Not a numeric.';
            }
        }
        else if ($dataType === 'text') {
            ;
        }
        else if ($dataType === 'jsonb') {
            if (is_null(json_decode($value))) {
                $validationResult = 'Not a valid json.';
            }
        }
        else if ($dataType === 'image') {
        }
        else if ($dataType === 'image_array') {
            if (is_null($value)) {
                $validationResult = 'missing image';
            }
        }
        else if ($dataType === 'time') {
            if (!preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]$/", $value)) {
                $validationResult = 'The time is not in the HH:MM:SS format.';
            }
        }
        else if ($dataType === 'date') {
            $dt = DateTime::createFromFormat($this->dateFormat, $value);
            if ($dt === false) {
                $validationResult = 'The date is not in the YYYY-MM-DD format.';
            }
            else {
                //array_sum trick is a terse way of ensuring that PHP did not do "month shifting" (e.g. consider that January 32 is February 1)
                //cf. https://stackoverflow.com/questions/13194322/php-regex-to-check-date-is-in-yyyy-mm-dd-format
                $monthShifting = array_sum($dt::getLastErrors());

                if ($monthShifting) $validationResult = 'Wrong date.';
            }
        }

        return $validationResult === true ? true : $this->errorMessage($placeholder, $rinx, $cinx, $validationResult, $required, $errorMsgAlt);
    }

    /**
     * Helper function: generates consistent error message
     * @param {integer} $rinx row index
     * @param {integer} $cinx column index
     * @param {string} $key_name_id variable name
     */

    private function errorMessage($placeholder, $rinx, $cinx, $error, $required, $errorMsgAlt='', $encode = true) {
        $value = empty($cinx) ? null : (isset($this->currentRow[$cinx]) ? $this->currentRow[$cinx] : null);
        $variableKey = ltrim($placeholder,':');
        $err = [
            'row'=> $rinx,
            'column'=> $cinx,
            'value' => $value,
            'variable' => $variableKey,
            'error' => $error,
            'required' => $required
        ];
        
        if (!empty($errorMsgAlt)) {
            $err['error'].=': '.$errorMsgAlt;
        }

        return $encode ? json_encode($err) : $err;
    }
}
