<?php

/*
mb2data.cnt_monitoringArray
              
(
    [0] => Array
        (
            [id] => 5
            [_vrsta_opazovanja] => 17366
            [_start_date] => 2021-08-10
            [_end_date] => 2021-08-10
            [_notes] => 
            [monitoring_data] => 
        )

)

[8] => Array
        (
            [id] => 9
            [_location] => 0101000020E6100000567F8A509F652D40B4A18C6F87E84640
            [_location_data] => {"_location": {"lat": 45.8166331707647, "lon": 14.6984810990395, "spatial_request_result": {"oe_ime": "Kočevje", "lov_ime": "DOBREPOLJE", "luo_ime": "Kočevsko Belokranjsko", "ob_uime": "Dobrepolje"}}}
            [_local_name] => Vrhunska voda
            [_cnt_permanent_spot_code] => 061-047
            [_cnt_spot_skrbnik] => 
            [_cnt_active] => 1
        )

*/

class Mbase2Triggers {

    public $updateParameters = [];
    public $json_flags;
    public $dbFunctionName = null;
    public $removeParameters = [];

    function __construct($triggerBefore, $requestType, $tname, $result, $input, $json_flags) {
        $this->tname = $tname;
        $this->result = $result;
        $this->requestType = $requestType;
        $this->input = $input;
        $this->updateParameters = [];
        $this->json_flags = $json_flags;

        if ($triggerBefore) {
            $this->preprocess();
        }
        else {
            $this->process();
        }
    }

    private function preprocess() {
    

        if ($this->tname === 'mb2data.dmg_affectees') {
            $this->json_flags = 0;
        }
        else if ($this->tname === 'mb2data.gensam') {
            if ($this->requestType ==='POST' && isset($this->input[':sample_code'])) {
                $sampleCode = trim($this->input[':sample_code']);
                if (!Mbase2Utils::checkSampleCode($sampleCode)) {
                    throw new Exception("The sample code does not follow the database sample code rules.");
                }
            }
        }
        /*if ($this->tname === 'mb2data.dmg') {
            if ($this->requestType ==='POST' || $this->requestType ==='PUT') {
                if (isset($this->input[':_affectees'])) {
                    $adata = \DB::table('mb2data.dmg_affectees')
                    ->whereIn('id', $this->input[':_affectees'])
                    ->get()->all();

                    $this->updateParameters[':affectees_data'] = $adata;
                }
            }    
        }*/
    }

    private function process() {
        if ($this->requestType==='POST') {
            if ($this->tname === 'mb2data.cnt_monitoring') {
                $this->cnt_monitoring($this->result);
            }
        }
        else if ($this->requestType==='DELETE') {
            if ($this->tname==='Mbase2Files.deleteUploadedFile') {
                if ($this->result['subfolder']==='dmg') {
                    $id = intval($this->result['id']);
                    $res = Mbase2Database::query("SELECT id, _attachments a FROM mb2data.dmg dc WHERE _attachments @> :x",[':x'=>'[{"id":'.$id.'}]']);
                    if (!empty($res)) {
                        $res = $res[0];
                        $ats=[];
                        $attachments = json_decode($res['a']);
                        foreach ($attachments as $a) {
                            if ($a->id==$id) continue;
                            $ats[] = $a;
                        }

                        Mbase2Database::query("UPDATE mb2data.dmg SET _attachments=:a WHERE id = :id", [':a'=>json_encode($ats), ':id'=>$res['id']]);
                    }
                }
            }
        }
    }

    private function cnt_monitoring() {
        $vrstaMonitoringaId = @$this->result[0]['_vrsta_opazovanja'];
        if (empty($vrstaMonitoringaId)) return;

        $clrow = Mbase2Database::query("SELECT * FROM mbase2.code_list_options_vw WHERE id=:id",[':id' => $vrstaMonitoringaId]);

        if (isset($clrow[0]) && $clrow[0]['key'] === 'stalna števna mesta') {
            $res = Mbase2Database::query("SELECT * FROM mb2data.cnt_permanent_spots WHERE _cnt_active=true");
            $monitoringId = $this->result[0]['id'];

            $values = "($monitoringId, ".implode("),($monitoringId,",array_column($res,'id')).')';

            Mbase2Database::query("INSERT INTO mb2data.cnt_observation_reports (cnt_monitoring_id, _cnt_location_id) values $values");
        }
        else {
            $res = Mbase2Database::query("SELECT * FROM mbase2.lov_vw");
            $monitoringId = $this->result[0]['id'];
            $values = "($monitoringId, ".implode("),($monitoringId,",array_column($res,'koda')).')';
            Mbase2Database::query("INSERT INTO mb2data.cnt_estimations (cnt_monitoring_id, _cnt_lov_koda) values $values");
        }
        
    }
}
