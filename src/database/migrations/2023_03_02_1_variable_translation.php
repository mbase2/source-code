<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    
        \DB::update("update mbase2.code_list_options set translations=translations || :t where id = 
        (select id from mbase2.code_list_options_vw clov where key='gensam' and list_key='modules')",[':t'=>'{"sl":"Genetski vzorci"}']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
};
