<?php

use Illuminate\Database\Migrations\Migration;
use Mbase2dtl\Helpers\GeneralMbase2Helper;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ini_set('memory_limit', '-1');
  
        \DB::statement("DROP schema IF EXISTS mb2data CASCADE");
        \DB::statement("DROP schema IF EXISTS  mbase2 CASCADE");

        \DB::unprepared(file_get_contents(__DIR__.'/sql/mbase2_ge-schema-with-data-exp.sql'));
        \DB::unprepared(file_get_contents(__DIR__.'/sql/mbase2_ge__spatial_units.sql'));
        \DB::statement("CREATE INDEX IF NOT EXISTS spatial_units_geom_idx ON mbase2_ge.spatial_units USING gist (geom)");

        \DB::unprepared(file_get_contents(__DIR__.'/sql/mbase2_schema_updated.sql'));
        \DB::unprepared(file_get_contents(__DIR__.'/sql/mbase2_data_updated.sql'));
        \DB::unprepared(file_get_contents(__DIR__.'/sql/mb2data-schema-only_updated.sql'));
     
        GeneralMbase2Helper::updateCodeListOptionsForeignKeysMaterializedView();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
