<?php

use Illuminate\Database\Migrations\Migration;

require_once(__DIR__.'/../../mbase2/Mbase2SchemaPatches.php');

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //add code lists as referenced tables

        $tnames = \DB::select("SELECT table_name FROM information_schema.tables 
        WHERE table_schema = 'laravel' and table_name like '%_list'");

        foreach ($tnames as $row) {
            $tname = $row->table_name;
            \Mbase2SchemaPatches::addReferenceTable($tname,'id','laravel','slug');
        }

        \Mbase2SchemaPatches::addCodeListOption('referenced_tables', 'laravel.animal');
        \Mbase2SchemaPatches::addCodeListOption('referenced_tables', 'laravel.bears_biometry_animal_handling');
        \Mbase2SchemaPatches::addCodeListOption('referenced_tables', 'laravel.bears_biometry_data');

        \Mbase2SchemaPatches::addReferenceTable('laravel.animal','id','laravel','name', "SELECT id, id::varchar, coalesce(name, 'ANIMAL_ID_' || id::varchar) name from laravel.animal");

        \DB::statement("create or replace view mbase2.laravel_luo_vw as
        SELECT id, (name::jsonb)->>'name' name from laravel.spatial_unit_filter_elements where upper(slug) like '%LUO%'");

        \DB::statement("create or replace view mbase2.laravel_lov_vw as
        SELECT id, (name::jsonb)->>'name' name from laravel.spatial_unit_filter_elements where upper(slug) like '%LOV%'");

        \Mbase2SchemaPatches::addReferenceTable('laravel_lov_vw','id','mbase2','name');
        \Mbase2SchemaPatches::addReferenceTable('laravel_luo_vw','id','mbase2','name');

        \Mbase2SchemaPatches::importVariables([
            [
                'key_name_id' => 'species_list_id',
                'translations' => ['en' => 'Species', 'sl' => 'Živalska vrsta'],
                'key_data_type_id' => 'table_reference',
                'ref' => 'species_list',
                'required' => true
            ],
            [
                'key_name_id' => 'sex_list_id',
                'translations' => ['en' => 'Sex', 'sl' => 'Spol'],
                'key_data_type_id' => 'table_reference',
                'ref' => 'sex_list',
                'required' => true
            ],
            [
                'key_name_id' => 'status',
                'key_data_type_id' => 'text'
            ],
            [
                'key_name_id' => 'title',
                'key_data_type_id' => 'text'
            ],
            [
                'key_name_id' => 'name',
                'key_data_type_id' => 'text'
            ],
            [
                'key_name_id' => 'died_at',
                'key_data_type_id' => 'timestamp'
            ]], 'laravel.animal','referenced_tables',
            [
                'importable' => true,
                'required' => false
            ]
        );

        $dataTypeMap = [
            'boolean'=>'boolean',
            'character varying'=>'text',
            'double precision'=>'real',
            'integer'=>'integer',
            'text'=>'text',
            'timestamp with time zone'=>'timestamp'
        ];

        $skipColumns = [
            'id',
            'created_at',
            'updated_at',
            'lat',
            'lng',
            'data_entered_by_user_id',
            'spatial_unit_gid',
            'x',
            'y',
            'zoom',
            'hunting_management_area_id',
            'hunter_finder_country_id',
            'hunting_ground',
            'hunting_management_area',
            'animal_id',
            'bears_biometry_animal_handling_id'
        ];

        \Mbase2SchemaPatches::importVariables([
            [
                'key_name_id' => '_location_data',
                'key_data_type_id' => 'location_data_json',
                'required' => true
            ],
            [
                'key_name_id' => 'hunting_ground',
                'translations' => ['en' => 'Hunting ground', 'sl' => 'Lovišče'],
                'key_data_type_id' => 'table_reference',
                'ref' => 'laravel_lov_vw',
                'required' => false
            ],
            [
                'key_name_id' => 'hunting_management_area',
                'translations' => ['en' => 'Hunting management area', 'sl' => 'LUO'],
                'key_data_type_id' => 'table_reference',
                'ref' => 'laravel_luo_vw',
                'required' => false
            ]
        
            ], 'laravel.bears_biometry_animal_handling','referenced_tables',
            [
                'importable' => true,
                'required' => false
            ]
        );

        foreach (['bears_biometry_animal_handling', 'bears_biometry_data'] as $tname) {

            $columns = \DB::select("select column_name, is_nullable, data_type from information_schema.columns where table_name = :tname and table_schema = 'laravel'",[':tname' => $tname]);

            $variables = [];

            foreach($columns as $row) {
                $cname = $row->column_name;
                $dataType = $row->data_type;
                $isNullable = $row->is_nullable;

                if (in_array($cname, $skipColumns)) continue;

                $dataType = $dataTypeMap[$dataType];

                if (empty($dataType)) continue;

                $ref = null;
                $required = $isNullable === "YES" ? false : true;

                if ($dataType === 'integer' && strpos($cname,'_list_id')!==FALSE) { //table reference
                    $dataType = 'table_reference';
                    $ref = str_replace('_list_id','_list',$cname);
                }

                $variables[] = [
                    'key_name_id' => $cname,
                    'key_data_type_id' => $dataType,
                    'required' => $required,
                    'ref' => $ref
                ];
            }

            \Mbase2SchemaPatches::importVariables($variables, 'laravel.'.$tname,'referenced_tables',
                [
                    'importable' => true,
                    'required' => false
                ]
            );
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
