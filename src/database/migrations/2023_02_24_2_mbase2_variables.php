<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    
        \DB::update("update mbase2.module_variables SET ref = (select id from mbase2.referenced_tables_vw where key='licence_list') 
        where id in (select id from mbase2.module_variables_vw mvv where reference = 'licences')");

        \DB::update("update mbase2.referenced_tables set label_key = 'naziv' where id = (select id from mbase2.referenced_tables_vw where key='oe')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
};
