<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Storage;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $path = Storage::path('');
        if (!file_exists($path.'mbase2')) {
            mkdir($path.'mbase2', 0777, true);
        }

        if (!file_exists($path.'mbase2/.private')) {
            mkdir($path.'mbase2/.private', 0777, true);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
