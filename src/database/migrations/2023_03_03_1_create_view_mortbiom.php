<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    
        \DB::statement("DROP VIEW IF EXISTS mb2data.mortbiom_vw");
        \DB::statement("create or replace view mb2data.mortbiom_vw as
        select h.id,a.species_list_id, 
        h.animal_handling_date as event_date,
        data_entered_by_user_id as _uname,
        public.st_setsrid(public.st_makepoint(lng,lat),4326) as _location,
        ('{\"coordinates\":['|| lng||','|| lat ||']}')::json as geom
        from laravel.bears_biometry_animal_handling h, laravel.animal a
        where h.animal_id=a.id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("DROP VIEW IF EXISTS mb2data.mortbiom_vw");
    }
};
