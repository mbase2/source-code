<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::unprepared(file_get_contents(dirname(__FILE__) . '/sql/slo_lovd_2023_wgs84.sql'));
		DB::unprepared(file_get_contents(dirname(__FILE__) . '/sql/2023_07_29_add_spatial_units.sql'));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\DB::delete("delete from laravel.spatial_units_spatial_unit_filter_elements where spatial_unit_filter_element_id in 
		(select id from laravel.spatial_unit_filter_elements where spatial_unit_filter_type_version_id in(4,5))");

		\DB::delete("delete from mbase2_ge.spatial_units where gid not in (select spatial_unit_gid from laravel.spatial_units_spatial_unit_filter_elements)");

		\DB::delete("delete from laravel.spatial_unit_filter_elements where spatial_unit_filter_type_version_id in(4,5)");

		\DB::delete("delete from laravel.spatial_unit_filter_type_versions where id in (5)");

		\DB::statement("drop table mbase2_ge.slo_lovd_2023_wgs84");
	}
};
