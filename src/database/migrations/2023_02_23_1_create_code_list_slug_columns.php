<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tnames = \DB::select("SELECT table_name FROM information_schema.tables 
        WHERE table_schema = 'laravel' and table_name like '%_list'");

        foreach($tnames as $tname) {
            $tname=$tname->table_name;
            \DB::statement("alter table laravel.$tname add column if not exists slug varchar(128) unique;");
            \DB::statement("UPDATE laravel.$tname SET slug = coalesce(name->>'en', name->>'default')");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tnames = \DB::select("SELECT table_name FROM information_schema.tables 
        WHERE table_schema = 'laravel' and table_name like '%_list'");

        foreach($tnames as $tname) {
            $tname=$tname->table_name;
            \DB::statement("alter table laravel.$tname drop column if exists slug");
        }
    }
};
